-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: gallery
-- ------------------------------------------------------
-- Server version	5.5.44-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abuses`
--

DROP TABLE IF EXISTS `abuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abuses` (
  `abuid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `abuObj` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'objid',
  `abuIsExtras` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'object in "extras" table instead of "objects" table?',
  `abuSubmitter` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `abuSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `abuComment` text NOT NULL COMMENT 'abuse submitter comment',
  `abuCreator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'The owner of the reported object.',
  `abuMod` enum('?','+','-') CHARACTER SET latin1 NOT NULL DEFAULT '?' COMMENT 'moderator''s decision',
  `abuModName` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid of moderator',
  `abuModComment` text NOT NULL COMMENT 'moderator comment',
  `abusMod` enum('?','+','-') CHARACTER SET latin1 NOT NULL DEFAULT '?' COMMENT 'supermoderator''s decision',
  `abusModName` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid of supermoderator',
  `abusModComment` text NOT NULL COMMENT 'supermoderator comment',
  `aburMod` enum('?','+','-') CHARACTER SET latin1 NOT NULL DEFAULT '?' COMMENT 'reviewer moderator''s decision',
  `aburModName` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid of reviewer moderator',
  `aburModComment` text NOT NULL COMMENT 'reviewer moderator comment',
  `abuFinal` enum('?','+','-') NOT NULL DEFAULT '?' COMMENT 'The final decision by the moderators.',
  `abuCloseDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the final decision was made.',
  `abuRule` varchar(3) NOT NULL COMMENT 'The rule violated',
  `abuReason` text NOT NULL COMMENT 'a brief, to-the-point reason for the original abuse submission, to be displayed in the submission artist''s comments once abuse moderation has ended',
  `abuResolved` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  PRIMARY KEY (`abuid`),
  KEY `abuSubmitDate` (`abuSubmitDate`),
  KEY `abuMod` (`abuMod`),
  KEY `aburMod` (`aburMod`,`abusMod`,`abuMod`),
  KEY `abusMod` (`abusMod`,`abuMod`),
  KEY `abuCreator` (`abuCreator`)
) ENGINE=InnoDB AUTO_INCREMENT=43455 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Abuse handling data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `abusesRules`
--

DROP TABLE IF EXISTS `abusesRules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abusesRules` (
  `abrId` smallint(5) NOT NULL AUTO_INCREMENT,
  `abrRule` varchar(10) NOT NULL COMMENT 'The rule violated.',
  `abrText` text NOT NULL COMMENT 'Auto-text when submission is removed. ',
  PRIMARY KEY (`abrId`),
  KEY `abrRule` (`abrRule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The automatic text shown to the user when a submission is de';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `access`
--

DROP TABLE IF EXISTS `access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access` (
  `accIp` char(8) NOT NULL DEFAULT '',
  `accCount` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`accIp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='IP access info';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `accessExt`
--

DROP TABLE IF EXISTS `accessExt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accessExt` (
  `accid` int(11) NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'useid',
  `isPermAdmin` set('0','1','','') NOT NULL DEFAULT '0',
  `isKeywordsAdmin` set('0','1','','') NOT NULL DEFAULT '0',
  `isAdminChat` set('0','1','','') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adminChat`
--

DROP TABLE IF EXISTS `adminChat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adminChat` (
  `adcId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adcText` text NOT NULL,
  `adcCreator` int(10) unsigned NOT NULL DEFAULT '0',
  `adcSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`adcId`),
  KEY `adcSubmitDate` (`adcSubmitDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adminNotes`
--

DROP TABLE IF EXISTS `adminNotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adminNotes` (
  `admid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admNote` text NOT NULL,
  `admCreator` int(10) unsigned NOT NULL DEFAULT '0',
  `admSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`admid`),
  KEY `admSubmitDate` (`admSubmitDate`)
) ENGINE=InnoDB AUTO_INCREMENT=11187 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ads`
--

DROP TABLE IF EXISTS `ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `startdate` date NOT NULL,
  `enddate` date NOT NULL,
  `type` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `url` longtext COLLATE utf8_unicode_ci NOT NULL,
  `clicks` int(11) NOT NULL,
  `shows` int(11) NOT NULL,
  `data_blob` longblob NOT NULL,
  `data_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `type` enum('sidebar','banner') NOT NULL,
  `url` longtext NOT NULL,
  `clicks` int(11) NOT NULL DEFAULT '0',
  `shows` int(11) NOT NULL DEFAULT '0',
  `data-blob` blob NOT NULL,
  `data-name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `banid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `banShows` int(10) unsigned NOT NULL DEFAULT '0',
  `banType` enum('banner','sidebar') DEFAULT 'sidebar',
  `banClicks` int(10) unsigned NOT NULL DEFAULT '0',
  `banURL` varchar(255) NOT NULL DEFAULT '',
  `banUntil` date DEFAULT NULL,
  PRIMARY KEY (`banid`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cluExtData`
--

DROP TABLE IF EXISTS `cluExtData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cluExtData` (
  `cluEid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'cluid',
  `cluCreationDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cluCreator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `cluRequireAccept` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '1',
  `cluProfile` text NOT NULL,
  `cluFeaturedObj` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'objid',
  `cluFeaturedDesc` varchar(255) NOT NULL DEFAULT '',
  `cluIdObj` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'objid',
  `cluNoMembers` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'disallow membership?',
  `cluWatcherCount` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'club popularity',
  PRIMARY KEY (`cluEid`),
  KEY `cluWatcherCount` (`cluWatcherCount`),
  KEY `cluCreationDate` (`cluCreationDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Extended user club data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clubObjects`
--

DROP TABLE IF EXISTS `clubObjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clubObjects` (
  `cloid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cloClub` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'clubs.cluid',
  `cloObject` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'objects.objid',
  PRIMARY KEY (`cloid`),
  KEY `cloObject` (`cloObject`),
  KEY `cloClub` (`cloClub`)
) ENGINE=InnoDB AUTO_INCREMENT=1922065 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Objects posted in a club';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clubs`
--

DROP TABLE IF EXISTS `clubs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clubs` (
  `cluid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cluName` varchar(255) NOT NULL DEFAULT '',
  `cluDesc` varchar(255) NOT NULL DEFAULT '',
  `cluHide` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `cluIsProject` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  PRIMARY KEY (`cluid`),
  KEY `cluIndexes` (`cluHide`,`cluIsProject`)
) ENGINE=InnoDB AUTO_INCREMENT=11554 DEFAULT CHARSET=utf8 COMMENT='User club data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `commentDrafts`
--

DROP TABLE IF EXISTS `commentDrafts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commentDrafts` (
  `drfid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `drfUser` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'users.useid',
  `drfObjType` varchar(3) NOT NULL DEFAULT '' COMMENT 'comments.comObjType',
  `drfObj` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'comments.comObj',
  `drfText` text NOT NULL,
  PRIMARY KEY (`drfid`),
  KEY `drfUser` (`drfUser`,`drfObjType`,`drfObj`)
) ENGINE=InnoDB AUTO_INCREMENT=264 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Comment drafts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `comid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `comObj` int(11) unsigned NOT NULL DEFAULT '0',
  `comObjType` enum('obj','com','clu','jou','use','new','pol','ext') CHARACTER SET latin1 NOT NULL DEFAULT 'obj',
  `comRootObj` int(11) unsigned NOT NULL DEFAULT '0',
  `comRootObjType` enum('obj','com','clu','jou','use','new','pol','ext') NOT NULL DEFAULT 'obj',
  `comCreator` int(11) unsigned NOT NULL DEFAULT '0',
  `comSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comSubmitIp` varchar(8) NOT NULL DEFAULT '',
  `comComment` text NOT NULL,
  `comNoEmoticons` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disable emoticons in time/tools/comments',
  `comNoSig` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disable signature append to comments',
  `comNoBBCode` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disable BBCode in time/tools/comments',
  `comLastEdit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comEditIp` varchar(8) NOT NULL DEFAULT '',
  `comTotalEdits` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comid`),
  KEY `comCreator` (`comCreator`),
  KEY `comIndexes` (`comObjType`,`comObj`,`comSubmitDate`)
) ENGINE=InnoDB AUTO_INCREMENT=15025067 DEFAULT CHARSET=utf8 COMMENT='Artwork comment data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `conName` varchar(255) NOT NULL DEFAULT '',
  `conValue` varchar(255) NOT NULL DEFAULT '',
  `conDesc` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`conName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='System-wide configuration';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customThemes`
--

DROP TABLE IF EXISTS `customThemes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customThemes` (
  `cusid` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `cusUser` int(11) unsigned NOT NULL DEFAULT '0',
  `cusIsClub` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `cusData` text NOT NULL,
  PRIMARY KEY (`cusid`),
  KEY `cusUser` (`cusUser`)
) ENGINE=InnoDB AUTO_INCREMENT=6807 DEFAULT CHARSET=utf8 COMMENT='Custom themes data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `donations`
--

DROP TABLE IF EXISTS `donations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donations` (
  `donid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `donAmt` decimal(6,2) unsigned NOT NULL DEFAULT '0.00',
  `donCreator` int(11) unsigned NOT NULL DEFAULT '0',
  `donDate` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`donid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Donation tracking';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emoticons`
--

DROP TABLE IF EXISTS `emoticons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emoticons` (
  `emoid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `emoExpr` varchar(16) NOT NULL DEFAULT '',
  `emoFilename` varchar(32) NOT NULL DEFAULT '',
  `emoExtra` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Do not display in selection list; for synonymous smilies',
  PRIMARY KEY (`emoid`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COMMENT='Emoticon translation';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `extExtData`
--

DROP TABLE IF EXISTS `extExtData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extExtData` (
  `objEid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'objid',
  `objImageWidth` smallint(5) unsigned NOT NULL DEFAULT '0',
  `objImageHeight` smallint(5) unsigned NOT NULL DEFAULT '0',
  `objImageSize` int(9) unsigned NOT NULL DEFAULT '0' COMMENT 'image file size',
  `objImageURL` varchar(255) NOT NULL DEFAULT '' COMMENT 'full URL of the image file',
  `objPreviewWidth` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'preview image resolution (0x0 = no preview)',
  `objPreviewHeight` smallint(5) unsigned NOT NULL DEFAULT '0',
  `objPreviewURL` varchar(255) NOT NULL DEFAULT '' COMMENT 'full URL of the preview image file',
  `objComment` text NOT NULL COMMENT 'Creator''s comment on the object',
  `objNumEdits` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of edits this object has had',
  `objNoEmoticons` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disable emoticons in time/tools/comments',
  `objNoSig` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disable signature append to comments',
  `objNoBBCode` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disable BBCode in time/tools/comments',
  `objNoAbuse` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disallow the object from being submitted for abuse again if it has been cleared',
  `objSubmitIp` varchar(8) NOT NULL DEFAULT '',
  `objEditIp` varchar(8) NOT NULL DEFAULT '',
  `objDeletedBy` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `objDeleteDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'when deleted?',
  `objCollabConfirmed` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Is collaboration confirmed by the other participant?',
  PRIMARY KEY (`objEid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 COMMENT='Extended extras data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `extras`
--

DROP TABLE IF EXISTS `extras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extras` (
  `objid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `objTitle` varchar(255) NOT NULL DEFAULT '',
  `objCreator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `objForClub` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'cluid',
  `objForClub2` int(11) unsigned NOT NULL DEFAULT '0',
  `objForClub3` int(11) unsigned NOT NULL DEFAULT '0',
  `objForUser` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `objFolder` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'folid',
  `objCollab` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `objThumbWidth` smallint(5) unsigned NOT NULL DEFAULT '0',
  `objThumbHeight` smallint(5) unsigned NOT NULL DEFAULT '0',
  `objThumbURL` varchar(255) NOT NULL DEFAULT '' COMMENT 'full URL of the thumbnail image file',
  `objSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `objLastEdit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `objViewed` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of views the object has received',
  `objPopularity` int(11) unsigned NOT NULL DEFAULT '0',
  `objMature` set('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Hide mature objects from unregistered/under 18 users',
  `objPending` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'This object is pending abuse investigation, see abuse table for more information',
  `objPendingUser` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'Pending input from user during abuse moderation.',
  `objDeleted` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'This object has been deleted',
  `objGuestAccess` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`objid`),
  KEY `objSubmitDate` (`objDeleted`,`objPending`,`objSubmitDate`),
  KEY `objCreator` (`objCreator`,`objPending`,`objDeleted`,`objSubmitDate`,`objPendingUser`)
) ENGINE=InnoDB AUTO_INCREMENT=9067 DEFAULT CHARSET=utf8 CHECKSUM=1 COMMENT='Extras data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fans`
--

DROP TABLE IF EXISTS `fans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fans` (
  `fanUser` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'users.useid',
  `fanArtist` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'users.useid',
  `fanNumViews` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'How many times the User viewed that Artists works',
  `fanNumComments` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'How many times the User commented on that Artists works',
  KEY `fanIndex` (`fanArtist`,`fanUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Fan-to-Artist statistics';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `favourites`
--

DROP TABLE IF EXISTS `favourites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favourites` (
  `favObj` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'objid',
  `favCreator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `favSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `favObj` (`favObj`),
  KEY `favCreator` (`favCreator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Favourite''d artwork data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `filters`
--

DROP TABLE IF EXISTS `filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filters` (
  `fltid` int(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Filter index (0 to 63)',
  `fltName` varchar(20) NOT NULL DEFAULT '' COMMENT 'Filter name (English string) -- run /strings for changes to take effect',
  PRIMARY KEY (`fltid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Filters';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `folders`
--

DROP TABLE IF EXISTS `folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders` (
  `folid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `folCreator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `folIdent` varchar(15) NOT NULL DEFAULT '' COMMENT 'identifier (canonized short name)',
  `folName` varchar(40) NOT NULL DEFAULT '' COMMENT 'full name',
  PRIMARY KEY (`folid`),
  KEY `folCreator` (`folCreator`)
) ENGINE=InnoDB AUTO_INCREMENT=46551 DEFAULT CHARSET=utf8 CHECKSUM=1 COMMENT='Folders inside user galleries';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forgiveness_2007`
--

DROP TABLE IF EXISTS `forgiveness_2007`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forgiveness_2007` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `ban_reason` varchar(255) NOT NULL DEFAULT '',
  `why` text NOT NULL,
  `other_accounts` text NOT NULL,
  `birthdate` date NOT NULL DEFAULT '0000-00-00',
  `valid_birthdate` tinyint(1) NOT NULL DEFAULT '0',
  `valid_email` tinyint(1) NOT NULL DEFAULT '0',
  `remote_addr` int(11) NOT NULL DEFAULT '0',
  `granted` tinyint(1) DEFAULT NULL,
  `comments` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forgiveness_2008`
--

DROP TABLE IF EXISTS `forgiveness_2008`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forgiveness_2008` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `ban_reason` varchar(255) NOT NULL DEFAULT '',
  `why` text NOT NULL,
  `other_accounts` text NOT NULL,
  `birthdate` date NOT NULL DEFAULT '0000-00-00',
  `valid_birthdate` tinyint(1) NOT NULL DEFAULT '0',
  `valid_email` tinyint(1) NOT NULL DEFAULT '0',
  `remote_addr` int(11) NOT NULL DEFAULT '0',
  `granted` tinyint(1) DEFAULT NULL,
  `comments` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forgiveness_2009`
--

DROP TABLE IF EXISTS `forgiveness_2009`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forgiveness_2009` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `ban_reason` varchar(255) NOT NULL DEFAULT '',
  `why` text NOT NULL,
  `other_accounts` text NOT NULL,
  `birthdate` date NOT NULL DEFAULT '0000-00-00',
  `valid_birthdate` tinyint(1) NOT NULL DEFAULT '0',
  `valid_email` tinyint(1) NOT NULL DEFAULT '0',
  `remote_addr` int(11) NOT NULL DEFAULT '0',
  `granted` tinyint(1) DEFAULT NULL,
  `comments` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forgiveness_2010`
--

DROP TABLE IF EXISTS `forgiveness_2010`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forgiveness_2010` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `ban_reason` varchar(255) NOT NULL DEFAULT '',
  `why` text NOT NULL,
  `other_accounts` text NOT NULL,
  `birthdate` date NOT NULL DEFAULT '0000-00-00',
  `valid_birthdate` tinyint(1) NOT NULL DEFAULT '0',
  `valid_email` tinyint(1) NOT NULL DEFAULT '0',
  `remote_addr` int(11) NOT NULL DEFAULT '0',
  `granted` tinyint(1) DEFAULT NULL,
  `comments` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `guests`
--

DROP TABLE IF EXISTS `guests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guests` (
  `gstIp` char(8) NOT NULL DEFAULT '',
  `gstIsBot` enum('0','1') NOT NULL DEFAULT '0',
  `gstLastVisit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `gstCount` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`gstIp`),
  KEY `gstLastVisit` (`gstLastVisit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Guest access info';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helpdesk`
--

DROP TABLE IF EXISTS `helpdesk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpdesk` (
  `hlpid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hlpUrgency` tinyint(3) unsigned NOT NULL DEFAULT '3' COMMENT 'Importance of the request (1 to 5) modifiable by the staff only',
  `hlpStatus` enum('wait-assign','in-progress','wait-submitter','wait-owner','completed') NOT NULL DEFAULT 'wait-assign' COMMENT 'Different states the request can be in',
  `hlpSummary` text NOT NULL COMMENT 'Short summary written by the submitter',
  `hlpCategory` smallint(5) NOT NULL DEFAULT '0' COMMENT 'helpdeskCats.hdcid',
  `hlpTaskGroup` smallint(5) NOT NULL DEFAULT '0' COMMENT 'helpdeskTaskGroups.htgid',
  `hlpSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hlpSubmitter` int(11) NOT NULL DEFAULT '0' COMMENT 'User who submitted the request (useid)',
  `hlpAssignedTo` int(11) NOT NULL DEFAULT '0' COMMENT 'Staff member this request is assigned to (useid) (0 - unassigned)',
  `hlpOwner` int(11) NOT NULL DEFAULT '0' COMMENT 'User that owns the object questioned by the request (useid)',
  `hlpReferenceType` enum('none','submission','extra','pm','comment','journal','poll','faq') NOT NULL DEFAULT 'none' COMMENT 'Type of an object within the gallery',
  `hlpReferenceId` int(11) NOT NULL DEFAULT '0' COMMENT 'ID of an object within the gallery',
  `hlpResolution` text NOT NULL COMMENT 'Resolution of the case',
  `hlpResolutionStatus` enum('undecided','delete','keep') NOT NULL DEFAULT 'undecided' COMMENT 'Resolution status of the case',
  PRIMARY KEY (`hlpid`),
  KEY `hlpSubmitDate` (`hlpSubmitDate`),
  KEY `hlpUrgency` (`hlpUrgency`),
  KEY `hlpCategory` (`hlpCategory`),
  KEY `hlpTaskGroup` (`hlpTaskGroup`),
  KEY `hlpSubmitter` (`hlpSubmitter`),
  KEY `hlpAssignedTo` (`hlpAssignedTo`),
  KEY `hlpOwner` (`hlpOwner`),
  KEY `hlpReference` (`hlpReferenceType`,`hlpReferenceId`),
  KEY `hlpResolutionStatus` (`hlpResolutionStatus`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Helpdesk data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helpdeskCats`
--

DROP TABLE IF EXISTS `helpdeskCats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpdeskCats` (
  `hdcid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `hdcName` varchar(128) NOT NULL DEFAULT '' COMMENT 'Category name',
  `hdcType` enum('general','explain','fix','improve','verify') NOT NULL DEFAULT 'general' COMMENT 'Category task type',
  PRIMARY KEY (`hdcid`),
  KEY `hdcType` (`hdcType`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Helpdesk request categories';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helpdeskDetails`
--

DROP TABLE IF EXISTS `helpdeskDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpdeskDetails` (
  `hddid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hddItem` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'helpdesk.hlpid',
  `hddSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'When this detail was added',
  `hddPrivacy` enum('private','submitter','owner','all') NOT NULL DEFAULT 'all' COMMENT 'Who (beside the staff) can view this detail',
  `hddMessage` text NOT NULL COMMENT 'Message in the detail',
  `hddAttachment` varchar(255) NOT NULL DEFAULT '' COMMENT 'File uploaded along with this detail',
  `hddAttachOrigName` varchar(255) NOT NULL DEFAULT '' COMMENT 'Original file name of the attachment',
  PRIMARY KEY (`hddid`),
  KEY `hddItem` (`hddItem`),
  KEY `hddSubmitDate` (`hddSubmitDate`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Helpdesk details';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helpdeskFAQ`
--

DROP TABLE IF EXISTS `helpdeskFAQ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpdeskFAQ` (
  `hfqid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hfqCategory` smallint(5) NOT NULL DEFAULT '0' COMMENT 'helpdeskFAQCats.hfcid',
  `hfqCreateDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Creation time',
  `hfqCreatedBy` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Created by',
  `hfqEditDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Last modification time',
  `hfqEditedBy` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Last modified by',
  `hfqIdent` varchar(32) NOT NULL DEFAULT '' COMMENT 'Human-useable article identifier (unique within the category)',
  `hfqTitle` varchar(255) NOT NULL DEFAULT '' COMMENT 'Article title, the question',
  `hfqText` text NOT NULL COMMENT 'Article text (accepts BBCode/emoticons), the answer',
  PRIMARY KEY (`hfqid`),
  KEY `hfqCategory` (`hfqCategory`),
  KEY `hfqIdent` (`hfqIdent`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Helpdesk FAQ articles';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helpdeskFAQCats`
--

DROP TABLE IF EXISTS `helpdeskFAQCats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpdeskFAQCats` (
  `hfcid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `hfcIdent` varchar(32) NOT NULL DEFAULT '' COMMENT 'Unique human-useable category identifier',
  `hfcName` varchar(128) NOT NULL DEFAULT '' COMMENT 'FAQ category name',
  `hfcIsGeneral` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'Display this category on the main FAQ page?',
  PRIMARY KEY (`hfcid`),
  KEY `hfcIdent` (`hfcIdent`),
  KEY `hfcIsGeneral` (`hfcIsGeneral`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Helpdesk FAQ categories';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helpdeskFAQTagMap`
--

DROP TABLE IF EXISTS `helpdeskFAQTagMap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpdeskFAQTagMap` (
  `hfmid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hfmArticle` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'helpdeskFAQ.hfqid',
  `hfmTag` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'helpdeskFAQTags.hftid',
  PRIMARY KEY (`hfmid`),
  KEY `hfmArticle` (`hfmArticle`),
  KEY `hfmTag` (`hfmTag`)
) ENGINE=InnoDB AUTO_INCREMENT=1070 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Helpdesk FAQ tag-to-article map';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helpdeskFAQTags`
--

DROP TABLE IF EXISTS `helpdeskFAQTags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpdeskFAQTags` (
  `hftid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hftName` varchar(32) NOT NULL DEFAULT '' COMMENT 'Tag name',
  `hftCount` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Total number of this tag''s references',
  PRIMARY KEY (`hftid`),
  KEY `hftName` (`hftName`)
) ENGINE=InnoDB AUTO_INCREMENT=472 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Helpdesk FAQ article tag list';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helpdeskTaskGroupMembers`
--

DROP TABLE IF EXISTS `helpdeskTaskGroupMembers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpdeskTaskGroupMembers` (
  `hgmGroup` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'helpdeskTaskGroups.htgid',
  `hgmUser` int(11) NOT NULL DEFAULT '0' COMMENT 'Staff member in the group (useid)',
  KEY `hgmGroup` (`hgmGroup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Helpdesk task group members';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helpdeskTaskGroups`
--

DROP TABLE IF EXISTS `helpdeskTaskGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpdeskTaskGroups` (
  `htgid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `htgName` varchar(60) NOT NULL DEFAULT '' COMMENT 'Name of the task group',
  PRIMARY KEY (`htgid`),
  KEY `htgName` (`htgName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Helpdesk task groups';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `includes`
--

DROP TABLE IF EXISTS `includes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `includes` (
  `incFilename` varchar(32) NOT NULL DEFAULT '',
  `incContent` text NOT NULL,
  PRIMARY KEY (`incFilename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `journals`
--

DROP TABLE IF EXISTS `journals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `journals` (
  `jouid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `jouCreator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid/cluid',
  `jouCreatorType` enum('use','clu') CHARACTER SET latin1 NOT NULL DEFAULT 'use',
  `jouAnnCreator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid (in case jouCreatorType = ''clu'') - announcement creator',
  `jouSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jouSubmitIp` varchar(8) NOT NULL DEFAULT '',
  `jouLastEdit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jouEditIp` varchar(8) NOT NULL DEFAULT '',
  `jouTitle` varchar(255) NOT NULL DEFAULT '',
  `jouEntry` text NOT NULL,
  `jouNoEmoticons` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disable emoticons in journal entry',
  `jouNoSig` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disable signature append to journal entry',
  `jouNoBBCode` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disable BBCode in journal entry',
  PRIMARY KEY (`jouid`),
  KEY `jouCreatorType` (`jouCreatorType`,`jouCreator`,`jouSubmitDate`)
) ENGINE=InnoDB AUTO_INCREMENT=308408 DEFAULT CHARSET=utf8 COMMENT='User journal data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `keywords`
--

DROP TABLE IF EXISTS `keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keywords` (
  `keyid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `keyWord` varchar(255) NOT NULL DEFAULT '',
  `keySubcat` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'keyid If this keyword is a subkeyword of another keyword, indicate here',
  `keyDesc` text NOT NULL,
  `keyFilter` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63') CHARACTER SET latin1 NOT NULL DEFAULT '',
  `keyCount` int(11) NOT NULL DEFAULT '-1' COMMENT 'Number of submissions using this keyword',
  PRIMARY KEY (`keyid`),
  KEY `keySubcat` (`keySubcat`),
  KEY `keyWord` (`keyWord`)
) ENGINE=InnoDB AUTO_INCREMENT=10848 DEFAULT CHARSET=utf8 COMMENT='Keywords for art relevancy searches';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `lanid` varchar(5) NOT NULL DEFAULT '' COMMENT 'at most, language_locale (ln_lc). Normally just language (ln)',
  `lanName` varchar(255) NOT NULL DEFAULT '' COMMENT 'human-readable language name in original character set',
  `lanEngName` varchar(255) NOT NULL DEFAULT '' COMMENT 'language name, in English',
  PRIMARY KEY (`lanid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Installed language packs';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `modlogs`
--

DROP TABLE IF EXISTS `modlogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modlogs` (
  `modid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `modSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modUser` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'User''s useid',
  `modModerator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Moderator''s useid',
  `modMessage` text NOT NULL COMMENT 'Moderator''s comment on this user',
  PRIMARY KEY (`modid`),
  KEY `modIndexes` (`modUser`,`modSubmitDate`)
) ENGINE=InnoDB AUTO_INCREMENT=80288 DEFAULT CHARSET=utf8 CHECKSUM=1 COMMENT='Moderator''s logs';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `newid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `newCreator` int(11) unsigned NOT NULL DEFAULT '0',
  `newSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `newSubject` varchar(255) NOT NULL DEFAULT '',
  `newComment` text NOT NULL,
  `newNoEmoticons` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `newNoSig` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `newNoBBCode` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `newLastEdit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `newTotalEdits` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`newid`),
  KEY `newSubmitDate` (`newSubmitDate`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=utf8 COMMENT='News data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `objExtData`
--

DROP TABLE IF EXISTS `objExtData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objExtData` (
  `objEid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'objid',
  `objServer` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'serid',
  `objImageWidth` smallint(5) unsigned NOT NULL DEFAULT '0',
  `objImageHeight` smallint(5) unsigned NOT NULL DEFAULT '0',
  `objImageSize` int(9) unsigned NOT NULL DEFAULT '0' COMMENT 'image file size',
  `objPreviewWidth` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'preview image resolution (0x0 = no preview)',
  `objPreviewHeight` smallint(5) unsigned NOT NULL DEFAULT '0',
  `objExtension` varchar(4) NOT NULL DEFAULT '',
  `objAniType` enum('no','pch','oeb') CHARACTER SET latin1 NOT NULL DEFAULT 'no',
  `objComment` text NOT NULL COMMENT 'Creator''s comment on the object',
  `objNumEdits` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of edits this object has had',
  `objNoEmoticons` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disable emoticons in time/tools/comments',
  `objNoSig` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disable signature append to comments',
  `objNoBBCode` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disable BBCode in time/tools/comments',
  `objNoAbuse` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disallow the object from being submitted for abuse again if it has been cleared',
  `objSubmitIp` varchar(8) NOT NULL DEFAULT '',
  `objEditIp` varchar(8) NOT NULL DEFAULT '',
  `objDeletedBy` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `objDeleteDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'when deleted?',
  `objCollabConfirmed` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Is collaboration confirmed by the other participant?',
  `objViewed` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of views the object has received',
  `objFavs` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of favourites the object has received (this should be faster than running COUNT(*) on the favourites table)',
  `objFilename` varchar(255) NOT NULL DEFAULT '' COMMENT 'Used for Content-Disposition. Also makes it easier to detect stolen art',
  PRIMARY KEY (`objEid`),
  KEY `objExtension` (`objExtension`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Extended artwork data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `objKeywords`
--

DROP TABLE IF EXISTS `objKeywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objKeywords` (
  `objKobject` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'objid',
  `objKkeyword` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT 'keyid',
  KEY `objKobject` (`objKobject`),
  KEY `objKkeyword` (`objKkeyword`,`objKobject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Keywords relevant to artwork';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `objects`
--

DROP TABLE IF EXISTS `objects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objects` (
  `objid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `objTitle` varchar(255) NOT NULL DEFAULT '',
  `objCreator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `objForClub` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'cluid',
  `objForClub2` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Secondary club (cluid)',
  `objForClub3` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Tertiary club (cluid)',
  `objForUser` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `objFolder` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'folid',
  `objCollab` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `objThumbWidth` smallint(5) unsigned NOT NULL DEFAULT '0',
  `objThumbHeight` smallint(5) unsigned NOT NULL DEFAULT '0',
  `objThumbDefault` enum('0','1') NOT NULL DEFAULT '0',
  `objSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `objLastEdit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `objPopularity` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'objFavs * objFavs * 1000 / objViewed',
  `objMature` set('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `objPending` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'This object is pending abuse investigation, see abuse table for more information',
  `objPendingUser` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'The object is pending input from the user during abuse moderation.',
  `objDeleted` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'This object has been deleted',
  `objGuestAccess` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '1' COMMENT 'This object can be viewed by non-registed users',
  PRIMARY KEY (`objid`),
  KEY `objForClub` (`objForClub`),
  KEY `objForUser` (`objForUser`),
  KEY `objPopularity` (`objDeleted`,`objPending`,`objPopularity`,`objGuestAccess`),
  KEY `objSubmitDate` (`objDeleted`,`objPending`,`objSubmitDate`,`objGuestAccess`),
  KEY `objForClub2` (`objForClub2`),
  KEY `objForClub3` (`objForClub3`),
  KEY `objMature` (`objDeleted`,`objPending`,`objSubmitDate`,`objMature`,`objGuestAccess`),
  KEY `objCollab` (`objCollab`,`objPending`,`objDeleted`,`objSubmitDate`),
  KEY `objCreator` (`objCreator`,`objPending`,`objDeleted`,`objSubmitDate`,`objPendingUser`)
) ENGINE=InnoDB AUTO_INCREMENT=1023032 DEFAULT CHARSET=utf8 COMMENT='Artwork data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pageCache`
--

DROP TABLE IF EXISTS `pageCache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pageCache` (
  `pagOwner` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Owner of the page (users.useid)',
  `pagType` varchar(40) NOT NULL DEFAULT '' COMMENT 'Type of the cached content',
  `pagOption` varchar(40) NOT NULL DEFAULT '' COMMENT 'Primary option (usually an id)',
  `pagOption2` varchar(40) NOT NULL DEFAULT '' COMMENT 'Secondary option (usually an offset)',
  `pagLanguage` varchar(5) NOT NULL DEFAULT '' COMMENT 'Language of the cached text (languages.lanid)',
  `pagContent` text NOT NULL COMMENT 'Cached text',
  KEY `pagOwner` (`pagOwner`),
  KEY `pagPage` (`pagType`,`pagLanguage`,`pagOption`,`pagOption2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Cached page contents';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pms`
--

DROP TABLE IF EXISTS `pms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pms` (
  `pmsid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pmsObj` int(11) unsigned NOT NULL DEFAULT '0',
  `pmsCreator` int(11) unsigned NOT NULL DEFAULT '0',
  `pmsPmUser` int(11) unsigned NOT NULL DEFAULT '0',
  `pmsTitle` varchar(255) NOT NULL DEFAULT '',
  `pmsComment` text NOT NULL,
  `pmsSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pmsSubmitIp` varchar(8) NOT NULL DEFAULT '',
  `pmsLastEdit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pmsEditIp` varchar(8) NOT NULL DEFAULT '',
  `pmsTotalEdits` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `pmsNoEmoticons` enum('0','1') NOT NULL DEFAULT '0',
  `pmsNoSig` enum('0','1') NOT NULL DEFAULT '0',
  `pmsNoBBCode` enum('0','1') NOT NULL DEFAULT '0',
  `pmsdelete` set('0','1','','') NOT NULL DEFAULT '0',
  `pmsReceiverDelete` enum('0','1') NOT NULL DEFAULT '0',
  `pmsCreatorDelete` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`pmsid`),
  KEY `pmsIndexes` (`pmsObj`,`pmsSubmitDate`),
  KEY `pmsCreator` (`pmsCreator`,`pmsSubmitDate`),
  KEY `pmsPmUser` (`pmsPmUser`,`pmsSubmitDate`,`pmsCreator`)
) ENGINE=InnoDB AUTO_INCREMENT=2795429 DEFAULT CHARSET=utf8 COMMENT='Private message data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pollOptions`
--

DROP TABLE IF EXISTS `pollOptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pollOptions` (
  `polOid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `polOPoll` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'polls.polid',
  `polOOption` varchar(255) NOT NULL DEFAULT '',
  `polOVotes` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`polOid`),
  KEY `polOPoll` (`polOPoll`)
) ENGINE=InnoDB AUTO_INCREMENT=211856 DEFAULT CHARSET=utf8 COMMENT='Poll options';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pollVotes`
--

DROP TABLE IF EXISTS `pollVotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pollVotes` (
  `pollVid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pollVUser` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `pollVSelected` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'polOid, selected option',
  PRIMARY KEY (`pollVid`),
  KEY `pollVSelected` (`pollVSelected`)
) ENGINE=InnoDB AUTO_INCREMENT=1319055 DEFAULT CHARSET=utf8 COMMENT='Poll results';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `polls`
--

DROP TABLE IF EXISTS `polls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `polls` (
  `polid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `polCreator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `polSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `polExpireDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `polSubject` varchar(255) NOT NULL DEFAULT '',
  `polComment` text NOT NULL,
  `polVoted` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`polid`),
  KEY `polCreator` (`polCreator`,`polSubmitDate`)
) ENGINE=InnoDB AUTO_INCREMENT=33342 DEFAULT CHARSET=utf8 COMMENT='Poll data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profiler`
--

DROP TABLE IF EXISTS `profiler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiler` (
  `prfPage` varchar(40) NOT NULL DEFAULT '' COMMENT 'requested page (1st level)',
  `prfCount` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'number of requests',
  `prfTime` decimal(12,3) unsigned NOT NULL DEFAULT '0.000' COMMENT 'total time in seconds',
  `prfWMCount` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'number of requests (watermark)',
  `prfWMTime` decimal(12,3) unsigned NOT NULL DEFAULT '0.000' COMMENT 'total time in seconds (watermark)',
  PRIMARY KEY (`prfPage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Profiler info';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `randomObjects`
--

DROP TABLE IF EXISTS `randomObjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `randomObjects` (
  `rndid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rndFilterPtn` set('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `rndObject` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'objid',
  `rndTimeout` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'when to consider this selection outdated (UNIX timestamp)',
  PRIMARY KEY (`rndid`),
  KEY `rndFilterPtn` (`rndFilterPtn`)
) ENGINE=InnoDB AUTO_INCREMENT=33430062 DEFAULT CHARSET=utf8 CHECKSUM=1 COMMENT='Randomly selected objects';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `search`
--

DROP TABLE IF EXISTS `search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search` (
  `srcid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `srcUser` int(11) unsigned NOT NULL DEFAULT '0',
  `srcSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `srcKeywordList` varchar(80) NOT NULL DEFAULT '',
  `srcSearchType` enum('0','1') NOT NULL DEFAULT '0',
  `srcSearchText` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`srcid`),
  KEY `srcUser` (`srcUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Searching';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `searchItems`
--

DROP TABLE IF EXISTS `searchItems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searchItems` (
  `sriSearch` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'search.srcid',
  `sriObject` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'objects.objid',
  KEY `sriSearch` (`sriSearch`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Searching (items)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `searchcache`
--

DROP TABLE IF EXISTS `searchcache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searchcache` (
  `seaObject` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'objid',
  `seaCreator` varchar(15) NOT NULL DEFAULT '',
  `seaText` text NOT NULL COMMENT 'All textual information associated with the object',
  `seaNeedsUpdate` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'Instead of recalculating the cache every time, just mark it as updated',
  PRIMARY KEY (`seaObject`),
  KEY `seaNeedsUpdate` (`seaNeedsUpdate`),
  FULLTEXT KEY `seaText` (`seaText`)
) ENGINE=MyISAM AUTO_INCREMENT=70441 DEFAULT CHARSET=utf8 COMMENT='Searching cache';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `servers`
--

DROP TABLE IF EXISTS `servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servers` (
  `serid` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `serAddr` varchar(255) NOT NULL DEFAULT '' COMMENT 'Remote server address; there MUST be at least one host or nothing will work (this should be a WORLD-ACCESSIBLE ADDRESS-- not for internal caching!!)',
  `serUsername` varchar(255) NOT NULL DEFAULT '',
  `serPassword` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`serid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Load-balancing';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `sesid` char(40) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `sesCreator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `sesPersistentLogin` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `sesStart` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sesLastUpdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sesIpAddress` char(8) CHARACTER SET latin1 NOT NULL DEFAULT '',
  PRIMARY KEY (`sesid`),
  KEY `sesCreator` (`sesCreator`),
  KEY `sesIpAddress` (`sesIpAddress`),
  KEY `sesLastUpdate` (`sesLastUpdate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='User tracking and authentication';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sessions2`
--

DROP TABLE IF EXISTS `sessions2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions2` (
  `sesid` char(40) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `sesCreator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `sesPersistentLogin` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `sesStart` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sesLastUpdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sesIpAddress` char(8) CHARACTER SET latin1 NOT NULL DEFAULT '',
  PRIMARY KEY (`sesid`),
  KEY `sesCreator` (`sesCreator`),
  KEY `sesIpAddress` (`sesIpAddress`),
  KEY `sesLastUpdate` (`sesLastUpdate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='User tracking and authentication';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `strings`
--

DROP TABLE IF EXISTS `strings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `strings` (
  `strid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `strLanguage` varchar(5) NOT NULL DEFAULT '',
  `strName` varchar(40) NOT NULL DEFAULT '',
  `strText` text NOT NULL,
  `strLastModified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `strCategory` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`strid`),
  KEY `strName` (`strName`)
) ENGINE=InnoDB AUTO_INCREMENT=7510 DEFAULT CHARSET=utf8 COMMENT='Language-dependent strings';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `themes`
--

DROP TABLE IF EXISTS `themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `themes` (
  `theid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `theName` varchar(32) NOT NULL DEFAULT '',
  `theLongName` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`theid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Themes support';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `timezones`
--

DROP TABLE IF EXISTS `timezones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timezones` (
  `timid` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `timOffset` mediumint(6) NOT NULL DEFAULT '0' COMMENT 'time, in seconds',
  `timName` varchar(40) NOT NULL DEFAULT '' COMMENT 'POSIX name',
  `timLongname` varchar(50) NOT NULL DEFAULT '' COMMENT '''Normal'' name',
  `timShortname` varchar(20) NOT NULL DEFAULT '' COMMENT 'NIST abbreviation',
  `timHasDST` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `timDSTLongname` varchar(50) NOT NULL DEFAULT '' COMMENT '''Normal'' name',
  `timDSTShortname` varchar(20) NOT NULL DEFAULT '' COMMENT 'NIST abbreviation',
  PRIMARY KEY (`timid`),
  KEY `timOffset` (`timOffset`)
) ENGINE=InnoDB AUTO_INCREMENT=554 DEFAULT CHARSET=utf8 COMMENT='Timezone data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `totalCounts`
--

DROP TABLE IF EXISTS `totalCounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `totalCounts` (
  `cntid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cntTable` varchar(32) NOT NULL DEFAULT '',
  `cntWhere` varchar(255) NOT NULL DEFAULT '',
  `cntDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cntCount` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`cntid`)
) ENGINE=InnoDB AUTO_INCREMENT=3010 DEFAULT CHARSET=utf8 PACK_KEYS=0 COMMENT='Navigation cache';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `translators`
--

DROP TABLE IF EXISTS `translators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translators` (
  `traid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `traUser` int(11) unsigned NOT NULL DEFAULT '0',
  `traLanguage` varchar(5) NOT NULL DEFAULT '',
  PRIMARY KEY (`traid`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='Official translators';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitList`
--

DROP TABLE IF EXISTS `twitList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twitList` (
  `twtid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `twtCreator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `twtBadUser` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  PRIMARY KEY (`twtid`),
  KEY `twtCreator` (`twtCreator`)
) ENGINE=InnoDB AUTO_INCREMENT=59874 DEFAULT CHARSET=utf8 CHECKSUM=1 COMMENT='Twit list';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `updates`
--

DROP TABLE IF EXISTS `updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `updates` (
  `updDate` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `updType` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `updCreator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'User that is being notified of an action',
  `updObj` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'varied depending on updType',
  `updUser` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'User that is causing the notification',
  KEY `updIndexes` (`updCreator`,`updType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Watch, fav, etc. update data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `updatesArt`
--

DROP TABLE IF EXISTS `updatesArt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `updatesArt` (
  `updDate` date NOT NULL DEFAULT '1970-01-01',
  `updCreator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'User that is being notified of an action',
  `updObj` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'objid',
  PRIMARY KEY (`updCreator`,`updObj`),
  KEY `updObj` (`updObj`),
  KEY `updDate` (`updDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `useClubs`
--

DROP TABLE IF EXISTS `useClubs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `useClubs` (
  `useCid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `useCclub` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'cluid',
  `useCmember` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `useCpending` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '1' COMMENT 'Pending acceptance into the club by the club owner',
  `useCModerator` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'Denotes user with extra club rights',
  PRIMARY KEY (`useCid`),
  KEY `useCmember` (`useCmember`,`useCpending`,`useCModerator`),
  KEY `useCclub` (`useCclub`,`useCmember`,`useCpending`,`useCModerator`)
) ENGINE=InnoDB AUTO_INCREMENT=461021 DEFAULT CHARSET=utf8 COMMENT='User club enrollment data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `useExtData`
--

DROP TABLE IF EXISTS `useExtData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `useExtData` (
  `useEid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid',
  `useFeaturedObj` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'objid',
  `useIdObj` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'objid',
  `useLastIp` varchar(8) NOT NULL DEFAULT '',
  `useFuzzyNumbers` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '1',
  `useUpdObj` smallint(5) unsigned NOT NULL DEFAULT '0',
  `useUpdExt` smallint(5) unsigned NOT NULL DEFAULT '0',
  `useUpdJou` smallint(5) unsigned NOT NULL DEFAULT '0',
  `useUpdCom` smallint(5) unsigned NOT NULL DEFAULT '0',
  `useUpdPms` smallint(5) unsigned NOT NULL DEFAULT '0',
  `useUpdWat` smallint(5) unsigned NOT NULL DEFAULT '0',
  `useUpdFav` smallint(5) unsigned NOT NULL DEFAULT '0',
  `useTheme` varchar(32) NOT NULL DEFAULT '1' COMMENT 'Default is specified in `config` table',
  `useFont` varchar(32) NOT NULL DEFAULT '' COMMENT 'Default is specified in `config` table',
  `useSortOrder` enum('0','1','2','3') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `useSortLimit` enum('0','1','2','3') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `useEmail` varchar(255) NOT NULL DEFAULT '',
  `useShowEmail` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '1',
  `useSignupDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `useLanguage` varchar(5) NOT NULL DEFAULT 'en',
  `useTimezone` mediumint(6) NOT NULL DEFAULT '260' COMMENT '260 is `GMT''',
  `useAIM` varchar(255) NOT NULL DEFAULT '',
  `useICQ` varchar(20) NOT NULL DEFAULT '',
  `useMSN` varchar(255) NOT NULL DEFAULT '',
  `useYIM` varchar(255) NOT NULL DEFAULT '',
  `useJabber` varchar(255) NOT NULL DEFAULT '',
  `useBirthday` date NOT NULL DEFAULT '1970-01-01',
  `useShowBirthday` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '1',
  `useCustomTitle` varchar(32) NOT NULL DEFAULT '',
  `useSignature` text NOT NULL,
  `useProfile` text NOT NULL,
  `useNoMature` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `useObjFilters` set('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59','60','61','62','63') CHARACTER SET latin1 NOT NULL DEFAULT '',
  `useNoEmoticons` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disable emoticons all posts by default',
  `useNoSig` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disable signature in all posts by default',
  `useNoBBCode` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Disable BBCode in all posts by default',
  `useActivationKey` varchar(40) NOT NULL DEFAULT '',
  `useIsActive` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `useIsRetired` enum('0','1') NOT NULL DEFAULT '0',
  `useIsHelpdesk` enum('0','1') NOT NULL,
  `useIsModerator` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `useIsSModerator` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `useIsDeveloper` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `useSuspendedUntil` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `useSuspendedReason` varchar(255) NOT NULL DEFAULT '',
  `useSidebar` text NOT NULL,
  `useSidebarThumbs` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '1' COMMENT 'Show panels containing thumbnails on the right side?',
  `useObjPreview` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '1' COMMENT 'Show submissions in the lower quality mode?',
  `useDisableCustom` enum('0','1','2','3') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Custom themes configuration',
  `useStatsPublic` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '1' COMMENT 'Make this user''s account statistics public',
  `useStatsHide` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Remove any stats info from this user''s view',
  `useNotifyWatch` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '1' COMMENT 'Notify this user about +watches',
  `useNotifyFavs` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '1' COMMENT 'Notify this user about +favs',
  `useObjCount` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of submissions posted by this user',
  `useHideExtras` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0' COMMENT 'Do not show anything submitted as Extras to this user',
  `useGuestAccess` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '1' COMMENT 'Allow guests (unregistered users) to access this user''s pages',
  `useLastUpdate` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Total number of updates for this user after last recount',
  `useInvitation` varchar(40) NOT NULL DEFAULT '' COMMENT 'Invitation code, see config[ invitations ]',
  `useInvitedBy` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'User whose invitation code was used to create this account',
  `useShowAvatars` enum('0','1') NOT NULL DEFAULT '1' COMMENT 'Show user avatars in comments?',
  `useShowFolders` enum('0','1') NOT NULL DEFAULT '1' COMMENT 'Show user-created folders in galleries?',
  `useShowSignatures` enum('0','1') NOT NULL DEFAULT '1' COMMENT 'Show user signatures in comments?',
  `useEnableUI2` enum('0','1','2') NOT NULL DEFAULT '1',
  `useWebpage` varchar(255) NOT NULL DEFAULT '' COMMENT 'User''s webpage link',
  `useBrowser` varchar(255) NOT NULL DEFAULT '',
  `useAcceptCom` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'User accepts commissions',
  `useAcceptReq` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'User accepts requests',
  `useAcceptTrade` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'User accepts art trades',
  PRIMARY KEY (`useEid`),
  KEY `useSignupDate` (`useSignupDate`),
  KEY `useGuestAccess` (`useGuestAccess`),
  KEY `useActivationKey` (`useActivationKey`),
  KEY `useLastIp` (`useLastIp`),
  KEY `useObjCount` (`useObjCount`),
  KEY `useAcceptCom` (`useAcceptCom`),
  KEY `useAcceptReq` (`useAcceptReq`),
  KEY `useAcceptTrade` (`useAcceptTrade`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Extended user data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `useid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `useUsername` varchar(32) NOT NULL DEFAULT '',
  `usePassword` varchar(40) NOT NULL DEFAULT '',
  `useRealName` varchar(255) NOT NULL DEFAULT '',
  `useDateFormat` varchar(32) NOT NULL DEFAULT '',
  `useShowRealName` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '1',
  `useLastAction` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `useLastSubmission` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `useIsHidden` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `useIsSuspended` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `useIsBanned` enum('0','1') CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `useAvatarWidth` smallint(5) unsigned NOT NULL DEFAULT '0',
  `useAvatarHeight` smallint(5) unsigned NOT NULL DEFAULT '0',
  `useAvatarDate` int(11) unsigned NOT NULL DEFAULT '0',
  `useAvatarExt` char(3) CHARACTER SET latin1 NOT NULL DEFAULT 'jpg',
  PRIMARY KEY (`useid`),
  KEY `useUsername` (`useUsername`),
  KEY `useLastAction` (`useLastAction`)
) ENGINE=InnoDB AUTO_INCREMENT=349906 DEFAULT CHARSET=utf8 COMMENT='User accounts data';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `watches`
--

DROP TABLE IF EXISTS `watches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `watches` (
  `watid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `watCreator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid/cluid that is being watched',
  `watType` enum('use','clu') CHARACTER SET latin1 NOT NULL DEFAULT 'use',
  `watUser` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'useid that is watching for updates',
  `watSubmitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`watid`),
  UNIQUE KEY `watCreator_2` (`watCreator`,`watType`,`watUser`),
  KEY `watCreator` (`watCreator`),
  KEY `watUser` (`watUser`)
) ENGINE=InnoDB AUTO_INCREMENT=4995012 DEFAULT CHARSET=utf8 COMMENT='Watched users data';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-04 15:29:17
