-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: gallery
-- ------------------------------------------------------
-- Server version	5.5.46-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `conName` varchar(255) NOT NULL DEFAULT '',
  `conValue` varchar(255) NOT NULL DEFAULT '',
  `conDesc` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`conName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='System-wide configuration';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES ('abuseGrace','172800','Length, in seconds, of the grace period for abuse cases pending user input.'),('adminUser','1','The admin user for the site.'),('allowedAniFormats','gif,mng,swf','Allowed animation formats in comma-delimited format.'),('allowedImageFormats','gif,jpg,png','Allowed image formats in comma-delimited format.'),('allowedTextFormats','txt,rtf,htm','Allowed text formats in comma-delimited format.'),('allowIEPNG','0','Enable proper PNG support for IE6.'),('balancingMode','1','See readme for information.'),('checkSubmitDate','0','Cross-reference the submission date of comments with the URI of the page access in order to prevent casual browsing of random comments.'),('dateFormat','Y-m-d @ g:i A','Default date format.'),('defaultFilters','','Filters defined after user registration.'),('defaultFont','original','The default font selection for new users.'),('defaultTheme','original-blue','The default theme for new users.'),('disableEmail','0','Disable the ability to send emails for new events (useful for high-load galleries).'),('disableIEWarning','0','Disable the warning for Internet Explorer users.'),('disableUnder18','1','Disable the ability for visitors under 18 to register.'),('enableFilters','4, 1, 6, 7, 5, 2, 3, 8','Enable specific content description filters, displayed in the order specified.'),('extrasLiftedRules','#1a-#1f, #2a, #2c, #9a-9c','Terms of Service rules that do not apply to Extras.'),('extrasPreviewRes','800x700','Maximum preview size, in pixels.'),('extrasPreviewSize','2097152','Maximum preview size, in bytes.'),('extrasRatio','1:1','How many extras can be submitted per one normal submission.'),('extrasThumbSize','1048576','Maximum thumbnail size, in bytes.'),('featureResolution','300x300','Maximum feature art size, in pixels.'),('filesHost','www.y-gallery.net','Rewrite galRoot with this hostname for file requests (leave blank if not needed).'),('filesPort','','Use this port to access the /files directory (leave blank if not needed).'),('filterKeywordAssoc','0=4701; 1=4703; 2=4705; 3=4704; 4=4702; 5=4707; 6=4706; 7=8810','Filter (fltid) to keyword (keyid) association.'),('fullviewRegOnly','0','Allow to fullview submissions for registered users only.'),('galName','y!Gallery','The name of this gallery.'),('galRoot','www.y-gallery.net','The root address of the site. Exclude http:// and www.'),('galSubname','Yaoi Gallery','A brief description of the gallery.'),('iconResolution','80x80','Folder icon size, in pixels.'),('idResolution','350x500','Maximum ID size, in pixels.'),('idSWFMaxSize','51200','Maximum Flash ID size, in bytes.'),('invitations','0','Enable invitations. New users must have an invitation from an existing user to be able to create an account.'),('keywordExplainURI','http://www.y-gallery.net/announcement/1/175333/','The location of the more verbose explanatory notice for the keyword system.'),('logDisabled','0','Disable error logging.'),('logReaders','4,16470','Users who should be receiving the error log messages to their emails.'),('matureFilters','1,2,3,4,5','Force these filters on under 18 years old users.'),('maxAvatarResolution','80x80','Maximum avatar size, in pixels.'),('maxAvatarSize','100000','Maximum allowed avatar size, in bytes.'),('maxIconSize','100000','Maximum folder icon size, in bytes.'),('maxIconsPerPost','30','Maximum number of icons (thumbnails, user icons, and club icons) allowed per post.'),('maxObjectEdits','10','The maximum number of times a user is allowed to edit their submission before it must be reset.'),('maxOpenAbuse','10','Maximum number of abuse reports one user (not artist) is allowed to have open at a time.'),('maxOpenAbuseGallery','3','Maximum number of reports open per user per gallery. IE user X can only report 3 of user Ys submissions'),('maxUsernameLength','18','Maximum username length.'),('minAvatarResolution','30x30','Minimum avatar size, in pixels.'),('minPasswordLength','6','Minimum password length.'),('minUsernameLength','3','Minimum username length.'),('mostRecentDays','3','Do not show submissions older than this number of days in the Most Recent panel.'),('newCommentDelay','10','The minimum amount of time (in seconds) between new comments or journal postings.'),('newObjectDelay','30','The minimum amount of time (in seconds) between new artwork submissions.'),('nonRegBrowseFilters','1,2,3,4,5','Filters enabled for guests at the Whats New and Browse pages.'),('nonRegFilters','1,2,3,4,5','Filters enabled for guests in personal galleries.'),('previewDefault','1','Switch on the preview mode (smaller files) by default?'),('previewMaxArea','409600','Preview image maximal area (width * height), in square pixels.'),('readOnly','0','Set entire system to read-only. Useful for site upgrades and emergency shutdown.'),('returnEmail','gallery@y-gallery.net','Email address that all outgoing mail is from.'),('returnPathEmail','postmaster@y-gallery.net','Email address that message should be returned to in case it\'s undeliverable.'),('sessionExpiry','900','Time until session expires, in seconds.'),('thumbResolution','150x150','Maximum thumbnail size, in pixels.');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-20 23:59:00
