function setActiveStyleSheet( title )
{
	var i, a, main, found = 0;

	for( i = 0; a = document.getElementsByTagName( "link" )[ i ]; i++ )
	{
		if( a.getAttribute( "rel" ).indexOf( "style" ) != -1 && a.getAttribute( "title" ))
		{
			if( a.getAttribute( "title" ) == title )
			{
				found = 1;
				break;
			}
		}
	}

	if( found )
	{
		for( i = 0; a = document.getElementsByTagName( "link" )[ i ]; i++ )
		{
			if( a.getAttribute( "rel" ).indexOf( "style" ) != -1 && a.getAttribute( "title" ))
			{
				a.disabled = true;

				if( a.getAttribute( "title" ) == title )
				{
					a.disabled = false;
				}
			}
		}
	}

	return( found );
};

function getActiveStyleSheet()
{
	var i, a;

	for( i = 0; a = document.getElementsByTagName( "link" )[ i ]; i++ )
	{
		if( a.getAttribute( "rel" ).indexOf( "style" ) != -1 &&
			a.getAttribute( "title" ) && !a.disabled )
		{
			return( a.getAttribute( "title" ));
		}
	}

	return( 0 );
};

function startSwitcher()
{
	if( !setActiveStyleSheet( "(custom)" ))
	{
		var cookie = readCookie( "style" );

		if( cookie )
		{
			setActiveStyleSheet( cookie );
		}
	}
};

function stopSwitcher()
{
	var title = getActiveStyleSheet();

	if( title && title != "(custom)" )
	{
		createCookie( "style", title, 365 * 9 );
	}
};

window.onload = function( e )
{
	startSwitcher();
};

window.onunload = function( e )
{
	stopSwitcher();
};

startSwitcher();
