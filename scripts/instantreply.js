var _BBCODE_B = 'b';
var _BBCODE_I = 'i';
var _BBCODE_U = 'u';
var _BBCODE_S = 's';
var _BBCODE_SUB = 'sub';
var _BBCODE_SUP = 'sup';
var _BBCODE_CENTER = 'center';
var _BBCODE_LEFT = 'left';
var _BBCODE_RIGHT = 'right';
var _BBCODE_JUSTIFY = 'justify';
var _BBCODE_URL = 'url';
var _BBCODE_ICON = 'icon';
var _BBCODE_USER = 'u';
var _BBCODE_CLUB = 'club';
var _BBCODE_CLUBLINK = 'c';
var _BBCODE_THUMB = 'thumb';
var _BBCODE_IMG = 'img';
var _BBCODE_HR = 'hr';
var _BBCODE_BR = 'br';

var _IR = { };

function showReplyForm( TargetURL, RefererURL )
{
	return( showReplyFormHeader( TargetURL, RefererURL ) +
		showReplyBox() +
		showReplyFormFooter() );
};

function showReplyFormHeader( TargetURL, RefererURL )
{
	return( '<form action="' + TargetURL + '" method="post">' +
		iefixStart() +
		'<input type="hidden" name="refererURL" value="' + RefererURL + '" />' );
};

function showReplyFormFooter()
{
	return( '<div class="sep">' +
		'<button type="submit" class="submit" name="sendReply" ' +
			( _IR.commentId == '' ? 'disabled="disabled" ' : '' ) +
			'onclick="el = get_by_id( \'' + _IR.commentId + '\' ); ' +
				'if( !el.value )' +
				'{ alert( \'' + _BLANK_COMMENT + '\'); return false } else return true;">' +
			getIMG( 'images/emoticons/checked.png' ) +
			' ' + _SEND_COMMENT +
		'</button>' +
		'</div>' +
		'<div class="clear"><br /></div>' +
		iefixEnd() +
		'</form>' );
};

function showReplyBox()
{
	var i;

	if( !isLoggedIn )
	{
		return( '<div>' +
			'<textarea class="' + ( isIE ? 'wide' : 'notsowide' ) + '" ' +
			'readonly="readonly" cols="78" rows="' + _IR.commentRows + '">' +
			_REQUIRE_LOGIN + '</textarea></div>' );
	}

	if( isReadOnly )
	{
		return( '<div>' +
			'<textarea class="' + ( isIE ? 'wide' : 'notsowide' ) + '" ' +
			'readonly="readonly" cols="78" rows="' + _IR.commentRows + '">' +
			_READONLY + '</textarea></div>' );
	}

	_IR.emoticonButtonId = generateId();
	_IR.emoticonHintId = generateId();
	_IR.noEmoticonsCheckboxId = generateId();
	_IR.noBBCodeCheckboxId = generateId();
	_IR.noSigCheckboxId = generateId();
	_IR.previewId = generateId();
	_IR.previewContainerId = generateId();
	_IR.previewButtonId = generateId();
	_IR.previewButtonTextId = generateId();

	var ht = '<div>' + // Text area
		'<textarea ' +
			'class="' + ( _IR.commentWide ? 'wide' : 'notsowide' ) + '" ' +
			'id="' + _IR.commentId + '" name="' + _IR.commentName + '" ' +
			'cols="78" rows="' + _IR.commentRows + '">' +
			_IR.commentDefault +
		'</textarea>' +
		'</div>' + // /Text area
		// Buttons
		'<div>' + iefixStart();

	var Buttons = new Array(
		{
			tag: _BBCODE_B,
			title: _BB_BOLD,
			icon: 'bold.png'
		},
		{
			tag: _BBCODE_I,
			title: _BB_ITALIC,
			icon: 'italic.png'
		},
		{
			tag: _BBCODE_U,
			title: _BB_UNDERLINE,
			icon: 'underline.png'
		},
		{
			tag: _BBCODE_S,
			title: _BB_STRIKED,
			icon: 'striked.png'
		},
		{
			tag: _BBCODE_SUP,
			title: _BB_SUPERSCRIPT,
			icon: 'superscript.png'
		},
		{
			tag: _BBCODE_SUB,
			title: _BB_SUBSCRIPT,
			icon: 'subscript.png'
		},
		{
			tag: _BBCODE_LEFT,
			title: _BB_ALIGN_LEFT,
			icon: 'a-left.png',
			separate: true
		},
		{
			tag: _BBCODE_CENTER,
			title: _BB_ALIGN_CENTER,
			icon: 'a-center.png'
		},
		{
			tag: _BBCODE_JUSTIFY,
			title: _BB_ALIGN_JUSTIFY,
			icon: 'a-justify.png'
		},
		{
			tag: _BBCODE_RIGHT,
			title: _BB_ALIGN_RIGHT,
			icon: 'a-right.png'
		},
		{
			tag: _BBCODE_URL,
			title: _BB_URL,
			icon: 'url.png',
			separate: true,
			query1: _ENTER_LINK_URL,
			query2: _ENTER_LINK_TEXT
		},
		{
			tag: _BBCODE_ICON,
			title: _BB_USERICON,
			icon: 'avatar_v2.png',
			query1: _ENTER_USERNAME,
			tagOptions: 'size=100%'
		},
		{
			tag: _BBCODE_CLUB,
			title: _BB_CLUBICON,
			icon: 'club3.png',
			query1: _ENTER_CLUBNAME,
			tagOptions: 'size=100%'
		},
		{
			tag: _BBCODE_THUMB,
			title: _BB_THUMB,
			icon: 'submission.png',
			query1: _ENTER_THUMBID,
			tagOptions: 'size=30%'
		},
		{
			title: _BB_EMOTICON,
			icon: 'smiley.png',
			insertOnClick: false,
			openEmoticons: true
		},
		{
			tag: _BBCODE_HR,
			title: 'Thematic Break',
			icon: 'page-break.png',
		},
		{
			tag: _BBCODE_BR,
			title: 'Line Break',
			icon: 'cutout-of-a-document.png',
		}
	);

	for(i = 0; i < Buttons.length; i++ )
	{
		var data = Buttons[ i ];

		if( typeof( data.insertOnClick ) == "undefined" )
		{
			data.insertOnClick = true;
		}

		if( typeof( data.openEmoticons ) == "undefined" )
		{
			data.openEmoticons = false;
		}

		if( typeof( data.query1 ) == "undefined" )
		{
			data.query1 = false;
		}

		if( typeof( data.query2 ) == "undefined" )
		{
			data.query2 = false;
		}

		if( typeof( data.separate ) == "undefined" )
		{
			data.separate = false;
		}

		if( typeof( data.tagOptions ) == "undefined" )
		{
			data.tagOptions = '';
		}
		else
		{
			data.tagOptions = ' ' + data.tagOptions;
		}

		ht += '<div style="' +
			( data.separate ? 'margin-left: 5px; ' : 'margin-left: 0px; ' ) +
			'white-space: nowrap; padding: 2px; border: none" class="button button_cmt"'; // Single button

		if( data.openEmoticons )
		{
			ht += ' onclick="toggleAnim(\'' + _IR.emoticonButtonId + '\', 20)"';
		}

		if( data.insertOnClick )
		{
			ht += ' onclick="current_textarea = get_by_id( \'' +
				_IR.commentId + '\' );';

			if( data.query1 )
			{
				var query1Str = new String( data.query1 );
				query1Str = query1Str.replace( /'/g, "\\'" );

				ht += 'var query1 = prompt( \'' + query1Str + '\', \'\' );' +
					'if( !query1 ) return;';
			}

			if( data.query2 )
			{
				var query2Str = new String( data.query2 );
				query2Str = query2Str.replace( /'/g, "\\'" );

				ht += 'var query2 = prompt( \'' + query2Str + '\', \'\' );' +
					'if( !query2 ) return;';
			}

			ht += 'wrap_textarea_selection( \'[' + data.tag;

			if( data.query1 && !data.query2 )
			{
				ht += '=\' + query1 + \'' + data.tagOptions + ']\', \'\'';
			}
			else
			{
				if( data.query1 && data.query2 )
				{
					ht += '=\' + query1 + \'' + data.tagOptions + ']\' + query2, ' +
						'\'[/' + data.tag + ']\'';
				}
				else
				{
					ht += data.tagOptions + ']\', \'[/' + data.tag + ']\'';
				}
			}

			ht += ' );"';
		}

		ht += '><acronym title="' + data.title + '">' +
			getIMG( 'images/emoticons/' + data.icon ) +
			'</acronym>' +
			'</div>'; // /Single button
	}

	// Plus/Minus
	ht += '<div class="button button_cmt" style="margin-left: 5px; padding: 2px; border: none" ' +
		'onclick="var el = get_by_id( \'' + _IR.commentId + '\' ); if( el && el.rows > 12 ) el.rows -= 10;">' +
		getIMG( 'images/emoticons/minus.png' ) +
		'</div>' +
		'<div class="button button_cmt" style="margin-left: 0px; padding: 2px; border: none" ' +
		'onclick="var el = get_by_id( \'' + _IR.commentId + '\' ); if( el && el.rows < 30 ) el.rows += 10;">' +
		getIMG( 'images/emoticons/plus.png' ) +
		'</div>';

	ht += '<div class="clear"><br /></div>' + iefixEnd() +
		'</div>' + // /Buttons
		// Emoticons box (hidden)
		'<div style="clear: both;">' +
		'<div id="' + _IR.emoticonButtonId + '" style="display: none; width: 100%;">' +
		'<div id="' + _IR.emoticonButtonId + '_">' +
			// Caption
			'<div class="sep caption">' + _EMOTICONS +
				'<span class="hint" onclick="toggle_visibility(\'' +
					_IR.emoticonHintId + '\')">' +
					'*' +
				'</span>:' +
			'</div>' + // /Caption
			// Hint (hidden)
			'<div style="display: none" id="' + _IR.emoticonHintId + '">' +
				'<div class="hint_body">' +
					'<div class="minibutton" onclick="toggle_visibility(\'' +
						_IR.emoticonHintId + '\')">' + _HIDE +
					'</div>' +
					_EMOTICON_EXPLAIN +
				'</div>' +
			'</div>' + // /Hint
			// Emoticon buttons
			'<div class="container2" style="padding: 3px">' +
				iefixStart();

	for(i = 0; i < Emoticons.length; i++ )
	{
		var emo = Emoticons[ i ];

		ht += 	// Single emoticon button
				'<div class="button a_center smalltext" style="border: none; margin: 0px; padding: 2px 3px;" ' +
					'onclick="current_textarea = get_by_id( \'' + _IR.commentId + '\' ); ' +
					'wrap_textarea_selection( \'' + emo.expr + '\', \'\' )">' +
					'<div style="height: 20px' + ( isIE ? 'width: 16px' : '' ) + '">' +
						getIMG( 'images/emoticons/' + emo.icon, 'alt="' + emo.expr +
							'" title="' + emo.expr + '"' ) +
					'</div>' +
				'</div>'; // / Single emoticon button
	}

	ht += 		// More >
				'<div class="sep f_right">' +
					'<a class="disable_wrapping smalltext" ' +
						'href="javascript:popup(\'' +
						_IR.emoticonPopupURL + '\')">' +
					_MORE + getIMG( 'images/emoticons/nav-next.png' ) +
					'</a>' +
				'</div>' + // /More >
				'<div class="clear"><br /></div>' +
				iefixEnd() +
			'</div>' + // /Emoticon buttons
		'</div></div></div>' + // /Emoticons box
		// Preview box (hidden)
		'<div id="' + _IR.previewId + '" style="display: none">' +
			'<div class="caption">' + _PREVIEW + ':</div>' +
			'<div class="container2 mar_bottom" ' +
				//'onclick="toggle_visibility( \'' + _IR.previewId + '\' ); return false;" ' +
				'id="' + _IR.previewContainerId + '">' +
				'<br />' +
			'</div>' +
		'</div>'; // /Preview box

	if( !_IR.commentNoOptions )
	{
		ht += // Options
			'<div class="smalltext">' +
			'<span class="nowrap">' +
				'<input ' + ( _IR.commentNoEmoticons ? ' checked="checked" ' : '' ) +
					'class="checkbox" id="' + _IR.noEmoticonsCheckboxId + '" ' +
					'name="' + _IR.commentName + 'NoEmoticons" type="checkbox" /> ' +
				'<label for="' + _IR.noEmoticonsCheckboxId + '">' + _NO_EMOTICONS + '</label> ' +
			'</span>' +
			'<span class="nowrap">' +
				'<input ' + ( _IR.commentNoSig ? ' checked="checked" ' : '' ) +
					'class="checkbox" id="' + _IR.noSigCheckboxId + '" ' +
					'name="' + _IR.commentName + 'NoSig" type="checkbox" /> ' +
				'<label for="' + _IR.noSigCheckboxId + '">' + _NO_SIG + '</label> ' +
			'</span>' +
			'<span class="nowrap">' +
				'<input ' + ( _IR.commentNoBBCode ? ' checked="checked" ' : '' ) +
					'class="checkbox" id="' + _IR.noBBCodeCheckboxId + '" ' +
					'name="' + _IR.commentName + 'NoBBCode" type="checkbox" /> ' +
				'<label for="' + _IR.noBBCodeCheckboxId + '">' + _NO_BBCODE + '</label>' +
			'</span>' +
			'</div>'; // /Options
	}

	ht += // Preview button
		'<div class="f_left mar_right sep" style="margin-left: 4px">' +
			'<button id="' + _IR.previewButtonId + '" ' +
				'type="button" class="submit" ' +
				'onclick="preview_comment( \'' +
					_IR.commentId + '\', \'' +
					_IR.previewURL + '\', \'' +
					_IR.previewContainerId + '\', \'' +
					_IR.previewId + '\', \'' +
					_IR.previewButtonId + '\', \'' +
					_IR.previewButtonTextId + '\', \'' +
					_IR.noEmoticonsCheckboxId + '\', \'' +
					_IR.noBBCodeCheckboxId + '\' );">' +
				getIMG( 'images/emoticons/comment.png' ) +
				' <span id="' + _IR.previewButtonTextId + '">' +
					_PREVIEW +
				'</span>' +
			'</button>' +
		'</div>'; // /Preview button

	return( ht );
};
