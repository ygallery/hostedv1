var ygAssetRequest = Class.create();

ygAssetRequest.prototype =
{
	initialize: function()
	{
		this.AssetRenderers = {
			"comments": assetRenderer_comments,
			"updates": assetRenderer_updates };

		this.AssetRequests = [];
	},

	activate: function()
	{
		/*
		if( isLoggedIn )
		{
			// Request the "updates" asset every once in a while.

			this.UpdatesRequestTimeout = 2 * 60 * 1000;

			window.setTimeout( this.requestUpdates.bind( this ),
				this.UpdatesRequestTimeout );
		}
		*/
	},

	requestUpdates: function()
	{
		this.requestAsset( $( "updates" ), { _kind: "updates" });

		// Double the timeout before the next request for the "updates" asset.

		if( this.UpdatesRequestTimeout < 2 * 60 * 1000 )
		{
			this.UpdatesRequestTimeout = 2 * 60 * 1000; // :3
		}

		this.UpdatesRequestTimeout *= 2;

		window.setTimeout( this.requestUpdates.bind( this ),
			this.UpdatesRequestTimeout );
	},

	/**
	 * Gathers asset requests for 0.3 sec, then makes a single HTTP request
	 * for all the gathered asset requests at once.
	 *
	 * @param TargetNode A DOM node which should be replaced by the requested
	 * asset. It can either be a DOM node reference or a text string with the
	 * id of the DOM node.
	 * @param RequestData An object containing asset request data, e.g.
	 * { _kind: "comment", id: "123" }. Note that the "_kind" variable is
	 * always required, it specifies the name of the asset handler (e.g.
	 * "updates", "avatar", "comment", etc).
	 */

	requestAsset: function( TargetNode, RequestData, RunInstant )
	{
		var i = this.AssetRequests.length;

		this.AssetRequests[ i ] = {};
		this.AssetRequests[ i ].RequestData = RequestData;
		this.AssetRequests[ i ].TargetNode = TargetNode;

		if( RunInstant )
		{
			this.runAssetQueue();
		}
		else
		{
			if( i == 0 )
			{
				window.setTimeout( this.runAssetQueue.bind( this ), 300 );
			}
		}
	},

	/**
	 * Performs all pending asset requests as a single HTTP request.
	 */

	runAssetQueue: function()
	{
		if( this.AssetRequests.length == 0 )
		{
			return;
		}

		var Requests = $A( this.AssetRequests ); // Make a local copy.
    
		this.AssetRequests = []; // Empty global queue.
    
		var UserData = [];
		var PostVars = [];
    
		PostVars[ "count" ] = Requests.length; // Number of asset requests.

		for( var i = 0; i < Requests.length; i++ )
		{
			// Make a linear array of POST vars to be sent to the HTTP request
			// function.
    
			for( var Key in Requests[ i ].RequestData )
			{
				// Asset request variables, e.g. "5_id" => "123" means that the
				// "id" variable of the 5th request should have the value of "123".

				var Value = Requests[ i ].RequestData[ Key ];

				if( typeof( Value ) == "boolean" )
				{
					Value = Value == true ? 1 : 0;
				}

				PostVars[ i + "_" + Key ] = Value;
			}
    
			UserData[ i ] = new Object;
			UserData[ i ].TargetNode = Requests[ i ].TargetNode;
			UserData[ i ].RequestData = Requests[ i ].RequestData;
		}
    
		var PostVarsStr = "";
	    
		if( PostVars )
		{
			var First = true;
	    
			for( var Key in PostVars )
			{
				PostVarsStr += ( First ? "" : "&" ) +
					encodeURIComponent( Key ) + "=" +
					encodeURIComponent( PostVars[ Key ]);
	    
				First = false;
			}
		}

		var myAjax = new Ajax.Request(
			"/asset",
			{
				method: "post",
				parameters: PostVarsStr,
				onComplete: this.callback.bind( this, UserData )
			});
	},

	/**
	 * Processes the result of HTTP request.
	 */

	callback: function( UserData, OriginalRequest )
	{
		var Objs = eval( OriginalRequest.responseText );

		for( var i = 0; i < Objs.length; i++ )
		{
			var ResultData = Objs[ i ];
			var TargetNode = UserData[ ResultData._id ].TargetNode;
			var RequestData = UserData[ ResultData._id ].RequestData;

			// confirm( ResultData.gen_time );

			this.renderAsset( TargetNode, RequestData, ResultData );
		}
	},

	/**
	 * Renders an asset. This function will call another function of this
	 * instance, the name depends on the kind of asset, e.g. for the "updates"
	 * asset it will call this.renderAsset_updates(...). The called function
	 * should use DOM to replace TargetNode with the newly asset rendered
	 * using the RequestData nad ResultData objects.
	 */

	renderAsset: function( TargetNode, RequestData, ResultData )
	{
		if( !ResultData._kind )
		{
			return;
		}

		var Node = ( typeof( TargetNode ) != "string" ) ?
			TargetNode : $( TargetNode );

		this.AssetRenderers[ ResultData._kind ]( Node, RequestData,
			ResultData );
	}
};
