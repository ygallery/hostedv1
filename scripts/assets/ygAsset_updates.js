function assetRenderer_updates( TargetNode, RequestData, ResultData )
{
	var showUpdate = function( Count, Hint, Icon )
		{
			if( Count > 0 )
			{
				return( getIMG( "images/emoticons/" + Icon,
					'alt="" title="' + Hint + '"' ) +
					( isFuzzyNumbers ? " " : "" ) +
					( Hint == _COMMENTS ? '<span id="_globCmtCnt">' : "" ) +
					( Hint == _JOURNALS ? '<span id="_globJouCnt">' : "" ) +
					fuzzy_number( Count ) +
					( Hint == _COMMENTS || Hint == _JOURNALS ? '</span>' : "" ) +
					( isFuzzyNumbers ? "<br />" : " " ));
			}
			else
			{
				return( "" );
			}
		};

	var Updates = 
		showUpdate( ResultData.numMessages, _MESSAGES, "watch.png" ) +
		showUpdate( ResultData.numFavourites, _FAVOURITES, "fav1.png" ) +
		showUpdate( ResultData.numComments, _COMMENTS, "comment.png" ) +
		showUpdate( ResultData.numJournals, _JOURNALS, "journal.png" ) +
		showUpdate( ResultData.numSubmissions, _SUBMISSIONS, "submission.png" );

	if( isExtras )
	{
		Updates += showUpdate( ResultData.numExtras, _SUBMIT_TYPE_EXTRA, "star4.png" );
	}

	if( Updates != "" )
	{
		$( "updates" ).innerHTML =
			'<a href="/updates">' + Updates + '</a>';
	}
};
