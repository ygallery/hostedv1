function refreshVisuals()
{
	document.getElementsByClassName( "tabs" ).each( function( obj )
	{
		var Tabs = obj.getElementsByTagName( "li" );

		for( var i = 0; i < Tabs.length; i++ )
		{
			var Check = Tabs[ i ].getElementsByTagName( "span" );

			if( Check.length > 0 )
			{
				continue;
			}

			var Anchor = Tabs[ i ].getElementsByTagName( "a" )[ 0 ];

			new Insertion.Before( Anchor,
				'<span class="ui2-tab-bg ui2-tab-bg1"></span>' +
				'<span class="ui2-tab-bg ui2-tab-bg2"></span>' +
				'<span class="ui2-tab-bg ui2-tab-bg3"></span>' +
				'<span class="ui2-tab-bg ui2-tab-bg4"></span>' +
				'<span class="ui2-tab-left"></span>' +
				'<span class="ui2-tab-center"></span>' +
				'<span class="ui2-tab-right"></span>' +
				'<span class="ui2-tab-content">' + Anchor.innerHTML + '</span>' );
		}
	});
}
