var isInHeader = true;
var i, j, desc, data;
// render items one after another, depending on their appearances
for(i = 0; i < KeywordData.length; i++) {
	data = KeywordData[i];
	// for better compression, items without parameters are stored as simple
	// numbers and not objects
	if(typeof(data) == "number")
		data = {a:data};
	// use KEYWORD by default if appearance is not defined
	if(typeof(data.a) == "undefined")
		data.a = 3;
	// for better compression, if data.f is not defined then previous value
	// of fullname will be used
	if(typeof(data.f) != "undefined" && typeof(data.c) != "undefined") {
		var f = data.f.split(','), first = 1, fullname = '';
		for(j = 0; j < f.length; j++) {
			fullname += (first ? '&bull; ' : ' &ndash; ') + Captions[f[j]];
			first = 0;
		}
		fullname += ' &ndash; ';
	}
	if(typeof(data.c) != "undefined") {
		var fullcaption = fullname + Captions[data.c];
		// since fullcaption is used within '' brackets,
		// escape the ' characters inside it
		fullcaption = fullcaption.replace(/\'/g, "\\'");
	}
	var kclass = data.k ? ' keyword_space' : '';
	// render the item depending on its appearance
	switch(data.a) {
		case -2: // OPEN
			document.write('<div style="display: none" id="keywordTab_' + data.i + '">');
			if(isIE) document.write( iefixStart() );
			break;

		case -1: // CLOSE
			document.write('<div class="clear"><br /></div>');
			if(isIE) document.write( iefixEnd() );
			document.write('</div>');
			break;

		case 0: // CLEAR
			document.write('<div class="clear"><br /></div>');
			break;

		case 1: // SWITCH
			document.write('</div><div class="container">');
			isInHeader = false;
			break;

		case 2: // TAB
			desc = new String( Descriptions[ data.c ] != '' ? Descriptions[ data.c ] : '' );
			desc = desc.replace( /'/g, "\\'" );

			document.write(
				( isInHeader ? '' : '<div class="f_left' + kclass + '">' ) +
				'<div class="tab normaltext" id="kwcache_tab' + data.i + '" ' +
				'onclick="open_tab(this,\'keywords_' + data.g + '\',' +
				'\'keywordTab_' + data.t + '\')">' +
				'<span class="keywordlike_place">' +
				( desc != '' ?
					'<span onmouseover="showMainDescFloater(this,\'' + desc + '\');" ' +
					'onmouseout="hideMainDescFloater();">' : '' ) +
				Captions[data.c] +
				( desc != '' ? '</span>' : '' ) +
				'</span>' +
				'</div>' +
				( isInHeader ? '' : '</div>' ));
			break;

		case 3: // KEYWORD
			desc = new String( Descriptions[ data.c ] != '' ? Descriptions[ data.c ] : '' );
			desc = desc.replace( /'/g, "\\'" );

			document.write('<div class="f_left' + kclass + '"><div class="tablike_place">' +
				'<span class="keyword" id="keywordSpan' + data.i + '" ' +
				'onclick="switch_keyword(this,\'chosenKeywordsList\',' +
				'\'' + data.i + '\',\'' + fullcaption + '\',\'keywordList\')">' +
				( desc != '' ?
					'<span onmouseover="showMainDescFloater(this,\'' + desc + '\');" ' +
					'onmouseout="hideMainDescFloater();">' : '' ) +
				Captions[data.c] +
				( desc != '' ? '</span>' : '' ) +
				'</span>' +
				'</div></div>');
			break;

		case 4: // TAB+KEYWORD
			document.write('<div class="f_left' + kclass + '">' +
				'<div class="tab normaltext" id="kwcache_tab' + data.i + '" ' +
				'onclick="open_tab(this,\'keywords_' + data.g + '\',' +
				'\'keywordTab_' + data.t + '\')">' +
				'<span class="keyword" id="keywordSpan' + data.t + '" ' +
				'onclick="tabkwd_click(this,\'kwcache_tab' + data.i + '\',' +
				'\'chosenKeywordsList\',\'' + data.t + '\',\'' + fullcaption + '\')">' +
				Captions[data.c] + '</span></div></div>');
			break;

		case 5: // HEADER
			if(isIE) document.write( iefixStart() );
			document.write('<div class="header">' + Captions[data.c] + '</div>');
			if(isIE) document.write( iefixEnd() );
			break;

		case 6: // HLINE
			if(isIE) document.write( iefixStart() );
			document.write('<div class="hline">&nbsp;</div>');
			if(isIE) document.write( iefixEnd() );
			break;
	}
}
