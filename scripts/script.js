var current_textarea = 0;
var tab_groups = new Array();
var tab_bars = new Array();
var chosen_keywords = new Array();
var current_floater = 0;
var current_desc_obj = 0;
var xmlhttp = false;
var onDescToElement, move_to_element;
var last_focused = 0;

// Pops up a window.
function popup(url, name, width, height) {
	width = width ? width : 550;
	height = height ? height : 600;
	window.open(url, name, 'top=0,left=0,location=0,scrollbars=1,toolbar=0,menubar=0,statusbar=0,resizable=1,width=' + width + ',height=' + height);
};

// Returns the element by its id.
function get_by_id(id) {
	return document.getElementById ? document.getElementById(id) :
		document.all ? document.all[id] : 0;
};

// Returns the element from the opener's window by the element's id.
function get_by_id_from_opener(id) {
	return opener.document.getElementById ? opener.document.getElementById(id) :
		opener.document.all ? opener.document.all[id] : 0;
};

// Searches for class 'find_class' and returns 'false' if it is not found.
function find_className(el, find_class) {
	var i;
	var classes = el.className.split(' ');
	for(i = 0; i < classes.length; i++)
		if(classes[i] == find_class) return true;
	return false;
};

// Adds class 'add_class' to the class list of element 'el'.
function add_className(el, add_class) {
	var i;
	var classes = el.className.split(' ');
	for (i = 0; i < classes.length; i++)
		if(classes[i] == add_class) return;
	classes.push(add_class);
	el.className = classes.join(' ');
};

// Removes class 'remove_class' from the class list of element 'el'.
function remove_className(el, remove_class) {
	var classes = el.className.split(' ');
	var new_classes = new Array ();
	var found = false;
	var i;
	for(i = 0; i < classes.length; i++)
		if(classes[i] != remove_class) new_classes.push(classes[i]);
		else found = true;
	if(!found) return;
	el.className = new_classes.join(' ');
};

// Sets the clicked keyword ON/OFF.
function switch_keyword(el, listDivId, keywordId, keywordName, keywordList) {
	if(find_className(el, 'keyword_enabled')) {
		remove_className(el, 'keyword_enabled');
		chosen_keywords[keywordId] = false;
	}
	else {
		add_className(el, 'keyword_enabled');
		chosen_keywords[keywordId] = keywordName;
	}
	var listDiv = get_by_id(listDivId);
	if(!listDiv) return;
	var keywordListInput = get_by_id(keywordList);
	if(!keywordListInput) return;
	var listDivContent = new Array();
	var keywordListInputString = "";
	var first = true;
	for(i = 0; i < chosen_keywords.length; i++) {
		if(chosen_keywords[i]) {
			listDivContent.push(chosen_keywords[i]);
			keywordListInputString += (first ? "" : " ") + i;
			first = false;
		}
	}
	listDivContent.sort();
	listDiv.innerHTML = listDivContent.length ? listDivContent.join("<br />") : "none";
	keywordListInput.value = keywordListInputString;
};

// Makes the div with the specified 'id' visible.
function make_visible(id) {
	var el = get_by_id(id);
	if(!el) return 0;
	el.style.display = 'block';
	return id;
};

// Makes the div with the specified 'id' invisible.
function make_invisible(id) {
	var el = get_by_id(id);
	if(!el) return 0;
	el.style.display = 'none';
	return id;
};

// Toggles visibility of any div with the specified 'id'.
function toggle_visibility(id) {
	var el = get_by_id(id);
	if(!el) return 0;
	if(el.style.display == 'none')
		el.style.display = 'block';
	else
		el.style.display = 'none';
	return id;
};

// Sets the focus to specified element.
function set_focus(id) {
	var el = get_by_id(id);
	if(el) { el.focus(); last_focused=id; }
};

// Sets the focus to specified element.
function reset_focus() {
	var el = get_by_id(last_focused);
	if(el) el.blur();
};

// Shows div 'target_id' while hiding all the other divs from the same 'group'.
function open_tab(tab, group, target_id) {
	if(!tab) return;
	if(tab_bars[group])
		remove_className(tab_bars[group], 'tab_active');
	tab_bars[group] = tab;
	add_className(tab, 'tab_active');
	if(tab_groups[group])
		make_invisible(tab_groups[group]);
	tab_groups[group] = target_id;
	make_visible(target_id);
};

// Displays the preview image.
// TODO: move all text to the language file.
function show_preview_image(id, message_id, filename) {
	var s;
	var el = get_by_id(id);
	if(!el) return;
	var el_msg = get_by_id(message_id);
	if(!el_msg) return;
	el.src = 'file:///' + filename;
	make_visible(id);
	el_msg.innerHTML = '';
	make_invisible(message_id);
	filename = filename.toLowerCase();
	var url = '<a target="_blank" href="file:///' + filename + '">';
	if(filename.match(/swf$/)) {
		make_invisible(id);
		make_visible(message_id);
		s = _PREV_FLASH_MOVIE;
		s = s.replace( /\[URL\]/g, url );
		s = s.replace( /\[\/URL\]/g, '</a>' );
		el_msg.innerHTML = s;
		return;
	}
	if(filename.match(/txt$/) ||
		filename.match(/html$/) ||
		filename.match(/htm$/)) {
		make_invisible(id);
		make_visible(message_id);
		s = _PREV_TEXT_FILE;
		s = s.replace( /\[URL\]/g, url );
		s = s.replace( /\[\/URL\]/g, '</a>' );
		el_msg.innerHTML = s;
		return;
	}
	if((!filename.match(/jpg$/)) &&
		(!filename.match(/jpeg$/)) &&
		(!filename.match(/png$/)) &&
		(!filename.match(/gif$/))) {
		make_invisible(id);
		make_visible(message_id);
		el_msg.innerHTML = '<span class="error">' + _INVALID_FILE_TYPE + '</span>';
	}
};

// Wraps selected text in the current textarea with 'open_tag' and 'close_tag'.
function wrap_textarea_selection(open_tag, close_tag) {
	if(!current_textarea) return;
	var inserted = 0;
	if(document.selection) { /* IE */
		var sel = document.selection.createRange();
		if(sel.text.length > 0) {
			sel.text = open_tag + sel.text + close_tag;
			document.selection.empty();
			inserted = 1;
		}
	}
	else {
		if(typeof(current_textarea.selectionStart) != 'undefined') { /* Moz */
			var sel_start = current_textarea.selectionStart;
			var sel_end = current_textarea.selectionEnd;
			var sel_length = current_textarea.textLength;
			current_textarea.value =
				current_textarea.value.substring(0, sel_start) + open_tag +
				current_textarea.value.substring(sel_start, sel_end) + close_tag +
				current_textarea.value.substring(sel_end, sel_length);
			current_textarea.selectionStart = sel_start + open_tag.length;
			current_textarea.selectionEnd = sel_start + open_tag.length;
			inserted = 1;
		}
	}
	if(!inserted) current_textarea.value += open_tag + close_tag;
	current_textarea.focus();
};

function tabkwd_click(kwd, tabid, listid, keyid, keyName) {
	if(!predefine_keywords) {
		var el = get_by_id(tabid);
		if(!el) return;
		if(!find_className(el,'tab_active')) return;
	}
	switch_keyword(kwd, listid, keyid, keyName, 'keywordList');
};

move_to_element = function (e) {
	var mouse_x, mouse_y, width, height;
	if(!current_floater) return;
	if(isIE) {
		mouse_x = (document.documentElement && document.documentElement.scrollLeft) ?
			event.clientX + document.documentElement.scrollLeft :
			event.clientX + document.body.scrollLeft;
		mouse_y = (document.documentElement && document.documentElement.scrollTop ) ?
			event.clientY + document.documentElement.scrollTop :
			event.clientY + document.body.scrollTop;
	}
	else {
		mouse_x = e.pageX;
		mouse_y = e.pageY;
	}
	width = parseInt(current_floater.style.width);
	height = parseInt(current_floater.style.height);
	var left = mouse_x - width / 2 - 8;
	left = left < 10 ? 10 : left;
	current_floater.style.left = left + 'px';
	current_floater.style.top = (mouse_y - height - 16) + 'px';
	current_floater.style.display = 'block';
};

function createCookie( name, value, days )
{
	if( days )
	{
		var date = new Date();

		date.setTime( date.getTime() + ( days * 24 * 60 * 60 * 1000 ));

		var expires = "; expires=" + date.toGMTString();
	}
	else
	{
		expires = "";
	}

	document.cookie = name + "=" + value + expires +
		"; path=/; domain=" + domain;
};

function readCookie( name )
{
	var nameEQ = name + "=";
	var ca = document.cookie.split( ';' );

	for( var i = 0; i < ca.length; i++ )
	{
		var c = ca[ i ];

		while( c.charAt( 0 ) == ' ' )
		{
			c = c.substring( 1,c.length );
		}

		if( c.indexOf( nameEQ ) == 0 )
		{
			return( c.substring( nameEQ.length, c.length ));
		}
	}

	return( 0 );
};

function xmlhttp_reconnect()
{
	if( xmlhttp )
	{
		delete xmlhttp;
	}

	xmlhttp = false;

	if( window.XMLHttpRequest )
	{
		xmlhttp = new XMLHttpRequest();
	}
	else
		if( window.ActiveXObject )
		{
			xmlhttp = new ActiveXObject( "msxml2.XMLHTTP" );
		}
};

function preview_comment(id, baseurl, preview_target, preview_outer,
	preview_button, preview_button_text, id_no_emoticons, id_no_bbcode)
{
	var el1;

	var el = get_by_id(id);
	if(!el) return;
	var text = el.value;

	var no_emoticons = 0;
	el1 = get_by_id(id_no_emoticons);
	if(el1) no_emoticons = el1.checked ? 1 : 0;

	var no_bbcode = 0;
	el1 = get_by_id(id_no_bbcode);
	if(el1) no_bbcode = el1.checked ? 1 : 0;

	xmlhttp_reconnect();

	if(xmlhttp)
	{
		xmlhttp_preview = get_by_id(preview_target);
		xmlhttp_preview_outer = get_by_id(preview_outer);
		xmlhttp_preview_button = get_by_id(preview_button);
		xmlhttp_preview_button_text = get_by_id(preview_button_text);

		xmlhttp_preview_button.disabled = true;
		xmlhttp_preview_button_text.innerHTML = _PLEASE_WAIT;

		xmlhttp.onreadystatechange = function()
		{
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				xmlhttp_preview.innerHTML = decodeURIComponent(xmlhttp.responseText);
				xmlhttp_preview_outer.style.display = 'block';
				xmlhttp_preview_button.disabled = false;
				xmlhttp_preview_button_text.innerHTML = _PREVIEW;
			}
		};

		xmlhttp.open('POST', baseurl, true);
		xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
		xmlhttp.send('text=' + encodeURIComponent(text) + '&noem=' + no_emoticons +
			'&nobb=' + no_bbcode);
		// in case server didn't respond:
		window.setTimeout(
			"xmlhttp_preview_button.disabled = false;" +
			"xmlhttp_preview_button_text.innerHTML = _PREVIEW;", 5000);
	}
	else
	{
		popup(baseurl + "&text=" + encodeURIComponent(text) + '&noem=' + no_emoticons +
			'&nobb=' + no_bbcode, 'preview', 500, 300);
	}
};

function xmlhttp_go( baseurl, respObject )
{
	xmlhttp_reconnect();

	if( xmlhttp )
	{
		xmlhttp.failureRedirect = baseurl + '&redirect=yes';
		xmlhttp.onSuccessCallback = respObject.callback;

		xmlhttp.onreadystatechange = function()
		{
			if( xmlhttp.readyState == 4 )
			{
				if( xmlhttp.status == 200 )
				{
					xmlhttp.onSuccessCallback();
				}
				else
				{
					document.location = xmlhttp.failureRedirect;
				}
			}
		}

		xmlhttp.open( 'POST', baseurl, true );

		xmlhttp.setRequestHeader( 'Content-Type',
			'application/x-www-form-urlencoded;charset=UTF-8' );

		xmlhttp.send( '' );
	}
	else
	{
		document.location = baseurl + '&redirect=yes';
	}
};

function add_operation( operation )
{
	if( isCookiesDisabled )
	{
		var resp = new Object();

		resp.callback = function()
		{
			// ...
		};

		xmlhttp_go( baseURL + "op/" + operation +
			"/?popup=yes&yGalSession=" + yGalSession +
			"&HTTP_REFERER=" + thisURL, resp );
	}
	else
	{
		// Opcodes: f = fav, fu = unfav, w = watch, wu = unwatch,
		// c = clear art update, m = mark as read.

		var ops = readCookie( "yGalOps" );

		if( !ops )
			ops = "";

		var regExp = new RegExp( operation, "g" );

		ops = ops.replace( regExp, "" ) + " " + operation;

		createCookie( "yGalOps", ops, 20 );
	}
};

function start_oekaki( oekaki_form, base_url )
{
	var o_form = get_by_id( oekaki_form );

	if( !o_form )
		return;

	var editor = 0;
	var i;

	for( i = 0; i < o_form.editor.length; i++ )
	{
		if( o_form.editor[ i ].checked )
			editor = o_form.editor[ i ].value;
	}

	if( !editor )
	{
		alert( _OEKAKI_NO_EDITOR );
		return;
	}

	var width = o_form.width.value;
	var height = o_form.height.value;
	var saveAnimation = o_form.saveAnimation.checked ? 1 : 0;
	var url = base_url + '&width=' + width + '&height=' + height +
		'&editor=' + editor + '&saveAnimation=' + saveAnimation;

	document.location = url;
};

function iefixStart()
{
	return( isIE ? '<table border="0" cellpadding="0" cellspacing="0" ' +
		'width="100%"><tr><td>' : '' );
};

function iefixEnd()
{
	return( isIE ? '</td></tr></table>' : '' );
};

function getIMG( src, attrs )
{
	if( typeof( attrs ) == "undefined" )
	{
		attrs = '';
	}
	else
	{
		attrs = ' ' + attrs;
	}

	if( allowIEPNG )
	{
		if( !isIE || !src.toLowerCase().match( /png$/ ))
		{
			return( '<img src="' + baseURLF + src + '"' + attrs + ' />' );
		}
		else
		{
			return( '<span class="pngIE" ' +
				'style="filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(' +
				'src=\'' + baseURL + src + '\', sizingMethod=\'none\')">' +
				'<img src="' + baseURL + src + '" style="filter: alpha(opacity=0);"' +
					attrs + ' />' +
				'</span>' );
		}
	}
	else
	{
		return( '<img src="' + baseURLF + src + '"' + attrs + ' />' );
	}
};

var lastGeneratedId = 0;

function generateId()
{
	lastGeneratedId++;
	return( "yg_js_id" + lastGeneratedId );
};

function fuzzy_number( number )
{
	if( !isFuzzyNumbers )
		return number;

	var result = "?";

	if(number >= 987) result = _FUZZY_987;
	else if(number >= 456) result = _FUZZY_456;
	else if(number >= 234) result = _FUZZY_234;
	else if(number >= 99) result = _FUZZY_99;
	else if(number >= 48) result = _FUZZY_48;
	else if(number >= 24) result = _FUZZY_24;
	else if(number >= 11) result = _FUZZY_11;
	else if(number >= 7) result = _FUZZY_7;
	else if(number >= 3) result = _FUZZY_3;
	else if(number >= 2) result = _FUZZY_2;
	else if(number >= 1) result = _FUZZY_1;
	else result = _FUZZY_0;

	return '<acronym title="' + number + '">' + result + '</acronym>';
};

onDescToElement = function (e) {
	var mouse_x, mouse_y;
	if(!current_floater) return;
	if(isIE) {
		mouse_x = (document.documentElement && document.documentElement.scrollLeft) ?
			event.clientX + document.documentElement.scrollLeft :
			event.clientX + document.body.scrollLeft;
		mouse_y = (document.documentElement && document.documentElement.scrollTop ) ?
			event.clientY + document.documentElement.scrollTop :
			event.clientY + document.body.scrollTop;
	}
	else {
		mouse_x = e.pageX;
		mouse_y = e.pageY;
	}
	current_floater.style.left = (mouse_x - 10) + 'px';
	current_floater.style.top = (mouse_y + 25) + 'px';
	current_floater.style.display = 'block';
};

function showMainDescFloater( obj, text )
{
	el = get_by_id( "mainDescFloaterContent" );
	el.innerHTML = text;

	current_floater = get_by_id( "mainDescFloater" );
	current_desc_obj = obj;
	current_desc_obj.onmousemove = onDescToElement;
};

function hideMainDescFloater()
{
	make_invisible( "mainDescFloater" );

	current_floater = 0;
	current_desc_obj.onmousemove = 0;
};

var animInstance = 0;

function collapseAnim( id, speed, instance, action )
{
	var el = get_by_id( id );
	var el2 = get_by_id( id + "_" );

	if( !el || !el2 )
	{
		return;
	}

	if( 1 /*isIE*/ )
	{
		el.style.display = ( speed < 0 ) ? 'none' : 'block';
		eval( action );
		return;
	}

	var y;
	var height = el2.offsetHeight;

	if( height == 0 || instance == 0 )
	{
		el.style.display = 'block';
		el.style.overflow = 'hidden';
		el.style.minHeight = '1px';
		el.style.marginTop = '-1px';
	}

	if( height == 0 )
	{
		height = 1024;
		y = ( speed < 0 ) ? 0 : -height;
		el.animInstance = 0;
	}
	else
	{
		if( instance == 0 )
		{
			y = parseInt( el2.style.marginTop );

			if( isNaN( y ))
			{
				y = ( speed < 0 ) ? 0 : -height;
			}
			else
			{
				if( speed > 0 && y < -height )
				{
					y = -height;
				}
			}

			el.animInstance = ++animInstance;
		}
		else
		{
			y = parseInt( el2.style.marginTop ) + speed;

			if( el.animInstance != instance )
			{
				return;
			}
		}
	}

	if(( speed < 0 && y > -height ) || ( speed > 0 && y < 0 ))
	{
		el2.style.marginTop = y + 'px';

		setTimeout( "collapseAnim( '" + id + "', " + speed + ", " +
			el.animInstance + ", '" + action.replace( /\'/g, "\\'" ) + "' );", 10 );
	}
	else
	{
		el.style.display = ( speed < 0 ) ? 'none' : 'block';
		el.style.minHeight = '0px';
		el.style.marginTop = '0px';
		el2.style.marginTop = ( speed < 0 ) ? ( '-' + height + 'px' ) : '0px';

		eval( action );
	}
};

function toggleAnim( id, speed )
{
	var el = get_by_id( id );

	if( !el )
	{
		return;
	}

	if( typeof( el.animDirection ) == "undefined" )
	{
		el.animDirection = -1;
	}

	if( el.animDirection > 0 )
	{
		el.animDirection = -1;
		collapseAnim( id, -speed, 0, '' );
	}
	else
	{
		el.animDirection = 1;
		collapseAnim( id, speed, 0, '' );
	}
};

function fixRowCount( textarea, min, max )
{
	var R = textarea.value.length / 50 + 1;

	R = R < min ? min : R > max ? max : R;

	textarea.rows = R;
};
