function getAdColor( el, styleProp, defaultValue )
{
	if( isIE )
	{
		return( defaultValue );
	}

	var x = document.getElementById( el );

	if( x.currentStyle ) 
	{
		var y = x.currentStyle[ styleProp ];
	}
	else
	{
		if( window.getComputedStyle )
		{
			var y = document.defaultView.getComputedStyle( x, null ).getPropertyValue( styleProp );
		}
	}

	if( y.substr( 0, 1 ) == "#" )
	{
		return( y.substr( 1 ).toUpperCase() );
	}

	if( y.substr( 0, 3 ).toLowerCase() == "rgb" )
	{
		var rgb = y.substr( 4, y.length - 5 ).replace( /\s/g, "" ).split( "," );

		return( decToHex2( rgb[ 0 ]) + decToHex2( rgb[ 1 ]) + decToHex2( rgb[ 2 ]) );
	}
 
	return( y );
}

function decToHex2( value )
{
	var hex = "0123456789ABCDEF";

	return( hex.substr( value / 16, 1 ) + hex.substr( value % 16, 1 ));
}

var adColBorder = getAdColor( "idColorPicker", "border-left-color", "B4D0DC" );
var adColBG = getAdColor( "idColorPicker", "background-color", "ECF8FF" );
var adColLink = getAdColor( "idColorPicker2", "color", "0000CC" );
var adColURL = getAdColor( "idColorPicker", "color", "008000" );
var adColText = getAdColor( "idColorPicker", "color", "6F6F6F" );
