#!/usr/bin/php
<?
	$_argv = $_SERVER["argv"];
	$_argc = $_SERVER["argc"];

	if($_argc != 4) {
		echo("Usage: findabuser.php <logfile> <number of lines to read> <number of users to return>");
		exit;
	}

	$accesses = array();
	$accessList = array();
	exec("tail -$_argv[2] $_argv[1]",$lastLines);
	foreach($lastLines as $line) {
		$info = explode("- -",$line);

		if(array_key_exists($info[0],$accesses))
			$accesses[$info[0]]++;
		else
			$accesses[$info[0]] = 1;

		if(strpos($info[1],"search") !== FALSE)
			$searched[$info[0]] = $info[1];
	}
	foreach($accesses as $ip => $amt) {
		$accessList[$ip] = $amt . "," . $ip;
	}
	rsort($accessList,SORT_NUMERIC);

	for($i=0;$i<$argv[3];$i++) {
		list($amt,$ip) = explode(",",$accessList[$i]);
		echo $amt . " - " . $ip;
		if(array_key_exists($ip,$searched))
			echo " - " . $searched[$ip];
		echo "\n";
	}
?>