<? // sets the timezone defined by the user and provides time correction functions

$_timezone = array("timid" => 262 /*GMT*/, "timOffset" => 0, "timHasDST" => 0);
if($_auth["useid"]) {
	$result = sql_query("SELECT * FROM `timezones` WHERE `timid` = '".$_auth["useTimezone"]."' LIMIT 1");
	if(mysql_num_rows($result) > 0)
		$_timezone = mysql_fetch_assoc($result);
}

// Returns timestamp modified to the current timezone; the original timestamp must be in GMT.
function applyTimezone($timestamp = 0) {
	global $_timezone;
	if(!$timestamp) $timestamp = time();
	if(!isset($_timezone["timOffset"])) return $timestamp;
	$offset = $_timezone["timOffset"];
	if($_timezone["timHasDST"] && isTimestampInDST($timestamp)) $offset += 3600; // apply DST
	$timestamp += $offset;
	return $timestamp;
}

// Returns true if the specified timestamp is in the current timezone's DST.
function isTimestampInDST($timestamp) {
	global $_timezone;
	if( function_exists( "date_default_timezone_set" ))
	{
		$saveTZ = date_default_timezone_get();
		date_default_timezone_set( $_timezone[ "timName" ]);
		$time = localtime($timestamp, true);
		date_default_timezone_set( $saveTZ );
		return $time["tm_isdst"];
	}
	else
	{
		$saveTZ = getenv("TZ");
		if(!$saveTZ) $saveTZ = "";
		putenv("TZ=".$_timezone["timName"]);
		$time = localtime($timestamp, true);
		putenv("TZ=".$saveTZ);
		return $time["tm_isdst"];
	}
}

// Returns textual representation of the timezone offset, e.g. +09:00.
function getTimezoneOffset($timOffset) {
	$offsetHours = round($timOffset / 3600);
	$offsetMinutes = round($timOffset % 3600 / 60);
	if($offsetMinutes < 0) {
		$offsetHours++;
		$offsetMinutes = -$offsetMinutes;
	}
	elseif($offsetMinutes > 0 && $offsetMinutes != 7) {
		$offsetHours--;
	}
	if($offsetHours == 0 && $offsetMinutes == 0)
		return "";
	else
		return ($offsetHours > 0 ? "+" : "-").
			sprintf("%02u:%02u", abs($offsetHours), $offsetMinutes);
}

?>