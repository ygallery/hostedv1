<?

$whereGuest = isLoggedIn() ? "" : ( " AND `useGuestAccess` = '1' " );

$result = sql_query( "SELECT * FROM `users`,`useExtData` ".
	"WHERE `useUsername` = '".addslashes( $_cmd[ 1 ])."' AND `useEid` = `useid` $whereGuest LIMIT 1" );

if( !$useData = mysql_fetch_assoc( $result ))
{
	if( isLoggedIn() )
	{
		redirect( url( "friends/".strtolower( $_auth[ "useUsername" ])));
	}

	include( INCLUDES."p_notfound.php" );
	return;
}

$useUsername = strtolower( $useData[ "useUsername" ]);

$_pollUser = $useData[ "useid" ];

$_documentTitle = $useData[ "useUsername" ].": "._FRIENDS;

?>
<div class="header">
	<div class="f_right mar_left a_center normaltext">
		<?= getUserAvatar( "",$useData[ "useid" ], true )?>
	</div>
	<div class="f_left header_title">
		<?= $useData[ "useUsername" ] ?>
		<div class="subheader"><?= _FRIENDS ?></div>
	</div>
	<?

	$active = 3;

	include( INCLUDES."mod_usermenu.php" );

	?>
</div>

<div class="container">
	<?

	ob_start();

	$fr_offset = isset( $_GET[ "offset" ]) ? intval( $_GET[ "offset" ]) : 0;
	$fr_limit = 30;

	iefixStart();

	$guestAccess = isLoggedIn() ? "" : "AND `useGuestAccess` = '1'";

	$query = "FROM `users`,`useExtData`,`watches` AS w1,`watches` AS w2 ".
		"WHERE `useid` = w1.`watCreator` ".
		"AND `useid` = `useEid` ".
		"$guestAccess ".
		"AND w1.`watUser` = '".$useData[ "useid" ]."' ".
		"AND w1.`watType` = 'use' ".
		"AND w2.`watType` = 'use' ".
		"AND w1.`watCreator` = w2.`watUser` ".
		"AND w2.`watCreator` = w1.`watUser` ".
		"ORDER BY w1.`watSubmitDate` DESC";

	$result = sql_query( "SELECT DISTINCT `useid` $query LIMIT $fr_offset, $fr_limit" );

	while( $rowData = mysql_fetch_assoc( $result ))
	{
		?>
		<div class="f_left a_center mar_right mar_bottom" style="height: 100px">
			<?= getUserAvatar( "", $rowData[ "useid" ], true ) ?>
		</div>
		<?
	}

	$result = sql_query( "SELECT COUNT(*) $query" );

	$fr_totalCount = mysql_result( $result, 0 );

	?>
	<div class="clear">&nbsp;</div>
	<?

	iefixEnd();

	$ht = ob_get_contents();

	ob_end_clean();

	$active = 1;

	include( INCLUDES."mod_friendsubmenu.php" );

	echo $ht;

	?>
</div>
