<?

$whereGuest = isLoggedIn() ? "" : ( " AND `useGuestAccess` = '1' " );

$result = sql_query( "SELECT * FROM `users`, `useExtData` ".
	"WHERE `useid` = `useEid` AND `useUsername` = '".addslashes( $_cmd[ 1 ])."' $whereGuest LIMIT 1" );

if( !$useData = mysql_fetch_assoc( $result ))
{
	if( isLoggedIn() )
	{
		redirect( url( "stats/".strtolower( $_auth[ "useUsername" ])));
	}

	include( INCLUDES."p_notfound.php" );
	return;
}

if( $useData[ "useid" ] != $_auth[ "useid" ] && !$useData[ "useStatsPublic" ] && !$_auth["useIsModerator"] && !$_auth["useIsSModerator"] && !$_auth["useIsDeveloper"])
{
	include( INCLUDES."p_notfound.php" );
	return;
}

if( $_auth[ "useStatsHide" ])
{
	include( INCLUDES."p_notfound.php" );
	return;
}

$useUsername = strtolower( $useData[ "useUsername" ]);
$useid = intval( $useData[ "useid" ]);

$_pollUser = $useData[ "useid" ];
$_documentTitle = $useData[ "useUsername" ].": "._STATS;

?>
<div class="header">
	<div class="f_left header_title">
		<?= $useData[ "useUsername" ] ?>
		<div class="subheader"><?= _STATS ?></div>
	</div>
	<?

	$active = 6;
	include( INCLUDES."mod_usermenu.php" );

	?>
</div>

<div class="container">
	<?
		if( !$_auth[ "useid" ])
		{
			header("Status: 403 Forbidden");
			notice( _REQUIRE_LOGIN );
			echo "</div>";
			return;
		}

		$result = sql_query( "SELECT COUNT(*) FROM `comments` ".
			"WHERE `comCreator`='$useid'" );

		$comments = mysql_result( $result, 0 );

		$result = sql_query( "SELECT COUNT(*) FROM `objects` ".
			"WHERE `objCreator`='$useid' AND `objDeleted` = '0' ".
			"AND `objPending` = '0'" );

		$submissions = mysql_result( $result, 0 );

		$result = sql_query( "SELECT COUNT(*) FROM `objects`, `objExtData` ".
			"WHERE `objid` = `objEid` ".
			"AND `objCollab` <> '0' ".
			"AND '$useid' IN(`objCollab`,`objCreator`) ".
			"AND `objDeleted` = '0' ".
			"AND `objPending` = '0'" );

		$collabs = mysql_result( $result, 0 );

		$result = sql_query( "SELECT COUNT(*) FROM `extras` ".
			"WHERE `objCreator`='$useid' AND `objDeleted` = '0' ".
			"AND `objPending` = '0'" );

		$extras = mysql_result( $result, 0 );

		$result = sql_query( "SELECT COUNT(*) FROM `favourites`, `objects` ".
			"WHERE `favCreator`='$useid' AND `favObj` = `objid` ".
			"AND `objDeleted` = '0' AND `objPending` = '0'" );

		$favourites = mysql_result( $result, 0 );

		$result = sql_query( "SELECT COUNT(*) FROM `journals` ".
			"WHERE `jouCreatorType` = 'use' AND `jouCreator`='$useid'" );

		$journals = mysql_result( $result, 0 );

		$result = sql_query( "SELECT SUM(`objViewed`) FROM `objects`,`objExtData` ".
			"WHERE `objid` = `objEid` AND `objCreator`='$useid'" );

		$totalviews = mysql_result( $result, 0 );

		$result = sql_query( "SELECT SUM(`objViewed`) FROM `extras` ".
			"WHERE `objCreator`='$useid'" );

		$totalviews += mysql_result( $result, 0 );

		$result = sql_query( "SELECT SUM(`objFavs`) FROM `objects`,`objExtData` ".
			"WHERE `objid` = `objEid` AND `objCreator`='$useid'");

		$totalfavs = mysql_result( $result, 0 );

		$result = sql_query( "SELECT COUNT(*) FROM `comments` ".
			"WHERE `comObjType` = 'use' AND `comObj` = '$useid'" );

		$totalComments = mysql_result( $result, 0 );

		$result = sql_query( "SELECT COUNT(*) FROM `comments`,`objects` ".
			"WHERE `comObj` = `objid` AND `comObjType` = 'obj' AND `objCreator` = '$useid'" );

		$totalComments += mysql_result( $result, 0 );

		/*
		$result = sql_query( "SELECT COUNT(*) FROM `comments` AS c1 ".
			"LEFT JOIN `comments` AS c2 ON c1.`comObj` = c2.`comid` ".
			"WHERE c1.`comObjType` = 'com' AND c2.`comCreator` = '$useid'" );
		*/

		$result = sql_query( "SELECT COUNT(*) FROM `comments` AS c1 ".
			"LEFT JOIN `comments` AS c2 ON c1.`comid` = c2.`comObj` ".
			"WHERE c1.`comCreator` = '$useid' AND c2.`comObjType` = 'com'" );

		$totalComments += mysql_result( $result, 0 );

		$result = sql_query( "SELECT COUNT(*) FROM `comments`,`cluExtData` ".
			"WHERE `comObj` = `cluEid` AND `comObjType` = 'clu' AND `cluCreator` = '$useid'" );

		$totalComments += mysql_result( $result, 0 );

		$result = sql_query( "SELECT COUNT(*) FROM `comments`,`journals` ".
			"WHERE `comObj` = `jouid` AND `comObjType` = 'jou' AND `jouCreator` = '$useid' ".
			"AND `jouCreatorType` = 'use'" );

		$totalComments += mysql_result( $result, 0 );

		$result = sql_query( "SELECT COUNT(*) FROM `comments`,`polls` ".
			"WHERE `comObj` = `polid` AND `comObjType` = 'pol' AND `polCreator` = '$useid'" );

		$totalComments += mysql_result( $result, 0 );

		$result = sql_query( "SELECT COUNT(*) FROM `comments`,`news` ".
			"WHERE `comObj` = `newid` AND `comObjType` = 'new' AND `newCreator` = '$useid'" );

		$totalComments += mysql_result( $result, 0 );

	?>
	<div class="container2 user_stats">
		<div class="f_right mar_left a_center normaltext">
			<?= getUserAvatar( "", $useData[ "useid" ], true ) ?>
		</div>
		<div class="largetext">
			<div>
				<?= getIMG( url()."images/emoticons/comment.png") ?>
				<b><?= _COMMENTS ?></b>: <?= fuzzy_number( $comments ) ?>
			</div>
			<?

			if( $favourites > 0 )
			{
				?>
				<div>
					<?= getIMG( url()."images/emoticons/fav1.png" ) ?>
					<b><?= _FAVOURITES ?></b>: <?= fuzzy_number( $favourites )?>
				</div>
				<?
			}

			if( $journals > 0 )
			{
				?>
				<div>
					<?= getIMG( url()."images/emoticons/journal.png" ) ?>
					<b><?= _JOURNALS ?></b>: <?= fuzzy_number( $journals ) ?>
				</div>
				<?
			}

			if( $submissions > 0 )
			{
				?>
				<div>
					<?= getIMG( url()."images/emoticons/submission.png" ) ?>
					<b><?= _SUBMISSIONS ?></b>: <?= fuzzy_number( $submissions ) ?>
				</div>
				<?
			}

			if( $collabs > 0 )
			{
				?>
				<div>
					<?= getIMG( url()."images/emoticons/club.png" ) ?>
					<b><?= _COLLABS ?></b>: <?= fuzzy_number( $collabs ) ?>
				</div>
				<?
			}

			if( $extras > 0 )
			{
				?>
				<div>
					<?= getIMG( url()."images/emoticons/star4.png" ) ?>
					<b><?= _SUBMIT_TYPE_EXTRA ?></b>: <?= fuzzy_number( $extras ) ?>
				</div>
				<?
			}

			?>
		</div>
		<div class="sep caption">
			<i><?= _RECEIVED ?></i>:
		</div>
		<?

		if( $totalviews > 0 )
		{
			?>
			<div>
				<?= getIMG( url()."images/emoticons/watch.png" ) ?>
				<b><?= _TOTAL_VIEWS ?></b>: <?= fuzzy_number( $totalviews, 10 ) ?>
			</div>
			<?
		}

		if( $totalfavs > 0 )
		{
			?>
			<div>
				<?= getIMG( url()."images/emoticons/heart2.png" ) ?>
				<b><?= _TOTAL_FAVS ?></b>: <?= fuzzy_number( $totalfavs ) ?>
			</div>
			<?
		}

		?>
		<div>
			<?= getIMG( url()."images/emoticons/comment.png" ) ?>
			<b><?= _COMMENTS ?></b>: <?= fuzzy_number( $totalComments ) ?>
		</div>
		<div class="clear">&nbsp;</div>
	</div>
</div>
