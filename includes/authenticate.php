<? // user authentication system
	require_once(INCLUDES."common.php");

function verifyLogin($username,$password,$persistentLogin = 0) // this will confirm that a user exists and is not suspended/banned
{
	global $db,$_config;
	$result = sql_query("SELECT `useid`,`useIsSuspended`,`useIsBanned`,`useIsActive`,`useEmail`,`useUsername`,`useSuspendedReason` FROM `users`,`useExtData` WHERE `useid` = `useEid` AND `useUsername` = '".mysql_real_escape_string($username)."' AND `usePassword` = '" . SHA1($password) . "' LIMIT 1",$db); // find user data
	if(mysql_num_rows($result)) // if user exists
	{
		$loginData = mysql_fetch_assoc($result); // get user data
		if(!$loginData["useIsActive"]) // check if user account is not activated
		{
			notice(sprintf(_USE_INACTIVE, $loginData["useEmail"])); // alert user that they should activate the account
			echo '<div class="sep"><a href="'.url("emailresend/".strtolower($loginData["useUsername"])).'">'._USE_ACTIVATE_RESEND.'</a></div>';
			echo '<div class="sep"><a href="'.url("emailchange/".strtolower($loginData["useUsername"])).'">'._USE_ACTIVATE_CHANGE.'</a></div>';
			return false; // fail login verification
		}
		elseif($loginData["useIsBanned"]) // check if user is banned
		{
			notice(sprintf(_ABUSE_BANNED_NOTICE,$loginData["useSuspendedReason"])); // alert user that they are banned
			sql_query("DELETE FROM `sessions` WHERE `sesCreator` = '" . $loginData["useid"] . "'",$db); // remove all user sessions
			return false; // fail login verification
		}
		elseif($loginData["useIsSuspended"]) // check if user is suspended
		{
			$useSuspendedUntil = mysql_result(sql_query("SELECT UNIX_TIMESTAMP(`useSuspendedUntil`) FROM `useExtData` WHERE `useEid` = '" . $loginData["useid"] . "' LIMIT 1",$db),0); // get suspension time
			if($useSuspendedUntil <= time()) // check if suspension is over
			{
				sql_query("UPDATE `users` SET `useIsSuspended` = '0' WHERE `useid` = '" . $loginData["useid"] . "' LIMIT 1",$db); // suspension is over
				return createSession($loginData["useid"]); // succeed login verification
			}
			else // suspension is still in effect
			{
				notice(sprintf(_ABUSE_SUSPENDED_NOTICE,gmdate($_config["dateFormat"], applyTimezone($useSuspendedUntil)),$loginData["useSuspendedReason"])); // alert user that they are suspended
				sql_query("DELETE FROM `sessions` WHERE `sesCreator` = '" . $loginData["useid"] . "'",$db); // remove all user sessions
				return false; // fail login verification
			}
		}
		else return createSession($loginData["useid"],$persistentLogin); // normal user, succeed login verification
	}
	else
	{ // user not found or password mismatch
		notice(_USE_LOGIN_ERROR);
		return false; // fail login verification
	}
};

?>
