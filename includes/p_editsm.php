<?

if( substr( $_cmd[ 1 ], 0, 1 ) == "e" )
{
	$isExtras = true;
	$objid = intval( substr( $_cmd[ 1 ], 1 ));
	$_objects = "`extras`";
	$_objExtData = "`extExtData`";
}
else
{
	$objid = intval( $_cmd[ 1 ]);
	$isExtras = false;
	$_objects = "`objects`";
	$_objExtData = "`objExtData`";
}

$result = sql_query( "SELECT * FROM $_objects, $_objExtData ".
	"WHERE `objid` = '$objid' AND `objEid` = '$objid' LIMIT 1" );

if( !$objData = mysql_fetch_assoc( $result ))
{
	include( INCLUDES."p_notfound.php" );
	return;
}

$_documentTitle = _EDIT_SUBMISSION;

?>
<div class="header">
	<div class="header_title">
		<?= _EDIT_SUBMISSION ?>
		<div class="subheader">
			<?= formatText( $objData[ "objTitle" ]) ?>
		</div>
	</div>
	<div class="clear">&nbsp;</div>
</div>

<div class="container">
<?

$editAction = isset( $_GET[ "edit" ]) ? $_GET[ "edit" ] : "";

switch( $editAction )
{
	case "resetedits": // reset submission's number of edits

		if( atLeastSModerator() )
		{
			sql_query( "UPDATE $_objExtData SET `objNumEdits` = '0' ".
				"WHERE `objEid` = '$objid'" );
		}

		redirect(url("view/".( $isExtras ? "e" : "" ).$objid));

	case "undelete": // undelete submission (administrator/creator only)

		if( $objData[ "objDeleted" ] && ( isAdmin() ||
			$objData[ "objDeletedBy" ] == $_auth[ "useid" ]))
		{
			sql_query( "UPDATE $_objects SET `objDeleted` = '0' ".
				"WHERE `objid` = '$objid'");

			sql_query( "UPDATE $_objExtData SET `objDeletedBy` = '0', `objDeleteDate` = NOW() ".
				"WHERE `objEid` = '$objid'");

			include_once(INCLUDES."submission.php");
			updateObjCount( $objData[ "objCreator" ]);
		}

		redirect( url( "view/".( $isExtras ? "e" : "" ).$objid ));

	case "erase": // erase submission, removing the submission files from the disk (administrator only)

		if( isAdmin() )
		{
			include_once(INCLUDES."submission.php");
			eraseSubmission( $objid, $isExtras );

			notice( _SUBMISSION_ERASED );
		}

		break;

	default: // no correct action specified, redirect back to the submission

		redirect( url( "view/".( $isExtras ? "e" : "" ).$objid ));
}

?>
</div>
