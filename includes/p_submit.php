<?

$_documentTitle = _SUBMIT;

if( !$_auth[ "useid" ])
{
	header("Status: 403 Forbidden");
	?>
	<div class="header">
		<div class="header_title"><?= _SUBMIT_TITLE ?></div>
	</div>
	<div class="container">
		<?

		if( $_cmd[ 1 ] == "oekaki" )
		{
			notice( _OEKAKI_LOGOUT );

			?>
			<form action="<?= url( "." ) ?>" method="post">
				<div class="sep caption"><?= _USERNAME ?>:</div>
				<div><input class="narrow" name="username" type="text"
					<?=isset( $_POST[ "username" ])
						? 'value="'.htmlspecialchars( $_POST[ "username" ]).'"' : "" ?> /></div>

				<div class="sep caption"><?= _PASSWORD ?>:</div>
				<div><input class="narrow" name="password" type="password" /></div>

				<div class="sep"><input checked="checked" class="checkbox"
					id="loginPersistent" name="persistent" type="checkbox" />
					<label for="loginPersistent"><?= _USE_REMEMBER ?></label></div>

				<div class="sep">
					<button class="submit" type="submit">
						<?= getIMG( url()."images/emoticons/checked.png" ) ?>
						<?= _LOGIN ?>
					</button>
				</div>
			</form>
			<?
		}
		else
		{
			notice( _REQUIRE_LOGIN );
		}

		?>
	</div>
	<?

	return;
}

if( $_config[ "readOnly" ])
{
	?>
	<div class="header"><div class="header_title"><?= _SUBMIT_TITLE ?></div></div>
	<div class="container">
		<? notice(_READONLY); ?>
	</div>
	<?

	return;
}

if( $_cmd[ 1 ] == "oekaki" )
{
	switch( $_cmd[ 2 ])
	{
		case OEK_SHI:
		case OEK_SHIPRO:
		case OEK_PAINTBBS:
		case OEK_OEKAKIBBS:
			$_POST[ "type" ] = "1"; // Type = Image/Animation
			break;

		default:

			?>
			<div class="header"><div class="header_title"><?= _SUBMIT_TITLE ?></div></div>
			<div class="container">
				<? notice(_OEKAKI_UNKNOWN_EDITOR); ?>
			</div>
			<?

			return;
	}
}

?>
<form action="<?= url( "." ) ?>" enctype="multipart/form-data" method="post">
<?

$putKeywordsList = false;

// Determine submission step.

// Step 0. Submission type.

$step = 0;

if( isset( $_POST[ "type" ]))
{
	$validated0 = true;

	if( $_POST[ "type" ] == "4" )
	{
		redirect( url( "extrasubmit" ));
	}

	if( $_POST[ "type" ] == "5" )
	{
		redirect( url( "oekaki" ));
	}

	if( $_POST[ "type" ] != "1" && $_POST[ "type" ] != "2" && $_POST[ "type" ] != "10" )
	{
		$validated0 = false;
	}

	if( $validated0 )
	{
		// Step 1. Title and comment.

		?>
		<input name="type" type="hidden" value="<?= $_POST[ "type" ] ?>" />
		<?

		$step = 1;

		if( isset( $_POST[ "title" ]) && isset( $_POST[ "comment" ]))
		{
			$validated1 = true;

			if( $_POST[ "title" ] == "" )
			{
				$validated1 = false;
				$requireTitle = true;
			}

			if( $_POST[ "comment" ] == "" )
			{
				$validated1 = false;
				$requireComment = true;
			}

			if( isset( $_POST[ "keywordList" ]) &&
				$_POST[ "keywordList" ] != "" )
			{
				$putKeywordsList = true;
			}

			if( $validated1 && !isset( $_POST[ "backToStep1" ]))
			{
				// Step 2. Keywords.

				$putKeywordsList = false; // Don't put additional
					// <input name="keywordList"> at this point.

				?>
				<input name="title" type="hidden"
					value="<?= htmlspecialchars( substr( $_POST[ "title" ], 0, 60 )) ?>" />

				<input name="comment" type="hidden"
					value="<?= htmlspecialchars( substr( $_POST["comment"], 0, 4000 )) ?>" />

				<input name="forClub" type="hidden"
					value="<?=isset($_POST["forClub"]) ? $_POST["forClub"] : "0"?>" />

				<input name="forClub2" type="hidden"
					value="<?=isset($_POST["forClub2"]) ? $_POST["forClub2"] : "0"?>" />

				<input name="forClub3" type="hidden"
					value="<?=isset($_POST["forClub3"]) ? $_POST["forClub3"] : "0"?>" />

				<input name="folder" type="hidden"
					value="<?=isset($_POST["folder"]) ? $_POST["folder"] : "0"?>" />

				<input name="collab" type="hidden"
					value="<?=isset($_POST["collab"]) ? $_POST["collab"] : "0"?>" />
				<?

				$step = 2;

				if( isset( $_POST[ "keywordList" ]))
				{
					$validated2 = true;

					include_once( INCLUDES."keywording.php" );

					$requireKGroups = requireRootKeywords( $_POST[ "keywordList" ]);

					if( $requireKGroups != "" )
					{
						$validated2 = false;
					}

					if( $validated2 && !isset( $_POST[ "backToStep2" ]))
					{
						// step 3: preview

						$putKeywordsList = true;

						?>
						<input name="preview" type="hidden" value="1" />
						<?

						$step = 3;

						if( isset( $_POST[ "preview" ]))
						{
							$validated3 = true;

							// Query user's last submission time.

							$result = sql_query( "SELECT UNIX_TIMESTAMP(`useLastSubmission`) ".
								"FROM `users` WHERE `useid` = '".$_auth[ "useid" ]."' LIMIT 1");

							if( mysql_num_rows( $result ) > 0 )
							{
								$timePassed = round( time() - mysql_result( $result, 0 ));

								if($timePassed < $_config["newObjectDelay"])
								{
									$validated3 = false;

									notice( sprintf( _SUBMIT_WAIT,
										$_config[ "newObjectDelay" ] - $timePassed ));
								}
							}

							if( $validated3 && !isset( $_POST[ "backToStep3" ]))
							{
								// step 4: uploading the files

								$step = 4;
								$oekakiExtras = "";

								if( $_cmd[ 1 ] == "oekaki" )
								{
									$oekakiSession = $_auth[ "useid" ];

									$baseFilename = preg_replace( '/\..+$/', "",
										findNewestFile( "files/oekakitemp/".$oekakiSession."-*", "" ));

									if( file_exists( $baseFilename.".png" ))
									{
										$_FILES[ "submission" ][ "error" ] = UPLOAD_ERR_OK;
										$_FILES[ "submission" ][ "tmp_name" ] = $baseFilename.".png";
										$_FILES[ "submission" ][ "name" ] = "/oekaki/".intval( $_cmd[ 2 ]);
										$_FILES[ "submission" ][ "type" ] = "image/png";
										$_FILES[ "submission" ][ "simple_move" ] = true;

										if( file_exists( $baseFilename.".pch" ))
										{
											$_FILES[ "submission" ][ "also_move" ] = $baseFilename.".pch";
											$_FILES[ "submission" ][ "also_move_ext" ] = ".pch";
											$oekakiExtras = "`objAniType` = 'pch'";
										}
										elseif( file_exists( $baseFilename.".oeb" ))
										{
											$_FILES[ "submission" ][ "also_move" ] = $baseFilename.".oeb";
											$_FILES[ "submission" ][ "also_move_ext" ] = ".oeb";
											$oekakiExtras = "`objAniType` = 'oeb'";
										}
										else
										{
											$oekakiExtras = "`objAniType` = 'no'";
										}
									}
								}

								include_once( INCLUDES."submission.php" );
								include_once( INCLUDES."files.php" ); // File uploading functions.

								if( isset( $_POST[ "textfile" ]) && $_POST[ "textfile" ] != "" )
								{
									if( str_word_count($_POST[ "textfile" ], 0) >= 375 )
									{
										$tempfilename = "files/texttemp/".$_auth[ "useid" ].".txt";

										forceFolders( dirname( $tempfilename ));

										$fp = fopen( $tempfilename, "wb" );
										fputs( $fp, $_POST[ "textfile" ]);
										fclose( $fp );

										$_FILES[ "submission" ][ "error" ] = UPLOAD_ERR_OK;
										$_FILES[ "submission" ][ "tmp_name" ] = $tempfilename;
										$_FILES[ "submission" ][ "name" ] = "textfile.txt";
										$_FILES[ "submission" ][ "type" ] = "text/plain";
										$_FILES[ "submission" ][ "simple_move" ] = true;
									}
									else
									{
										$uploadError = sprintf('Submission length does not meet required minimum of %d words', 375);
									}
								}

								if( isset( $_FILES[ "submission" ]))
								{
									$uploadError = checkUploadedFile( "submission" );
									$uploadErrorThumb = checkUploadedFile( "thumb" );

									if( !$uploadError &&
										($uploadErrorThumb == "" ||
										$uploadErrorThumb == _UPL_NO_FILE))
									{
										// Submit artwork.

										include_once(INCLUDES."submission.php");

										// Double-check if the user is the owner of specified folder

										$folder = isset( $_POST[ "folder" ]) ? intval( $_POST[ "folder" ]) : 0;

										$result = sql_query( "SELECT COUNT(*) FROM `folders`".dbWhere( array(
											"folCreator" => $_auth[ "useid" ],
											"folid" => $folder )));

										if( mysql_result( $result, 0 ) == 0 )
											$folder = 0;

										// Double-check if the user is a member of this club.

										function checkClubMember( $forClub )
										{
											global $_auth;

											$forClub = intval( $forClub );

											$result = sql_query( "SELECT `useCid` FROM `useClubs` ".
												"WHERE `useCclub` = '$forClub' ".
												"AND `useCmember` = '".$_auth[ "useid" ]."' ".
												"AND `useCpending` = '0' LIMIT 1" );

											if( mysql_num_rows( $result ) == 0 )
											{
												return( 0 ); // hacked POST, change to "no club"
											}
											else
											{
												return( $forClub );
											}
										}

										$forClub = checkClubMember( $_POST[ "forClub" ]);
										$forClub2 = checkClubMember( $_POST[ "forClub2" ]);
										$forClub3 = checkClubMember( $_POST[ "forClub3" ]);

										$collab = isset( $_POST[ "collab" ]) ? intval( $_POST[ "collab" ]) : 0;
										$gift = isset( $_POST[ "gift" ]) ? intval( $_POST[ "gift" ]) : 0;

										// Submit new title and receive the object id.

										$filters = implode( ",",
											getFiltersByKeywords( preg_split( '/\s/', $_POST[ "keywordList" ],
											-1, PREG_SPLIT_NO_EMPTY )));

										$objid = submitNewTitle( $_POST[ "title" ],
											$_POST[ "comment" ], $filters, $forClub, $folder, $collab, $gift,
											$forClub2, $forClub3 );

										submitKeywords( $objid, $_POST[ "keywordList" ]);

										if( submitImage( $objid, "submission",
											$uploadErrorThumb == _UPL_NO_FILE ? "" : "thumb",
											$uploadErrorThumb, $imageChanged ))
										{
											// Make the submission accessible by setting
											// `objDeleted` = '0'.

											sql_query( "UPDATE `objects` ".
												"SET `objDeleted` = '0' ".
												"WHERE `objid` = '$objid' LIMIT 1" );

											if( $oekakiExtras != "" )
											{
												sql_query( "UPDATE `objExtData` ".
													"SET $oekakiExtras ".
													"WHERE `objEid` = '$objid' LIMIT 1" );
											}

											// Notify the watchers.

											addArtUpdateToWatchers( $_auth[ "useid" ], $objid, $forClub );
											addArtUpdateToWatchers( $_auth[ "useid" ], $objid, $forClub2 );
											addArtUpdateToWatchers( $_auth[ "useid" ], $objid, $forClub3 );

											removeDupeArtUpdates( $objid );

											// Clean up the objects table - remove
											// submissions that failed uploading.

											$result = sql_query( "SELECT `objid` ".
												"FROM `objects`, `objExtData` ".
												"WHERE `objid` = `objEid` ".
												"AND `objExtension` = '' ".
												"AND `objDeleted` = '1'" );

											while( $delData = mysql_fetch_assoc( $result ))
											{
												sql_query( "DELETE FROM `objects` ".
													"WHERE `objid` = '".$delData[ "objid" ]."'" );

												sql_query( "DELETE FROM `objExtData` ".
													"WHERE `objEid` = '".$delData[ "objid" ]."'" );
											}

											include_once(INCLUDES."submission.php");
											updateObjCount( $_auth[ "useid" ]);

											// Redirect the user to the newly submitted artwork...

											redirect( url( "view/".$objid ));
										}

										if( !$uploadError && $uploadErrorThumb )
										{
											$uploadError = _SUBMIT_THUMBNAIL_ERROR;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

if( $putKeywordsList )
{
	?>
	<input name="keywordList" type="hidden"
		value="<?= substr( htmlspecialchars( $_POST[ "keywordList" ]), 0, 1000 ) ?>" />
	<?
}

switch( $step )
{
	case 0: // Step 0 - Select the type of submission

		?>
		<div class="header">
			<div class="header_title">
				<?=_SUBMIT_TITLE ?>
				<div class="subheader"><?=_SUBMIT_SUBTITLE0 ?></div>
			</div>
		</div>
		<div class="container largetext">
			<div>
				<input id="idType1" type="radio" class="radio" name="type" value="1" />
				<label for="idType1">
					<?= getIMG( url()."images/emoticons/submission.png" ) ?>
					<?= _SUBMIT_TYPE_IMAGE ?></label>
			</div>
			<div class="sep notsowide normaltext">
				<?= _SUBMIT_TYPE_IMAGE_EXPLAIN ?>
			</div>
			<div class="sep">
				<input id="idType2" type="radio" class="radio" name="type" value="2" />
				<label for="idType2">
					<?= getIMG( url()."images/emoticons/a-left.png" ) ?>
					<?= _SUBMIT_TYPE_LIT ?></label>
			</div>
			<div class="sep notsowide normaltext">
				<?= _SUBMIT_TYPE_LIT_EXPLAIN ?>
			</div>
			<div class="sep">
				<input id="idType5" type="radio" class="radio" name="type" value="5" />
				<label for="idType5">
					<?= getIMG( url()."images/emoticons/submission.png" ) ?>
					<?= _OEKAKI ?></label>
			</div>
			<div class="sep notsowide normaltext">
				<?= _OEKAKI_SUBTITLE ?>
			</div>
			<div>
				<input id="idType10" type="radio" class="radio" name="type" value="10" />
				<label for="idType10">
					<?= getIMG( url()."images/emoticons/submission.png" ) ?>
					<?= _SUBMIT_TYPE_VID ?></label>
			</div>
			<div class="sep notsowide normaltext">
				<?= _SUBMIT_TYPE_VID_EXPLAIN ?>
			</div>
			<?/*
			<div class="sep">
				<input id="idType3" type="radio" name="type" value="3" />
				<label for="idType3">
					<?= getIMG( url()."images/emoticons/zip.png" ) ?>
					ZIP archive</label>
			</div>
			<div class="sep notsowide normaltext">
				<b>Coming soon</b>.
				You will be able to upload a ZIP file containing multiple images
				and assign them a common description. Each image from the
				archive will become a separate submission. If you want the
				images to expand into a project, please
				<a href="<?= url( "newclub" ) ?>">create the project</a> first,
				then choose it while submitting your ZIP file.
			</div>
			*/?>
			<?

			if( isExtras() && isArtist( $_auth[ "useid" ]) )
			{
				?>
				<div class="sep">
					<input id="idType4" type="radio" class="radio" name="type" value="4" />
					<label for="idType4">
						<?= getIMG( url()."images/emoticons/star4.png" ) ?>
						<?= _SUBMIT_TYPE_EXTRA ?></label>
				</div>
				<div class="sep notsowide normaltext">
					<?= sprintf( _SUBMIT_TYPE_EXTRA_EXPLAIN, $_config[ "extrasLiftedRules" ]) ?>
				</div>
				<?
			}

			?>
			<div class="sep">
				<button class="submit" name="submit" type="submit">
					<?= getIMG( url()."images/emoticons/checked.png" ) ?>
					<?= _CONTINUE ?>
				</button>
			</div>
		</div>
		<?

		break;

	case 1: // Step 1 - I. Describe artwork (enter title and comment)
		?>
		<div class="header">
			<div class="header_title">
				<?=_SUBMIT_TITLE ?>
				<div class="subheader"><?=_SUBMIT_SUBTITLE1 ?></div>
			</div>
		</div>

		<div class="container">
			<?
			if($_cmd[1] == "oekaki") {
				echo '<div class="mar_bottom notsowide error">'._SUBMIT_OEKAKI_EXPLAIN.'</div>';
			}
			$clubMember = $_auth["useid"];
			include(INCLUDES."mod_submit_page1.php");
			?>
			<button class="submit" name="submit"
				onclick="el=get_by_id('submitTitle'); if(!el.value){ alert('<?=_SUBMIT_NO_TITLE ?>'); return false } el=get_by_id('<?=$commentId ?>'); if(!el.value){ alert('<?=_SUBMIT_NO_COMMENT ?>'); return false } return true"
				type="submit">
				<?=getIMG(url()."images/emoticons/checked.png") ?>
				<?=_CONTINUE ?>
			</button>
		</div>
		<?
		break;

	case 2: // Step 2 -- II. Categorise artwork
		?>
		<div class="header">
			<div class="header_title">
				<?=_SUBMIT_TITLE ?>
				<div class="subheader"><?=_SUBMIT_SUBTITLE2 ?></div>
			</div>
			<?
			if(isset($_POST["keywordList"]))
				$defaultKeywords = $_POST["keywordList"];
			include (INCLUDES."mod_keywords.php");

			if(isset($requireKGroups) && $requireKGroups)
				notice($requireKGroups);
			?>
			<div class="padded notsowide normaltext">
				<?= sprintf( _SUBMIT_KEYWORDS_EXPLAIN, $_config[ "keywordExplainURI" ]) ?>
			</div>
			<div>
				<input class="submit" type="submit" name="backToStep1"
					value="<?=_BACK ?>" />
				&nbsp;
				<input class="submit" type="submit" name="submit"
					value="<?=_CONTINUE ?>" />
			</div>
		</div>
		<?
		break;

	case 3: // Step 3 - III. Preview submission

		$allowContinue = true;

		?>
		<div class="header">
			<div class="header_title">
				<?=_SUBMIT_TITLE ?>
				<div class="subheader"><?=_SUBMIT_SUBTITLE3 ?></div>
			</div>
		</div>

		<div class="container">

			<div class="caption"><?=_TITLE ?>:</div>
			<div class="container2 notsowide largetext">
				<?=formatText($_POST["title"])?>
			</div>

			<div class="sep caption"><?=_COMMENT ?>:</div>
			<div class="container2 notsowide">
				<div style="margin-top: -8px">
					<?
					include_once(INCLUDES."comments.php");
					$comData = array();
					$comData["comid"] = 0;
					$comData["comCreator"] = $_auth["useid"];
					$comData["comComment"] = $_POST["comment"];
					$comData["comSubmitDate"] = 0;
					$comData["comNoEmoticons"] = 0;
					$comData["comNoSig"] = 0;
					$comData["comNoBBCode"] = 0;
					showComment($comData, 0);
					?>
				</div>
			</div>
			<?

			ob_start();

			?>
			<div class="sep caption"><?=_SUBMIT_FOR_CLUB ?>:</div>
			<div class="container2 notsowide">
				<?
					$clubQuery = "SELECT `cluid`,`cluName`,`cluDesc` FROM `clubs`,`useClubs` ".
						"WHERE `cluid` = `useCclub` AND ".
						"`cluid` = '".intval($_POST["forClub"])."' AND ".
						"`useCmember` = '".$_auth["useid"]."' AND `useCpending` = '0' LIMIT 1";
					include(INCLUDES."mod_clubs.php");

					$clubQuery = "SELECT `cluid`,`cluName`,`cluDesc` FROM `clubs`,`useClubs` ".
						"WHERE `cluid` = `useCclub` AND ".
						"`cluid` = '".intval($_POST["forClub2"])."' AND ".
						"`useCmember` = '".$_auth["useid"]."' AND `useCpending` = '0' LIMIT 1";
					include(INCLUDES."mod_clubs.php");

					$clubQuery = "SELECT `cluid`,`cluName`,`cluDesc` FROM `clubs`,`useClubs` ".
						"WHERE `cluid` = `useCclub` AND ".
						"`cluid` = '".intval($_POST["forClub3"])."' AND ".
						"`useCmember` = '".$_auth["useid"]."' AND `useCpending` = '0' LIMIT 1";
					include(INCLUDES."mod_clubs.php");
				?>
			</div>
			<?

			if( $clubCount > 0 )
				ob_end_flush();
			else
				ob_end_clean();

			?>
			<div class="sep caption"><?= _KEYWORDS ?>:</div>
			<div class="container2 notsowide">
				<?

				// Fetch the keyword list

				$result = sql_query( "SELECT * FROM `keywords` WHERE 1 ORDER BY `keySubcat`, `keyWord`" );

				$keywords = array();

				while( $rowData = mysql_fetch_assoc( $result ))
				{
					$rowData[ "keyWord" ] = trim( preg_replace( '/^.*\|/', "", $rowData[ "keyWord" ]));
					$rowData[ "keyWord" ] = preg_replace( '/\@$/', "", $rowData[ "keyWord" ]);
					$keywords[ $rowData[ "keyid" ]] = $rowData;
				}

				mysql_free_result( $result );

				function __traverseKeyword( $keywords, $keyid )
				{
					if( !isset( $keywords[ $keyid ]))
						return( "?" );

					$word = $keywords[ $keyid ][ "keyWord" ];

					if( $keywords[ $keyid ][ "keySubcat" ])
					{
						$word = __traverseKeyword( $keywords, $keywords[ $keyid ][ "keySubcat" ])." - ".$word;
					}

					return( $word );
				}

				// Show the list of chosen keywords.

				$idList = $_POST[ "keywordList" ];

				$idList = preg_split( '/\s/', $idList, -1, PREG_SPLIT_NO_EMPTY );

				$stringList = array();

				foreach( $idList as $keyid )
				{
					$stringList[ $keyid ] = "&bull; ".__traverseKeyword( $keywords, $keyid )."<br />";
				}

				sort( $stringList );

				echo implode( "", $stringList );

				?>
			</div>
			<?

			$filters = getFiltersByKeywords( preg_split( '/\s/', $_POST[ "keywordList" ],
				-1, PREG_SPLIT_NO_EMPTY ));

			if( count( $filters ) > 0 )
			{
				$useFilters = preg_split( '/[\s\,\;]/', $_auth[ "useObjFilters" ],
					64, PREG_SPLIT_NO_EMPTY );

				?>
				<div class="sep caption"><?= _FILTER_OPTIONS ?>:</div>
				<div class="container2 notsowide">
					<?

					foreach( $filters as $filter )
					{
						$filterName = getFilterName( $filter );

						?>
						<div>&bull; <?= $filterName ?>
						<?

						if( in_array( $filter, $useFilters ))
						{
							?>
							<span class="error">-- <?= _SUBMIT_DISABLED_FILTERS ?></span>
							<?

							$allowContinue = false;
						}

						?>
						</div>
						<?
					}

					?>
				</div>
				<?
			}

			if($_cmd[1] == "oekaki")
			{
				$oekakiSession = $_auth["useid"];
				$baseFilename = preg_replace('/\..+$/', "",
					findNewestFile("files/oekakitemp/".$oekakiSession."-*", ""));

				if(file_exists($baseFilename.".png")) {
					?>
					<div class="sep caption"><?=_SUBMIT_OEKAKI ?>:</div>
					<table><tr><td>
					<div class="container2">
						<?=getIMG(url().$baseFilename.".png")?>
						<?
						if(file_exists($baseFilename.".pch") || file_exists($baseFilename.".oeb")) {
							?>
							<div class="sep"><?=_SUBMIT_OEKAKI_ANIM ?></div>
							<?
						}
						?>
					</div>
					</td></tr></table>
					<?
				}
			}
			?>

			<div><br /></div>
			<div>
				<input class="submit" type="submit" name="backToStep1"
					value="<?=_CHANGE." "._TITLE."/"._COMMENT ?>" />
				<input class="submit" type="submit" name="backToStep2"
					value="<?=_CHANGE." "._KEYWORDS ?>" />
				&nbsp;
				<input class="submit" type="submit" name="submit"
					<?= $allowContinue ? "" : 'disabled="disabled"' ?>
					value="<?= $allowContinue ? _CONTINUE : _SUBMIT_CANNOT_CONTINUE ?>" />
			</div>
		</div>
		<?
		break;

	case 4: // Step 4 -- IV. Upload files
		?>
		<div class="header"><div class="header_title"><?=_SUBMIT_TITLE ?>
		<?
		if($_cmd[1] == "oekaki") {
			$oekakiSession = $_auth[ "useid" ];//getSessionId();
			$baseFilename = preg_replace('/\..+$/', "",
				findNewestFile("files/oekakitemp/".$oekakiSession."-*", ""));

			if(file_exists($baseFilename.".png")) {
				?>
				<div class="subheader"><?=_OEKAKI ?></div>
				</div></div>
				<div class="container">
				<div><?=_SUBMIT_OEKAKI ?>:</div>
				<div class="sep">
				<?=getIMG(url().$baseFilename.".png")?>
				</div>
				<?
				if(file_exists($baseFilename.".pch") || file_exists($baseFilename.".oeb")) {
					?>
					<div class="sep"><?=_SUBMIT_OEKAKI_ANIM ?></div>
					<?
				}
			}
		}
		else {
			// TODO: move text to strings_??.php
			?>
			<div class="subheader"><?=_SUBMIT_SUBTITLE4 ?></div>
			</div></div>
			<div class="container">
			<?

			if( $_POST[ "type" ] == "1" )
			{
				?>
				<div class="caption"><?=_FILE ?>:</div>
				<div><input accept="text/plain, image/gif, image/jpeg, image/png, shockwave/flash"
					onchange="make_visible('preview_container'); show_preview_image('previewx', 'preview_message', this.value)"
					name="submission" size="79" type="file" /></div>
				<? if(isset($uploadError)) notice($uploadError); ?>
				<table><tr><td>
				<div style="display: none" id="preview_container">
					<div class="sep caption"><?=_PREVIEW ?>:</div>
					<div class="container2 a_center">
						<img alt="preview" style="display: none" id="previewx" src="" />
						<div id="preview_message"><?=_SUBMIT_SELECT_FILE ?></div>
					</div>
				</div>
				</td></tr></table>
				<?
			}
			
			if( $_POST[ "type" ] == "10" )
			{
				?>
				<div class="caption"><?=_FILE ?>:</div>
				<div><input accept="video/mp4, video/x-ms-wmv" name="submission" size="79" type="file" /><br><?= _SUBMIT_SUBTITLE5 ?></div>
				<? if(isset($uploadError)) notice($uploadError); ?>
				<table><tr><td>

				</td></tr></table>
				<?
			}

			?>
			<div class="sep caption"><?=_THUMBNAIL ?>:</div>
			<div><input accept="image/gif, image/jpeg, image/png"
				onchange="make_visible('preview_container_thumb'); show_preview_image('previewx_thumb', 'preview_message_thumb', this.value)"
				name="thumb" size="79" type="file" /></div>
			<? if(isset($uploadErrorThumb)) notice($uploadErrorThumb); ?>
			<table><tr><td>
			<div style="display: none" id="preview_container_thumb">
				<div class="sep caption"><?=_THUMB_PREVIEW ?>:</div>
				<div class="container2 a_center">
					<img alt="preview" style="display: none" id="previewx_thumb" src="" />
					<div id="preview_message_thumb"><?=_SUBMIT_SELECT_FILE ?></div>
				</div>
			</div>
			</td></tr></table>

			<div class="clear">&nbsp;</div>
			<?

			if( $_POST[ "type" ] == "2" )
			{
				?>
				<div class="caption"><?=_TEXT ?>:</div>
				<? if(isset($uploadError)) notice($uploadError); ?>
				<?

				iefixStart();

				$commentName = "textfile";
				$commentDefault = isset( $_POST[ "textfile" ]) ? $_POST[ "textfile" ] : "";
				$commentRows = 16;
				$commentNoOptions = true;

				include( INCLUDES."mod_comment.php" );

				?>
				<div class="clear">&nbsp;</div>
				<?

				iefixEnd();
			}

			if( $_POST[ "type" ] == "1" )
			{
				?>
				<div class="sep">
					<?= _SUBMIT_THUMB_NOT_REQUIRED ?>
				</div>
				<?
			}
			if( $_POST[ "type" ] == "10" )
			{
				?>
				<div class="sep">
					<?= _SUBMIT_THUMBNAIL_REQ ?>
				</div>
				<?
			}
		}

		?>
		<div class="sep">
			<input class="submit" type="submit" name="backToStep3"
				value="<?=_BACK ?>" />
			&nbsp;
			<input class="submit" type="submit" name="submit"
				value="<?=_CONTINUE ?>" />
		</div>
		</div>
		<?
		break;
}

?>
</form>
