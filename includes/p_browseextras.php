<?

if( !isExtras() )
{
	include(INCLUDES."p_notfound.php");
	return;
}

$_documentTitle = _SUBMIT_TYPE_EXTRA;

?>
<div class="header">
	<div class="header_title">
		<?= $_documentTitle ?>
	</div>
</div>

<div class="container">
	<?

	include_once( INCLUDES."gallery.php" );

	// Create parameters to pass to showThumbnails.

	$showArray = array(
		"isExtras" => true,

		"select" => "SELECT `extras`.* FROM `extras` ".
			"LEFT JOIN `clubs` ON(`objForClub` = `cluid`) " );

	showThumbnails( $showArray );

	?>
</div>
