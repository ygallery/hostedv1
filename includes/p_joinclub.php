<? // adds the current users to the $_cmd[1] club's members list

if($_auth["useid"] && !$_config["readOnly"]) {
	// check if this club exists
	$cluid = intval($_cmd[1]);
	$result = sql_query("SELECT `cluid`,`cluRequireAccept`,`cluCreator` FROM `clubs`,`cluExtData` WHERE `cluEid` = `cluid` AND `cluid` = '$cluid' LIMIT 1");
	if($cluData = mysql_fetch_assoc($result)) {
		// check if the user is already a member of this club
		$result = sql_query("SELECT `useCid` FROM `useClubs` WHERE `useCmember` = '".$_auth["useid"]."' AND `useCclub` = '$cluid' LIMIT 1");
		if($memData = mysql_fetch_assoc($result)) { // a member already? remove them from the club
			sql_query("DELETE FROM `useClubs` WHERE `useCid` = '".$memData["useCid"]."' LIMIT 1");
		}
		else { // not a member? add them to the club's members list (probably pending)
			$pending = $cluData["cluRequireAccept"];
			sql_query("INSERT INTO `useClubs`(`useCclub`,`useCmember`,`useCpending`) ".
				"VALUES('$cluid','".$_auth["useid"]."','$pending')");
			// notify the club's owner about the new member
			addUpdate(updTypeMessageClubMember, $cluData["cluCreator"], $cluid, $_auth["useid"]);
		}
	}
}
// also add the club to the watch list, if not already watching
redirect(url("watchclub/".intval($_cmd[1]), array("disableUnwatch" => "yes")));

?>