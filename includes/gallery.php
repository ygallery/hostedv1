<?

// -----------------------------------------------------------------------------
// The function shows a table of thumbnails in the current div. All parameters
// are passed with an array:
//
// "quickSearch" => if true, this will enable quick-search through the siblings
//     of the currently chosen keyword;
//
// "isFavorites" => is this a favorites gallery page? (in this case thumbnails
//     will be sorted depending on the favoriting date instead of submission's
//     revision date);
//
// "showDeleted" => if true, deleted and pending submissions will be also
//     displayed;
//
// "select" => the SELECT query until the WHERE part;
//
// "where" => the WHERE part of the MySQL query;
//
// "maxcols" => maximum number of columns in the thumbs table (default 4);
//
// "limit" => force certain limit (e.g. 8, 12, 24, 48);
//
// "order" => force certain order (e.g. 0, 1, 2, 3);

function showThumbnails( $params )
{
	global $_config, $_cmd, $_auth;

	if( !isset( $params[ "quickSearch" ]))
		$params[ "quickSearch" ] = false;

	if( !isset( $params[ "isFavorites" ]))
		$params[ "isFavorites" ] = false;

	if( !isset( $params[ "showDeleted" ]))
		$params[ "showDeleted" ] = false;

	if( !isset( $params[ "countDisabled" ]))
		$params[ "countDisabled" ] = false;

	if( !isset( $params[ "select" ]))
		$params[ "select" ] = "SELECT * FROM `objects`";

	if( !isset( $params[ "where" ]))
		$params[ "where" ] = "1";

	if( !isset( $params[ "having" ]))
		$params[ "having" ] = "";

	if( !isset( $params[ "maxcols" ]))
		$params[ "maxcols" ] = 4;

	if( !isset( $params[ "isExtras" ]))
		$params[ "isExtras" ] = false;

	if( !isset( $params[ "disableFiltering" ]))
		$params[ "disableFiltering" ] = false;

	if( !isset( $params[ "onDisplayFiltering" ]))
		$params[ "onDisplayFiltering" ] = false;

	if( !isset( $params[ "sortById" ]))
		$params[ "sortById" ] = "";

	//if( !isset( $params[ "noMostFaved" ]))
		$params[ "noMostFaved" ] = false;

	if( !isLoggedIn() )
	{
		if( $params[ "where" ] == "1" )
		{
			$params[ "where" ] = "`objGuestAccess` = '1'";
		}
		else
		{
			$params[ "where" ] = "(".$params[ "where" ].") AND `objGuestAccess` = '1'";
		}
	}

	// Define the sorting method. There are 5 sorting methods:
	//
	// 0 - newest first
	// 1 - oldest first
	// 2 - most viewed
	// 3 - most faved
	// 4 - random
	// 5 - relevant

	if( isset( $params[ "order" ]))
	{
		$order = $params[ "order" ];
	}
	else
	{
		if( $_cmd[ 0 ] == "search" )
		{
			$order = isset( $_GET[ "order" ]) ? intval( $_GET[ "order" ]) :
				( isset( $_COOKIE[ "yGalOrder" ]) ?
				intval( $_COOKIE[ "yGalOrder" ]) : 0 );
		}
		else
		{
			$order = isset( $_GET[ "order" ]) ? intval( $_GET[ "order" ]) :
				( isset( $_COOKIE[ "yGalOrderSearch" ]) ?
				intval( $_COOKIE[ "yGalOrderSearch" ]) : 0 );
		}
	}

	if( $params[ "noMostFaved" ] && ( $order == 2 || $order == 3 ))
	{
		$order = 0;
	}

	if( $params[ "isExtras" ] && $order == 2 )
	{
		$order = 0;
	}

	// Disable sorting by favs and views for the searching engine because it's too slow :(

	if( $_cmd[ 0 ] == "search" && $order != 0 && $order != 1)
	{
		$order = 0;
	}

	$order1 = $order;

	// Disable sorting by popularity for favourites because it's too slow :(

	if( $_cmd[ 0 ] == "favourites" && $order1 != 0 && $order1 != 1 )
	{
		$order1 = 0;
	}

	switch( $order1 )
	{
		case 1: // Oldest first
		{
			if( $params[ "sortById" ])
			{
				$orderString = "ORDER BY ".$params[ "sortById" ];
			}
			else
			{
				$orderString = $params[ "isFavorites" ] ?
					"ORDER BY `favSubmitDate`" :
					"ORDER BY `objSubmitDate`";
			}

			break;
		}

		case 2: // By popularity
		{
			$orderString = "ORDER BY `objPopularity` DESC";

			/*
			$params[ "select" ] = str_replace(
				"FROM `objects`",
				"FROM `objects` USE INDEX(`objPopularity`)",
				$params[ "select" ]);
			*/

			break;
		}

		default: // Newest first
		{
			if( $params[ "sortById" ])
			{
				$orderString = "ORDER BY ".$params[ "sortById" ]." DESC";
			}
			else
			{
				$orderString = $params[ "isFavorites" ] ?
					"ORDER BY `favSubmitDate` DESC" :
					"ORDER BY `objSubmitDate` DESC";
			}
		}
	}

	// Define the amount of thumbnails to show. The default value is 12.

	if( isset( $params[ "limit" ]))
	{
		$limit = $params[ "limit" ];
	}
	else
	{
		$limit = isset( $_GET[ "limit" ]) ? intval( $_GET[ "limit" ]) :
			( isset( $_COOKIE[ "yGalLimit" ]) ? intval( $_COOKIE[ "yGalLimit" ]) : 12 );

		if( $limit != 8 && $limit != 12 && $limit != 24 && $limit != 48 )
			$limit = 12;
	}

	// All pages except the front page "remember" the new
	// sorting/limiting settings.

	if( $_cmd[ 0 ] != "" )
	{
		if( !$params[ "noMostFaved" ])
		{
			if( $_cmd[ 0 ] == "search" )
			{
				setcookie( "yGalOrder", $order, strtotime( "+9 years" ), "/",
					".".$_config[ "galRoot" ]);
			}
			else
			{
				setcookie( "yGalOrderSearch", $order, strtotime( "+9 years" ), "/",
					".".$_config[ "galRoot" ]);
			}
		}

		setcookie( "yGalLimit", $limit, strtotime( "+9 years" ), "/",
			".".$_config[ "galRoot" ]);
	}

	// Define the current offset.

	$offset = isset( $_GET[ "offset" ]) ? intval( $_GET[ "offset" ]) : 0;

	if( $offset < 0 )
		$offset = 0;

	if( !$params[ "disableFiltering" ])
	{
		// If the user isn't mature, hide mature submissions.

		applyObjFilters( $params[ "where" ]);

		if( isset( $params[ "folderWhere" ]))
		{
			applyObjFilters( $params[ "folderWhere" ]);
		}

		if( isset( $params[ "folderCalcWhere" ]))
		{
			applyObjFilters( $params[ "folderCalcWhere" ]);
		}

		if( !$params[ "showDeleted" ])
		{
			$params[ "where" ] = "(".$params[ "where" ].") ".
				"AND `objDeleted` = '0' AND `objPending` = '0'";

			if( isset( $params[ "folderWhere" ]))
			{
				$params[ "folderWhere" ] = "(".$params[ "folderWhere" ].") ".
					"AND `objDeleted` = '0' AND `objPending` = '0'";
			}

			if( isset( $params[ "folderCalcWhere" ]))
			{
				$params[ "folderCalcWhere" ] = "(".$params[ "folderCalcWhere" ].") ".
					"AND `objDeleted` = '0' AND `objPending` = '0'";
			}
		}
	}

	$totalFolders = 0;
	$gallery = array();

	if( isset( $params[ "folderParent" ]))
	{
		$gallery[ -1 ] = -1;
		$limit--;
	}

	if( isset( $params[ "folderSelect" ]))
	{
		$query = $params[ "folderSelect" ]." WHERE ".$params[ "folderWhere" ].
			" GROUP BY `folid` ORDER BY `folName` LIMIT $offset, $limit";

		$result = sql_query( $query );

		while( $folData = mysql_fetch_assoc( $result ))
		{
			$gallery[ -$folData[ "folid" ]] = $folData;
		}

		$query = $params[ "folderSelect" ]." WHERE ".$params[ "folderWhere" ].
			" GROUP BY `folid`";

		$result = sql_query( $query );

		$totalFolders = mysql_num_rows( $result );

		$offset -= $totalFolders;
	}

	// Query all the currently visible thumbnails and store the data in the
	// $gallery array.

	if( $params[ "countDisabled" ])
	{
		$selectCmd = $params[ "select" ];
	}
	else
	{
		$selectCmd = preg_replace( '/^SELECT/', "SELECT SQL_CALC_FOUND_ROWS", $params[ "select" ]);
	}

	$tmpOffset = $offset < 0 ? 0 : $offset;
	$tmpLimit = $offset < 0 ? ( $limit + $offset ) : $limit;

	$limit1 = $limit;

	if( $params[ "countDisabled" ])
	{
		$limit1++;
	}

	$query = "$selectCmd WHERE ".$params[ "where" ]." ".
		$params[ "having" ]." $orderString LIMIT $tmpOffset, $limit1";

	/*
	if( $_auth[ "useid" ] == 7 )
	{
		echo $query;
	}
	*/

	$result = sql_query( $query );

	while( $objData = mysql_fetch_assoc( $result ))
	{
		$gallery[ $objData[ "objid" ]] = $objData;
	}

	// Calculate the total amount of submissions that's possible to browse
	// through with the current options.

	if( $params[ "countDisabled" ])
	{
		$totalCount = $offset + $limit;

		if( count( $gallery ) > $limit )
		{
			$totalCount++;
		}
	}
	else
	{
		$result = sql_query( "SELECT FOUND_ROWS()" );

		$totalCount = $totalFolders + mysql_result( $result, 0 );
	}

	// Generate the navigation bar.

	if( $_cmd[ 0 ] == "" )
	{
		$galleryNavs = "";
	}
	else
	{
		iefixStart();

		ob_start();

		$tmpOffset = isset( $_GET[ "offset" ]) ? intval( $_GET[ "offset" ]) : 0;

		navControls( $tmpOffset, $limit, $totalCount );

		?>
		<div class="a_center">
			<?
			if( $params[ "quickSearch" ] )
			{
				?>
				<form action="<?=url("search") ?>" method="get">
				<?

				foreach( $_GET as $key => $value )
					if( $key != "keywordList" && $key != "offset" )
						echo '<input name="'.htmlspecialchars( $key ).
							'" type="hidden" value="'.htmlspecialchars( $value ).'" />';

				?>
				<select name="keywordList" onchange="this.form.submit();">
				<option value=""></option>
				<option value=""><?=_SEARCH ?></option>
				<?

				// By default, select the first root keyword group.

				$result = sql_query( "SELECT `keyid` FROM `keywords` ".
					"WHERE `keySubcat` = '0' ORDER BY `keyWord` LIMIT 1" );

				if( mysql_num_rows( $result ))
					$mainSubcat = mysql_result( $result, 0 );
				else
					$mainSubcat = 0;

				if( isset( $_GET[ "keywordList" ]))
				{
					// If it's already a search request then see what keyword groups are
					// involved in the search and allow selection of siblings inside
					// those groups.

					$list = preg_split( '/\s/', $_GET[ "keywordList" ], -1, PREG_SPLIT_NO_EMPTY );
				}
				else
					$list = array( $mainSubcat );

				$first1 = true;
				$where1 = "`keyid` IN(";

				foreach( $list as $keyid )
				{
					$keyid = intval( $keyid );

					if( $keyid == 0 )
						continue;

					$result = sql_query( "SELECT `keySubcat` FROM `keywords` ".
						"WHERE `keyid` = '$keyid' LIMIT 1");

					if( mysql_num_rows( $result ) > 0 )
					{
						$keySubcat = mysql_result( $result, 0 );
						$where1 .= ( $first1 ? "" : "," )."'$keySubcat'";
						$first1 = false;
					}
				}

				if( $mainSubcat > 0 )
					$where1 .= ( $first1 ? "" : "," )."'$mainSubcat'";

				$where1 .= ")";
				$limit1 = 5;

				$result = sql_query( "SELECT `keyid`,`keyWord` FROM `keywords` ".
					"WHERE $where1 ORDER BY `keyWord` LIMIT $limit1" );

				while( $keyData = mysql_fetch_assoc( $result ))
				{
					$keyData[ "keyWord" ] = trim( preg_replace( '/^.*\|/', "",
						$keyData[ "keyWord" ]));

					if( preg_match( '/\@$/', $keyData[ "keyWord" ]))
						continue;

					?>
					<optgroup label="<?=$keyData["keyWord"]?>">
					<?

					$result2 = sql_query( "SELECT * FROM `keywords` ".
						"WHERE `keySubcat` = '".$keyData[ "keyid" ]."' ORDER BY `keyWord`" );

					while( $rowData = mysql_fetch_assoc( $result2 ))
					{
						$rowData[ "keyWord" ] = trim( preg_replace( '/^.*\|/', "",
							$rowData[ "keyWord" ]));

						if( preg_match( '/\@$/', $rowData[ "keyWord" ]))
							continue;

						?>
						<option <?= ( isset( $_GET[ "keywordList" ]) &&
							$_GET[ "keywordList" ] == $rowData[ "keyid" ] ?
							'selected="selected"' : "")?>
							value="<?= $rowData[ "keyid" ]?>"><?= htmlspecialchars( $rowData[ "keyWord" ]) ?></option>
						<?
					}

					?>
					</optgroup>
					<?
				}

				?>
				</select>
				</form>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				<?
			}

			?>
			<form action="<?=url( "." )?>" method="get">
			<?

			foreach( $_GET as $key => $value )
				if( $key != "order" && $key != "limit" && $key != "offset" )
					echo '<input name="'.htmlspecialchars( $key ).
						'" type="hidden" value="'.htmlspecialchars( $value ).'" />';

			?>
			<select name="order">
				<option <?= $order == 0 ? 'selected="selected"' : "" ?> value="0"><?=_NEWEST_FIRST ?></option>
				<option <?= $order == 1 ? 'selected="selected"' : "" ?> value="1"><?=_OLDEST_FIRST ?></option>
				<?

				if( $_cmd[ 0 ] != "search" && $_cmd[ 0 ] != "favourites" )
				{
					if( !$params[ "noMostFaved" ])
					{
						if( !$params[ "isExtras" ])
						{
							?>
							<option <?= $order == 2 ? 'selected="selected"' : "" ?> value="2"><?=_CLUB_POPULAR ?></option>
							<?
						}

						/*
						?>
						<option <?= $order == 3 ? 'selected="selected"' : "" ?> value="3"><?=_MOST_VIEWED ?></option>
						<?
						*/
					}

					/*
					?>
					<!--<option <?= $order == 4 ? 'selected="selected"' : "" ?> value="4"><?=_RANDOM ?></option>-->
					<?

					if( $_cmd[ 0 ] == "search" )
					{
						?>
						<option <?= $order == 5 ? 'selected="selected"' : "" ?> value="5"><?=_SEARCH_RELEVANT ?></option>
						<?
					}
					*/
				}

				?>
			</select>
			<?

			$tmpLimit = $limit;

			if( isset( $params[ "folderParent" ]))
			{
				$tmpLimit++;
			}

			?>
			<select name="limit">
				<option <?= $tmpLimit == 8 ? 'selected="selected"' : "" ?> value="8"><?=fuzzy_number( 8 )?></option>
				<option <?= $tmpLimit == 12 ? 'selected="selected"' : "" ?> value="12"><?=fuzzy_number( 12 )?></option>
				<option <?= $tmpLimit == 24 ? 'selected="selected"' : "" ?> value="24"><?=fuzzy_number( 24 )?></option>
				<option <?= $tmpLimit == 48 ? 'selected="selected"' : "" ?> value="48"><?=fuzzy_number( 48 )?></option>
			</select>

			<input class="submit" type="submit" value="<?= _UPDATE ?>"
				style="vertical-align: middle" />

			</form>
		</div>
		<?

		$galleryNavs = ob_get_contents();

		ob_end_flush();

		?>
		<div class="hline">&nbsp;</div>
		<?

		iefixEnd();
	}

	$cols = 0;
	$imagesToGo = $limit;

	if( isset( $params[ "folderParent" ]))
		$imagesToGo++;

	$useids = array();

	foreach( $gallery as $objData )
	{
		if( isset( $objData[ "objCreator" ]))
		{
			$useids[] = $objData[ "objCreator" ];
		}
	}

	prefetchUserData( array_unique( $useids ));

	if( $params[ "onDisplayFiltering" ])
	{
		// Prepare user filters array (used below).

		$useFilters = preg_split( '/[^0-9]/', $_auth[ "useObjFilters" ], 63, PREG_SPLIT_NO_EMPTY );
	}

	foreach( $gallery as $objid => $objData )
	{
		if( $objid < 0 )
		{
			// Show a folder icon.

			?>
			<div class="gallery_col">
				<div class="a_center padded mar_bottom<?= $cols < 3 ? " mar_right" : "" ?>">
					<?

					if( isset( $params[ "folderParent" ]))
					{
						$url = url(( $params[ "isExtras" ] ? "extras" : "gallery" )."/".
							strtolower( $params[ "folderParent" ][ "useUsername" ]));

						?>
						<div style="padding-top: 35px;">
							<a href="<?= $url ?>">
								<?= getIMG( urlf()."images/folder_up.png" ) ?><br />
								<b><?= _SET_FOLDER_BACK ?></b>
							</a>
						</div>
						<?
					}
					else
					{
						$url = url(( $params[ "isExtras" ] ? "extras" : "gallery" )."/".
							strtolower( $params[ "folderCreator" ][ "useUsername" ]).
							"/".$objData[ "folIdent" ]);

						?>
						<div style="background: url(<?= urlf()."images/folder.png" ?>) center top no-repeat; height: 140px;">
							<div style="padding-top: 35px">
								<a href="<?= $url ?>"><?= getFolderIcon( $objData[ "folid" ]) ?></a>
							</div>
						</div>
						<div>
							<b><a href="<?= $url ?>"><?= formatText( $objData[ "folName" ], false, true ) ?></a></b>
						</div>
						<?

						if( isset( $params[ "folderCalcWhere" ]))
						{
							$query = $params[ "folderCalc" ]." WHERE ".
								$params[ "folderCalcWhere" ]." AND `objFolder` = '".$objData[ "folid" ]."'";

							$result = sql_query( $query );

							echo "( ".fuzzy_number( mysql_result( $result, 0 ))." )";
						}
					}

					?>
				</div>
			</div>
			<?
		}
		else
		{
			// Show a thumbnail.

			if( $params[ "onDisplayFiltering" ])
			{
				// Check objDeleted, objPending, objMature manually.

				if( $objData[ "objDeleted" ] || $objData[ "objPending" ])
				{
					continue; // Don't show deleted/pending
				}

				if( count( $useFilters ) > 0 )
				{
					$objFilters = preg_split( '/[^0-9]/', $objData[ "objMature" ], 63, PREG_SPLIT_NO_EMPTY );
					$isFiltered = false;

					foreach( $objFilters as $filter )
					{
						if( in_array( $filter, $useFilters ))
						{
							$isFiltered = true;
							break;
						}
					}

					if( $isFiltered )
					{
						continue; // Don't show filtered objects
					}
				}
			}

			$anchor = url( "view/".( $params[ "isExtras" ] ? "e" : "" ).$objData[ "objid" ]);

			if( $params[ "isExtras" ])
			{
				$src = $objData[ "objThumbURL" ];
			}
			elseif( $objData [ "objThumbDefault" ])
			{
				$src = urlf()."images/litthumb.png";
			}
			else
			{
				$src = urlf().applyIdToPath( "files/thumbs/", $objData[ "objid" ])."-".
					preg_replace( '/[^0-9]/', "", $objData[ "objLastEdit" ]).".jpg";
			}

			$objTitle = formatText( $objData[ "objTitle" ]);

			// Do not display "by <artist_name>" in /gallery and /galleryclubs.

			if( $_cmd[ 0 ] != "gallery" && $_cmd[ 0 ] != "galleryclubs" && $_cmd[ 0 ] != "extras" )
			{
				$objTitle .= "<br /> ";

				if( $objData[ "objCollab" ] > 0 )
				{
					$objTitle .= sprintf( _BY_AND,
						getUserLink( $objData[ "objCreator" ]),
						getUserLink( $objData[ "objCollab" ]));
				}
				else
				{
					$objTitle .= sprintf( _BY,
						getUserLink( $objData[ "objCreator" ]));
				}
			}
			else
			{
				if( $objData[ "objCollab" ] > 0 )
				{
					$objTitle .= " <br />".sprintf( _BY_COLLAB_WITH,
						getUserLink( $objData[ "objCollab" ]));
				}
			}

			if( $objData[ "objForUser" ] > 0 && $_cmd[ 0 ] != "gifts" )
			{
				$objTitle .= " <br />".sprintf( _FOR, getUserLink( $objData[ "objForUser" ]));
			}

			// Display "in <club_name>" in /gallery and /galleryclubs if the
			// work is submitted to a club.

			if( $_cmd[ 0 ] == "galleryclubs" ||
				( $_cmd[ 0 ] == "gallery" && $objData[ "objForClub" ] > 0 ))
			{
				$result = sql_query( "SELECT `cluName` FROM `clubs` ".
					"WHERE `cluid` = '".$objData[ "objForClub" ]."'" );

				if( mysql_num_rows( $result ) > 0 )
				{
					$club = '<a href="'.url( "club/".$objData[ "objForClub" ]).'">'.
						mysql_result( $result, 0 ).'</a>';

					$objTitle .= '<br /> '.sprintf( _IN, $club );
				}
			}

			?>
			<div class="gallery_col">
				<div class="a_center padded mar_bottom<?= $cols < 3 ? " mar_right" : "" ?>">
					<a href="<?= $anchor ?>">
						<?= getIMG( $src, 'alt="'.strip_tags( $objTitle ).'" class="thumb'.
						($objData[ "objMature" ] && isLoggedIn() ? " mature" : "" ).
						($objData[ "objPending" ] ? " pending" : "" ).
						($objData[ "objDeleted" ] ? " deleted" : "" ).
						'" width="'.$objData[ "objThumbWidth" ].
						'" height="'.$objData[ "objThumbHeight" ].
						'" title="'.strip_tags( $objTitle ).'"') ?></a>
					<div><?= $objTitle ?></div>
				</div>
			</div>
			<?
		}

		$cols++;

		if( $cols >= $params[ "maxcols" ])
		{
			$cols = 0;

			?>
			<div class="clear">&nbsp;</div>
			<?
		}

		$imagesToGo--;

		if( $imagesToGo <= 0 )
			break;
	}

	if( count( $gallery ) == 0 )
	{
		?>
		<div><?= _NO_SUBMISSIONS ?></div>
		<?
	}

	if( $_cmd[ 0 ] != "" || $params[ "limit" ] == 12 )
	{
		?>
		<div class="hline">&nbsp;</div>
		<?
	}

	// On the front page also show the "More >" link which leads to the
	// global gallery browsing.

	if( $_cmd[ 0 ] == "" && $params[ "limit" ] == 12 )
	{
		?>
		<div class="a_right mar_bottom mar_right">
			<a class="disable_wrapping smalltext" href="<?= url( "browse" )?>">
			<?= _MORE ?>
			<?= getIMG( url()."images/emoticons/nav-next.png" ) ?>
			</a>
		</div>
		<?
	}

	echo $galleryNavs;

	?>
	<div class="clear">&nbsp;</div>
	<?
}
