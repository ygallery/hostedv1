<?
	$_pollMaxOptions = 15;

	include_once(INCLUDES."polling.php");

	if($_cmd[1] == "")
	{
		if( isLoggedIn() )
		{
			redirect( url( "poll/".strtolower( $_auth[ "useUsername" ])));
		}

		$_cmd[2] = $_cmd[1];
		$_cmd[1] = strtolower($_auth["useUsername"]);
	}

	$pollAction = isset($_GET["action"]) ? $_GET["action"] : "";

	$whereGuest = isLoggedIn() ? "" : ( " AND `useGuestAccess` = '1' " );
	$result = sql_query("SELECT * FROM `users`,`useExtData` WHERE `useUsername` = '".addslashes($_cmd[1])."' $whereGuest AND `useEid` = `useid` LIMIT 1");
	if(!$useData = mysql_fetch_assoc($result))
	{
		include(INCLUDES."p_notfound.php");
		return;
	}
	$useUsername = strtolower($useData["useUsername"]);

	$polid = $_cmd[2] != "" ? intval($_cmd[2]) : 0;

	// TODO: separate to pages if there are too many entries
	$entries = array();
	$result = sql_query("SELECT * FROM `polls` WHERE `polCreator` = '".$useData["useid"]."' ORDER BY `polSubmitDate` DESC");
	while($polData = mysql_fetch_assoc($result))
	{
		$entries[] = $polData;
		if($polData["polid"] == $polid)
			$currentEntry = $polData;
	}
	if(!$polid && isset($entries[0]["polid"]))
	{
		$polid = $entries[0]["polid"];
		$currentEntry = $entries[0];
	}
	// if it is a specific poll entry, make sure it exists and belongs to the user
	if($polid)
	{
		$result = sql_query("SELECT `polid` FROM `polls` WHERE `polCreator` = '".$useData["useid"]."' AND `polid` = '$polid' LIMIT 1");
		if(!mysql_num_rows($result))
		{
			include(INCLUDES."p_notfound.php");
			return;
		}
		if(!$_auth["useid"] || $_auth["useid"] == $useData["useid"])
			$alreadyVoted = true;
		else
		{
			$result = sql_query("SELECT `pollVid` FROM `pollVotes`,`pollOptions`,`polls` WHERE `polOPoll` = '$polid' AND `pollVSelected` = `polOid` AND `pollVUser` = '".$_auth["useid"]."' LIMIT 1");
			$alreadyVoted = (mysql_num_rows($result) > 0);
		}
		$daysLeft = round((strtotime($currentEntry["polExpireDate"]) - time()) / 60/60/24);
		if($daysLeft < 0)
		{
			$daysLeft = 0;
			$alreadyVoted = true; // expired
		}
		$currentEntry["polDaysLeft"] = $daysLeft;

		$options = array();
		$result = sql_query("SELECT * FROM `pollOptions` WHERE `polOPoll` = '$polid' ".($alreadyVoted ? "ORDER BY `polOVotes` DESC,`polOid`" : "")." LIMIT ".$_pollMaxOptions);
		while($optData = mysql_fetch_assoc($result))
			$options[$optData["polOid"]] = $optData;
	}

	// only the poll's owner can do special actions (post/edit/delete)
	if($_auth["useid"] != $useData["useid"] && $pollAction != "")
	{
		include(INCLUDES."p_notfound.php");
		return;
	}

	if(isset($_POST["submitVote"]) && !$alreadyVoted)
	{
		foreach($options as $optData)
		{
			if(isset($_POST["option".$optData["polOid"]]))
			{
				sql_query("INSERT INTO `pollVotes`(`pollVUser`,`pollVSelected`) ".
					"VALUES('".$_auth["useid"]."','".$optData["polOid"]."')");
				// count the overall number of votes for this option
				$result = sql_query("SELECT COUNT(*) FROM `pollVotes` WHERE `pollVSelected` = '".$optData["polOid"]."'");
				$count = mysql_result($result, 0);
				sql_query("UPDATE `pollOptions` SET `polOVotes` = '$count' WHERE `polOid` = '".$optData["polOid"]."' LIMIT 1");
			}
		}
		// count the total number of votes
		$result = sql_query("SELECT SUM(`polOVotes`) FROM `pollOptions` WHERE `polOPoll` = '$polid'");
		$totalCount = mysql_result($result, 0);
		sql_query("UPDATE `polls` SET `polVoted` = '$totalCount' WHERE `polid` = '$polid' LIMIT 1");
		markAsRead(updTypeJournalPoll, $polid);
		redirect(url("poll/".$useUsername."/".$polid));
	}

	$_pollUser = 0;

	$_documentTitle = $useData["useUsername"].": "._POLL;
?>

<div class="header">
	<div class="f_right mar_left a_center normaltext">
		<?=getUserAvatar("",$useData["useid"], true)?>
	</div>
	<div class="f_left header_title">
		<?=$useData["useUsername"]?>
		<div class="subheader"><?=_POLL ?></div>
	</div>
	<?
	$active = 8;
	include(INCLUDES."mod_usermenu.php");
	?>
</div>

<div class="container">

	<?=iefixStart()?>
	<div class="f_left leftside"><div class="header"><?
		switch($pollAction) {
			case "post": echo _POLL_NEW_ENTRY; break;
			case "edit": echo _POLL_EDIT_ENTRY; break;
			case "delete": echo _POLL_DELETE_ENTRY; break;
			default:
				echo _POLL;
				// also the New Entry button:
				if($_auth["useid"] == $useData["useid"]) {
					$url = url(".", array("action" => "post"));
					?>
					&nbsp; &nbsp;
					<div class="button normaltext" style="float: none; display: inline; margin-right: -8px" onclick="document.location='<?=$url?>'">
						<a href="<?=$url?>">
						<?=getIMG(url()."images/emoticons/poll.png") ?>
						<?=_POLL_NEW_ENTRY ?>
						</a>
					</div>
					<?
				}
		}
	?></div></div>
	<div class="f_right a_right"><div class="header"><?
		switch($pollAction)
		{
			case "post":
			case "edit":
			case "delete":
				echo _POLL_PAST_ENTRIES;
				break;
			default:
				if($polid)
					echo isset($_GET["replied"]) ? _COMMENTS : _POLL_LEAVE_COMMENT;
		}
	?></div></div>
	<div class="clear">&nbsp;</div>
	<?=iefixEnd()?>

	<div class="leftside">
		<?
			$ableToPost = ($_auth["useid"] == $useData["useid"]);

			if($ableToPost && isset($_POST["postNewEntry"])) {
				$subject = addslashes($_POST["subject"]);
				$comment = addslashes($_POST["comment"]);
				$expire = intval($_POST["expire"]); // days
				if($expire == 0) $expire = 30;
				if($expire > 999) $expire = 999;
				sql_query("INSERT INTO `polls`(`polCreator`,`polSubmitDate`,`polExpireDate`,`polSubject`,`polComment`) ".
					"VALUES('".$_auth["useid"]."',NOW(),DATE_ADD(NOW(),INTERVAL $expire DAY),'$subject','$comment')");
				$polidNew = mysql_insert_id();
				for($i = 1; $i <= $_pollMaxOptions; $i++) {
					if(isset($_POST["option".$i]) && trim($_POST["option".$i]) != "") {
						sql_query("INSERT INTO `pollOptions`(`polOPoll`,`polOOption`) ".
							"VALUES('$polidNew','".addslashes($_POST["option".$i])."')");
					}
				}
				// notify watchers about the new poll
				addUpdateToWatchers( updTypeJournalPoll, $_auth[ "useid" ], $polidNew );
				redirect(url("poll/".strtolower($_auth["useUsername"])));
			}

			if($ableToPost && isset($_POST["editOldEntry"])) {
				$subject = addslashes($_POST["subject"]);
				$comment = addslashes($_POST["comment"]);
				$expire = intval($_POST["expire"]); // days
				if($expire == 0) $expire = 30;
				sql_query("UPDATE `polls` SET `polSubmitDate` = NOW(), `polExpireDate` = DATE_ADD(NOW(),INTERVAL $expire DAY), `polSubject` = '$subject', `polComment` = '$comment' WHERE `polid` = '$polid' LIMIT 1");
				$editOptions = array();
				$i = 1;
				foreach($options as $optData) {
					$editOptions[$i] = $optData;
					$i++;
				}
				for($i = 1; $i <= $_pollMaxOptions; $i++) {
					if(isset($_POST["option".$i])) {
						if(isset($editOptions[$i])) { // edit old option
							if(trim($_POST["option".$i]) != "") // update old option
								sql_query("UPDATE `pollOptions` SET `polOOption` = '".addslashes($_POST["option".$i])."' ".
									"WHERE `polOid` = '".$editOptions[$i]["polOid"]."' LIMIT 1");
							else // delete old option
								sql_query("DELETE FROM `pollOptions` WHERE `polOid` = '".$editOptions[$i]["polOid"]."' LIMIT 1");
						}
						elseif(trim($_POST["option".$i]) != "") // insert new option
							sql_query("INSERT INTO `pollOptions`(`polOPoll`,`polOOption`) ".
								"VALUES('$polid','".addslashes($_POST["option".$i])."')");
					}
				}
				// notify watchers about the poll update
				addUpdateToWatchers( updTypeJournalPoll, $_auth[ "useid" ], $polid );
				redirect(url("poll/".strtolower($_auth["useUsername"])."/".$polid));
			}

			if($ableToPost && $pollAction == "delete") {
				sql_query("DELETE FROM `polls` WHERE `polid` = '$polid' LIMIT 1");
				sql_query("DELETE FROM `pollOptions` WHERE `polOPoll` = '$polid' LIMIT 1");
				sql_query("DELETE FROM `updates` WHERE `updType` = '".updTypeJournalPoll."' AND `updObj` = '$polid'");
				redirect(url("poll/".strtolower($_auth["useUsername"])));
			}

			if($ableToPost && ($pollAction == "post" || $pollAction == "edit")) {
				?>
				<div class="mar_bottom">
					<form action="<?=url(".")?>" method="post">
					<div class="caption"><?=_NEWS_SUBJECT ?>:</div>
					<div><input class="notsowide largetext" name="subject" type="text"
						<?=$pollAction == "edit" ? 'value="'.htmlspecialchars( $currentEntry["polSubject"]).'"' : "" ?> />
					</div>
					<div class="caption"><?=_COMMENT ?>:</div>
					<div>
						<?iefixStart()?>
						<?
						$commentDefault = $pollAction == "edit" ? $currentEntry["polComment"] : "";
						$commentNoOptions = true;
						include (INCLUDES."mod_comment.php");
						?>
						<div class="clear">&nbsp;</div>
						<?iefixEnd()?>
					</div>
					<div class="sep caption"><?=_OEKAKI_OPTIONS ?>:</div>
					<div class="container2">
						<table class="notsowide">
						<?
						if($pollAction == "edit") {
							$editOptions = array();
							$i = 1;
							foreach($options as $optData) {
								$editOptions[$i] = $optData;
								$i++;
							}
						}
						for($i = 1; $i <= $_pollMaxOptions; $i++) {
							?>
							<tr>
								<td class="a_right"><?=$i?>.</td>
								<td class="wide"><input type="text" name="option<?=$i?>" class="notsowide"
									<?=isset($editOptions[$i]) ? 'value="'.htmlspecialchars($editOptions[$i]["polOOption"]).'"' : "" ?> /></td>
							</tr>
							<?
						}
						?>
						</table>
						<div class="sep">
							<?=_POLL_EXPIRE ?>
							<input type="text" class="largetext" name="expire" size="2"
								value="<?=$pollAction == "edit" ? $currentEntry["polDaysLeft"] : "30" ?>" />
							<?=_POLL_EXPIRE_UNITS ?>
						</div>
					</div>
					<div class="sep">
					</div>
					<div class="sep">
						<button class="submit" name="<?=$pollAction == "edit" ? "editOldEntry" : "postNewEntry" ?>" type="submit">
							<?=getIMG(url()."images/emoticons/checked.png") ?>
							<?=$pollAction == "edit" ? _SAVE_CHANGES : _POLL_POST_ENTRY ?>
						</button>
					</div>
					</form>
				</div>
				<?
			}

			if($pollAction == "" && isset($currentEntry)) {
				?>
				<div class="container2 mar_bottom">
					<form action="<?=url(".")?>" method="post">
					<div class="mar_bottom largetext">
						<b><?=formatText($currentEntry["polSubject"])?></b>
					</div>
					<?

					if( trim( $currentEntry["polComment"]) != "" )
					{
						?>
						<div class="mar_bottom">
							<?=formatText($currentEntry["polComment"])?>
						</div>
						<?
					}

					showPollOptions($options, $alreadyVoted, $currentEntry["polVoted"],
						$currentEntry["polid"]);

					if(!$alreadyVoted) {
						?>
						<div class="sep">
							<button class="submit" name="submitVote" type="submit">
								<?=getIMG(url()."images/emoticons/checked.png") ?>
								<?=_VOTE ?>
							</button>
						</div>
						<?
					}
					?>
					</form>
				</div>
				<?
				if($_auth["useid"] == $currentEntry["polCreator"]) {
					$urlEdit = url(".", array("action" => "edit"));
					$urlDelete = url(".", array("action" => "delete"));
					?>
					<div class="mar_bottom" style="margin-top: -8px">
						<?iefixStart()?>
						<div class="button" style="float: right" onclick="if(confirm('<?=_ARE_YOU_SURE?>')) document.location='<?=$urlDelete?>'">
							<a href="<?=$urlDelete?>" onclick="return false">
							<?=getIMG(url()."images/emoticons/delete.png") ?>
							<?=_DELETE ?>
							</a>
						</div>
						<div class="button" style="float: right" onclick="document.location='<?=$urlEdit?>'">
							<a href="<?=$urlEdit?>">
							<?=getIMG(url()."images/emoticons/edit.png") ?>
							<?=_EDIT ?>
							</a>
						</div>
						<div class="clear">&nbsp;</div>
						<?iefixEnd()?>
					</div>
					<?
				}
			}

		if($pollAction != "") {
			// if editing/posting, switch to the right side here
			// and show the past entries there
			?>
			</div>
			<div class="rightside" style="margin-top: -1px">
			<?
		}
		else {
			?>
			<div class="header"><?=_POLL_PAST_ENTRIES ?></div>
			<?
		}
		?>
		<div class="container2 mar_bottom">
			<?
			$found = false;
			foreach($entries as $entry) {
				// do not display the current entry on the list
				// disabled - because it looks confusing
				//if($entry["polid"] == $currentEntry["polid"]) continue;
				iefixStart();
				?>
				<div class="f_left mar_bottom">
					<a href="<?=url("poll/".$useUsername."/".$entry["polid"])?>">
					<?=getIMG(url()."images/emoticons/poll.png") ?>
					<?=formatText($entry["polSubject"]) ?>
					</a>
				</div>
				<div class="f_right mar_bottom">
					<?=gmdate($_auth["useDateFormat"], applyTimezone(strtotime($entry["polSubmitDate"]))) ?>
				</div>
				<div class="clear">&nbsp;</div>
				<?
				iefixEnd();
				$found = true;
			}
			if(!$found)
				echo _POLL_NONE_FOUND;
			?>
		</div>
		<div class="clear">&nbsp;</div>
	</div>

	<?
	if($pollAction == "") {
		?>
		<div class="rightside" style="margin-top: -1px">
		<?
		if(isset($currentEntry)) {
			unset($commentRows);
			unset($commentWide);
			unset($commentName);
			include_once(INCLUDES."comments.php");
			showAllComments($currentEntry["polid"], "pol", false, true, true, false);
		}
		?>
		</div>
		<?
	}
	?>
	<div class="clear">&nbsp;</div>
</div>
