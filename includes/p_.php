<?

$_newsPerPage = 5;

$_documentTitle = _WHATSNEW;

?>
<div class="header">
	<div class="header_title">
		<?

		$ableToPost = atLeastSModerator();

		if($ableToPost)
		{
			if(isset($_POST["subject"]) && isset($_POST["comment"]))
			{
				$noEmoticons = isset($_POST["commentNoEmoticons"]) ? 1 : 0;
				$noSig = isset($_POST["commentNoSig"]) ? 1 : 0;
				$noBBCode = isset($_POST["commentNoBBCode"]) ? 1 : 0;
				sql_query("INSERT INTO `news`(`newCreator`,`newSubmitDate`,`newSubject`,`newComment`,`newNoEmoticons`,`newNoSig`,`newNoBBCode`) ".
					"VALUES('".$_auth["useid"]."',NOW(),'".addslashes($_POST["subject"])."','".addslashes($_POST["comment"])."','$noEmoticons','$noSig','$noBBCode')");
				redirect(url("."));
			}
			?>
			<div class="button normaltext" id="postNewsButton"
				onclick="<?
					if($_isIE)
						echo "document.location='".url(".", array("postNews" => "yes"))."'";
					else
						echo "make_invisible('postNewsButton'); make_visible('postNews');";
				?>" style="float: right<?=($_isIE && isset($_GET["postNews"])) ? "; display: none" : "" ?>"><?=_NEWS_POST ?></div>
			<?
		}
		?>
		<?=_WHATSNEW?>
		<div class="subheader"><?=_MAIN_SUBTITLE?></div>
	</div>
	<div class="clear">&nbsp;</div>
</div>

<div class="container">
	<?=iefixStart()?>
	<?

	include_once( INCLUDES."gallery.php" );

	if( !isset($_auth["useSidebarThumbs"]) || $_auth["useSidebarThumbs"])
	{
		?>
		<div class="leftside">
			<div class="header"><?=_MOST_RECENT ?></div>
			<div class="container2" style="padding: 2px">
			<div class="container" style="padding: 2px">
				<?

				$where = "`objPending` = '0' AND `objDeleted` = '0'";

				if( !isLoggedIn() )
				{
					$where .= " AND `objGuestAccess` = '1'";
				}

				applyObjFilters( $where );

				$objList = array();

				$result = sql_query( "SELECT DISTINCT `objCreator` FROM `objects` WHERE $where ".
					"ORDER BY `objSubmitDate` DESC LIMIT 48" );

				while( $objData = mysql_fetch_row( $result ))
				{
					$creator = $objData[ 0 ];

					$result2 = sql_query( "SELECT `objid` FROM `objects` WHERE ($where) ".
						"AND `objCreator` = '$creator' ORDER BY `objSubmitDate` DESC LIMIT 1" );

					if( $obj2Data = mysql_fetch_row( $result2 ))
					{
						$objList[] = $obj2Data[ 0 ];
					}
				}

				$objList = array_unique( $objList );

				showThumbnails( array(
					"where" => "`objid` IN('".implode( "','", $objList )."')",
					"maxcols" => "2",
					"limit" => 12,
					"order" => 0 ));

				?>
			</div>
			</div>
		</div>
		<div class="rightside">
		<?
	}
	else
	{
		?>
		<div class="notsowide">
		<?
	}

		if($ableToPost) {
			?>
			<div id="postNews" style="<?=
				($_isIE && isset($_GET["postNews"])) ? "" : "display: none"?>">
				<form action="<?=url(".")?>" method="post">
				<div class="header a_right" style="margin-bottom: -16px"><?=_NEWS_POST ?></div>
				<div class="caption"><?=_NEWS_SUBJECT ?>:</div>
				<div><input class="notsowide largetext" name="subject" type="text" /></div>
				<div class="sep">
					<?
					$commentRows = 18;
					$commentWide = false;
					include(INCLUDES."mod_comment.php");
					?>
				</div>
				<div class="sep">
					<button class="submit" name="sendReply"
						<?=!$commentId ? 'disabled="disabled"' : "" ?>
						onclick="el=get_by_id('<?=$commentId ?>'); if(!el.value){ alert('<?=_BLANK_COMMENT ?>'); return false } else return true"
						type="submit">
						<?=getIMG(url()."images/emoticons/checked.png")?>
						<?=_NEWS_POST ?>
					</button>
				</div>
				</form>
			</div>
			<?
		}
		?>
		<div class="header a_right"><?=_NEWS ?></div>
		<div style="margin-top: -8px">
		<?
		$offset = isset($_GET["newsOffset"]) ? intval($_GET["newsOffset"]) : 0;
		include_once(INCLUDES."comments.php");
		$result = sql_query("SELECT COUNT(*) FROM `news`");
		$totalCount = mysql_result($result, 0);
		$result = sql_query("SELECT * FROM `news` ORDER BY `newSubmitDate` DESC LIMIT $offset,".($_newsPerPage + 1));
		$newsToGo = $_newsPerPage;
		while($rowData = mysql_fetch_assoc($result)) {
			$comData = array();
			$comData["comid"] = 0;
			$comData["comCreator"] = $rowData["newCreator"];
			$comData["comComment"] = $rowData["newComment"];
			$comData["comSubject"] = $rowData["newSubject"];
			$comData["comSubmitDate"] = $rowData["newSubmitDate"];
			$comData["comNoEmoticons"] = $rowData["newNoEmoticons"];
			$comData["comNoSig"] = $rowData["newNoSig"];
			$comData["comNoBBCode"] = $rowData["newNoBBCode"];
			$comData["newid"] = $rowData["newid"];
			showComment($comData, 0);
			$newsToGo--;
			if(!$newsToGo) break;
		}
		?>
		</div>
		<div class="padded a_center">
		<?
		navControls( $offset, $_newsPerPage, $totalCount, "", "", "", "", "newsOffset" );
		?>
		</div>
	</div>
	<div class="clear">&nbsp;</div>
	<?=iefixEnd()?>
</div>
