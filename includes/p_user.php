<?php

$whereGuest = isLoggedIn() ? "" : ( " AND `useGuestAccess` = '1' " );

$result = sql_query("SELECT * FROM `users`,`useExtData` ".
	"WHERE `useUsername` = '".mysql_real_escape_string( $_cmd[ 1 ])."' AND `useEid` = `useid` $whereGuest LIMIT 1");

if( !$useData = mysql_fetch_assoc( $result ))
{
	if( isLoggedIn() && $_cmd[ 1 ] == "" )
	{
		redirect( url( "user/".strtolower( $_auth[ "useUsername" ])));
	}

	// If the user was not found, try finding them through the searchable artists list.

	if( isLoggedIn() )
	{
		redirect( url( "browseartists", array( "searchText" => $_cmd[ 1 ])));
	}
	else
	{
		include(INCLUDES."p_notfound.php");
		return;
	}
}

$useUsername = strtolower( $useData[ "useUsername" ]);

$_documentTitle = $useData[ "useUsername" ];

?>
<div class="header">
	<div class="f_right mar_left a_center normaltext">
		<?= getUserAvatar( "",$useData[ "useid" ], true )?>
	</div>
	<div class="f_left mar_right header_title">
		<?= $useData[ "useUsername" ] ?>
		<div class="subheader">
			<?= $useData[ "useCustomTitle" ] ? formatText( $useData[ "useCustomTitle" ], false, true ) : "<br />"?>
		</div>
	</div>
	<?

	$active = 1;
	include( INCLUDES."mod_usermenu.php" );

	?>
</div>

<div class="container">
<?

iefixStart();

if( $useData[ "useIsBanned" ] || $useData[ "useIsSuspended" ])
{
	?>
	<div class="container2 mar_top largetext">
	<?

	if( $useData[ "useIsBanned" ])
	{
		printf( _ABUSE_BANNED_NOTICE,
			formatText( $useData[ "useSuspendedReason" ]));
	}
	elseif( $useData[ "useIsSuspended" ])
	{
		printf( _ABUSE_SUSPENDED_NOTICE,
			gmdate( $_auth[ "useDateFormat" ], applyTimezone( strtotime( $useData[ "useSuspendedUntil" ]))),
			formatText( $useData[ "useSuspendedReason" ]));
	}

	?>
	</div>
	<?
}

?>
<div class="header f_left">
	<?= _PROFILE ?>
</div>
<div class="header f_right a_right">
	<?= isset( $_GET[ "replied" ]) ? _COMMENTS : _LEAVE_COMMENT ?>
</div>
<div class="clear">&nbsp;</div>
<?

iefixEnd();

$whereMature = "1";

applyObjFilters( $whereMature );

$whereMature = "AND ".$whereMature;

?>
<div class="leftside">
	<?php

	$ID = "";
	$filename = findNewestFile( applyIdToPath( "files/ids/", $useData[ "useid" ])."-*", "" );

	if( $filename != "" )
	{
		if( preg_match( '/\.swf$/', $filename ))
		{
			list( $maxWidth, $maxHeight ) = preg_split( '/x/', $_config[ "idResolution" ]);

			$ID = '<div class="a_center mar_bottom">'.getSWF( $filename, 1.0, $maxWidth, $maxHeight ).'</div>';
		}
		else
		{
			$ID = '<div class="a_center mar_bottom"><img src="'.urlf().$filename.'" /></div>';
		}
	}

	if( $ID != "" || trim( $useData[ "useProfile" ]) != "" )
	{
		?>
		<div class="container2 mar_bottom">
			<?= $ID ?>
			<div>
				<?= formatText( $useData[ "useProfile" ]) ?>
			</div>
		</div>
		<?
	}

	$result = sql_query( "SELECT * FROM `objects` ".
		"WHERE `objid` = '".$useData[ "useFeaturedObj" ]."' ".
		"AND `objDeleted` = '0' AND `objPending` = '0' $whereMature LIMIT 1" );

	if( $objData = mysql_fetch_assoc( $result ))
	{
		?>
		<div class="caption">
			<?= _FEATURE ?>:
		</div>
		<div class="container2 a_center mar_bottom">
			<?

			if( $filename = findNewestFile( applyIdToPath( "files/features/", $useData[ "useid" ])."-*", "" ))
			{
				echo '<div><a href="'.url( "view/".$objData[ "objid" ]).'">'.
					'<img alt="'.strip_tags( formatText( $objData[ "objTitle" ])).
					'" class="thumb'.( $objData[ "objMature"] ? " mature" : "" ).
					'" title="'.strip_tags( formatText( $objData[ "objTitle" ])).
					'" src="'.urlf().$filename.'" /></a></div>'.
					'<div class="sep">'.formatText( $objData[ "objTitle" ]).'</div>';
			}

			?>
		</div>
		<?
	}

	ob_start();

	$select = "SELECT `objects`.* FROM `objects` LEFT JOIN `clubs` ON(`objForClub` = `cluid`) ";
	$where = "(`objForClub` = '0' OR `cluIsProject` = '0') ".
		"AND `objPending` = '0' AND `objDeleted` = '0' ".
		"AND `objCreator` = '".$useData[ "useid" ]."' $whereMature";

	$limit = 8;

	unset( $order );

	$more = url( "gallery/".strtolower( $useData[ "useUsername" ]));
	include( INCLUDES."mod_minigallery.php" );

	$objContents = ob_get_contents();
	ob_end_clean();

	if( $objectCount > 0 )
	{
		?>
		<div class="container2 a_center mar_bottom">
			<div>
				<form action="<?=url( "gallery/".strtolower( $useData[ "useUsername" ])) ?>" method="get">
				<button class="submit wide largetext" type="submit">
					<?= getIMG( url()."images/emoticons/submission.png" ) ?>
					<?= _SUBMISSIONS ?>
				</button>
				</form>
			</div>
			<div class="sep">
				<?= $objContents ?>
			</div>
		</div>
		<?
	}

	ob_start();

	$result = sql_query( "SELECT * FROM `journals` ".
		"WHERE `jouCreatorType` = 'use' ".
		"AND `jouCreator` = '".$useData[ "useid" ]."' ORDER BY `jouSubmitDate` DESC LIMIT 1" );

	if( $jouData = mysql_fetch_assoc( $result ))
	{
		include_once( INCLUDES."comments.php" );

		$comData = array();

		$comData[ "comid" ] = 0;
		$comData[ "comCreator" ] = $jouData[ "jouCreator" ];
		$comData[ "comComment" ] = $jouData[ "jouEntry" ];
		$comData[ "comSubject" ] = $jouData[ "jouTitle" ];
		$comData[ "comSubmitDate" ] = $jouData[ "jouSubmitDate" ];
		$comData[ "comSubmitIp" ] = $jouData[ "jouSubmitIp" ];
		$comData[ "comLastEdit" ] = $jouData[ "jouLastEdit" ];
		$comData[ "comEditIp" ] = $jouData[ "jouEditIp" ];
		$comData[ "comNoEmoticons" ] = $jouData[ "jouNoEmoticons" ];
		$comData[ "comNoSig" ] = $jouData[ "jouNoSig" ];
		$comData[ "comNoBBCode" ] = $jouData[ "jouNoBBCode" ];
		$comData[ "comAllowImage" ] = true;

		echo '<div style="margin-top: -6px">';

		showComment( $comData, 0 );

		echo '</div>';
	}

	$journalContents = ob_get_contents();
	ob_end_clean();

	if( $journalContents != "" )
	{
		?>
		<div class="container2">
			<div class="a_center">
				<form action="<?= url( "journal/".strtolower( $useData[ "useUsername" ])) ?>" method="get">
				<button class="submit wide largetext" type="submit">
					<?= getIMG( url()."images/emoticons/journal.png" ) ?>
					<?= _JOURNAL ?>
				</button>
				</form>
			</div>
		</div>
		<div class="mar_bottom"><?= $journalContents ?></div>
		<?
	}

	ob_start();

	$_pollUser = $useData[ "useid" ];

	$result1 = sql_query( "SELECT * FROM `polls` ".
		"WHERE `polCreator` = '".intval( $_pollUser )."' ORDER BY `polSubmitDate` DESC LIMIT 1" );

	if( $polData = mysql_fetch_assoc( $result1 ))
	{
		$options = array();

		$result2 = sql_query( "SELECT * FROM `pollOptions` ".
			"WHERE `polOPoll` = '".$polData[ "polid" ]."' ORDER BY `polOVotes` DESC,`polOid` LIMIT 20" );

		while( $optData = mysql_fetch_assoc( $result2 ))
		{
			$options[ $optData[ "polOid" ]] = $optData;
		}

		if( count( $options ) > 0 )
		{
			?>
			<div class="container2">
			<?

			include( INCLUDES."mod_poll.php" );

			?>
			</div>
			<?
		}
	}

	$_pollUser = 0;

	$pollContents = ob_get_contents();
	ob_end_clean();

	if( $pollContents != "" )
	{
		?>
		<div class="container2">
			<div class="a_center">
				<form action="<?= url( "poll/".strtolower( $useData[ "useUsername" ])) ?>" method="get">
				<button class="submit wide largetext" type="submit">
					<?= getIMG( url()."images/emoticons/poll.png" ) ?>
					<?= _POLL ?>
				</button>
				</form>
			</div>
		</div>
		<div class="mar_bottom"><?= $pollContents ?></div>
		<?
	}

	ob_start();

	$friendLimit = 8;
	$guestAccess = isLoggedIn() ? "" : "AND `useGuestAccess` = '1'";

	// Friends = those who watch each other.

	$friendQuery = "SELECT DISTINCT `useid` FROM `users`,`useExtData`,`watches` AS w1,`watches` AS w2 ".
		"WHERE `useid` = w1.`watCreator` ".
		"AND `useid` = `useEid` ".
		"$guestAccess ".
		"AND w1.`watType` = 'use' ".
		"AND w2.`watType` = 'use' ".
		"AND w1.`watUser` = '".$useData[ "useid" ]."' ".
		"AND w1.`watCreator` = w2.`watUser` ".
		"AND w2.`watCreator` = w1.`watUser` ".
		"ORDER BY w1.`watSubmitDate` DESC LIMIT ".( $friendLimit + 1 );

	$result = sql_query( $friendQuery );

	$friendCount = mysql_num_rows($result);

	$odd = true;
	$togo = $friendLimit;

	while( $rowData = mysql_fetch_assoc( $result ))
	{
		if( $togo <= 0 )
		{
			break;
		}

		$togo--;

		?>
		<div class="<?= $odd ? "f_left" : "f_right" ?> mar_right mar_bottom">
			<?

			if( !$odd )
			{
				echo getUserLink( $rowData[ "useid" ])." &nbsp; ";
			}

			echo getUserAvatar( "", $rowData["useid"], false, true );

			if( $odd )
			{
				echo " &nbsp; ".getUserLink( $rowData[ "useid" ]);
			}

			?>
		</div>
		<?

		$odd = !$odd;

		if( $odd )
		{
			echo '<div class="clear">&nbsp;</div>';
		}
	}

	$friendsContents = ob_get_contents();
	ob_end_clean();

	if( $friendsContents != "" )
	{
		?>
		<div class="container2 mar_bottom">
			<div class="a_center">
				<form action="<?= url( "friends/".strtolower( $useData[ "useUsername" ])) ?>" method="get">
				<button class="submit wide largetext" type="submit">
					<?= getIMG( url()."images/emoticons/watch.png" ) ?>
					<?= _FRIENDS ?>
				</button>
				</form>
			</div>
			<div class="mar_left sep">
				<? iefixStart() ?>
				<?= $friendsContents ?>
				<? iefixEnd() ?>
				<div class="clear">&nbsp;</div>
				<?

				if( $friendCount > $friendLimit )
				{
					?>
					<div class="a_right">
						<a class="disable_wrapping smalltext"
							href="<?= url( "friends/".strtolower( $useData[ "useUsername" ])) ?>">
						<?= _MORE ?>
						<?= getIMG( url()."images/emoticons/nav-next.png" ) ?>
						</a>
					</div>
					<?
				}
				?>
			</div>
		</div>
		<?
	}

	ob_start();

	$select = "SELECT * FROM `objects`,`favourites`";
	$where = "`objPending` = '0' AND `objDeleted` = '0' AND `objid` = `favObj` AND `favCreator` = '".$useData[ "useid" ]."'";
	$limit = 7;
	$order = "`favSubmitDate` DESC";
	$more = url( "favourites/".strtolower( $useData[ "useUsername" ]));

	include( INCLUDES."mod_minigallery.php" );

	$favContents = ob_get_contents();

	ob_end_clean();

	if( $objectCount > 0 )
	{
		?>
		<div class="container2 a_center mar_bottom">
			<div>
				<form method="get" action="<?= url( "favourites/".strtolower( $useData[ "useUsername" ])) ?>">
				<button class="submit wide largetext" type="submit">
					<?= getIMG( url()."images/emoticons/fav1.png" ) ?>
					<?= _FAVOURITES ?>
				</button>
				</form>
			</div>
			<div class="sep">
				<?= $favContents ?>
			</div>
		</div>
		<?
	}

	ob_start();

	$useid = $useData["useid"];
	$clubLimit = "LIMIT 3";

	include( INCLUDES."mod_clubs.php" );

	if( $clubCount >= 3 )
	{
		?>
		<div class="a_right">
			<a class="disable_wrapping smalltext"
				href="<?= url( "clubs/".strtolower( $useData[ "useUsername" ])) ?>">
			<?= _MORE ?>
			<?= getIMG( url()."images/emoticons/nav-next.png" ) ?>
			</a>
		</div>
		<?
	}

	$clubContents = ob_get_contents();
	ob_end_clean();

	if( $clubContents != "" )
	{
		?>
		<div class="container2 mar_bottom">
			<div class="a_center">
				<form action="<?= url( "clubs/".strtolower( $useData[ "useUsername" ]))?>" method="get">
				<button class="submit wide largetext" type="submit">
					<?= getIMG( url()."images/emoticons/club2.png" ) ?>
					<?= _CLUBS ?>
				</button>
				</form>
			</div>
			<div class="sep">
				<?= $clubContents ?>
			</div>
		</div>
		<?
	}

	?>
	<div class="container2 mar_bottom">
		<?= iefixStart() ?>
		<div class="a_center largetext">
			<?= _SET_CONTACTS ?> / <?= _INFORMATION ?>
		</div>
		<div class="sep mar_left mar_right">
			<?

			if( $useData[ "useShowRealName" ])
			{
				?>
				<div class="mar_bottom">
					<b><?=_REAL_NAME ?></b>:
					<?= htmlspecialchars($useData[ "useRealName" ]) ?>
				</div>
				<?
			}

			if( $useData[ "useAIM" ] != "" )
			{
				?>
				<div class="halfwidth mar_bottom">
					<b><?= _SET_CONTACTS_AIM ?></b>:
					<?= htmlspecialchars($useData[ "useAIM" ]) ?>
				</div>
				<?
			}

			if( $useData[ "useICQ" ] != "" )
			{
				?>
				<div class="halfwidth mar_bottom">
					<b><?= _SET_CONTACTS_ICQ ?></b>:
					<?= htmlspecialchars($useData[ "useICQ" ]) ?>
				</div>
				<?
			}

			if( $useData[ "useMSN" ] != "" )
			{
				?>
				<div class="halfwidth mar_bottom">
					<b><?= _SET_CONTACTS_MSN ?></b>:
					<?= htmlspecialchars($useData[ "useMSN" ]) ?>
				</div>
				<?
			}

			if( $useData[ "useYIM" ] != "" )
			{
				?>
				<div class="halfwidth mar_bottom">
					<b><?= _SET_CONTACTS_YIM ?></b>:
					<?= htmlspecialchars($useData[ "useYIM" ]) ?>
				</div>
				<?
			}

			if( $useData[ "useJabber" ] != "" )
			{
				?>
				<div class="halfwidth mar_bottom">
					<b><?= _SET_CONTACTS_JABBER ?></b>:
					<?= htmlspecialchars($useData[ "useJabber" ]) ?>
				</div>
				<?
			}

			?>
			<div class="clear">&nbsp;</div>
			<?

			if( $_auth[ "useid" ])
			{
				?>
				<div>
					<b><?= _PREFERRED_LANGUAGE ?></b>:
					<?

					$result = sql_query( "SELECT `lanName`,`lanEngName` FROM `languages` ".
						"WHERE `lanid` = '".$useData[ "useLanguage" ]."'" );

					if( $lanData = mysql_fetch_assoc( $result ))
					{
						echo $lanData[ "lanName" ]." (".$lanData[ "lanEngName" ].")";
					}

					?>
				</div>
				<div>
					<b><?=_SET_TIMEZONE ?></b>:
					<?

					$timezone = $useData[ "useTimezone" ] == 0 ? 260 : $useData[ "useTimezone" ];

					$result = sql_query( "SELECT `timName`,`timOffset` FROM `timezones` ".
						"WHERE `timid` = '$timezone'");

					if( $timData = mysql_fetch_assoc( $result ))
					{
						$timName = htmlspecialchars( str_replace( "_", " ", $timData[ "timName" ]));

						echo "(GMT".getTimezoneOffset( $timData[ "timOffset" ]).") ".$timName;
					}

					?>
				</div>
				<?
			}

			?>
		</div>
		<? if(atLeastModerator()) { ?>
		<div class="sep a_center largetext" style="width: 100%; border: none; background: #aaa; color: #000; cursor: pointer;"
			onclick="document.location='<?=url("moderate/".$useUsername)?>'"><?=_MODERATION ?></div>
		<? } ?>
		<?= iefixEnd() ?>
	</div>
</div>

<div class="rightside" style="margin-top: -1px">
	<?

	include_once( INCLUDES."comments.php" );

	showAllComments( $useData[ "useid" ], "use", false, true, true, false,
		/*enable_cache*/true );

	?>
</div>
<div class="clear">&nbsp;</div>

</div>
