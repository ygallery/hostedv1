<?
if( !atLeastHelpdesk() ) {
	return; 
}

$_documentTitle = 'b&';

?>
<div class="header">
	<div class="header_title">
		Banned
		<div class="subheader">IP Banned</div>
	</div>
</div>

<div class="container">
	You or another user on the same IP address have been banned from accessing y!Gallery for <i>insert optional reason here</i>. Blah blah blah<br />
	Sample :D
</div>
