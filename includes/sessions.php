<? // session handling
	require_once(INCLUDES."common.php");

function createSession($useid,$persistentLogin = 0) { // it is imperitive that the user ID has been validated and that checkSession() has been run /before/ this function is called
	global $_config;
	$sessionid = sha1(mt_rand() * (microtime() * 0.001)); // generate a random session id

//	Commented out to allow multiple sessions per user ID
//	terminateSession($useid);

	sql_query("INSERT INTO `sessions` VALUES('$sessionid','$useid','$persistentLogin',NOW(),NOW(),'" . getHexIp($_SERVER["REMOTE_ADDR"]) . "');"); // create the session in the database
	$expiry = $persistentLogin ? strtotime("+9 years") : time()+$_config["sessionExpiry"];
	setcookie("yGalSession",$sessionid,$expiry,"/","." . $_config["galRoot"]); // give the user a fresh session cookie
	$GLOBALS["_yGalSession"] = $sessionid;
	return $sessionid; // return the session id
};

function terminateSession($useid) { // terminates all sessions that belong to the user ID specified
	sql_query("DELETE FROM `sessions` WHERE `sesCreator` = '" . $useid . "'"); // remove specified sessions from the database
	unset($GLOBALS["_yGalSession"]);
};

function getSessionId() {
	$sessionid = false;
	if(isset($_COOKIE["yGalSession"])) $sessionid = addslashes($_COOKIE["yGalSession"]); // set sessionid from COOKIE
	elseif(isset($_GET["yGalSession"])) $sessionid = addslashes($_GET["yGalSession"]); // sessionid may also be in GET
	return $sessionid;
};

function checkSession() {
	global $_config;
	$sessionid = getSessionId();

	if($sessionid) { // session cookie exists on client machine
		$result = sql_query("SELECT * FROM `sessions` WHERE `sesid` = '" . $sessionid . "' AND `sesIpAddress` = '" . getHexIp($_SERVER["REMOTE_ADDR"]) . "' LIMIT 1"); // make sure session is valid and matches this IP address
		if(mysql_num_rows($result)) { // if session is valid
			$sessionData = mysql_fetch_assoc($result);
			sql_query("UPDATE `sessions` SET `sesLastUpdate` = NOW() WHERE `sesid` = '" . $sessionid . "' LIMIT 1;"); // update the session expiration
			$expiry = $sessionData["sesPersistentLogin"] ? strtotime("+9 years") : time()+$_config["sessionExpiry"];
			setcookie("yGalSession",$sessionid,$expiry,"/","." . $_config["galRoot"]); // give the user a fresh session cookie
			$GLOBALS["_yGalSession"] = $sessionid;
			return $sessionData["sesCreator"]; // session currently exists and is valid
		}
		else return false; // no session currently exists
	}
	else return false; // no session currently exists, in that there is no way of determining what the session ID would be
};

?>
