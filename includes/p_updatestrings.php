<?

// -----------------------------------------------------------------------------
// This page is only accessible by a supermoderator or admin.

if( !atLeastSModerator() || $_cmd[ 1 ] != "")
{
	include( INCLUDES."p_notfound.php" );
	return;
}

updateEmoticons();
updateThemes();

$result = sql_query( "SELECT `lanid` FROM `languages`" );

while( $lanData = mysql_fetch_row( $result ))
{
	$lang1 = $lanData[ 0 ];

	updateStrings( $lang1 );
	updateJSStrings( $lang1 );
}

echo "Strings, emoticons and themes updated successfully.";

// -----------------------------------------------------------------------------
// Updates strings for the given language. Strings that are missing in the
// $lang but present in the default language (English) will be taken from the
// default language.

function updateStrings( $lang )
{
	$filename = INCLUDES."strings/".$lang.".php";

	$fp = fopen( $filename."_tmp", "w" );

	fwrite( $fp, "<?\n// DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED.\n" );

	$category = "";
	$result1 = sql_query( "SELECT * FROM `strings` ".
		"WHERE `strLanguage` = 'en' ORDER BY `strName`");

	while( $strData = mysql_fetch_assoc( $result1 ))
	{
		if( $strData[ "strCategory" ] != $category )
		{
			$category = $strData[ "strCategory" ];
		}

		$result2 = sql_query( "SELECT * FROM `strings` ".
			"WHERE `strLanguage` = '$lang' AND `strName` = '".
			$strData[ "strName" ]."' LIMIT 1" );

		if( $str2Data = mysql_fetch_assoc( $result2 ))
		{
			// If this strings exists in $lang, output it.

			$data = $str2Data;

			if( $data[ "strName" ] == "__LANG" )
			{
				sql_query( "UPDATE `languages` SET `lanName` = '".
					addslashes( $data[ "strText" ])."' ".
					"WHERE `lanid` = '$lang' LIMIT 1" );
			}
		}
		else
		{
			// Otherwise, output an english string if it's not translated yet.

			$data = $strData;
		}

		$text = formatText( $data[ "strText" ], true );
		$text = str_replace( '"', '\"', $text );
		$text = str_replace( "\n", '\n', $text );
		$text = str_replace( "\r", '', $text );

		fwrite( $fp, 'define("'.$data[ "strName" ].'","'.$text.'");'."\n" );
	}

	fwrite( $fp, "?>" );
	fclose( $fp );

	if( file_exists( $filename ))
	{
		unlink( $filename );
	}

	rename( $filename."_tmp", $filename );
}

// -----------------------------------------------------------------------------
// Updates /scripts/strings/$lang.js

function updateJSStrings( $lang )
{
	include( INCLUDES."emoticons.inc" ); // local $_emoticons

	include_once( INCLUDES."files.php" );

	forceFolders( "scripts/strings" );

	$neededStrings = array(
		"_ARE_YOU_SURE" => "_ARE_YOU_SURE",
		"_BB_ALIGN_CENTER" => "_BB_ALIGN_CENTER",
		"_BB_ALIGN_JUSTIFY" => "_BB_ALIGN_JUSTIFY",
		"_BB_ALIGN_LEFT" => "_BB_ALIGN_LEFT",
		"_BB_ALIGN_RIGHT" => "_BB_ALIGN_RIGHT",
		"_BB_BOLD" => "_BB_BOLD",
		"_BB_CLUBICON" => "_BB_CLUBICON",
		"_BB_EMOTICON" => "_BB_EMOTICON",
		"_BB_ITALIC" => "_BB_ITALIC",
		"_BB_STRIKED" => "_BB_STRIKED",
		"_BB_UNDERLINE" => "_BB_UNDERLINE",
		"_BB_SUBSCRIPT" => "_BB_SUBSCRIPT",
		"_BB_SUPERSCRIPT" => "_BB_SUPERSCRIPT",
		"_BB_THUMB" => "_BB_THUMB",
		"_BB_URL" => "_BB_URL",
		"_BB_USERICON" => "_BB_USERICON",
		"_BLANK_COMMENT" => "_BLANK_COMMENT",
		"_EMOTICONS" => "_EMOTICONS",
		"_EMOTICON_EXPLAIN" => "_EMOTICON_EXPLAIN",
		"_ENTER_CLUBNAME" => "_ENTER_CLUBNAME",
		"_ENTER_LINK_TEXT" => "_ENTER_LINK_TEXT",
		"_ENTER_LINK_URL" => "_ENTER_LINK_URL",
		"_ENTER_THUMBID" => "_ENTER_THUMBID",
		"_ENTER_USERNAME" => "_ENTER_USERNAME",
		"_FUZZY_0" => "_FUZZY_0",
		"_FUZZY_1" => "_FUZZY_1",
		"_FUZZY_2" => "_FUZZY_2",
		"_FUZZY_3" => "_FUZZY_3",
		"_FUZZY_7" => "_FUZZY_7",
		"_FUZZY_11" => "_FUZZY_11",
		"_FUZZY_24" => "_FUZZY_24",
		"_FUZZY_48" => "_FUZZY_48",
		"_FUZZY_99" => "_FUZZY_99",
		"_FUZZY_234" => "_FUZZY_234",
		"_FUZZY_456" => "_FUZZY_456",
		"_FUZZY_987" => "_FUZZY_987",
		"_HIDE" => "_HIDE",
		"_INVALID_FILE_TYPE" => "_JS_INVALID_FILE_TYPE",
		"_MORE" => "_MORE",
		"_NO_BBCODE" => "_NO_BBCODE",
		"_NO_EMOTICONS" => "_NO_EMOTICONS",
		"_NO_SIG" => "_NO_SIG",
		"_OEKAKI_NO_EDITOR" => "_JS_OEKAKI_NO_EDITOR",
		"_PLEASE_WAIT" => "_JS_PLEASE_WAIT",
		"_PREVIEW" => "_PREVIEW",
		"_PREV_FLASH_MOVIE" => "_JS_PREV_FLASH_MOVIE",
		"_PREV_TEXT_FILE" => "_JS_PREV_TEXT_FILE",
		"_READONLY" => "_READONLY",
		"_REQUIRE_LOGIN" => "_REQUIRE_LOGIN",
		"_SEND_COMMENT" => "_SEND_COMMENT",
		"_UPDATES" => "_UPDATES",
		"_MESSAGES" => "_MESSAGES",
		"_FAVOURITES" => "_FAVOURITES",
		"_COMMENTS" => "_COMMENTS",
		"_JOURNALS" => "_JOURNALS",
		"_SUBMISSIONS" => "_SUBMISSIONS",
		"_SUBMIT_TYPE_EXTRA" => "_SUBMIT_TYPE_EXTRA",
	);

	$filename = "scripts/strings/".$lang.".js";

	$fp = fopen( $filename."_tmp", "w" );

	foreach( $neededStrings as $key => $value )
	{
		$result = sql_query( "SELECT `strText` FROM `strings` ".
			"WHERE `strLanguage` = '$lang' AND `strName` = '".$value."' LIMIT 1" );

		if( $data = mysql_fetch_assoc( $result ))
		{
			$text = formatText( $data[ "strText" ], true );
		}
		else
		{
			$result = sql_query( "SELECT `strText` FROM `strings` ".
				"WHERE `strLanguage` = 'en' AND `strName` = '".$value."' LIMIT 1" );

			if( $data = mysql_fetch_assoc( $result ))
			{
				$text = formatText( $data[ "strText" ], true );
			}
			else
			{
				$text = "???";
			}
		}

		fwrite( $fp, "var $key = '".
			str_replace( "\n", "", addslashes( $text ))."';\n" );
	}

	fwrite( $fp, "var Emoticons = new Array( " );

	$emots = array();

	foreach( $_emoticons as $emo1 )
	{
		if( !$emo1[ "emoExtra" ])
		{
			$emots[ $emo1[ "emoExpr" ]] = $emo1;
		}
	}

	ksort( $emots );

	$first = true;

	foreach( $emots as $emo1 )
	{
		if( $first )
		{
			$first = false;
		}
		else
		{
			fwrite( $fp, "," );
		}

		fwrite( $fp, "{ expr: '".$emo1[ "emoExpr" ]."', icon: '".$emo1[ "emoFilename" ]."' }" );
	}

	unset( $emots );

	fwrite( $fp, " );\n" );

	fclose( $fp );

	if( file_exists( $filename ))
	{
		unlink( $filename );
	}

	rename( $filename."_tmp", $filename );
}

// -----------------------------------------------------------------------------
// Updates emoticons.inc.

function updateEmoticons()
{
	$_emoticons = array();

	// order them first by the length of the emoticon expression in the ascending order,
	// so that ":XD:" would apply before "XD".

	$result = sql_query( "SELECT SQL_CACHE SQL_SMALL_RESULT * FROM `emoticons` ".
		"ORDER BY LENGTH(`emoExpr`) DESC,`emoExpr`" );

	$str = "<?\n\n".'$_emoticons = array();'."\n";
	$n = 1;

	while( $row = mysql_fetch_assoc( $result ))
	{
		$str .= '$_emoticons[] = array( ';

		foreach( $row as $key => $value )
		{
			if( $key != "emoid" )
				$str .= '"'.$key.'" => \''.$value.'\', ';
		}

		$str .= '"img" => \''.getIMG( url().'images/emoticons/'.$row[ "emoFilename" ],
			'alt="'.$row[ "emoExpr" ].'" title="'.$row[ "emoExpr" ].'"' ).'\' );'."\n";

		$n++;
	}

	$str .= "\n?>";

	$fp = fopen( INCLUDES."emoticons.inc", "w" );
	fwrite( $fp, $str );
	fclose( $fp );
}

// -----------------------------------------------------------------------------
// Updates themes.inc.

function updateThemes()
{
    global $_config;

	$result = sql_query( "SELECT * FROM `themes`" );

	// First, include the main stylesheet that defines the basic shapes of
	// the layout.

	$str = "";

	/*
	$str .= '<link href="'.urlf()."themes/style.css?1".
		'" rel="stylesheet" type="text/css" media="screen, projection" />'."\n";
	*/

	// Include the list of alternate stylesheets, so from the browsers such
	// as Firefox it will be possible to change the stylesheet on the fly.
	// The default theme is defined in the `config` table.

	while( $theData = mysql_fetch_assoc( $result ))
	{
		$filename = "themes/theme-".$theData[ "theName" ].".css";
		$filetime = filemtime( $filename );

		$str .= "\t".'<link href="'.urlf().$filename.'?'.$filetime.'" '.
			'rel="'.( $theData[ "theName" ] != $_config[ "defaultTheme" ] ? "alternate " : "" ).
			'stylesheet" title="'.$theData[ "theLongName" ].'" type="text/css" media="screen, projection" />'."\n";
	}

	$fp = fopen( INCLUDES."themes.inc", "w" );
	fwrite( $fp, $str );
	fclose( $fp );
}

// -----------------------------------------------------------------------------

?>
