<div class="header">
	<div class="header_title"><?=_EMOTICONS ?></div>
</div>	
<div class="container">
<div class="container2">
<?=iefixStart()?>
	<?
	$_documentTitle = _EMOTICONS;

	include_once(INCLUDES."formatting.php");
	$textarea = isset($_GET["comment"]) ? addslashes($_GET["comment"]) : "";

	$emots = array();
	foreach ($_emoticons as $emo1)
		$emots[trim($emo1["emoExpr"])] = $emo1;
	ksort($emots);
	reset($emots);

	$cols = 0;
	foreach ($emots as $emo1) {
		echo '<div class="sep halfwidth">'.
			'<a href="javascript:current_textarea=get_by_id_from_opener(\''.$textarea.'\'); '.
			'wrap_textarea_selection(\''.$emo1["emoExpr"].'\', \'\'); window.close();">'.
			getIMG(url().'images/emoticons/'.$emo1["emoFilename"]).
			' '.$emo1["emoExpr"].'</a></div>';
		$cols++;
		if($cols >= 2) {
			echo '<div class="clear">&nbsp;</div>';
			$cols = 0;
		}
	}
	?>
	<div class="clear">&nbsp;</div>
<?=iefixEnd()?>
</div>
</div>
