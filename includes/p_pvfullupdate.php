<?

// This script can be ran by a supermod. It will force updating preview
// images for ALL submissions, from the first to the last. In fact,
// this script should not ever be ran unless you completely lost the
// /files/preview/ folder contents or something about preview images
// has been changed.

if( !atLeastSModerator() )
{
	return;
}

if( !isset( $_GET[ "first" ]) || !isset( $_GET[ "last" ]))
{
	redirect( url( ".", array( "first" => 1, "last" => 50 )));
}

$first = intval( $_GET[ "first" ]);
$last = intval( $_GET[ "last" ]);
$isFinished = false;

$result = sql_query( "SELECT MAX(`objid`) FROM `objects`" );

if( $data = mysql_fetch_row( $result ))
{
	if( $last > $data[ 0 ])
	{
		$last = $data[ 0 ];
		$isFinished = true;
	}
}

include_once( INCLUDES."files.php" );

for( $objid = $first; $objid <= $last; $objid++ )
{
	$result = sql_query( "SELECT * FROM `objects`, `objExtData` ".
		"WHERE `objid` = `objEid` AND `objid` = '$objid' LIMIT 1" );

	if( !$objData = mysql_fetch_assoc( $result ))
		continue;

	$imageFilename = applyIdToPath( "files/data/", $objData[ "objid" ])."-".
		preg_replace( '/[^0-9]/', "", $objData[ "objLastEdit" ]).".".
		$objData[ "objExtension" ];

	if( !file_exists( $imageFilename ))
		continue;

	$imageFileSize = filesize( $imageFilename );

	$size = getimagesize( $imageFilename );
	$imageWidth = $size[ 0 ];
	$imageHeight = $size[ 1 ];
	$imageNonResizeable = true;

	if( $size[ 2 ] == 2 || $size[ 2 ] == 3 )
	{
		$imageNonResizeable = false;
	}

	$previewWidth = 0;
	$previewHeight = 0;

	if( $imageWidth > 0 && $imageHeight > 0 && !$imageNonResizeable )
	{
		$newRevisionDate = preg_replace( '/[^0-9]/', "", $objData[ "objLastEdit" ]);

		$previewFilename = applyIdToPath( "files/preview/", $objid )."-".
			$newRevisionDate.".jpg";

		$coeff = sqrt( $_config[ "previewMaxArea" ] / ( $imageWidth * $imageHeight ));

		// Require significant size reduction, so that images wouldn't become
		// very blurry with just an unnoticeable size change.

		if( $coeff > 0.9 )
			$coeff = 1.0;

		$previewMaxWidth = round( $imageWidth * $coeff );
		$previewMaxHeight = round( $imageHeight * $coeff );

		thumbifyImage( $imageFilename, $previewFilename,
			$previewMaxWidth, $previewMaxHeight, 86 );

		if( file_exists( $previewFilename ))
		{
			$size = getimagesize( $previewFilename );
			$previewWidth = $size[ 0 ];
			$previewHeight = $size[ 1 ];

			// In case we've accidentally generated a larger file of the same
			// resolution, throw it away (why would we need a preview file that
			// is larger than the original image file?)

			if( $imageWidth == $previewWidth && $imageHeight == $previewHeight &&
				$imageFileSize * 0.8 < filesize( $previewFilename ))
			{
				$previewWidth = 0;
				$previewHeight = 0;
				unlink( $previewFilename );
			}
		}
	}

	sql_query( "UPDATE `objExtData` ".
		"SET `objPreviewWidth` = '$previewWidth', ".
		"`objPreviewHeight` = '$previewHeight', ".
		"`objImageSize` = '$imageFileSize' ".
		"WHERE `objEid` = '$objid' LIMIT 1" );
}

echo "Updating preview images for submissions from $first to $last...";

$_documentTitle = "$first to $last";

if( !$isFinished )
{
	?>
	<script type="text/javascript">
	//<![CDATA[

		window.setTimeout(
			"document.location='<?= url( ".", array( "first" => $last + 1, "last" => $last + 50 ), '&') ?>';", 200);

	//]]>
	</script>
	<?
}

?>