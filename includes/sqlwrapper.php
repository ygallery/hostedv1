<?

// -----------------------------------------------------------------------------

$_SQLUser = 0;
$_SQLArray = array();
$_SQLCount = 0;
$_SQLTime = 0.0;
$_SQLLogActivity = array();
$_SQLDisplayActivity = array();
$_SQLCalc = false;
$_SQLLog = false;
$_SQLLastQuery = "";

$_SQLValues = false;
$_SQLWhere = false;
$_SQLLimit = -1;
$_SQLOffset = -1;
$_SQLOrder = "";
$_SQLGroup = "";

// -----------------------------------------------------------------------------

function sql_reset()
{
	global $_SQLValues, $_SQLWhere, $_SQLLimit, $_SQLOffset, $_SQLOrder,
		$_SQLGroup;

	$_SQLValues = false;
	$_SQLWhere = false;
	$_SQLLimit = -1;
	$_SQLOffset = -1;
	$_SQLOrder = "";
	$_SQLGroup = "";
}

// -----------------------------------------------------------------------------

function sql_init()
{
	global $_auth, $_SQLUser, $_SQLCalc, $_SQLLog, $_SQLLogActivity,
		$_SQLDisplayActivity;

	// Users whose MySQL activity is logged to /_log_sql.html.

	$_SQLLogActivity = array( /*9690*/ ); // Users whose MySQL activity is logged to /_log_sql.html.

	// Admins who can view MySQL queries for the current page.

	$_SQLDisplayActivity = array( 4, 7, 34814/*Mayhem*/ ); // Admins who can view MySQL queries for the current page.

	// Should MySQL query processing time be calculated?

	$_SQLCalc = atLeastSModerator() ||
		in_array( $_auth[ "useid" ], $_SQLLogActivity ) ||
		in_array( $_auth[ "useid" ], $_SQLDisplayActivity );

	// Should MySQL queries be gathered?

	$_SQLLog = in_array( $_auth[ "useid" ], $_SQLDisplayActivity ) ||
		in_array( $_auth[ "useid" ], $_SQLLogActivity );

	$_SQLUser = $_auth[ "useid" ];
}

// -----------------------------------------------------------------------------

function sql_query( $query )
{
	global $_auth, $_SQLUser, $_SQLArray, $_SQLCount, $_SQLTime, $_SQLCalc,
		$_SQLLog, $_SQLLastQuery;

	if( $_SQLCalc )
	{
		$time_start = gettimeofday();
	}

	$_SQLLastQuery = $query;

	$result = mysql_query( $query ) or fatal_error( mysql_error() );

	if( $_SQLCalc )
	{
		$time_end = gettimeofday();
		$secdiff = $time_end[ "sec" ] - $time_start[ "sec" ];
		$usecdiff = $time_end[ "usec" ] - $time_start[ "usec" ];
		$timeF = ( $secdiff * 1000000 + $usecdiff ) / 1000000;

		$_SQLCount++;
		$_SQLTime += $timeF;
	}

	if( $_SQLLog )
	{
		if( preg_match( '/^SELECT\sCOUNT\(\*\)\sFROM/', $query ))
		{
			$numRows = is_resource( $result ) ?
				( "=".mysql_result( $result, 0 )) : "-";
		}
		else
		{
			$numRows = is_resource( $result ) ?
				mysql_num_rows( $result ) : "-";
		}

		while( strlen( $numRows ) < 5 )
		{
			$numRows = " ".$numRows;
		}

		$_SQLArray[] = array( "time" => $timeF, "rows" => $numRows,
			"query" => $query );
	}

	return( $result );
}

// -----------------------------------------------------------------------------

function sql_free( &$resource )
{
	mysql_free_result( $resource );
	unset( $resource );
}

// -----------------------------------------------------------------------------
// Returns number of rows in resource.

function sql_num_rows( $resource )
{
	return( mysql_num_rows( $resource ));
}

// -----------------------------------------------------------------------------
// Returns next row or false if there is no more rows.

function sql_next( $resource )
{
	return( mysql_fetch_assoc( $resource ));
}

// -----------------------------------------------------------------------------

function sql_result( $resource, $row = 0 )
{
	return( mysql_result( $resource, $row ));
}

// -----------------------------------------------------------------------------

function sql_note( $text )
{
	global $_SQLArray, $_SQLLog;

	if( $_SQLLog )
	{
		$_SQLArray[] = array( "time" => 0, "rows" => "&nbsp;", "query" => $text );
	}
}

// -----------------------------------------------------------------------------

function sql_write_log()
{
	global $_auth, $_config, $_lang, $_SQLArray, $_SQLCount, $_SQLTime, $_SQLLog,
		$_SQLLogActivity, $_SQLDisplayActivity;

	if( !$_SQLLog )
		return;

	$str =
		'<div style="padding: 8px">'.
		'<div style="font: 12px Verdana; border: 1px solid #ccc; cursor: default; '.
			'background: #eee; color: #000; line-height: 1.6" '.
			'onclick="var el = this.firstChild.nextSibling; el.style.display = '.
				'el.style.display == \'none\' ? \'block\' : \'none\';">'.
		'<div style="background: #eee; color: #888; cursor: pointer; padding: 4px 8px;">'.
			'MySQL: '.$_SQLCount.' queries, '.round( $_SQLTime, 5 ).' sec. - '.
			( isset( $_SERVER[ "REQUEST_URI" ]) ? $_SERVER[ "REQUEST_URI" ] : "???" ).
		'</div>'.
		'<div style="display: none">'.
		'<table width="100%" cellspacing="0" cellpadding="4" border="0">';

	$sqlCommands = array( "DELETE", "INSERT", "SELECT", "UPDATE" );
	$sqlKeywords = array( "AND", "FROM", "LEFT JOIN", "LIMIT", "ORDER BY", "SET", "WHERE" );

	foreach( $_SQLArray as $query )
	{
		$q = trim( $query[ "query" ]);

		if( $query[ "time" ] > 0 )
		{
			foreach( $sqlCommands as $keyword )
			{
				$l = 10 - strlen( $keyword );
				$keywordAligned = $keyword;

				while( $l > 0 )
				{
					$keywordAligned = '&nbsp;'.$keywordAligned;
					$l--;
				}

				$q = preg_replace( '/^'.$keyword.'/', $keywordAligned, $q );
			}

			foreach( $sqlKeywords as $keyword )
			{
				$l = 10 - strlen( $keyword );
				$keywordAligned = $keyword;

				while( $l > 0 )
				{
					$keywordAligned = '&nbsp;'.$keywordAligned;
					$l--;
				}

				$q = str_replace( " ".$keyword." ", "<br />".$keywordAligned." ", $q );
			}
		}

		$str .= '<tr valign="top">'.
			'<td align="right" style="border-top: 1px solid #ccc; border-right: 1px solid #ccc">'.
				sprintf( "%8.5f", $query[ "time" ])."</td>".
			'<td align="center" style="border-top: 1px solid #ccc; border-right: 1px solid #ccc">'.
				$query[ "rows" ]."</td>".
			'<td style="border-top: 1px solid #ccc"><code>'.$q.'</code></td>'.
			"</tr>\n";
	}

	$str .= '</table></div></div></div>';

	if( in_array( $_auth[ "useid" ], $_SQLLogActivity ))
	{
		$fp = fopen( "_log_sql.html", "a" );
		flock( $fp, LOCK_EX );
		fwrite( $fp, $str );
		fclose($fp);
	}

	if( in_array( $_auth[ "useid" ], $_SQLDisplayActivity ))
	{
		echo $str;
	}

	$_SQLArray = array();
}

// -----------------------------------------------------------------------------
// Safe generation of the WHERE expression. Each condition is passed as an
// array item like [field name] => [value]; all conditions will be joined
// using the AND operator. If the "field name" ends with "<", ">", "<=", ">="
// or "<>" then the specified operator substitutes the "=" that is used by
// default. If more than one condition to a field is required, you may add
// "|0", "|1" etc at the end of the field name to make the key unique within
// the array. If "field name" is an empty string (even after elimination of
// "|0", "|1" etc) then the "value" is included as is.
//
// TODO: explain "*".

function sql_parse_where( $whereArray, $initialString, $delimiter )
{
	$where = " ".$initialString." ";
	$first = true;
	$signs = array( "#=", "~=", "<>", "<=", ">=", "<", ">", "=" ); // In order
		// of priority.

	foreach( $whereArray as $key => $value )
	{
		$key = preg_replace( '/\|[0-9]+$/', "", $key );

		if( $value === false )
		{
			$value = 0;
		}

		$value = strval( $value );
		$where .= $first ? "" : $delimiter." ";
		$first = false;

		if( $key == "" ) // "value" contains the whole expression within itself?
		{
			$where .= "($value) ";
			continue;
		}

		$sign = "="; // default sign is "="
		$brackets = "'";

		//if( preg_match( '/^\*/', $value ))
		//{
		//	$value = substr( $value, 1 );
		//	$brackets = "`";
		//} 

		if( preg_match( '/\*$/', $key ))
		{
			$key = substr( $key, 0, strlen( $key ) - 1 );
			$brackets = "`";
		}

		if( preg_match( '/\!$/', $key ))
		{
			$key = substr( $key, 0, strlen( $key ) - 1 );
			$brackets = "";
		}

		reset( $signs );

		foreach( $signs as $s1 )
		{
			if( substr( $key, strlen( $key ) - strlen( $s1 ), strlen( $s1 )) == $s1 )
			{
				$sign = $s1;
				$key = substr( $key, 0, strlen( $key ) - strlen( $s1 ));
				break;
			}
		}

		$binary = "";

		if( $sign == "~=" )
			$sign = " LIKE ";

		if( $sign == "#=" )
		{
			$binary = trim( $value ) == "" ? "" : "BINARY ";
			$sign = " = ";
		}

		$where .= "$binary`".mysql_real_escape_string( $key )."` ".$sign.
			" $binary$brackets".mysql_real_escape_string( $value )."$brackets ";
	}

	if( $where == " ".$initialString." " )
	{
		$where .= "1 ";
	}

	return( $where );
}

// -----------------------------------------------------------------------------
// Works almost the same way as dbWhere(), except it's used for the INSERT
// query. Returns the string like "(...) VALUES(...)" that has to be inserted
// inside the MySQL query right after "INSERT INTO `mytable`".

function sql_parse_values( $valuesArray )
{
	$fields = "(";
	$values = " VALUES(";
	$first = true;

	foreach( $valuesArray as $key => $value )
	{
		$isExpr = preg_match( '/[\=\!]$/', $key );
		$key = preg_replace( '/[\=\!]$/', "", $key );

		if( $value === false )
		{
			$value = 0;
		}

		$value = strval( $value );

		$fields .= ( $first ? "" : "," )."`".mysql_real_escape_string( $key )."`";

		$values .= ( $first ? "" : "," ).
			( $isExpr ? $value : "'".mysql_real_escape_string( $value )."'");

		$first = false;
	}

	return( $fields.")".$values.")" );
}

// -----------------------------------------------------------------------------

function sql_limit( $limit )
{
	global $_SQLLimit;

	$_SQLLimit = $limit;
}

// -----------------------------------------------------------------------------

function sql_offset( $offset )
{
	global $_SQLOffset;

	$_SQLOffset = $offset;
}

// -----------------------------------------------------------------------------

function sql_order( $order )
{
	global $_SQLOrder;

	$_SQLOrder = $order;
}

// -----------------------------------------------------------------------------

function sql_group( $group )
{
	global $_SQLGroup;

	$_SQLGroup = $group;
}

// -----------------------------------------------------------------------------

function sql_where( $whereArray )
{
	global $_SQLWhere;

	$_SQLWhere = $whereArray;
}

// -----------------------------------------------------------------------------

function sql_values( $valuesArray )
{
	global $_SQLValues;

	$_SQLValues = $valuesArray;
}

// -----------------------------------------------------------------------------

function sql_fix_table( &$table )
{
	if( !preg_match( '/\,/', $table ) && !preg_match( '/\`/', $table ))
	{
		$table = "`".mysql_real_escape_string( trim( $table ))."`";
	}
}

// -----------------------------------------------------------------------------
// Counts rows in a table.

function sql_count( $table )
{
	global $_SQLWhere;

	sql_fix_table( $table );

	if( $_SQLWhere === false )
	{
		fatal_error( "WHERE is required" );
	}

	$result = sql_query( "SELECT COUNT(*) FROM ".$table.
		sql_parse_where( $_SQLWhere, "WHERE", "AND" ));

	$count = sql_result( $result, 0 );

	sql_free( $result );

	sql_reset();

	return( $count );
}

// -----------------------------------------------------------------------------
// Deletes rows from table.

function sql_delete( $table )
{
	global $_SQLWhere;

	sql_fix_table( $table );

	if( $_SQLWhere === false )
	{
		fatal_error( "WHERE is required" );
	}

	sql_query( "DELETE FROM ".$table.
		sql_parse_where( $_SQLWhere, "WHERE", "AND" ));

	sql_reset();
}

// -----------------------------------------------------------------------------
// Inserts a row into table.

function sql_insert( $table )
{
	global $_SQLValues;

	sql_fix_table( $table );

	if( $_SQLValues === false )
	{
		fatal_error( "VALUES is required" );
	}

	sql_query( "INSERT INTO ".$table.
		sql_parse_values( $_SQLValues ));

	sql_reset();

	return( mysql_insert_id() );
}

// -----------------------------------------------------------------------------
// Performs a SELECT query and returns MySQL resource.

function sql_rowset( $table, $fields = "*" )
{
	global $_SQLWhere, $_SQLLimit, $_SQLOffset, $_SQLOrder, $_SQLGroup;

	sql_fix_table( $table );

	if( $_SQLWhere === false )
	{
		$_SQLWhere = array();
	}

	$query = "SELECT ".$fields." FROM ".$table.
		sql_parse_where( $_SQLWhere, "WHERE", "AND" );

	if( $_SQLGroup != "" )
	{
		$query .= " GROUP BY ".$_SQLGroup;
	}

	if( $_SQLOrder != "" )
	{
		$query .= " ORDER BY ".$_SQLOrder;
	}

	if( $_SQLOffset > 0 )
	{
		if( $_SQLLimit >= 0 )
		{
			$query .= " LIMIT ".$_SQLOffset.",".$_SQLLimit;
		}
		else
		{
			$query .= " LIMIT ".$_SQLOffset.",999999";
		}
	}
	else
	{
		if( $_SQLLimit >= 0 )
		{
			$query .= " LIMIT ".$_SQLLimit;
		}
	}

	$resource = sql_query( $query );

	sql_reset();

	return( $resource );
}

// -----------------------------------------------------------------------------

function sql_value( $table, $field, $default = false )
{
	global $_SQLWhere;

	if( $_SQLWhere === false )
	{
		fatal_error( "WHERE is required" );
	}

	sql_offset( 0 );
	sql_limit( 1 );

	$result = sql_rowset( $table, $field );

	if( sql_num_rows( $result ) > 0 )
	{
		$value = sql_result( $result, 0 );

		sql_free( $result );

		return( $value );
	}
	else
	{
		sql_free( $result );

		return( $default );
	}
}

// -----------------------------------------------------------------------------

function sql_row( $table, $fields = "*" )
{
	global $_SQLWhere;

	if( $_SQLWhere === false )
	{
		fatal_error( "WHERE is required" );
	}

	sql_offset( 0 );
	sql_limit( 1 );

	$result = sql_rowset( $table, $fields );

	$data = sql_next( $result );

	sql_free( $result );

	return( $data );
}

// -----------------------------------------------------------------------------

function sql_update( $table )
{
	global $_SQLValues, $_SQLWhere;

	sql_fix_table( $table );

	if( $_SQLValues === false )
	{
		fatal_error( "VALUES is required" );
	}

	if( $_SQLWhere === false )
	{
		fatal_error( "WHERE is required" );
	}

	$query = "UPDATE ".$table.
		sql_parse_where( $_SQLValues, "SET", "," ).
		sql_parse_where( $_SQLWhere, "WHERE", "AND" );

	sql_query( $query );

	sql_reset();
}

// -----------------------------------------------------------------------------

function sql_lock_write( $table )
{
	sql_fix_table( $table );

	sql_query( "LOCK TABLES ".$table." WRITE" );

	sql_reset();
}

// -----------------------------------------------------------------------------

function sql_unlock()
{
	sql_query( "UNLOCK TABLES" );

	sql_reset();
}

// -----------------------------------------------------------------------------

?>