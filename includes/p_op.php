<?

if( !isLoggedIn() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

performOperation( $_cmd[ 1 ]);

if( isset( $_GET[ "HTTP_REFERER" ]))
	$referer = $_GET[ "HTTP_REFERER" ];
else
	if( isset( $_POST[ "HTTP_REFERER" ]))
		$referer = $_POST[ "HTTP_REFERER" ];
	else
		$referer = isset( $_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : url( "/" );

redirect( $referer );

?>