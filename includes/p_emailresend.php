<div class="header">
	<div class="header_title"><?=_USE_ACTIVATE_RESEND_TITLE ?></div>
</div>
<div class="container">
<?
	$_documentTitle = _USE_ACTIVATE_RESEND_TITLE;

	$user = $_cmd[1];
	$result = sql_query("SELECT `useid`,`useActivationKey`,`useEmail` FROM `users`,`useExtData` WHERE `useid` = `useEid` AND `useUsername` = '".addslashes($user)."' AND `useIsActive` = '0' LIMIT 1");
	if(!$useData = mysql_fetch_assoc($result)) {
		notice(_USE_ACTIVATE_NOT_FOUND);
		echo '</div>';
		return;
	}
	// send the activation key via email
	include_once(INCLUDES."mailing.php");
	$activationLink = url("activate/".$useData["useActivationKey"]);

	// TODO: how to avoid a possible hack that would be sending like 1000 emails a second ???

	sendEmail($useData["useid"], sprintf(_USE_ACTIVATE_SUBJ, $_config["galName"]),
		sprintf(_USE_ACTIVATE_BODY, $activationLink));

	notice(sprintf(_EMAIL_SENT, htmlspecialchars($useData["useEmail"])));
?>
</div>
