<?
$_documentTitle = "Administrative Chat";


if( !atLeastHelpdesk() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

if( isset( $_POST[ "submitLog" ]))
{
	sql_query( "INSERT INTO `adminChat`".dbValues( array(
		"adcText" => str_replace("\r\n", "\n", $_POST['modMessage']),
		"adcCreator" => $_auth[ "useid" ],
		"adcSubmitDate!" => "NOW()")));
	redirect( url( "." ));
}

$modOffset = isset($_GET[ "offset" ]) ? $_GET[ "offset" ] : "0";

?>
<div class="header">
	<div class="header_title">
		<?= _ADMINISTRATION ?>
		<div class="subheader">Admin Chat</div>
	</div>	
	<?

	$active = 2;
	include(INCLUDES."mod_adminmenu.php");

	?>
</div>

<div class="container">
	<h1>Site Statistics</h1>
	<div class="container2 mar_bottom">
<?php
			//facs 2011-09-15
			//Time for some hot new experimental caching!
			$f_userstats = 'cached_userstats.php';
			try{
			$fh = fopen(INCLUDES.$f_userstats,"r");
			$stats = fread($fh, filesize(INCLUDES.$f_userstats));
			fclose($fh);
			$cacheData = unserialize($stats);


?>
		<b>User Accounts:</b> <?= $cacheData['user_total'] ?><br />
		<b>Active Users:</b> <?= $cacheData['user_active'] ?><br />
		<b>Today's New Accounts:</b> <?= $cacheData['user_new'] ?><br />
		<b>Banned/Suspended Accounts:</b> <?= $cacheData['user_banned'] ?><br />
		<b>Unmodded Abuse Tickets:</b> <?= $cacheData['user_unmodded_abuse'] ?><br />
		<b>Abuses Pending User:</b> <?= $cacheData['user_pending_abuses'] ?><br />
		<b>Total Open Abuse Tickets:</b> <?= $cacheData['user_open_abuses'] ?><br />
		<b>Last Stat Update:</b> <?= date("i.s", time() - $cacheData['cache_time'])?> minutes ago<br />

<?php
			}catch(Exception $e){
				echo $e->getMessage();
			}
		/*
			$timer = microtime(true);
			$userNumber = mysql_result(sql_query("SELECT COUNT(*) FROM `useExtData` WHERE `useIsActive`='1'"),0);
			$activeUsersTime= date("Y\-m\-d",time() - (60*60*24*30));
			$newUsersTime= date("Y\-m\-d",time() - (60*60*24));
			$activeUsers = mysql_result(sql_query("SELECT COUNT(*) FROM `users` WHERE `useLastAction`>'$activeUsersTime'"),0);
			$newUsers = mysql_result(sql_query("SELECT COUNT(*) FROM `useExtData` WHERE `useIsActive`='1' AND `useSignupDate`>'$newUsersTime'"),0);
			$bannedUsers = mysql_result(sql_query("SELECT COUNT(*) FROM `users` WHERE `useIsBanned`='1' OR `useIsSuspended`='1'"),0);
			$pendingAbuse = mysql_result(sql_query("SELECT COUNT(*) FROM `abuses` WHERE `abuFinal`='?' AND `abuResolved`='0'"),0);
			$openAbuse = mysql_result(sql_query("SELECT COUNT(*) FROM `abuses` WHERE `abuResolved`='0'"),0);
			$pendingUser = mysql_result(sql_query("SELECT COUNT(*) FROM `objects` WHERE `objPendingUser`='1'"),0);
			$timer = microtime(true) - $timer;
		<b>User Accounts:</b> <?= $userNumber ?><br />
		<b>Active Users:</b> <?= $activeUsers ?><br />
		<b>Today's New Accounts:</b> <?= $newUsers?><br />
		<b>Banned/Suspended Accounts:</b> <?= $bannedUsers ?><br />
		<b>Unmodded Abuse Tickets:</b> <?= $pendingAbuse ?><br />
		<b>Abuses Pending User:</b> <?= $pendingUser ?><br />
		<b>Total Open Abuse Tickets:</b> <?= $openAbuse?><br />
		<b>Unassigned Helpdesk Tickets:</b> Coming soon!<br />
		<b>User Stats Posted in:</b> <?= $timer?>s<br />
		*/
?>	
	</div>
	<h1>Admin chat</h1>
	<div class="container2 mar_bottom">
		<?= iefixStart() ?>
		<div class="mar_bottom">
			<a href="/adminchat/<?= isset($_GET['all']) ? '?all' : '' ?>">Refresh</a>
			 &bull; <a href="/adminchat/?all">View all previous notes</a>
			  <?= $modOffset >= "30" ? '&bull; <a href="'.url( ".", array( "offset" => $modOffset-30 )).'">Previous Page</a>' : ""?>
			 &bull;  <a href="<?= url( ".", array( "offset" => $modOffset+30))?>">Next Page</a>
		</div>
		<form action="<?= url( "." )?>" method="post">
			<a name="modlog"></a>
			<?

			if(isset($_GET['all']))
			{
				$modLimit = "";
			}
			else
			{
				$modLimit = "LIMIT $modOffset,30";
			}

			$modResult = sql_query("SELECT * FROM `adminChat` ORDER BY `adcSubmitdate` DESC $modLimit");

			while( $modData = mysql_fetch_assoc( $modResult ))
			{
				?>
				<div class="sep mar_left mar_right">
					#<?= $modData["adcId"] ?> - <?= gmdate($_auth["useDateFormat"], applyTimezone(strtotime($modData[ "adcSubmitDate" ]))) ?>
					- <?= getUserLink( $modData[ "adcCreator" ]) ?>:
					<?= formatText( $modData[ "adcText" ]) ?>
				</div>
				<?
			}

			mysql_free_result( $modResult );

			?>
			<div class="sep a_center">
				<?
				iefixStart();

						$commentName = "modMessage";
						$commentNoOptions = true;
						$commentRows = 8;

						include( INCLUDES."mod_comment.php" );

					iefixEnd();
							
				?>
			</div>
			<div class="sep a_center">
				<input class="submit" type="submit" name="submitLog" value="Add Message" />
			</div>
		</form>
		<?= iefixEnd() ?>
	</div>
</div>
