<?

include_once(INCLUDES."polling.php");

$usernameData = getUserData( $polData[ "polCreator" ]);
$useUsername = $usernameData[ "useUsername" ];

?>
<form action="<?=url("poll/".strtolower($useUsername)."/".$polData["polid"])?>" method="post">
<?

if($_cmd[0] != "user")
{
	?>
	<div class="mar_bottom">
		<?=($_cmd[0] != "" && $_cmd[0] != "user") ? getUserLink($polData["polCreator"]).": " : "" ?>
		<?=formatText($polData["polSubject"]) ?>
	</div>
	<?

	if( $polData[ "polComment" ] != "" )
	{
		?>
		<div class="mar_bottom a_right smalltext">
			<a href="<?=url("poll/".strtolower($useUsername)."/".$polData["polid"])?>">
				<?=_READ_EXPLANATION?>
				<?=getIMG(url()."images/emoticons/nav-next.png") ?></a>
		</div>
		<?
	}
}
else
{
	?>
	<div class="mar_bottom largetext">
		<?=($_cmd[0] != "" && $_cmd[0] != "user") ? getUserLink($polData["polCreator"]).": " : "" ?>
		<?=formatText($polData["polSubject"]) ?>
	</div>
	<div class="mar_bottom">
		<?=formatText($polData["polComment"]);?>
	</div>
	<?
}

if(!$_auth["useid"] || $polData["polCreator"] == $_auth["useid"])
	$alreadyVoted = true;
else
{
	$result = sql_query("SELECT `pollVid` FROM `pollVotes`,`pollOptions`,`polls` ".
		"WHERE `polOPoll` = '".$polData["polid"]."' ".
		"AND `polid` = `polOPoll` ".
		"AND `pollVSelected` = `polOid` ".
		"AND (`pollVUser` = '".$_auth["useid"]."' OR `polExpireDate` < NOW()) LIMIT 1");

	$alreadyVoted = (mysql_num_rows($result) > 0);
}

showPollOptions($options, $alreadyVoted, $polData["polVoted"], $polData[ "polid" ]);

?>
<div class="sep a_left">
	<?

	if(!$_auth["useid"])
		notice(_REQUIRE_LOGIN);

	?>
	<div class="sep f_right smalltext">
		<a href="<?=url("poll/".strtolower($useUsername))?>">
			<?=_POLL_PAST_ENTRIES ?>
			<?=getIMG(url()."images/emoticons/nav-next.png") ?>
		</a>
	</div>
	<?

	if(!$alreadyVoted)
	{
		?>
		<button class="submit" name="submitVote" type="submit">
			<?=getIMG(url()."images/emoticons/checked.png") ?>
			<?=_VOTE ?>
		</button>
		<?
	}

	?>
	<div class="clear">&nbsp;</div>
</div>
</form>
