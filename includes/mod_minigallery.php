<? // Module: mini-gallery.

$objectCount = 0;

// Returns: $objectCount - the number of thumbnails actually shown.

if(!isset($limit)) $limit = 5;
if(!isset($order)) $order = "`objSubmitDate` DESC";

applyObjFilters( $where );

$actualLimit = isset($more) ? $limit + 1 : $limit;

if( !isLoggedIn() )
{
	$where = "(".$where.") AND `objGuestAccess` = '1'";
}

$result = sql_query("$select WHERE $where ORDER BY $order LIMIT $actualLimit");

$objList = array();
while($objData = mysql_fetch_assoc($result)) {
	$objList[$objData["objid"]] = $objData;
}

list($tableWidth,$tableHeight) = preg_split('/x/', $_config["thumbResolution"]);
$tableWidth += 30;
$tableHeight += 40;

$tabsId = generateId();

$bigList = "";
$first = true;
$firstThumbId = "";
$firstMicroId = "";
echo '<div class="a_center"'.( $limit == 1 ? ' style="margin-top: -16px"' : "" ).'>'."\n";
reset($objList);
$togo = $limit;

$useids = array();

foreach( $objList as $objData )
{
	if( isset( $objData[ "objCreator" ]))
	{
		$useids[] = $objData[ "objCreator" ];
	}
}

prefetchUserData( array_unique( $useids ));

foreach($objList as $objData) {
	$togo--;
	if($togo < 0) break;
	$microId = generateId();
	$thumbId = generateId();

	$u = getUserData( $objData[ "objCreator" ]);

	$objTitle = formatText($objData["objTitle"]).' <br />';

	if( $objData[ "objCollab" ] > 0 )
	{
		$objTitle .= sprintf( _BY_AND,
			getUserLink( $u[ "useid" ]),
			getUserLink( $objData[ "objCollab" ]));
	}
	else
	{
		$objTitle .= sprintf( _BY, getUserLink( $u[ "useid" ]));
	}
	
	if( $objData[ "objThumbDefault" ]) 
	{
		$src = urlf()."images/litthumb.png";		
	}
	else 
	{	
		$src = urlf().applyIdToPath("files/thumbs/", $objData["objid"])."-".preg_replace('/[^0-9]/', "", $objData["objLastEdit"]).".jpg";
	}
	
	$url = url("view/".$objData["objid"]);

	if( $limit > 1 )
	{
		echo '<span id="'.$microId.'" '.
			"onmouseover=\"open_tab(this,'$tabsId','$thumbId')\">".
			'<a href="'.$url.'">'.
			'<img alt="'._ALT_THUMB.'" class="microthumb'.($objData["objMature"] ? " mature" : "").($objData["objPending"] ? " pending" : "").($objData["objDeleted"] ? " deleted" : "").'" src="'.$src.'" title="'.strip_tags($objTitle).'" />'.
			'</a></span>'."\n";
	}

	$bigList .= '<div '.($first ? "" : 'style="display: none" ').'id="'.$thumbId.'">'.
		'<table align="center" border="0" cellpadding="0" cellspacing="0"><tr><td class="a_center v_middle pad_top" style="height: '.$tableHeight.'px">'.
		'<div style="width: '.$tableWidth.'px">'.
		'<a href="'.$url.'">'.
		'<img alt="'.strip_tags($objTitle).'" class="thumb'.($objData["objMature"] ? " mature" : "").($objData["objPending"] ? " pending" : "").($objData["objDeleted"] ? " deleted" : "").'" src="'.$src.'" title="'.strip_tags($objTitle).'" />'.
		'</a></div></td></tr></table>'."\n".
		'<table border="0" cellpadding="0" cellspacing="0" class="wide" style="height: 40px"><tr><td class="a_center v_middle">'.
		'<div>'.$objTitle.'</div>'."\n".
		'</td></tr></table></div>'."\n";
	if($first) {
		$first = false;
		$firstMicroId = $microId;
		$firstThumbId = $thumbId;
	}
	$objectCount++;
}
if(isset($more) && count($objList) > $limit) {
	?>
	&nbsp;
	<a class="disable_wrapping smalltext" href="<?=$more?>">
	<?=_MORE ?>
	<?=getIMG(url()."images/emoticons/nav-next.png") ?>
	</a>
	<?
}
echo $bigList.'</div>'."\n";

if( $limit > 1 )
{
	?>
	<script type="text/javascript">
	//<![CDATA[
		toggle_visibility('<?=$firstThumbId ?>');
		var bar = get_by_id ('<?=$firstMicroId ?>');
		open_tab (bar, '<?=$tabsId ?>', '<?=$firstThumbId ?>');
	//]]>
	</script>
	<?
}

unset($select);
unset($where);
unset($limit);
unset($order);
unset($more);

?>