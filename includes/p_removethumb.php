<?php 

if( !atLeastModerator() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

elseif( atLeastModerator() ) {
	
	$_documentTitle = "Remove Thumbnail";
	
?>

<div class="header">
	<div class="header_title">
		<?= _ADMINISTRATION ?>
		<div class="subheader">Remove Thumbnail</div>
	</div>
	<?php 
		$active = 10;
		include(INCLUDES."mod_adminmenu.php");
	
	?>
</div>

	
<div class="container">

	<div class="mar_bottom container2 notsowide">
			<?=iefixStart()?>
			<div class="mar_bottom">
				<?= notice("This will remove the thumbnail of any text submission and replace it with the default lit thumbnail.
				This is only temporary until I finish the better system.")?>
				<div class="sep"><b>Enter Submission ID</b>:</div>
				<form action="<?= url( "." )?>" method="post">
					<input type="text" name="submissionId" size="50" /> 
				    <input class="submit" type="submit" name="submitThumbExploder" value="Blammo" />
				</form>
			</div>
			
			
				<?php 
					if( isset($_POST[ "submissionId" ])) 
					{
						$submissionId = $_POST[ "submissionId" ];
						$result = sql_query( "SELECT * FROM `objects`,`objExtData` WHERE `objid`=`objEid` AND `objid` = '".addslashes( $submissionId )."' AND `objExtension`='txt' LIMIT 1" );
						$resultcount = mysql_num_rows( $result );
						if( $resultcount == 0 )
						{
							echo "Submission does not exist or is not a text file.";
						}
						else {
							sql_query( "UPDATE `objects` SET `objThumbWidth`='150', `objThumbHeight`='150', `objThumbDefault`='1' WHERE `objid` = '".addslashes( $submissionId )."' LIMIT 1" )
							?>
							<div class="mar_bottom">
							<b>Raeped</b>
							<?php 
							echo getObjectThumb( $submissionId );
							?>
							</div>
							<?php 
						}
					}
				
				?>
			
			</div>
			<?=iefixEnd()?>

</div>
<?php 
}
?>