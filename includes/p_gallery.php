<?

$whereGuest = isLoggedIn() ? "" : ( " AND `useGuestAccess` = '1' " );

$result = sql_query( "SELECT `useid`,`useUsername` FROM `users`, `useExtData` ".
	"WHERE `useUsername` = '".addslashes( $_cmd[ 1 ])."' AND `useid` = `useEid` $whereGuest LIMIT 1" );

if( !$useData = mysql_fetch_assoc( $result ))
{
	include(INCLUDES."p_notfound.php");
	return;
}

$useUsername = strtolower( $useData[ "useUsername" ]);

$_pollUser = $useData[ "useid" ];

$_documentTitle = $useData[ "useUsername" ].": "._SUBMISSIONS;

$title = _SUBMISSIONS;

$folid = 0;

if( $_cmd[ 2 ] != "" )
{
	$folResult = sql_query( "SELECT * FROM `folders`".dbWhere( array(
		"folCreator" => $useData[ "useid" ],
		"folIdent" => $_cmd[ 2 ])));

	if( $folData = mysql_fetch_assoc( $folResult ))
	{
		$folid = $folData[ "folid" ];

		$_documentTitle .= ": ".strip_tags( formatText( $folData[ "folName" ]));
		$title .= ": ".formatText( $folData[ "folName" ]);
	}
	else
	{
		redirect( url( "gallery/".strtolower( $useData[ "useUsername" ])));
	}
}

?>
<div class="header">
	<div class="f_right mar_left a_center normaltext">
		<?=getUserAvatar("",$useData["useid"], true)?>
	</div>
	<div class="f_left header_title">
		<?=$useData["useUsername"]?>
		<div class="subheader"><?= $title ?></div>
	</div>
	<?

	$active = 2;

	include(INCLUDES."mod_usermenu.php");

	?>
</div>

<div class="container">
	<?

	$active = 1;

	include( INCLUDES."mod_usersubmenu.php" );

	include_once( INCLUDES."gallery.php" );

	// Create parameters to pass to showThumbnails.

	if( $showFolders )
	{
		if( $folid == 0 )
		{
			$showArray = array(
				"folderCreator" => $useData,

				"folderSelect" => "SELECT `folders`.* FROM `folders` ".
					"LEFT JOIN `objects` ON(`objFolder` = `folid`) ".
					"LEFT JOIN `clubs` ON(`objForClub` = `cluid`)",

				"folderWhere" => "`objFolder` IS NOT NULL ".
					"AND (`objForClub` = '0' OR `cluIsProject` = '0') ".
					"AND `objCreator` = '".$useData[ "useid" ]."' ",
					"AND `folCreator` = '".$useData[ "useid" ]."'",

				"folderCalc" => "SELECT COUNT(*) FROM `objects` ".
					"LEFT JOIN `clubs` ON(`objForClub` = `cluid`) ",

				"folderCalcWhere" => "(`objForClub` = '0' OR `cluIsProject` = '0') ".
					"AND `objCreator` = '".$useData["useid"]."'",

				"select" => "SELECT `objects`.* FROM `objects` ".
					"LEFT JOIN `clubs` ON(`objForClub` = `cluid`) ",

				"where" => "(`objForClub` = '0' OR `cluIsProject` = '0') ".
					"AND `objFolder` = '0' ".
					"AND `objCreator` = '".$useData["useid"]."'" );
		}
		else
		{
			$showArray = array(
				"folderParent" => $useData,

				"select" => "SELECT `objects`.* FROM `objects` ".
					"LEFT JOIN `clubs` ON(`objForClub` = `cluid`) ",

				"where" => "(`objForClub` = '0' OR `cluIsProject` = '0') ".
					"AND `objCreator` = '".$useData["useid"]."'" );

			if( $_cmd[ 2 ] != "" )
			{
				$showArray[ "where" ] .= " AND `objFolder` = '".$folid."'";
			}
		}
	}
	else
	{
		$showArray = array(
			"select" => "SELECT `objects`.* FROM `objects` ".
				"LEFT JOIN `clubs` ON(`objForClub` = `cluid`) ",

			"where" => "(`objForClub` = '0' OR `cluIsProject` = '0') ".
				"AND `objCreator` = '".$useData["useid"]."'" );
	}

	showThumbnails($showArray);

	?>
</div>
