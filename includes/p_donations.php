<?
if(!$_auth["useid"]) {
	header("Status: 403 Forbidden");
	echo '<div class="header">'._DONATIONS.'</div><div class="container">';
	notice(_REQUIRE_LOGIN);
	echo "</div>";
	return;
}

$_documentTitle = _DONATIONS;

?>

<style>
	.well{
		text-align: center;
		min-height: 20px;
		padding: 19px;
		margin-top: 10px;
		margin-bottom: 10px;
		margin-left: auto;
		margin-right: auto;
		background-color: #477396;
		border-radius: 4px;
		-moz-box-shadow:    inset 0 0 5px #000000;
		-webkit-box-shadow: inset 0 0 5px #000000;
		box-shadow:         inset 0 0 5px #000000;
		max-width: 500px;
	}
</style>
<?php
if( isset($_POST["action"]) && $_POST["action"] == "Donate" && isset($_POST["donAmnt"]) && filter_var($_POST["donAmnt"], FILTER_VALIDATE_INT) !== false && $_POST["donAmnt"] > 0)
{
	
	$donationAmount = $_POST["donAmnt"];
	$donationStripeAmount = $_POST["donAmnt"]*100;
	setcookie("donationStripeAmount", $donationStripeAmount, time()+300);  /* expire in 1 minute */
	echo "
	<div class='well'>
	<img src='/images/128x128c.png' width='100px'></img>
	<p>By donating the <b>$".$donationAmount."</b> you entered on the previous page, you will be helping y!Gallery stay alive and up and running, as well as to help better itself.</p>
	<p>Our developers have busy lives, more often than not, they have to put their own commitments before y!Gallery. Any amount raised will help pay them for their services as well as keep our servers running (Our top priority).</p>
	<form action='/payment/process.php' method='POST'>
		<script
			src='https://checkout.stripe.com/checkout.js' class='stripe-button'
			data-key='pk_test_bXtOLBOxWUuMjrNH7smSMLhZ'
			data-amount='".$donationStripeAmount."'
			data-name='y!Gallery'
			data-description='y!Gallery Donation ($".$donationAmount.")'
			data-image='/images/128x128.png'
			data-locale='auto'>
		</script>
		<input hidden value='".$donationStripeAmount."' name='amntDon'>
	</form>
	<a href='/donations'>Back to donation choice</a>
	</div>";
	
}

elseif( isset($_POST["action"]) && $_POST["action"] == "process")
	{
		echo "Im alive";
	}

else
{
	echo "
	<div class='well'>
	<img src='/images/128x128c.png' width='100px'></img>
	<p>By donating, you will be helping y!Gallery stay alive and up and running, as well as to help better itself.</p>
	<p>our developers have busy lives, more often than not, they have to put their own commitments before y!Gallery. Any amount raised will help pay them for their services as well as keep our servers running (Our top priority).</p>
	<form action='' method='POST'>
			$<input id='' type='number' maxlength='4' min='0' autofocus name='donAmnt'>
			<input type='submit' name='action' value='Donate' />
	</form>
	</div>";
}
?>