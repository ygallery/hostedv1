<div class="header">
	<div class="header_title">
		<?=_LOGOUT ?>
		<div class="subheader"><?=_LOGOUT_EXPLAIN ?></div>
	</div>	
</div>
<div class="container">
<?
	$_documentTitle = _LOGOUT;

	terminateSession($_auth["useid"]); // presuming, this will exit() on error
	redirect(url("/")); // redirect to the front page on success
?>
</div>
