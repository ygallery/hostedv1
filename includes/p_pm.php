<?

$_pm_per_page = 20;

include_once( INCLUDES."comments.php" );

if( $_cmd[ 1 ] == "read" && $_cmd[ 2 ] != "" )
{
	$_GET[ "read" ] = $_cmd[ 2 ];
}

if( $_cmd[ 1 ] == "reply" && $_cmd[ 2 ] != "" )
{
	$_GET[ "replyTo" ] = $_cmd[ 2 ];
}

if( $_cmd[ 1 ] == "for" && $_cmd[ 2 ] != "" )
{
	$_GET[ "composeFor" ] = $_cmd[ 2 ];
}

if( isset( $_POST[ "sendPMto" ]))
{
	redirect( url( "pm/for/".preg_replace( '/[^a-zA-Z0-9]/', "",
		$_POST[ "sendPMto" ])));
}

if(isset($_POST['deleteSentMessage'])){
	$pmid = array();
	foreach($_POST as $key => $value)
	{
		
		if(preg_match('/pmsid_(\w+)/', $key))
		{
			$pmid[] = preg_replace('/pmsid_(.*)/', "$1", $key);
		}
	}
	
	for($i = 0; $i < count($pmid); $i++)
	{
		$stmt = $dbh->prepare("UPDATE `pms`
			SET `pmsCreatorDelete` = '1'
			WHERE `pmsCreator` = ?
			AND `pmsid` = ?");
		$stmt->bindParam(1, $_auth['useid'], PDO::PARAM_INT);
		$stmt->bindParam(2, $pmid[$i], PDO::PARAM_INT);
		$stmt->execute();	
	}
}

if(isset($_POST['deleteMessage']))
{
	$pmid = array();
	foreach($_POST as $key => $value)
	{
		
		if(preg_match('/pmsid_(\w+)/', $key))
		{
			$pmid[] = preg_replace('/pmsid_(.*)/', "$1", $key);
		}
	}

	for($i = 0; $i < count($pmid); $i++)
	{
		$sth = $dbh->prepare("UPDATE `pms`
			SET `pmsReceiverDelete` = '1'
			WHERE `pmsPmUser` = ?
			AND `pmsid` = ?");

		$sth->bindParam(1, $_auth['useid'], PDO::PARAM_INT);
		$sth->bindParam(2, $pmid[$i], PDO::PARAM_INT);
		$sth->execute();

		$sth = $dbh->prepare("DELETE FROM `updates`
							WHERE `updCreator` = ?
							AND `updType` = '".updTypePM."'
							AND updObj = ?
							LIMIT 1");
		$sth->bindParam(1, $_auth['useid'], PDO::PARAM_INT);
		$sth->bindParam(2, $pmid[$i], PDO::PARAM_INT);
		$sth->execute();
		
		recountUpdates( updTypePM, $_auth[ "useid" ]);
	}
}

$_documentTitle = _PMS;

?>
<div class="header">
	<div class="header_title">
		<?= _PMS ?>
		<div class="subheader"><?= _PM_SUBTITLE ?></div>
	</div>
</div>

<div class="container">
	<?

	if( !isLoggedIn() )
	{
		header("Status: 403 Forbidden");
		notice( _REQUIRE_LOGIN );
		echo "</div>";
		return;
	}

    // Delete Filters....

    if(isset($_GET['deleteByUsername'])){
    	$sth = $dbh->prepare("UPDATE `pms`
    	    SET `pmsReceiverDelete` = '1'
    		WHERE `pmsCreator` = ?
    		AND `pmsPmUser` = ?");
    	$sth->bindParam(1, $_GET['deleteByUsername'], PDO::PARAM_INT);
    	$sth->bindParam(2, $_auth['useid'], PDO::PARAM_INT);
    	$sth->execute();
    }
	
	if(isset($_GET['deleteByUsernameSender'])){
    	$sth = $dbh->prepare("UPDATE `pms`
    	    SET `pmsCreatorDelete` = '1'
    		WHERE `pmsCreator` = ?
			AND `pmsPmUser` = ?");
    	$sth->bindParam(2, $_GET['deleteByUsernameSender'], PDO::PARAM_INT);
    	$sth->bindParam(1, $_auth['useid'], PDO::PARAM_INT);
    	$sth->execute();
    }

    function dateModifier($s, $m, $h, $d, $mo, $y){
        return ( time() - ($s * $m * $h * $d * $mo *$y));
    }
	
	if(isset($_GET['deleteByDateSender'])){
        $deleteTime = null;
        switch($_GET['deleteByDateSender']){
            case 0:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsCreatorDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
                AND pmsCreator = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 1:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsCreatorDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 1 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsCreator = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 2:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsCreatorDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 7 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsCreator = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 3:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsCreatorDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 31 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsCreator = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 4:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsCreatorDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 90 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsCreator = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 5:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsCreatorDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 180 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsCreator = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 6:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsCreatorDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 270 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsCreator = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 7:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsCreatorDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 365 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsCreator = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 8:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsCreatorDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 730 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsCreator = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 9:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsCreatorDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 1095 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsCreator = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 10:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsCreatorDelete` = '1'
                WHERE pmsCreator = '".$_auth['useid']."';");
                $sth->execute();
            break; 
        }
    }
	
    if(isset($_GET['deleteByDate'])){
        $deleteTime = null;
        switch($_GET['deleteByDate']){
            case 0:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsReceiverDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
                AND pmsPmUser = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 1:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsReceiverDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 1 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight);");
                $sth->execute();
            break;
            case 2:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsReceiverDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 7 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsPmUser = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 3:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsReceiverDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 31 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsPmUser = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 4:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsReceiverDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 90 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsPmUser = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 5:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsReceiverDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 180 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsPmUser = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 6:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsReceiverDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 270 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsPmUser = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 7:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsReceiverDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 365 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsPmUser = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 8:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsReceiverDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 730 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsPmUser = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 9:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsReceiverDelete` = '1'
                WHERE `pmsSubmitDate` >= (SELECT DATE(NOW()) - INTERVAL 1095 DAY Midnight)
AND `pmsSubmitDate` <= (SELECT DATE(NOW()) - INTERVAL 0 DAY Midnight)
AND pmsPmUser = '".$_auth['useid']."';");
                $sth->execute();
            break;
            case 10:
                $sth = $dbh->prepare("UPDATE pms
                SET `pmsReceiverDelete` = '1'
				WHERE pmsPmUser = '".$_auth['useid']."';");
                $sth->execute();
            break; 
        }
    }

	?>
	<div class="leftside">
		<?
		if( isset( $_GET[ "replyTo" ]))
		{
			?>
			<div class="header">
				<?= _PM_RECEIVED_MESSAGE ?>
			</div>
			<?
                
			$comid = intval( $_GET[ "replyTo" ]);

			if( isAdmin() )
			{
				$result = sql_query( "SELECT * FROM `pms` ".
					"WHERE `pmsid` = '$comid' ".
					"LIMIT 1" );
			}
			else
			{
				$result = sql_query( "SELECT * FROM `pms` ".
					"WHERE `pmsPmUser` = '".$_auth[ "useid" ]."' ".
					"AND `pmsid` = '$comid' ".getTwitWhere( "pmsCreator" ).
					"LIMIT 1" );
			}
			if( mysql_num_rows( $result ) > 0 )
			{
				$comData = mysql_fetch_assoc( $result );

				?>
				<div class="mar_bottom" style="margin-top: -8px">
					<?

					showComment( $comData, 0, true );

					if( !isset( $_GET[ "composeFor" ]))
					{
						$usernameData = getUserData( $comData[ "pmsCreator" ]);

						$_GET[ "composeFor" ] = $usernameData[ "useUsername" ];
					}

					?>
				</div>
				<?

				sql_query( "DELETE FROM `updates` ".
					"WHERE `updCreator` = '".$_auth[ "useid" ]."' ".
					"AND `updType` = '".updTypePM."' ".
					"AND `updObj` = '".$comid."' LIMIT 1" );

				recountUpdates( updTypePM, $_auth[ "useid" ]);
			}
			else
			{
				?>
				<div class="container2">
					<?= _PM_NOT_FOUND ?>
				</div>
				<?
			}

			?>
			<div><br /></div>
			<?
		}
		else
		{
			?>
			<div><br /></div>
			<?
		}

		?>
	</div>

	<div class="rightside">
		<?

		$hidePMSender = false;

		if( isset( $_POST[ "sendReply" ]))
		{
			$title = $_POST[ "title" ];

			if( $title == "" ) // Make sure the title is not blank.
				$title = _PM_UNTITLED;

			$comment = substr( $_POST[ "comment" ], 0, 40000 );

			// Add the comment to the database.

			$composeFor = intval( $_POST[ "composeFor" ]);

			if( $composeFor != 0 )
			{
				$userIp = getHexIp($_SERVER["REMOTE_ADDR"]);

				sql_query( "INSERT INTO `pms`".dbValues( array(
					"pmsObj" => intval( $_POST[ "parentComment" ]),
					"pmsCreator" => $_auth[ "useid" ],
					"pmsPmUser" => $composeFor,
					"pmsSubmitDate!" => "NOW()",
					"pmsTitle" => $title,
					"pmsComment" => $comment,
					"pmsSubmitIp" => $userIp,
					"pmsEditIp" => $userIp,
					"pmsNoEmoticons" => isset( $_POST[ "commentNoEmoticons" ]) ? 1 : 0,
					"pmsNoSig" => isset( $_POST[ "commentNoSig" ]) ? 1 : 0,
					"pmsNoBBCode" => isset( $_POST[ "commentNoBBCode" ]) ? 1 : 0 )));

				$comid = mysql_insert_id();

				// Add the unread message notification to the target user's
				// updates.

				addUpdate( updTypePM, $composeFor, $comid, $_auth[ "useid" ]);

				// Redirect the browser to the newly created message.

				redirect( url( "pm/read/".$comid ));
			}
		}

		if( isset( $_GET[ "composeFor" ]))
		{
			$composeFor = preg_replace( '/[^a-zA-Z0-9]/', "",
				$_GET[ "composeFor" ]);

			$result = sql_query( "SELECT `useid` FROM `users` ".
				"WHERE `useUsername` = '$composeFor' LIMIT 1" );

			if( mysql_num_rows( $result ) > 0 )
			{
				$composeFor = mysql_result($result, 0);
			}
			else
			{
				$composeFor = 0;
			}

			$parentComment = isset($_GET["replyTo"]) ? intval($_GET["replyTo"]) : 0;

			$userLink = getUserLink( $composeFor );

			if( $userLink == "???" )
			{
				notice( sprintf( _UNKNOWN_USER,
					htmlspecialchars( $_GET[ "composeFor" ])));
			}
			else
			{
				?>
				<div class="header a_right">
					<? printf( _PM_COMPOSE, $userLink ) ?>
				</div>

				<form action="<?= url( "pm" ) ?>" method="post">
					<input type="hidden" name="composeFor" value="<?= htmlspecialchars( $composeFor ) ?>" />
					<input type="hidden" name="parentComment" value="<?= htmlspecialchars( $parentComment ) ?>" />
					<?

					iefixStart();

					?>
					<div class="caption" style="margin-top: -15px">
						<?= _TITLE ?>:
					</div>
					<div class="mar_bottom">
						<input class="<?= $_isIE ? "wide" : "notsowide" ?> largetext"
							name="title" type="text"
							value="<?= isset( $comData ) ? htmlspecialchars( increaseReplyCount( $comData[ "pmsTitle" ])) : "" ?>" />
					</div>
					<?

					$commentName = "comment";
					$commentDefault = "";
					$commentWide = false;

					include( INCLUDES."mod_comment.php" );

					?>
					<div class="sep">
						<button class="submit" name="sendReply"
							<?=!$commentId ? 'disabled="disabled"' : "" ?>
							onclick="el=get_by_id('<?=$commentId ?>'); if(!el.value){ alert('<?=_PM_BLANK_MESSAGE ?>'); return false } else return true"
							type="submit">
							<?=getIMG(url()."images/emoticons/checked.png")?>
							<?=_PM_SEND ?>
						</button>
					</div>
					<div class="clear">&nbsp;</div>
					<?iefixEnd()?>
				</form>
				<div><br /></div>
				<?
				$hidePMSender = true;
			}
		}
		elseif(isset($_GET["read"])) {
			echo '<div class="header a_right">'._PM_SENT_MESSAGE.'</div>';
			$comid = intval($_GET["read"]);
			$result = sql_query("SELECT * FROM `pms` WHERE `pmsCreator` = '".$_auth["useid"]."' AND `pmsid` = '$comid' ".getTwitWhere( "pmsCreator" )." LIMIT 1");
			if(mysql_num_rows($result)) {
				$comData = mysql_fetch_assoc($result);
				echo '<div class="mar_bottom" style="margin-top: -8px">';
				showComment($comData, 0, true);
				echo '</div>';
			}
			else echo '<div class="container2">'._PM_NOT_FOUND.'</div>';

			echo '<div><br /></div>';
			$hidePMSender = true;
		}

		if(!$hidePMSender) {
			?>
			<form action="<?=url("pm")?>" method="post">
			<div class="mar_bottom">
				<?=_PM_SEND_TO ?>:
				<input type="text" class="narrow" name="sendPMto" />
				<button type="submit" class="submit" style="vertical-align: middle">
					<?=_GO ?>
				</button>
			</div>
			</form>
			<?
		}
		?>
	</div>

	<div class="clear">&nbsp;</div>

	<div class="leftside">
		<? iefixStart() ?>
		<div class="header"><?=_PM_RECEIVED_MESSAGES ?></div>
		<div class="container2">
		<form action="<?= url( "pm/delete/" ) ?>" method="post">
		<input type="hidden" name="deleteMessage" value="1"/>
			<?
			$recvOffset = isset($_GET["recvOffset"]) ? intval($_GET["recvOffset"]) : 0;

			$where = array(
				"useid*" => "pmsCreator",
				"pmsPmUser" => $_auth[ "useid" ],
				"pmsReceiverDelete" => '0');

			if( isset( $_GET[ "filter" ]) && $_GET[ "filter" ] != "*" )
			{
				$where[ "useUsername" ] = $_GET[ "filter" ];
			}

			$result = sql_query("SELECT COUNT(*) FROM `pms`,`users`".dbWhere( $where ).getTwitWhere( "pmsCreator" ));

			$totalCount = mysql_result( $result, 0 );

			$result = sql_query("SELECT `pmsid`,`pmsSubmitDate`,`pmsCreator`,`pmsTitle`,`useUsername` ".
				"FROM `pms`,`users` ".dbWhere( $where ).getTwitWhere( "pmsCreator" ).
				"ORDER BY `pmsSubmitDate` DESC LIMIT $recvOffset,$_pm_per_page");

			$found = false;
			while($rowData = mysql_fetch_assoc($result)) {
				$icon = "pmin.png";
				// see if this messages is unread
				$result2 = sql_query("SELECT COUNT(*) FROM `updates` WHERE `updCreator` = '".$_auth["useid"]."' AND `updType` = '".updTypePM."' AND `updObj` = '".$rowData["pmsid"]."' LIMIT 1");
				if( mysql_result( $result2, 0 ) > 0 )
					$icon = "pmunread.png";
				else {
					// see if this messages was replied to
					$result2 = sql_query("SELECT `pmsid` FROM `pms` WHERE `pmsObj` = '".$rowData["pmsid"]."' LIMIT 1");
					if(mysql_num_rows($result2) > 0)
						$icon = "pmreplied.png";
				}
				iefixStart();
				?>
				<div class="f_left mar_bottom">
					<input type="checkbox" name="pmsid_<?=$rowData["pmsid"]?>"/>
					<a href="<?=url("pm/reply/".$rowData["pmsid"])?>">
						<?=getIMG(url()."images/emoticons/".$icon) ?>
						<?=formatText($rowData["pmsTitle"], false, true) ?>
					</a>
					<?=_PM_FROM?>
					<?=getUserLink($rowData["pmsCreator"]) ?>
				</div>
				<div class="f_right mar_bottom a_right">
					<?=gmdate($_auth["useDateFormat"], applyTimezone(strtotime($rowData["pmsSubmitDate"]))) ?>
				</div>
				<div class="clear">&nbsp;</div>
				<?
				iefixEnd();
				$found = true;
			}
			if(!$found)
				echo _PM_NONE_FOUND;
			?>
			<button type="submit" class="submit button" style="vertical-align: middle;">Delete</button>
			</form>
		</div>
		<div class="sep">
			<div class="f_right mar_right">
				<form action="<?= url( "." ) ?>" method="get">
				<select class="smalltext" name="filter" onchange="this.form.submit()">
				<option>*</option>
				<?

				$result = sql_query( "SELECT `useUsername`, COUNT(*) AS `count` FROM `pms`,`users`".dbWhere( array(
					"pmsCreator*" => "useid",
					"pmsPmUser" => $_auth[ "useid" ],
					"pmsReceiverDelete" => 0)).
					getTwitWhere( "pmsCreator" ).
					"GROUP BY `pmsCreator` ORDER BY `useUsername`" );

				while( $rowData = sql_next( $result ))
				{
					echo '<option ';

					if( isset( $_GET[ "filter" ]) && $_GET[ "filter" ] == $rowData[ "useUsername" ])
					{
						echo 'selected="selected" ';
					}

					echo 'value="'.$rowData[ "useUsername" ].'">'.$rowData[ "useUsername" ].
						" (".fuzzy_number( $rowData[ "count" ]).")</option>";
				}

				sql_free( $result );

				?>
				</select>
				</form>
			</div>
			<?navControls( $recvOffset, $_pm_per_page, $totalCount, "", "", "", "", "recvOffset",
				"text_under_icon_container_left" );?>
		</div>
		<br /><br />
		<div class="header">Delete Messages</div>
			<br />
			<div class="container2">
				<form action="<?=url(".")?>" method="get">
				<select class="smalltext" name="deleteByDate">
					<option>By Timeframe</option>
					<option value="0">Today</option>
					<option value="1">Yesterday</option>
					<option value="2">Last Week</option>
					<option value="3">Month</option>
					<option value="4">3 Months</option>
					<option value="5">6 Months</option>
					<option value="6">9 Months</option>
					<option value="7">Year</option>
					<option value="8">2 Years</option>
					<option value="9">3 Years</option>
					<option value="10">All</option>
				</select>
				<button type="submit" class="submit" style="vertical-align: middle;">Delete</button>
			</form><br /><br />
			<form action="<?=url(".")?>" method="get">
				<select class="smalltext" name="deleteByUsername">
					<option>By User</option>
					<?
						$sth = $dbh->prepare("SELECT `useid`, `useUsername`, COUNT(*) AS `count` 
								FROM `pms`, `users`
								WHERE `pmsCreator` = `useid`
								AND `pmsPmUser` = ?
								AND `pmsReceiverDelete` = '0'
								GROUP BY `pmsCreator`
								ORDER BY `useUsername`");
						$sth->bindParam(1, $_auth['useid'], PDO::PARAM_INT);
						$sth->execute();
						$result = $sth->fetchAll(PDO::FETCH_ASSOC);

						for($i = 0; $i < count($result); $i++)
						{
							echo '<option value="'.$result[$i]['useid'].'">'.$result[$i]['useUsername'].' ('.$result[$i]['count'].')</option>';
						}
					?>
				</select>
				<button type="submit" class="submit" style="vertical-align: middle;">Delete</button>
			</form>
			</div>
		<? iefixEnd() ?>
	</div>

	<div class="rightside">
		<? iefixStart() ?>
		<div class="header a_right"><?=_PM_SENT_MESSAGES ?></div>
		<div class="container2">
			<?
			$sentOffset = isset($_GET["sentOffset"]) ? intval($_GET["sentOffset"]) : 0;

			$where = array(
				"useid*" => "pmsPmUser",
				"pmsCreator" => $_auth[ "useid" ],
				"pmsCreatorDelete" => '0');

			if( isset( $_GET[ "filterOut" ]) && $_GET[ "filterOut" ] != "*" )
			{
				$where[ "useUsername" ] = $_GET[ "filterOut" ];
			}

			$result = sql_query("SELECT COUNT(*) FROM `pms`,`users`".dbWhere( $where ).getTwitWhere( "pmsCreator" ));

			$totalCount = mysql_result( $result, 0 );

			$result = sql_query("SELECT `pmsid`,`pmsSubmitDate`,`pmsPmUser`,`pmsTitle` FROM `pms`,`users` ".
				dbWhere( $where ).getTwitWhere( "pmsCreator" ).
				"ORDER BY `pmsSubmitDate` DESC LIMIT $sentOffset,$_pm_per_page");

			$found = false;
			while($rowData = mysql_fetch_assoc($result)) {
				iefixStart();
				?>
                <form action="<?= url( "pm/delete/" ) ?>" method="post">
                <input type="hidden" name="deleteSentMessage" value="1" />
				<div class="f_left mar_bottom">
                	<input type="checkbox" name="pmsid_<?=$rowData["pmsid"]?>" />
					<a href="<?=url("pm/read/".$rowData["pmsid"])?>">
						<?=getIMG(url()."images/emoticons/pmout.png") ?>
						<?=formatText($rowData["pmsTitle"], false, true) ?>
					</a>
					<?=_PM_FOR ?>
					<?=getUserLink($rowData["pmsPmUser"]) ?>
				</div>
				<div class="f_right mar_bottom a_right">
					<?= date($_auth['useDateFormat'], applyTimezone(strtotime($rowData["pmsSubmitDate"]))); ?>
				</div>
				<div class="clear">&nbsp;</div>
				<?
				iefixEnd();
				$found = true;
			}
			if(!$found)
				echo _PM_NONE_FOUND;
			?>
        <button type="submit" class="submit button" style="vertical-align: middle;">Delete</button>
        </form>
		</div>
		<div class="sep">
			<div class="f_left mar_left">
				<form action="<?= url( "." ) ?>" method="get">
				<select class="smalltext" name="filterOut" onchange="this.form.submit()">
				<option>*</option>
				<?

				$result = sql_query( "SELECT `useUsername`, COUNT(*) AS `count` FROM `pms`,`users` ".dbWhere( array(
					"pmsPmUser*" => "useid",
					"pmsCreatorDelete" => '0',
					"pmsCreator" => $_auth[ "useid" ])).
					getTwitWhere( "pmsPmUser" ).
					"GROUP BY `pmsPmUser` ORDER BY `useUsername`" );

				while( $rowData = sql_next( $result ))
				{
					echo '<option ';

					if( isset( $_GET[ "filterOut" ]) && $_GET[ "filterOut" ] == $rowData[ "useUsername" ])
					{
						echo 'selected="selected" ';
					}

					echo 'value="'.$rowData[ "useUsername" ].'">'.$rowData[ "useUsername" ].
						" (".fuzzy_number( $rowData[ "count" ]).")</option>";
				}

				sql_free( $result );

				?>
				</select>
				</form>
			</div>
			<?navControls( $sentOffset, $_pm_per_page, $totalCount, "", "", "", "", "sentOffset" );?>
		</div>
        <br /><br />
		<div class="header">Delete Messages</div>
			<br />
			<div class="container2">
				<form action="<?=url(".")?>" method="get">
				<select class="smalltext" name="deleteByDateSender">
					<option>By Timeframe</option>
					<option value="0">Today</option>
					<option value="1">Yesterday</option>
					<option value="2">Last Week</option>
					<option value="3">Month</option>
					<option value="4">3 Months</option>
					<option value="5">6 Months</option>
					<option value="6">9 Months</option>
					<option value="7">Year</option>
					<option value="8">2 Years</option>
					<option value="9">3 Years</option>
					<option value="10">All</option>
				</select>
				<button type="submit" class="submit" style="vertical-align: middle;">Delete</button>
			</form><br /><br />
			<form action="<?=url(".")?>" method="get">
				<select class="smalltext" name="deleteByUsernameSender">
					<option>By User</option>
					<?
						$sth = $dbh->prepare("SELECT `useid`, `useUsername`, COUNT(*) as `count`
											FROM `pms`, `users`
											WHERE `pmsPmUser` = `useid`
											AND `pmsCreator` = ?
											AND `pmsCreatorDelete` = '0'
											GROUP BY `pmsPmUser`
											ORDER BY `useUsername`");
						$sth->bindParam(1, $_auth['useid'], PDO::PARAM_INT);
						$sth->execute();
						$result = $sth->fetchAll(PDO::FETCH_ASSOC);

						for($i = 0; $i < count($result); $i++)
						{
							echo '<option value="'.$result[$i]['useid'].'">'.$result[$i]['useUsername'].' ('.$result[$i]['count'].')</option>';
						}
					?>
				</select>
				<button type="submit" class="submit" style="vertical-align: middle;">Delete</button>
			</form>
			</div>
		<? iefixEnd() ?>
	</div>

	<div class="clear">&nbsp;</div>
</div>
<?

// -----------------------------------------------------------------------------
// Recalculates the number of "Re:" in the private message's subject and
// returns a new subject with increased number of "Re:".

function increaseReplyCount( $pmSubject )
{
	global $__ReCount;

	$__ReCount = 1;

	while( preg_match( '/^Re[0-9\(\)]*\:/', $pmSubject ))
	{
		$pmSubject = preg_replace_callback( '/^Re([0-9\(\)]*)\:[\s]*/',
			"increaseReplyCount_cb", $pmSubject );
	}

	if( $__ReCount == 1 )
	{
		$pmSubject = "Re: ".$pmSubject;
	}
	else
	{
		$pmSubject = "Re(".$__ReCount."): ".$pmSubject;
	}

	return $pmSubject;
}

// Callback used in the function above.

function increaseReplyCount_cb( $sender )
{
	global $__ReCount;

	$n = intval( preg_replace( '/[^0-9]/', "", $sender[ 1 ]));

	if( $n == 0 )
		$n = 1;

	$__ReCount += $n;

	return "";
}

?>
