<?

$_documentTitle = _BROWSE.": "._MOST_RECENT_FAVS;

?>
<div class="header">
	<div class="header_title">
		<?= _BROWSE ?>
		<div class="subheader"><?= _MOST_RECENT_FAVS ?></div>
	</div>
	<?

	$active = 7;

	include( INCLUDES."mod_browsemenu.php" );

	?>
</div>

<div class="container">
	<?

	include_once( INCLUDES."gallery.php" );

	$where = "`objPending` = '0' AND `objDeleted` = '0' ".
		"AND `objPopularity` >= '2000' ".
		"AND `objSubmitDate` > DATE_SUB(NOW(), INTERVAL ".
		$_config[ "mostRecentDays" ]." DAY)";

	applyObjFilters( $where );

	$objList = array();

	$result = sql_query( "SELECT DISTINCT `objid` FROM `objects` WHERE $where ".
		"ORDER BY `objPopularity` DESC, `objSubmitDate` DESC LIMIT 96" );

	while( $objData = mysql_fetch_row( $result ))
	{
		$objList[] = $objData[ 0 ];
	}

	showThumbnails( array(
		"noMostFaved" => true,
		"where" => "`objid` IN('".implode( "','", $objList )."')" ));

	?>
</div>
