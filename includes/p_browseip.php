<?php 

if( !atLeastModerator() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

elseif( atLeastModerator() ) {
	
	$_documentTitle = "IP Search";
	
?>

<div class="header">
	<div class="header_title">
		<?= _ADMINISTRATION ?>
		<div class="subheader">IP Search</div>
	</div>
	<?php 
		$active = 9;
		include(INCLUDES."mod_adminmenu.php");
	
	?>
</div>

	
<div class="container">

	<div class="mar_bottom container2 notsowide">
			<?=iefixStart()?>
			<div class="mar_bottom">
				<b>Enter IP</b>:
				<form action="<?= url( "." )?>" method="post">
					<input type="text" name="useIPReq" size="50" /> 
				    <input class="submit" type="submit" name="submitIPSearch" value="Search" />
				</form>
			</div>
			
			
				<?php 
					if( isset($_POST[ "useIPReq" ])) {
						$useIPReq = $_POST[ "useIPReq" ];
						$result = sql_query( "SELECT `useEid` FROM `useExtData` WHERE `useLastIp` LIKE '%".getHexIP( $useIPReq )."%' LIMIT 90" );
						$resultcount = mysql_num_rows( $result );
						if( $resultcount == 0 )
						{
							echo "No users found with this IP.";
						}
						else {
                            echo "<h3>Users who were last active at: ".$useIPReq." (".getHexIP( $useIPReq ).")</h3>";
                            while( $user = mysql_fetch_assoc( $result ))
                            {
                                echo 'User '.getUserLink($user[ "useEid" ]);
                                echo '<br>';
                            }
						}
					}
				
				?>
			
			</div>
			<?=iefixEnd()?>

</div>
<?php 
}
?>