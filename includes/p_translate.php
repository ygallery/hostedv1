<?

$_documentTitle = "Translator's Tool";

$lang = preg_replace('/[^A-Za-z\-]/',"",$_cmd[1]);
$offset = isset($_GET["offset"]) ? intval($_GET["offset"]) : 0;

// check if the user is allowed to make translations for this language
$result = sql_query("SELECT `traid` FROM `translators` WHERE `traUser` = '".$_auth["useid"]."' AND `traLanguage` = '$lang' LIMIT 1");
if(!mysql_num_rows($result) && !atLeastModerator())
{
	include(INCLUDES."p_notfound.php");
	return;
}

if(!isset($_GET["order"])) $_GET["order"] = 1; // Show New/Updated by default

if(!isset($_GET["limit"])) $_GET["limit"] = 0; // Show 12 strings per page by default

switch(intval($_GET["limit"])) {
	case 1: $limit = 8; break;
	case 2: $limit = 24; break;
	case 3: $limit = 48; break;
	default: $limit = 12;
}

if(isset($_POST["submit"]))
{
	foreach($_POST as $key => $value)
	{
		if($key == "submit" || trim($value) == "")
			continue;

		$result = sql_query("SELECT `strCategory` FROM `strings` WHERE `strName` = '".addslashes($key)."' AND `strLanguage` = 'en'");

		if($rowData = mysql_fetch_row($result))
		{
			$category = $rowData[0];

			$result2 = sql_query("SELECT `strid` FROM `strings` WHERE `strLanguage` = '$lang' AND `strName` = '".addslashes($key)."' LIMIT 1");

			if(mysql_num_rows($result2) > 0)
			{
				// string already translated and needs just an update
				sql_query("UPDATE `strings` SET `strText` = '".addslashes(trim($value))."', `strLastModified` = NOW() ".
					"WHERE `strLanguage` = '$lang' AND `strName` = '".addslashes($key)."' LIMIT 1");
			}
			else
			{
				// string has not been translated yet, it is new and needs to be inserted
				sql_query("INSERT INTO `strings`(`strLanguage`,`strName`,`strText`,`strLastModified`,`strCategory`) ".
					"VALUES('$lang','".addslashes($key)."','".addslashes(trim($value))."',NOW(),'".addslashes($category)."')");
			}
		}
	}

	redirect(url(".", array("offset" => $offset, "limit" => $_GET["limit"], "order" => $_GET["order"])));
}

// query new/updated strings

switch(intval($_GET["order"])) {
	case 1:
		$result = sql_query("SELECT COUNT(*) FROM `strings` AS s1 ".
			"LEFT JOIN `strings` AS s2 ON s2.`strLanguage` = '$lang' ".
			"AND s1.`strName` = s2.`strName` ".
			"WHERE s1.`strLanguage` = 'en' ".
			"AND (s2.`strName` IS NULL OR s1.`strLastModified` > s2.`strLastModified`)");
		$totalCount = mysql_result( $result, 0 );

		$result = sql_query("SELECT DISTINCT s1.* FROM `strings` AS s1 ".
			"LEFT JOIN `strings` AS s2 ON s2.`strLanguage` = '$lang' ".
			"AND s1.`strName` = s2.`strName` ".
			"WHERE s1.`strLanguage` = 'en' ".
			"AND (s2.`strName` IS NULL OR s1.`strLastModified` > s2.`strLastModified`) ".
			"ORDER BY s1.`strCategory`,s1.`strName` LIMIT $offset,".($limit + 1));
		break;

	default:
		$result = sql_query("SELECT COUNT(*) FROM `strings` WHERE `strLanguage` = 'en'");

		$totalCount = mysql_result( $result, 0 );

		$result = sql_query("SELECT * FROM `strings` WHERE `strLanguage` = 'en' ".
			"ORDER BY `strCategory`,`strName` LIMIT $offset,".($limit + 1));
}

$newData = array();
$newDataCount = mysql_num_rows($result);
$togo = $limit;
while($strData = mysql_fetch_assoc($result))
{
	// query the current translation of this string
	$result2 = sql_query("SELECT * FROM `strings` WHERE `strLanguage` = '$lang' AND `strName` = '".addslashes($strData["strName"])."' LIMIT 1");

	if(mysql_num_rows($result2) > 0)
	{
		$langData = mysql_fetch_assoc($result2);

		$strData["strOldText"] = $langData["strText"];

		if(trim($langData["strText"]) == "" ||
			$strData["strLastModified"] > $langData["strLastModified"])
			$strData["strHighlight"] = true;
	}
	else
		$strData["strHighlight"] = true;

	$newData[$strData["strName"]] = $strData;

	$togo--;
	if(!$togo) break;
}

$getVars = array();
if(isset($_GET["offset"])) $getVars["offset"] = $_GET["offset"];
if(isset($_GET["limit"])) $getVars["limit"] = $_GET["limit"];
if(isset($_GET["order"])) $getVars["order"] = $_GET["order"];

?>
<div class="header">
	<div class="header_title">Translator's tool</div>
</div>
<div class="container">

<div class="notsowide" style="display: none" id="translateHint">
	<div class="mar_bottom">
		Welcome to the Translator's Tool!
	</div>
	<div class="mar_bottom">
		There are over 500 strings to translate and it takes minimum 3-4 hours to
		translate them all.
	</div>
	<div class="mar_bottom">
		But don't worry. Please translate as many strings as you can translate properly,
		without getting tired;
		while other strings are non-translated, they will just be presented to the
		users as original English strings.
		You may finish the translation at any time.
	</div>
	<div class="mar_bottom">
		If you don't know the proper translation then it's much better to just leave the
		string non-translated.
	</div>
	<div class="mar_bottom">
		Special characters such as %s, %u, %d or BBCode tags such as [url=...][/url], [b], [i]
		<b>must be</b> exactly preserved in the translated string (they will be replaced with
		contents at the browsing time); you may move these special characters inside the string
		for proper translation but you <b>must not</b> change their order inside the string in case
		there's more than one %s, %u or %d character.
	</div>
	<div class="mar_bottom">
		Example: &quot;%s has added %s to their favourites&quot;. The first %s will be replaced
		with the user name and the second with the submission title. You have to translate
		it the way that the order of the two %s characters remain the same: the %s for the
		user name must go first in the string and the %s for the submission title must
		go after (e.g. you must not translate it as something like
		&quot;%s has been added to %s's favourites&quot; because it will change the semantic
		order of the two %s characters).
	</div>
	<div class="mar_bottom">
		For some languages it's hard to translate such terms as &quot;favourites&quot; and
		&quot;submissions&quot;.
		We figured that you can translate &quot;favourites&quot; as &quot;selected&quot;
		and &quot;submissions&quot; as &quot;works&quot;.
		You may want to do something like that with some other terms, just make sure
		the the users will understand them.
	</div>
	<div class="mar_bottom">
		Thank you very much for your help with the translation!
	</div>
	<div class="hline">&nbsp;</div>
</div>
<div class="a_center">
	<div class="minibutton" onclick="toggle_visibility('translateHint')">
		Translation hints
	</div>
	<?
	navControls( $offset, $limit, $totalCount );
	?>
	<form action="<?=url(".")?>" method="get">
	<?
	foreach($_GET as $key => $value)
		if($key != "order" && $key != "limit" && $key != "offset")
			echo '<input name="'.htmlspecialchars($key).'" type="hidden" value="'.htmlspecialchars($value).'" />';
	?>
	<select name="order">
		<option <?=isset($_GET["order"]) && $_GET["order"] == 0 ? 'selected="selected"' : "" ?> value="0">All strings</option>
		<option <?=isset($_GET["order"]) && $_GET["order"] == 1 ? 'selected="selected"' : "" ?> value="1">New/Updated</option>
	</select>
	<select name="limit">
		<option <?=isset($_GET["limit"]) && $_GET["limit"] == 1 ? 'selected="selected"' : "" ?> value="1"><?=fuzzy_number(8)?></option>
		<option <?=isset($_GET["limit"]) && $_GET["limit"] == 0 ? 'selected="selected"' : "" ?> value="0"><?=fuzzy_number(12)?></option>
		<option <?=isset($_GET["limit"]) && $_GET["limit"] == 2 ? 'selected="selected"' : "" ?> value="2"><?=fuzzy_number(24)?></option>
		<option <?=isset($_GET["limit"]) && $_GET["limit"] == 3 ? 'selected="selected"' : "" ?> value="3"><?=fuzzy_number(48)?></option>
	</select>
	<input class="submit" type="submit" value="<?=_UPDATE ?>" style="vertical-align: middle" />
	</form>
</div>
<div class="hline">&nbsp;</div>

<table><tr><td><div class="smalltext">Translated:</div></td><td width="100%">
<?

// translation progress bar

$result = sql_query("SELECT COUNT(*) FROM `strings` AS s1 ".
	"LEFT JOIN `strings` AS s2 ON s2.`strLanguage` = '$lang' AND s1.`strName` = s2.`strName` ".
	"WHERE s1.`strLanguage` = 'en' ".
	"AND (s2.`strName` IS NULL OR s1.`strLastModified` > s2.`strLastModified`)");
$totalTranslated = mysql_result($result, 0);

$result = sql_query("SELECT COUNT(*) FROM `strings` ".
	"WHERE `strLanguage` = 'en'");
$totalStrings = mysql_result($result, 0);

include_once(INCLUDES."polling.php");
showPollBar($totalStrings - $totalTranslated, $totalStrings, $totalStrings);

?>
</td></tr></table>

<form action="<?=url(".", array("offset" => $offset, "limit" => $_GET["limit"], "order" => $_GET["order"]))?>" method="post">
<table><tr><td width="100%"><div class="header a_center">English string</div></td>
<td><div class="header a_center">Translation</div></td></tr>
<?

foreach($newData as $strData)
{
	?>
	<tr>
	<td valign="top">
		<div class="container2">[<span class="error"><?=$strData["strCategory"]?></span>] &nbsp;
		<?=formatText($strData["strText"], true, true) ?>
		</div>
	</td>
	<td valign="top">
		<textarea cols="50" rows="2"
			<?=isset($strData["strHighlight"]) ? 'style="border: 2px solid red"' : "" ?>
			name="<?=$strData["strName"]?>"><?=isset($strData["strOldText"]) ?
				htmlspecialchars($strData["strOldText"]) : "" ?></textarea>
	</td>
	</tr>
	<?
}

?>
</table>
<div class="sep a_center mar_bottom">
<input class="largetext" type="submit" name="submit" value="Submit translations" />
</div>
</form>
</div>
