<?

if( !atLeastModerator() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

?>
<div class="header">
	<div class="header_title">
		<?= _ADMINISTRATION ?>
		<div class="subheader"><?= _HOME ?></div>
	</div>	
	<?

	$active = 1;
	include(INCLUDES."mod_adminmenu.php");

	?>
</div>

<div class="container">
	<h1>Search for users</h1>
	<form action="<?= url( "." )?>" method="post">
		<div class="leftside">
			<div class="container2">
				<div style="margin-left : auto; margin-right : 0;">
					<label for="username">Username</label>
					<? selectSearchType("useUsername","text") ?>
					<input id="username" name="useUsername" />
				</div>
				<div class="sep">
					<label for="realName">Real name</label>
					<? selectSearchType("useRealName","text") ?>
					<input id="realName" name="useRealName" />
				</div>
				<div class="sep">
					<label for="lastAction">Last action</label>
					<? selectSearchType("useLastAction","number") ?>
					<input id="lastAction" name="useLastAction" />
				</div>
				<div class="sep">
					<label for="lastSubmission">Last submission</label>
					<? selectSearchType("useLastSubmission","number") ?>
					<input id="lastSubmission" name="useLastSubmission" />
				</div>
				<div class="sep">
					<label for="isSuspended">Suspended?</label>
					<select id="isSuspended" name="useIsSuspended">
						<option value="">Don't care</option>
						<option value="1">Yes</option>
						<option value="0">No</option>
					</select>
				</div>
				<div class="sep">
					<label for="isBanned">Banned?</label>
					<select id="isBanned" name="useIsBanned">
						<option value="">Don't care</option>
						<option value="1">Yes</option>
						<option value="0">No</option>
					</select>
				</div>
			</div>
		</div>
		<div class="rightside">
			<div class="container2">
				<div class="sep">
					<label for="lastIp">Last IP</label>
					<? selectSearchType("useLastIp","text") ?>
					<input id="lastIp" name="useLastIp" />
				</div>
				<div class="sep">
					<label for="email">Email</label>
					<? selectSearchType("useEmail","text") ?>
					<input id="email" name="useEmail" />
				</div>
				<div class="sep">
					<label for="signupDate">Signup date</label>
					<? selectSearchType("useSignupDate","number") ?>
					<input id="signupDate" name="useSignupDate" />
				</div>
				<div class="sep">
					<label for="AIM">AIM</label>
					<? selectSearchType("useAIM","text") ?>
					<input id="AIM" name="useAIM" />
				</div>
				<div class="sep">
					<label for="ICQ">ICQ</label>
					<? selectSearchType("useICQ","text") ?>
					<input id="AIM" name="useICQ" />
				</div>
				<div class="sep">
					<label for="MSN">MSN</label>
					<? selectSearchType("useMSN","text") ?>
					<input id="MSN" name="useMSN" />
				</div>
				<div class="sep">
					<label for="YIM">Yahoo!</label>
					<? selectSearchType("useYIM","text") ?>
					<input id="YIM" name="useYIM" />
				</div>
				<div class="sep">
					<label for="jabber">Jabber</label>
					<? selectSearchType("useJabber","text") ?>
					<input id="jabber" name="useJabber" />
				</div>
				<div class="sep">
					<label for="birthday">Birthday</label>
					<? selectSearchType("useBirthday","number") ?>
					<input id="birthday" name="useBirthday" />
				</div>
				<div class="sep">
					<label for="lastUpdate">Last update</label>
					<? selectSearchType("useLastUpdate","number") ?>
					<input id="lastUpdate" name="useLastUpdate" />
				</div>
			</div>
		</div>
		<div class="clear">
			<input type="submit" name="submit" value="Search" />
			<div class="note">Use sparingly; can be slow!</div>
		</div>
	</form>
</div>

<?
function selectSearchType($fieldName,$fieldType="text")
{
?>
	<select name="sType<?=$fieldName ?>" id="sType<?=$fieldName ?>">
<?
	if($fieldType == "number")
	{
?>
		<option>&lt;</option>
		<option>&lt;=</option>
		<option>&gt;=</option>
		<option>&gt;</option>
<?
	}
?>
		<option selected="selected">=</option>
		<option>!=</option>
		<option>LIKE</option>
		<option>NOT LIKE</option>
<?
	if($fieldType == "text")
	{
?>
		<option>REGEXP</option>
		<option>NOT REGEXP</option>
<?
	}
?>
	</select>
<?
};
?>