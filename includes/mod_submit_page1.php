<div class="caption">
	<?= _TITLE ?>:
</div>
<div>
	<input class="notsowide largetext" id="submitTitle" name="title" type="text"
		<?= isset( $_POST[ "title" ]) ?
			'value="'.htmlspecialchars( $_POST[ "title" ]).'"' : "" ?> />
</div>
<?

if( isset( $requireTitle ))
{
	notice( _SUBMIT_NO_TITLE );
}

?>
<div class="sep caption">
	<?= _COMMENT ?>:
</div>
<table cellspacing="0" cellpadding="0" border="0" class="notsowide"><tr><td>
<?

$commentDefault = isset( $_POST[ "comment" ]) ? $_POST[ "comment" ] : "";
$commentNoOptions = true;

include( INCLUDES."mod_comment.php" );

if( isset( $requireComment ))
{
	notice( _SUBMIT_NO_COMMENT );
}

?>
</td></tr></table>
<?

$found = false;

ob_start();

?>
<div class="sep caption"><?=_SUBMIT_TO_FOLDER ?>:</div>
<div class="f_left mar_right mar_top">
	<?= getIMG( url()."images/emoticons/folder.png" ) ?>
</div>
<select name="folder" class="largetext">
	<option value="0">( <?= _NONE ?> )</option>
	<?

	$folResult = sql_query( "SELECT `folid`, `folName` FROM `folders`".dbWhere( array(
		"folCreator" => $clubMember ))."ORDER BY `folName`" );

	while( $folData = mysql_fetch_assoc( $folResult ))
	{
		?>
		<option <?= isset( $_POST[ "folder" ]) && $_POST[ "folder"] == $folData[ "folid" ] ?
			'selected="selected"' : "" ?>
			value="<?= $folData[ "folid" ]?>"><?= htmlspecialchars( $folData[ "folName" ]) ?></option>
		<?

		$found = true;
	}

	?>
</select>
&nbsp;
<a target="new-tab" href="<?= url( "helpdesk/faq/general/folders" ) ?>">
	<?= getIMG( url()."images/emoticons/help.png" ) ?>
	<?= _WHATS_THIS ?></a>
<div class="clear">&nbsp;</div>
<?

if( $found )
{
	ob_end_flush();
}
else
{
	ob_end_clean();

	?>
	<input type="hidden" name="folder" value="0" />
	<?
}

?>
<div class="sep caption"><?= _SUBMIT_FOR_CLUB ?>:</div>
<?

function putClubList( $clubMember, $clubVar )
{
	?>
	<option value="0">( <?= _NONE ?> )</option>
	<?

	$sql = "SELECT `cluid`,`cluName` FROM `useClubs`,`clubs` ".
		"WHERE `cluid` = `useCclub` ".
		"AND `useCmember` = '$clubMember' ".
		"AND `useCpending` = '0' ".
		"ORDER BY `cluName`";

	$cluResult = sql_query( $sql );

	while( $cluData = mysql_fetch_assoc( $cluResult ))
	{
		?>
		<option <?= isset( $_POST[ $clubVar ]) && $_POST[ $clubVar ] == $cluData[ "cluid" ] ?
			'selected="selected"' : "" ?>
			value="<?= $cluData[ "cluid" ]?>"><?= htmlspecialchars( $cluData[ "cluName" ]) ?></option>
		<?
	}

	mysql_free_result( $cluResult );
}

?>
<div class="mar_right mar_top">
	<?= getIMG( url()."images/emoticons/club2.png" ) ?>
	<select name="forClub" class="largetext"><? putClubList( $clubMember, "forClub" ) ?></select>
	&nbsp; (Primary)
	&nbsp;
	<a target="new-tab" href="<?= url( "helpdesk/faq/general/clubs" ) ?>">
		<?= getIMG( url()."images/emoticons/help.png" ) ?>
		<?= _WHATS_THIS ?></a>
</div>
<div class="mar_right mar_top">
	<?= getIMG( url()."images/emoticons/club2.png" ) ?>
	<select name="forClub2" class="largetext"><? putClubList( $clubMember, "forClub2" ) ?></select>
	&nbsp; (Secondary)
</div>
<div class="mar_right mar_top">
	<?= getIMG( url()."images/emoticons/club2.png" ) ?>
	<select name="forClub3" class="largetext"><? putClubList( $clubMember, "forClub3" ) ?></select>
	&nbsp; (Tertiary)
</div>

<div class="clear">&nbsp;</div>
<?

$confirmed = false;

if( $_cmd[ 0 ] == "edit" )
{
	$sql = "SELECT `objCollabConfirmed` FROM `objExtData`".dbWhere( array(
		"objEid" => intval( $_cmd[ 1 ])));

	$colResult = sql_query( $sql );

	if( $colData = mysql_fetch_row( $colResult ))
	{
		$confirmed = $colData[ 0 ];
	}

	mysql_free_result( $colResult );
}

if( !$confirmed || atLeastSModerator() )
{
	?>
	<div class="sep caption"><?= _SUBMIT_COLLAB ?>:</div>
	<div class="f_left mar_right mar_top">
		<?= getIMG( url()."images/emoticons/club.png" ) ?>
	</div>
	<select name="collab" class="largetext">
		<option value="0">( <?= _NONE ?> )</option>
		<?

		$sql = "SELECT `useid`, `useUsername` FROM `watches`, `users` ".
			"WHERE `useid` = `watCreator` ".
			"AND `watUser` = '$clubMember' ".
			"AND `watType` = 'use' ".
			"AND `useIsBanned` = '0'".
			"AND `useIsSuspended` = '0'".
			"ORDER BY `useUsername`";

		$watResult = sql_query( $sql );

		while( $watData = mysql_fetch_assoc( $watResult ))
		{
			?>
			<option <?= isset( $_POST[ "collab" ]) && $_POST[ "collab" ] == $watData[ "useid" ] ?
				'selected="selected"' : "" ?>
				value="<?= $watData[ "useid" ]?>"><?= htmlspecialchars( $watData[ "useUsername" ]) ?></option>
			<?
		}

		mysql_free_result( $watResult );

		?>
	</select>
	&nbsp;
	<a target="new-tab" href="<?= url( "helpdesk/faq/general/collabs" ) ?>">
		<?= getIMG( url()."images/emoticons/help.png" ) ?>
		<?= _WHATS_THIS ?></a>
	<?
}

?>
<div class="sep caption"><?= _SUBMIT_GIFT ?>:</div>
<div class="f_left mar_right mar_top">
	<?= getIMG( url()."images/emoticons/gift.png" ) ?>
</div>
<select name="gift" class="largetext">
	<option value="0">( <?= _NONE ?> )</option>
	<?

	$sql = "SELECT `useid`, `useUsername` FROM `watches`, `users` ".
		"WHERE `useid` = `watCreator` ".
		"AND `watUser` = '$clubMember' ".
		"AND `watType` = 'use' ".
		"ORDER BY `useUsername`";

	$watResult = sql_query( $sql );

	while( $watData = mysql_fetch_assoc( $watResult ))
	{
		?>
		<option <?= isset( $_POST[ "gift" ]) && $_POST[ "gift" ] == $watData[ "useid" ] ?
			'selected="selected"' : "" ?>
			value="<?= $watData[ "useid" ]?>"><?= htmlspecialchars( $watData[ "useUsername" ]) ?></option>
		<?
	}

	mysql_free_result( $watResult );

	?>
</select>
&nbsp;
<a target="new-tab" href="<?= url( "helpdesk/faq/general/gifts" ) ?>">
	<?= getIMG( url()."images/emoticons/help.png" ) ?>
	<?= _WHATS_THIS ?></a>

<div class="clear">&nbsp;</div>

<div><br /></div>
