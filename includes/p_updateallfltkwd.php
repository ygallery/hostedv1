<?

// Updates keywords under "Filters" for ALL submissions.

if( !atLeastSModerator() )
{
	return;
}

include_once( INCLUDES."submission.php" );

if( !isset( $_GET[ "first" ]) || !isset( $_GET[ "last" ]))
{
	redirect( url( ".", array( "first" => 1, "last" => 2000 )));
}

$first = intval( $_GET[ "first" ]);
$last = intval( $_GET[ "last" ]);
$isFinished = false;

$result = sql_query( "SELECT MAX(`objid`) FROM `objects`" );

if( $data = mysql_fetch_row( $result ))
{
	if( $last > $data[ 0 ])
	{
		$last = $data[ 0 ];
		$isFinished = true;
	}
}

include_once( INCLUDES."files.php" );

for( $objid = $first; $objid <= $last; $objid++ )
{
	updateFilterKeywords( $objid );
}

echo "Updating filters and keywords for submissions from $first to $last...";

$_documentTitle = "$first to $last";

if( !$isFinished )
{
	?>

<script type="text/javascript">
//<![CDATA[
	window.setTimeout(
		"document.location='<?= url( ".", array( "first" => $last + 1, "last" => $last + 2000 ), '&') ?>';", 500);
//]]>
</script>

	<?
}

?>