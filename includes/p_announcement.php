<?

if($_cmd[1] == "") {
	$_cmd[2] = $_cmd[1];
	$_cmd[1] = strtolower($_auth["useUsername"]);
}

$journalAction = isset($_GET["action"]) ? $_GET["action"] : "";

$cluid = intval($_cmd[1]);

$result = sql_query("SELECT * FROM `clubs`,`cluExtData` WHERE `cluEid` = `cluid` AND `cluid` = '".$cluid."' LIMIT 1");
if(!$cluData = mysql_fetch_assoc($result)) {
	include(INCLUDES."p_notfound.php");
	return;
}

$jouid = $_cmd[2] != "" ? intval($_cmd[2]) : 0;

// TODO: separate to pages if there are too many entries
$entries = array();
$result = sql_query("SELECT * FROM `journals` WHERE `jouCreatorType` = 'clu' AND `jouCreator` = '".$cluData["cluid"]."' ORDER BY `jouSubmitDate` DESC");
while($jouData = mysql_fetch_assoc($result)) {
	$entries[] = $jouData;
	if($jouData["jouid"] == $jouid)
		$currentEntry = $jouData;
}
if(!$jouid && isset($entries[0]["jouid"])) {
	$jouid = $entries[0]["jouid"];
	$currentEntry = $entries[0];
}
// if it is a specific journal entry, make sure it exists and belongs to the user
if($jouid) {
	$result = sql_query("SELECT `jouid` FROM `journals` WHERE `jouCreatorType` = 'clu' AND `jouCreator` = '".$cluData["cluid"]."' AND `jouid` = '$jouid' LIMIT 1");
	if(!mysql_num_rows($result)) {
		include(INCLUDES."p_notfound.php");
		return;
	}
}

// only the club member can do special actions (post/edit/delete)

if( $journalAction != "" &&
	!isClubMember( $_auth[ "useid" ], $cluData[ "cluid" ]) &&
	!atLeastSModerator() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

$_documentTitle = $cluData["cluName"].": "._ANNOUNCEMENT;

?>

<div class="header">
	<div class="f_right mar_left a_center normaltext">
		<?=getClubIcon($cluData["cluid"])?>
	</div>
	<div class="header_title">
		<?=$cluData["cluName"]?>
		<div class="subheader"><?=_ANNOUNCEMENTS ?></div>
	</div>
	<?
	$active = 5;
	include(INCLUDES."mod_clubmenu.php");
	?>
</div>

<div class="container">

	<?=iefixStart()?>
	<div class="f_left leftside"><div class="header"><?
		switch($journalAction) {
			case "post": echo _JOURNAL_NEW_ENTRY; break;
			case "edit": echo _JOURNAL_EDIT_ENTRY; break;
			case "delete": echo _JOURNAL_DELETE_ENTRY; break;
			default:
				echo _ANNOUNCEMENT;
				// also the New Entry button:
				if(isClubMember($_auth["useid"], $cluData["cluid"])) {
					$url = url(".", array("action" => "post"));
					?>
					&nbsp; &nbsp;
					<div class="button normaltext" style="float: none; display: inline; margin-right: -8px" onclick="document.location='<?=$url?>'">
						<a href="<?=$url?>">
						<?=getIMG(url()."images/emoticons/journal.png") ?>
						<?=_JOURNAL_NEW_ENTRY ?>
						</a>
					</div>
					<?
				}
		}
	?></div></div>
	<div class="f_right a_right"><div class="header"><?
		switch($journalAction) {
			case "post":
			case "edit":
			case "delete":
				echo _ANNOUNCEMENT_PAST_ENTRIES;
				break;
			default:
				if($jouid)
					echo isset($_GET["replied"]) ? _COMMENTS : _JOURNAL_LEAVE_COMMENT;
		}
	?></div></div>
	<div class="clear">&nbsp;</div>
	<?=iefixEnd()?>

	<div class="leftside">
		<?
			$ableToPost = isClubMember($_auth["useid"], $cluData["cluid"]) || atLeastSModerator();

			if($ableToPost && isset($_POST["postNewEntry"])) {
				$noEmoticons = isset($_POST["entryNoEmoticons"]) ? 1 : 0;
				$noSig = isset($_POST["entryNoSig"]) ? 1 : 0;
				$noBBCode = isset($_POST["entryNoBBCode"]) ? 1 : 0;
				$title = addslashes($_POST["title"]);
				if(!$title) $title = _ANNOUNCEMENT_DEFAULT_TITLE;
				$entry = substr(addslashes($_POST["entry"]), 0, 50000);
				$userIp = getHexIp($_SERVER["REMOTE_ADDR"]);
				sql_query("INSERT INTO `journals`(`jouCreatorType`,`jouCreator`,`jouAnnCreator`,`jouSubmitIp`,`jouSubmitDate`,`jouLastEdit`,`jouTitle`,`jouEntry`,`jouNoEmoticons`,`jouNoSig`,`jouNoBBCode`) ".
					"VALUES('clu','".$cluData["cluid"]."','".$_auth["useid"]."','".$userIp."',NOW(),NOW(),'".$title."','".$entry."','$noEmoticons','$noSig','$noBBCode')");
				$jouidNew = mysql_insert_id(); // get the id of the newly added entry

				// notify watchers about the journal update
				addUpdateToClubWatchers( updTypeAnnouncement, $_auth[ "useid" ], $jouidNew, $cluid );

				redirect(url("announcement/".$cluid));
			}

			if($ableToPost && isset($_POST["editOldEntry"])) {
				$noEmoticons = isset($_POST["entryNoEmoticons"]) ? 1 : 0;
				$noSig = isset($_POST["entryNoSig"]) ? 1 : 0;
				$noBBCode = isset($_POST["entryNoBBCode"]) ? 1 : 0;
				$title = addslashes($_POST["title"]);
				if(!$title) $title = _ANNOUNCEMENT_DEFAULT_TITLE;
				$entry = substr(addslashes($_POST["entry"]), 0, 50000);
				$userIp = getHexIp($_SERVER["REMOTE_ADDR"]);
				sql_query("UPDATE `journals` SET `jouSubmitDate` = NOW(), ".
					"`jouEditIp` = '".$userIp."', `jouTitle` = '".$title."', ".
					"`jouEntry` = '".$entry."', `jouNoEmoticons` = '$noEmoticons', ".
					"`jouNoSig` = '$noSig', `jouNoBBCode` = '$noBBCode' ".
					"WHERE `jouid` = '$jouid' LIMIT 1");

				// notify watchers about the journal update

				if( isClubMember($_auth["useid"], $cluData["cluid"]))
				{
					addUpdateToClubWatchers( updTypeAnnouncement, $_auth[ "useid" ], $jouid, $cluid );
				}

				redirect(url("announcement/".$cluid."/".$jouid));
			}

			if($journalAction == "delete" &&
				( $currentEntry["jouAnnCreator"] == $_auth["useid"] || atLeastSModerator() )) {
				sql_query("DELETE FROM `journals` WHERE `jouid` = '$jouid' LIMIT 1");
				sql_query("DELETE FROM `updates` WHERE `updType` = '".updTypeAnnouncement."' AND `updObj` = '$jouid'");
				redirect(url("announcement/".$cluid));
			}

			if($ableToPost && ($journalAction == "post" || $journalAction == "edit")) {
				if( $journalAction == "edit" &&
					$currentEntry["jouAnnCreator"] != $_auth["useid"] && !atLeastSModerator() )
				{
					redirect(url("announcement/".$cluid));
				}
				?>
				<div class="mar_bottom">
					<form action="<?=url(".")?>" method="post">
					<div class="caption"><?=_TITLE ?>:</div>
					<div><input class="notsowide largetext" name="title" type="text"
						<?=$journalAction == "edit" ? 'value="'.htmlspecialchars( $currentEntry["jouTitle"]).'"' : "" ?> /></div>
					<div class="sep">
						<?
						$commentRows = 18;
						$commentWide = false;
						$commentName = "entry";
						$commentDefault = "";
						if($journalAction == "edit") {
							$commentDefault = $currentEntry["jouEntry"];
							if($currentEntry["jouNoEmoticons"]) $commentNoEmoticons = true;
							if($currentEntry["jouNoSig"]) $commentNoSig = true;
							if($currentEntry["jouNoBBCode"]) $commentNoBBCode = true;
						}
						include(INCLUDES."mod_comment.php");
						?>
					</div>
					<div class="sep">
						<button class="submit" name="<?=$journalAction == "edit" ? "editOldEntry" : "postNewEntry" ?>"
							<?=!$commentId ? 'disabled="disabled"' : "" ?>
							onclick="el=get_by_id('<?=$commentId ?>'); if(!el.value){ alert('<?=_BLANK_COMMENT ?>'); return false } else return true"
							type="submit">
							<?=getIMG(url()."images/emoticons/checked.png") ?>
							<?=$journalAction == "edit" ? _SAVE_CHANGES : _JOURNAL_POST_ENTRY ?>
						</button>
					</div>
					</form>
				</div>
				<?
			}

			if($journalAction == "" && isset($currentEntry)) {
				?>
				<div class="mar_bottom" style="margin-top: -8px">
				<?
				include_once(INCLUDES."comments.php");
				$jouData = $currentEntry;
				$comData = array();
				$comData["comid"] = 0;
				$comData["comCreator"] = $jouData["jouAnnCreator"];
				$comData["comComment"] = $jouData["jouEntry"];
				$comData["comSubject"] = $jouData["jouTitle"];
				$comData["comSubmitDate"] = $jouData["jouSubmitDate"];
				$comData["comSubmitIp"] = $jouData["jouSubmitIp"];
				$comData["comLastEdit"] = $jouData["jouLastEdit"];
				$comData["comEditIp"] = $jouData["jouEditIp"];
				$comData["comNoEmoticons"] = $jouData["jouNoEmoticons"];
				$comData["comNoSig"] = $jouData["jouNoSig"];
				$comData["comNoBBCode"] = $jouData["jouNoBBCode"];
				$comData["comAllowImage"] = true;
				showComment($comData, 0);
				?>
				</div>
				<?
				if($currentEntry["jouAnnCreator"] == $_auth["useid"] || atLeastSModerator() )
				{
					$urlEdit = url(".", array("action" => "edit"));
					$urlDelete = url(".", array("action" => "delete"));
					?>
					<div class="mar_bottom" style="margin-top: -8px">
						<?iefixStart()?>
						<div class="button" style="float: right" onclick="if(confirm('<?=_ARE_YOU_SURE?>')) document.location='<?=$urlDelete?>'">
							<a href="<?=$urlDelete?>" onclick="return false">
							<?=getIMG(url()."images/emoticons/delete.png") ?>
							<?=_DELETE ?>
							</a>
						</div>
						<div class="button" style="float: right" onclick="document.location='<?=$urlEdit?>'">
							<a href="<?=$urlEdit?>">
							<?=getIMG(url()."images/emoticons/edit.png") ?>
							<?=_EDIT ?>
							</a>
						</div>
						<div class="clear">&nbsp;</div>
						<?iefixEnd()?>
					</div>
					<?
				}
			}

		if($journalAction != "") {
			// if editing/posting, switch to the right side here
			// and show the past entries there
			?>
			</div>
			<div class="rightside" style="margin-top: -1px">
			<?
		}
		else {
			?>
			<div class="header"><?=_ANNOUNCEMENT_PAST_ENTRIES?></div>
			<?
		}
		?>
		<div class="container2 mar_bottom">
			<?
			$found = false;
			foreach($entries as $entry) {
				// do not display the current entry on the list
				// disabled - because it looks confusing
				//if($entry["jouid"] == $currentEntry["jouid"]) continue;
				iefixStart();
				?>
				<div class="f_left mar_bottom">
					<a href="<?=url("announcement/".$cluid."/".$entry["jouid"])?>">
					<?=getIMG(url()."images/emoticons/journal.png") ?>
					<?=formatText( $entry["jouTitle"]) ?>
					</a>
				</div>
				<div class="f_right mar_bottom">
					<?=gmdate($_auth["useDateFormat"], applyTimezone(strtotime($entry["jouSubmitDate"]))) ?>
				</div>
				<div class="clear">&nbsp;</div>
				<?
				iefixEnd();
				$found = true;
			}
			if(!$found)
				echo _ANNOUNCEMENT_NONE_FOUND;
			?>
		</div>
		<div class="clear">&nbsp;</div>
	</div>

	<?
	if($journalAction == "") {
		?>
		<div class="rightside" style="margin-top: -1px">
		<?
		if(isset($currentEntry)) {
			unset($commentRows);
			unset($commentWide);
			unset($commentName);
			include_once(INCLUDES."comments.php");
			showAllComments($currentEntry["jouid"], "jou", false, true, true, false);
		}
		?>
		</div>
		<?
	}
	?>
	<div class="clear">&nbsp;</div>
</div>
