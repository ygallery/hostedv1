<div class="header">
	<div class="header_title"><?=_USE_PASSWORD_CHANGE ?></div>
</div>
<div class="container">
<?
	$_documentTitle = _USE_PASSWORD_CHANGE;

	$key = $_cmd[1] ? $_cmd[1] : "--";
	$result = sql_query("SELECT `useid`,`useActivationKey`,`useEmail`,`useUsername` FROM `users`,`useExtData` WHERE `useid` = `useEid` AND `useActivationKey` = '".addslashes($key)."' LIMIT 1");
	if(!$useData = mysql_fetch_assoc($result)) {
		notice(_USE_CHANGE_PASSWORD_NO_PERMISSION);
		echo '</div>';
		return;
	}
	if(isset($_POST["submit"])) {
		$password = $_POST["joinpassword"];
		$password2 = $_POST["joinpassword2"];
		$validated = true;

		if (!$password) {
			$validated = false; notice(_BLANK_PASSWORD);
		}
		elseif(strlen ($password) < 6) {
			$validated = false; notice(_JOIN_PASSWORD_TOO_SHORT);
		}
		elseif(!$password2) {
			$validated = false; notice('Password confirmation required.');
		}
		elseif ($password != $password2)
		{
			$validated = false; notice('Password mismatch.');
		}

		if($validated) {
			sql_query("UPDATE `users` SET `usePassword` = '".sha1($password)."' WHERE `useid` = '".$useData["useid"]."' LIMIT 1");
			terminateSession($_auth["useid"]); // force the user to log in with their new password
			redirect(url("passwordchanged"));
		}
	}
?>

<form action="<?=url(".")?>" method="post">
<div class="caption"><?=_USE_ENTER_NEW_PASSWORD ?>:</div>
<div><input class="narrow" name="joinpassword" type="password" /></div>
<div class="sep caption"><?=_USE_ENTER_NEW_PASSWORD_CONFIRM ?>:</div>
<div><input class="narrow" name="joinpassword2" type="password" /></div>
<div class="sep"><input class="submit" name="submit" type="submit" value="<?=_CHANGE ?>" /></div>
</form>

</div>
