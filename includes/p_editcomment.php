<?

$_documentTitle = _EDIT." ("._COMMENT.")";

if($_config["readOnly"]) {
	echo '<div class="header">'._COMMENT.'</div>'.
		'<div class="container">';
	notice(_READONLY);
	echo '</div>';
	return;
}

$comid = intval($_cmd[1]);

$result = sql_query("SELECT * FROM `comments` WHERE `comid` = '$comid' LIMIT 1");

if(!mysql_num_rows($result)) {
	include(INCLUDES."p_notfound.php");
	return;
}
else
	$comData = mysql_fetch_assoc($result);

if(!atLeastSModerator() && ($comData["comCreator"] != $_auth["useid"])) {
	include(INCLUDES."p_notfound.php");
	return;
}

?>
<div class="header">
	<div class="header_title">
		<?= _COMMENT ?>
		<div class="subheader"><?=_EDIT ?></div>
	</div>
</div>

<div class="container">
<div align="center"><table class="a_left"><tr><td class="notsowide">

<form action="<?=url(".")?>" method="post">
	<input type="hidden" name="referer" value="<?=isset($_SERVER["HTTP_REFERER"]) ? htmlspecialchars($_SERVER["HTTP_REFERER"]) : url("/")?>" />
	<?
	include_once(INCLUDES."comments.php");

	if(isset($_POST["sendReply"])) {
		$noEmoticons = isset($_POST["commentNoEmoticons"]) ? 1 : 0;
		$noSig = isset($_POST["commentNoSig"]) ? 1 : 0;
		$noBBCode = isset($_POST["commentNoBBCode"]) ? 1 : 0;
		$comment = addslashes(substr($_POST["comment"], 0, 4000));
		$userIp = getHexIp($_SERVER["REMOTE_ADDR"]);

		sql_query("UPDATE `comments` SET `comTotalEdits` = `comTotalEdits` + 1, `comLastEdit` = NOW(), `comEditIp` = '$userIp', ".
			"`comComment` = '$comment', `comNoEmoticons` = '$noEmoticons', `comNoSig` = '$noSig', `comNoBBCode` = '$noBBCode' ".
			"WHERE `comid` = '$comid' LIMIT 1");
		redirect($_POST["referer"]);
	}

	$commentDefault = $comData["comComment"];

	showComment($comData, 0, true);

	echo '<div class="sep header">'._EDIT_COMMENT.'</div>';

	include(INCLUDES."mod_comment.php");

	?>
	<div class="sep">
		<button class="submit" name="sendReply"
			<?=!$commentId ? 'disabled="disabled"' : "" ?>
			onclick="el=get_by_id('<?=$commentId ?>'); if(!el.value){ alert('<?=_BLANK_COMMENT ?>'); return false } else return true"
			type="submit">
			<?=getIMG(url()."images/emoticons/checked.png")?>
			<?=_SAVE_CHANGES ?>
		</button>
	</div>
	<div class="clear">&nbsp;</div>
</form>

</td></tr></table></div>
</div>
