<?

if( !$isEditMode )
{
	return;
}

if( isset( $_POST[ $editAction."Article" ]))
{
	$values = array(
		"hfqEditDate!" => "NOW()",
		"hfqEditedBy" => $_auth[ "useid" ],
		"hfqCategory" => $_POST[ "category" ],
		"hfqTitle" => $_POST[ $editAction."Article" ],
		"hfqIdent" => $_POST[ "ident" ],
		"hfqText" => $_POST[ "article" ]);

	$id = 0;

	if( $editAction == "Add" )
	{
		$values[ "hfqCreateDate!" ] = "NOW()";
		$values[ "hfqCreatedBy" ] = $_auth[ "useid" ];

		sql_values( $values );
		$id = sql_insert( "helpdeskFAQ" );
	}

	if( $editAction == "Edit" && isset( $faqData[ "hfqid" ]))
	{
		$id = $faqData[ "hfqid" ];

		sql_values( $values );
		sql_where( array( "hfqid" => $id ));
		sql_update( "helpdeskFAQ" );
	}

	if( $id != 0 )
	{
		// Unlink from old tags and delete old tag list

		sql_where( array( "hfmArticle" => $id ));

		$tagResult = sql_rowset( "helpdeskFAQTagMap" );
		$oldTags = array();

		while( $tagData = sql_next( $tagResult ))
		{
			$oldTags[] = $tagData[ "hfmTag" ];
		}

		sql_free( $tagResult );

		sql_where( array( "hfmArticle" => $id ));
		sql_delete( "helpdeskFAQTagMap" );

		foreach( $oldTags as $tagid )
		{
			recountTagReferences( $tagid );
		}

		// Add new tags and recount their references

		for( $i = 0; $i < 6; $i++ )
		{
			$tag = trim( $_POST[ "tag".( $i + 1 ) ]);

			if( $tag == "" )
			{
				continue;
			}

			sql_where( array( "hftName" => $tag ));

			$tagid = sql_value( "helpdeskFAQTags", "hftid", 0 );

			if( $tagid == 0 )
			{
				sql_values( array(
					"hftName" => $tag ));

				$tagid = sql_insert( "helpdeskFAQTags" );
			}

			sql_values( array(
				"hfmArticle" => $id,
				"hfmTag" => $tagid ));

			sql_insert( "helpdeskFAQTagMap" );

			recountTagReferences( $tagid );
		}
	}

	redirect( url( "." ));
}

function recountTagReferences( $tagid )
{
	sql_where( array( "hfmTag" => $tagid ));
	$count = sql_count( "helpdeskFAQTagMap" );

	if( $count > 0 )
	{
		sql_values( array( "hftCount" => $count ));
		sql_where( array( "hftid" => $tagid ));
		sql_update( "helpdeskFAQTags" );
	}
	else
	{
		sql_where( array( "hftid" => $tagid ));
		sql_delete( "helpdeskFAQTags" );
	}
}

?>
<div><br /></div>
<div class="container2 notsowide">
	<?

	$editCategory = isset( $faqData[ "hfqCategory" ]) ? $faqData[ "hfqCategory" ] : "";
	$editArticleTitle = isset( $faqData[ "hfqTitle" ]) ? $faqData[ "hfqTitle" ] : "";
	$editArticleIdent = isset( $faqData[ "hfqIdent" ]) ? $faqData[ "hfqIdent" ] : "";
	$editArticleText = isset( $faqData[ "hfqText" ]) ? $faqData[ "hfqText" ] : "";
	$editTags = array( "", "", "", "", "", "" );

	if( isset( $faqData[ "hfqid" ]))
	{
		sql_order( "hfmid" );
		sql_where( array(
			"hfmArticle" => $faqData[ "hfqid" ],
			"hfmTag*" => "hftid" ));

		$tagResult = sql_rowset( "helpdeskFAQTagMap, helpdeskFAQTags" );
		$editTagNum = 0;

		while( $tagData = sql_next( $tagResult ))
		{
			$editTags[ $editTagNum ] = $tagData[ "hftName" ];
			$editTagNum++;
		}
	    
		sql_free( $tagResult );
	}

	?>
	<form action="<?= url( ".", array( "enableEditMode" => "yes" )) ?>" method="post">
		<label for="id<?= $editAction ?>Article"><?= $editAction ?> Article:</label>
		<input name="<?= $editAction ?>Article" type="text" size="36" id="id<?= $editAction ?>Article"
			value="<?= htmlspecialchars( $editArticleTitle ) ?>" />
		&nbsp;
		<label for="idArticleIdent">Short:</label>
		<input name="ident" type="text" id="idArticleIdent" size="10" value="<?= htmlspecialchars( $editArticleIdent ) ?>" />
		<div class="mar_top">
			<textarea name="article" cols="70" rows="15"><?= htmlspecialchars( $editArticleText ) ?></textarea>
		</div>
		<div class="mar_top">
			Category:
			<select name="category">
				<?

				sql_order( "hfcName" );
				$editCatResult = sql_rowset( "helpdeskFAQCats" );

				while( $editCatData = sql_next( $editCatResult ))
				{
					?><option<?= $editCatData[ "hfcid" ] == $editCategory ? ' selected="selected"' : '' ?>
						value="<?= $editCatData[ "hfcid" ] ?>"><?=
						htmlspecialchars( preg_replace( '/^.*\|/', "", $editCatData[ "hfcName" ])) ?></option><?
				}

				sql_free( $editCatResult );

				?>
			</select>
		</div>
		<div class="mar_top">
			Tags (more relevant first):
		</div>
		<div class="mar_top">
			<input name="tag1" type="text" size="20" id="idArticleTag1" value="<?= $editTags[ 0 ] ?>" />
			&nbsp;
			<input name="tag2" type="text" size="20" id="idArticleTag2" value="<?= $editTags[ 1 ] ?>" />
			&nbsp;
			<input name="tag3" type="text" size="20" id="idArticleTag3" value="<?= $editTags[ 2 ] ?>" />
		</div>
		<div class="mar_top">
			<input name="tag4" type="text" size="20" id="idArticleTag4" value="<?= $editTags[ 3 ] ?>" />
			&nbsp;
			<input name="tag5" type="text" size="20" id="idArticleTag5" value="<?= $editTags[ 4 ] ?>" />
			&nbsp;
			<input name="tag6" type="text" size="20" id="idArticleTag6" value="<?= $editTags[ 5 ] ?>" />
		</div>
		<div class="mar_top">
			<button class="smalltext"><?= getIMG( url()."images/emoticons/checked.png" )?>
				<?= $editAction ?> article!</button>
		</div>
	</form>
</div>
