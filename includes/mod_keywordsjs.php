<script type="text/javascript">
//<![CDATA[
	var predefine_keywords, el;

	predefine_keywords = true;
<?
// "click" on the default keywords to select them
if(!isset($defaultKeywords)) $predefined = array();
else $predefined = preg_split('/\s/', $defaultKeywords, 200, PREG_SPLIT_NO_EMPTY);
foreach($predefined as $keyid) {
	if(!$keyid = intval($keyid)) continue;
	echo "el = get_by_id('keywordSpan$keyid'); if(el) el.onclick();\n";
}
// "click" on the first tab to make it open by default
if($firstKeywordId)
	echo "el = get_by_id('$firstKeywordId'); if(el) el.onclick();\n";
?>
	predefine_keywords = false;
//]]>
</script>
