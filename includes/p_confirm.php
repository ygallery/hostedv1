<?

// Confirm/Refuse a collaborative work.

$accept = isset( $_GET[ "accept" ]) ? ( $_GET[ "accept" ] != "0" ) : false;

$sql = "SELECT `objid`, `objCreator` FROM `objects`, `objExtData`".dbWhere( array(
	"objid*" => "objEid",
	"objid" => intval( $_cmd[ 1 ]),
	"objCollab" => $_auth[ "useid" ],
	"objCollabConfirmed" => 0,
	"objPending" => 0,
	"objDeleted" => 0 ));

$objResult = sql_query( $sql );

if( !$objData = mysql_fetch_assoc( $objResult ))
{
	include( INCLUDES."p_notfound.php" );
	return;
}

mysql_free_result( $objResult );

if( $accept )
{
	$sql = "UPDATE `objExtData`".dbSet( array(
		"objCollabConfirmed" => 1 )).dbWhere( array(
		"objEid" => $objData[ "objid" ]));

	sql_query( $sql );

	// Send to watchers.

	addArtUpdateToWatchers( $_auth[ "useid" ], $objData[ "objid" ], 0,
		$objData[ "objCreator" ]);

	removeDupeArtUpdates( $objData[ "objid" ]);

	// Move all associated comments from the last 10 days to Updates.

	$sql = "SELECT `comid`, `comCreator` FROM `comments`".dbWhere( array(
		"|1" => "`comSubmitDate` > DATE_SUB( CURDATE(), INTERVAL 10 DAY )",
		"comObj" => $objData[ "objid" ],
		"comObjType" => "obj" ));

	$comResult = sql_query( $sql );

	while( $comData = mysql_fetch_assoc( $comResult ))
	{
		if( $comData[ "comCreator" ] == $_auth[ "useid" ])
			continue;

		addUpdate( updTypeComment, $_auth[ "useid" ], $comData[ "comid" ],
			$comData[ "comCreator" ]);
	}

	mysql_free_result( $comResult );
}
else
{
	$sql = "UPDATE `objects`".dbSet( array(
		"objCollab" => 0 )).dbWhere( array(
		"objid" => $objData[ "objid" ]));

	sql_query( $sql );
}

redirect( url( "view/".$objData[ "objid" ]));

?>