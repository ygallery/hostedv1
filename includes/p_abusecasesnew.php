<?

if( !atLeastHelpdesk() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

$_documentTitle = _ABUSE_LIST;

?>
<div class="header">
	<div class="header_title">
		<?= _ADMINISTRATION ?>
		<div class="subheader"><?= _ABUSE_LIST ?></div>
	</div>
	<?

	$active = 3;
	include(INCLUDES."mod_adminmenu.php");

	?>
</div>

<div class="container">
	<div class="a_center">
		<form action="<?=url(".")?>" method="get">
			<?
			foreach($_GET as $key => $value)
				if($key != "show" && $key != "order")
					echo '<input name="'.htmlspecialchars($key).'" type="hidden" value="'.htmlspecialchars($value).'" />';
			?>
			<select name="show">
				<optgroup label="Open Abuse Cases">
					<option <?=isset($_GET["show"]) && $_GET["show"] == 0 ? 'selected="selected"' : "" ?> value="0">All Cases</option>
					<option <?=isset($_GET["show"]) && $_GET["show"] == 1 ? 'selected="selected"' : "" ?> value="1">Pending Moderator</option>
					<option <?=isset($_GET["show"]) && $_GET["show"] == 2 ? 'selected="selected"' : "" ?> value="2">Pending Supermoderator</option>
					<option <?=isset($_GET["show"]) && $_GET["show"] == 3 ? 'selected="selected"' : "" ?> value="3">Pending Administrator</option>
					<option <?=isset($_GET["show"]) && $_GET["show"] == 4 ? 'selected="selected"' : "" ?> value="4">Pending User</option>
				<optgroup label="Resolved Abuse Cases">
					<option <?=isset($_GET["show"]) && $_GET["show"] == 5 ? 'selected="selected"' : "" ?> value="5">All Cases</option>
					<option <?=isset($_GET["show"]) && $_GET["show"] == 6 ? 'selected="selected"' : "" ?> value="6">Resolved - Restore</option>
					<option <?=isset($_GET["show"]) && $_GET["show"] == 7 ? 'selected="selected"' : "" ?> value="7">Resolved - Delete</option>
					<option <?=isset($_GET["show"]) && $_GET["show"] == 8 ? 'selected="selected"' : "" ?> value="8">Resolved By Admin</option>
			</select>
			<select name="order">
				<option <?=isset($_GET["order"]) && $_GET["order"] == 0 ? 'selected="selected"' : "" ?> value="0"><?=_OLDEST_FIRST ?></option>
				<option <?=isset($_GET["order"]) && $_GET["order"] == 1 ? 'selected="selected"' : "" ?> value="1"><?=_NEWEST_FIRST ?></option>
			</select>
			<input class="submit" type="submit" value="<?=_UPDATE ?>" style="vertical-align: middle" />
		</form>
	</div>
<div class="hline">&nbsp;</div>
		
	<?

	$offset = isset( $_GET[ "offset" ]) ? intval( $_GET[ "offset" ]) : 0;
	$limit = 24;

	if( isset( $_GET[ "order" ]))
	{
		$order = ($_GET[ "order" ] == 1 ? "DESC" : "ASC");
	}
	else $order = "ASC";
	if( isset( $_GET [ "show" ])) {
		$_GET [ "show" ] == 0 ? $show = "`abuResolved`='0' AND `abuFinal`='?' " : "";
		$_GET [ "show" ] == 1 ? $show = "`abuResolved`='0' AND `abuMod`='?' " : "";
		$_GET [ "show" ] == 2 ? $show = "`abuResolved`='0' AND `abusMod`='?' AND `abuMod`!='?' " : "";
		$_GET [ "show" ] == 3 ? $show = "`abuResolved`='0' AND `abuMod`!=`abusMod` AND `abuMod`!='?' AND `abusMod`!='?' AND `abuFinal`='?' " : "";
		$_GET [ "show" ] == 4 ? $show = "`abuResolved`='0' AND `abuFinal`='-' " : "";
		$_GET [ "show" ] == 5 ? $show = "`abuResolved`='1' " : "";
		$_GET [ "show" ] == 6 ? $show = "`abuResolved`='1' AND `abuFinal`='+'" : "";
		$_GET [ "show" ] == 7 ? $show = "`abuResolved`='1' AND `abuFinal`='-'" : "";
		$_GET [ "show" ] == 8 ? $show = "`abuResolved`='1' AND `aburMod`!='?' " : "";
		
		
	}
	else $show = "`abuResolved`='0' AND `abuFinal`='?' ";
	
	$abuResult = sql_query( "SELECT SQL_CALC_FOUND_ROWS * FROM `abuses` WHERE $show ".
		"ORDER BY `abuSubmitDate` $order LIMIT $offset, $limit" );

	$cntResult = sql_query( "SELECT FOUND_ROWS()" );

	$totalCount = mysql_result( $cntResult, 0 );

	navControls( $offset, $limit, $totalCount );

	?>
	<div class="clear"><br /></div>
	<br />
	<table>
	<?

	while( $abuData = mysql_fetch_assoc( $abuResult ))
	{
		?>
		<tr class="v_top">
			<td class="a_center"><?= getObjectThumb( $abuData[ "abuObj" ], 100, $abuData[ "abuIsExtras" ]) ?></td>
			<td width="100%">
				<div class="outer notsowide" style="padding: 2px">
				<div class="<?= $abuData[ "abuResolved" ] ? "container2" : "container" ?>">
					<table>
					<?if(!$abuData[ "abuResolved" ] && trim( $abuData[ "abuReason" ]) != "" && ($abuData[ "aburMod" ] == "-" || $abuData["abuMod"] == $abuData["abusMod"]) )
						{
						?>
							<tr class="v_top">
								<td class="a_right"></td>
								<td><div class="error"><b>Pending User Input</b><br />
								<? if( ( time() - $_config["abuseGrace"] ) < strtotime($abuData[ "abuCloseDate" ]))
									{
										echo "On Hold for Deletion Until:<br /> ".gmdate( $_auth[ "useDateFormat" ],
										applyTimezone( strtotime( $abuData[ "abuCloseDate" ] ) + $_config[ "abuseGrace" ] ));
									}
									else {
										echo "Eligible for Deletion";	
									}
							?>
						</div></td>
							</tr>
						<?	
						}
						elseif(!$abuData[ "abuResolved" ] && trim( $abuData[ "abuReason" ]) != "" && $abuData["abuMod"] != $abuData["abusMod"] && $abuData[ "aburMod" ] == "?" )
						{
						?>
							<tr class="v_top">
								<td class="a_right"></td>
								<td><div class="error"><b>Waiting on Administrator</b></div></td>
							</tr>
						<?	
						}
						
						?>
						<tr class="v_top">
							<td class="a_right"><b><?= _ABUSE_REPORT ?></b>:</td>
							<td width="100%"><?= getUserLink( $abuData[ "abuSubmitter" ]) ?>:
								<?= formatText( $abuData[ "abuComment" ]) ?></td>
						</tr>
						<?

						if( $abuData[ "abuMod" ] != "?" )
						{
							?>
							<tr class="v_top">
								<td class="a_right"><b><?= _MODERATOR ?></b>:</td>
								<td>
									<span class="error">(<?= $abuData[ "abuMod" ] ?>)</span>
									<?= getUserLink( $abuData[ "abuModName" ]) ?>:
									<?= formatText( $abuData[ "abuModComment" ]) ?></td>
							</tr>
							<?
						}

						if( $abuData[ "abusMod" ] != "?" )
						{
							?>
							<tr class="v_top">
								<td class="a_right"><b><?= _SUPERMODERATOR ?></b>:</td>
								<td><span class="error">(<?= $abuData[ "abusMod" ] ?>)</span>
									<?= getUserLink( $abuData[ "abusModName" ]) ?>:
									<?= formatText( $abuData[ "abusModComment" ]) ?></td>
							</tr>
							<?
						}

						if( $abuData[ "aburMod" ] != "?" )
						{
							?>
							<tr class="v_top">
								<td class="a_right"><b><?= _ADMINISTRATOR ?></b>:</td>
								<td><span class="error">(<?= $abuData[ "aburMod" ] ?>)</span>
									<?= getUserLink( $abuData[ "aburModName" ]) ?>:
									<?= formatText( $abuData[ "aburModComment" ]) ?></td>
							</tr>
							<?
						}

						if( trim( $abuData[ "abuReason" ]) != "" && $abuData[ "aburMod" ] != "+")
						{
							?>
							<tr class="v_top">
								<td class="a_right"><b><?= _ABUSE_RULE ?></b>:</td>
								<td><?= formatText( $abuData[ "abuRule" ]) ?></td>
							</tr>
							<tr class="v_top">
								<td class="a_right"><b><?= _ABUSE_REASON ?></b>:</td>
								<td><?= formatText( $abuData[ "abuReason" ]) ?></td>
							</tr>
							<?
						}
						?>
					</table>
					<div class="a_right">
						<a href="<?= url( "abuse/".$abuData[ "abuid" ]) ?>">
							<?= _ABUSE_STATUS ?>
							<?= getIMG( url()."images/emoticons/nav-next.png" ) ?></a>
					</div>
				</div>
				</div>
			</td>
		</tr>
		<?
	}

	?>
	</table>
	<br />
	<?

	navControls( $offset, $limit, $totalCount );

	?>
	<div class="clear"><br /></div>
</div>
		