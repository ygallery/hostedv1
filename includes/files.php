<?php

// -----------------------------------------------------------------------------
// File uploading functions.

// get the correct supported extension of an uploaded file
function getUploadedFileExt($varname)
{
	if(isset($_FILES[$varname]) && ($_FILES[$varname]["error"] == UPLOAD_ERR_OK))
	{
		if( $_FILES[$varname]["type"] == "text/plain" )
			return "txt";

		$fileName = $_FILES[ $varname ][ "tmp_name" ];

		if( $fileName == "" || !file_exists( $fileName ) || filesize( $fileName ) == 0 )
			return "";

		$fileInfo = getimagesize( $fileName ); // detect image type

		$mimetype = $fileInfo["mime"] ? $fileInfo["mime"] : $_FILES[$varname]["type"];

		switch($mimetype)
		{
			case "image/gif": return "gif";
			case "image/jpeg": return "jpg";
			case "video/x-mng": return "mng";
			case "video/mp4": return "mp4";
			case "video/x-ms-wmv": return "wmv";

			case "image/png": return "png";
			case "application/x-shockwave-flash": return "swf";
			case "text/html": return "html";
			case "text/plain": return "txt";
			case "application/octet-stream": return "bin";
		}
	}
	return "";
}

// Checks if the file $_FILES[$varname] has been uploaded successfully.
// Returns error message string on failure, or "" on success.
function checkUploadedFile($varname) {
	if(!isset($_FILES[$varname])) {
		return _UPL_NO_FILE;
	}
	switch($_FILES[$varname]["error"]) {
		case UPLOAD_ERR_OK:
			if(getUploadedFileExt($varname))
				return ""; // correct upload and file type
			else
				return sprintf(_UPL_WRONG_TYPE, $_FILES[$varname]["type"], $_FILES[$varname]["name"]);
		case UPLOAD_ERR_INI_SIZE:
			return sprintf(_UPL_EXCEED_INI,$_FILES[$varname]["name"]);
		case UPLOAD_ERR_FORM_SIZE:
			return sprintf(_UPL_EXCEED_MAXSIZE, $_FILES[$varname]["name"]);
		case UPLOAD_ERR_PARTIAL:
			return sprintf(_UPL_PARTIAL, $_FILES[$varname]["name"]);
		case UPLOAD_ERR_NO_FILE:
			return _UPL_NO_FILE;
		default:
			return sprintf(_UPL_ERROR, $_FILES[$varname]["error"], $_FILES[$varname]["name"]);
	}
};

// Moves uploaded file $_FILES[$varname] to the specified $location.
// checkUploadedFile() must be called first to check for any errors.
// $location must not contain the extension part, it will be detected
// and added automatically.
function uploadFile($varname, $location, &$extension) {
	$extension = getUploadedFileExt($varname);
	$filename = $location.".".$extension;
	forceFolders(dirname($filename));
	if(file_exists($filename)) @unlink($filename);

	if(isset($_FILES[$varname]["simple_move"])) {
		rename($_FILES[$varname]["tmp_name"], $filename);

		if(isset($_FILES[$varname]["also_move"]))
			rename($_FILES[$varname]["also_move"],
				$location.$_FILES[$varname]["also_move_ext"]);
	}
	else {
		move_uploaded_file($_FILES[$varname]["tmp_name"], $filename);
		chmod($filename, 0644);
	}
	/*
	// convert text files to UTF-8 automatically
	if(function_exists('mb_convert_encoding') &&
		($extension == "txt" || $extension == "html")) {
		$str = file_get_contents($filename);
		//$str = mb_convert_encoding($str, "UTF-8", "auto");
		$str = mb_convert_encoding($str, "UTF-8", "ISO-8859-1,SJIS,UTF-8");
		$fp = fopen($filename, "w");
		fputs($fp, $str);
		fclose($fp);
	}
	*/
};

// Creates a thumbnail from file $imagename and stores it to file $thumbnail
// Returns 'false' if $imagename is not a valid image file.
function thumbifyImage($imageName, $thumbName, $thumbWidth, $thumbHeight, $quality = 80, $allowLarger = false) {
	if(!$imageName || !file_exists($imageName) || filesize($imageName) < 500)
	{
		return false;
	}
	$size = getimagesize($imageName); // get image size/type information
	$srcWidth = $size[0];
	$srcHeight = $size[1];
	$invalid = false;
	switch($size[2]) {
		case 1: // GIF
			// with GD 2.0.28+ installed GIF files can be read
			if(function_exists("imagecreatefromgif"))
				$srcImage = imagecreatefromgif($imageName) or $invalid = true;
			else $invalid = true;
			break;
		case 2: // 
			$srcImage = imagecreatefromjpeg($imageName) or $invalid = true;
			break;
		case 3: // png
			$srcImage = imagecreatefrompng($imageName) or $invalid = true;
			break;
		default:
			$invalid = true;
			break;
	}
	if($invalid)
	{
		return false;
	}
	// create a thumbnail
	if($srcWidth <= $thumbWidth && $srcHeight <= $thumbHeight && !$allowLarger) {
		$dstWidth = $srcWidth;
		$dstHeight = $srcHeight;
	}
	else {
		$aspect = $srcHeight / $srcWidth;
		$dstWidth = $thumbWidth;
		$dstHeight = $thumbWidth * $aspect;
		if($dstHeight > $thumbHeight) {
			$coeff = $thumbHeight / $dstHeight;
			$dstWidth = $dstWidth * $coeff;
			$dstHeight = $dstHeight * $coeff;
		}
	}
	$dstImage = imagecreatetruecolor($dstWidth, $dstHeight);
	imagecopyresampled($dstImage, $srcImage, 0, 0, 0, 0, $dstWidth, $dstHeight, $srcWidth, $srcHeight);
	forceFolders(dirname($thumbName));
	if(file_exists($thumbName)) @unlink($thumbName); // delete the old thumbnail if any
	imagejpeg($dstImage, $thumbName, $quality);
	imagedestroy($srcImage);
	imagedestroy($dstImage);
	return true;
};

// Recursively creates all required subfolders until $path is created.
function forceFolders($path) {
	if(strlen($path) == 0) return false;
	if(strlen($path) < 3) return true;
	elseif(is_dir($path)) return true;
		elseif(dirname($path) == $path)
			return true;
	return(forceFolders(dirname($path)) and mkdir($path, 0755));
};

?>
