<?

if( !isAdmin() )
{
	echo "This is only available to the administrator.";
	return;
}

$keySeries = intval( $_cmd[ 1 ]);

if( $keySeries == 0 )
{
	echo "Usage: /kwdpurgechars/<b>keyid</b>, where <b>keyid</b> = 'Series' keyword id";
	return;
}

traverseKWTree( $keySeries );

echo "Success.";

function traverseKWTree( $keyidParent = 0, $level = 0 )
{
	$result = sql_query( "SELECT `keyid` FROM `keywords` WHERE `keySubcat` = '$keyidParent'" );

	while( $keyData = mysql_fetch_row( $result ))
	{
		$keyid = $keyData[ 0 ];

		traverseKWTree( $keyid, $level + 1 );

		// Level 0: A,B,...Z
		// Level 1: Series titles
		// Level 2: Characters <-- this is what we're going to delete

		if( $level >= 2 )
		{
			sql_query( "DELETE FROM `keywords` WHERE `keyid` = '$keyid' LIMIT 1" );
		}
	}
}

?>