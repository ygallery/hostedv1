<?php
	// Shows the friends list
	//
	// Input variables:
	//
	// $useid = the user's id

	if(!isset($friendLimit)) $friendLimit = "";

	if(!isset($friendQuery))
		$friendQuery = "SELECT DISTINCT `useid` FROM `users`,`watches` AS w1,`watches` AS w2 ".
			"WHERE `useid` = w1.`watCreator` ".
			"AND w1.`watUser` = '$useid' ".
			"AND w1.`watType` = 'use' ".
			"AND w2.`watType` = 'use' ".
			"AND w1.`watCreator` = w2.`watUser` ".
			"AND w2.`watCreator` = w1.`watUser` ".
			"ORDER BY w1.`watSubmitDate` DESC $friendLimit";

	if(isset($friendQuery)) {

	iefixStart();

	$result = sql_query($friendQuery);
	$friendCount = 0;

	$friends = array();
	$useids = array();

	while($rowData = mysql_fetch_assoc($result))
	{
		$useids[] = $rowData[ "useid" ];
		$friends[] = $rowData;
		$friendCount++;
	}

	prefetchUserData( $useids );

	foreach( $friends as $rowData )
	{
		?>
		<div class="f_left a_center mar_right mar_bottom" style="height: 100px">
			<?=getUserAvatar("", $rowData["useid"], true) ?>
		</div>
		<?
	}
	?>
	<div class="clear">&nbsp;</div>
	<?
	iefixEnd();

	}
?>
