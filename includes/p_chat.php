<?
	$_documentTitle = _CHAT;

if( !isLoggedIn() )
{
	header("Status: 403 Forbidden");
	echo '<div class="header">' . _CHAT . ' (beta)</div>'.
		'<div class="container">';
	notice(_REQUIRE_LOGIN);
	echo '</div>';
	return;
}
?>

<div class="header">
	<div class="header_title"><?=_CHAT ?> (beta)</div>
</div>
<div class="container">
	<div class="header">How to connect to chat</div>
	<ol>
		<li>Download and install any <a href="http://en.wikipedia.org/wiki/List_of_XMPP_client_software">instant messaging client that supports Jabber/XMPP</a>. <a href="http://pidgin.im">Pidgin</a> is probably the most-used client, but any will do.</li>
		<li>
			<p>Add a new account to the instant messaging client using the following information:</p>
			<ul>
				<li>Jabber ID: <strong><?php echo htmlspecialchars($_auth["useUsername"]); ?>@y-gallery.net</strong> (if your client asks for a username and domain separately, just enter your username)</li>
				<li>Password: <strong>your y!Gallery password</strong></li>
				<li>Server: <strong>y-gallery.net</strong></li>
				<li>Resource: doesn't matter</li>
			</ul>
			<p>Advanced users may want to enable "Force old (port 5223) SSL" if applicable, and change the connect port to 5223.</p>
		</li>
		<li>Connect and enjoy! To talk to other users, you can add them to your buddy list; their username will be <strong>&lt;your buddy's username&gt;@y-gallery.net</strong>. Alternatively, you can go to the general chat to meet new people (below).</li>
	</ol>
	<p><strong>NOTE:</strong> Your client may complain that the password is not sent securely; this can be ignored, as it is sent no more insecurely than when you log on to the site itself.</p>

	<div class="header">Multi-user chat</div>
	<p>There is a general conference set up on the server. Go to the Room List in your IM client to view the list of available conference rooms, or add <a href="xmpp:general@conference.y-gallery.net?join">general@conference.y-gallery.net</a> to your list of chats to go directly to the General Conference room.</p>

	<hr />

	<div class="header">Specific instructions for Pidgin 2.5</div>
	<p><strong>To connect to your y!Gallery account:</strong></p>
	<ol>
		<li>In the Buddy List, go to the Accounts menu and select Manage Accounts</li>
		<li>Click the Add button</li>
		<li>In the new window, select <strong>XMPP</strong> from the Protocol menu</li>
		<li>Enter the information listed above (username, password, domain, resource)</li>
		<li>Click Save</li>
		<li>Check the box next to the account in the Accounts list, if it isn't already checked</li>
	</ol>
	<p><strong>To add a new buddy:</strong></p>
	<ol>
		<li>In the Buddy List, go to the Buddies menu and select Add buddy</li>
		<li>Select your y!Gallery account from the drop-down menu</li>
		<li>Enter <strong>&lt;your buddy's username&gt;@y-gallery.net</strong> for the user you want to add</li>
		<li>Click Add</li>
	</ol>
	<p><strong>To browse conferences (and add them to your buddy list):</strong></p>
	<ol>
		<li>In the Buddy List, go to the Tools menu and select Room List</li>
		<li>Select your y!Gallery account from the drop-down menu</li>
		<li>Click Get List</li>
		<li>Enter <strong>conference.y-gallery.net</strong> in the window that appears and click Find Rooms</li>
		<li>Click Add Chat to add the conference to your buddy list, or click Join to join the conference without adding it to your buddy list</li>
	</ol>

	<hr />

	<p><strong>Please note that this is a private server with no S2S service; you cannot communicate with y!Gallery users from any other Jabber/XMPP/Gtalk servers at this time.</strong></p>
</div>
