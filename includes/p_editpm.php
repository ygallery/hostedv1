<?

$_documentTitle = _EDIT." ("._PM.")";

if( $_config[ "readOnly" ])
{
	?>
	<div class="header"><?= _COMMENT ?></div>
    <div class="container"><? notice( _READONLY ); ?></div>
	<?

	return;
}

$pmsid = intval( $_cmd[ 1 ]);

$result = sql_query( "SELECT * FROM `pms`".dbWhere( array(
	"pmsid" => $pmsid )));

if( !$comData = mysql_fetch_assoc( $result ))
{
	include( INCLUDES."p_notfound.php" );
	return;
}

if( !atLeastSModerator() && ( $comData[ "pmsCreator" ] != $_auth[ "useid" ]))
{
	include( INCLUDES."p_notfound.php" );
	return;
}

?>
<div class="header">
	<div class="header_title">
		<?= _PM ?>
		<div class="subheader"><?=_EDIT ?></div>
	</div>
</div>

<div class="container">
<div align="center">
<table class="a_left"><tr><td class="notsowide">

<form action="<?= url( "." ) ?>" method="post">

	<input type="hidden" name="referer"
		value="<?= isset( $_SERVER[ "HTTP_REFERER" ]) ? htmlspecialchars( $_SERVER[ "HTTP_REFERER" ]) : url( "/" ) ?>" />
	<?

	include_once( INCLUDES."comments.php" );

	if( isset( $_POST[ "sendReply" ]))
	{
		$noEmoticons = isset( $_POST[ "commentNoEmoticons" ]) ? 1 : 0;
		$noSig = isset( $_POST[ "commentNoSig" ]) ? 1 : 0;
		$noBBCode = isset( $_POST[ "commentNoBBCode" ]) ? 1 : 0;
		$comment = addslashes( substr( $_POST[ "comment" ], 0, 40000 ));
		$title = addslashes( $_POST[ "title" ]);

		if( $title == "" ) // Make sure the title is not blank
		{
			$title = _PM_UNTITLED;
		}

		$userIp = getHexIp( $_SERVER[ "REMOTE_ADDR" ]);

		sql_query( "UPDATE `pms` ".
			"SET `pmsTotalEdits` = `pmsTotalEdits` + 1, ".
			"`pmsLastEdit` = NOW(), ".
			"`pmsEditIp` = '$userIp', ".
			"`pmsTitle` = '$title', ".
			"`pmsComment` = '$comment', ".
			"`pmsNoEmoticons` = '$noEmoticons', ".
			"`pmsNoSig` = '$noSig', ".
			"`pmsNoBBCode` = '$noBBCode' ".
			"WHERE `pmsid` = '$pmsid' LIMIT 1" );

		redirect( $_POST[ "referer" ]);
	}

	$commentDefault = $comData[ "pmsComment" ];
	$comTitle = $comData[ "pmsTitle" ];

	showComment( $comData, 0, true );

	echo '<div class="sep header">'._EDIT_PM.'</div>';

	?>
	<div class="mar_bottom">
		<input class="<?= $_isIE ? "wide" : "notsowide" ?> largetext" name="title" type="text"
			value="<?= htmlspecialchars( $comTitle ) ?>" />
	</div>
	<?

	include( INCLUDES."mod_comment.php" );

	?>
	<div class="sep">
		<button class="submit" name="sendReply" <?= !$commentId ? 'disabled="disabled"' : "" ?>
			onclick="el=get_by_id('<?= $commentId ?>'); if(!el.value){ alert('<?= _BLANK_COMMENT ?>'); return false } else return true"
			type="submit">
			<?= getIMG( url()."images/emoticons/checked.png" ) ?>
			<?= _SAVE_CHANGES ?>
		</button>
	</div>
	<div class="clear">&nbsp;</div>
</form>

</td></tr></table></div>
</div>
