<?php 
	class dbconn {
		var $pdo_conn;

		// Connects to a database given the following:
		// 'host'
		// 'user'
		// 'pass'
		// 'database'
		// 
		function __construct($args){
			if(!isset($args['host'])){
				$args['host'] = 'localhost';
			}

			if(!isset($args['port'])){
				$args['port'] = 3306;
			}

			if(isset($args['user']) &&
				isset($args['pass']) &&
				isset($args['database'])){

					$dsn = "mysql:host=" . $args['host'] . ";port=" . $args['port']
						. ";dbname=" . $args['database'];

					try{
						$this->pdo_conn = new PDO($dsn, $args['username'], $args['pass'], 
						array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
					}catch(e){
						$this->pdo_conn = false;
					}
			}else{
				$this->pdo_conn = false;
			}
		}



	}

?>
