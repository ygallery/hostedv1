<?
	$cluid = intval($_cmd[1]);

	$result = sql_query("SELECT * FROM `clubs`,`cluExtData` WHERE `cluid` = '$cluid' AND `cluEid` = `cluid` LIMIT 1");
	if(!$cluData = mysql_fetch_assoc($result)) {
		include(INCLUDES."p_notfound.php");
		return;
	}

	$_documentTitle = ($cluData["cluIsProject"] ? _PROJECT : _CLUB1).": ".$cluData["cluName"];
?>

<div class="header">
	<div class="f_right mar_left a_center normaltext">
		<?=getClubIcon($cluData["cluid"])?>
	</div>
	<div class="f_left mar_right header_title">
		<?=$cluData["cluName"]?>
		<div class="subheader"><?=$cluData["cluIsProject"] ? _PROJECT : _CLUB1 ?></div>
	</div>
	<?
		if($_auth["useid"]) {
			$result = sql_query("SELECT `useCpending` FROM `useClubs` WHERE `useCclub` = '$cluid' AND `useCmember` = '".$_auth["useid"]."' LIMIT 1");
			$isMember = mysql_num_rows($result) > 0;
			$isPending = $isMember ? mysql_result($result, 0) : false;
			$result = sql_query("SELECT `watid` FROM `watches` WHERE `watCreator` = '$cluid' AND `watUser` = '".$_auth["useid"]."' AND `watType` = 'clu' LIMIT 1");
			$watched = mysql_num_rows($result) > 0;
			?>
			<div class="f_right mar_right">
				<div class="largetext">
					<a title="<?=$watched ? _WATCH_CLUB_STOP : _WATCH_CLUB_START ?>"
						href="<?=url("watchclub/".$cluid)?>">
						<?=getIMG(url()."images/emoticons/fav".($watched?"0":"1").".png") ?>
						<?=_WATCH?>
					</a>
				</div>
				<?

				if( !$cluData[ "cluNoMembers" ])
				{
					?>
					<div class="normaltext">
						<a title="<?=$isMember ? _CLUB_LEAVE : _CLUB_JOIN ?>"
							href="<?=url("joinclub/".$cluid)?>"
							onclick="return confirm('<?=_ARE_YOU_SURE ?>')">
							<?=getIMG(url()."images/emoticons/fav".($isMember?"0":"1").".png")?>
							<b><?=_CLUB_MEMBERSHIP ?></b>
						</a>
						<?=$isPending ? '<div class="normaltext">'._CLUB_PENDING.'</div>' : "" ?>
					</div>
					<?
				}

				?>
			</div>
			<?
		}
		$active = 1;
		include(INCLUDES."mod_clubmenu.php");
	?>
</div>

<div class="container">

<?=iefixStart()?>
<div class="header f_left"><?=_PROFILE ?></div>
<div class="header f_right a_right"><?=isset($_GET["replied"]) ? _COMMENTS : _LEAVE_COMMENT ?></div>
<div class="clear">&nbsp;</div>
<?=iefixEnd()?>
<?

$whereMature = "1";

applyObjFilters( $whereMature );

$whereMature = "AND ".$whereMature;

?>
<div class="leftside">
	<?

	$ID = "";
	$filename = findNewestFile( applyIdToPath( "files/clubids/", $cluData[ "cluid" ])."-*", "" );

	if( $filename != "" )
	{
		if( preg_match( '/\.swf$/', $filename ))
		{
			list( $maxWidth, $maxHeight ) = preg_split( '/x/', $_config[ "idResolution" ]);

			$ID = '<div class="a_center mar_bottom">'.getSWF( $filename, 1.0, $maxWidth, $maxHeight ).'</div>';
		}
		else
		{
			$ID = '<div class="a_center mar_bottom"><img src="'.urlf().$filename.'" /></div>';
		}
	}

	if( $ID != "" || trim( $cluData[ "cluProfile" ]) != "" )
	{
		?>
		<div class="container2 mar_bottom">
			<?= $ID ?>
			<div><?= formatText( $cluData[ "cluProfile" ]) ?></div>
		</div>
		<?
	}

	if(!$cluData[ "cluNoMembers" ] && $_auth["useid"] == $cluData["cluCreator"]) {
		$result = sql_query("SELECT `useid`,`useUsername` FROM `users`,`useClubs` WHERE `useid` = `useCmember` AND `useCclub` = '$cluid' AND `useCpending` = '1' ORDER BY `useCid` DESC LIMIT 5");
		if(mysql_num_rows($result)) {
			?>
			<div class="sep container2">
				<div class="largetext"><?=_CLUB_MEMBERS_PENDING ?></div>
				<div><?=_CLUB_MEMBERS_PENDING_EXPLAIN ?></div>
				<div class="error"><?=_CLUB_MEMBER_EXPLAIN?></div>
				<div class="sep mar_left">
				<?=iefixStart()?>
					<?
					while($rowData = mysql_fetch_assoc($result)) {
						?>
						<div class="mar_bottom">
							<div class="f_left a_center">
								<?=getUserAvatar("", $rowData["useid"], true) ?>
							</div>
							<div class="f_left mar_left"><br />
								&nbsp;
								<a href="<?=url("affirm/".$cluid."/".strtolower($rowData["useUsername"]), array("accept" => "1")) ?>" onclick="return confirm('<?=_ARE_YOU_SURE ?>')">
									<?=getIMG(url()."images/emoticons/checked.png")?>
									<?=_CLUB_ACCEPT ?>
								</a>
								&nbsp;
								<a href="<?=url("affirm/".$cluid."/".strtolower($rowData["useUsername"]), array("accept" => "0")) ?>" onclick="return confirm('<?=_ARE_YOU_SURE ?>')">
									<?=getIMG(url()."images/emoticons/cancel.png")?>
									<?=_CLUB_DECLINE ?>
								</a>
							</div>
							<div class="clear">&nbsp;</div>
						</div>
						<?
					}
					?>
				<?=iefixEnd()?>
				</div>
			</div>
			<?
		}
	}

	$result = sql_query("SELECT * FROM `objects` WHERE `objid` = '".$cluData["cluFeaturedObj"]."' AND `objDeleted` = '0' AND `objPending` = '0' $whereMature LIMIT 1");
	if($objData = mysql_fetch_assoc($result)) {
		?>
		<div class="sep caption"><?=_FEATURE ?>:</div>
		<div class="container2 a_center">
			<?
			if($filename = findNewestFile(applyIdToPath("files/clubfeatures/", $cluData["cluFeaturedObj"])."-*", "")) {
				$objTitle = formatText($objData["objTitle"]).' <br />'.
					sprintf(_BY,getUserLink($objData["objCreator"]));
				echo '<div><a href="'.url("view/".$objData["objid"]).'">'.
					'<img alt="'._ALT_IMAGE.'" class="thumb'.($objData["objMature"] ? " mature" : "").
					'" title="'.strip_tags(formatText($objData["objTitle"])).
					'" src="'.urlf().$filename.'" /></a></div>'.
					'<div class="sep">'.$objTitle.'</div>'.
					'<div class="hline">&nbsp;</div>'.
					'<div class="sep a_left">'.formatText($cluData["cluFeaturedDesc"]).'</div>';
			}
			?>
		</div>
		<?
	}
	?>

	<?ob_start()?>
	<div class="sep container2 a_center">
		<div>
			<form action="<?=url("clubgallery/".$cluid)?>" method="get">
			<button class="submit wide largetext" type="submit">
				<?=getIMG(url()."images/emoticons/submission.png") ?>
				<?=_SUBMISSIONS ?>
			</button>
			</form>
		</div>
		<div class="sep">
			<?
			$select = "SELECT * FROM `objects`,`clubObjects`";

			$where = "`objPending` = '0' AND `objDeleted` = '0' ".
				"AND `cloObject` = `objid` AND `cloClub` = '$cluid' $whereMature";
			$limit = 8;
			unset($order);
			$more = url("clubgallery/".$cluid);
			include(INCLUDES."mod_minigallery.php");
			?>
		</div>
	</div>
	<?

	if(count($objList))
		ob_end_flush();
	else
		ob_end_clean();

	ob_start();

	$result = sql_query("SELECT * FROM `journals` WHERE `jouCreatorType` = 'clu' AND `jouCreator` = '".$cluid."' ORDER BY `jouSubmitDate` DESC LIMIT 1");

	if($jouData = mysql_fetch_assoc($result))
	{
		include_once(INCLUDES."comments.php");
		$comData = array();
		$comData["comid"] = 0;
		$comData["comCreator"] = $jouData["jouAnnCreator"];
		$comData["comComment"] = $jouData["jouEntry"];
		$comData["comSubject"] = $jouData["jouTitle"];
		$comData["comSubmitDate"] = $jouData["jouSubmitDate"];
		$comData["comNoEmoticons"] = $jouData["jouNoEmoticons"];
		$comData["comNoSig"] = $jouData["jouNoSig"];
		$comData["comNoBBCode"] = $jouData["jouNoBBCode"];
		$comData["comAllowImage"] = true;
		echo '<div style="margin-top: -6px">';
		showComment($comData, 0);
		echo '</div>';
	}

	$journalContents = ob_get_contents();
	ob_end_clean();

	if( $journalContents != "" )
	{
		?>
		<div class="sep container2">
			<div class="a_center">
				<form action="<?=url("announcement/".$cluid)?>" method="get">
				<button class="submit wide largetext" type="submit">
					<?=getIMG(url()."images/emoticons/journal.png") ?>
					<?=_ANNOUNCEMENT ?>
				</button>
				</form>
			</div>
		</div>
		<div><?= $journalContents ?></div>
		<div class="container2" style="margin-top: 2px">
			<div class="largetext mar_bottom a_center"><?= _ANNOUNCEMENT_PAST_ENTRIES ?></div>
			<?

			$entries = array();
			$result = sql_query("SELECT * FROM `journals` WHERE `jouCreatorType` = 'clu' AND `jouCreator` = '".$cluData["cluid"]."' ORDER BY `jouSubmitDate` DESC LIMIT 9");

			while($jouData = mysql_fetch_assoc($result)) {
				$entries[] = $jouData;
			}

			$togo = 8;

			foreach($entries as $entry)
			{
				iefixStart();
				?>
				<div class="f_left mar_bottom">
					<a href="<?=url("announcement/".$cluid."/".$entry["jouid"])?>">
					<?=getIMG(url()."images/emoticons/journal.png") ?>
					<?=formatText( $entry["jouTitle"]) ?>
					</a>
				</div>
				<div class="f_right mar_bottom">
					<?=gmdate($_auth["useDateFormat"], applyTimezone(strtotime($entry["jouSubmitDate"]))) ?>
				</div>
				<div class="clear">&nbsp;</div>
				<?
				iefixEnd();

				$togo--;

				if( $togo == 0 )
					break;
			}

			if( count( $entries ) == 9 )
			{
				?>
				<div class="a_right">
					<a class="disable_wrapping smalltext" href="<?=url("announcement/".$cluid)?>">
					<?=_MORE ?>
					<?=getIMG(url()."images/emoticons/nav-next.png") ?>
					</a>
				</div>
				<?
			}

			?>
		</div>
		<?
	}

	if( !$cluData[ "cluNoMembers" ])
	{
		?>
		<div class="sep container2">
			<div>
				<form action="<?=url("members/".$cluid)?>" method="get">
				<button class="submit wide largetext" type="submit">
					<?=_MEMBERS ?>
				</button>
				</form>
			</div>
			<div class="mar_left sep">
				<?iefixStart()?>
				<?
				$friendLimit = 8;
				$guestAccess = isLoggedIn() ? "" : "AND `useGuestAccess` = '1'";
				// friends = those who watch each other
				$friendQuery = "SELECT `useid` FROM `users`,`useClubs`,`useExtData` ".
					"WHERE `useid` = `useCmember` ".
					"AND `useid` = `useEid` ".
					"$guestAccess ".
					"AND `useCclub` = '$cluid' ".
					"AND `useCpending` = '0' ORDER BY `useCid` DESC LIMIT 10";
				$result = sql_query($friendQuery);
				$friendCount = mysql_num_rows($result);
				$odd = true;
				$togo = $friendLimit;
				while($rowData = mysql_fetch_assoc($result)) {
					if($togo <= 0) break;
					$togo--;
					?>
					<div class="<?=$odd ? "f_left" : "f_right"?> mar_right mar_bottom">
						<?
							if(!$odd) echo getUserLink($rowData["useid"])." &nbsp; ";
							echo getUserAvatar("", $rowData["useid"], false, true);
							if($odd) echo " &nbsp; ".getUserLink($rowData["useid"]);
						?>
					</div>
					<?
					$odd = !$odd;
					if($odd) echo '<div class="clear">&nbsp;</div>';
				}
				?>
				<?iefixEnd()?>
				<div class="clear">&nbsp;</div>
				<?
				if($friendCount > $friendLimit) {
					?>
					<div class="a_right">
						<a class="disable_wrapping smalltext" href="<?=url("members/".$cluid)?>">
						<?=_MORE ?>
						<?=getIMG(url()."images/emoticons/nav-next.png") ?>
						</a>
					</div>
					<?
				}
				?>
			</div>
		</div>
		<?
	}

	?>
</div>

<div class="rightside" style="margin-top: -1px">
	<?
	include_once(INCLUDES."comments.php");
	showAllComments($cluid, "clu", false, true, true, false);
	?>
</div>
<div class="clear">&nbsp;</div>

</div>
