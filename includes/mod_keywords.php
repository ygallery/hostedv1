<?

// Input variables:
//
// $defaultKeywords - the list of pre-defined keywords, e.g. "5 7 12".
//
// Output variables:
//
// $_POST["keywordList"] - after submission of the form, contains the list of chosen keywords,
//   e.g. "7 12 17"

?>
<script type="text/javascript">
//<![CDATA[
	var isIE = <?= $_isIE ? 1 : 0 ?>;
//]]>
</script>
<?

$fnKeywords = findNewestFile( "scripts/__keywordsCache_*.js", "" );
$fnKeywordDesc = findNewestFile( "scripts/__keywordsCacheDesc_*.js", "" );

// 0 = embed scripts into the page (gzip)
// 1 = load scripts remotely (no gzip, but js files should be cached by the browser)

if( 1 )
{
	?>
	<script src="<?= urlf().$fnKeywords ?>" type="text/javascript"></script>
	<script src="<?= urlf().$fnKeywordDesc ?>" type="text/javascript"></script>
	<?
}
else
{
	?>
	<script type="text/javascript">
	//<![CDATA[

		<?
		include( $fnKeywords );
		include( $fnKeywordDesc );
		?>

	//]]>
	</script>
	<?
}

?>
<script src="<?= urlf() ?>scripts/keywords_decoder.js?1" type="text/javascript"></script>
<noscript>
	</div>
	<div class="container">
		<div class="container2 error notsowide">
			<?= _NEED_JAVASCRIPT ?>
		</div>
</noscript>
<div style="clear: both"><br /></div>
<div class="sep caption"><?= _KEYWORDS_CHOSEN ?>:</div>
<div class="container2 notsowide" id="chosenKeywordsList"><?= strtolower( _NONE ) ?></div>
<input id="keywordList" name="keywordList" type="hidden" value="<?
	if( isset( $_POST[ "keywordList" ]) && ( $_POST[ "keywordList" ] != "" ))
		echo htmlspecialchars( substr( $_POST[ "keywordList" ], 0, 1000 ));
	else
		echo "";
?>" />
<?

$fn = findNewestFile(SCRIPTS."__keywordsCache_*.id","");

if( file_exists( $fn ))
{
	$firstKeywordId = file_get_contents($fn);
	include( INCLUDES."mod_keywordsjs.php" );
	return;
}

?>