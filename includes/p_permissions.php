<?php

$_documentTitle = "Administrative Permissions";
if(!isAuthorized('isPermAdmin'))
{
	include( INCLUDES."p_notfound.php" );
	return;
}

if($_cmd[1] == 'change' && $_cmd[2] != null)
{
	$result = array_keys($_POST);
	$uid = (int)$_cmd[2];

	for($i = 0; $i < (count($result)); $i++)
	{
		$sth = $dbh->prepare("UPDATE accessExt
			SET `".$result[$i]."` = '".$_POST[$result[$i]]."'
			WHERE `uid` = '".$uid."'");
		$sth->execute();
	}
}

?>

<?
	// Reload the Permissions Table
if(isset($_GET['reload'])){
	$sth = $dbh->prepare("SELECT useid, useIsModerator, useIsSModerator, useIsDeveloper
		FROM users, useExtData
		WHERE (useid = useEid AND useIsModerator = '1')
		OR (useid = useEid AND useIsSModerator = '1')
		OR (useid = useEid AND useIsDeveloper = '1')");
	$sth->execute();

	$stm = $dbh->prepare("SELECT `useUsername`, `useid`
					FROM `users`, `accessExt`
					WHERE `uid` = `useid`
					");
	$stm->execute();
	if($stm->rowCount() == 0){
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		for($i = 0; $i < count($result); $i++)
		{
			$sth = $dbh->prepare("INSERT INTO accessExt(uid)
			VALUES(?)");
			$sth->bindParam(1, $result[$i]['useid'], PDO::PARAM_INT);
			$sth->execute();
		}
	}
}

// Reload brand new users
if(isset($_GET['loadnew'])){
	$sth = $dbh->prepare("INSERT INTO `accessExt`(`uid`)
	SELECT useEid FROM `useExtData` WHERE useEid NOT IN ( SELECT uid FROM `accessExt`)
	AND `useIsHelpdesk` = '1'
	OR useEid NOT IN ( SELECT uid FROM `accessExt`)
	AND `useIsModerator` = '1'
	OR useEid NOT IN ( SELECT uid FROM `accessExt`)
	AND `useIsSModerator`= '1'
	OR useEid NOT IN ( SELECT uid FROM `accessExt`)
	AND `useIsDeveloper` = '1'");
	$sth->execute();
}
?>

<div class="header">
	<div class="header_title"><?= _ADMINISTRATOR ?></div>
	<div class="subheader">Admin Permissions Set</div>

	<?
		$active = 4;
		include(INCLUDES."mod_adminmenu.php");
	?>
</div>
<div class="container">
<?
$sth = $dbh->prepare("SELECT COUNT(accid) FROM accessExt");
$sth->execute();
if($sth->fetch()[0] == 0){
?>
<form action="<?= url(".")?>" method="GET">
<button name="reload" value="1">Reload (First time only!)</button>
</form>
<?}
else
{
?>
<form action="<?= url(".")?>" method="GET">
<button name="loadnew" value="1">Load New Users</button>
</form>
<?
}
?>
<h1>Permission Setup:
<?
	if( isset($_GET['filter']) || $_cmd[1] == 'change' && $_cmd[2] != null)
	{
		$fid = (int)( ( isset($_GET['filter'])) ? $_GET['filter'] : $_cmd[2]);
		$sth = $dbh->prepare("SELECT useUsername
			FROM users
			WHERE useid = ?");
		$sth->bindParam(1, $fid, PDO::PARAM_INT);
		$sth->execute();
		echo $sth->fetch(PDO::FETCH_ASSOC)['useUsername'];
	}
?></h1>
<div class="container2 mar_bottom">
<?
	if(isset($_GET['filter']) || $_cmd[1] == 'change')
	{
		$fid = (int)((isset($_GET['filter'])) ? $_GET['filter'] : $_cmd[2]);
		$sth = $dbh->prepare("SELECT *
			FROM accessExt
			WHERE uid = ?");
		$sth->bindParam(1, $fid, PDO::PARAM_INT);
		$sth->execute();

		$result = $sth->fetch(PDO::FETCH_ASSOC);

		function returnOptSel($value){
			global $result;

			$html = null;
			for($i = 0; $i <= 1; $i++)
			{
				$html .= '<option value="'.$i.'"
				'.(($result[$value] != $i) ? null : 'selected="selected"').'
				>
				'.(($i == 0) ? "No" : "Yes").'
				</option>'."\n";
			}
			return $html;
		}
?>
<form action="<?= url("permissions/change/".(isset($_GET['filter']) ? (int)$_GET['filter'] : null))?>" method="POST">
	<select name="isPermAdmin" class="smalltext">
		<?= returnOptSel('isPermAdmin'); ?>
	</select> <label for="isPermAdmin"> Has Access to Permissions</label>
	<br /><br/>
	<select name="isKeywordsAdmin" class="smalltext">
		<?= returnOptSel('isKeywordsAdmin'); ?>
	</select> <label for="isKeywordsAdmin"> Has Access to Keywords</label>
	<br />
	<div style="float:right;">
		<button class="submit">Change</button>
	</div>
	<div style="clear: both;"></div>
</form>
<?
	}
?>
</div>
<div class="mar_right">
	<form action="<?= url("permissions")?>" method="GET">
		<select name="filter" class="smalltext">
			<?
				$sth = $dbh->prepare("SELECT `useUsername`, `useid`
					FROM `users`, `accessExt`
					WHERE `uid` = `useid`
					");
				$sth->execute();
				if($sth->rowCount() == 0){
					echo '<option value="*">*</option>';
				}
				else
				{
					while($result = $sth->fetch(PDO::FETCH_ASSOC))
					{
						echo '<option value="'.$result['useid'].'" '.
						(( $_cmd['1'] == 'change' && $result['useid'] == $_cmd[2] ||
							$result['useid'] == (isset($_GET['filter']) ? (int)$_GET['filter'] : 0)) ? "selected=\"selected\"" : null )
						.'>'.$result['useUsername'].'</option>';
					}
				}
			?>
		</select>
		<button class="button">Load</button>
	</form>
</div>
</div>