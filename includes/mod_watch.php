<?

if( !isLoggedIn() || $useData[ "useid" ] == $_auth[ "useid" ])
{
	return;
}

$sql = "SELECT COUNT(*) FROM `watches`".dbWhere( array(
	"watCreator" => $useData[ "useid" ],
	"watUser" => $_auth[ "useid" ],
	"watType" => "use" ));

$result = sql_query( $sql );

$watched = ( mysql_result( $result, 0 ) > 0 );

$watchId = generateId();
$unwatchId = generateId();

$useid = $useData[ "useid" ];

$watchScript =
	"add_operation( 'w$useid' ); ".
	"var el = get_by_id( '$watchId' ); ".
	"if( el ) el.style.display = 'none'; ".
	"el = get_by_id( '$unwatchId' ); ".
	"if( el ) el.style.display = 'inline'; ".
	"return false;";

$unwatchScript =
	"add_operation( 'wu$useid' ); ".
	"var el = get_by_id( '$unwatchId' ); ".
	"if( el ) el.style.display = 'none'; ".
	"el = get_by_id( '$watchId' ); ".
	"if( el ) el.style.display = 'inline'; ".
	"return false;";

?>
<div class="f_right mar_right">
	<div class="largetext">
		<span id="<?= $unwatchId ?>" <?= $watched ? "" : 'style="display: none"' ?>>
			<a onclick="<?= $unwatchScript ?>" href="<?= url( "op/wu$useid" ) ?>" title="<?= _WATCH_STOP ?>">
			<?= getIMG( url()."images/emoticons/fav0.png" ) ?>
			<?= _WATCH ?></a>
		</span>
		<span id="<?= $watchId ?>" <?= $watched ? 'style="display: none"' : "" ?>>
			<a onclick="<?= $watchScript ?>" href="<?= url( "op/w$useid" ) ?>" title="<?= _WATCH_START ?>">
			<?= getIMG( url()."images/emoticons/fav1.png" ) ?>
			<?= _WATCH ?></a>
		</span>
	</div>
	<div class="normaltext">
		<a href="<?= url( "pm/for/".strtolower( $useData[ "useUsername" ])) ?>">
			<?= getIMG( url()."images/emoticons/pm.png" ) ?>
			<b><?= _PM_SEND ?></b>
		</a>
	</div>
</div>
