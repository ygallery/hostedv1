<div class="termsOfService">
	<p>Last updated: 4 November 2012</p>
	
	<h3 style="clear : both; font-weight : normal; padding-top : 12pt; border-top : 3px double; margin-left : 30pt;">If you are confused about any of these rules, see the <a href="/help">Help</a> section, ask a <a href="/helpdesk/faq/staff/info">staff member</a>, or post to <a href="/club/1">Gallery Star Runners</a>. <strong>DO NOT post a work you are concerned may violate the Terms of Service about without <a href="/club/1">checking with the staff</a> first.</strong></h3>
	
	<ol>
		<!-- Rule 1 -->
		<li><h3>Sex &amp; species</h3>
			<div>
				<h4>Penalties for Violation</h4>
				<span class="rule">1. a-i:</span>
				<ul>
					<li>First violation: Warning</li>
					<li>Second violation: 2 week suspension</li>
					<li>Third violation: 3 month suspension</li>
					<li>Fourth violation: Permanent ban</li>
				</ul>
			</div>
			<div>
				<h4>Exceptions to the Rules</h4>
				<span class="rule">1. a:</span>
				<ul>
					<li>Avatars</li>
					<li>Themes</li>
					<li>Extras</li>
					<li>IDs</li>
				</ul>
				b:
				<ul>
					<li>Extras</li>
				</ul>
			</div>
			<ol>
				<li>Humanoid (including furry) characters must be the focus of all works submitted to the gallery.  Works containing no characters (eg. landscapes, inanimate objects) are not allowed in the main gallery.</li>
				<li>Unless they are "tentacles from the formless void", animals (except humanoid furry characters) may not appear to be having sexual contact with any character or each other.</li>
				<li>Works containing only non-gender-specific body parts (eg. works containing only hands, eyes, feet, etc.) are not allowed in the main gallery unless they are part of an anatomy tutorial or appear as a minor part of a multi-page comic.</li>
			</ol>
		</li>
		<!-- Rule 2 -->
		<li style="clear : both;"><h3>Photographic materials</h3>
			<div>
				<h4>Penalties for Violation</h4>
				<span class="rule">2. a-c:</span>
				<ul>
					<li>First violation: Warning</li>
					<li>Second violation: 2 week suspension</li>
					<li>Third violation: 3 month suspension</li>
					<li>Fourth violation: Permanent ban</li>
				</ul>
				d:
				<ul>
					<li>First violation: Permanent ban</li>
				</ul>
			</div>
			<div>
				<h4>Exceptions to the Rules</h4>
				<span class="rule">2. a, c:</span>
				<ul>
					<li>Avatars</li>
					<li>Themes</li>
					<li>Extras</li>
					<li>IDs</li>
				</ul>
				c:
				<ul>
					<li>Literary work thumbnails</li>
				</ul>
			</div>
			<ol>
				<li>Photographs containing real people (alive or dead) are not allowed in the main gallery.</li>
				<li>Screenshots are not allowed in the main gallery unless they meet one or more of these criteria:
					<ol>
						<li>Screenshot is taken of a wholly original work (eg. a work in progress within Photoshop)</li>
						<li>Screenshot is from a game that you developed (eg. <strong>not</strong> World of Warcraft/Second Life/any other commercial game you didn't develop)</li>
						<li>Screenshot is used as part of a tutorial</li>
					</ol>
				</li>
				<li>Photomanipulation is not allowed in the main gallery.</li>
				<li><strong>Absolutely NO photographic material may be adult in nature anywhere on the site EVER.</strong></li>
			</ol>
		</li>
		<!-- Rule 3 -->
		<li style="clear : both;"><h3>Shota</h3>
			<div>
				<h4>Penalties for Violation</h4>
				<span class="rule">3. a:</span>
				<ul>
					<li>First violation: Permanent ban,<br />ISP &amp; law enforcement report filed</li>
				</ul>
				b, c, d:
				<ul>
					<li>First violation: 2 week suspension</li>
					<li>Second violation: Permanent ban</li>
				</ul>
				e:
				<ul>
					<li>First Violation: Warning</li>
					<li>Second Violation: 2 week suspension</li>
					<li>Third Violation: Permanent Ban</li>
				</ul>
				<h4>Exceptions to the Rule</h4>
				<span class="rule">3. e:</span>
				<ul>	
					<li>Extras</li>
				</ul>
			</div>
			<ol>
				<li>Characters that are depicted as minors in sexually-themed works anywhere on the site must not appear imperceptible from a photograph and must not be modelled or based upon an actual child.</li>
				<li>Sexually-themed 3D illustrations that contain characters that appear depicted as minors are not allowed anywhere on the site.</li>
				<li>
					Illustrations with characters depicted as minors anywhere on the site must not be based upon an actual child and must be stylised in a manner that could in no way be perceived as resembling an actual child.
				</li>
				<li><strong>Absolutely NO photographs of people that are or appear to be under the age of 18 may appear anywhere on the site EVER.</strong></li>
				<li>Sexually-themed illustrations with characters depicted as minors that are, or appear to be, six (6) years old and under are not allowed on the main gallery.</li>
			</ol>
		</li>
		<!-- Rule 4 -->
		<li style="clear : both;"><h3>Keywords &amp; filters</h3>
			<div>
				<h4>Penalties for Violation</h4>
				<span class="rule">4. a:</span><br />
				Accidental violations:
				<ul>
					<li>Occasional violations: Warning</li>
					<li>Repeated violations: Permanent ban</li>
				</ul>
				Intentional violations:
				<ul>
					<li>First violation: 2 week suspension</li>
					<li>Second violation: Permanent ban</li>
				</ul>
				b:
				<ul>
					<li>First violation: Warning</li>
					<li>Second violation: 1 week suspension</li>
					<li>Third violation: 1 month suspension</li>
					<li>Fourth violation: Permanent ban</li>
				</ul>
			</div>
			<ol>
				<li>You must properly classify all submitted works within the keywords system.</li>
				<li>You must properly classify all submitted works within the filters system.</li>
			</ol>
		</li>
		<!-- Rule 5 -->
		<li style="clear : both;"><h3>Age restriction</h3>
			<div>
				<h4>Penalties for Violation</h4>
				<span class="rule">5. a:</span>
				<ul>
					<li>First violation: Suspension until 18<sup>th</sup> birthday</li>
					<li>Second violation: New account ban and<br />+3 month suspension on original account</li>
					<li>Third violation: All accounts banned &amp;<br />ISP report filed</li>
				</ul>
				b:
				<ul>
					<li>First violation: Suspension equal to original account</li>
				</ul>
				c:
				<ul>
					<li>First violation: Permanent account ban</li>
				</ul>
			</div>
			<ol>
				<li>You must be over the age of 18 to join the site.</li>
				<li>You may not allow a user under the age of 18 to access the site using your account.</li>
				<li>You may not advise underaged users to hide their age, or instruct or assist underage users on circumventing suspensions.</li>
			</ol>
		</li>
		<!-- Rule 6 -->
		<li style="clear : both;"><h3>Plagiarism &amp; art theft</h3>
			<div>
				<h4>Penalties for Violation</h4>
				<span class="rule">6. a, g, f:</span>
				<ul>
					<li>First violation: 2 week suspension</li>
					<li>Second violation: Permanent ban &amp;<br />ISP report filed</li>
				</ul>
				c:<br />
				Accidental violations:
				<ul>
					<li>Occasional violations: Warning</li>
					<li>Repeated violations: Permanent ban</li>
				</ul>
				Intentional violations:
				<ul>
					<li>First violation: 2 week suspension</li>
					<li>Second violation: Permanent ban</li>
				</ul>
				b, e, h:
				<ul>
					<li>First violation: Warning</li>
					<li>Second violation: 2 week suspension</li>
					<li>Third violation: 3 month suspension</li>
					<li>Fourth violation: Permanent ban &amp;<br />ISP report filed</li>
				</ul>
				d:
				<ul>
					<li>First violation: Permanent ban &amp;<br />ISP report filed</li>
				</ul>
			</div>
			<div>
				<h4>Exceptions to the Rules</h4>
				<span class="rule">6. a-c:</span>
				<ul>
					<li>Avatars</li>
					<li>Literary work thumbnails</li>
					<li>Themes</li>
					<li>IDs</li>
				</ul>
				f, g, h:
				<ul>
					<li>Avatars</li>
					<li>Themes</li>
				</ul>
				e, h:
				<ul>
					<li>IDs</li>
				</ul>
			</div>
			<ol>
				<li>Posting works created entirely by others is not allowed. <strong>This includes gifts or commissioned art.</strong></li>
				<li>To post works partially created by others, you must have done a significant portion of the work yourself (eg. colouring, sketching, inking the work) and <strong>you MUST link it to the other artist using the Collaboration feature</strong>. If you cannot use the Collaboration feature for any reason, <strong>you may not post the work</strong>.</li>
				<li>Works that are created using references (including photos) must provide a link or citation to the original reference material.</li>
				<li>You may not claim to be the original creator of a work that is not yours, even if you are allowed to redistribute it by the original artist.</li>
				<li>Works created using "doll makers" or other such generators are not allowed.</li>
				<li>Works that reference other works (including photos) must differ <strong>significantly</strong> from the original work. (No direct copies!)</li>
				<li>Traces, paint-overs, grid copies, vectors, overlays, etc. are not allowed unless you are the copyright owner of the original source material. <strong>This includes photos!</strong></li>
				<li>Use of official art, trademarks, or copyrighted materials, such as corporate logos or commercial music, is not allowed.</li>
			</ol>
		</li>
		<!-- Rule 7 -->
		<li style="clear : both;"><h3>Community standards &amp; <a href="http://www.ietf.org/rfc/rfc1855.txt">etiquette</a></h3>
			<div>
				<h4>Penalties for Violation</h4>
				<span class="rule">7. a, c:</span>
				<ul>
					<li>First violation: 1 week suspension</li>
					<li>Second violation: 1 month suspension</li>
					<li>Third violation: 3 month suspension</li>
					<li>Fourth violation: Permanent ban</li>
				</ul>
				b:<br />
				Accidental violations:
				<ul>
					<li>Occasional violations: 1 week suspension</li>
					<li>Repeated violations: 6 month suspension</li>
				</ul>
				Intentional violations:
				<ul>
					<li>First violation: 2 week suspension</li>
					<li>Second violation: Permanent ban</li>
				</ul>
				d, e:<br />
				<ul>
					<li>First violation: Warning</li>
					<li>Subsequent violations: 1 week suspension</li>
				</ul>
				f: <br />
				<ul>
					<li>First violation: 1 day suspension</li>
					<li>Second violation: 2 week suspension</li>
					<li>Third violation: Permanent ban</li>
				</ul>
			</div>
			<ol>
				<li>Do not <a href="http://www.catb.org/~esr/jargon/html/F/flame.html">flame</a>, harass, or mock other users or staff, or invoke <a href="http://en.wikipedia.org/wiki/Godwin%27s_Law">Godwin's Law</a>. This includes posting journals attacking specific people on the site without calling them out by name.</li>
				<li>Do not forward <a href="http://www.cs.rutgers.edu/~watrous/chain-letters.html">chain letters</a>, tell others to engage in these behaviours, use the comments or private message system to send bulk messages (&quot;spam&quot;), send invitations or other junk mail to the y!Gallery email address, or otherwise abuse the y!Gallery services.</li>
				<li>Do not post works blatantly designed to mock, goad, or degrade other users or groups of people.  This includes directing racial slurs or derogatory words at others.</li>
				<li>Do not participate in &quot;tagging&quot; or tell others to engage in this behaviour.</li>
				<li>Do not submit more than three (3) similar or &quot;look-a-like&quot; submissions onto the gallery.  These will be considered spam and will be removed as duplicate submissions.  More than 3 can be uploaded as a composite picture or .gif animation, putting them all on one image or animation.</li>
				<li>Do not purposefully re-upload images that have been reported for moderation.</li>
			</ol>
		</li>
		<!-- Rule 8 -->
		<li style="clear : both;"><h3>Additional accounts</h3>
			<div>
				<h4>Penalties for Violation</h4>
				<span class="rule">By user:</span>
				<ul>
					<li>First violation: New account ban and<br />+3 month ban on original account</li>
					<li>Second violation: Permanent ban &amp;<br />ISP report filed</li>
				</ul>
				<span class="rule">By friend:</span>
				<ul>
					<li>First violation: Same punishment as<br /> the other user</li>
				</ul>
			</div>
			<p>
				Do not attempt to bypass account suspension/bans or post materials that are against the Terms of Service by creating additional accounts. Ever. Even if you think you were suspended wrongly, you will still be penalised for creating a new account to bypass the suspension, so don't do it.
			</p>
			<p>
				If your account is suspended or banned and you feel the need to discuss it with a staff member, you can contact the staff by clicking on &quot;<a href="/members/1">Staff</a>&quot; in the main menu, clicking on a staff member's name, and looking in their profile for off-site contact methods (email, instant message, etc.). Not finding contact information for a staff member does not give you permission to create a new account. See the previous paragraph.
			</p>
			<p>
				<strong>If you assist another user in bypassing their suspension, you will receive the same punishment as them.</strong>
			</p>
		</li>
		
		<!-- Rule 9 -->
		<li style="clear : both;"><h3>Objective quality of works</h3>
			<div>
				<h4>Penalties for Violation</h4>
				<span class="rule">9. a-c:</span><br />
				Accidental violations:
				<ul>
					<li>Occasional violations: Warning</li>
					<li>Repeated violations: Permanent ban</li>
				</ul>
				Intentional violations:
				<ul>
					<li>First violation: 2 week suspension</li>
					<li>Second violation: Permanent ban</li>
				</ul>
			</div>
			<div>
				<h4>Exceptions to the Rules</h4>
				<span class="rule">9. a-c:</span>
				<ul>
					<li>Avatars</li>
					<li>Extras</li>
					<li>IDs</li>
				</ul>
			</div>
			<ol>
				<li>
					Do not submit works that contain any of the following issues without first <a href="http://y-hosting.net/fixart">fixing them</a>:
					<ul>
						<li>Lines from line or graph paper</li>
						<li>Wrinkling or crumpling of the paper that detracts from the drawing</li>
						<li>Dirt, stains, or smudges on the paper</li>
						<li>Blurry, grainy, or pixelated transfer (transferred using a camera)</li>
						<li>Very light lines (lines that are invisible or nearly invisible when thumbnailed)</li>
						<li>Visible binder rings or spiral binding</li>
						<li>Visible edges of paper</li>
						<li>Scanner shadows around one or more edges</li>
						<li><a href="http://en.wikipedia.org/wiki/Vignetting">Vignetting</a></li>
						<li>Poor contrast between lines and background (for line art)</li>
						<li>Writing visible from the other side of the paper</li>
						<li>Excessive white-space (insufficient cropping)</li>
						<li>Excessively large submission file size</li>
						<li>Visible unrelated writing (eg. school notes)</li>
						<li>Censor bars used jarringly to hide Terms of Service violations</li>
						<li>Digital effects that emulate any of the above</li>
					</ul>
				</li>
				<li>This rule intentionally left blank.</li>
				<li>
					Scribbles are not allowed. Not to be confused with quick sketches, works in progress, or works by amateur artists, scribbles are generally identified as having one or more of these features:
					<ul>
						<li>Hastily-drawn works with no apparent care given to the outcome of the work</li>
						<li>Works that contain stick figures or blob figures in lieu of fully drawn characters</li>
					</ul>
				</li>
			</ol>
		</li>
		<!-- Rule 10 -->
		<li style="clear : both;"><h3>Written Works</h3>
			<div>
				<h4>Penalties for Violation</h4>
				<span class="rule">10. a:</span><br />
				Accidental violations:
				<ul>
					<li>Occasional violations: Warning</li>
					<li>Repeated violations: Permanent ban</li>
				</ul>
				Intentional violations:
				<ul>
					<li>First violation: 2 week suspension</li>
					<li>Second violation: Permanent ban</li>
				</ul>
				b:<br />
				<ul>
					<li>First violation: Warning</li>
					<li>Second violation: 2 week suspension</li>
					<li>Third violation: 3 month suspension</li>
					<li>Fourth violation: Permanent ban</li>
			</div>
			<p>
				<strong>Written submissions must conform to the previous rules</strong>
			</p>
			<ol>
				<li>
 					Written works must be reasonably well edited and use proper spelling, grammar, capitalisation, and punctuation, and be at least 375 words long. Written works less than 375 words may be posted as part of a compilation of multiple works that total at least 375 words.
 				</li>
 				<li>
					Use of official art, trademarks, or copyrighted materials for thumbnails is not allowed.  You may use the following:
					<ul>
						<li>Artwork you have created yourself. You must state that it is your own work in the Artist's comment and provide a link to where you have posted it if possible.</li>
						<li>Artwork created by someone else provided that you have permission.  You must cite this permission, and provide a link to the original source.</li>
						<li>Free-use photography.  You must provide a link to the original source.</li>
						<li>Text-only thumbnails</li>
						<li>The <a href="/images/litthumb_new.png">y!Gallery Default Literary Thumbnaili (new)</a></li>
						<li>The <a href="/images/litthumb.png">y!Gallery Default Literary Thumbnail (old)</a></li>
						<li><a href="/images/litthumb_4.jpg">Alternate Thumbnail 1</a></li>
						<li><a href="/images/litthumb_5.jpg">Alternate Thumbnail 2</a></li>
						<li><a href="/images/litthumb_8.jpg">Alternate Thumbnail 3</a></li>
						<li><a href="/images/litthumb_9.jpg">Alternate Thumbnail 4</a></li>
						<li>For more information regarding literary thumbnails see <a href="http://fukuniji.y-gallery.net/helpdesk/faq/policy/litthumb/">this FAQ article</a>.</li>
					</ul>
					
 				</li>

		</li>
	</ol>
	<h3 style="clear : both; font-weight : normal; padding-top : 12pt; border-top : 3px double; margin-left : 30pt;">If you are confused about any of these rules, see the <a href="/help">Help</a> section, ask a <a href="/helpdesk/faq/staff/info/">staff member</a>, or post to <a href="/club/1">Gallery Star Runners</a>. <strong>DO NOT post a work you are concerned may violate the Terms of Service about without <a href="/club/1">checking with the staff</a> first.</strong></h3>
	<p>
		Please follow the above rules to make this a happy, safe, and overall friendly community. <strong>We reserve the right to display, edit, and remove any or all artwork, journal entries, polls, custom themes, comments, and any other material posted to the site by any member at any time. We also reserve the right to suspend or ban any user account at any time for any reason we see fit.</strong>
	</p>
	<p>
		<strong>This Terms of Service is <em>not</em> an exhaustive list of what is and is not allowed on y!Gallery.</strong> The spirit of this document is just as important as its contents; it is written not only to define the rules, but also to give you an overall idea of what is and is not acceptable here, even if it isn't listed explicitly. In other words, there are no loopholes. You are expected to use common sense and your best judgement when on the gallery in order to be a helpful, upstanding member of the community (or, at least, not a nuisance to others).
	</p>
	<p>
		<strong>User accounts may not be renamed or deleted.</strong> Please be aware of this policy and think wisely when creating your account.
	</p>
	<p>
		<strong>Boring legal disclaimer</strong>: y!hosting, its owners, agents, and any other entities related to y!hosting or the y!Gallery system take no responsibility for the works posted to the y!Gallery system by its members. While y!hosting and its agents attempt to remove all illegal works from the site as quickly and thoroughly as possible, there is always the possibility that some submissions may be overlooked or dismissed in error. The y!Gallery system includes a rigorous and complex abuse control system in order to prevent improper use of the y!Gallery service, and we hope that its deployment indicates a good-faith effort to eliminate any illegal material on the site in a fair and unbiased manner. This abuse control system is run in accordance with the strict guidelines specified above. All works displayed here, whether pictorial or literary, are the property of their owners and not y!hosting. Opinions stated in personal journals or profiles of may not reflect the opinions or views of y!hosting or any of its owners, agents, or related entities.
	</p>
</div>
