<div class="sep error">
	Everything possible has been done to ensure this site works with every Web browser.<br />
	However, due to circumstances beyond our control, not all features will work properly in the browser you are using.<br />
	<b>Unfortunately, this is a problem with your Web browser (Microsoft Internet Explorer), and not with this site.</b><br />
	<br />
	We strongly recommend you switch to using a more standards-compliant browser as your primary Web browser. Standards-compliant
	browsers include <a href="http://getfirefox.com">Mozilla Firefox</a> (All OS), <a href="http://opera.com">Opera</a> (All OS),
	<a href="http://apple.com/safari">Apple Safari</a> (Mac OS X), and <a href="http://konqueror.org">Konqueror</a> (Linux).
	In addition to being faster and more secure, these browsers will display this and every other standards-compliant site properly.
</div>
<div class="sep">
	<h1>A brief history</h1>
	During the browser wars of the late 1990s, modifications of Internet Explorer and Netscape Navigator were focused on the addition of
	non-standard features. This is in contrast to more recent browsers which have been designed with Web standards in mind. Since version
	5, there have been no significant changes to IE's rendering engine, Trident. As a result, as of 2006, IE lags behind in support for
	major Web standards.
</div>
<div class="sep">
	<h1>This site is <em>standards-compliant</em></h1>
	That means that it has been written to ensure that any Web browser that properly implements the industry-defined
	<a href="http://www.w3.org">Web standards</a> (XHTML, CSS, and ECMAScript) will display it properly. Unfortunately, certain Web
	browsers, including the one you are currently using, violate these standards; therefore, you may notice problems when viewing
	the site.<br />
	<br />
	<div style="float : right; width : 200px; text-align : justify; border : 1px solid; padding : 10px; margin : 5px;">For examples of many of the problems with Microsoft Internet Explorer's standards compliance, please visit
	<a href="http://www.howtocreate.co.uk/wrongWithIE">How To Create</a>.<br />For an up-to-date chart illustrating standards-compliance within
	several major Web browsers, please visit <a href="http://nanobox.chipx86.com/browser_support.php">Nanobox</a>.</div>
	The following Web standards used on this site are not correctly supported by your Web browser:
	<ul>
		<li>PNG image format alpha channel</li>
		<li>PNG image format gamma correction</li>
		<li>JPEG image format progressive display</li>
		<li>IANA MIME-types</li>
		<li>CSS box model</li>
		<li>CSS selectors</li>
		<li>CSS pseudo-elements &amp; pseudo-classes</li>
		<li>CSS list styles</li>
		<li>CSS inheritable properties</li>
		<li>CSS float</li>
		<li>HTTP Referer for plug-ins (breaks Flash and Java)</li>
	</ul>
	Please download a standards-compliant browser today and start taking advantage of faster page loads, more advanced site
	features, and better security.
	<ul>
		<li><a href="http://www.getfirefox.com">Mozilla Firefox</a> (for Windows 95+, Mac OS X 10.3+, Linux 2.2.14+)</li>
		<li><a href="http://www.opera.com">Opera</a> (for Windows 95+, BeOS 5, Mac OS X 10.2+, Linux with libstdc++ &amp; X11R6.3)</li>
		<li><a href="http://www.apple.com/safari">Apple Safari</a> (for Mac OS X 10.3+)</li>
		<li><a href="http://www.konqueror.org">KDE Konqueror</a> (Linux with X11 &amp; KDE)</li>
	</ul>
</div>
<div class="sep">
	(Portions of this page taken and modified from <a href="http://wikipedia.org">Wikipedia</a>)
</div>
