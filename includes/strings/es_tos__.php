﻿<h1>Esto los términos del servicio traducidos es ANTICUADO. Lea por favor los términos del servicio INGLESES para la información correcta.</h1>
<div class="termsOfService">
	<ol>
		<!-- Rule 1 -->
		<li><h3>Sexo y especies</h3>
			<div>
				<h4>Penalidades por Violación</h4>
				<span class="rule">1. a-f:</span>
				<ul>
					<li>Primera violación: Advertencia</li>
					<li>Segunda violación: 2 semanas de suspensión</li>
					<li>Tercera violación: Prohibición permanente</li>
				</ul>
			</div>
			<div>
				<h4>Excepciones a las Reglas</h4>
				<span class="rule">1. a, d:</span>
				<ul>
					<li>Avatares</li>
					<li>Temas</li>
					<li>Extras</li>
					<li>IDs</li>
				</ul>
				e:
				<ul>
					<li>Avatares</li>
					<li>Temas</li>
					<li>Imágenes descriptivas de<br />trabajo literario</li>
					<li>Extras</li>
					<li>IDs</li>
				</ul>
				b, c, f:
				<ul>
					<li>Extras</li>
				</ul>
			</div>
			<ol>
				<li>Trabajos que contengan mujeres como característica principal no son permitidos en la galería principal.</li>
				<li>Chicas con pene/&quot;Futanari&quot; no son permitidas en la galería principal.</li>
				<li>El transvestismo esta permitido, pero solo si el personaje no se parece físicamente a una mujer (ej. senos) o ser identificado como mujer.</li>
				<li>Trabajos que contengan sólo animales no son permitidos en la galería principal. Los antropomorfos también no están permitidos.</li>
				<li>Trabajos que no contengan personajes (ej. paisajes, objetos inanimados) no son permitidos en la galería principal.</li>
				<li>Para trabajos que estén excusados de las reglas mencionadas anteriormente (ver Excepciones a la derecha), genitales femeninos y mujeres mostradas en poses altamente lascivas no están permitidos.</li>
			</ol>
		</li>
		<!-- Rule 2 -->
		<li style="clear : both;"><h3>Materiales fotográficos</h3>
			<div>
				<h4>Penalidades por Violación</h4>
				<span class="rule">2. a-c:</span>
				<ul>
					<li>Primera violación: Advertencia</li>
					<li>Segunda violación: 2 semanas de suspensión</li>
					<li>Tercera violación: Prohibición permanente</li>
				</ul>
				d:
				<ul>
					<li>Primera violación: Prohibición permanente</li>
				</ul>
			</div>
			<div>
				<h4>Excepciones a las Reglas</h4>
				<span class="rule">2. a, c:</span>
				<ul>
					<li>Avatares</li>
					<li>Temas</li>
					<li>Imágenes descriptivas de<br />trabajo literario</li>
					<li>Extras</li>
					<li>IDs</li>
				</ul>
			</div>
			<ol>
				<li>Material fotográfico no puede ser el punto principal de la mayor parte de un trabajo en la galería principal.</li>
				<li>a.Fotos de la pantalla no son permitidas en cualquier parte del sitio a menos que sigan uno o más de los siguientes criterios:
					<ol>
						<li>La foto fue tomada de un trabajo totalmente original (ej. un trabajo en progreso dentro de Photoshop)</li>
						<li>La foto es de un juego que tu desarrollaste (ej. <b>no</b> World of Warcraft/Second Life/cualquier otro juego comercial que tú no hiciste)</li>
						<li>i.La foto es usada como parte de un tutorial</li>
						<li>i.La foto es usada como un comprobante para <i>kiriban</i> o regalo artístico en este sitio</li>
					</ol>
				</li>
				<li>La foto manipulación no esta permitida en la galería principal.</li>
				<li><b>Absolutamente NINGUN material fotográfico puede tener contenido adulto en cualquier parte de este sitio JAMAS.</b></li>
			</ol>
		</li>
		<!-- Rule 3 -->
		<li style="clear : both;"><h3>Shota</h3>
			<div>
				<h4>Penalidades por Violación</h4>
				<span class="rule">3. a:</span>
				<ul>
					<li>Primera violación: Prohibición permanente,<br />ISP (proveedor de servicio de Internet)<br />y autoridades</li>
				</ul>
				b:
				<ul>
					<li>Primera violación: 2 semanas de suspensión</li>
					<li>Segunda violación: Prohibición permanente</li>
				</ul>
			</div>
			<ol>
				<li>Personajes que sean ilustrados como menores de edad en trabajos con un tema sexual en cualquier parte del sitio, no deben parecer imperceptibles de una fotografía y no deben ser modelados o basados de un niño real.</li>
				<li>Ilustraciones 3D con un tema sexual que contengan personajes que parezcan menores de edad no son permitidos en cualquier parte del sitio.</li>
			</ol>
		</li>
		<!-- Rule 4 -->
		<li style="clear : both;"><h3>Palabras clave y Filtros</h3>
			<div>
				<h4>Penalidades por Violación</h4>
				<span class="rule">4. a-b:</span><br />
				Accidental violations:
				<ul>
					<li>Occasional violations: Advertencia</li>
					<li>Repeated violations: Prohibición permanente</li>
				</ul>
				Intentional violations:
				<ul>
					<li>Primera violación: 2 semanas de suspensión</li>
					<li>Segunda violación: Prohibición permanente</li>
			</ul>
			</div>
			<ol>
				<li>Debes clasificar apropiadamente todos tus trabajos enviados con el sistema de palabras clave.</li>
				<li>Debes clasificar apropiadamente todos tus trabajos enviados con el sistema de filtros.</li>
			</ol>
		</li>
		<!-- Rule 5 -->
		<li style="clear : both;"><h3>Age restriction</h3>
			<div>
				<h4>Penalidades por Violación</h4>
				<span class="rule">5.</span>
				<ul>
					<li>Primera violación: Suspension until 18<sup>th</sup> birthday</li>
					<li>Segunda violación: New account ban and<br />+3 month suspension on original account</li>
					<li>Tercera violación: All accounts banned y<br />un reporte para tu ISP</li>
					</ul>
			</div>
			Debes tener 18 años o más para poderte unir al sitio.
		</li>
		<!-- Rule 6 -->
		<li style="clear : both;"><h3>Plagios y robo de arte</h3>
			<div>
				<h4>Penalidades por Violación</h4>
				<span class="rule">6. a-b:</span>
				<ul>
					<li>Primera violación: 2 semanas de suspensión</li>
					<li>Segunda violación: Prohibición permanente y<br />un reporte para tu ISP</li>
				</ul>
				c, e:
				<ul>
					<li>Primera violación: Advertencia</li>
					<li>Segunda violación: 2 semanas de suspensión</li>
					<li>Tercera violación: Prohibición permanente y<br />un reporte para tu ISP</li>
				</ul>
				d:
				<ul>
					<li>Primera violación: Prohibición permanente y<br />un reporte para tu ISP</li>
				</ul>
			</div>
			<div>
				<h4>Excepciones a las Reglas</h4>
				<span class="rule">6. a:</span>
				<ul>
					<li>Avatares</li>
					<li>Temas</li>
				</ul>
				b:
				<ul>
					<li>Avatares</li>
					<li>Temas</li>
					<li>IDs</li>
				</ul>
				c:
				<ul>
					<li>Avatares</li>
					<li>Temas</li>
					<li>Imágenes descriptivas de<br />trabajo literario</li>
					<li>Extras</li>
					<li>IDs</li>
				</ul>
			</div>
			<ol>
				<li>No puedes mostrar arte hecho por otros en ningún lugar del sitio sin <b>su clara <u>petición</u> para poder hacerlo</b>. Además, debes escribir lo siguiente en todos los trabajos que envíes por otros:
					<ol>
						<li>El nombre del artista original</li>
						<li>Información de contacto del artista original (si el artista prefiere anonimato, eres responsable de asegurar que cualquiera que desee contactarlo pueda hacerlo; también debes proveer información de contacto a cualquier moderador si se pide)</li>
						<li>El derecho de autor del artista original</li>
					</ol>
				</li>
				<li>Calcas o copias a mano del trabajo de otros (como manga o fotos de anime) o arte usando porciones del trabajo de otros artistas (como gráficas de videojuegos, arte oficial y <i>doll makers</i>) sin el permiso del autor original, no están permitidas en el sitio. Debes escribir lo siguiente en todos los trabajos de los cuales obtuviste permiso para usar dicho material:
					<ol>
						<li>El nombre del artista original</li>
						<li>Información de contacto del artista original (si el artista prefiere anonimato, eres responsable de asegurar que cualquiera que desee contactarlo pueda hacerlo; también debes proveer información de contacto a cualquier moderador si se pide)</li>
						<li>El derecho de autor del artista original</li>
						<li>Un enlace al arte original</li>
					</ol>
				</li>
				<li>Arte que sea fuertemente influenciado por el trabajo de otro artista pero no calcado/copiado debe citar al artista original y, si es posible, proveer de un enlace a la influencia original.</li>
				<li>No debes proclamar ser el creador original de un trabajo que no es tuyo, aunque fuese permitido por el artista original para redistribución.</li>
				<li>Arte creado con doll makers y otros generadores similares no está permitido.</li>
			</ol>
		</li>
		<!-- Rule 7 -->
		<li style="clear : both;"><h3>Estándares de la Comunidad</h3>
			<div>
				<h4>Penalidades por Violación</h4>
				<span class="rule">7.</span>
				<ul>
					<li>Primera violación: Advertencia</li>
					<li>Segunda violación: 2 semanas de suspensión</li>
					<li>Tercera violación: Prohibición permanente</li>
				</ul>
			</div>
			No se <a href="http://www.catb.org/~esr/jargon/html/F/flame.html">flamea</a> a otros usuarios.
		</li>
		<!-- Rule 8 -->
		<li style="clear : both;"><h3>Cuentas adicionales</h3>
			<div>
				<h4>Penalidades por Violación</h4>
				<ul>
					<li>Primera violación: New account ban and<br />+3 meses de suspensión a la cuenta original</li>
					<li>Segunda violación: Prohibición permanente y<br />un reporte para tu ISP</li>
				</ul>
			</div>
			No intentes burlar la suspensión/prohibición de cuentas o enviar materiales que estén en contra de los Términos de Uso al crear otras cuentas adicionales.
		</li>
	</ol>
	<h3 style="clear : both; padding-top : 12pt; border-top : 3px double; margin-left : 30pt;">¿Preguntas? Checa la seccion de <a href="/help">Ayuda</a>.</h3>
	<p style="font-weight : bold;">Por favor sigue las reglas mencionadas para hacer de esta una comunidad feliz, cauta y en general amistosa. Nos reservamos el derecho de editar o remover cualquier o todo el trabajo artístico enviado por cualquier miembro en cualquier momento. También nos reservamos el derecho de suspender o prohibir cualquier cuenta de usuario en cualquier momento por cualquier razón que nos sea conveniente.</p>
	<p style="font-weight : bold;">Debido a la gran libertad personal que ofrece y!Gallery a sus artistas (comparado con otros sitios de arte), puedes encontrarte expuesto a arte que talvez no te guste. Si crees que serás incapaz de tener una responsabilidad personal en el caso de que seas expuesto a dichos trabajos artísticos, te pedimos que no te unas a esta comunidad.</p>
	<p><span style="font-weight : bold;">Denegación</span>: y!hosting, sus dueños, y cualquier otra entidad relacionada con y!hosting o y!Gallery no tomarán responsabilidad alguna de los trabajos enviados al sistema de y!Gallery por sus miembros. Mientras que y!hosting y sus agentes intenta remover todos los trabajos ilegales del sitio tan rápido y cuidadosamente como sea posible, siempre hay la posibilidad de que algunos ingresos sean pasados por alto o ignorados por error. El sitio y!Gallery incluye un sistema riguroso y complejo de control de abuso para prevenir el uso indebido del servicio, y esperamos que su dispersión indique un esfuerzo de buena voluntad para eliminar cualquier material ilegal en el sitio de manera objetiva. Este sistema de control de abuso será ejecutado de acuerdo a las estrictas normas especificadas anteriormente. Todo el trabajo mostrado aquí, sea pictórico o literario, es propiedad de sus respectivos dueños y no de y!hosting. Las opiniones dichas en los diarios personales o perfiles del usuario no son un reflejo de las opiniones o puntos de vista de y!hosting o de cualquiera de sus dueños o entidades relacionadas.</p>
	<p>Los dueños y operadores de este sitio web no son los productores primarios o secundarios (términos definidos en 28 C.F.R. 75.1(c)(2)) de cualquier contenido visual en este sitio web. Los operadores de este sitio web se basan en el lenguaje básico de los estatutos y en las decisiones razonadas de la Corte de Súplicas de los Estados Unidos para los Tenth Circuit en Sundance Associates, Inc. v. Reno, 139 F.3d 804, 808 (10º Cir 1998). En este caso tomado, inter alia, las entidades que no tienen parte en &quot;contratar, organizar o en otros casos arreglar la participación&quot; de modelos o actores, están exentos de los requerimientos de almacenaje de 18 U.S.C. 2257.</p>
</div>
