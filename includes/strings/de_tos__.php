﻿<h1>Dieses übersetzte Bezeichnungen des Services ist ÜBERHOLT. Lesen Sie bitte die ENGLISCHEN Bezeichnungen des Services zu korrekter Information.</h1>
<div class="termsOfService">
	<ol>
		<!-- Rule 1 -->
		<li><h3>Geschlechts- und Arten-Definitionen</h3>
			<div>
				<h4>Strafmaßnahmen bei Missbrauch</h4>
				<span class="rule">1. a-f:</span>
				<ul>
					<li>Erster Missbrauch: Verwarnung</li>
					<li>Zweiter Missbrauch: 2 wöchige Sperrung</li>
					<li>Dritter Missbrauch: Permanente Sperrung</li>
				</ul>
			</div>
			<div>
				<h4>Ausnahmen innerhalb der Regeln</h4>
				<span class="rule">1. a, d:</span>
				<ul>
					<li>Avatare</li>
					<li>Thema / „Theme“</li>
					<li>Extras</li>
					<li>IDs</li>
				</ul>
				e:
				<ul>
					<li>Avatare</li>
					<li>Thema / „Theme“</li>
					<li>Miniaturbilder Literarischer Werke</li>
					<li>Extras</li>
					<li>IDs</li>
				</ul>
				b, c, f:
				<ul>
					<li>Extras</li>
				</ul>
			</div>
			<ol>
				<li>Arbeiten, die Frauen in einer bevorzugten oder zentralen Position darstellen, sind in der Hauptgalerie nicht erlaubt.</li>
				<li>Darstellungen von Frauen mit Penis, so genannte Hermaphroditen (<a href="http://de.wikipedia.org/wiki/Futanari">futanari</a>) sind in der Hauptgalerie nicht erlaubt.</li>
				<li>Cross-Dressing wird toleriert, solange die Person keine körperlich typischen Merkmale einer Frau aufweist (zum Beispiel Brüste) oder anderweitig als weiblich angesehen werden kann.</li>
				<li>Arbeiten, auf denen ausschließlich Tiere dargestellt werden, sind in der Hauptgalerie nicht erlaubt. <a href="http://de.wikipedia.org/wiki/Furry">Anthromorphe Darstellungen</a> nicht werden toleriert.</li>
				<li>Arbeiten, die keine Personendarstellungen zeigen (zum Beispiel Landschaften, unbelebte Objekte) sind in der Hauptgalerie nicht erlaubt.</li>
				<li>Außer den Punkten, die oben aufgelistet sind (bitte die Ausnahmen der rechten Tabelle entnehmen) ist die Darstellung von weiblichen Genitalien und Frauen in lasziven Posen in der Hauptgalerie nicht erlaubt.</li>
			</ol>
		</li>
		<!-- Rule 2 -->
		<li style="clear : both;"><h3>Photografisches Material</h3>
			<div>
				<h4>Strafmaßnahmen bei Missbrauch</h4>
				<span class="rule">2. a-c:</span>
				<ul>
					<li>Erster Missbrauch: Verwarnung</li>
					<li>Zweiter Missbrauch: 2 wöchige Sperrung</li>
					<li>Dritter Missbrauch: Permanente Sperrung</li>
				</ul>
				d:
				<ul>
					<li>Erster Missbrauch: Permanente Sperrung</li>
				</ul>
			</div>
			<div>
				<h4>Ausnahmen innerhalb der Regeln</h4>
				<span class="rule">2. a, c:</span>
				<ul>
					<li>Avatare</li>
					<li>Thema / „Theme“</li>
					<li>Miniaturbilder Literarischer Werke</li>
					<li>Extras</li>
					<li>IDs</li>
				</ul>
			</div>
			<ol>
				<li>Fotografisches Material darf nicht der Mittelpunkt und der Hauptbestandteil eines Werkes in der Hauptgalerie sein.</li>
				<li>Screenshots sind nirgendwo auf der Seite erlaubt, es sei denn sie entsprechen mehreren dieser Kriterien:
					<ol>
						<li>Der Screenshot wurde von einer gesamten Arbeit gemacht (z. B. unfertige Arbeiten innerhalb von Photoshop)</li>
						<li>Der Screenshot ist von einem Spiel, das du entwickelt hast (verboten ist somit alles, was du <b>nicht</b> entwickelt hast, z. B. World of Warcraft/Second Life oder ein anderes kommerzielles Spiel)</li>
						<li>Der Screenshot wird innerhalb eines Tutorials genutzt</li>
						<li>Der Screenshot wird als Beleg für einen Kiriban oder ein Gift Art auf dieser Seite verwendet</li>
					</ol>
				</li>
				<li>Photomanipulation ist nicht in der Hauptgalerie erlaubt.</li>
				<li><b>Absolut verboten ist Aktfotografie!</b></li>
			</ol>
		</li>
		<!-- Rule 3 -->
		<li style="clear : both;"><h3>Shota</h3>
			<div>
				<h4>Strafmaßnahmen bei Missbrauch</h4>
				<span class="rule">3. a:</span>
				<ul>
					<li>Erster Missbrauch: Permanente Sperrung,<br />Benachrichtigung des Internet Service Providers<br />und es wird ebenfalls eine strafrechtliche Verfolgung eingeleitet</li>
				</ul>
				b:
				<ul>
					<li>Erster Missbrauch: 2 wöchige Sperrung</li>
					<li>Zweiter Missbrauch: Permanente Sperrung</li>
				</ul>
			</div>
			<ol>
				<li>Minderjährige Personen, (unter 18 Jahre) die in einer sexuellen Handlung dargestellt werden, dürfen nicht von einer photographischen Referenz entnommen werden und keine real existierenden Personen darstellen.</li>
				<li>3D-Illustrationen mit sexuellem Inhalt, in welchem minderjährige Personen dargestellt werden, sind generell nicht auf dieser Seite erlaubt.</li>
			</ol>
		</li>
		<!-- Rule 4 -->
		<li style="clear : both;"><h3>Stichworte und Filter</h3>
			<div>
				<h4>Strafmaßnahmen bei Missbrauch</h4>
				<span class="rule">4. a-b:</span><br />
				Accidental violations:
				<ul>
					<li>Occasional violations: Verwarnung</li>
					<li>Repeated violations: Permanente Sperrung</li>
				</ul>
				Intentional violations:
				<ul>
					<li>Erster Missbrauch: 2 wöchige Sperrung</li>
					<li>Zweiter Missbrauch: Permanente Sperrung</li>
			</ul>
			</div>
			<ol>
				<li>Du musst alle hochgeladenen Arbeiten genau in das Strichwort System einordnen.</li>
				<li>Du musst alle hochgeladenen Werke genau in das Filter-System einordnen.</li>
			</ol>
		</li>
		<!-- Rule 5 -->
		<li style="clear : both;"><h3>Altersbegrenzung</h3>
			<div>
				<h4>Strafmaßnahmen bei Missbrauch</h4>
				<span class="rule">5.</span>
				<ul>
					<li>Erster Missbrauch: Sperrung bis zum 18. Lebensjahr</li>
					<li>Zweiter Missbrauch: Sperrung des neuen Accounts und<br />zusätzliche 3 Monate auf den Originalaccount</li>

					<li>Dritter Missbrauch: Alle Accounts werden gesperrt und<br />eine Benachrichtigung des Internet Service Providers erfolgt</li>
					</ul>
			</div>
			Du mußt 18 Jahre alt sein, um einen Account auf dieser Seite erstellen zu dürfen.
		</li>
		<!-- Rule 6 -->
		<li style="clear : both;"><h3>Plagiat und Bilderdiebstahl</h3>
			<div>
				<h4>Strafmaßnahmen bei Missbrauch</h4>
				<span class="rule">6. a-b:</span>
				<ul>
					<li>Erster Missbrauch: 2 wöchige Sperrung</li>
					<li>Zweiter Missbrauch: Permanente Sperrung &amp;<br />Benachrichtigung des Internet Service Providers</li>
				</ul>
				c:
				<ul>
					<li>Erster Missbrauch: Verwarnung</li>
					<li>Zweiter Missbrauch: 2 wöchige Sperrung</li>
					<li>Dritter Missbrauch: Permanente Sperrung &amp;<br />Benachrichtigung des Internet Service Providers</li>
				</ul>
				d:
				<ul>
					<li>Erster Missbrauch: Permanente Sperrung &amp;<br />Benachrichtigung des Internet Service Providers</li>
				</ul>
			</div>
			<div>
				<h4>Ausnahmen innerhalb der Regeln</h4>
				<span class="rule">6. a:</span>
				<ul>
					<li>Avatare</li>
					<li>Thema / „Theme“</li>
				</ul>
				b:
				<ul>
					<li>Avatare</li>
					<li>Thema / „Theme“</li>
					<li>IDs</li>
				</ul>
				c:
				<ul>
					<li>Avatare</li>
					<li>Thema / „Theme“</li>
					<li>Miniaturbilder Literarischer Werke</li>
					<li>Extras</li>
					<li>IDs</li>
				</ul>
			</div>
			<ol>
				<li>Du darfst nirgendwo auf der Seite von Anderen angefertigte Kunst veröffentlichen, ohne <b>deren ausdrücklichen <u>Wunsch</u></b>, genau dieses zu tun. Außerdem musst du folgendes unter allen Werke schreiben, welche du für andere veröffentlichst:
					<ol>
						<li>Den Namen des Original Künstlers</li>
						<li>Kontaktinformationen bezüglich des Originalkünstlers (wenn der Künstler Anonymität wünscht, bist du dafür verantwortlich, dass jeder, der den Künstler kontaktieren möchte, immer noch befähigt ist, dies zu tun; du musst außerdem immer fähig sein, auf Antrag Kontaktinformationen zu jedem Moderator der Seite zu geben)</li>
						<li>Das Copyright des Original Künstlers</li>
					</ol>
				</li>
				<li>Das Durchpausen (Abzeichnen) oder Freihandkopieren von Werken Anderer (so etwas wie Manga oder Anime Screenshots), oder Werke welche Anteile von Arbeiten anderer Künstler beinhaltet (so etwas wie Spiele Kobolde, Offizielle Bilder, und Doll Maker), sind ohne die Erlaubnis der Original Erschaffer überall auf der Seite verboten. Du musst folgende Quellenbezeichnung auf all deine Arbeiten, für die du die Erlaubnis der Veröffentlichung hast, schreiben:
					<ol>
						<li>Der Name des Original Künstlers</li>
						<li>Kontaktinformationen bezüglich des Originalkünstlers (wenn der Künstler Anonymität wünscht, bist du verantwortlich, dass jeder, der den Künstler kontaktieren möchte, immer noch befähigt ist, dies zu tun; du musst außerdem immer fähig sein auf Antrag Kontaktinformationen zu jedem Moderator der Seite zu geben)</li>
						<li>Das Copyright des Original Künstlers</li>
						<li>Einen Verweislink zur Originalquelle</li>
					</ol>
				</li>
				<li>Illustrationen, die stark von der Arbeit anderer Künstler beeinflusst sind, aber nicht kopiert/abgepaust wurden, müssen den Original Künstler und, wenn möglich, einen Verweislink auf das Original, welches Einfluß genommen hatte, beinhalten.</li>
				<li>Du darfst nicht behaupten, der Original Erfinder einer Arbeit, die nicht deine ist, zu sein, auch wenn dir der Original Künstler diese Verbreitung erlaubt hat.</li>
			</ol>
		</li>
		<!-- Rule 7 -->
		<li style="clear : both;"><h3>Voraussetzungen/Bedingungen innerhalb der Gemeinschaft</h3>
			<div>
				<h4>Strafmaßnahmen bei Missbrauch</h4>
				<span class="rule">7.</span>
				<ul>
					<li>Erster Missbrauch: Verwarnung</li>
					<li>Zweiter Missbrauch: 2 wöchige Sperrung</li>
					<li>Dritter Missbrauch: Permanente Sperrung</li>
				</ul>
			</div>
			Beleidige („<a href="http://de.wikipedia.org/wiki/Flame">flame</a>“) keine anderen Benutzer.
		</li>
		<!-- Rule 8 -->
		<li style="clear : both;"><h3> Zusätzliche Accounts</h3>
			<div>
				<h4>Strafmaßnahmen bei Missbrauch</h4>
				<ul>
					<li>Erster Missbrauch: Sperrung des neuen Accounts und<br />zusätzliche 3 Monate auf den Originalaccount</li>
					<li>Zweiter Missbrauch: Permanente Sperrung &amp;<br />Benachrichtigung des Internet Service Providers</li>
				</ul>
			</div>
			Du darfst keine zusätzlichen Accounts erstellen, um die Sperrung deines Originalaccounts zu umgehen oder um Werke hochzuladen, die die Servicebedingungen verletzen.
		</li>
	</ol>
	<h3 style="clear : both; padding-top : 12pt; border-top : 3px double; margin-left : 30pt;">Questions? See the <a href="/help">Help</a> section.</h3>
	<p style="font-weight : bold;">Please follow the above rules to make this a happy, safe, and overall friendly community. We reserve the right to edit or remove any or all artwork, journal entries, or comments posted by any member at any time. We also reserve the right to suspend or ban any user account at any time for any reason we see fit.</p>
	<p style="font-weight : bold;">Because of the great personal freedom that y!Gallery provides its artists (relative to other art sites), you may find yourself exposed to artwork that you feel uncomfortable with. If you believe that you will be unable to take personal responsibility in the event that you are exposed to such works, we ask that you do not join this community.</p>
	<p><span style="font-weight : bold;">Disclaimer</span>: y!hosting, its owners, agents, and any other entities related to y!hosting or the y!Gallery system take no responsibility for the works posted to the y!Gallery system by its members. While y!hosting and its agents attempt to remove all illegal works from the site as quickly and thoroughly as possible, there is always the possibility that some submissions may be overlooked or dismissed in error. The y!Gallery system includes a rigorous and complex abuse control system in order to prevent improper use of the y!Gallery service, and we hope that its deployment indicates a good-faith effort to eliminate any illegal material on the site in a fair and unbiased manner. This abuse control system is run in accordance with the strict guidelines specified above. All work displayed here, whether pictorial or literary, is the property of its owner and not y!hosting. Opinions stated in personal journals or profiles may not reflect the opinions or views of y!hosting or any of its owners, agents, or related entities.</p>
	<p>The owners and operators of this website are not the primary or secondary producer (as those terms are defined in 28 C.F.R. 75.1(c)(2)) of any of the visual content contained in this web site. The operators of this web site rely on the plain language of the statute and on the well-reasoned decision of the United States Court of Appeals for the Tenth Circuit in Sundance Associates, Inc. v. Reno, 139 F.3d 804, 808 (10th Cir 1998). This case held, inter alia, that entities having no role in the "hiring, contracting for, managing, or otherwise arranging for the participation" of the models or performers, are exempt from the record-keeping requirements of 18 U.S.C. 2257.</p>
</div>
