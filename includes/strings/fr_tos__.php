﻿<h1>Ce des limites de service traduites est PÉRIMÉ. Veuillez lire les limites du service ANGLAISES pour l'information correcte.</h1>
<b>REMARQUE:</b> Cette galerie est une <b>galerie à thème</b>, et contient des images et des textes traitant des relations homosexuelles masculines. Si ce thème vous met mal à l'aise, <b>ne vous inscrivez pas sur le site.</b><br />
<br />
(Dans les règles énoncées ci-dessous, les manquements dits &quot;simultanés&quot; désignent les groupes d'oeuvres soumises  qui ont été envoyées dans un intervalle de moins de 24 heures entre chacune. Les manquements &quot;non-simultanées&quot; désignent les oeuvres ayant été envoyées à un intervalle de plus de 24 heures entre elles. Les oeuvres soumises ne sont pas considérées comme des manquements aux Clauses et Conditions d'utilisation du site tant qu'elles n'ont pas subit l'examen des modérateurs par le biais du système de contrôle des abus.)<br />
<br />
<b>Règle #1a:</b> Cette galerie a été créée dans le but d'apprécier la forme humaine masculine et la sexualité masculine. Les femmes ne doivent être figurées en tant que partie ou centre d'intérêt d'une oeuvre soumise en aucun cas. Si vous soumettez une oeuvre comportant des personnages dont le genre est ambigu parcequ'ils sont jeunes et/ou effeminés, qualifiez l'oeuvre comme telle pour éviter tout retrait accidentel.<br />
<br />
<b>Règle #1b:</b> De fait, envoyer une oeuvre figurant un personnage masculin en personnage féminin constitue une transgression de cette règle, étant donné que l'objectif de ce site est d'apprécier la forme masculine. Pour la même raison, les personnages féminins affublés d'un pénis, désignés sous les termes de &quot;dickgirls&quot; et/ou &quot;futanari&quot; ne sont pas autorisés.<br />
<br />
<b>Règle #1c:</b> Les personnages travestis et hermaphrodites ne sont autorisés que dans la mesure où le personnage ne comporte aucune caractéristique féminine manifeste, comme une poitrine, et où le personnage est identifié comme un personnage masculin. Si les rembourrages au niveau de la poitrine ne sont pas évidents, le personnage sera traîté comme s'il avait une vraie poitrine. Les personnages identifiés comme féminins seront traités comme tels.<br />
<br />
<b>Règle #1d:</b> Les animaux n'ayant pas de traits humains suffisants afin d'être désignés comme &quot;anthropomorphes&quot; ne sont pas autorisés en tant que personnage unique/personnages exclusifs dans une oeuvre.<br />
<br />
De manièrer générale, l'anthropomorphisme (confronté à la forme animale) est défini selon ces critères:
<ol>
	<li>Bipédie</li>
	<li>Pouces opposables</li>
	<li>Expression humaine du visage</li>
	<li>Sourcils visibles</li>
</ol>
Pour être considérés comme anthropomorphes et non comme animaux, les personnages doivent comprendre au moins 3 de ces 4 critères. Tout personnage ne comprenant pas au moins 3 de ces 4 critères peut éventuellement être anthropomorphe, mais ne l'est probablement pas, et ne doit être posté sans l'accord préalable d'un modérateur.<br />
<br />
Règle #1e: Les oeuvres ne contenant aucun personnage (exemples: paysages, natures mortes,...) ne sont pas autorisées.<br />
<br />
-- OUTDATED INFORMATION --<br />
<b>Les manquements à la règle #1 ne sont autorisées que pour les avatars, imagettes d'oeuvres littéraires, thèmes, &quot;Extras&quot;, et profils.</b><br />
La première occurence d'un manquement à l'une des règles #1 sera sanctionnée par un avertissement. Toute récidive, simultanée ou non au premier manquement, sera sanctionnée par une suspension de deux semaines, pénalité à caractère cumulable. <b>2 manquements non-simultanés seront le motif d'un banissement permanent.</b><br />
<br />
<b>Règle #2a:</b> La technique de la photographie ne peut être représentée qu'en tant que partie secondaire d'une oeuvre plus grande, et ne doit pas constituer le centre d'intérêt ou la partie principale d'une oeuvre. (Ainsi, sont tolérés dans le cadre de l'utilisation de photographies les arrière-plans, les références concernant la pose, IDs, et imagettes d'oeuvres littéraires.) Si une photographie est utilisée de la sorte, vous devez mentionner l'oeuvre originale et son auteur. (Voire la règle #6 concernant les attributions.)<br />
<br />
<b>Règle #2b:</b> Les captures d'écran ne sont autorisées que dans la mesure où elles figurent une oeuvre originale (comme un site internet original ou un projet en cours de réalisation sous un logiciel d'édition comme Photoshop), utilisées comme une partie de tutorial, ou comme le reçu d'un Kiriban ou d'un oeuvre-cadeau sur ce site. Les captures d'écran figurant l'écran du Bureau sont strictement interdites. <b>Les captures d'écran provenant de jeux vidéos ne sont pas autorisées, à moins que vous ne soyez développeur de jeu.</b> Gardez à l'esprit que toute capture d'écran, exception faite des reçus de Kiriban, doit respecter le règlement du site.<br />
<br />
Règle #2c: La technique de la photomanipulation est interdite.<br />
<br />
<b>Règle #2d: Absolument AUCUN matériel photographique envoyé sur le site ne saurait être de caractère adulte.</b><br />
<br />
<b>Le manquement aux règles #2a et #2c n'est autorisée que dans le cadre des avatars, imagettes d'oeuvres littéraires, themes, Extras, et IDs.</b><br />
La première occurence d'un manquement à la règle #2 sera sanctionnée par un avertissement. Toute récidive, simultanée ou non à la première transgression, sera sanctionnée par une suspension de deux semaines, sanction à caractère cumulatif. <b>2 manquements non-simultanés seront le motif d'un banissement définitif.</b><br />
<br />
<b>Règle #3a:</b> Aux Etats-Unis, où est situé le Serveur, la représentation de mineurs dans une oeuvre d'art n'est toléré qu'à 2 conditions: la Première, que l'oeuvre de l'artiste soit discernable d'une photographie (par exemple, d'un réalisme photographique ); et la Deuxième, qu'aucun modèle ou véritable enfant ne soit utilisé comme base pour la réalisation de l'oeuvre. Les oeuvres déposées sur ce site doivent être clairement conformes à ces deux conditions.<br />
<br />
<b>La première occurence d'un manquement à la règle #3a sera sanctionnée par un banissement définitif.</b><br />
<br />
<b>Règle #3b:</b> Les illustrations en 3D réalisées avec des logiciels spécialisés dans le rendu de la 3D ne sauraient contenir la représentation d'un modèle qui, paraissant âgé de moins de 18 ans, est engagé dans une activité sexuelle, ou contenir la représentation d'un modèle qui apparaît comme étant âgé de moins de 18 ans dans une attitude ou position lascive.<br />
<br />
La premier manquement ou suite de manquements simultanés à la règle #3a seont sanctionnés par un avertissement. <b>Toute récidive sera sanctionnée par un banissement définitif.</b><br />
<br />
Nous vous prions de bien vouloir prendre connaissance des lois de votre propre pays concernant la représentation des mineurs dans l'art. Si vous vivez au Canada, l'utilisation d'individus mineurs dans une oeuvre de caractère adulte est prohibée, quoique l'utilisation de jeunes garçons dans des oeuvres sans contenu sexuel non voilé soit cependant légalement tolérée.<br />
<br />
<b>Nous menons une politique de <em>tolérance zéro</em> en ce qui concerne les abus à l'encontre d'enfants et en ce qui concerne la pédopornographie.<br />
Si nous découvrons la preuve qu'un enfant réel et non-fictif a été utilisé comme base de votre oeuvre, ou que ce que vous envoyez sur le site sont effectivement des photos d'un enfant réel, non fictif, <em>nous alerterons les autorités compétentes</em>.</b><br />
<br />
<b>Règle #4a:</b>Vous devez caractériser convenablement votre oeuvre par le biais du système des mot-clés. Nous nous réservons le droit de classer à nouveau votre oeuvre si nous jugeons que vous avez utilisé les mauvais mot-clés.<br />
<br />
<b>Règle #4b:</b> Vous devez poser les bons filtres sur votre oeuvre, par le biais du système des filtres. Nous nous réservons le droit de classer à nouveau votre oeuvre si nous avons le sentiment que vous avez utilisé les mauvais mots clés.<br />
<br />
Tout manquement aux Règles #4a et #4b sera sanctionné par un avertissement. <b>Le mauvais emploi des systèmes des mots clés ou des filtres, s'il est manifeste ou délibéré, pourra être sanctionné par une suspension ou un banissement permanent.</b><br />
<br />
<b>Règle #5:</b> Etant donné que ce site est hébergé par un serveur localisé aux Etats-Unis, et en raison du caractère adulte de la plupart des oeuvres hébergées ici, réservées à un public averti, vous devez avoir 18 ans ou plus afin de vous inscrire, d'envoyer ou de visualiser une oeuvre sur ce site. Falsifier son âge dans le but de s'inscrire est strictement interdit.<br />
<br />
<b>Nous menons une politique de <em>tolérance zéro</em> en ce qui concerne les inscriptions fallacieuses.</b><br />
Les manquements à la Règle #5 seront sanctionnés par un banissement jusqu'à la date de votre 18ème anniversaire. Si nous sommes incapables de déterminer votre date de naissance avec précision, la date à laquelle votre compte cessera d'être suspendu sera fixé au dernier jour de l'année durant laquelle vous devriez atteindre vos 18 ans.<br />
<br />
<b>Règle #6a:</b> Vous ne devez pas envoyer la copie d'une oeuvre que vous n'avez pas créée et dont vous n'avez pas obtenu les droits de redistribution auprès de l'auteur. Une mention de copyright précisant que vous avez reçu l'autorisation de publier/redistribuer l'oeuvre doit être inclue accompagnant chaque oeuvre que vous envoyez en tant que redistributeur, avec le copyright de l'auteur original. Une preuve écrite de cette autorisation, de même que des références permettant de contacter l'artiste original, doivent être immédiatement disponibles sur la demande du personnel du site. Les oeuvres envoyées pourront transiter par le système de répression des abus dans l'attente de ces informations. Ces informations doivent être fournies dans le mois qui suit la demande ou l'oeuvre envoyée sera retirée en tant que manquement à cette règle.<br />
<br />
Le premier manquement ou suite de manquements simultanés à la Règle #6a seront sanctionnés par une suspension de deux semaines. <b>Toute récidive sera sanctionnée par un banissement définitif.</b><br />
<br />
<b>Règle #6b:</b> Vous ne devez pas envoyer d'oeuvre ayant été décalquée ou inversée à partir d'une autre source, ou plagier une autre source littéraire. Cela veut dire que vous ne pouvez utiliser les oeuvres d'autres artistes, ceci incluant les graphismes d'un jeu, l'art officiel,ou les doll makers, comme parties principales d'une oeuvre envoyée sans la permission de l'auteur original.<br />
<br />
Le premier manquement ou suite de manquements simultanés à la Règle #6b seront sanctionnés par une suspension de deux semaines. <b>Toute récidive sera sanctionnée par un banissement définitif.</b><br />
<br />
<b>Règle #6c:</b> Si vous produisez une oeuvre illustrée ou imagée qui est largement inspirée d'une oeuvre produite par un autre artiste, vous devez le citer comme l'artiste original et, si possible, fournir un lien approprié à cette source d'influence.<br />
<br />
La première occuence d'un manquement à la règle #6c sera sanctionnée par un avertissement, et l'oeuvre sera effacée et devra être envoyée à nouveau avec une bonne attribution. Toute récidive suivant ce cas, simultanée à la première occurence ou non, sera sanctionnée par une suspension de 2 semaines, sanction à caractère cumulable, et l'oeuvre sera effacée et devra être envoyée à nouveau avec la bonne attribution. <b>2 ou plus de deux manquements non-simultanés seront le motif d'un banissement permanent.</b><br />
<br />
<b>Les manquements aux règles #6a et #6c ne sont tolérées que pour les avatars et les thèmes. Les transgressions de la règle #6b ne sont tolérées que pour les IDs, avatars, thèmes, imagettes d'oeuvres littéraires.</b><br />
<br />
<b>Règle #6d:</b> Vous ne devez pas affirmer que vous êtes le créateur original d'une oeuvre qui n'est pas la vôtre, cela même si vous avez les droits de redistribution.<br />
<br />
<b>La première occurence d'une transgression de la Règle #6d sera sanctionnée par un banissement immédiat et permanent.</b><br />
<br />
<b>Nous menons une politique de <em>tolérance zéro</em> en ce qui concerne le vol d'oeuvres.<br />
Toute transgression sanctionnée par un banissement sera également suivie d'un rapport qui sera communiqué à votre fournisseur d'accès Internet local dénonçant la violation d'un copyright.</b><br />
<br />
<hr />
<br />
ADDENDUM: <b>REGLES CONCERNANT LE VOL D'OEUVRE/LA COPIE</b><br />
<br />
(&quot;Publié&quot; définit une oeuvre uniquement offerte gratuitement au public, créée spécifiquement en vue d'une consomation par un grand nombre de personnes, ou qui est déclarée comme publiée par l'artiste. &quot;Derivée&quot; définit une oeuvre qui a été conçue à partir d'une illustration, photographie ou film préexistant.)<br />
<br />
Les types d'oeuvres dérivées qui peuvent être envoyées sur le site dépendent de ces règles.<br />
<br />
Une oeuvre NE DOIT PAS être décalquée ou peinte par-dessus une oeuvre d'un autre artiste. Les décalques seront immédiatement effacées et celui qui les aura envoyé réprimandé comme décrit ci-dessus.<br />
<br />
L'auteur d'une oeuvre dérivée d'une oeuvre non-publiée/&quot;non-officielle&quot; doit obtenir la permission de la part du créateur des dites séries and doit être capable de fournir cette permission à n'importe quel moment. En plus de cela,l'artiste original et la série (si applicable) doivent être mentionés, accompagnés d'un lien vers le site internet de l'artiste original, s'il en existe un.<br />
<br />
Une oeuvre dérivée d'une oeuvre publiée/&quot;officielle&quot; doit mentionner l'artiste original et les séries, et doit éventuellement fournir un lien internet vers le site Web officiel.<br />
<br />
Nous encourageons les artistes qui utilisent les personnages de séries aussi bien publiées que non publiées à obtenir la permission expresse de l'artiste, bien que nous soyons conscient du fait que cela n'est pas toujours possible. Dans le cas où celui qui envoie l'oeuvre ne reçoit pas la permission de l'artiste,et que l'artiste ou l'entité légale représentant l'artiste envoie une plainte contre l'oeuvre envoyée, l'oeuvre sera retirée du site si les deux parties ne peuvent trouver de solution satisfaisante.<br />
<br />
<hr />
<br />
<b>Règle #7:</b> Si nous respectons tout à fait votre liberté d'expression, et ne souhaitons par inhiber votre capacité à parler ouvertement de quelconque manière, vous devez faire montre d'une certaine courtoisie envers les autres membres du site. Vous ne devez pas &quot;flamer&quot;. &quot;Flamer&quot; est définit comme le fait de poster un message destiné à insulter ou provoquer les autres, en particulier quand il est dirigé contre une personne ou un groupe de personnes précis (Source: <a href="http://www.catb.org/~esr/jargon/html/F/flame.html" target="_blank">Jargon File</a>).<br />
<br />
Traitez les autres membres avec respect et ne cherchez que les oeuvres dont vous êtes vous-même amateur. Rappelez-vous que tout ce qui est écrit dans le journal personnel d'un membre est son opinion et  qu'il a le droit de la soutenir. Les journaux personnels et commentaires peuvent être édités ou supprimés dans le cas où des commentaires particulièrement vicieux ou calomnieux ou ou dans le cas où des propos diffamatoires sont exprimés à l'encontre d'une personne ou d'un groupe de personnes. Les journaux et commentaires peuvent également être édités ou supprimés s'ils contiennent manifestement des informations ou des demandes d'informations fausses ou illégales.<br />
<br />
La première occurence d'un manquement à la Règle #7 sera sanctionné par un avertissement. Toute récidive, simultanée à la première ou non, sera sanctionnée par deux semaines de suspension cumulables. <b>3 récidives simultanées ou plus seront le motif d'un banissement permanent.</b><br />
<br />
<b>Règle #8:</b> Vous ne devez pas créer de comptes supplémentaires sur le site dans le but de contourner les suspensions/banissements, ou pour poster des éléments en violation des Clauses et Conditions d'utilisation.<br />
<br />
<b>Les manquements à la Règle #8 seront sanctionnés par un banissement permanent et immédiat sur tous les comptes supplémentaires</b>, et également d'un mois de suspension (ou d'une extension de trois mois à une suspension déjà fixée) sur le compte original. Si plus d'un compte est créé et est utilisé afin de transgresser le règlement du site, un rapport sera fait à votre fournisseur d'accès à Internet, <b>et le compte original sera également banni.</b><br >
<br />
<h1>Des informations supplémentaires se trouvent dans la section <a href="/help" target="_blank">Aide</a> du site, si vous désirez les lire.</h1>
<br />
<b>Nous vous prions de bien vouloir respecter les règles énoncées ci-dessus pour faire de cette communauté une communauté heureuse, sûre, et globalement amicale. Nous nous réservons le droit d'éditer ou de supprimer certains ou tous les travaux artistiques, articles de journal, ou commentaires postés par tout membre à tout moment. Nous nous réservons également le droit de suspendre ou de bannir tout compte d'utilisateur à tout moment pour toute raison qui nous semble justifier cette mesure.</b><br />
<br />
<b>Etant donnée la grande liberté d'expression que y!Gallery confère aux artistes (si 'lon considère d'autres sites-galeries), vous vous trouverez vous-mêmes confrontés à des oeuvres face auxquelles vous pourrez vous sentir mal à l'aise. Si vous pensez ne pas être capable de prendre personnellement vos responsabilités au cas où vous vous trouveriez exposés à de telles oeuvres, nous vous demandons de ne pas joindre la communauté.</b><br />
<br />
<b>Dégagement de responsabilité:</b> y!hosting, ses propriétaires, ses agents, and toutes les autres entitées en relation avec y!hosting ou avec le système y!Gallery ne prennent pas la responsabilité des oeuvres qui sont postées sur le système y!Gallery par ses membres. Si y!hosting et ses agents essayent de retirer toutes les oeuvres illégales du site aussi de manière aussi rapide et systématique que possible, la possibilité que l'un ou l'autre des envois échappe à ce contrôle ou soit retiré par erreur. Le système y!Gallery comporte un système de contrôle des abus rigoureux et complexe dans le but d'empêcher tout usage impropre du service  y!Gallery, et nous espérons que son déploiement dénote un effort de bonne volonté pour éliminer tout propos illégal sur le site de manière équitable et impartiale. Ce système de contrôle des abus est géré selon les directive spécifiées ci-dessus. Toute oeuvre présentée ici, qu'elle soit picturale ou littéraire, est la propriété de celui qui en détient les droits et non de y!hosting. Les opinions énoncées dans les journaux personnels ou dans les profils peuvent ne pas refléter les opinions ou points de vue de y!hosting ou de l'un de ses propriétaires, agents ou entitées en relation.<br />
<br />
Les propriétaires et les opérateurs de ce site Web ne sont pas les principaux producteurs (comme ce terme est défini dans 28 C.F.R. 75.1(c)(2)) du contenu visuel du site Web. Les opérateurs de ce site web se fondent sur le langage juridique clair de la loi et sur la décision raisonnable rendue par la Cour d'Appel des Etats-Unis du Dixième Circuit dans l'affaire &quot;Sundance Associates&quot; contre Reno, 139 F.3d 804, 808 (10th Cir 1998)). Ceci étant établi, inter alia, que les entités qui ne jouent aucun rôle dans &quot;l'embauche, l'établissement d'un contrat, l'administration ou tout autre arrangement pour la participation&quot; des modèles ou des interprètes, sont dispensées de conserver les archives comme requis par la loi 18 U.S.C. 2257.
