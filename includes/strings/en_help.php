<h2>General questions</h2>
<ul>
	<li><a href="#general">How does this all work?</a></li>
	<li><a href="#keywords">Why don't you allow users to add keywords directly?</a></li>
	<li><a href="#friends">How do I add friends?</a></li>
	<li><a href="#submitForClub">What are clubs and projects, and how do I create them/submit art to them?</a></li>
	<li><a href="#changeWorks">How do I change/remove a submission?</a></li>
	<li><a href="#themedesigner">How do I create a custom theme for my user page?</a></li>
	<li><a href="#extras">What are &quot;Extras&quot;, and how do they work?</a></li>
	<li><a href="#folders">What are folders, and how do they work?</a></li>
	<li><a href="#collabs">What are Collabs, and how do they work?</a></li>
	<li><a href="#gifts">What are Gifts, and how do they work?</a></li>
	<li><a href="#newQuestion">I have another question not covered here.</a></li>
</ul>
<h2>Technical problems</h2>
<ul>
	<li><a href="#preview">Why doesn't the image preview work?</a></li>
	<li><a href="#avatars">What are the limitations for avatars?</a></li>
	<li><a href="#forbiddenImages">Why do I get red &quot;warning&quot; images all over the site?</a></li>
	<li><a href="#forbiddenFlash">Why do I get a &quot;warning&quot; images instead of what I asked for, but only when trying to view Flash animations?</a></li>
	<li><a href="#logouts">Why do I keep getting logged out of the site?</a></li>
	<li><a href="#nis">When I try to log in, I get a white page with a &quot;0&quot; in the corner.</a></li>
</ul>
<h2>Filters</h2>
<ul>
	<li><a href="#nudity">What classifies an image as containing &quot;nudity&quot;?</a></li>
	<li><a href="#adult">What classifies an image or story as containing &quot;adult imagery&quot;?</a></li>
	<li><a href="#violent">What classifies an image or story as containing &quot;violent imagery&quot;?</a></li>
	<li><a href="#disturbing">What classifies an image or story as containing &quot;disturbing imagery&quot;?</a></li>
	<li><a href="#shota">What classifies an image or story as containing &quot;shota&quot;?</a></li>
</ul>
<h2>Policy questions</h2>
<ul>
	<li>(Rule #1) <a href="#r1mxm">Does a male x male relationship have to the be central focus of the work for it to qualify, or is a central male character enough?</a></li>
	<li>(Rule #1) <a href="#r1fem">Is a work not allowed because it has female characters?</a></li>
	<li>(Rule #1) <a href="#r1maj">Is it simply necessary for the work to have a majority of male characters, or do the males have to be the major focus and the females unimportant?</a></li>
	<li>(Other) <a href="#roadd">Is it alright to post work that may not be in agreement with all of the rules of the Terms of Service as long as it is part of a larger whole?</a></li>
</ul>
<h2>Other problems</h2>
<ul>
	<li><a href="#reportedWork">One of my works has been reported to the abuse system. What do I do?</a></li>
	<li><a href="#updates">Some of my updates are missing!</a></li>
</ul>

<hr />

<a name="general"></a>
<h1>How does this all work?</h1>
Unlike a typical art site where things are ordered only into broad categories, y!Gallery allows submissions to be very finely classified.
Choose as many of the keywords as are applicable to the artwork you are submitting. You must choose at least one keyword from each category.
If you find that our database is missing a keyword that is particularly relevant to the art you are submitting, please post to
<a href="/comment/255">this thread</a>, following the instructions there.
<br /><br />

<a name="keywords"></a>
<h1>Why don't you allow users to add keywords directly?</h1>
In order to prevent redundant keywords, and in order to reduce abuse of the keywording system, all keywords must be approved by a supermoderator
or administrator. We realise that this may add some delay to proper classification of your art, but we feel that in order to keep the system
running smoothly, we need to ensure some level of quality control over the keywords entered into the system.
<br /><br />

<a name="friends"></a>
<h1>How do I add friends?</h1>
You cannot add friends. Friends are people whom you watch, and who watch you in return.
<br /><br />

<a name="submitForClub"></a>
<h1>What are clubs and projects, and how do I create them/submit art to them?</h1>
The club/projects option allows you to post your submission to a specific club or project that you are a member of. You can start a new club/project
at the Settings page (the Special tab). This way you can, for example, create a comic/manga project and then start submitting the pages there. Those
submissions will not appear in your main gallery, but will be stored inside the club/project's gallery instead.
<br /><br />

<a name="changeWorks"></a>
<h1>How do I change/remove a submission?</h1>
On every submission view page, there is a section called &quot;Edit Submission&quot; which contains links to modify or remove the submission.
<br /><br />

<a name="themedesigner"></a>
<h1>How do I create a custom theme for my user page?</h1>
<ul>
	<li>You must have at least 1 submission in order to be able to use the <a href="/themedesigner">Theme Designer</a>.</li>
	<li>You must upload all four images at the same time in order to initialise your theme. (You do not have to upload all four images
		for subsequent changes.)</li>
	<li>Further instructions are available temporarily <a href="/journal/sweetyt/8699">here</a>.</li>
</ul>
<br />

<a name="extras"></a>
<h1>What is &quot;Extras&quot;, and how does it work?</h1>
&quot;Extras&quot; is a special feature of the gallery that allows you to link to submissions stored off-site that don't follow certain
rules of the <a href="/tos">Terms of Service</a> (Rules #1a-1e, 2a, and 2c). In order to use Extras, you must have at least 1 submission
in the main gallery. You can upload 1 Extra for every submission you have in your main gallery. Extras requires 3 different images to be
created: a thumbnail, a low-quality full image, and the original image. You must store these images on your own servers, or on a free
image hosting provider. If you are hosting on a free image hosting provider, please ensure that what you are uploading is not in
violation of their Terms of Service.<br />
<br />
Extras can be hidden from your view by going to your <a href="/settings">Settings</a> page and checking
&quot;Do not show Extras&quot;.<br />
<br />
When Extras are submitted to the site, they are validated to ensure they are valid images and fall within the size requirements.
If you want, you can use the same file for the low-quality full image and original image if it is small enough.<br />
<br />
If you are having trouble making your thumbnails a proper size in Photoshop, please use the Save For Web option. If you are still
having trouble, try using <a href="http://www.irfanview.com">IrfanView</a>.
<br /><br />

<a name="folders"></a>
<h1>What are folders, and how do they work?</h1>
Folders can be used to sort your gallery without the need to create clubs or projects to hold your submissions.<br />
Create, modify, and delete folders by going to the <a href="/settings/folders/">Folders tab</a> in Settings.<br />
Existing submissions can be moved into folders from the Edit Submission settings that appear on your own works.<br />
When a folder is deleted, all submissions that were in that folder will appear back in your main gallery.<br />
There is no limit to the number of folders that you can have. Empty folders will not appear in your gallery.
<br /><br />

<a name="collabs"></a>
<h1>What are Collabs, and how do they work?</h1>
Collabs can be used to allow collaborative works to show up in two users' galleries.<br />
When submitting a new work, under the header &quot;This submission is a collaborative work with&quot;, enter the
username of the other person that you want to have linked to the submission. The person that submits the work should
be the primary artist &endash; this person will have the rights to edit the submission, and their custom theme will
be the one used when viewing the submission.<br />
Once submitted, the second user will be asked to confirm or refuse that the submission should be linked to them
(on that user's Updates page).
Once confirmed, their watchers will be notified of the submission, and both the primary and secondary artists will
have comments sent to their Updates.<br />
Once the secondary artist has confirmed the work, the secondary artist can only be changed by a supermoderator.
<br /><br />

<a name="gifts"></a>
<h1>What are Gifts, and how do they work?</h1>
Gifts can be used to allow the work made for a specific user (e.g. a gift, an art-trade or a commission)
to show up in that user's &quot;From Others&quot; gallery.<br />
When submitting a new work, under the header &quot;This submission was made for&quot;, enter the
username of the other person that you want to have linked to the submission.<br />
<br /><br />

<a name="newQuestion"></a>
<h1>I have another question not covered here.</h1>
If you have a question that is not covered here, please send a message to a moderator, supermoderator, or administrator. You can also
post questions to the <a href="/club/1">Gallery Star Runners</a> club. If it is a question that
is relevant to everyone, we will add it to this list.
<br /><br />

<hr />

<a name="preview"></a>
<h1>Why doesn't the image preview work?</h1>
Security restrictions in many Web browsers prevent local files from being loaded by remote sites.<br />
<br />
<b>To bypass this restriction in Mozilla Seamonkey prior to 1.8/Mozilla Firefox prior to version 1.5</b>:
<ol>
	<li>Type &quot;<b>about:config</b>&quot; into your Location bar.</li>
	<li>On the list that comes up, double-click on <b>security.checkloaduri</b>. This will disable the local zone check and allow local files to be loaded by remote sites.</li>
</ol>
Please note that this does slightly reduce the security of your browser.<br />
<br />
<b>To bypass this restriction in Mozilla Seamonkey 1.8 or later/Mozilla Firefox 1.5 or later</b>:
<ol>
	<li>Type &quot;<b>about:config</b>&quot; into your Location bar.</li>
	<li>Right-click anywhere on the list that appears and choose &quot;<b>New -&gt; String</b>&quot;.</li>
	<li>Enter &quot;<b>capability.policy.policynames</b>&quot; in the dialogue box that appears.</li>
	<li>Enter &quot;<b>yGalPreview</b>&quot; in the next dialogue box.</li>
	<li>Right-click anywhere on the list again and choose &quot;<b>New -&gt; String</b>&quot;.</li>
	<li>Enter &quot;<b>capability.policy.yGalPreview.sites</b>&quot; in the dialogue box that appears.</li>
	<li>Enter &quot;<b>http://fukuniji.y-gallery.net http://y-gallery.net http://gallery.y-hosting.net</b>&quot; in the next dialogue box.</li>
	<li>Right-click anywhere on the list again and choose &quot;<b>New -&gt; String</b>&quot;.</li>
	<li>Enter &quot;<b>capability.policy.yGalPreview.checkloaduri.enabled</b>&quot; in the dialogue box that appears.</li>
	<li>Enter &quot;<b>allAccess</b>&quot; in the next dialogue box.</li>
</ol>
Please note that these new settings will not appear in the about:config list.<br />
Please also note that this does not reduce the security of your browser, as the policies are now per-site and not global.
<br /><br />

<a name="avatars"></a>
<h1>What are the limitations for avatars?</h1>
Avatars must be between 30x30 and 80x80 pixels in dimensions, with a maximum filesize of 32kB.
<br /><br />

<a name="forbiddenImages"></a>
<h1>Why do I get red &quot;warning&quot; images all over the site?</h1>
If you have software on your computer that is filtering the HTTP &quot;referer&quot; header, you will experience problems viewing the site.<br />
<br />
Many consumer software firewalls, such as Norton Personal Firewall (which is also part of Norton Internet Security), ZoneAlarm, and McAfee, remove
the referrer from HTTP requests which will prevent you from viewing images or downloading files on any site that does a referrer check. Therefore,
such options should always be turned <b>off</b>.<br />
<br />
<b>For Symantec and Norton Firewalls</b>: Go to the Privacy Settings and disable &quot;Privacy Control&quot;. (Please note that &quot;Privacy
Control&quot; does not protect your privacy in any way, as your IP address will always be logged regardless of that setting).<br />
<b>For ZoneAlarm</b>: Go to Privacy -> Cookie Control -> Custom and uncheck &quot;remove private header information&quot;.<br />
<b>For McAfee Firewall</b>: Go to Privacy Service -> Protect my Identity -> Erase Revealing Information and uncheck &quot;Prevent Referer Information
to be Shared&quot;.<br />
<br />
If you use a Gecko-based browser, such as Mozilla Suite, Firefox, or Netscape, referrer sending can also be disabled (but by default, it is enabled).
To change that setting, browse to &quot;about:config&quot;, then search for &quot;network.http.sendRefererHeader&quot;. If the value is set to 1 or
2, referrer sending is enabled. If it is set to 0, it is disabled. To enable it, double-click on the setting, then change the value. The default is
<b>2</b>.<br />
<br />
If you use Opera, referrer sending can also be disabled. To change that setting, press F12 and check &quot;Enable referrer logging&quot;.<br />
<br />
Software like AdSubstract also blocks the referrer and makes image viewing impossible.<br />
<br />
We're sorry for any inconvenience this may cause and hope you understand our reasons for doing so. Bandwith doesn't come cheap and we have to do
all we can to keep the bandwith usage down. This also protects our site from any liability problems that could be caused from bypassing our
registration system to view works.
<br /><br />

<a name="forbiddenFlash"></a>
<h1>Why do I get a &quot;warning&quot; images instead of what I asked for, but only when trying to view Flash animations?</h1>
Internet Explorer incorrectly does not send the HTTP &quot;referer&quot; header when loading a Flash animation. The only option to view
animations is to use an alternate browser that correctly follows the RFC for HTTP, such as Firefox or Opera. More information is available
<a href="/noie">here</a>.
<br /><br />

<a name="logouts"></a>
<h1>Why do I keep getting logged out of the site?</h1>
The first reason may be that your session is timing out. If you do not check &quot;Remember me&quot; on the login window, your session will
automatically be terminated after 300 seconds of inactivity.<br />
<br />
The second reason may be due to a malicious user or your ISP triggering our security system.
In order to ensure your account remains secure, we check to make sure that your IP address always matches the one that was recorded when your session
was started. If a different IP address attempts to access the site using the same session ID, the session is automatically terminated in order to prevent
a malicious person from stealing your credentials and using your account without your permission. &quot;Internet accelerator&quot; software, and several
ISPs (most notably AOL), silently and without your consent re-route all of your Web traffic through their own servers, instead of giving you direct
access to the Web. Unfortunately, the traffic is often passed between several servers with different IP addresses during a single session, which
causes the IP address on our records to no longer match your IP address and causes our security system to terminate your session.<br />
<br />
<b>Web accelerator users:</b> Disable your Web accelerator, or exclude this site from being accelerated in its Preferences.<br />
<br />
<b>AOL users:</b> Using an alternate Web browser, such as <a href="http://www.getfirefox.com">Firefox</a> or <a href="http://www.opera.com">Opera</a>
will circumvent AOL's proxy servers. It is highly recommended that you never use the AOL browser due to its security holes. (The Web browser built into
AOL is Internet Explorer.)<br />
<br />
<b>Other ISPs:</b> Using an alternate Web browser may help. Disabling automatic proxy detection in your Web browser may also help. Otherwise, contact
your ISP and ask them to stop redirecting your Web traffic.
<br /><br />

<a name="nis"></a>
<h1>When I try to log in, I get a white page with a &quot;0&quot; in the corner.</h1>
This is because you have installed a copy of Norton Internet Security, some of the world's worst security software.<br />
We recommend prompt removal of this software. Your computer will run <em>much</em> faster and you can install
<a href="http://free.grisoft.com">AVG Free</a>, a free antivirus package which will provide much <em>better</em> protection for your computer.<br />
Barring the removal of their terrible security software, we have been told that temporarily disabling the entire program
will allow you to log into the site. However, be aware that doing this will leave your computer vulnerable to malicious activity.
<br /><br />

<hr />

<a name="nudity"></a>
<h1>What classifies an image as containing &quot;nudity&quot;?</h1>
In order to classify as requiring the &quot;nudity&quot; flag, an image must contain full frontal nudity of a non-sexual manner.
<br /><br />

<a name="adult"></a>
<h1>What classifies an image or story as containing &quot;adult imagery&quot;?</h1>
In order to classify as requiring the &quot;adult imagery&quot; flag, an image must show full frontal nudity of a sexual manner or
imply or display sexual activity. Kissing, cuddling, hugging, embracing, and other non-sexual acts of affection do not require the
&quot;adult imagery&quot; flag.
<br /><br />

<a name="violent"></a>
<h1>What classifies an image or story as containing &quot;violent imagery&quot;?</h1>
In order to classify as requiring the &quot;violent imagery&quot; flag, an image must depict gore, mutilation, or violence toward a person or
animal (including self-mutilation). Illicit (needles) or excessive drug use should also be marked as &quot;violent imagery&quot;. Blatantly
non-consensual sex acts should also have the &quot;violent imagery&quot; flag.
<br /><br />

<a name="disturbing"></a>
<h1>What classifies an image or story as containing &quot;disturbing imagery&quot;?</h1>
In order to classify as requiring the &quot;disturbing imagery&quot; flag, an image must depict urination, defecation, vomitting,
male pregnancy, &quot;vore&quot;, fisting, bestiality, or heavy BDSM.
<br /><br />

<a name="shota"></a>
<h1>What classifies an image or story as containing &quot;shota&quot;?</h1>
In order to classify as requiring the &quot;shota&quot; flag, an image or story must depict a character that is under 16 engaging
in some form of adult sexual activity.
<br /><br />

<!-- <a name="anthropomorphic"></a>
<h1>What classifies an image or story as being &quot;anthropomorphic&quot;?</h1>
In order to classify as requiring the &quot;anthropomorphic&quot; flag, an image or story must feature a character that is
an animal with added human features. This is in contrast to a human with added animal features or mythical creatures, such as elves,
catbois, etc.
<br /><br /> -->

<hr />

<a name="r1mxm"></a>
<h1>Does a male x male relationship have to the be central focus of the work for it to qualify, or is a male central character enough?</h1>
Just a male character is enough. There does not need to be a sexual focus to art for it to be acceptible here.
<br /><br />

<a name="r1fem"></a>
<h1>Is a work not allowed because it has female characters?</h1>
As long as they are not the principal characters, female characters in art is OK.
<br /><br />

<a name="r1maj"></a>
<h1>Is it simply necessary for the work to have a majority of male characters, or do the males have to be the major focus and the females unimportant?</h1>
No, the women must not have a prominent role in the work. As an example, there was a submission that had a single woman as the focal point and
she was surrounded by men. This was ruled as being against the Terms of Service because, while there were more men, the woman was clearly the
principal character.
<br /><br />

<a name="roadd"></a>
<h1>Is it alright to post work that may not be in agreement with all of the rules of the Terms of Service as long as it is part of a larger
whole?</h1>
This OK in certain circumstances. If you have, for example, one page of a 10-page comic or an addendum for a story that contains a woman as a
prominent character, it's nearly always fine.
There are exceptions to this -- stolen/traced/photographic art or women in explicit sexual scenes are never allowed regardless, but most of the
smaller things are OK, as long as they are relevant to the overall story.
<br /><br />

<hr />

<a name="reportedWork"></a>
<h1>One of my works has been reported to the abuse system. What do I do?</h1>
First, <b>do not</b> resubmit the work. If the work is truly a breech of the Terms of Service of the site, submitting it again will earn
you two ToS violations, and may result in a suspension/ban of your account. Information will be provided to you on why the submission was
removed if it is decided that it is against the site's Terms of Service after moderation is completed.<br />
If your work is not cleared from the abuse system within 24 hours, congratulations, we're either extremely busy or you've stumped us.
Most cut-and-dry cases are resolved quickly, but more complicated cases may take longer. Please do not contact us until the moderation is
completed; we will contact you if we require more information.
<br /><br />

<a name="updates"></a>
<h1>Some of my updates are missing!</h1>
Updates older than 20 days are purged from the system to prevent the updates table from being overloaded.
<br /><br />
