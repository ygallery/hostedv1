<?

if($_SERVER['HTTP_HOST'] != 'ygv1.app')
{
	error_reporting(E_ALL);
	set_error_handler( "error_handler" );
}

// -----------------------------------------------------------------------------

function fatal_error( $message )
{
	trigger_error( $message, E_USER_ERROR );
}

// -----------------------------------------------------------------------------

function warning( $message )
{
	trigger_error( $message, E_USER_WARNING );
}

// -----------------------------------------------------------------------------

function notice( $message )
{
	?><div class="error"><?= $message ?></div><?
}

// -----------------------------------------------------------------------------

function error_handler( $errno = "?", $errstr = "?", $errfile = "?", $errline = "?", $vars = array() )
{
	global $_auth, $_config;

	if($errno === 8192)
	{
		return;
	}

	if( $errstr == "var: Deprecated. Please use the public/private/protected modifiers" )
		return; // ignore PHP5 warning about using "var" inside classes

	if( $errno == E_USER_NOTICE )
	{
		?><div class="error"><?= $errstr ?></div><?
		return;
	}

	?><div class="error">Error.</div><?

	$str =
		'<div style="padding: 8px">'.
		'<div style="font: 12px Verdana; border: 1px solid #d77; cursor: default; '.
			'background: #ddd; color: #000; line-height: 1.6" '.
			'onclick="var el = this.firstChild.nextSibling; el.style.display = '.
				'el.style.display == \'none\' ? \'block\' : \'none\';">'.
		'<div style="background: #fdd; color: #f00; cursor: pointer; padding: 4px 8px;">'.
			'<b>'.date( "M j, Y, g:ia" ).'</b>. '.
			$errstr.
		'</div>'.
		'<div style="display: none">'.
		'<div style="background: #eee; padding: 4px 8px;">'.
			'<div><b>User</b>: '.$_auth[ "useUsername" ].' (#'.$_auth[ "useid" ].')</div>'.
			'<div><b>Request</b>: '.
				( isset( $_SERVER[ "REQUEST_URI" ]) ? $_SERVER[ "REQUEST_URI" ] : "???" ).
			'</div>'.
			'<div><b>Host</b>: '.
				( isset( $_SERVER[ "HTTP_HOST" ]) ? $_SERVER[ "HTTP_HOST" ] : "???" ).
			'</div>'.
			'<div><b>Software</b>: '.
				( isset( $_SERVER[ "HTTP_USER_AGENT" ]) ? $_SERVER[ "HTTP_USER_AGENT" ] : "???" ).
			'</div>'.
		'</div>'.
		'<div style="padding: 4px 8px;">';

	//echo "<!-- $str -->";

	foreach( array_reverse( debug_backtrace() ) as $bt )
	{
		$str .= '<div>';

		if( isset( $bt[ "file" ]))
		{
			$str .= "<b>".basename( $bt[ "file" ])."</b>";
		}

		if( isset( $bt[ "line" ]))
		{
			$str .= ":".$bt[ "line" ];
		}

		if( isset( $bt[ "function" ]))
		{
			if( in_array( $bt[ "function" ], array( "error_handler", "trigger_error" )))
			{
				$str .= '</div>';
				continue;
			}

			$str .= " - ".$bt[ "function" ]."( ";

			if( isset( $bt[ "args" ]))
			{
				$first = true;

				foreach( $bt[ "args" ] as $argno => $arg )
				{
					if( $bt[ "function" ] == "include" ||
						$bt[ "function" ] == "include_once" ||
						$bt[ "function" ] == "require" ||
						$bt[ "function" ] == "require_once" )
					{
						$arg = basename( $arg );
					}

					if( is_array( $arg ))
					{
						ob_start();
						print_r( $arg );
						$arg = ob_get_contents();
						ob_end_clean();
					}

					$str .= ( $first ? "" : ", " ).
						'<span style="background: #eee; padding: 0 4px;">'.$arg.'</span>';

					$first = false;
				}
			}

			$str .= " ) ";

			if( $bt[ "function" ] == "fatal_error" )
			{
				$str .= '</div>';
				break;
			}
		}

		$str .= '</div>';
	}

	$str .= '</div></div></div></div>';

	$logReaders = preg_split( '/[^0-9]/', $_config[ "logReaders" ], -1, PREG_SPLIT_NO_EMPTY );

	if( in_array( $_auth[ "useid" ], $logReaders ))
	{
		echo $str;
	}

	// Write to log.

	if( !$_config[ "logDisabled" ])
	{
		$logFile = "_log.html";

		$sendEmail = ( !file_exists( $logFile ) || filesize( $logFile ) == 0 );

		$fp = fopen( $logFile, "a" );
		flock( $fp, LOCK_EX );
		fwrite( $fp, $str."\n" );
		fclose( $fp );

		if( $sendEmail )
		{
			// In case the log file is empty, send an email to the site's admin(s)
			// notifying that the log file is no longer empty.

			include_once( INCLUDES."mailing.php" );

			$strMail = str_replace( '<div', "\n<div", $str );
			$strMail = strip_tags( str_replace( '<br', "\n<br", $strMail ));

			$strMail = "LOG: ".@url( "log" )."\n\n".$strMail;

			foreach( $logReaders as $reader )
			{
				sendEmail( $reader, "*LOG*", $strMail );
			}
		}
	}

	// Terminate generation on errors.

	if( $errno != E_USER_WARNING )
    {
		sql_note( "Script terminated with errors: ".strip_tags( $errstr ));
		sql_write_log();

    	echo "</body></html>";

		exit();
	}
}

?>
