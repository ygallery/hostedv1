<?php

// -----------------------------------------------------------------------------
// Various formatting functions.

// The content of this variable will be written at the end of layout.php.

$_addToFooter = '<table id="mainDescFloater" class="mainDescFloater" '.
	'cellspacing="0" cellpadding="0"><tr><td>'.
	'<div class="container2" style="padding: 2px"><div class="container2" id="mainDescFloaterContent"></div></div>'.
	'</td></tr></table>';

if( $_cmd[ 0 ] != "updatestrings" )
{
	include_once( INCLUDES."emoticons.inc" );
}
else
{
	$_emoticons = array();
}

// -----------------------------------------------------------------------------
// BBcodes.

define("_BBCODE_B", "b");
define("_BBCODE_I", "i");
define("_BBCODE_U", "u");
define("_BBCODE_S", "s");
define("_BBCODE_SUB", "sub");
define("_BBCODE_SUP", "sup");
define("_BBCODE_CENTER", "center");
define("_BBCODE_LEFT", "left");
define("_BBCODE_RIGHT", "right");
define("_BBCODE_JUSTIFY", "justify");
define("_BBCODE_URL", "url");
define("_BBCODE_ICON", "icon");
define("_BBCODE_USER", "u");
define("_BBCODE_CLUB", "club");
define("_BBCODE_CLUBLINK", "c");
define("_BBCODE_THUMB", "thumb");
define("_BBCODE_IMG", "img");
define("_BBCODE_FAQ", "faq");
define("_BBCODE_BR", "br");
define("_BBCODE_HR", "hr");

// -----------------------------------------------------------------------------
// delect if the browser is IE

$_isIE = false;
$_isOpera = false;

if(isset($_SERVER["HTTP_USER_AGENT"]))
{
	if(preg_match('/MSIE ([0-9]+)/', $_SERVER["HTTP_USER_AGENT"], $matches))
	{
		// yay, ie8 doesn't suck!
		if(intval($matches[1]) < 8)
			$_isIE = true;
	}

	if(preg_match('/Opera/', $_SERVER["HTTP_USER_AGENT"]))
	{
		$_isIE = false;
		$_isOpera = true;
	}
}

// -----------------------------------------------------------------------------
// Fix for the broken rendering of floating elements in IE

function iefixStart($width = 'width="100%"')
{
	global $_isIE;

	if($_isIE)
		echo '<table border="0" cellpadding="0" cellspacing="0" '.$width.'><tr><td>';
}

function iefixEnd()
{
	global $_isIE;

	if($_isIE)
		echo '</td></tr></table>';
}

// -----------------------------------------------------------------------------

function stringForJavascript( $str )
{
	return str_replace(array("\\", "\n", "\r", "'"), array("\\\\", '\n', '\r', "\\'"), $str);

/*	$str = addslashes( htmlspecialchars( $str ));
	$str = preg_replace( "/[\n]/", '\n', $str );
	$str = preg_replace( "/[\r]/", '\r', $str );

	return $str;*/
}

// -----------------------------------------------------------------------------
// PNG transparency support for IE

function getIMG( $src, $attrs = 'alt=""', $sizing = false )
{
	global $_isIE, $_config;

	if( $_config[ "filesPort" ] != "" || $_config[ "filesHost" ] != "" )
	{
		$src = str_replace( url(), urlf(), $src );
	}

	if( $_config[ "allowIEPNG" ])
	{
		if( !$_isIE || !preg_match( '/\.png$/', strtolower( $src )))
		{
			return( '<img src="'.$src.'" '.$attrs.' />' );
		}
		else
		{
			return( '<span class="pngIE" '.
				'style="filter: progid:DXImageTransform.Microsoft.AlphaImageLoader('.
				'src=\''.$src.'\', sizingMethod=\''.($sizing ? "scale" : "none").'\')">'.
				'<img src="'.$src.'" style="filter: alpha(opacity=0);" '.$attrs.' />'.
				// hide the original image but do not remove it as a visible element
				'</span>' );
		}
    }
    else
    {
		return( '<img src="'.$src.'" '.$attrs.' />' );
    }
}

// -----------------------------------------------------------------------------

function getSWF( $localFileName, $scale = 1.0, $maxWidth = 4096, $maxHeight = 4096 )
{
	if( trim( $localFileName ) == "" )
		return( "" );

	$imageInfo = getimagesize( $localFileName );
	$width = round( $imageInfo[ 0 ] * $scale );
	$height = round( $imageInfo[ 1 ] * $scale );
	$src = urlf().$localFileName;

	if( $width > $maxWidth || $height > $maxHeight )
	{
		$aspect = $height / $width;
		$width = $maxWidth;
		$height = $maxWidth * $aspect;

		if( $height > $maxHeight )
		{
			$coeff = $maxHeight / $height;
			$width *= $coeff;
			$height *= $coeff;
		}
	}

	ob_start();

	?>
	<object type="application/x-shockwave-flash" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
		codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"
		width="<?= $width ?>" height="<?= $height ?>">
		<param name="movie" value="<?= $src ?>" />
		<param name="quality" value="high" />
		<param name="wmode" value="transparent" />
		<embed src="<?= $src ?>" quality="high" wmode="transparent"
			width="<?= $width ?>" height="<?= $height ?>"
			name="submission" type="application/x-shockwave-flash"
			pluginspage="http://www.macromedia.com/go/getflashplayer">
		</embed>
	</object>
	<?

	$str = ob_get_contents();
	ob_end_clean();

	return( $str );
}

// -----------------------------------------------------------------------------
// Returns an id (used in html tags) that is unique within the current page.

function generateId($prefix = "yg_id")
{
	global $_lastGeneratedId;

	if( !isset( $_lastGeneratedId ))
	{
		$_lastGeneratedId = 1;
	}
	else
	{
		$_lastGeneratedId++;
	}

	return( $prefix.$_lastGeneratedId );
}

// -----------------------------------------------------------------------------
// URL formatting function. $path is a virtual path, meaning, it doesn't
// refer to any existing folders, but just defines a comment for the script.
// For example, when $path = "something/A/B/C" with mod_rewrite turned off
// the function returns: "[root/]?page=something&par1=A&par2=B&par3=C"
// and with mod_rewrite turned on it returns: "[root/]something/A/B/C/".
// $getvars contains the array of additional GET variables.
// Additional features:
// $path = "." will return the current path with only the GET variables
// changed (session id preserved).
// $path = "/" will return the root URL with all GET variables applied
// (session id is preserved)
// $path = "" will return the root URL without any GET variables, even
// if $getvars is set (session id is NOT preserved); this is used to attach
// the actual site root URL to image src="" and similar things.

function url( $path = "", $getvars = array(), $delimiter = '&amp;', $port = "", $host = "" )
{
	global $_config, $_mod_rewrite;

	$url = "http://".
		( $host != "" ? $host : $_config[ "galRoot" ]).
		( $port != "" ? ( ":".$port ) : "" )."/";

	if( $path == "" )
	{
		return( $url );
	}

	if( $path == "/" )
		$path = "";

	if( $path == "." )
		$path = getCurrentPath();

	$first = true; // put "?" instead of "&amp;" before the first GET variable

	if( $_mod_rewrite )
	{
		$url .= $path;

		if( !preg_match( '/\/$/', $url ))
			$url .= "/";
	}
	elseif( $path != "" )
	{
		$path_items = preg_split( '/\//', $path );

		for( $i = 0; $i < count( $path_items ); $i++ )
		{
			$pathItem = preg_replace( '/([^a-zA-Z0-9\.\-\_\~])/', "", $path_items[ $i ]);

			$url .= ( $first ? '?page' : $delimiter.'par'.$i ).'='.$pathItem;

			$first = false;
		}
	}

	foreach( array( "yGalSession", "yGalLanguage", "yGalFont" ) as $keyName )
	{
		if( !isset( $_COOKIE[ $keyName ]) && isset( $GLOBALS[ "_".$keyName ]))
		{
			$getvars[ $keyName ] = $GLOBALS[ "_".$keyName ];
		}
	}

	foreach( $getvars as $name => $value )
	{
		$url .= ( $first ? '?' : $delimiter ).$name.'='.urlencode( $value );
		$first = false;
	}

	return( $url );
}

// An addition if you have installed a different httpd server to access
// the /files directory. That server's port and/or hostname should be defined
// in the filesPort and filesHost config variables.

function urlf( $path = "", $getvars = array(), $delimiter = "&amp;" )
{
	global $_config;

	return( url( $path, $getvars, $delimiter, $_config[ "filesPort" ],
		$_config[ "filesHost" ]));
}

// -----------------------------------------------------------------------------
// Returns the user's current location.

function getCurrentPath()
{
	global $_cmd;

	$path = "";
	$first = true;

	reset( $_cmd );

	foreach( $_cmd as $cmd1 )
	{
		if( $cmd1 == "" )
			continue;

		$path .= ( $first ? "" : "/" ).$cmd1;
		$first = false;
	}

	return( $path );
}

// -----------------------------------------------------------------------------
// Text formatting function.

function formatText($str, $noEmoticons = false, $noBBCode = false, $allowImages = false)
{
	global $_emoticons, $__maxIcons, $_config;

	$__maxIcons = $_config["maxIconsPerPost"];

	// Convert http://... to clickable links.

	$str = preg_replace_callback(
		'/(.)?(http(s?):\/\/)([\w\-]+\.[\w]+[\w\~\!\@\#\.\/\%\&\:\;\=\?\+\-]+)/',
		'fmtCallbackPlainLink', $str );

	$str = nl2br(htmlspecialchars($str));
	$str = str_replace("\n", '', $str);
	$str = str_replace("\r", '', $str);

	$str = preg_replace( '/(\&amp\;)(bull|cent|clubs|copy|darr|deg|diams|'.
		'harr|hearts|laquo|larr|not|middot|pound|raquo|rarr|reg|spades|'.
		'times|trade|uarr|yen)\;/', '&\\2;', $str );

	// process emoticon expressions

	if( !$noEmoticons )
	{
		reset( $_emoticons );

		foreach( $_emoticons as $emokey => $emo1 )
		{
			$strold = $str;

			if( $emo1[ "emoExpr" ][ 0 ] != ";" )
			{
				$str = str_replace( $emo1[ "emoExpr" ], '@@<'.$emokey.'>@@', $str );
			}
			else
			{
				$str = str_replace( ' '.$emo1[ "emoExpr" ], ' @@<'.$emokey.'>@@', $str );
			}

			if( $strold != $str && !isset( $_emoticons[ $emokey ][ "img" ]))
			{
				$_emoticons[ $emokey ][ "img" ] =
					getIMG(url().'images/emoticons/'.$emo1["emoFilename"],
					'alt="'.$emo1["emoExpr"].'" title="'.$emo1["emoExpr"].'"').
					($GLOBALS["_isIE"] ? " " : "");
			}
		}

		reset( $_emoticons );

		foreach( $_emoticons as $emokey => $emo1 )
		{
			if( isset( $_emoticons[ $emokey ][ "img" ]))
			{
				$str = str_replace( '@@<'.$emokey.'>@@',
					$_emoticons[ $emokey ][ "img" ], $str );
			}
		}
	}

	if(!$noBBCode)
	{
		// convert user avatar icons to images

		$str = preg_replace_callback('/\['._BBCODE_ICON.'\=(.+?)\]/', 'fmtCallbackUsericon', $str);

		// convert user names

		$str = preg_replace_callback('/\['._BBCODE_USER.'\=(.+?)\]/', 'fmtCallbackUsername', $str);

		// convert club icons to images

		$str = preg_replace_callback('/\['._BBCODE_CLUB.'\=(.+?)\]/', 'fmtCallbackClubicon', $str);

		// convert club links

		$str = preg_replace_callback('/\['._BBCODE_CLUBLINK.'\=(.+?)\]/', 'fmtCallbackClublink', $str);

		// convert faq links

		$str = preg_replace_callback('/\['._BBCODE_FAQ.'\=(.+?)\]/', 'fmtCallbackFaqlink', $str);

		// convert thumbs to images

		$str = preg_replace_callback('/\['._BBCODE_THUMB.'\=(.+?)\]/', 'fmtCallbackThumb', $str);

		// convert images
		/*
		if($allowImages)
			$str = preg_replace_callback('/\['._BBCODE_IMG.'\=(.+?)\]/', 'fmtCallbackImage', $str);
		*/

		// convert URLs to links

		$str = preg_replace_callback('/\['._BBCODE_URL.'\=(.*?)\](.*?)\[\/'._BBCODE_URL.'\]/', 'fmtCallbackURL', $str);

		// apply misc tags

		$str = fmtProcessTag(_BBCODE_B, '<b>', '</b>', $str);
		$str = fmtProcessTag(_BBCODE_BR, '<br>', '', $str);
		$str = fmtProcessTag(_BBCODE_HR, '<hr>', '', $str);
		$str = fmtProcessTag(_BBCODE_I, '<i>', '</i>', $str);
		$str = fmtProcessTag(_BBCODE_U, '<u>', '</u>', $str);
		$str = fmtProcessTag(_BBCODE_S, '<span style="text-decoration : line-through;">', '</span>', $str);
		$str = fmtProcessTag(_BBCODE_SUP, '<sup>', '</sup>', $str);
		$str = fmtProcessTag(_BBCODE_SUB, '<sub>', '</sub>', $str);
		$str = fmtProcessTag(_BBCODE_LEFT, '<div class="a_left">', '</div>', $str);
		$str = fmtProcessTag(_BBCODE_CENTER, '<div class="a_center">', '</div>', $str);
		$str = fmtProcessTag(_BBCODE_JUSTIFY, '<div class="a_justify">', '</div>', $str);
		$str = fmtProcessTag(_BBCODE_RIGHT, '<div class="a_right">', '</div>', $str);

		// avoid extra empty lines after text alignment tags

		$str = str_replace( '</div><br />', '</div>', $str );
	}

	return $str;
}

// -----------------------------------------------------------------------------
// Substitutes "[$tag]" with 'resOpenTag' and "[/$tag]" with 'resCloseTag',
// but only if the count of "[$tag]" matches the count of "[/$tag]" in 'str'.

function fmtProcessTag($tag, $resOpenTag, $resCloseTag, $str)
{
	$c1 = preg_match_all('/\['.$tag.'\]/', $str, $out);
	$c2 = preg_match_all('/\[\/'.$tag.'\]/', $str, $out);

	if($c1 == $c2)
	{
		$str = str_replace('['.$tag.']', $resOpenTag, $str);
		$str = str_replace('[/'.$tag.']', $resCloseTag, $str);
	}

	return $str;
}

function fmtParseParams($str, &$name, &$size)
{
	$par = preg_split('/\s/', $str, -1, PREG_SPLIT_NO_EMPTY);
	$name = $par[0];

	foreach($par as $par1)
	{
		$pdata = preg_split('/=/', $par1, 2, PREG_SPLIT_NO_EMPTY);

		if(count($pdata) < 2)
			continue;

		switch(trim(strtolower($pdata[0])))
		{
			case "size":
				$size = intval(trim($pdata[1]));
				break;
		}
	}
}

function fmtCallbackURL($sender)
{
	global $_config;

	if($sender[1] == "%s")
		return '<a href="%s">'.$sender[2].'</a>';

	if( preg_match( '/^\//', $sender[ 1 ]))
		return '<a href="http://'.$_config["galRoot"].$sender[ 1 ].'">'.
			$sender[ 2 ].'</a>';

	if(preg_match('/^([\w]+\.[\w])/', $sender[1]))
		return '<a href="http://'.$sender[1].'">'.$sender[2].'</a>';

	if(preg_match('/^((http(s?))|(ftp))\:\/\//', $sender[1]))
		return '<a href="'.$sender[1].'">'.$sender[2].'</a>';

	return $sender[0];
}

function fmtCallbackPlainLink($sender)
{
	global $_config;

	if( $sender[ 1 ] == "" || $sender[ 1 ] == " " ||
		$sender[ 1 ] == "(" || $sender[ 1 ] == ">" )
	{
		$url = substr( $sender[ 0 ], strlen( $sender[ 1 ]));
		$text = preg_replace( '/^http(s?)\:\/\//', "", $url );
		$text = preg_replace( '/(\.)(\w+?)\/$/', "\\1\\2", $text );
		$text = strlen( $text ) < 32 ? $text :
			substr( $text, 0, 22 )."&middot;&middot;&middot;&middot;".
			substr( $text, strlen( $text ) - 8 );

		return( $sender[ 1 ].'[url='.$url.']'.$text.'[/url]' );
	}
	else
	{
		return( $sender[ 0 ]);
	}
}

function fmtCallbackImage($sender)
{
	if(preg_match('/^([\w]+\.[\w])/', $sender[1]))
		return '<img alt="[..]" src="http://'.$sender[1].'" />';

	if(preg_match('/^((http)|(https)|(ftp))\:\/\//', $sender[1]))
		return '<img alt="[..]" src="'.$sender[1].'" />';

	return $sender[0];
}

// -----------------------------------------------------------------------------
// Callback for substituting [icon=username] with user's avatar icon.
// Returns the original expression if the specified user was not found.

function fmtCallbackUsericon($sender)
{
	global $__maxIcons;

	if($__maxIcons <= 0)
		return '['._BBCODE_ICON.'='.$sender[1].']';

	$__maxIcons--;
	$size = 100; // default value

	fmtParseParams($sender[1], $name, $size);

	if($size < 40)
		$size = 40; // minimum value

	if($size > 100)
		$size = 100; // maximum value

	return getUserAvatar($name, 0, false, false, $size);
}

function fmtCallbackUsername($sender)
{
	$result = sql_query("SELECT `useid` FROM `users` ".
		"WHERE `useUsername` = '".addslashes($sender[1])."' LIMIT 1");

	// check if the user exists

	if(!$useData = mysql_fetch_assoc($result)) // no?
		return $sender[0];

	return getUserLink($useData["useid"]);
}

function fmtCallbackClublink( $sender )
{
	$result = sql_query( "SELECT * FROM `clubs` ".
		"WHERE `cluid` = '".intval( $sender[ 1 ])."' LIMIT 1" );

	if( !$cluData = mysql_fetch_assoc( $result ))
		return $sender[ 0 ];

	return getIMG( url()."images/emoticons/club2.png" ).
		'<a title="'.strip_tags( formatText( $cluData[ "cluDesc" ])).
		'" href="'.url( "club/".$cluData[ "cluid" ]).'">'.
		$cluData[ "cluName" ].'</a>';
}

// -----------------------------------------------------------------------------
// Callback for substituting [faq=faq_id] with a link to that article.
// Returns the original expression if the specified object was not found.

function fmtCallbackFaqlink ( $sender )
{
	$result = sql_query( "SELECT * FROM `helpdeskFAQ`,`helpdeskFAQCats` ".
		"WHERE `hfqCategory`=`hfcid` AND `hfqid` = '".intval( $sender[ 1 ])."' LIMIT 1" );

	if( !$faqData = mysql_fetch_assoc( $result ))
		return $sender[ 0 ];

	$faqTitle = preg_replace( '/^.*\|/', "", $faqData[ "hfqTitle" ]);

	return 	'<b>FAQ '.$faqData[ "hfqid"].':</b>
				<a href="'.url( "helpdesk/faq/".$faqData[ "hfcIdent" ]."/".$faqData[ "hfqIdent" ]).'">
				'.htmlspecialchars( $faqTitle ).'</a>';

	mysql_free_result($result);
}

// -----------------------------------------------------------------------------
// Callback for substituting [club=club_id] with club icon.
// Returns the original expression if the specified club was not found.

function fmtCallbackClubicon($sender)
{
	global $__maxIcons;

	if($__maxIcons <= 0)
		return '['._BBCODE_CLUB.'='.$sender[1].']';

	$__maxIcons--;
	$size = 100; // default value

	fmtParseParams($sender[1], $id, $size);

	if($size < 40)
		$size = 40; // minimum value

	if($size > 100)
		$size = 100; // maximum value

	return getClubIcon($id, $size);
}

// -----------------------------------------------------------------------------
// Callback for substituting [thumb=obj_id] with half-sized thumb.
// Returns the original expression if the specified object was not found.

function fmtCallbackThumb($sender)
{
	global $__maxIcons;

	if($__maxIcons <= 0) return '['._BBCODE_THUMB.'='.$sender[1].']';

	$__maxIcons--;
	$size = 30; // default value

	fmtParseParams($sender[1], $id, $size);

	if($size < 20)
		$size = 20; // minimum value

	if($size > 60)
		$size = 60; // maximum value

	return getObjectThumb($id, $size);
}

// -----------------------------------------------------------------------------
// Returns user id by username.

$_getUseridByNameCache = array();

function getUseridByName( $username )
{
	global $_getUseridByNameCache;

	if( isset( $_getUseridByNameCache[ $username ]))
	{
		return( $_getUseridByNameCache[ $username ]);
	}

	$useResult = sql_query( "SELECT `useid` FROM `users`".dbWhere( array(
		"useUsername" => $username )));

	if( $useData = mysql_fetch_row( $useResult ))
	{
		$useid = $useData[ 0 ];
	}
	else
	{
		$useid = 0;
	}

	$_getUseridByNameCache[ $username ] = $useid;

	return( $useid );
}

// -----------------------------------------------------------------------------

$_getUserDataCache = array();
$_getUserDataCacheIds = array();

function prefetchUserData( $useids )
{
	global $_getUserDataCache, $_getUserDataCacheIds;

	$useids = array_diff( $useids, $_getUserDataCacheIds );

	if( count( $useids ) == 0 )
		return;

	sort( $useids );

	$result = sql_query( "SELECT `useid`,`useUsername`,`useLastAction`, `useIsRetired`, `useIsHelpdesk`,`useIsModerator`,".
		"`useIsSModerator`,`useIsDeveloper`,`useIsBanned`,UNIX_TIMESTAMP(`useSuspendedUntil`) AS `useSuspendedUntilTS`,`useIsHidden`,`useGuestAccess`,".
		"`useAvatarWidth`,`useAvatarHeight`,`useAvatarDate`,`useAvatarExt`,".
		"UNIX_TIMESTAMP(`useLastAction`) AS `useLastActionTS` ".
		"FROM `users`,`useExtData` WHERE `useEid` = `useid` ".
		"AND `useid` IN('".implode( "','", $useids )."')" );

	while( $useData = mysql_fetch_assoc( $result ))
	{
		$_getUserDataCache[ $useData[ "useid" ]] = $useData;
		$_getUserDataCacheIds[] = $useData[ "useid" ];
	}
}

// -----------------------------------------------------------------------------

function getUserData( $useid )
{
	global $_getUserDataCache, $_getUserDataCacheIds;

	if( isset( $_getUserDataCache[ $useid ]))
	{
		$useData = $_getUserDataCache[ $useid ];
	}
	else
	{
		$result = sql_query( "SELECT `useid`,`useUsername`,`useLastAction`, `useIsRetired`, `useIsHelpdesk`,`useIsModerator`,".
			"`useIsSModerator`,`useIsDeveloper`,`useIsBanned`,UNIX_TIMESTAMP(`useSuspendedUntil`) AS `useSuspendedUntilTS`,`useIsHidden`,`useGuestAccess`,".
			"`useAvatarWidth`,`useAvatarHeight`,`useAvatarDate`,`useAvatarExt`,".
			"UNIX_TIMESTAMP(`useLastAction`) AS `useLastActionTS` ".
			"FROM `users`,`useExtData` WHERE `useEid` = `useid` ".
			"AND `useid` = '$useid' LIMIT 1" );

		$found = true;

		if( !$useData = mysql_fetch_assoc( $result )) // User does not exist
			$found = false;

		if( !isLoggedIn() && !$useData[ "useGuestAccess" ]) // Inaccessible for guests
			$found = false;

		if( !$found )
		{
			$useData[ "useid" ] = 0;
		}

		$_getUserDataCache[ $useid ] = $useData;
		$_getUserDataCacheIds[] = $useid;
	}

	return( $useData );
}

// -----------------------------------------------------------------------------
// Returns the html text showing the avatar icon. You may specify either
// $useid or $username. If $showName is true, the username will be displayed
// under the avatar icon.
// RULE: $username must not contain " and ' characters!

$_getUserAvatarCache = array();

function getUserAvatar( $username = "", $useid = 0, $showName = false, $minimized = false,
	$size = 100, $targetURL = "" )
{
	global $_getUserAvatarCache;

	if( $username != "" )
	{
		$useid = getUseridByName( $username );
	}

	if( isset( $_getUserAvatarCache[ $useid ][ $showName ][ $minimized ][ $size ][ $targetURL ]))
	{
		return( $_getUserAvatarCache[ $useid ][ $showName ][ $minimized ][ $size ][ $targetURL ]);
	}

	$useData = getUserData( $useid );

	if( $useData[ "useid" ] == 0 )
	{
		$str = '['._BBCODE_ICON.'=???]';

		$_getUserAvatarCache[ $useid ][ $showName ][ $minimized ][ $size ][ $targetURL ] = $str;

		return( $str );
	}

	// found the user

	if( $useData[ "useAvatarDate" ] == 0 )
	{
		$filename = "images/noavatar_v4.png";
		$width = 48;
		$height = 48;
	}
	else
	{
		$filename = applyIdToPath( "files/avatars/", $useData[ "useid" ])."-".
			$useData[ "useAvatarDate" ].".".$useData[ "useAvatarExt" ];

		$width = $useData[ "useAvatarWidth" ];
		$height = $useData[ "useAvatarHeight" ];
	}

	$origWidth = $width;
	$origHeight = $height;
	$width = round( $width * $size / 100 );
	$height = round( $height * $size / 100 );

	if( $minimized )
	{
		$width = 48; // in pixels
		$height = 48;
	}

	if( $width != $origWidth || $height != $origHeight )
	{
		makeFloatingThumb( $useData[ "useUsername" ], url().$filename,
			$origWidth, $origHeight, 0, false, $onmouseover, $onmouseout );
	}
	else
	{
		$onmouseover = "";
		$onmouseout = "";
	}

	if( $targetURL == "" )
	{
		$targetURL = url( "user/".strtolower( $useData[ "useUsername" ]));
	}

	$str = '<a href="'.$targetURL.'" class="avatar">'.
		getIMG( urlf().$filename, 'style="width: '.$width.'px; height: '.$height.'px" '.
		'alt="['._BBCODE_ICON.'='.$useData[ "useUsername" ].']" '.
		'onmouseover="'.$onmouseover.'" onmouseout="'.$onmouseout.'" '.
		'title="'.$useData[ "useUsername" ].'"', true );

	if( $showName )
	{
		$str .= '<br />'.getOnlineLamp( $useData ).$useData[ "useUsername" ];
	}

	$str .= '</a>';

	$_getUserAvatarCache[ $useid ][ $showName ][ $minimized ][ $size ][ $targetURL ] = $str;

	return( $str );
}

$_getUserLinkCache = array();


// -----------------------------------------------------------------------------
// Returns the html text showing the username linked to the user's page and their online status

function getUserLink($useid)
{
	global $_getUserLinkCache;

	$useid = intval( $useid );

	if(isset($_getUserLinkCache[$useid]))
		return $_getUserLinkCache[$useid];

	$useData = getUserData( $useid );

	if( $useData[ "useid" ] == 0 )
	{
		$str = '???';
		$_getUserLinkCache[ $useid ] = $str;
		return( $str );
	}

	$str = '<span class="nowrap">'.getOnlineLamp($useData).
		'<a href="'.url("user/".strtolower($useData["useUsername"])).'">'.
		$useData["useUsername"].'</a></span>';

	$_getUserLinkCache[$useid] = $str;

	return $str;
}


// -----------------------------------------------------------------------------
// Returns the html text showing the club icon.

$_getClubIconCache = array();

function getClubIcon( $cluid = 0, $size = 100 )
{
	global $_getClubIconCache;

	$cluid = intval( $cluid );

	if( isset( $_getClubIconCache[ $cluid ][ $size ]))
	{
		return( $_getClubIconCache[ $cluid ][ $size ]);
	}

	$result = sql_query("SELECT `cluid`,`cluName` FROM `clubs` ".
		"WHERE `cluid` = '$cluid' LIMIT 1");

	// check if the club exists

	if(!$cluData = mysql_fetch_assoc($result)) // no?
		return '['._BBCODE_CLUB.'='.$cluid.']';

	// found the club

	$filename = findNewestFileById("files/clubs/", $cluData["cluid"], "images/noavatar_v4.png");
	$imageInfo = getimagesize($filename);

	$width = round($imageInfo[0] * $size / 100);
	$height = round($imageInfo[1] * $size / 100);

	if($size < 100)
		makeFloatingThumb(htmlspecialchars($cluData["cluName"]), url().$filename,
			$imageInfo[0], $imageInfo[1], 0, false, $onmouseover, $onmouseout);
	else
	{
		$onmouseover = "";
		$onmouseout = "";
	}

	$str = '<a href="'.url("club/".$cluData["cluid"]).'">'.
		getIMG(urlf().$filename, 'style="width: '.$width.'px; height: '.$height.'px" '.
		'alt="['._BBCODE_CLUB.'='.$cluid.']" '.
		'onmouseover="'.$onmouseover.'" onmouseout="'.$onmouseout.'" '.
		'title="'.htmlspecialchars($cluData["cluName"]).'"', true).'</a>';

	$_getClubIconCache[ $cluid ][ $size ] = $str;

	return $str;
}

// -----------------------------------------------------------------------------
// Returns the html text showing the half-sized thumb.

function getObjectThumb($objid = 0, $size = 30, $isExtras = false)
{
	global $_auth;

	$objid = intval($objid);

	$where = "`objid` = '$objid'";

	if( !atLeastHelpdesk() )
	{
		$where = "($where) AND `objDeleted` = '0' AND `objPending` = '0'";
	}

	applyObjFilters( $where );

	$result = sql_query("SELECT * FROM `".( $isExtras ? "extras" : "objects" ).
		"` WHERE $where LIMIT 1");

	// check if the object exists - Why did this say club before?

	if(!$objData = mysql_fetch_assoc($result)) // no?
		return getIMG(urlf()."images/nothumb.gif",
			'alt="['._BBCODE_THUMB.'='.$objid.']" title="'.
			sprintf(_UNKNOWN_SUBMISSION, $objid).'" class="microthumb"');

	if( !isLoggedIn() )
	{
		$sql = "SELECT `useGuestAccess` FROM `useExtData`".dbWhere( array(
			"useEid" => $objData[ "objCreator" ]));

		$result = sql_query( $sql );

		if( !mysql_result( $result, 0 ))
			return getIMG(urlf()."images/nothumb.gif",
				'alt="['._BBCODE_THUMB.'='.$objid.']" title="'.
				sprintf(_UNKNOWN_SUBMISSION, $objid).'" class="microthumb"');
	}

	// found the object

	if( !$isExtras && $objData[ "objThumbDefault"] )
	{
		$filename = "images/litthumb.png";
	}
	else
	{
		$filename = $isExtras ? "" : findNewestFileById("files/thumbs/", $objid, "images/nothumb.gif");
	}

	$width = round($objData["objThumbWidth"] * $size / 100);
	$height = round($objData["objThumbHeight"] * $size / 100);

	$objTitle = htmlspecialchars( $objData[ "objTitle" ]).' <br /> ';

	if( $objData[ "objCollab" ] > 0)
	{
		$objTitle .= sprintf( _BY_AND,
			getUserLink($objData["objCreator"]),
			getUserLink( $objData[ "objCollab" ]));
	}
	else
	{
		$objTitle .= sprintf( _BY,
			getUserLink($objData["objCreator"]));
	}

	$src = $isExtras ? $objData[ "objThumbURL" ] : ( urlf().$filename );

	if($size < 100)
		makeFloatingThumb(strip_tags($objTitle), $src,
			$objData["objThumbWidth"], $objData["objThumbHeight"], $objData["objMature"], true,
			$onmouseover, $onmouseout);
	else
	{
		$onmouseover = "";
		$onmouseout = "";
	}

	$str = '<a href="'.url("view/".( $isExtras ? "e" : "" ).$objData["objid"]).'">'.
		getIMG( $src,
		'alt="['._BBCODE_THUMB.'='.$objid.']" class="microthumb'.($objData["objMature"] ? " mature" : "").'" '.
		'style="width: '.$width.'px; height: '.$height.'px" '.
		'onmouseover="'.$onmouseover.'" onmouseout="'.$onmouseout.'" '.
		'title="'.strip_tags($objTitle).'"', true).'</a> ';

	return $str;
}

function makeFloatingThumb($title, $src, $width, $height, $isMature, $isThumb,
	&$onmouseover, &$onmouseout)
{
	global $_addToFooter;

	$floaterId = generateId("thumbFloat");

	$title = strip_tags( formatText( $title ));

	$_addToFooter .= // this will be later written at the very end of the page
		'<div id="'.$floaterId.'" class="floating_thumb" '.
		'style="width: '.($width + ($isThumb ? 6 : 0)).'px; height: '.($height + ($isThumb ? 6 : 0)).'px">'.
		getIMG($src, 'alt="'.$title.'" class="'.($isThumb ? "thumb " : "").($isMature ? " mature" : "").
			'" title="'.$title.'"').'</div>'."\n";

	$onmouseover = "current_floater = get_by_id('$floaterId'); this.onmousemove = move_to_element;";
	$onmouseout = "el=get_by_id('$floaterId'); if(el) { make_invisible('$floaterId'); current_floater = 0; }";
}

$_cacheOnlineLamp = array();

function getOnlineLamp(&$useData)
{
	global $_auth, $_cacheOnlineLamp;

	if(!$_auth["useid"])
		return ""; // don't show the lamps to non-registered users

	if(isset($_cacheOnlineLamp[$useData["useid"]]))
		return $_cacheOnlineLamp[$useData["useid"]];

	if($useData["useIsHidden"] && !atLeastModerator())
		$isOnline = false;

	else
	{
		$visitedAgo = strlen($useData["useLastAction"]) > 10 ?
			(time() - $useData["useLastActionTS"]) : -1;
		$isOnline = ($visitedAgo >= 0 && $visitedAgo < 10 * 60);
	}

	if($useData["useIsDeveloper"])
		$str = getIMG(url()."images/emoticons/dev".($isOnline ? "" : "0").".png",
			'alt="'.($isOnline ? _ONLINE : _OFFLINE).' / '._DEVELOPER.'" title="'.($isOnline ? _ONLINE : _OFFLINE).' / '._DEVELOPER.'"');
	elseif($useData["useIsSModerator"])
		$str = getIMG(url()."images/emoticons/smod".($isOnline ? "" : "0").".png",
			'alt="'.($isOnline ? _ONLINE : _OFFLINE).' / '._SUPERMODERATOR.'" title="'.($isOnline ? _ONLINE : _OFFLINE).' / '._SUPERMODERATOR.'"');
	elseif($useData["useIsModerator"])
		$str = getIMG(url()."images/emoticons/mod".($isOnline ? "" : "0").".png",
			'alt="'.($isOnline ? _ONLINE : _OFFLINE).' / '._MODERATOR.'" title="'.($isOnline ? _ONLINE : _OFFLINE).' / '._MODERATOR.'"');
	elseif($useData["useIsHelpdesk"])
		$str = getIMG(url()."images/emoticons/hds".($isOnline ? "" : "0").".png",
			'alt="'.($isOnline ? _ONLINE : _OFFLINE).' / HDStaff" title="'.($isOnline ? _ONLINE : _OFFLINE).' / HDStaff"');
	elseif($useData["useIsRetired"])
		$str = getIMG(url()."images/emoticons/ret".($isOnline ? "" : "0").".png",
			'alt="'.($isOnline ? _ONLINE : _OFFLINE).' / '._RETIRED.'" title="'.($isOnline ? _ONLINE : _OFFLINE).' / '._RETIRED.'"');
	elseif($useData["useIsBanned"])
		$str = getIMG(url()."images/emoticons/banned.png",
		  'alt="'. _MOD_BANNED .'" title="'. _MOD_BANNED .'"');
	elseif($useData["useSuspendedUntilTS"] > time())
		$str = getIMG(url()."images/emoticons/suspended.png",
		  'alt="'. _MOD_SUSPENDED .'" title="'. _MOD_SUSPENDED .'"');
	else
		$str = getIMG(url()."images/emoticons/".($isOnline ? "online" : "offline").".png",
			'alt="'.($isOnline ? _ONLINE : _OFFLINE).'" title="'.($isOnline ? _ONLINE : _OFFLINE).'"');

	$_cacheOnlineLamp[$useData["useid"]] = $str;

	return( $str );
}

function getOnlineStatus( &$useData )
{
	global $_auth;

	if(!$_auth["useid"])
		return ""; // don't show the lamps to non-registered users

	if($useData["useIsHidden"] && !atLeastModerator())
		$isOnline = false;
	else
	{
		$visitedAgo = strlen($useData["useLastAction"]) > 10 ?
			(time() - $useData["useLastActionTS"]) : -1;
		$isOnline = ($visitedAgo >= 0 && $visitedAgo < 10 * 60);
	}

	if($useData["useIsDeveloper"])
		$str = "dev".($isOnline ? "" : "0");
	elseif($useData["useIsSModerator"])
		$str = "smod".($isOnline ? "" : "0");
	elseif($useData["useIsModerator"])
		$str = "mod".($isOnline ? "" : "0");
	elseif($useData["useIsHelpdesk"])
		$str = "hds".($isOnline ? "" : "0");
	elseif($useData["useIsRetired"])
		$str = "ret".($isOnline ? "" : "0");
	elseif($useData["useIsBanned"])
		$str = "banned";
	elseif($useData["useSuspendedUntilTS"] > time())
		$str = "suspended";
	else
		$str = ($isOnline ? "1" : "0");

	return( $str );
}

function getFolderIcon( $folid )
{
	return( getIMG( urlf().findNewestFile(
		applyIdToPath( "files/foldericons/", $folid )."-*",
		"images/folder_noicon.png" )));
}

// -----------------------------------------------------------------------------
// Returns the last matching filename on the sorted list of all filenames
// found by specified $wildcard, otherwise returns $notfound if no files found.

function findNewestFile($wildcard, $notfound = "")
{
	$filefind = glob($wildcard, GLOB_NOESCAPE);

	return ($filefind && count($filefind)) ?
		$filefind[count($filefind) - 1] : $notfound;
}

// -----------------------------------------------------------------------------
// Searches for an image with specified $id inside $path.
// Returns the last matching image filename on the sorted list of all
// the filenames that match (this can be handy to keep the image edition history,
// e.g. "files/data/0/0/123-200501241651.jpg" while you only need the id 123 to
// get the latest edition), otherwise returns $notfound if no images found.

function findNewestFileById($path, $id, $notfound = "")
{
	return findNewestFile(applyIdToPath($path, $id)."-*", $notfound);
}

function applyIdToPath($path, $id)
{
	$subid2 = strval(floor($id / 10000));
	$subid1 = substr(strval($subid2), 0, 1);

	return $path.$subid1."/".$subid2."/".$id;
}

// -----------------------------------------------------------------------------
// Returns a fuzzy number instead of the exact $number.

function fuzzy_number($number, $divisor = 1)
{
	global $_auth;

	if($_auth["useid"] && !$_auth["useFuzzyNumbers"])
		return $number;

	if($number/$divisor >= 987) $result = _FUZZY_987;
	elseif($number/$divisor >= 456) $result = _FUZZY_456;
	elseif($number/$divisor >= 234) $result = _FUZZY_234;
	elseif($number/$divisor >= 99) $result = _FUZZY_99;
	elseif($number/$divisor >= 48) $result = _FUZZY_48;
	elseif($number/$divisor >= 24) $result = _FUZZY_24;
	elseif($number/$divisor >= 11) $result = _FUZZY_11;
	elseif($number/$divisor >= 7) $result = _FUZZY_7;
		// the following numbers don't have "/$divisor" because we don't want
		// 10 and 20 to be shown as "one" and "a couple" if $divisor is say 10
	elseif($number >= 3) $result = _FUZZY_3;
	elseif($number >= 2) $result = _FUZZY_2;
	elseif($number >= 1) $result = _FUZZY_1;
	else $result = _FUZZY_0;

	return '<acronym title="'.$number.'">'.$result.'</acronym>';
}

// -----------------------------------------------------------------------------
// Returns offset for page navigation depending on the current $offset, used
// $limit and the $totalCount of entries. Returns the values in $prevOffset,
// $nextOffset and $lastOffset. If the offset given by this function is the
// same as the current $offset, the corresponding navigation button should not
// be displayed (or should be grayed out).

function getPagingOffsets( $offset, $limit, $totalCount, & $prevOffset,
	& $nextOffset, & $lastOffset )
{
	global $_cmd;

	if( $limit > 0 )
	{
		$lastOffset = floor(( $totalCount - 1 ) / $limit ) * $limit;
	}
	else
	{
		$lastOffset = 0;
	}

	$lastOffset = $lastOffset < 0 ? 0 : $lastOffset;

	$prevOffset = $offset - $limit;
	$nextOffset = $offset + $limit;

	if( $_cmd[ 0 ] != "search" && $_cmd[ 0 ] != "user" )
	{
		$prevOffset = $prevOffset < 0 ? 0 : ( $prevOffset > $lastOffset ?
			$lastOffset : $prevOffset );

		$nextOffset = $nextOffset < 0 ? 0 : ( $nextOffset > $lastOffset ?
			$lastOffset : $nextOffset );
	}
	else
	{
		$prevOffset = $prevOffset < 0 ? 0 : $prevOffset;
		$nextOffset = $nextOffset < 0 ? 0 : $nextOffset;
	}
}

// -----------------------------------------------------------------------------
// Outputs standard navigation control for browsing through multiple pages.

function navControls( $offset, $limit, $totalCount, $firstURL = "",
	$prevURL = "", $nextURL = "", $lastURL = "", $offsetVar = "offset",
	$containerClass = "text_under_icon_container_right", $checkOffset = -1 )
{
	global $_cmd;

	if( $checkOffset == -1 )
	{
		$checkOffset = $offset;
	}

	getPagingOffsets( $offset, $limit, $totalCount, $prevOffset, $nextOffset,
		$lastOffset );

	if( $_cmd[ 0 ] == "user" )
	{
		$lastOffset = isset( $_GET[ "last" ]) ? intval( $_GET[ "last" ]) : "last";
	}

	if( $firstURL == "" )
	{
		$firstURL = url( ".", array_merge( $_GET, array( $offsetVar => 0 )));
		$prevURL = url( ".", array_merge( $_GET, array( $offsetVar => $prevOffset )));
		$nextURL = url( ".", array_merge( $_GET, array( $offsetVar => $nextOffset )));
		$lastURL = url( ".", array_merge( $_GET, array( $offsetVar => $lastOffset )));
	}
	elseif( $prevURL == "" )
	{
		$prevURL = str_replace( "OFFSET", $prevOffset, $firstURL );
		$nextURL = str_replace( "OFFSET", $nextOffset, $firstURL );
		$lastURL = str_replace( "OFFSET", $lastOffset, $firstURL );
		$firstURL = str_replace( "OFFSET", 0, $firstURL );
	}

	?>
	<div class="<?=$containerClass?>">
	<?

	if( $firstURL != $prevURL )
	{
		textUnderIcon( $offset != 0 ? $firstURL : "", _PAGE_FIRST,
			url()."images/emoticons/nav-first.png" );
	}

	textUnderIcon( $prevOffset != $checkOffset ? $prevURL : "", _PAGE_PREVIOUS,
		url()."images/emoticons/nav-prev.png" );

	$isLastOffset = isset( $_GET[ "last" ]) && intval( $_GET[ "last" ]) == $offset;

	if( !$isLastOffset )
	{
		textUnderIcon( $nextOffset != $checkOffset ? $nextURL : "", _PAGE_NEXT,
			url()."images/emoticons/nav-next.png" );
	}

	if( $nextOffset != $lastOffset && !$isLastOffset )
	{
		textUnderIcon(
			$lastOffset != $checkOffset || $_cmd[ 0 ] == "user" ? $lastURL : "",
			_PAGE_LAST, url()."images/emoticons/nav-last.png" );
	}

	?>
	<div class="clear">&nbsp;</div>
	</div>
	<?
}

function textUnderIcon( $targetURL, $text, $src )
{
	if( $targetURL == "" )
	{
		return;
	}

	$cssClass = "smalltext text_under_icon".
		( $targetURL == "" ? " text_under_icon_grayed" : "" );

	if( $targetURL != "" )
		echo '<a href="'.$targetURL.'" style="text-decoration: none">';

	echo '<span class="'.$cssClass.'">'.getIMG( $src ).'<br />'.$text.'</span>';

	if( $targetURL != "" )
		echo '</a>';
}

?>
