<?php

include_once( INCLUDES."commentreplyform.php" );

/**
 * Functions reads the POST data and adds a new comment to the database.
 */

function processCommentReply( $objid, $objType )
{
	global $_auth, $_config;

	$noEmoticons = isset( $_POST[ "commentNoEmoticons" ]) ? 1 : 0;
	$noSig = isset( $_POST[ "commentNoSig" ]) ? 1 : 0;
	$noBBCode = isset( $_POST[ "commentNoBBCode" ]) ? 1 : 0;

	$root_objid = $objid;
	$root_objType = $objType;
	$parent_objid = $objid;
	$parent_objType = $objType;

	if( $objType == "com" )
	{
		$comResult = sql_query( "SELECT `comRootObj`,`comRootObjType`,`comObj`,`comObjType` ".
			"FROM `comments`".dbWhere( array(
			"comid" => $objid )));

		if( $comData = mysql_fetch_row( $comResult ))
		{
			$root_objid = $comData[ 0 ];
			$root_objType = $comData[ 1 ];
			$parent_objid = $comData[ 2 ];
			$parent_objType = $comData[ 3 ];
		}

		mysql_free_result( $comResult );

		if( $parent_objid != $objid && $parent_objid != $root_objid )
		{
			$comResult = sql_query( "SELECT `comObj`,`comObjType` ".
				"FROM `comments`".dbWhere( array(
				"comid" => $parent_objid )));

			if( $comData = mysql_fetch_row( $comResult ))
			{
				$parent_objid = $comData[ 0 ];
				$parent_objType = $comData[ 1 ];
			}

			mysql_free_result( $comResult );
		}
	}

	// 16000 characters maximum.

	$commentText = substr( $_POST[ "comment" ], 0, 16000 );

	$userIp = getHexIp( $_SERVER[ "REMOTE_ADDR" ]);

	// Add the comment to the database.

	sql_query( "INSERT INTO `comments`".dbValues( array(
		"comObj" => $objid,
		"comObjType" => $objType,
		"comRootObj" => $root_objid,
		"comRootObjType" => $root_objType,
		"comCreator" => $_auth[ "useid" ],
		"comSubmitDate!" => "NOW()",
		"comComment" => $commentText,
		"comSubmitIp" => $userIp,
		"comEditIp" => $userIp,
		"comNoEmoticons" => $noEmoticons,
		"comNoSig" => $noSig,
		"comNoBBCode" => $noBBCode )));

	$comid = mysql_insert_id();

	// Now let's find out who created the parent object for this comment
	// (that would be a user id).

	$parentCreator = 0;

	switch( $objType )
	{
		case "obj": // objects
		{
			$collab = 0;

			$result = sql_query( "SELECT `objCreator`, `objCollab`, `objCollabConfirmed` ".
				"FROM `objects`, `objExtData`".dbWhere( array(
				"objid*" => "objEid",
				"objid" => $objid )));

			if( $data = mysql_fetch_assoc( $result ))
			{
				$parentCreator = $data[ "objCreator" ];
				$collab = $data[ "objCollabConfirmed" ] ? $data[ "objCollab" ] : 0;

				// Also gather Fan-to-Artist statistics...

				$where = array(
					"fanUser" => $_auth[ "useid" ],
					"fanArtist" => $parentCreator );
	            
				sql_where( $where );
	            
				if( sql_count( "fans" ) == 0 )
				{
					$where[ "fanNumComments" ] = 1;
					sql_values( $where );
					sql_insert( "fans" );
				}
				else
				{
					sql_where( $where );
					sql_values( array( "fanNumComments!" => "`fanNumComments` + 1" ));
					sql_update( "fans" );
				}

				if( $collab > 0 )
				{
					$where = array(
						"fanUser" => $_auth[ "useid" ],
						"fanArtist" => $collab );
		            
					sql_where( $where );
		            
					if( sql_count( "fans" ) == 0 )
					{
						$where[ "fanNumComments" ] = 1;
						sql_values( $where );
						sql_insert( "fans" );
					}
					else
					{
						sql_where( $where );
						sql_values( array( "fanNumComments!" => "`fanNumComments` + 1" ));
						sql_update( "fans" );
					}
                }
			}

			// If this submission is in user's updates, mark it as viewed
			// automatically.

			markAsRead( updTypeArt, $objid );
			updateSearchCache( $objid );

			if( $collab > 0 && $collab != $_auth[ "useid" ])
			{
				addUpdate( updTypeComment, $collab, $comid, $_auth[ "useid" ]);
			}

			break;
		}

		case "ext": // extra objects

			$result = sql_query( "SELECT `objCreator` FROM `extras` ".
				"WHERE `objid` = '$objid' LIMIT 1" );

			$parentCreator = mysql_num_rows( $result ) > 0 ?
				mysql_result( $result, 0 ) : 0;

			// If this submission is in user's updates, mark it as viewed
			// automatically.

			markAsRead( updTypeArtExtra, $objid );

			break;

		case "com": // comments

			$result = sql_query( "SELECT `comCreator` FROM `comments` ".
				"WHERE `comid` = '$objid' LIMIT 1" );

			$parentCreator = mysql_num_rows( $result ) > 0 ?
				mysql_result( $result, 0 ) : 0;

			// If this comment is in user's updates, mark it as read automatically.

			markAsRead( updTypeComment, $objid );
			break;

		case "clu": // clubs

			$result = sql_query( "SELECT `cluCreator` FROM `cluExtData` ".
				"WHERE `cluEid` = '$objid' LIMIT 1" );

			$parentCreator = mysql_num_rows( $result ) > 0 ?
				mysql_result( $result, 0 ) : 0;

			break;

		case "jou": // journals

			$result = sql_query( "SELECT `jouCreator` FROM `journals` ".
				"WHERE `jouCreatorType` = 'use' AND `jouid` = '$objid' LIMIT 1" );

			$parentCreator = mysql_num_rows( $result ) > 0 ?
				mysql_result( $result, 0 ) : 0;

			if( $parentCreator == 0 )
			{
				// Not found? Maybe that journal is actually an announcement.

				$result = sql_query( "SELECT `jouAnnCreator` FROM `journals` ".
					"WHERE `jouCreatorType` = 'clu' AND `jouid` = '$objid' LIMIT 1" );

				$parentCreator = mysql_num_rows( $result ) > 0 ? mysql_result( $result, 0 ) : 0;
			}

			// If this journal/announcement is in user's updates, mark it as read
			// automatically.

			markAsRead( updTypeJournal, $objid );
			break;

		case "pol": // polls

			$result = sql_query( "SELECT `polCreator` FROM `polls` ".
				"WHERE `polid` = '$objid' LIMIT 1" );

			$parentCreator = mysql_num_rows( $result ) > 0 ?
				mysql_result( $result, 0 ) : 0;

			// If this poll is in user's updates, mark it as read automatically.

			markAsRead( updTypeJournalPoll, $objid );
			break;

		case "new": // news

			// Those who posted news on the front page should not receive
			// comments on that news to their updates.

			$parentCreator = 0;
			break;

		case "use": // user pages

			$parentCreator = $objid;
			break;
	}

	// Notify the parent object's creator about the new comment on their creation.
	// If the creator is not the current user, of course.

	if( $parentCreator > 0 && $parentCreator != $_auth[ "useid" ])
	{
		addUpdate( updTypeComment, $parentCreator, $comid, $_auth[ "useid" ]);
	}

	// Redirect to the same page to clean up POST variables.

	$focus = "#comment".( $objType == "com" ? $objid : $comid );

	if( isset( $_POST[ "refererURL" ]))
	{
		if( preg_match( '/\/updates\//', $_POST[ "refererURL" ]))
		{
			$focus = "";
		}

		redirect( $_POST[ "refererURL" ].$focus );
	}
	else
	{
		redirect( url( ".", array( "replied" => "yes" )).$focus );
	}
}

/**
 * Shows all comments written on object $objid of type $objType.
 */

function showAllComments( $objid, $objType, $reserved = false,
	$allowReply = true, $showChilds = true, $oldestFirst = true )
{
	global $_auth, $_config, $_lang, $_lastGeneratedId;

	if( $allowReply )
	{
		// Check if the user has submitted a reply.

		if( !$_config["readOnly"] && $_auth[ "useid" ] > 0 &&
			isset( $_POST[ "sendReply" ]) && isset( $_POST[ "comment" ]) &&
			$_POST[ "comment" ] != "" )
		{
			processCommentReply( $objid, $objType );
		}

		//if( !isset( $_GET[ "replied" ]))
		{
			showReplyForm();
		}
	}

	echo '<div style="margin-top: -8px">';

	// Show child comments.

	if( !showChildComments( $objid, $objType, 0, $showChilds, true, $oldestFirst ))
	{
		echo '<div class="sep container2">'._NO_COMMENTS.'</div>';
	}

	echo '</div>';
}

function showChildComments( $comid, $comObjType, $level, $showChilds = true,
	$first = false, $oldestFirst = true )
{
	global $_cmd, $_commentsPerPage, $_config;

	if( $comObjType == "com" )
	{
		$sql = "SELECT `comCreator` FROM `comments`".dbWhere( array(
			"comid" => $comid ));

		$comResult = sql_query( $sql );

		if( !$comData = mysql_fetch_assoc( $comResult ))
			return false;

		if( isTwit( $comData[ "comCreator" ]))
			return false;

		if( !isLoggedIn() )
		{
			$result = sql_query( "SELECT `useGuestAccess` FROM `useExtData`".dbWhere( array(
				"useEid" => $comData[ "comCreator" ])));

			if( !mysql_result( $result, 0 ))
				return false;
		}
	}

	if( $level > 0 )
	{
		$offset = 0;
	}
	else
	{
		$offset = isset($_GET["offset"]) ? intval($_GET["offset"]) : 0;
	}

	$isLastOffset = ( isset( $_GET[ "offset" ]) && $_GET[ "offset" ] == "last" );

	if( $_cmd[ 0 ] != "user" || $isLastOffset )
	{
		$result = sql_query( "SELECT COUNT(*) FROM `comments` ".
			"WHERE `comObjType` = '$comObjType' AND `comObj` = '$comid'".
			getTwitWhere( "comCreator" ));

		$totalcount = mysql_result($result, 0);

		if( $isLastOffset && $_cmd[ 0 ] == "user" )
		{
			$lastOffset = floor( $totalcount / $_commentsPerPage ) * $_commentsPerPage;

			redirect( url( ".", array( "last" => $lastOffset, "offset" => $lastOffset )));
		}
	}
	else
	{
		$totalcount = $offset + $_commentsPerPage + 1;
	}

	$result = sql_query( "SELECT * FROM `comments` ".
		"WHERE `comObjType` = '$comObjType' ".
		"AND `comObj` = '$comid' ".getTwitWhere( "comCreator" ).
		"ORDER BY `comSubmitDate`".($oldestFirst ? "" : " DESC").
		" LIMIT ".$offset.",".($_commentsPerPage + 1 ));

	if(!mysql_num_rows($result))
		return false;

	$minLevel = 2; //$_cmd[0] == "view" ? 2 : 3;

	if($level >= $minLevel && $_cmd[0] != "comment")
	{
		for($i = 0; $i <= $level - 1; $i++)
		{
			?>
			<div class="<?=($level > 0 && $i < $level - 1 ? "cmt_border_left" : "") ?>">
			<?
		}
		$result1 = sql_query("SELECT `comSubmitDate` FROM `comments` WHERE `comid` = '$comid' LIMIT 1");
		$comSubmitDate = mysql_result($result1, 0);
		$readURL = url("comment/".$comid.
			( $_config["checkSubmitDate"] ? "/".preg_replace('/[^0-9]/', "", $comSubmitDate) : '' ));
		?>
		<div class="a_left smalltext" style="padding-top: 4px; padding-left: 20px">
			<a href="<?=$readURL?>#replies"><?=_READ_MORE_REPLIES ?>
			<?=getIMG(url()."images/emoticons/nav-next.png")?></a>
		</div>
		<?
		for($i = 0; $i <= $level - 1; $i++)
			echo "</div>";
		return true;
	}

	$commentsToGo = $_commentsPerPage;

	$useids = array();
	$comments = array();

	while( $comData = mysql_fetch_assoc( $result ))
	{
		$useids[] = $comData[ "comCreator" ];
		$comments[] = $comData;
	}

	prefetchUserData( array_unique( $useids ));

	foreach( $comments as $comData )
	{
		$first = false;
		showComment($comData, $level);
		if($showChilds)
			showChildComments($comData["comid"], "com", $level + 1, true, false, $oldestFirst);

		$commentsToGo--;
		if(!$commentsToGo)
			break; // do not display more than $_commentsPerPage comments
	}

	if($level == 0)
	{
		?>
		<div class="padded">
			<?
			navControls( $offset, $_commentsPerPage, $totalcount );
			?>
		</div>
		<?
	}
	return true;
}

$parentDataUseCache = array();

function getCommentParent( $comData, &$parentURL, &$parentType, &$parentTitle, $pickRoot = false )
{
	global $_config, $parentDataUseCache;

	$parentURL = "";
	$parentType = "";
	$parentData = false;

	if( $pickRoot && isset( $comData[ "comRootObj" ]))
	{
		$objid = $comData[ "comRootObj" ];
		$objType = $comData[ "comRootObjType" ];
	}
	else
	{
		$objid = $comData[ "comObj" ];
		$objType = $comData[ "comObjType" ];
	}

	switch( $objType )
	{
		case "obj": // objects
		{
			$result = sql_query( "SELECT * FROM `objects` WHERE `objid` = '".$objid."' LIMIT 1" );

			$parentData = mysql_fetch_assoc( $result );
			$parentURL = url( "view/".$objid );
			$parentType = _SUBMISSION;
			$parentTitle = $parentData[ "objTitle" ];
			break;
		}

		case "ext": // extras
		{
			$result = sql_query( "SELECT * FROM `extras` WHERE `objid` = '".$objid."' LIMIT 1" );

			$parentData = mysql_fetch_assoc( $result );
			$parentURL = url( "view/e".$objid );
			$parentType = _SUBMIT_TYPE_EXTRA;
			$parentTitle = $parentData[ "objTitle" ];
			break;
		}

		case "com": // comments
		{
			$result = sql_query( "SELECT * FROM `comments` WHERE `comid` = '".$objid."' LIMIT 1" );

			$parentData = mysql_fetch_assoc($result);
			$parentURL = url("comment/".$comData["comObj"].
				( $_config["checkSubmitDate"] ?
					"/".preg_replace('/[^0-9]/', "", $parentData["comSubmitDate"]) : '' ));
			$parentType = _COMMENT;
			$parentTitle = '#'.$parentData[ "comid" ];
			break;
		}

		case "clu": // clubs
		{
			$result = sql_query( "SELECT * FROM `clubs` WHERE `cluid` = '".$objid."' LIMIT 1" );

			$parentData = mysql_fetch_assoc( $result );
			$parentURL = url( "club/".$objid );
			$parentType = _CLUB;
			$parentTitle = $parentData[ "cluName" ];
			break;
		}

		case "jou": // journals & announcements
		{
			$result = sql_query( "SELECT * FROM `journals` ".
				"WHERE `jouid` = '".$objid."' LIMIT 1" );

			$parentData = mysql_fetch_assoc( $result );

			if( $parentData[ "jouCreatorType" ] == "clu" )
			{
				$clubid = $parentData[ "jouCreator" ];

				$parentURL = url( "announcement/".intval( $clubid )."/".$objid );

				$parentType = _ANNOUNCEMENT;
			}
			else
			{
				$username = "";

				$result = sql_query( "SELECT `useUsername` FROM `users`,`journals` ".
					"WHERE `useid` = `jouCreator` ".
					"AND `jouid` = '".$objid."' LIMIT 1" );

				if( mysql_num_rows( $result ) > 0 )
				{
					$username = mysql_result( $result, 0 );
				}

				$parentURL = url( "journal/".strtolower( $username )."/".$objid );

				$parentType = _JOURNAL;
			}

			$parentTitle = $parentData[ "jouTitle" ];

			break;
		}

		case "pol": // polls
		{
			$username = "";
			$result = sql_query( "SELECT `useUsername` FROM `users`,`polls` ".
				"WHERE `useid` = `polCreator` AND `polid` = '".$objid."' LIMIT 1" );

			if( mysql_num_rows( $result ))
			{
				$username = mysql_result( $result, 0 );
			}

			$result = sql_query( "SELECT * FROM `polls` ".
				"WHERE `polid` = '".$objid."' LIMIT 1" );

			$parentData = mysql_fetch_assoc( $result );
			$parentURL = url( "poll/".strtolower( $username )."/".$objid );
			$parentType = _POLL;
			$parentTitle = $parentData[ "polSubject" ];
			break;
		}

		case "new": // news
		{
			$result = sql_query( "SELECT * FROM `news` WHERE `newid` = '".$objid."' LIMIT 1" );

			$parentData = mysql_fetch_assoc( $result );
			$parentURL = url( "news/".$objid );
			$parentType = _NEWS;
			$parentTitle = $parentData[ "newSubject" ];
			break;
		}

		case "use": // user pages
		{
			if( isset( $parentDataUseCache[ $objid]))
				$parentData = $parentDataUseCache[ $objid];
			else
			{
				$result = sql_query( "SELECT * FROM `users` ".
					"WHERE `useid` = '".$objid."' LIMIT 1" );

				$parentData = mysql_fetch_assoc( $result );
				$parentDataUseCache[ $objid] = $parentData;
			}

			$parentURL = url( "user/".strtolower( $parentData[ "useUsername" ]));
			$parentType = _USER;
			$parentTitle = $parentData[ "useUsername" ];
			break;
		}
	}
}

$_signatureCache = array();

function showComment($comData, $level, $showParent = false, $markAsReadId = '', $commentCountId = '',
	$markAsReadId2 = '')
{
	global $_config, $_signatureCache, $_auth, $_cmd, $_currentPageURL;

	// _HACKFIX - facsimilnym 2011-09-05
	// Workaround for the "blank" useDateFormat that ~140,000 users have.
	// Setting it up here, since there's like 5 code paths.
	if(!isset($_auth["useDateFormat"]) || $_auth["useDateFormat"] == ""){
		$_auth["useDateFormat"] = "Y-m-d \@ g:i A";
	}

	if( isset( $comData[ "pmsid" ]))
	{
		$comData[ "comid" ] = $comData[ "pmsid" ];
		$comData[ "comCreator" ] = $comData[ "pmsCreator" ];
		$comData[ "comNoEmoticons" ] = $comData[ "pmsNoEmoticons" ];
		$comData[ "comNoSig" ] = $comData[ "pmsNoSig" ];
		$comData[ "comNoBBCode" ] = $comData[ "pmsNoBBCode" ];
		$comData[ "comSubject" ] = $comData[ "pmsTitle" ];
		$comData[ "comSubjectDateHide" ] = true;
		$comData[ "comComment" ] = $comData[ "pmsComment" ];
		$comData[ "comObj" ] = $comData[ "pmsObj" ];
		$comData[ "comObjType" ] = "pms";
		$comData[ "comSubmitDate" ] = $comData[ "pmsSubmitDate" ];
		$comData[ "comSubmitIp" ] = $comData[ "pmsSubmitIp" ];
		$comData[ "comLastEdit" ] = $comData[ "pmsLastEdit" ];
		$comData[ "comEditIp" ] = $comData[ "pmsEditIp" ];
		$comData[ "comTotalEdits" ] = $comData[ "pmsTotalEdits" ];
		$comData[ "comPmUser" ] = $comData[ "pmsPmUser" ];
	}

	if( isTwit( $comData[ "comCreator" ]))
		return;

	if( !isLoggedIn() )
	{
		$result = sql_query( "SELECT `useGuestAccess` FROM `useExtData`".dbWhere( array(
			"useEid" => $comData[ "comCreator" ])));

		if( !mysql_result( $result, 0 ))
		{
			return;
		}
	}

	$commentID = "yg-cmt".$comData[ "comid" ];

	$instantReplyOuterId = $commentID."InstantReplyOuter";
	$instantReplyId = $commentID."InstantReplyId";

	if( !isset( $comData[ "comNoEmoticons" ]))
	{
		$comData[ "comNoEmoticons" ] = false;
	}

	if( !isset( $comData[ "comNoSig" ]))
	{
		$comData[ "comNoSig" ] = false;
	}

	if( !isset( $comData[ "comNoBBCode"]))
	{
		$comData[ "comNoBBCode" ] = false;
	}

	if( isset( $comData[ "pmsid" ]))
	{
		if( $comData[ "comCreator" ] != $_auth[ "useid" ] &&
			$comData[ "comPmUser" ] != $_auth[ "useid" ])
		{
			echo '<div class="container2">'._PM_NO_ACCESS.'</div>';
			return;
		}
	}

	$levellim = $level < 8 ? $level : 8;

	for($i = 0; $i <= $levellim; $i++)
	{
		?>
		<div class="<?
			echo $i == $levellim ? "pad_top" : "";
			echo $level > 0 && $i < $levellim ? "cmt_border_left" : "";
			?>">
		<?
	}

//	if($_cmd[0] != "comment")
		echo '<a name="comment'.$comData["comid"].'"></a>';

//	echo '<div'.($markAsReadId != "" ? ' id="'.$markAsReadId.'"' : "" ).'>'; // Comment outer

//	echo '<div'.($markAsReadId != "" ? ' id="'.$markAsReadId.'_"' : "" ).' class="container2">'; // Comment body

	echo '<div class="container2">'; // Comment body

	iefixStart();

	// display parent comment

	if( $showParent && isset( $comData[ "comObjType" ]) && $comData["comObjType"] == "com" )
	{
		$result = sql_query( "SELECT * FROM `comments` WHERE `comid` = '".$comData[ "comObj" ]."' LIMIT 1" );

		if( $parData = mysql_fetch_assoc( $result ))
		{
			?>
			<div class="caption reminder_cap">
				<?= sprintf( _WROTE, getUserLink( $parData[ "comCreator" ])) ?>:
			</div>
			<div class="reminder">
				<?= formatText( $parData[ "comComment" ], $parData[ "comNoEmoticons" ],
					$parData[ "comNoBBCode" ]); ?>
			</div>
			<?
		}
	}
	elseif( $showParent && isset( $comData[ "pmsid" ]))
	{
		$result = sql_query( "SELECT * FROM `pms` WHERE `pmsid` = '".$comData[ "comObj" ]."' LIMIT 1" );

		if( $parData = mysql_fetch_assoc( $result ))
		{
			if( $parData[ "pmsCreator"] == $_auth[ "useid" ] || $parData[ "pmsPmUser"] == $_auth[ "useid" ])
			{
				?>
				<div class="caption reminder_cap">
					<?= sprintf( _WROTE, getUserLink( $parData[ "pmsCreator" ])) ?>:
				</div>
				<div class="reminder">
					<?= formatText( $parData[ "pmsComment" ], $parData[ "pmsNoEmoticons" ], $parData[ "pmsNoBBCode" ]); ?>
				</div>
				<?
			}
		}
	}

	if( $comData[ "comCreator" ] != 0 )
	{
		?>
		<div class="f_right mar_left a_center" style="position: relative; top: -3px; left: 3px">
			<?=getUserAvatar("", $comData["comCreator"], true); ?>
		</div>
		<?
	}

	if(isset($comData["comSubject"]))
	{
		if(!isset($comData["comSubjectDateHide"]))
		{
			?><div class="smalltext mar_bottom"><?

			// Show IPs to moderators+ (as an acronym).

			if( atLeastModerator() && isset( $comData[ "comSubmitIp" ]) &&
				isset( $comData[ "comEditIp" ]))
			{
				?>
				<acronym style="cursor: help" title="Submitted from <?= getDotDecIp( $comData[ "comSubmitIp" ]) ?>, last edited from <?= getDotDecIp( $comData[ "comEditIp" ]) ?>">
				<?
			}


			printf(_COMMENT_POSTED_ON, gmdate( $_auth["useDateFormat"] /* "Y-m-d \@ g:i A"*/,
				applyTimezone(strtotime($comData["comSubmitDate"]))));

			// Close the acronym opened above.

			if( atLeastModerator() && isset( $comData[ "comSubmitIp" ]) &&
				isset( $comData[ "comEditIp" ]))
			{
				?>
				</acronym>
				<?
			}

			?></div><?
		}
		?>
		<div class="largetext mar_bottom"><b><?=formatText($comData["comSubject"], false, true) ?></b></div>
		<?
	}

	?>
	<div class="commentData">
		<?
		$allowImages = isset($comData["comAllowImages"]) && $comData["comAllowImages"];
		echo formatText($comData["comComment"], $comData["comNoEmoticons"], $comData["comNoBBCode"],
			$allowImages);
		?>
	</div>
	<?

	if(!$comData["comNoSig"] && $_cmd[0] != "updates")
	{
		if(!isset($_signatureCache[$comData["comCreator"]]))
		{
			$result = sql_query("SELECT `useSignature` FROM `useExtData` WHERE `useEid` = '".$comData["comCreator"]."' LIMIT 1");

			if(mysql_num_rows($result))
				$_signatureCache[$comData["comCreator"]] = mysql_result($result, 0);
			else
				$_signatureCache[$comData["comCreator"]] = "";
		}

		if($_signatureCache[$comData["comCreator"]] != "")
		{
			?>
			<div class="hline" style="margin-right: 60px;">&nbsp;</div>
			<div class="smalltext">
			<?=formatText($_signatureCache[$comData["comCreator"]]); ?>
			</div>
			<?
		}
	}

	?>
	<div class="clear">&nbsp;</div>
	<?

	if( isset( $comData[ "newid" ]))
	{
		// News page.

		$result = sql_query( "SELECT COUNT(*) FROM `comments` ".
			"WHERE `comObjType` = 'new' AND `comObj` = '".$comData[ "newid" ]."'" );

		$newsCount = mysql_result( $result, 0 );

		$newsURL = url( "news/".$comData[ "newid" ]);

		?>
		<div class="sep">
			<a href="<?= $newsURL ?>">
			<span class="button smalltext" style="float: right"
				onclick="document.location='<?= $newsURL ?>';"><?= _COMMENTS ?>
				<?

				if( !$_auth[ "useStatsHide" ])
				{
					echo "(".fuzzy_number( $newsCount ).")";
				}

				?>
			</span></a>
		</div>
		<?
	}

	if( $comData[ "comid" ])
	{
		?>
		<div class="sep commentActions">
			<?

			getCommentParent( $comData, $parentURL, $parentType, $parentTitle, false );
			getCommentParent( $comData, $rootURL, $rootType, $rootTitle, true );

			$replyURL = url( "comment/".$comData[ "comid" ].
				( $_config[ "checkSubmitDate" ] ? "/".
				preg_replace( '/[^0-9]/', "", $comData[ "comSubmitDate" ]) : '' ));

			$replyCommentId = $commentID."ReplyCommentId";
			$replyButtonId = $commentID."ReplyButtonId";

			$replyScript =
				"var elin = get_by_id( '".$instantReplyId."' );".
				"var elb = get_by_id( '".$replyButtonId."' );".
				"if( !elb ) return false;".
				"if( elb.innerHTML == '"._CANCEL."' ) {".
				"  reset_focus(); ".
				"  collapseAnim( '$instantReplyOuterId', -30, 0, '' ); ".
				"  elb.innerHTML = '"._REPLY."';".
				"  return false;".
				"}".
				"_IR = { ".
				"commentId: '".$replyCommentId."',".
				"commentName: 'comment',".
				"commentDefault: '',".
				"commentWide: true,".
				"commentRows: 7,".
				"commentNoBBCode: ".( isLoggedIn() && $_auth[ "useNoBBCode" ] ? "true" : "false" ).",".
				"commentNoEmoticons: ".( isLoggedIn() && $_auth[ "useNoEmoticons" ] ? "true" : "false" ).",".
				"commentNoOptions: false,".
				"commentNoSig: ".( isLoggedIn() && $_auth[ "useNoSig" ] ? "true" : "false" ).",".
				"emoticonPopupURL: '".url( "emoticons",
					array( "popup" => "yes", "comment" => $replyCommentId ))."',".
				"previewURL: '".url( "preview", array( "popup" => "yes" ))."'".
				"};".
				"var ht = showReplyForm( '".$replyURL."', '".$_currentPageURL."' );".
				"elin.innerHTML = ht;".
				"collapseAnim( '$instantReplyOuterId', 30, 0, 'set_focus( \'' + _IR.commentId + '\' );' ); ".
				"elb.innerHTML = '"._CANCEL."';".
				"return false;";

			if( !isset( $comData[ "pmsid" ]) && $replyURL != url( "." ) && $_cmd[ 0 ] != "editcomment" )
			{
				?>
				<span class="button smalltext" style="float: right"
					id="<?= $replyButtonId ?>" onclick="<?= $replyScript ?>">
					<?=_REPLY ?>
				</span>
				<?
			}

			if($_cmd[0] == "comment" && $rootURL != url(".") && $level == 0)
			{
				?>
				<a href="<?=$rootURL ?>" title="<?= htmlspecialchars( $rootTitle ) ?>">
				<span class="button smalltext" style="float: right"
					onclick="document.location='<?=$rootURL ?>';">
					<?=_ROOT ?> (<?=$rootType ?>)
				</span></a>
				<?
			}

			if($_cmd[0] == "comment" && $parentURL != url(".") && $level == 0 && $parentURL != $rootURL )
			{
				?>
				<a href="<?=$parentURL ?>" title="<?= htmlspecialchars( $parentTitle ) ?>">
				<span class="button smalltext" style="float: right"
					onclick="document.location='<?=$parentURL ?>';">
					<?=_PARENT ?> (<?=$parentType ?>)
				</span></a>
				<?
			}

			if($_cmd[0] == "updates")
			{
				$comid = $comData[ "comid" ];

				$script = "add_operation( 'm$comid' );\n".
					"$('$markAsReadId').hide(); ".
					( $markAsReadId2 != "" ? "if() $('$markAsReadId2').hide(); " : "" ).
					"var elb = $( '$replyButtonId' );".
					"if( !elb ) return false;".
					"if( elb.innerHTML == '"._CANCEL."' ) {".
					"  reset_focus(); ".
					"  collapseAnim( '$instantReplyOuterId', -30, 0, '' ); ".
					"  elb.innerHTML = '"._REPLY."';".
					"  return false;".
					"}".
					"comment_count--; ".
					"$('_updCmtCnt').innerHTML = fuzzy_number( comment_count ); ".
					"$('_globCmtCnt').innerHTML = fuzzy_number( comment_count );";

				?>
				<div id="<?=$id1=$commentID."Updates1" ?>" class="button smalltext" style="display: none; float: right"
					onclick="<?= $script ?>"><?=_MARK_AS_READ?>: <span class="error"><?=_CLICK_TO_CONFIRM ?></span></div>
				<div id="<?=$id2=$commentID."Updates2" ?>" class="button smalltext" style="float: right"
					onclick="$('<?=$id1?>').show(); $('<?=$id2?>').hide();"><?=_MARK_AS_READ ?></div>
				<?
			}

			if(( atLeastSModerator() || $comData["comCreator"] == $_auth["useid"]) &&
				$_cmd[ 0 ] != "editcomment" && $_cmd[ 0 ] != "updates" )
			{
				$editURL = isset( $comData[ "pmsid" ]) ?
					url( "editpm/".$comData[ "pmsid" ]) :
					url( "editcomment/".$comData[ "comid" ]);

				?>
				<a href="<?=$editURL ?>">
				<span class="button smalltext" style="float: right"
					onclick="document.location='<?=$editURL ?>';">
					<?=_EDIT ?>
				</span></a>
				<?
			}

			?>
			<div class="f_left mar_top smalltext">
				<?

				// Show IPs to moderators+ (as an acronym).

				if( atLeastModerator() )
				{
					?>
					<acronym style="cursor: help" title="Submitted from <?= getDotDecIp( $comData[ "comSubmitIp" ]) ?>, last edited from <?= getDotDecIp( $comData[ "comEditIp" ]) ?>">
					<?
				}

				if( !isset( $comData[ "pmsid" ]))
				{
					?>
					<a class="originalUrl" href="<?= $replyURL ?>">#</a>
					<?
				}

				printf( _COMMENT_POSTED_ON, gmdate( $_auth[ "useDateFormat" ] /*"Y-m-d \@ g:i A"*/,
					applyTimezone( strtotime( $comData[ "comSubmitDate" ]))));

				if( isset( $comData[ "comTotalEdits" ]) && $comData[ "comTotalEdits" ] > 0)
				{
					echo "<br />".sprintf( _COMMENT_EDITED, $comData[ "comTotalEdits" ],
						gmdate( $_auth[ "useDateFormat" ] /*"Y-m-d \@ g:i A"*/,
						applyTimezone( strtotime( $comData[ "comLastEdit" ]))));
				}

				// Close the acronym opened above.

				if( atLeastModerator() )
				{
					?>
					</acronym>
					<?
				}

				?>
			</div>
		</div>
		<?
	}
	?>
	<div class="clear">&nbsp;</div>
	<?
	iefixEnd();

	echo '</div>'; // Comment body
	//echo '</div>'; // Comment outer

	?>
	<table cellspacing="0" cellpadding="0" width="100%"><tr><td>
	<div id="<?= $instantReplyOuterId ?>" style="display: none">
		<div class="cmt_border_left" style="padding: 4px; padding-right: 0;" id="<?= $instantReplyOuterId ?>_">
			<div class="caption smalltext"><?= _REPLY ?>:</div>
			<div style="padding: 1px; padding-bottom: 5px" id="<?= $instantReplyId ?>"><br /></div>
		</div>
	</div>
	</td></tr></table>
	<?

	for( $i = 0; $i <= $levellim; $i++ )
		echo "</div>";
}

$twitCache = array();

function isTwit( $useid )
{
	global $_auth, $twitCache;

	if( !isLoggedIn() )
	{
		return( false );
	}

	if( isset( $twitCache[ $useid ]))
	{
		return( $twitCache[ $useid ]);
	}

	$twtResult = sql_query( "SELECT COUNT(*) FROM `twitList`".dbWhere( array(
		"twtCreator" => $_auth[ "useid" ],
		"twtBadUser" => $useid )));

	$isTwit = ( mysql_result( $twtResult, 0 ) > 0 );

	mysql_free_result( $twtResult );

	$twitCache[ $useid ] = $isTwit;

	return( $isTwit );
}

$twitWhereCache = false;

function getTwitWhere( $fieldName )
{
	global $_auth, $twitWhereCache;

	if( !isLoggedIn() )
	{
		return( "" );
	}

	if( $twitWhereCache !== false )
	{
		$twits = $twitWhereCache;
	}
	else
	{
		$twtResult = sql_query( "SELECT `twtBadUser` FROM `twitList`".dbWhere( array(
			"twtCreator" => $_auth[ "useid" ])));

		$twits = array();

		while( $twtData = mysql_fetch_assoc( $twtResult ))
		{
			$twits[] = $twtData[ "twtBadUser" ];
		}

		mysql_free_result( $twtResult );

		$twitWhereCache = $twits;
	}

	if( count( $twits ) == 0 )
	{
		return( "" );
	}
	else
	{
		return( " AND `$fieldName` NOT IN('".implode( "','", $twits )."') " );
	}
}

?>
