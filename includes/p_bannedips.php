<?php

if( !atLeastModerator() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}
ini_set('display_errors', 1);
error_reporting(E_ALL);

?>
<div class="header">
	<div class="header_title">
		<?= _ADMINISTRATION ?>
		<div class="subheader">Banned IPs</div>
	</div>
	<?php

	$active = 6;
	include(INCLUDES."mod_adminmenu.php");

	$_documentTitle = "Banned IPs";

	?>
</div>

<div class="container">
	<h3>All IPs are sorted from newest to oldest.</h3>
	<h4>The newest IP/user to be banned will always be at the top of the list.</h4>
	<h4>For convenience, the date each username was registered has been placed beside them.</h4>
	<b>Bold</b> = banned accounts.<br />
	<i>Italic</i> = accounts currently suspended.<br /><br />
<?php

//$result = sql_query( "SELECT `useIsBanned`, `useUsername`, `useLastIp`, `useSuspendedUntil` ".
//	"FROM `users`, `useExtData` ".
//	"WHERE `useid` = `useEid` ORDER BY `useLastIp` DESC" );

$result = sql_query("select useIsBanned, useid, useSignupDate, useUsername, useLastIp, useSuspendedUntil from users
					join useExtData on useid = useEid
					WHERE useIsBanned = '1' OR useIsSuspended = '1'
					order by useEid DESC");

$ips = array();

while( $useData = mysql_fetch_assoc( $result ))
{
	$ip = $useData[ "useLastIp" ];
	$banned = $useData[ "useIsBanned" ];
	$suspended = ( strtotime( $useData[ "useSuspendedUntil" ]) - time() ) > 0;

	if( $ip == "" )
		continue;

	if( !isset( $ips[ $ip ]))
	{
		$ips[ $ip ][ "count" ] = 0;
		$ips[ $ip ][ "users" ] = "";
		$ips[ $ip ][ "banned" ] = false;
		$ips[ $ip ][ "non-banned" ] = false;
	}

	$ips[ $ip ][ "count" ]++;

	if( $banned )
	{
		$ips[ $ip ][ "users" ] .= "[b]";
		$ips[ $ip ][ "banned" ] = true;
	}

	if( $suspended )
	{
		$ips[ $ip ][ "users" ] .= "[i]";
		$ips[ $ip ][ "banned" ] = true;
	}

	if( !$banned && !$suspended )
	{
		$ips[ $ip ][ "non-banned" ] = true;
	}

	$ips[ $ip ][ "users" ] .= "[u=".$useData[ "useUsername" ]."] [i] (".str_replace(":",";",$useData[ "useSignupDate" ]).")[/i] [br][/br] ";

	if( $suspended )
		$ips[ $ip ][ "users" ] .= "[/i]";

	if( $banned )
		$ips[ $ip ][ "users" ] .= "[/b] ";
}

?>

<div class="sep largetext">Fully Banned or Suspended</div>
<table cellspacing="0" cellpadding="4" border="1px solid white">
<?

$c = 0;

foreach( $ips as $ip => $count )
{
	if( $count[ "banned" ] && !$count[ "non-banned" ])
	{
		?>
		<tr>
		<td><?= $c + 1 ?></td>
		<td><?= getDotDecIp( $ip ) ?></td>
		<td>(<?= $count[ "count" ] ?>)</td>
		<td><?= formatText( $count[ "users" ]) ?></td>
		</tr>
		<?

		$c++;

	}
}

?>
</table>
</div>
