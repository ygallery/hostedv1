<div class="header">
	<div class="header_title">
		<?=_JOIN ?>
		<div class="subheader"><?=_CREATE_ACCOUNT ?></div>
	</div>
</div>
<div class="container">
<?
$_documentTitle = _JOIN;

if($_config["readOnly"]) {
	notice(_READONLY);
	echo '</div>';
	return;
}
if($_auth["useid"]) {
	notice(_ALREADY_LOGGED);
	echo '</div>';
	return;
}

$result = sql_query( "SELECT `useid` FROM `users`, `useExtData` WHERE `useid` = `useEid` ".
	"AND `useLastIp` = '".getHexIp( $_SERVER[ "REMOTE_ADDR" ])."' ".
	"AND ( `useIsBanned` = '1' OR `useSuspendedUntil` > NOW() ) ".
	"LIMIT 1" );

if( mysql_num_rows( $result ) > 0 ) $banned = 1; // does not prevent from viewing the ToS

mysql_free_result( $result );

if(!isset($_POST["ReadTOS"])) {
	?>
	<div class="header"><?=_TOS ?></div>
	<div>
		<?

		$filename = INCLUDES."strings/".$_lang."_tos.php";

		if( !file_exists( $filename ))
			$filename = INCLUDES."strings/en_tos.php";

		include($filename);

		?>
	</div>
	<div class="sep a_center mar_bottom">
		<?
		if(isset($banned)) // prevents from starting the form
		{
			notice( _JOIN_BANNED_IP );
		}
		else
		{
		?>
		<form action="<?=url(".")?>" method="post">
			<input type="hidden" name="ReadTOS" value="yes" />
			<input type="submit" class="submit"
				value="<?=_JOIN_UNDERSTOOD_TOS ?>" />
		</form>
		<?
		}
		?>
	</div>
	</div><!-- container -->
	<?
	return;
}
if(isset($banned)) // prevents from viewing or submitting the form
{
	notice( _JOIN_BANNED_IP );
	echo '</div><!-- container -->';
	return;
}
if(isset($_POST["submit"])) {
	$validated = true;
	$username = addslashes($_POST["joinusername"]);
	$password = $_POST["joinpassword"];
	$password2 = $_POST["joinpassword2"];
	$realname = addslashes($_POST["realname"]);
	$showrealname = isset($_POST["showrealname"]) ? 1 : 0;
	$email = trim(addslashes($_POST["email"]));
	$showemail = isset($_POST["showemail"]) ? 1 : 0;
	$birthdaymonth = intval($_POST["birthdaymonth"]);
	$birthdayday = intval($_POST["birthdayday"]);
	$birthdayyear = intval($_POST["birthdayyear"]);
	$showbirthday = isset($_POST["showemail"]) ? 1 : 0;
	$invitation = isset($_POST["invitation"]) ? $_POST["invitation"] : 0;
	$invitedBy = 0;
	if(!$username) {
		$validated = false; notice(_BLANK_USERNAME);
	}
	elseif(strlen($username) < $_config["minUsernameLength"]) {
		$validated = false;
		notice(sprintf(_JOIN_USERNAME_TOO_SHORT, $_config["minUsernameLength"]));
	}
	elseif(strlen($username) > $_config["maxUsernameLength"]) {
		$validated = false;
		notice(sprintf(_JOIN_USERNAME_TOO_LONG, $_config["maxUsernameLength"]));
	}
	elseif($username != preg_replace('/[^a-zA-Z0-9]/', '', $username)) {
		$validated = false; notice(_JOIN_USERNAME_ALPHANUMERIC);
	}
	elseif(mysql_num_rows(sql_query(
		"SELECT `useid` FROM `users` WHERE `useUsername` = '".$username."'"))) {
		$validated = false; notice(sprintf(_JOIN_USERNAME_EXISTS,$username));
	}
	if (!$password) {
		$validated = false; notice(_BLANK_PASSWORD);
	}
	elseif(strlen ($password) < $_config["minPasswordLength"]) {
		$validated = false; notice(sprintf(_JOIN_PASSWORD_TOO_SHORT,$_config["minPasswordLength"]));
	}
	elseif((!$password2) || ($password != $password2)) {
		$validated = false; notice(_JOIN_PASSWORD_MISMATCH);
	}
	include_once(INCLUDES."mailing.php");
	if(!checkEmail($email)) {
		$validated = false; notice(_JOIN_EMAIL_INVALID);
	}
	else {
		$result = sql_query("SELECT COUNT(*) FROM `useExtData` WHERE `useEmail` = '".addslashes($email)."'");
		if( mysql_result($result, 0) > 0 ) {
			$validated = false; notice(sprintf(_JOIN_EMAIL_EXISTS,$email));
		}
	}
	
	if(preg_match('/reinf3ction\.com$/i', $email))
	{
		$validated = false;
		mail('gallery@y-hosting.net', 'Testament..', $_SERVER['REMOTE_ADDR'], '', "-f".$_config[ "returnPathEmail" ]);
		header('Location: http://goatse.ragingfist.net/');
		die();
	}
	if(preg_match('/mailinator\.com$/i', $email) 
		|| preg_match('/mailinator2\.com$/i', $email) 
		|| preg_match('/sogetthis\.com$/i', $email)
		|| preg_match('/mailin8r\.com$/i', $email)
		|| preg_match('/mailinator\.net$/i', $email)
		|| preg_match('/spamherelots\.com$/i', $email)
		|| preg_match('/thisisnotmyrealemail\.com$/i', $email)
		|| preg_match('/dodgit\.com$/i', $email)
		|| preg_match('/dodgeit\.com$/i', $email)
		|| preg_match('/bsnow\.com$/i', $email)
		|| preg_match('/mt2010\.com$/i', $email)
		|| preg_match('/mt2009\.com$/i', $email)
		|| preg_match('/trashymail\.com$/i', $email)
		|| preg_match('/mytrashmail\.com$/i', $email)
		|| preg_match('/yopmail\.com$/i', $email)
		|| preg_match('/justonemail\.com$/i', $email)
		|| preg_match('/letmymail\.com$/i', $email)
		|| preg_match('/onemoremail\.net$/i', $email)
		|| preg_match('/yopmail\.net$/i', $email)
		|| preg_match('/yopmail\.fr$/i', $email)
		|| preg_match('/cool\.fr\.nf$/i', $email)
		|| preg_match('/jetable\.fr\.nf$/i', $email)
		|| preg_match('/nospam\.ze\.tc$/i', $email)
		|| preg_match('/nomail\.xl\.cx$/i', $email)
		|| preg_match('/mega\.zik\.dj$/i', $email)
		|| preg_match('/speed\.ls\.fr$/i', $email)
		|| preg_match('/courriel\.fr\.nf$/i', $email)		
		|| preg_match('/moncourrier\.fr\.nf$/i', $email)	
		|| preg_match('/monemail\.fr\.nf$/i', $email)	
		|| preg_match('/monmail\.fr\.nf$/i', $email)	
		|| preg_match('/spam\.la$/i', $email)
		|| preg_match('/bsnow\.net$/i', $email)	
		|| preg_match('/guerrillamailblock\.com$/i', $email)	
		|| preg_match('/spambox\.us$/i', $email)	
		|| preg_match('/maileater\.com$/i', $email)
		)					
	{
		$validated = false; notice(_JOIN_EMAIL_DISALLOWED);
	}
	
	if(!checkdate($birthdaymonth, $birthdayday, $birthdayyear)) {
		$validated = false; notice(_JOIN_BIRTHDATE_INVALID);
	}
	elseif($_config["disableUnder18"]) {
		$birthday = strtotime("$birthdayyear-$birthdaymonth-$birthdayday");
		$age = floor((time() - $birthday) / (60*60*24*365));
		if($age < 18) {
			$validated = false; notice(_JOIN_BIRTHDATE_TOO_YOUNG);
		}
	}
	if( $_config[ "invitations" ])
	{
		$result = sql_query( "SELECT `useEid` FROM `useExtData`".dbWhere( array(
			"useInvitation" => $invitation )));

		if( $data = mysql_fetch_row( $result ))
		{
			$invitedBy = $data[ 0 ];
		}
		else
		{
			$validated = false;
			notice( _JOIN_INVITATION_INVALID );
		}
	}

	if( !$validated )
	{
		notice( _JOIN_ACCOUNT_NOT_CREATED );
	}
	else
	{
		if( $_config[ "invitations" ] && $invitedBy > 0 )
		{
			// Reset inviter's invitation code.

			sql_query( "UPDATE `useExtData`".dbSet( array(
				"useInvitation" => "" )).dbWhere( array(
				"useEid" => $invitedBy )));
		}

		// Add user to the database.

		sql_query( "INSERT INTO `users`(`useUsername`,`usePassword`,`useRealName`,`useShowRealName`) ".
			"VALUES('$username','".SHA1( $password )."','$realname','$showrealname')" );

		$useid = mysql_insert_id(); // Get newly created user id.

		// Generate activation key.

		// Generate a random activation key.

		$activationKey = sha1( mt_rand() * ( microtime() * 0.001 ));

		// Add extra user data to the database.

		sql_query( "INSERT INTO `useExtData`(`useEid`,`useEmail`,`useShowEmail`,`useBirthday`,`useShowBirthday`,`useIsActive`,`useActivationKey`,`useSignupDate`,`useLanguage`,`useObjFilters`,`useObjPreview`,`useInvitedBy`) ".
			"VALUES('$useid','$email','$showemail','$birthdayyear-$birthdaymonth-$birthdayday','$showbirthday','0','$activationKey',NOW(),'".$_lang."','".$_config["defaultFilters"]."','".$_config["previewDefault"]."','$invitedBy')" );

		// Send the activation key via email.

		include_once( INCLUDES."mailing.php" );

		$activationLink = url( "activate/".$activationKey );

		sendEmail( $useid, sprintf( _USE_ACTIVATE_SUBJ, $_config[ "galName" ]),
			sprintf( _USE_ACTIVATE_BODY, $activationLink ));

		// New account has been successfully created, proceed to the front page
		// this should actually go to a confirmation page which tells the user to check their email.

		redirect( url( "user/".strtolower( $username )));
	}
}
?>
<form action="<?=url('join')?>" method="post">
	<input type="hidden" name="ReadTOS" value="yes" />
	<div class="caption"><?=_USERNAME ?>:</div>
	<div><input class="narrow" name="joinusername" type="text"
		<?= isset ($_POST ['joinusername']) ? 'value="'.htmlspecialchars($_POST["joinusername"]).'"' : '' ?> />
		&nbsp; (<?=_JOIN_USERNAME_EXPLAIN ?>)
	</div>

	<div class="sep caption"><?=_PASSWORD ?>:</div>
	<div><input class="narrow" name="joinpassword" type="password" />
		&nbsp; (<? printf(_JOIN_PASSWORD_EXPLAIN,$_config["minPasswordLength"]); ?>)
	</div>

	<div class="sep caption"><?=_CONFIRM ?>:</div>
	<div><input class="narrow" name="joinpassword2" type="password" />
		&nbsp; (<?=_JOIN_PASSWORD_CONFIRM_EXPLAIN ?>)
	</div>

	<div class="sep caption"><?=_REAL_NAME ?>:</div>
	<div>
		<input class="notsowide" name="realname" type="text"
			<?= isset ($_POST ['realname']) ? 'value="'.htmlspecialchars($_POST["realname"]).'"' : '' ?> />
		<input class="checkbox" id="joinShowRealName" name="showrealname" type="checkbox"
			<?= isset ($_POST ['showrealname']) ? 'checked="checked"' : '' ?> />
			<label for="joinShowRealName"><?=_SHOW ?></label>
	</div>

	<div class="sep caption"><?=_EMAIL_ADDRESS ?>:</div>
	<div>
		<input class="notsowide" name="email" type="text"
			<?= isset ($_POST ['email']) ? 'value="'.htmlspecialchars($_POST["email"]).'"' : '' ?> />
		<input class="checkbox" id="joinShowEmail" name="showemail" type="checkbox"
			<?= isset ($_POST ['showemail']) ? 'checked="checked"' : '' ?> />
			<label for="joinShowEmail"><?=_SHOW ?></label>
	</div>

	<div class="sep caption"><?=_BIRTHDAY ?>:</div>
	<div class="container2">
		<select name="birthdaymonth">
			<option value="1"><?=_MONTH1 ?></option>
			<option value="2"><?=_MONTH2 ?></option>
			<option value="3"><?=_MONTH3 ?></option>
			<option value="4"><?=_MONTH4 ?></option>
			<option value="5"><?=_MONTH5 ?></option>
			<option value="6"><?=_MONTH6 ?></option>
			<option value="7"><?=_MONTH7 ?></option>
			<option value="8"><?=_MONTH8 ?></option>
			<option value="9"><?=_MONTH9 ?></option>
			<option value="10"><?=_MONTH10 ?></option>
			<option value="11"><?=_MONTH11 ?></option>
			<option value="12"><?=_MONTH12 ?></option>
		</select>

		<select name="birthdayday">
			<?
			for($i = 1; $i <= 31; $i++)
				echo '<option value="'.$i.'">'.$i.'</option>';
			?>
		</select>

		<select name="birthdayyear">
			<?
			$y = strval(date('Y', time()));
			for($i = $y - 90; $i <= $y; $i++)
				echo '<option '.($i == $y ? 'selected ' : '').'value="'.$i.'">'.$i.'</option>';
			?>
		</select>

		<input class="checkbox" id="joinShowBirthday" name="showbirthday" type="checkbox"
			<?= isset($_POST ['showbirthday']) ? 'checked="checked"' : '' ?> />
			<label for="joinShowBirthday"><?=_SHOW ?></label>
	</div>
	<?

	if( $_config[ "invitations" ])
	{
		?>
		<div class="sep caption"><?=_INVITATION_CODE ?>:</div>
		<div>
			<input class="narrow" name="invitation" type="text"
				<?= isset ($_POST ['invitation']) ? 'value="'.htmlspecialchars($_POST["invitation"]).'"' : '' ?> />
			&nbsp; (<?= _JOIN_INVITATION_EXPLAIN ?>)
		</div>
		<?
	}

	?>
	<div class="sep">
		<button class="submit" name="submit" type="submit">
			<?=getIMG(url()."images/emoticons/checked.png") ?>
			<?=_JOIN_CREATE_ACCOUNT ?>
		</button>
	</div>
</form>

</div><!-- container -->
