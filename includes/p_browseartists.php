<?
	$_documentTitle = _BROWSE.": "._ARTISTS;
?>

<div class="header">
	<div class="header_title">
		<?=_BROWSE ?>
		<div class="subheader"><?=_ARTISTS ?></div>
	</div>
	<?
	$active = 2;
	include(INCLUDES."mod_browsemenu.php");
	?>
</div>

<div class="container">
<?

$where = "`useObjCount` > '0'";
$enableCategories = true;

if(!isset($_GET["order"])) $_GET["order"] = 0;

if(isset($_GET["order"])) {
	switch(intval($_GET["order"])) {
		case 1:
			$order = "`useSignupDate`";
			break;
		case 2:
			$order = "`useUsername`";
			break;
		default:
			$order = "`useSignupDate` DESC";
	}
}

if(isset($_GET["searchText"]) && $_GET["searchText"] != "")
{
	$where = "(SOUNDEX(`useUsername`) = SOUNDEX('".addslashes($_GET["searchText"])."') OR `useUsername` LIKE '%".addslashes($_GET["searchText"])."%')";
    //($where) AND 
}

if(isset($_GET["limit"]))
{
	switch(intval($_GET["limit"]))
	{
		case 1: $limit = 8; break;
		case 2: $limit = 24; break;
		case 3: $limit = 48; break;
		default: $limit = 12;
	}
	setcookie( "yGalLimit", $limit, strtotime( "+9 years" ), "/", ".".$_config[ "galRoot" ]);
}
if(!isset($_GET["limit"])){
	$limit = $_COOKIE[ "yGalLimit" ];
}

$offset = isset($_GET["offset"]) ? intval($_GET["offset"]) : 0;
if($offset < 0) $offset = 0;

$whereGuest = isLoggedIn() ? "" : ( " AND `useGuestAccess` = '1' " );

$result = sql_query("SELECT `useid` FROM `users`, `useExtData` ".
	"WHERE `useid` = `useEid` AND $where $whereGuest ORDER BY $order LIMIT $offset,$limit" );

$members = array();
while($useData = mysql_fetch_assoc($result)) {
	$members[$useData["useid"]] = $useData;
}

$cntResult = sql_query( "SELECT COUNT(*) FROM `users`, `useExtData` ".
	"WHERE `useid` = `useEid` AND $where" );

$totalCount = mysql_result( $cntResult, 0 );

$getVars = array();
if(isset($_GET["offset"])) $getVars["offset"] = $_GET["offset"];
if(isset($_GET["limit"])) $getVars["limit"] = $_GET["limit"];
if(isset($_GET["order"])) $getVars["order"] = $_GET["order"];
if(isset($_GET["searchText"])) $getVars["searchText"] = $_GET["searchText"];

if(!isset($disableNav)) $disableNav = false;
if(!isset($enableCategories)) $enableCategories = false;

if(!$disableNav) {
	?>
	<?iefixStart()?>
		<?ob_start()?>
		<?
		navControls( $offset, $limit, $totalCount );
		?>
		<div class="a_center">
			<?
			if($enableCategories) {
				?>
				<form action="<?=url(".") ?>" method="get">
				<?
				foreach($_GET as $key => $value)
					if($key != "offset")
						echo '<input name="'.htmlspecialchars($key).'" type="hidden" value="'.htmlspecialchars($value).'" />';
				?>
				<input type="text" name="searchText" value="<?=isset($_GET["searchText"]) ? htmlspecialchars($_GET["searchText"]) : "" ?>" />
				<input class="submit" type="submit" value="<?=_SEARCH ?>" style="vertical-align: middle" />
				</form>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				<?
			}
			?>
			<form action="<?=url(".")?>" method="get">
			<?
			foreach($_GET as $key => $value)
				if($key != "order" && $key != "limit")
					echo '<input name="'.htmlspecialchars($key).'" type="hidden" value="'.htmlspecialchars($value).'" />';
			?>
			<select name="order">
				<option <?=isset($_GET["order"]) && $_GET["order"] == 0 ? 'selected="selected"' : "" ?> value="0"><?=_NEWEST_FIRST ?></option>
				<option <?=isset($_GET["order"]) && $_GET["order"] == 1 ? 'selected="selected"' : "" ?> value="1"><?=_OLDEST_FIRST ?></option>
				<option <?=isset($_GET["order"]) && $_GET["order"] == 2 ? 'selected="selected"' : "" ?> value="2"><?=_BY_NAME ?></option>
				<!--<option <?=isset($_GET["order"]) && $_GET["order"] == 3 ? 'selected="selected"' : "" ?> value="3"><?=_RANDOM ?></option> -->
			</select>
			<select name="limit">
				<option <?=isset($_GET["limit"]) && $_GET["limit"] == 1 ||  $_COOKIE[ "yGalLimit" ] == 8 ? 'selected="selected"' : "" ?> value="1"><?=fuzzy_number(8)?></option>
				<option <?=isset($_GET["limit"]) && $_GET["limit"] == 0 ||  $_COOKIE[ "yGalLimit" ] == 12 ? 'selected="selected"' : "" ?> value="0"><?=fuzzy_number(12)?></option>
				<option <?=isset($_GET["limit"]) && $_GET["limit"] == 2 ||  $_COOKIE[ "yGalLimit" ] == 24 ? 'selected="selected"' : "" ?> value="2"><?=fuzzy_number(24)?></option>
				<option <?=isset($_GET["limit"]) && $_GET["limit"] == 3 ||  $_COOKIE[ "yGalLimit" ] == 48 ? 'selected="selected"' : "" ?> value="3"><?=fuzzy_number(48)?></option>
			</select>
			<input class="submit" type="submit" value="<?=_UPDATE ?>" style="vertical-align: middle" />
			</form>
		</div>
		<?$membersNavs = ob_get_contents(); ob_end_flush()?>
		<div class="hline">&nbsp;</div>
	<?iefixEnd()?>
	<?
}
else $membersNavs = "";

$artistsToGo = $limit;

foreach($members as $useData) {
	?>
	<div class="gallery_col mar_bottom">
		<?
		echo getUserAvatar("", $useData["useid"], false, true);
		echo " &nbsp; ".getUserLink($useData["useid"]);
		?>
	</div>
	<?
	$artistsToGo--;
	if(!$artistsToGo) break;
}

if(!count($members))
	echo '<div>'._NO_MEMBERS.'</div>';

echo '<div class="hline">&nbsp;</div>';
echo $membersNavs;

?>
<div class="clear">&nbsp;</div>

</div>
