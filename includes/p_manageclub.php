<?

// This script controls the "Profile" tab of the settings.

if( !isLoggedIn() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}


$cluid = intval( $_cmd[ 1 ]);

$where = array(
	"cluEid*" => "cluid",
	"cluid" => $cluid );


$sql = "SELECT * FROM `clubs`, `cluExtData`".dbWhere( $where );

$cluResult = sql_query( $sql );

if( !$cluData = mysql_fetch_assoc( $cluResult ))
{
	include( INCLUDES."p_notfound.php" );
	return;
}

$clubModerator = mysql_num_rows(sql_query("SELECT * FROM `useClubs` WHERE `useCclub`='$cluid'".
								"AND `useCmember`='".$_auth[ 'useid' ]."'".
								"AND `useCModerator`='1'"));

if( atLeastModerator() || $cluData [ "cluCreator" ] == $_auth[ "useid" ]) 
{
	$clubOwner = 1;
	$clubModerator = 1;
}
elseif( $clubModerator == "1" ) 
{
	$clubOwner = 0;
	$clubModerator = 1;
}
else {
	include( INCLUDES."p_notfound.php" );
	return;
}

mysql_free_result( $cluResult );

$_documentTitle = ( $cluData[ "cluIsProject" ] ? _PROJECT : _CLUB1 ).
	": ".$cluData[ "cluName" ].": "._CLUB_MANAGE;

?>
<div class="header">
	<div class="f_right mar_left a_center normaltext">
		<?= getClubIcon( $cluData[ "cluid" ])?>
	</div>
	<div class="f_left mar_right header_title">
		<?= $cluData[ "cluName" ]?>
		<div class="subheader"><?= _CLUB_MANAGE ?></div>
	</div>
	<?

	$active = 4;

	include( INCLUDES."mod_clubmenu.php" );

	?>
</div>

<div class="container">
	<form action="<?= url( "." ) ?>" enctype="multipart/form-data" method="post">
	<?

	if( isset( $_GET[ "saved" ]))
	{
		notice( _SET_SAVED );
	}

	
	if( isset( $_POST[ "submit" ]))
	{
		$refresh = false;
		if($clubOwner) 
		{	
			// Profile
	
			if( $cluData[ "cluName" ] != $_POST[ "cluName" ])
			{
				$clubname = $_POST[ "cluName" ];
				$validated = true;
	
				if( !$clubname )
				{
					notice( _CLUB_NAME_BLANK );
					$validated = false;
				}
				elseif( strlen( $clubname ) < $_config[ "minUsernameLength" ])
				{
					notice( sprintf( _CLUB_NAME_TOO_SHORT, $_config[ "minUsernameLength" ]));
					$validated = false;
				}
				else
				{
					$sql = "SELECT `cluid` FROM `clubs`".dbWhere( array(
						"cluName" => $clubname ));
	
					$chkResult = sql_query( $sql );
	
					if( mysql_num_rows( $chkResult ) > 0 )
					{
						notice( sprintf( _CLUB_NAME_EXISTS, $clubname ));
						$validated = false;
					}
				}
	
				if( $validated )
				{
					$cluData[ "cluName" ] = $_POST[ "cluName" ];
					$refresh = true;
				}
				else
				{
					notice( _CLUB_NAME_NOT_CHANGED );
				}
			}
	
			$cluData[ "cluDesc" ] = $_POST[ "cluDesc" ];
	
			$cluData[ "cluProfile" ] = $_POST[ "cluProfile" ];
	
			// Options
	
			$cluData[ "cluIsProject" ] = $_POST[ "cluIsProject" ] ? 1 : 0;
	
			$cluData[ "cluNoMembers" ] = isset( $_POST[ "allowMembers" ]) ? 0 : 1;
	
			$cluData[ "cluRequireAccept" ] = isset( $_POST[ "cluRequireAccept" ]) ? 1 : 0;
	
			$cluData[ "cluHide" ] = isset( $_POST[ "cluHide" ]) ? 1 : 0;
			
			// User and Content Management
				
				if( atLeastSModerator() ) {
					if( $cluCreatorConfirm = getUseridByName($_POST[ "cluCreator" ] ))
					{
						$cluData[ "cluCreator" ] = $cluCreatorConfirm;
					}
					else {
						notice(  sprintf( _UNKNOWN_USER, $_POST[ "cluCreator" ]) );
						$cluData[ "cluCreator" ] = $cluData[ "cluCreator" ];	
					}
				}
				else {
					$cluData[ "cluCreator" ] = $cluData[ "cluCreator" ];
				}
				
				
			
	
			include_once( INCLUDES."files.php" );
	
			// Icon
	
			$avatarError = checkUploadedFile( "avatar" );
	
			if( !$avatarError )
			{
				if( filesize( $_FILES[ "avatar" ][ "tmp_name" ]) > $_config[ "maxAvatarSize" ])
				{
					$avatarError = sprintf( _SET_AVATAR_SIZE_EXCEEDED, $_config[ "maxAvatarSize" ]);
				}
				else
				{
					// Check avatar image size/type.
	
					$avatarInfo = getimagesize( $_FILES[ "avatar" ][ "tmp_name" ]);
	
					list( $minAvatarWidth, $minAvatarHeight ) = preg_split( '/x/', $_config[ "minAvatarResolution" ]);
					list( $maxAvatarWidth, $maxAvatarHeight ) = preg_split( '/x/', $_config[ "maxAvatarResolution" ]);
	
					if( $avatarInfo[ 0 ] > $maxAvatarWidth || $avatarInfo[ 1 ] > $maxAvatarHeight ||
						$avatarInfo[ 0 ] < $minAvatarWidth || $avatarInfo[ 1 ] < $minAvatarHeight ||
						( $avatarInfo[ 2 ] != 1 && $avatarInfo[ 2 ] != 2 && $avatarInfo[ 2 ] != 3 ))
					{
						$avatarError = sprintf( _SET_AVATAR_TOO_LARGE, $_config[ "minAvatarResolution" ],
							$_config[ "maxAvatarResolution" ]);
					}
					else
					{
						// Upload avatar icon to /files/clubs/#/#####/
	
						$avatarFilename = applyIdToPath("files/clubs/", $cluData[ "cluid" ]);
	
						$oldFiles = glob( $avatarFilename."-*", GLOB_NOESCAPE );
	
						if( is_array( $oldFiles ) && count( $oldFiles ) > 0 )
						{
							foreach( $oldFiles as $oldFile )
							{
								unlink( $oldFile ); // Delete old files
							}
						}
	
						uploadFile( "avatar", $avatarFilename.'-'.time(), $extension );
	
						$refresh = true;
					}
				}
			}
	
			if( $avatarError != "" && $avatarError != _UPL_NO_FILE )
			{
				notice( $avatarError );
			}
	
			// ID
	
			$idError = checkUploadedFile( "id" );
	
			if( !$idError )
			{
				$idImageName = $_FILES[ "id" ][ "tmp_name" ];
			}
			else
			{
				$idImageName = "";
			}
	
			if( $idError != "" && $idError != _UPL_NO_FILE )
			{
				notice( $idError );
			}
	
			// Featured work
	
			$objid = $_POST[ "cluFeaturedObj" ];
	
			$objResult = sql_query( "SELECT `objid`, `objTitle`, `objExtension`, `objLastEdit` ".
				"FROM `objects`, `objExtData`".dbWhere( array(
				"objid" => $objid,
				"objEid*" => "objid",
				"objDeleted" => 0,
				"objPending" => 0 ))."LIMIT 1" );
	
			$featChanged = false;
	
			if( $objData = mysql_fetch_assoc( $objResult ))
			{
				$featImageName = applyIdToPath( "files/data/", $objid)."-".
					preg_replace( '/[^0-9]/', "", $objData[ "objLastEdit" ]).".".$objData[ "objExtension" ];
	
				$featImageName2 = applyIdToPath( "files/thumbs/", $objid )."-".
					preg_replace( '/[^0-9]/', "", $objData[ "objLastEdit" ]).".jpg";
	
				if( ($cluData[ "cluFeaturedObj" ] != $objData[ "objid" ]) || ($cluData[ "cluFeaturedDesc" ] != $_POST[ "cluFeaturedDesc" ]) )
				{
					$cluData[ "cluFeaturedObj" ] = $objData[ "objid" ];
					$cluData[ "cluFeaturedDesc" ] = $_POST[ "cluFeaturedDesc" ];
					$featChanged = true;
				}
			}
			else
			{
				$featImageName = "";
				$featImageName2 = "";
				$featChanged = true;
				$cluData[ "cluFeaturedObj" ] = 0;
				$cluData[ "cluFeaturedDesc" ] = "";
			}
	
			mysql_free_result( $objResult );
	
			// Make thumbnails for ID & Featured work.
	
			foreach( array(
				array(
					"name" => "",
					"path" => "files/clubids/",
					"res" => $_config[ "idResolution" ],
					"allowSWF" => true,
					"imgname" => $idImageName,
					"imgname2" => "" ),
				array(
					"name" => "cluFeaturedObj",
					"path" => "files/clubfeatures/",
					"res" => $_config[ "featureResolution" ],
					"allowSWF" => false,
					"imgname" => $featImageName,
					"imgname2" => $featImageName2 )) as $item )
			{
				if( $item[ "name" ] == "cluFeaturedObj" && !$featChanged && !isset( $_POST[ "removeFeature" ]))
				{
					continue; // Item did not change.
				}
	
				if( $item[ "name" ] == "" && $idError == _UPL_NO_FILE && !isset( $_POST[ "removeId" ]))
				{
					continue; // Item did not change.
				}
	
				if( $item[ "name" ] == "cluFeaturedObj" )
				{
					$fileName = applyIdToPath( $item[ "path" ], $cluData[ "cluFeaturedObj" ]);
				}
				else
				{
					$fileName = applyIdToPath( $item[ "path" ], $cluData[ "cluid" ]);
				}

				if( $item[ "imgname" ] == "" || !file_exists( $item[ "imgname" ] ) )
				{
					continue;
				}
	
				$imageInfo = getimagesize( $item[ "imgname" ]);
	
				if( $item[ "allowSWF" ] && ( $imageInfo[ 2 ] == 4 || $imageInfo[ 2 ] == 13 )) // SWF/SWC
				{
					$useSWF = true;
					$newName = $fileName."-".time().".swf";
				}
				else
				{
					$useSWF = false;
					$newName = $fileName."-".time().".jpg";
				}
			
	
				$oldFiles = glob( $fileName."-*", GLOB_NOESCAPE );
	
				if( is_array( $oldFiles ) && count( $oldFiles ) > 0 )
				{
					foreach( $oldFiles as $oldFile )
					{
						unlink( $oldFile ); // Delete old files
					}
				}
	
				if( $item[ "name" ] == "" && isset( $_POST[ "removeId" ]))
				{
					continue; // Just remove - do not upload new file.
				}
	
				if( $item[ "name" ] == "cluFeaturedObj" && isset( $_POST[ "removeFeature" ]))
				{
					$cluData[ "cluFeaturedObj" ] = 0;
					$cluData[ "cluFeaturedDesc" ] = "";
					continue; // Just remove - do not upload new file.
				}
	
				if( $item[ "imgname" ] != "" )
				{
					$imageName = $item[ "imgname" ];
	
					list( $thumbMaxWidth, $thumbMaxHeight ) = preg_split( '/x/', $item[ "res" ]);
	
					if( $useSWF )
					{
						$filesize = filesize( $imageName );
	
						if( $filesize > $_config[ "idSWFMaxSize" ])
						{
							notice( sprintf( "Your Flash animation is %d bytes. Maximum is %d bytes.",
								$filesize, $_config[ "idSWFMaxSize" ]));
						}
						else
						{
							move_uploaded_file( $imageName, $newName );
							chmod( $newName, 0644 );
						}
					}
					else
					{
						if( !thumbifyImage( $imageName, $newName, $thumbMaxWidth, $thumbMaxHeight, 86 ))
						{
							// If it's impossible to read the original image (e.g. it's a text file
							// or a Flash animation), then make the featured image from the thumbnail.
	
							$imageName = $item[ "imgname2" ];
	
							thumbifyImage( $imageName, $newName, $thumbMaxWidth, $thumbMaxHeight, 86 );
						}
					}
				}
			}
			
			
	
			// Update the database
	
			sql_query( "UPDATE `clubs`".dbSet( array(
				"cluName" => $cluData[ "cluName" ],
				"cluDesc" => $cluData[ "cluDesc" ],
				"cluIsProject" => $cluData[ "cluIsProject" ],
				"cluHide" => $cluData[ "cluHide" ])).dbWhere( array(
				"cluid" => $cluData[ "cluid" ])));
	
			//While the initial separation of these two queries was due to testing,
			//It has been decided to leave them separated until further notice to 
			//prevent any possible issues that could arise from the ownership changes
			if( atLeastSModerator() ) { // This is the query for supermods and higher, deals with club ownership
				sql_query( "UPDATE `cluExtData`".dbSet( array(
				"cluRequireAccept" => $cluData[ "cluRequireAccept" ],
				"cluFeaturedObj" => $cluData[ "cluFeaturedObj" ],
				"cluFeaturedDesc" => $cluData[ "cluFeaturedDesc" ],
				"cluProfile" => $cluData[ "cluProfile" ],
				"cluNoMembers" => $cluData[ "cluNoMembers" ], 
				"cluCreator" => $cluData[ "cluCreator" ])).dbWhere( array(
				"cluEid" => $cluData[ "cluid" ])));
			}
			else { //This is the query for non-smoderators, does not affect club ownership
				sql_query( "UPDATE `cluExtData`".dbSet( array(
				"cluRequireAccept" => $cluData[ "cluRequireAccept" ],
				"cluFeaturedObj" => $cluData[ "cluFeaturedObj" ],
				"cluFeaturedDesc" => $cluData[ "cluFeaturedDesc" ],
				"cluProfile" => $cluData[ "cluProfile" ],
				"cluNoMembers" => $cluData[ "cluNoMembers" ])).dbWhere( array(
				"cluEid" => $cluData[ "cluid" ])));
			}
			//Add and remove moderator functions
			if( $_POST[ "cluNewModerator" ] != "" ) {
				//First we resolve that user's ID
				$clubNewMod = addslashes($_POST[ "cluNewModerator" ]);
				if( $cluNewModId = getUseridByName( $clubNewMod)) { //Checks if the user exists in the system
					//Now we can see if they're a member of this club, and if so, mod them
					$result = sql_query("SELECT `useCid` FROM `useClubs` WHERE `useCmember` = '".$cluNewModId."' AND `useCclub` = '$cluid' LIMIT 1");
					if($newModData = mysql_fetch_assoc($result)) { // Are they actually a member? If so, then welcome aboard!
						sql_query("UPDATE `useClubs` SET `useCModerator`='1' WHERE `useCid` = '".$newModData["useCid"]."' LIMIT 1");
					}
					else {
						notice( sprintf( _CLUB_NOT_A_MEMBER, $clubNewMod ) );
					}
				}
				else {
					notice( sprintf( _UNKNOWN_USER, $clubNewMod ) );
				}
			}
			if( $_POST[ "cluRemModerator" ] != "") {
				//First we resolve that user's ID
				$clubRemMod = addslashes($_POST[ "cluRemModerator" ]);
				if( $cluRemModId = getUseridByName( $clubRemMod)) { //Checks if the user exists in the system
					//Now we can see if they're a member of this club, and if so, unmod them
					$result = sql_query("SELECT `useCid` FROM `useClubs` WHERE `useCmember` = '".$cluRemModId."' AND `useCclub` = '$cluid' LIMIT 1");
					if($remModData = mysql_fetch_assoc($result)) { // Are they actually a member? If so, then bye-bye banhammer!
						sql_query("UPDATE `useClubs` SET `useCModerator`='0' WHERE `useCid` = '".$remModData["useCid"]."' LIMIT 1");
					}
					else {
						notice( sprintf( _CLUB_NOT_A_MEMBER, $clubRemMod ) );
					}
				}
				else {
					notice( sprintf( _UNKNOWN_USER, $clubRemMod ) );
				}
			}
		}
		
		if( $_POST[ "cluRemoveUser" ] != "" ) {
				//First we resolve that user's ID
				$cluRemoveUser=$_POST[ "cluRemoveUser" ];
				if( $cluRemoveId = addslashes( getUseridByName( $cluRemoveUser))) { //Checks if the user exists in the system
					//Now we can see if they're a member of this club, and if so, remove them
					$result = sql_query("SELECT `useCid` FROM `useClubs` WHERE `useCmember` = '".$cluRemoveId."' AND `useCclub` = '$cluid' LIMIT 1");
					if($remData = mysql_fetch_assoc($result)) { // Are they actually a member? If so, then bye-bye!
						sql_query("DELETE FROM `useClubs` WHERE `useCid` = '".$remData["useCid"]."' LIMIT 1");
					}
					else {
						notice( sprintf( _CLUB_NOT_A_MEMBER, $cluRemoveUser ) );
					}
				}
				else {
					notice( sprintf( _UNKNOWN_USER, $cluRemoveUser ) );
				}
				
			}
			if( $_POST[ "cluRemoveSubmission" ] != "" ) {
				$clubRemoveSub = intval($_POST[ "cluRemoveSubmission" ]);
				$clubSubQuery = sql_query("SELECT `objForClub`,`objForClub2`,`objForClub3` FROM `objects` WHERE `objid`='$clubRemoveSub' AND `objDeleted`='0' AND `objPending`='0' LIMIT 1");				
				if($clubSub = mysql_fetch_assoc($clubSubQuery)) {
					if($clubSub[ "objForClub" ] == $cluid) {
						sql_query("UPDATE `objects` SET `objForClub`='' WHERE `objid`='$clubRemoveSub' LIMIT 1");
						sql_query("DELETE FROM `clubObjects` WHERE `cloObject`='$clubRemoveSub' AND `cloClub`='$cluid' LIMIT 1");
					}
					elseif($clubSub[ "objForClub2" ] == $cluid) {
						sql_query("UPDATE `objects` SET `objForClub2`='' WHERE `objid`='$clubRemoveSub' LIMIT 1");
						sql_query("DELETE FROM `clubObjects` WHERE `cloObject`='$clubRemoveSub' AND `cloClub`='$cluid' LIMIT 1");
					}
					elseif($clubSub[ "objForClub3" ] == $cluid) {
						sql_query("UPDATE `objects` SET `objForClub3`='' WHERE `objid`='$clubRemoveSub' LIMIT 1");
						sql_query("DELETE FROM `clubObjects` WHERE `cloObject`='$clubRemoveSub' AND `cloClub`='$cluid' LIMIT 1");
					}
					else {
						notice( _CLUB_NOT_A_SUBMISSION );
					}
				}
				else {
					notice( sprintf( _UNKNOWN_SUBMISSION, intval($_POST[ "cluRemoveSubmission" ])));
				}
			}
		

		if( $refresh )
		{
			redirect( url( ".", array( "saved" => "yes" )));
		}
		else
		{
			notice( _SET_SAVED );
		}
	}

	if($clubOwner)
	{ //Restricts view of the non-user/content management sections to club owners
		// ======================================================================================================
		// PROFILE
		// ======================================================================================================
	
		?>
		<? iefixStart() ?>
		<div class="sep largetext"><?= _PROFILE ?></div>
		<div class="container2 notsowide">
			<table cellspacing="0" cellpadding="4" border="0" width="100%">
			<tr>
				<td align="right" class="nowrap"><?= _ICON ?>:</td>
				<td width="100%">
					<div id="avatar_current">
						<?= getClubIcon( $cluData[ "cluid" ]) ?>
						<?
	
						$script = "make_invisible('avatar_current'); ".
							"make_visible('avatar_hint'); ".
							"make_visible('avatar_choose'); ".
							"return false;";
	
						?>
						&nbsp;<a href="" onclick="<?= $script ?>">
							<?= getIMG( urlf()."images/emoticons/edit.png", "" )." "._CHANGE ?></a>
					</div>
					<div id="avatar_choose" style="display: none">
						<?
	
						$script = "make_visible('preview_container'); ".
							"make_invisible('preview_hint'); ".
							"show_preview_image('preview', 'preview_message', this.value);";
	
						?>
						<input accept="image/gif, image/jpeg, image/png" onchange="<?= $script ?>"
							name="avatar" size="50" type="file" />
					</div>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<div style="display: none" id="avatar_hint">
						<li><?= sprintf( _SET_AVATAR_TOO_LARGE,
							$_config[ "minAvatarResolution" ],
							$_config[ "maxAvatarResolution" ]) ?></li>
						<li><?= sprintf( _SET_AVATAR_SIZE_EXCEEDED,
							$_config[ "maxAvatarSize" ]) ?></li>
					</div>
					<div style="display: none" id="preview_container">
						<div class="sep caption"><?= _PREVIEW ?>:</div>
						<div class="a_center">
							<img alt="preview" style="display: none" id="preview" src="" />
							<div id="preview_message"><?= _SUBMIT_SELECT_FILE ?></div>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td align="right" class="nowrap"><?= _TITLE ?>:</td>
				<td>
					<input type="text" name="cluName" size="50"
						value="<?= htmlspecialchars( $cluData[ "cluName" ]) ?>" />
				</td>
			</tr>
			<tr>
				<td valign="top" align="right" class="nowrap">
					<div><?= _DESCRIPTION ?>:</div>
					<?
	
					$script = "make_invisible('descr_current'); ".
						"make_invisible('descr_edit'); ".
						"make_visible('descr_change'); ".
						"return false;";
	
					$descrEdit = '<div id="descr_edit">'.
						'<a href="" onclick="'.$script.'">'.
						getIMG( urlf()."images/emoticons/edit.png", "" )." "._EDIT.'</a></div>';
	
					if( trim( $cluData[ "cluDesc" ]) != "" )
					{
						echo $descrEdit;
					}
	
					?>
				</td>
				<td>
					<div id="descr_current">
						<?
	
						if( trim( $cluData[ "cluDesc" ]) != "" )
						{
							?>
							<div class="container2"><?= formatText( $cluData[ "cluDesc" ]) ?></div>
							<?
						}
						else
						{
							echo $descrEdit;
						}
	
						?>
					</div>
					<div id="descr_change" style="display: none">
						<div>
							<?
	
							iefixStart();
	
							$commentName = "cluDesc";
							$commentDefault = $cluData[ "cluDesc" ];
							$commentNoOptions = true;
							$commentRows = 3;
	
							include( INCLUDES."mod_comment.php" );
	
							iefixEnd();
	
							?>
							<div class="clear">&nbsp;</div>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td align="right" class="nowrap"><?= _ID ?>:</td>
				<td width="100%">
					<div id="id_current">
						<?
	
						$filename = findNewestFileById( "files/clubids/", $cluData[ "cluid" ],
							"images/nothumb.gif" );
	
						$imageInfo = getimagesize( $filename );
	
						if( $imageInfo[ 2 ] == 4 || $imageInfo[ 2 ] == 13 ) // SWF/SWC
						{
							echo getSWF( $filename, 1, 150, 150 );
						}
						else
						{
							$width = round( $imageInfo[ 0 ] * 0.25 );
							$height = round( $imageInfo[ 1 ] * 0.25 );
	
							makeFloatingThumb( "", url().$filename, $imageInfo[ 0 ], $imageInfo[ 1 ],
								0, false, $onmouseover, $onmouseout );
	
							echo getIMG( urlf().$filename, 'style="width: '.$width.'px; height: '.$height.'px" '.
								'onmouseover="'.$onmouseover.'" onmouseout="'.$onmouseout.'"', true );
						}
	
						$script = "make_invisible('id_current'); ".
							"make_visible('id_hint'); ".
							"make_visible('id_choose'); ".
							"return false;";
	
						?>
						&nbsp;<a href="" onclick="<?= $script ?>">
							<?= getIMG( urlf()."images/emoticons/edit.png", "" )." "._CHANGE ?></a>
						&nbsp;<input type="checkbox" class="checkbox" name="removeId" id="idRemoveID" />
							<label for="idRemoveID"><?= _REMOVE ?></label>
					</div>
					<div id="id_choose" style="display: none">
						<?
	
						$script = "make_visible('id_preview_container'); ".
							"make_invisible('id_preview_hint'); ".
							"show_preview_image('id_preview', 'id_preview_message', this.value);";
	
						?>
						<input accept="image/gif, image/jpeg, image/png" onchange="<?= $script ?>"
							name="id" size="50" type="file" />
					</div>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<div style="display: none" id="id_hint">
						<?= sprintf( _SET_ID_FILE_EXPLAIN, $_config[ "idSWFMaxSize" ]) ?>
					</div>
					<div style="display: none" id="id_preview_container">
						<div class="sep caption"><?= _PREVIEW ?>:</div>
						<div class="a_center">
							<img alt="preview" style="display: none" id="id_preview" src="" />
							<div id="id_preview_message"><?= _SUBMIT_SELECT_FILE ?></div>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td align="right" class="nowrap"><?= _FEATURE ?>:</td>
				<td width="100%">
					<div id="feature_current">
						<?
	
						$filename = findNewestFileById( "files/clubfeatures/", $cluData[ "cluFeaturedObj" ],
							"images/nothumb.gif" );
	
						$imageInfo = getimagesize( $filename );
	
						$width = round( $imageInfo[ 0 ] * 0.25 );
						$height = round( $imageInfo[ 1 ] * 0.25 );
	
						makeFloatingThumb( "", url().$filename, $imageInfo[ 0 ], $imageInfo[ 1 ],
							0, false, $onmouseover, $onmouseout );
	
						echo getIMG( urlf().$filename, 'style="width: '.$width.'px; height: '.$height.'px" '.
							'onmouseover="'.$onmouseover.'" onmouseout="'.$onmouseout.'"', true );
	
						$script = "make_invisible('feature_current'); ".
							"make_visible('feature_choose'); ".
							"make_visible('feature_hint'); ".
							"return false;";
	
						?>
						&nbsp;<a href="" onclick="<?= $script ?>">
							<?= getIMG( urlf()."images/emoticons/edit.png", "" )." "._CHANGE ?></a>
						&nbsp;<input type="checkbox" class="checkbox" name="removeFeature" id="idRemoveFeature" />
							<label for="idRemoveFeature"><?= _REMOVE ?></label>
					</div>
					<div id="feature_choose" style="display: none">
						<input class="narrow" name="cluFeaturedObj" type="text" id="idFeaturedObj"
							value="<?= $cluData[ "cluFeaturedObj" ] ?>" />
						&nbsp; (<?= _CLUB_FEATURE_EXPLAIN ?>)
					</div>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<div style="display: none" id="feature_hint">
						<div><?= _CLUB_FEATURE_EXPLAIN2 ?></div>
						<div class="sep caption"><?= _CLUB_FEATURE_DESCR ?>:</div>
						<div>
						<? iefixStart() ?>
						<?
	
						$commentName = "cluFeaturedDesc";
						$commentDefault = $cluData[ "cluFeaturedDesc" ];
						$commentNoOptions = true;
						$commentRows = 3;
	
						include( INCLUDES."mod_comment.php" );
	
						?>
						<div class="clear">&nbsp;</div>
						<? iefixEnd() ?>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td valign="top" align="right" class="nowrap">
					<div><?= _PROFILE ?>:</div>
					<?
	
					$script = "make_invisible('profile_current'); ".
						"make_invisible('profile_edit'); ".
						"make_visible('profile_change'); ".
						"return false;";
	
					$profileEdit = '<div id="profile_edit">'.
						'<a href="" onclick="'.$script.'">'.
						getIMG( urlf()."images/emoticons/edit.png", "" )." "._EDIT.'</a></div>';
	
					if( trim( $cluData[ "cluProfile" ]) != "" )
					{
						echo $profileEdit;
					}
	
					?>
				</td>
				<td>
					<div id="profile_current">
						<?
	
						if( trim( $cluData[ "cluProfile" ]) != "" )
						{
							?>
							<div class="container2"><?= formatText( $cluData[ "cluProfile" ]) ?></div>
							<?
						}
						else
						{
							echo $profileEdit;
						}
	
						?>
					</div>
					<div id="profile_change" style="display: none">
						<div>
							<?
	
							iefixStart();
	
							$commentName = "cluProfile";
							$commentDefault = $cluData[ "cluProfile" ];
							$commentNoOptions = true;
							$commentRows = 12;
	
							include( INCLUDES."mod_comment.php" );
	
							iefixEnd();
	
							?>
							<div class="clear">&nbsp;</div>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td align="right" class="nowrap"><?= _SET_THEME ?>:</td>
				<td width="100%">
					<a href="<?= url( "clubthemedesigner/".$cluid ) ?>">
						<span style="vertical-align: middle"><?= _SET_DESIGN_CUSTOM_THEME ?></span>
						<?= getIMG( urlf()."images/emoticons/nav-next.png", "" ) ?></a>
				</td>
			</tr>
			</table>
		</div>
		<? iefixEnd() ?>
		<?
	
		// ======================================================================================================
		// OPTIONS
		// ======================================================================================================
	
		?>
		<? iefixStart() ?>
		<div class="sep largetext"><?= _OEKAKI_OPTIONS ?></div>
		<div class="container2 notsowide">
			<table cellspacing="0" cellpadding="4" border="0">
			<tr>
				<td align="right"><br /></td>
				<td>
					<span style="vertical-align: middle"><b><?= _CLUB_STATUS ?></b>:</span>
	
					<input <?= !$cluData[ "cluIsProject" ] ? 'checked="checked"' : "" ?>
						id="clubStatusClub" class="radio" type="radio" name="cluIsProject" value="0" />
					<label for="clubStatusClub"><?= _CLUB1 ?></label>
	
					<input <?= $cluData[ "cluIsProject" ] ? 'checked="checked"' : "" ?>
						id="clubStatusProject" class="radio" type="radio" name="cluIsProject" value="1" />
					<label for="clubStatusProject"><?= _PROJECT ?></label>
				</td>
			</tr>
			<tr>
				<td align="right">
					<input <?= $cluData[ "cluNoMembers" ] ? "" : 'checked="checked"' ?> class="checkbox"
						id="idAllowMembers" name="allowMembers" type="checkbox" />
				</td>
				<td>
					<label for="idAllowMembers"><?= _CLUB_ALLOW_MEMBERSHIP ?></label>
				</td>
			</tr>
			<tr>
				<td align="right">
					<input <?= $cluData[ "cluRequireAccept" ] ? 'checked="checked"' : "" ?> class="checkbox"
						id="idRequireAccept" name="cluRequireAccept" type="checkbox" />
				</td>
				<td>
					<label for="idRequireAccept"><?= _CLUB_REQUIRE_ACCEPT ?></label>
				</td>
			</tr>
			<tr>
				<td align="right">
					<input <?= $cluData[ "cluHide" ] ? 'checked="checked"' : "" ?> class="checkbox"
						id="idHideClub" name="cluHide" type="checkbox" />
				</td>
				<td>
					<label for="idHideClub"><?= _CLUB_HIDE_CLUB ?></label>
				</td>
			</tr>
			</table>
		</div>
		<? iefixEnd() ?>
			<?
	}
	// ======================================================================================================
	// USER & CONTENT MANAGEMENT
	// ======================================================================================================

	?>
	<? iefixStart() ?>
	<div class="sep largetext"> <?= _CLUB_USER_CONTENT?></div>
	<div class="container2 notsowide">
		<table cellspacing="0" cellpadding="4" border="0">
		<?php  
		if( atLeastSModerator() ) {		
		?>
		<tr>
			<td align="right">
				<?= _CLUB_OWNER ?>:
			</td>
			<td>
			<?php $cluCreatorData = getUserData( $cluData[ "cluCreator" ]);  ?>
				<input type="text" name="cluCreator" size="30"
					value="<?= htmlspecialchars( $cluCreatorData[ "useUsername" ]) ?>" />
			</td>
		</tr>
		<?php } ?>
		<tr>
			<td align="right">
				<?= _CLUB_REMOVE_USER ?>:
			</td>
			<td>
				<input type="text" name="cluRemoveUser" size="30" />		
			</td>
			<tr>
				<td align="right">
					<?= _CLUB_REMOVE_SUBMISSION ?>:
				</td>
				<td>
					<input type="text" name="cluRemoveSubmission" size="30" />		
				</td>
			</tr>
		</tr>
		<? if($clubOwner) {?>
			
			<tr>
				<td align="right">
					<?= _CLUB_MODERATORS_ADD ?>:
				</td>
				<td>
					<input type="text" name="cluNewModerator" size="30" />		
				</td>
			</tr>
			<tr>
				<td align="right">
					<?= _REMOVE." "._CLUB_MODERATORS ?>:
				</td>
				<td>
					<input type="text" name="cluRemModerator" size="30" />
				</td>
			</tr>
		<? } ?>
		</table>
	</div>
	<? iefixEnd() ?>
	<?
	

	// ======================================================================================================

	?>
	<div class="sep">
		<button class="submit" name="submit" type="submit">
			<?= getIMG( url()."images/emoticons/checked.png" ) ?>
			<?= _SAVE_CHANGES ?>
		</button>
	</div>
	</form>
</div>
