<?

// Check if an asset is being requested.

ob_start( "ob_gzhandler" ); // Force GZip output.

$assetCount = intval( $_POST[ "count" ]);

echo "[";

for( $assetNum = 0; $assetNum < $assetCount; $assetNum++ )
{
	if( $assetNum > 0 )
	{
		echo ", ";
	}

	$_asset_in = array();

	foreach( $_POST as $key => $value )
	{
		$pos = strpos( $key, "_" );

		if( $pos === false )
			continue;

		$num = substr( $key, 0, $pos );

		if( $num != $assetNum )
			continue;

		$varname = substr( $key, $pos + 1 );

		$_asset_in[ $varname ] = $value;
	}

	$kind = preg_replace( '/[^0-9a-z\_\-]/', "",
		strtolower( $_asset_in[ "_kind" ]));

	if( $kind == "updates" )
	{
		echo "@!#&@$#(1";
		return;
	}

	$assetFilename = INCLUDES."asset_".$kind.".php";

	if( file_exists( $assetFilename ))
	{
		$_asset_out = array(
			"_kind" => $kind,
			"_id" => $assetNum );

		/*
		$time_start = gettimeofday();
		*/

		include( $assetFilename );

		/*
		$time_end = gettimeofday();
		$secdiff = $time_end[ "sec" ] - $time_start[ "sec" ];
		$usecdiff = $time_end[ "usec" ] - $time_start[ "usec" ];

		$_asset_out[ "gen_time" ] = round((
			$secdiff * 1000000 + $usecdiff ) / 1000000, 3 );
		*/

		asset_write_json( $_asset_out );
	}
	else
	{
		echo "{ _kind: 'error', message: 'There is no handler for asset: " +
			$kind + "' }";
	}

	if( 0 )
	{
		// Store profiler information.
    
		$time_start = $_stats[ "startTime" ];
		$time_end = gettimeofday();
		$secdiff = $time_end[ "sec" ] - $time_start[ "sec" ];
		$usecdiff = $time_end[ "usec" ] - $time_start[ "usec" ];
		$generationTime = round(( $secdiff * 1000000 + $usecdiff ) / 1000000, 3 );

		$page = "asset::".$kind;
    
		sql_where( array( "prfPage" => $page ));
    
		if( sql_count( "profiler" ) > 0 )
		{
			sql_query( "UPDATE `profiler` SET `prfCount` = `prfCount` + 1, ".
				"`prfTime` = `prfTime` + '".$generationTime."' ".
				"WHERE `prfPage` = '".addslashes( $page )."'" );
		}
		else
		{
			sql_values( array(
				"prfPage" => $page,
				"prfCount" => 1,
				"prfTime" => $generationTime ));
    
			sql_insert( "profiler" );
		}
	}
}

echo "]";

return;

/**
 * Writes a Javascript object containing specified asset data.
 *
 * @param assetData Asset data (can write either request or result data).
 */

function asset_write_json( $assetData )
{
	echo "{ ";

	$first = true;

	foreach( $assetData as $key => $value )
	{
		echo $first ? "" : ", ";

		$first = false;

		asset_write_json1( $key, $value );
	}

	echo " }";
}

function asset_write_json1( $key, $value )
{
	echo "'".$key."': ";

	if( is_array( $value ))
	{
		$mode = 1; // JS array mode

		foreach( $value as $arrayKey => $arrayItem )
		{
			if( !is_array( $arrayItem ))
			{
				$mode = 2; // JS object mode
				break;
			}
		}

		echo $mode == 1 ? "[ " : "{ ";

		$first2 = true;

		foreach( $value as $arrayKey => $arrayItem )
		{
			echo $first2 ? "" : ", ";

			$first2 = false;

			if( $mode == 1 )
			{
				asset_write_json( $arrayItem );
			}
			else
			{
				asset_write_json1( $arrayKey, $arrayItem );
			}
		}

		echo $mode == 1 ? " ]" : " }";
	}
	else
	{
		$value = str_replace( "\r", "", $value );
		$value = str_replace( "\n", '\\n', $value );

		echo "'".addslashes( $value )."'";
	}
}

?>
