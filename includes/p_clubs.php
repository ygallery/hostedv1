<?
	$result = sql_query("SELECT * FROM `users`,`useExtData` WHERE `useUsername` = '".addslashes($_cmd[1])."' AND `useEid` = `useid` LIMIT 1");
	if(!$useData = mysql_fetch_assoc($result))
	{
		if( isLoggedIn() )
		{
			redirect( url( "clubs/".strtolower( $_auth[ "useUsername" ])));
		}

		include(INCLUDES."p_notfound.php");
		return;
	}
	$useUsername = strtolower($useData["useUsername"]);

	$_pollUser = $useData["useid"];

	$_documentTitle = $useData["useUsername"].": "._CLUBS;
?>

<div class="header">
	<div class="f_right mar_left a_center normaltext">
		<?=getUserAvatar("",$useData["useid"], true)?>
	</div>
	<div class="f_left header_title">
		<?=$useData["useUsername"]?>
		<div class="subheader"><?=_CLUBS ?></div>
	</div>	
	<?
	$active = 5;
	include(INCLUDES."mod_usermenu.php");
	?>
</div>

<div class="container">
	<div class="notsowide">
		<?
		$useid = $useData["useid"];
		include(INCLUDES."mod_clubs.php");
		?>
		<div class="clear">&nbsp;</div>
		<?
		if($_auth["useid"] == $useData["useid"]) {
			?>
			<form action="<?=url("newclub") ?>" method="post">
				<div class="sep caption"><?=_CLUB_FOUND ?>:</div>
				<div>
					<input class="largetext notsowide" name="clubname" type="text" value="" />
				</div>
				<div class="sep">
					<button class="submit" name="submit" type="submit">
						<?=getIMG(url()."images/emoticons/club2.png") ?>
						<?=_CLUB_CREATE ?>
					</button>
				</div>
			</form>
			<?
		}
		?>
	</div>
</div>
