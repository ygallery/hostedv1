<?php
set_time_limit(1500);

include_once( INCLUDES."keywording.php" );

$keywords = fetchKeywords( 0 );

// Appearances passed to writeKeywordData():

define( "KWD_APP_OPEN", "-2" ); // Open content
define( "KWD_APP_CLOSE", "-1" ); // Close content
define( "KWD_APP_CLEAR", "0" ); // Clear content
define( "KWD_APP_SWITCH", "1" ); // Switch from header into container
define( "KWD_APP_TAB", "2" ); // Tab, non-selectable
define( "KWD_APP_KEYWORD", "3" ); // Keyword, selectable
define( "KWD_APP_KEYWORD_TAB", "4" ); // Tab+Keyword, selectable
define( "KWD_APP_HEADER", "5" ); // Group title (large text)
define( "KWD_APP_HLINE", "6" ); // Horizontal line

$keywordData = "var KeywordData = new Array(";
$keywordDataLast = 0;
$keywordCaptions = array();
$keywordCaptionLast = 0;
$keywordDesc = array();

// Note: First character of a parameter name must be unique within $params,
// i.e. all parameter names must begin with different characters. Currently
// all possible parameters are:
// - caption
// - fullname
// - group
// - id
// - keyword_space
// - target

function writeKeywordData( $appearance, $params = array() )
{
	global $keywordData, $keywordDataLast;

	$keywordData .= ( $keywordDataLast > 0 ? "," : "" );

	$keywordDataLast++;

	if( count( $params ) == 0 )
	{
		$keywordData .= $appearance;
		return;
	}

	$keywordData .= "{".( $appearance == KWD_APP_KEYWORD ? "" : "a:$appearance," ); // a:3 by default

	$first = true;

	foreach( $params as $key => $value )
	{
		$value = strval( $value );

		if( $value == "" )
			continue;

		if( $key == "keyword_space" && $value == "0" )
			continue; // k:0 by default

		$key = substr( $key, 0, 1 );

		$keywordData .= ( $first ? "" : "," )."$key:$value";

		$first = false;
	}

	$keywordData .= "}";
}

function getKeywordCaption( $captionText, $descText = "" )
{
	global $keywordCaptions, $keywordDesc, $keywordCaptionLast;

	$key = array_search( $captionText, $keywordCaptions );

	if( $key === false )
	{
		$key = $keywordCaptionLast;
		$keywordCaptions[ $key ] = $captionText;
		$keywordDesc[ $key ] = $descText;
		$keywordCaptionLast++;
	}

	if( $descText != "" )
	{
		$keywordDesc[ $key ] = $descText;
	}

	return $key;
}

function showKeywordSubcat( $keywords, $parentId = 0, $parentKeyName = "",
	$parentLastKeyName = "" )
{
	global $requiredTabs, $firstKeywordId, $isInHeader, $previousFullname;

	$noSubcats = true;

	foreach( $keywords as $keyData )
	{
		if( isset( $keyData[ "subcats" ]))
		{
			$noSubcats = false;
			break;
		}
	}

	if( $parentLastKeyName != "" )
	{
		writeKeywordData( KWD_APP_HEADER, array(
			"caption" => getKeywordCaption( $parentLastKeyName )));
	}

	foreach( $keywords as $keyData )
	{
		$tabid = generateId( "" );
		$nonSelectable = preg_match( '/\@$/', $keyData[ "keyWord" ]);
		$keyData["keyWord"] = preg_replace( '/\@$/', "", $keyData[ "keyWord" ]);
		$keyWord = htmlspecialchars( $keyData[ "keyWord" ]);

		if( !$firstKeywordId && $isInHeader )
			$firstKeywordId = "kwcache_tab".$tabid;

		$nameArr = preg_split( '/\"/', $parentKeyName.$keyWord, -1, PREG_SPLIT_NO_EMPTY );
		$fullname = "'";
		$first = true;

		foreach( $nameArr as $name1 )
		{
			if( $name1 == $keyWord )
				continue;

			$fullname .= ( $first ? "" : "," ).getKeywordCaption( $name1 );

			$first = false;
		}

		$fullname .= "'";

		/*
		if($fullname == $previousFullname)
			$fullname = "";
		else
			$previousFullname = $fullname;
		*/

		$keywordSpace = ($parentId != 0 && !$noSubcats) ? 1 : 0;

		// If it contains sub-keywords, display as tab.
		
		if( isset( $keyData[ "subcats" ]))
		{
			$params = array(
				"keyword_space" => $keywordSpace,
				"id" => $tabid,
				"group" => $parentId,
				"target" => $keyData[ "keyid" ],
				"caption" => getKeywordCaption( $keyWord,
					formatText( $keyData[ "keyDesc" ])));

			if( !( $parentId != 0 && !$nonSelectable ))
			{
				// If it's a root keyword or it's set as non-selectable, display as
				// simple tab.

				writeKeywordData( KWD_APP_TAB, $params );
			}
			else
			{
				// Otherwise, display as tab with a selectable keyword inside.

				writeKeywordData( KWD_APP_KEYWORD_TAB, array_merge( $params, array(
					"fullname" => $fullname )));
			}
		}
		else
		{
			// Otherwise, display as simple selectable keyword.

			writeKeywordData( KWD_APP_KEYWORD, array(
				"keyword_space" => $keywordSpace,
				"id" => $keyData[ "keyid" ],
				"caption" => getKeywordCaption( $keyWord,
					formatText( $keyData[ "keyDesc" ])),
				"fullname" => $fullname ));
		}
	}

	writeKeywordData( KWD_APP_CLEAR );

	if( $isInHeader )
	{
		writeKeywordData( KWD_APP_SWITCH );

		$isInHeader = false;
	}

	foreach( $keywords as $keyData )
	{
		$keyData[ "keyWord" ] = preg_replace( '/\@$/', "", $keyData[ "keyWord" ]);

		unset( $requiredTabs[ $keyData[ "keyid" ]]);

		if( isset( $keyData["subcats" ]))
		{
			writeKeywordData( KWD_APP_OPEN, array( "id" => $keyData[ "keyid" ]));

			if( $keyData[ "keySubcat" ] != 0 )
				writeKeywordData( KWD_APP_HLINE );

			showKeywordSubcat( $keyData[ "subcats" ], $keyData[ "keyid" ],
				$parentKeyName.htmlspecialchars( $keyData[ "keyWord" ]).'"',
				$parentId != 0 ? htmlspecialchars( $keyData[ "keyWord" ]) : "" );

			writeKeywordData( KWD_APP_CLOSE );
		}
	}
}

$previousFullname = "";
$firstKeywordId = ""; // Id of the very first keyword, so we could open that tab by default.
$isInHeader = true;

showKeywordSubcat( $keywords );

// If there were no root keywords, close the <div class="header"> tag anyway.

if( $isInHeader )
{
	writeKeywordData( KWD_APP_SWITCH );
}

// Write the file to disk.

$fp = fopen( SCRIPTS."keywords_cache_.js", "a+" );

fputs( $fp, "var Captions = new Array(" );

foreach( $keywordCaptions as $key => $caption )
{
	fputs( $fp, ( $key > 0 ? "," : "" )."'".addslashes( $caption )."'" );
}

fputs( $fp, ");\n" );
fputs( $fp, $keywordData );
fputs( $fp, ");\n" );

fclose( $fp );

// Write first tab's id so we could "click" it to make it
// open by default (we can't get that id from _keywords.cache).

$fp = fopen( SCRIPTS."keywords_cache_.id", "a+" );
fputs( $fp, $firstKeywordId );
fclose( $fp );

// Write keyword descriptions

$fp = fopen( SCRIPTS."keywords_cache_desc_.js", "a+" );

fputs( $fp, "var Descriptions = new Array(" );

foreach( $keywordDesc as $key => $desc )
{
	fputs( $fp, ( $key > 0 ? "," : "" )."'".addslashes( $desc )."'" );
}

fputs( $fp, ");\n" );

fclose( $fp );

// Quickly replace the old files with the new ones.

foreach( glob( SCRIPTS."__keywordsCache*.*" ) as $filename )
{
	unlink( $filename );
}

rename( SCRIPTS."keywords_cache_.js", SCRIPTS."__keywordsCache_".time().".js" );
rename( SCRIPTS."keywords_cache_.id", SCRIPTS."__keywordsCache_".time().".id" );
rename( SCRIPTS."keywords_cache_desc_.js", SCRIPTS."__keywordsCacheDesc_".time().".js" );

?>
