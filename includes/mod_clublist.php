<? // Module: clubs list.

$select = "SELECT * FROM `clubs`,`cluExtData`";
$selectCount = "";
$where = "`cluid` = `cluEid` AND `cluHide` = '0' AND `cluIsProject` = '$findProjects'";
	//  AND `cluDesc` <> ''
$groupby = "";
$enableCategories = true;

if(isset($_GET["searchText"]) && $_GET["searchText"] != "") {
	$where = "($where) AND (SOUNDEX(`cluName`) = SOUNDEX('".addslashes($_GET["searchText"])."') ".
		"OR `cluName` LIKE '%".addslashes($_GET["searchText"])."%' ".
		"OR `cluDesc` LIKE '%".addslashes($_GET["searchText"])."%')";
}

$whereCount = $where;

if(!isset($_GET["order"])) $_GET["order"] = 0;

if(isset($_GET["order"])) {
	switch(intval($_GET["order"])) {
		case 1:
			$order = "`cluCreationDate`";
			break;
		case 2:
			$order = "`cluName`";
			break;
// RAND temporarily disabled
/*		case 3:
			$order = "RAND()";
			break;*/
		case 4:
			/*
			$selectCount = "SELECT COUNT(*) FROM `clubs`,`cluExtData`";
			$select = "SELECT `clubs`.*, `cluExtData`.*, COUNT(`watid`) AS watcher_count".
				" FROM `clubs`,`cluExtData`,`watches`";
			$where = "($where) AND `watCreator` = `cluid` AND `watType` = 'clu'";
			$groupby = "GROUP BY `watCreator`";
			$order = "watcher_count DESC";
			*/
			$order = "`cluWatcherCount` DESC";
			break;
		default:
			$order = "`cluCreationDate` DESC";
	}
}

if(!isset($_GET["limit"])) $_GET["limit"] = 0;

if(isset($_GET["limit"])) {
	switch(intval($_GET["limit"])) {
		case 1: $limit = 8; break;
		case 2: $limit = 24; break;
		case 3: $limit = 48; break;
		default: $limit = 12;
	}
}

$offset = isset($_GET["offset"]) ? intval($_GET["offset"]) : 0;
if($offset < 0) $offset = 0;

if( $selectCount == "" )
	$selectCount = preg_replace('/SELECT(.*)FROM/', "SELECT COUNT(*) FROM", $select);

$result = sql_query("$selectCount WHERE $whereCount");

$totalCount = mysql_result($result, 0);

$result = sql_query("$select WHERE $where $groupby ORDER BY $order LIMIT $offset,".($limit + 1));

$clublist = array();
while($cluData = mysql_fetch_assoc($result)) {
	$clublist[$cluData["cluid"]] = $cluData;
}

$getVars = array();
if(isset($_GET["offset"])) $getVars["offset"] = $_GET["offset"];
if(isset($_GET["limit"])) $getVars["limit"] = $_GET["limit"];
if(isset($_GET["order"])) $getVars["order"] = $_GET["order"];
if(isset($_GET["searchText"])) $getVars["searchText"] = $_GET["searchText"];

if(!isset($disableNav)) $disableNav = false;
if(!isset($enableCategories)) $enableCategories = false;

if(!$disableNav) {
	?>
	<?iefixStart()?>
		<?ob_start()?>
		<?
		navControls( $offset, $limit, $totalCount );
		?>
		<div class="a_center">
			<?
			if($enableCategories) {
				?>
				<form action="<?=url(".") ?>" method="get">
				<?
				foreach($_GET as $key => $value)
					if($key != "offset" && $key != "searchText")
						echo '<input name="'.htmlspecialchars($key).'" type="hidden" value="'.htmlspecialchars($value).'" />';
				?>
				<input type="text" name="searchText" value="<?=isset($_GET["searchText"]) ? htmlspecialchars($_GET["searchText"]) : "" ?>" />
				<input class="submit" type="submit" value="<?=_SEARCH ?>" style="vertical-align: middle" />
				</form>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				<?
			}
			?>
			<form action="<?=url(".")?>" method="get">
			<?
			foreach($_GET as $key => $value)
				if($key != "order" && $key != "limit")
					echo '<input name="'.htmlspecialchars($key).'" type="hidden" value="'.htmlspecialchars($value).'" />';
			?>
			<select name="order">
				<option <?=isset($_GET["order"]) && $_GET["order"] == 0 ? 'selected="selected"' : "" ?> value="0"><?=_NEWEST_FIRST ?></option>
				<option <?=isset($_GET["order"]) && $_GET["order"] == 1 ? 'selected="selected"' : "" ?> value="1"><?=_OLDEST_FIRST ?></option>
				<option <?=isset($_GET["order"]) && $_GET["order"] == 2 ? 'selected="selected"' : "" ?> value="2"><?=_BY_NAME ?></option>
				<option <?=isset($_GET["order"]) && $_GET["order"] == 4 ? 'selected="selected"' : "" ?> value="4"><?=_CLUB_POPULAR ?></option>
<!--				<option <?=isset($_GET["order"]) && $_GET["order"] == 3 ? 'selected="selected"' : "" ?> value="3"><?=_RANDOM ?></option> -->
			</select>
			<select name="limit">
				<option <?=isset($_GET["limit"]) && $_GET["limit"] == 1 ? 'selected="selected"' : "" ?> value="1"><?=fuzzy_number(8)?></option>
				<option <?=isset($_GET["limit"]) && $_GET["limit"] == 0 ? 'selected="selected"' : "" ?> value="0"><?=fuzzy_number(12)?></option>
				<option <?=isset($_GET["limit"]) && $_GET["limit"] == 2 ? 'selected="selected"' : "" ?> value="2"><?=fuzzy_number(24)?></option>
				<option <?=isset($_GET["limit"]) && $_GET["limit"] == 3 ? 'selected="selected"' : "" ?> value="3"><?=fuzzy_number(48)?></option>
			</select>
			<input class="submit" type="submit" value="<?=_UPDATE ?>" style="vertical-align: middle" />
			</form>
		</div>
		<?$clublistNavs = ob_get_contents(); ob_end_flush()?>
		<div class="hline">&nbsp;</div>
	<?iefixEnd()?>
	<?
}
else $clublistNavs = "";

$clubsToGo = $limit;

foreach($clublist as $cluData) {
	?>
	<div class="container2 mar_bottom notsowide">
	<?iefixStart()?>
		<div class="f_right mar_left mar_bottom">
			<?=getClubIcon($cluData["cluid"]) ?>
		</div>
		<div class="largetext mar_bottom">
			<b><a href="<?=url("club/".$cluData["cluid"])?>"><?=$cluData["cluName"] ?></a></b>
		</div>
		<div>
			<?=formatText($cluData["cluDesc"]) ?>
		</div>
		<div class="clear">&nbsp;</div>
	<?iefixEnd()?>
	</div>
	<?
	$clubsToGo--;
	if(!$clubsToGo) break;
}

if(!count($clublist))
	echo '<div><br /></div>';

echo '<div class="hline">&nbsp;</div>';
echo $clublistNavs;

unset($select);
unset($where);
unset($offset);
unset($limit);
unset($order);
unset($disableNav);
unset($enableCategories);

?>
<div class="clear">&nbsp;</div>
