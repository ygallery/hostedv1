<?

ob_start();

if( $isExtras )
{
	$_objects = "`extras`";
	$_objExtData = "`extExtData`";
}
else
{
	$_objects = "`objects`";
	$_objExtData = "`objExtData`";
}

if( $isExtras )
	$offsetVar = "extOffset";
else
	$offsetVar = "artOffset";

$offset = isset( $_GET[ $offsetVar ]) ? intval( $_GET[ $offsetVar ]) : 0;
$limit = 12;

if(!isset($_GET["view"]))
	$_GET["view"] = isset($_COOKIE["yGalView"]) ? $_COOKIE["yGalView"] : 0;

$view = intval($_GET["view"]) == 1 ? 1 : 0;

setcookie("yGalView",$view,strtotime("+9 years"),"/","." . $_config["galRoot"]); // give the user a fresh cookie

ob_start();

?>
<table cellspacing="0" cellpadding="2" border="0" width="100%">
<tr>
	<td width="200px">
		<div class="header">
			<?

			if( $isExtras )
				echo _SUBMIT_TYPE_EXTRA;
			elseif( $isCollabs )
				echo _COLLABS;
			elseif( $isClubs )
				echo _CLUBS;
			else
				echo _SUBMISSIONS;

			?>
		</div>
	</td>
	<td align="right" width="200px">
		<form action="<?=url("updates/".( $isExtras ? "extras" : "art" ))?>" method="get">
		<select class="smalltext" name="view" onchange="this.form.submit()">
			<option <?=$view == 0 ? 'selected="selected"' : ""?> value="0"><?= _UPD_BY_DATE ?></option>
			<option <?=$view == 1 ? 'selected="selected"' : ""?> value="1"><?= _UPD_BY_ARTISTS ?></option>
		</select>
		</form>
	</td>
</tr>
</table>
<?

if( isset( $_GET[ "popup" ]))
{
	ob_end_clean();
}
else
{
	ob_end_flush();

	?>
	<div class="artUpdatesInner">
	<?
}

if($view == 0)
	include(INCLUDES."mod_artview0.php");
else
	include(INCLUDES."mod_artview1.php");

if( isset( $_GET[ "popup" ]))
{
	$thumbCount = count( $upd_art );
	$baseURL = preg_replace( '/\/$/', "",  url( "/" ));
	$basefURL = preg_replace( '/\/$/', "",  urlf( "/" ));
	$currentURL = str_replace( $baseURL, "", $artRefreshURL );
	$nextURL = "";
	$firstURL = "";
	$prevURL = "";

	echo "$thumbCount\n$baseURL\n$basefURL\n$currentURL\n$nextURL\n$firstURL\n$prevURL\n";

	foreach( $upd_art as $data )
	{
		$title = formatText( $data[ "objTitle" ], false, true );
		$title = str_replace( "\n", "", $title );
		$titleNoTags = strip_tags( $title );

		echo $data[ "objid" ]."\n$title\n$titleNoTags\n";

		$sql = "SELECT `useUsername`, `useIsHidden`, `useLastAction`, `useIsModerator`, ".
			"`useIsSModerator`, `useIsDeveloper`, UNIX_TIMESTAMP(`useLastAction`) AS `useLastActionTS` ".
			"FROM `users`, `useExtData`".dbWhere( array(
			"useid*" => "useEid",
			"useid" => $data[ "objCreator" ]));

		$useResult = sql_query( $sql );
		$useData = mysql_fetch_assoc( $useResult );

		echo getOnlineStatus( $useData )."\n".$useData[ "useUsername" ]."\n";

		if( $data[ "objCollab" ] > 0 )
		{
			$sql = "SELECT `useUsername`, `useIsHidden`, `useLastAction`, `useIsModerator`, ".
				"`useIsSModerator`, `useIsDeveloper`, UNIX_TIMESTAMP(`useLastAction`) AS `useLastActionTS` ".
				"FROM `users`, `useExtData`".dbWhere( array(
				"useid*" => "useEid",
				"useid" => $data[ "objCollab" ]));

			$useResult = sql_query( $sql );
			$useData = mysql_fetch_assoc( $useResult );
			$useStatus = getOnlineStatus( $useData );

			echo "$useStatus\n".$useData[ "useUsername" ]."\n";
		}
		else
		{
			echo "0\n-\n";
		}

		if( $data[ "objForClub" ] > 0 )
		{
			$sql = "SELECT `cluName` FROM `clubs`".dbWhere( array( "cluid" => $data[ "objForClub" ]));

			$cluResult = sql_query( $sql );
			$cluData = mysql_fetch_row( $cluResult );

			$title = $cluData[ 0 ];
			$title = str_replace( "\n", "", $title );

			echo $data[ "objForClub" ]."\n$title\n";
		}
		else
		{
			echo "0\n-\n";
		}

		if( $isExtras )
		{
			$thumb = $data[ "objThumbURL" ];
		}
		else
		{
			$thumb = applyIdToPath( "files/thumbs/", $data[ "objid" ])."-".
				preg_replace( '/[^0-9]/', "", $data[ "objLastEdit" ]);

			$thumb = str_replace( "files/thumbs/", "", $thumb );
		}

		while( $data[ "objThumbWidth" ] < 50 || $data[ "objThumbHeight" ] < 50 )
		{
			$data[ "objThumbWidth" ] *= 2;
			$data[ "objThumbHeight" ] *= 2;
		}

		while( $data[ "objThumbWidth" ] > 150 || $data[ "objThumbHeight" ] > 150 )
		{
			$data[ "objThumbWidth" ] /= 2;
			$data[ "objThumbHeight" ] /= 2;
		}

		echo ( $data[ "objMature" ] ? "1" : "0" )."\n".
			( $isExtras ? "1" : "0" )."\n".
			$data[ "objThumbWidth" ]."\n".
			$data[ "objThumbHeight" ]."\n$thumb\n";
	}

	exit();
}

$all_ids = array();

if( !count( $upd_art ))
{
	echo '<div>'._UPD_NO_SUBMISSIONS.'</div>';
}
else
{
	$cols = 0;

	iefixStart();

	reset( $upd_art );

	foreach( $upd_art as $key => $data )
	{
		if($_objects == "`objects`")
		{
			$result = sql_query("SELECT $_objects.*, $_objExtData.`objExtension` FROM $_objects INNER JOIN $_objExtData ON `objid` = `objEid` WHERE `objPending` = '0' AND `objDeleted` = '0' AND `objid` = '".$data["objid"]."' LIMIT 1");
		}
		else
		{
			$result = sql_query("SELECT $_objects.*, '' AS `objExtension` FROM $_objects WHERE `objPending` = '0' AND `objDeleted` = '0' AND `objid` = '".$data["objid"]."' LIMIT 1");
		}

		if(!$objData = mysql_fetch_assoc($result))
			continue;

		while( $objData[ "objThumbWidth" ] < 50 || $objData[ "objThumbHeight" ] < 50 )
		{
			$objData[ "objThumbWidth" ] *= 2;
			$objData[ "objThumbHeight" ] *= 2;
		}

		while( $objData[ "objThumbWidth" ] > 150 || $objData[ "objThumbHeight" ] > 150 )
		{
			$objData[ "objThumbWidth" ] /= 2;
			$objData[ "objThumbHeight" ] /= 2;
		}

		$anchor = url("view/".( $isExtras ? "e" : "" ).$objData["objid"]);

		if(!$isExtras && $objData[ "objThumbDefault" ])
		{
			$src = urlf()."images/litthumb.png";
		}
		else
		{
		$src = $isExtras ? $objData[ "objThumbURL" ] :
			urlf().applyIdToPath("files/thumbs/", $objData["objid"])."-".preg_replace('/[^0-9]/', "", $objData["objLastEdit"]).".jpg";
		}
		
		$useData = getUserData( $objData[ "objCreator" ]);

		$objTitle = formatText($objData["objTitle"], false, true).'</div><div> ';

		if( $objData[ "objCollab" ] > 0 )
		{
			$objTitle .= sprintf( _BY_AND,
				getUserLink( $objData[ "objCreator" ]),
				getUserLink( $objData[ "objCollab" ]));
		}
		else
		{
			$objTitle .= sprintf( _BY,
				getUserLink( $objData[ "objCreator" ]));
		}

		if( $objData[ "objForClub" ] > 0 )
		{
			$result = sql_query( "SELECT `cluName` FROM `clubs` ".
				"WHERE `cluid` = '".$objData[ "objForClub" ]."'" );

			$club = '<a href="'.url( "club/".$objData[ "objForClub" ]).'">'.
				mysql_result( $result, 0 ).'</a>';

			$objTitle .= '</div><div> '.sprintf( _IN, $club );
		}

		$id = generateId( "au_v" );
		$all_ids[] = $id;
		$divId = "";
		$starId = "";

		echo '<div '.$divId.' class="gallery_col a_center"'.
			($view == 1 ? ' style="width: 31%"' : "").'>'.
			'<div class="padded '.($cols < 3 ? " mar_right" : "").'">'.
			'<a href="'.$anchor.'">';

		echo '<img alt="'.htmlspecialchars(strip_tags($objTitle)).'" class="thumb'.
			($objData["objMature"] ? " mature" : "").
			'" src="'.$src.'" title="'.htmlspecialchars(strip_tags($objTitle)).
			'" width="'.$objData["objThumbWidth"].
			'" height="'.$objData["objThumbHeight"].'" /></a>';

		echo '</div><div>';

		echo '<input class="checkbox" id="'.$id.'" name="'.$key.'" type="checkbox" />';

		if( in_array($objData[ "objExtension" ], array("txt", "html")) )
		{
			echo getIMG(url()."images/emoticons/pm.png", 'alt="' . $objData["objExtension"] . '"').' ';
		}
		else
		{
			echo getIMG(url()."images/emoticons/submission.png", 'alt="' . $objData["objExtension"] . '"').' ';
		}

		echo $objTitle.'</div></div>';

		$cols++;

		if( $cols >= $maxcols )
		{
			$cols = 0;

			echo '<div class="clear">&nbsp;</div>';
		}
	}
	echo '<div class="clear">&nbsp;</div>';
	iefixEnd();
}

if( !isset( $_GET[ "popup" ]))
{
	?>
	</div>
	<?

	if( $view == 1 )
	{
		?>
		</td><td style="vertical-align: top" width="20%">
		<?=$artistsList?>
		</td></tr></table>
		<?
	}

	?>
	</div>
	<?
}

?>
<div class="sep">
	<?

	$totalCount = $isExtras ? $_auth[ "useUpdExt" ] : $_auth[ "useUpdObj" ];

	ob_start();

	if( $isExtras )
		$subpage = "extras";
	elseif( $isClubs )
		$subpage = "clubs";
	elseif( $isCollabs )
		$subpage = "collabs";
	else
		$subpage = "art";

	$getvars = array( $offsetVar => "OFFSET" );

	navControls( $offset, $limit, $totalCount,
		url( $_cmd[ 0 ]."/".$subpage, $getvars ),
		"", "", "", $offsetVar );

	$navControlsStr = ob_get_contents();

	ob_end_clean();

	echo "%navControls%";

	?>
	<button type="button" class="submit" onclick="<?
		foreach ($all_ids as $id) echo "el=get_by_id('$id'); el.checked=true; ";
		?>return false;"><?=_SELECT_ALL ?></button>
	<button class="submit" name="markAsRead" type="submit"><?=_MARK_AS_VIEWED ?></button>
	<?php
	 if( $isExtras) {
		//Button for extras will go here when done
		}
	else {     ?>
		<button class="submit" name="markAllObjects" type="submit" onclick="return confirm('<?= sprintf( _CONFIRM_MARK_AS_VIEWED, _SUBMISSIONS)?>')"><?=_MARK_ALL_AS_VIEWED ?></button>
	<?php } ?>
	<div class="clear">&nbsp;</div>
<?

if( !isset( $_GET[ "popup" ]))
{
	?>
	</div>
	<? // id="artUpdatesInner"
}

?>
</form>
</div>
<?

$content = ob_get_contents();

ob_end_clean();

$content = str_replace( "%navControls%", $navControlsStr, $content );

echo $content;

?>
