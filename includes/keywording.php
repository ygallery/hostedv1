<?php

// -----------------------------------------------------------------------------
// Recursive fetching of the keywords list.

function fetchKeywords( $keySubcat )
{
	$result = sql_query( "SELECT * FROM `keywords` ".
		"WHERE `keySubcat` = '$keySubcat' ORDER BY `keyWord`" );

	$keywords = array();

	while( $rowData = mysql_fetch_assoc( $result ))
	{
		$keyid = $rowData[ "keyid" ];

		$rowData[ "keyWord" ] = trim( preg_replace( '/^.*\|/', "",
			$rowData[ "keyWord" ]));

		$keywords[ $keyid ] = $rowData;
		$subKeywords = fetchKeywords( $keyid );

		if( count( $subKeywords ))
			$keywords[ $keyid ][ "subcats" ] = $subKeywords;
	}

	mysql_free_result( $result );
	
	return $keywords;
}

// -----------------------------------------------------------------------------
// Makes the user choose at least one keyword under each of the root keyword
// groups, e.g. at least one Category, at least one Mood, etc. Returns the list
// of root keywords groups that still require user's choice, otherwise
// returns "".

function requireRootKeywords( $enteredKeywords )
{
	$userKeywords = preg_split('/\s/', $enteredKeywords, 200, PREG_SPLIT_NO_EMPTY);

	// Fetch the root keywords list.

	$result = sql_query( "SELECT * FROM `keywords` WHERE `keySubcat` = '0'" );

	$rootKeywords = array();

	while( $rowData = mysql_fetch_assoc( $result ))
	{
		$rootKeywords[ $rowData[ "keyid" ]] = $rowData[ "keyWord" ];
	}

	mysql_free_result( $result );

	// Go through the user-entered keywords and unset root keyword groups
	// if they contain at least one user-entered keyword.

	foreach( $userKeywords as $keyid )
	{
		$kid = $keyid;

		while( $kid != 0 )
		{
			if( isset( $rootKeywords[ $kid ]))
			{
				unset( $rootKeywords[ $kid ]);
				break;
			}

			// get this keyword's parent keyword id

			$result = sql_query( "SELECT `keySubcat` FROM `keywords` ".
				"WHERE `keyid` = '$kid' LIMIT 1" );

			if( !mysql_num_rows( $result ))
				break;

			$kid = mysql_result( $result, 0 );

			mysql_free_result( $result );
		}
	}

	// Make the list of required root keyword groups.

	$required = "";

	natsort( $rootKeywords );

	foreach( $rootKeywords as $rootkeyid => $rootName )
	{
		$rootName = trim( preg_replace( '/^.*\|/', "", $rootName ));
		$rootName = preg_replace( '/\@$/', "", $rootName );
		$required .= "<br />\n&bull; ".$rootName;
	}

	if( $required == "" )
		return "";

	return _SUBMIT_REQUIRE_KEYWORDS.$required;
}

?>
