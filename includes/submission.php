<?

// -----------------------------------------------------------------------------
// Returns server id we're going to upload the submission to.

function getSubmissionServer()
{
	return( 1 );
}

// -----------------------------------------------------------------------------
// Updates useExtData.useObjCount (total number of submissions) for specified
// user.

function updateObjCount( $useid )
{
	if( $useid == 0 )
	{
		return;
	}

	$sql = "SELECT COUNT(*) FROM `objects`".dbWhere( array(
		"objDeleted" => 0,
		"objPending" => 0,
		"objCreator" => $useid ));

	$cntResult = sql_query( $sql );

	$count = mysql_result( $cntResult, 0 );

	mysql_free_result( $cntResult );

	$sql = "UPDATE `useExtData`".dbSet( array(
		"useObjCount" => $count )).dbWhere( array(
		"useEid" => $useid ));

	sql_query( $sql );
}

// -----------------------------------------------------------------------------
// Add object to the database and set is title/comment.

function submitNewTitle( $title, $comment, $mature, $forClub, $folder = 0,
	$collab = 0, $gift = 0, $forClub2 = 0, $forClub3 = 0 )
{
	global $_auth;

	$server = getSubmissionServer();
	$userIp = getHexIp( $_SERVER[ "REMOTE_ADDR" ]);

	if( $forClub2 == $forClub )
	{
		$forClub2 = 0;
	}

	if( $forClub3 == $forClub || $forClub3 == $forClub2 )
	{
		$forClub3 = 0;
	}

	// Add object to the database.

	$sql = "INSERT INTO `objects`".dbValues( array(
		"objDeleted" => 1, // hide object initially
		"objTitle" => $title,
		"objCreator" => $_auth[ "useid" ],
		"objSubmitDate!" => "NOW()",
		"objLastEdit!" => "NOW()",
		"objMature" => $mature,
		"objGuestAccess" => $_auth[ "useGuestAccess" ],
		"objForClub" => $forClub,
		"objForClub2" => $forClub2,
		"objForClub3" => $forClub3,
		"objFolder" => $folder,
		"objCollab" => $collab,
		"objForUser" => $gift ));

	sql_query( $sql );

	// Get ID of the newly added object.

	$objid = mysql_insert_id();

	sql_where( array( "cloObject" => $objid ));
	sql_delete( "clubObjects" );

	if( $forClub > 0 )
	{
		sql_values( array( "cloObject" => $objid, "cloClub" => $forClub ));
		sql_insert( "clubObjects" );
	}

	if( $forClub2 > 0 )
	{
		sql_values( array( "cloObject" => $objid, "cloClub" => $forClub2 ));
		sql_insert( "clubObjects" );
	}

	if( $forClub3 > 0 )
	{
		sql_values( array( "cloObject" => $objid, "cloClub" => $forClub3 ));
		sql_insert( "clubObjects" );
	}

	// Add extended data to the database.

	$sql = "INSERT INTO `objExtData`".dbValues( array(
		"objEid" => $objid,
		"objServer" => $server,
		"objComment" => $comment,
		"objSubmitIp" => $userIp,
		"objEditIp" => $userIp ));

	sql_query( $sql );

	updateSearchCache( $objid );
	updateObjCount( $_auth[ "useid" ]);
	updateObjCount( $collab );

	return( $objid );
}

// -----------------------------------------------------------------------------
// Modify object's title/comment.

function submitModifiedTitle( $objid, $title, $comment, $mature, $forClub,
	$folder = 0, $collab = 0, $gift = 0, $forClub2 = 0, $forClub3 = 0 )
{
	$oldCollab = 0;
	$collabConfirmed = 0;
	$objCreator = 0;

	$sql = "SELECT `objCollab`, `objCollabConfirmed`, `objCreator` ".
		"FROM `objects`, `objExtData`".dbWhere( array(
		"objid*" => "objEid", "objid" => $objid ));

	$objResult = sql_query( $sql );

	if( $objData = mysql_fetch_assoc( $objResult ))
	{
		$oldCollab = $objData[ "objCollab" ];
		$collabConfirmed = $objData[ "objCollabConfirmed" ];
		$objCreator = $objData[ "objCreator" ];
	}

	mysql_free_result( $objResult );

	if( $forClub2 == $forClub )
	{
		$forClub2 = 0;
	}

	if( $forClub3 == $forClub || $forClub3 == $forClub2 )
	{
		$forClub3 = 0;
	}

	sql_where( array( "cloObject" => $objid ));
	sql_delete( "clubObjects" );

	if( $forClub > 0 )
	{
		sql_values( array( "cloObject" => $objid, "cloClub" => $forClub ));
		sql_insert( "clubObjects" );
	}

	if( $forClub2 > 0 )
	{
		sql_values( array( "cloObject" => $objid, "cloClub" => $forClub2 ));
		sql_insert( "clubObjects" );
	}

	if( $forClub3 > 0 )
	{
		sql_values( array( "cloObject" => $objid, "cloClub" => $forClub3 ));
		sql_insert( "clubObjects" );
	}

	$values = array(
		"objTitle" => $title,
		"objMature" => $mature,
		"objForClub" => $forClub,
		"objForClub2" => $forClub2,
		"objForClub3" => $forClub3,
		"objFolder" => $folder );

	if( !$collabConfirmed || atLeastSModerator() )
	{
		$values[ "objCollab" ] = $collab;
	}

	$values[ "objForUser" ] = $gift;

	$sql = "UPDATE `objects`".dbSet( $values ).dbWhere( array(
		"objid" => $objid ));

	sql_query( $sql );

	$values = array( "objComment" => $comment );

	if( $collab == 0 && atLeastSModerator() )
	{
		$values[ "objCollabConfirmed" ] = 0;
	}

	$sql = "UPDATE `objExtData`".dbSet( $values ).dbWhere( array(
		"objEid" => $objid ));

	sql_query( $sql );

	updateSearchCache( $objid );
	updateObjCount( $objCreator );
	updateObjCount( $collab );

	if( $collab != $oldCollab )
	{
		updateObjCount( $oldCollab );
	}
}

// -----------------------------------------------------------------------------
// Add keywords to the submission.

function submitKeywords( $objid, $keywordList )
{
	// Delete previous keywords, if any.

	$sql = "DELETE FROM `objKeywords`".dbWhere( array(
		"objKobject" => $objid ));

	sql_query( $sql );

	// Add newly chosen keywords.

	$idList = preg_split( '/\s/', $keywordList, -1, PREG_SPLIT_NO_EMPTY );

	foreach( $idList as $keyid )
	{
		$keyid = intval( $keyid );

		if( $keyid > 0 )
		{
			$sql = "SELECT `keyid` FROM `keywords`".dbWhere( array(
				"keyid" => $keyid ));

			$result = sql_query( $sql );

			if( mysql_num_rows( $result ) > 0 && $keyid > 0 )
			{
				$sql = "INSERT INTO `objKeywords`".dbValues( array(
					"objKobject" => $objid,
					"objKkeyword" => $keyid ));

				sql_query( $sql );
			}
		}
	}

	updateFilterKeywords( $objid );
}

// -----------------------------------------------------------------------------
// Upload submission to /files/data/#/#####/

function submitImage( $objid, $fileVar, $thumbVar, &$thumbError,
	&$imageChanged )
{
	global $_config, $_auth;

	$imageChanged = true;

	if( $fileVar == "" && $thumbVar == "" )
	{
		$imageChanged = false;
		$thumbError = _SUBMIT_THUMB_REQUIRED;
		return false;
	}

	// Query old objLastEdit, so we can delete the old thumb and file.

	$sql = "SELECT `objExtension`, `objLastEdit`, `objFilename` ".
		"FROM `objects`, `objExtData`".dbWhere( array(
		"objid*" => "objEid",
		"objEid" => $objid ));

	$result = sql_query( $sql );

	$oldData = mysql_fetch_assoc( $result );

	// Delete the old thumbnail and file.

	$oldThumbFilename = applyIdToPath( "files/thumbs/", $objid )."-".
		preg_replace( '/[^0-9]/', "", $oldData[ "objLastEdit" ]).".jpg";

	$oldFilename = applyIdToPath( "files/data/", $objid )."-".
		preg_replace( '/[^0-9]/', "", $oldData[ "objLastEdit" ]).".".
		$oldData[ "objExtension" ];

	/*
	//Do not delete old files
	if(file_exists($oldThumbFilename)) @unlink($oldThumbFilename);
	if(file_exists($oldFilename)) @unlink($oldFilename);
	*/

	// Update objLastEdit and get the new value, so we have the new objLastEdit
	// in $newRevisionDate and the old value in $oldData["objLastEdit"].

	$sql = "UPDATE `objects`".dbSet( array(
		"objLastEdit!" => "NOW()" )).dbWhere( array(
		"objid" => $objid ));

	sql_query( $sql );

	$sql = "SELECT `objLastEdit` FROM `objects`".dbWhere( array(
		"objid" => $objid ));

	$result = sql_query( $sql );

	if( mysql_num_rows( $result ) > 0 )
	{
		$newRevisionDate = preg_replace( '/[^0-9]/', "", mysql_result( $result, 0 ));
	}
	else
	{
		$newRevisionDate = date( "YmdHis" );
	}

	// Make a thumbnail and store it to /files/thumbs/#/#####/.

	$thumbFilename = applyIdToPath( "files/thumbs/", $objid )."-".
		$newRevisionDate.".jpg";

	list( $thumbMaxWidth, $thumbMaxHeight ) = preg_split( '/x/',
		$_config[ "thumbResolution" ]);

	if( $thumbVar == "" )
	{
		// Automatic thumbnail generation.

		if( $fileVar == "" ||
			!thumbifyImage( $_FILES[ $fileVar ][ "tmp_name" ],
			$thumbFilename, $thumbMaxWidth, $thumbMaxHeight ))
		{
			$thumbError = _SUBMIT_THUMB_REQUIRED;
			return false;
		}
	}
	else
	{
		// Manually added thumbnail.

		if( !thumbifyImage( $_FILES[ $thumbVar ][ "tmp_name" ],
			$thumbFilename, $thumbMaxWidth, $thumbMaxHeight ))
		{
			$thumbError = _SUBMIT_THUMB_ERROR;
			return false;
		}
	}

	// Gather thumbnail size information.

	if( file_exists( $thumbFilename ))
	{
		$size = getimagesize( $thumbFilename );
		$thumbWidth = $size[ 0 ];
		$thumbHeight = $size[ 1 ];
	}
	else
	{
		// If $thumbWidth == $thumbHeight == 0 it means there is no thumbnail.

		$thumbWidth = 0;
		$thumbHeight = 0;
	}

	// Upload the file.

	$imageFilename = applyIdToPath( "files/data/", $objid )."-".$newRevisionDate;

	if( $fileVar != "" )
	{
		uploadFile( $fileVar, $imageFilename, $extension );
	}
	else
	{
		$imageChanged = false;
		$extension = $oldData[ "objExtension" ];
		rename( $oldFilename, $imageFilename.".".$extension );
	}

	// Gather image size information.

	$imageFilename .= ".".$extension;

	$imageFileSize = filesize( $imageFilename );

	$imageNonResizeable = true;

	if( $oldData[ "objExtension" ] == "txt" )
	{
		$imageWidth = 0;
		$imageHeight = 0;
	}
	else
	{
		$size = getimagesize( $imageFilename );
		$imageWidth = $size[ 0 ];
		$imageHeight = $size[ 1 ];

		if( $size[ 2 ] == 2 || $size[ 2 ] == 3 )
		{
			$imageNonResizeable = false;
		}
	}

	// Make a preview image, if possible.

	$previewWidth = 0;
	$previewHeight = 0;

	if( $imageWidth > 0 && $imageHeight > 0 && !$imageNonResizeable )
	{
		$previewFilename = applyIdToPath( "files/preview/", $objid )."-".
			$newRevisionDate.".jpg";

		$coeff = sqrt( $_config[ "previewMaxArea" ] / ( $imageWidth * $imageHeight ));

		// Require significant size reduction, so that images wouldn't become
		// very blurry with just an unnoticeable size change.

		if( $coeff > 0.9 )
			$coeff = 1.0;

		$previewMaxWidth = round( $imageWidth * $coeff );
		$previewMaxHeight = round( $imageHeight * $coeff );
		
		
		//Set a maximum width of 700px for the image preview and keep the image to it's
		//original ratio. This reduces submission view bandwidth and makes it so each new
		//image is of a consistent size
		$ratio = $previewMaxWidth/$previewMaxHeight;
		
		$previewMaxWidth = 700;
		
		$previewMaxHeight = $previewMaxWidth / $ratio;
		
		//end ratio math
		
		thumbifyImage( $imageFilename, $previewFilename,
			$previewMaxWidth, $previewMaxHeight, 86 );

		if( file_exists( $previewFilename ))
		{
			$size = getimagesize( $previewFilename );
			$previewWidth = $size[ 0 ];
			$previewHeight = $size[ 1 ];

			// In case we've accidentally generated a larger file of the same
			// resolution, throw it away (why would we need a preview file that
			// is larger than the original image file?)

			if( $imageWidth == $previewWidth && $imageHeight == $previewHeight &&
				( filesize( $imageFilename ) * 0.8 ) < filesize( $previewFilename ))
			{
				$previewWidth = 0;
				$previewHeight = 0;
				unlink( $previewFilename );
			}
		}
	}

	// Update the filename in the database.

	$objFilename = $fileVar != "" ? $_FILES[ $fileVar ][ "name" ] :
		$oldData[ "objFilename" ];

	$sql = "UPDATE `objects`".dbSet( array(
		"objLastEdit" => $newRevisionDate,
        "objThumbWidth" => $thumbWidth,
		"objThumbHeight" => $thumbHeight,
		"objThumbDefault" => "0" )).dbWhere( array(
		"objid" => $objid ));

	sql_query( $sql );

	$userIp = getHexIp($_SERVER["REMOTE_ADDR"]);

	$sql = "UPDATE `objExtData`".dbSet( array(
		"objImageWidth" => $imageWidth,
		"objImageHeight" => $imageHeight,
		"objImageSize" => $imageFileSize,
		"objPreviewWidth" => $previewWidth,
		"objPreviewHeight" => $previewHeight,
		"objExtension" => $extension,
		"objEditIp" => $userIp,
		"objFilename" => $objFilename )).dbWhere( array(
		"objEid" => $objid ));

	sql_query( $sql );

	// Touch user's last submission time.

	$sql = "UPDATE `users`".dbSet( array(
		"useLastSubmission!" => "NOW()" )).dbWhere( array(
		"useid" => $_auth[ "useid" ]));

	sql_query( $sql );

	return( true );
}

// -----------------------------------------------------------------------------

function updateFilterKeywords( $objid )
{
	global $_config;

	$KW = array();

	$kwList = preg_split( '/[\s\,\;]/', $_config[ "filterKeywordAssoc" ], 64, PREG_SPLIT_NO_EMPTY );

	foreach( $kwList as $kw1 )
	{
		list( $fltid, $keyid ) = preg_split( '/\=/', $kw1, 2 );

		$KW[ $fltid ] = $keyid;
	}

	// Update object's objMature field by enabling all required filters
	// according to the chosen keywords.

	$filters = getFiltersByObject( $objid );

	/*
	$sql = "SELECT `objMature` FROM `objects`".dbWhere( array(
		"objid" => $objid ));

	$objResult = sql_query( $sql );

	if( $objData = mysql_fetch_assoc( $objResult ))
	{
		$filters2 = preg_split( '/\,/', $objData[ "objMature" ], 0, PREG_SPLIT_NO_EMPTY );

		$filters = array_unique( array_merge( $filters, $filters2 ));
	}

	mysql_free_result( $objResult );
	*/

	$sql = "UPDATE `objects`".dbSet( array(
		"objMature" => implode( ",", $filters ))).dbWhere( array(
		"objid" => $objid ));

	sql_query( $sql );

	// Remove old keywords under "Filters".

	$sql = "DELETE FROM `objKeywords` WHERE `objKkeyword` IN ('".implode( "','", $KW )."') ".
		"AND `objKobject` = '".intval( $objid )."'";

	sql_query( $sql );

	// Add new keywords under "Filters" according to the filters enabled for
	// the object.

	if( count( $filters ) == 0 )
	{
		$filters = array( 0 );
	}

	foreach( $filters as $filter )
	{
		if( isset( $KW[ $filter ]))
		{
			$sql = "INSERT INTO `objKeywords`".dbValues( array(
				"objKobject" => $objid,
				"objKkeyword" => $KW[ $filter ]));

			sql_query( $sql );
		}
	}
}

// -----------------------------------------------------------------------------
// Erases a submission, including all comments attached to it.

function eraseSubmission( $objid, $isExtras = false, $checkSMod = true )
{
	if( $checkSMod && !atLeastSModerator() )
	{
		return;
	}

	$_objects = $isExtras ? "`extras`" : "`objects`";
	$_objExtData = $isExtras ? "`extExtData`" : "`objExtData`";

	// Delete comments

	eraseComments( $isExtras ? "ext" : "obj", $objid );

	// Erase object-related files

	if( !$isExtras )
	{
		$oldFiles = glob( applyIdToPath( "files/data/", $objid )."-*", GLOB_NOESCAPE );

		if( is_array( $oldFiles ) && count( $oldFiles ) > 0 )
		{
			foreach( $oldFiles as $oldFile )
			{
				unlink( $oldFile ); // Delete old files
			}
		}

		$oldFiles = glob( applyIdToPath( "files/thumbs/", $objid )."-*", GLOB_NOESCAPE );

		if( is_array( $oldFiles ) && count( $oldFiles ) > 0 )
		{
			foreach( $oldFiles as $oldFile )
			{
				unlink( $oldFile ); // Delete old files
			}
		}
	}

	// Delete object

	sql_query( "DELETE FROM $_objects WHERE `objid` = '$objid'" );
	sql_query( "DELETE FROM $_objExtData WHERE `objEid` = '$objid'" );

	if( !$isExtras )
	{
		sql_query( "DELETE FROM `favourites` WHERE `favObj` = '$objid'" );
		sql_query( "DELETE FROM `objKeywords` WHERE `objKobject` = '$objid'" );
		sql_query( "DELETE FROM `searchcache` WHERE `seaObject` = '$objid'" );
	}
}

// -----------------------------------------------------------------------------
// Erases all comments attached to specified object.

function eraseComments( $comObjType, $comObj )
{
	if( !atLeastSModerator() )
	{
		return;
	}

	$sql = "SELECT `comid` FROM `comments`".dbWhere( array(
		"comObjType" => $comObjType,
		"comObj" => $comObj ));

	$comResult = sql_query( $sql );

	while( $comData = mysql_fetch_assoc( $comResult ))
	{
		$sql = "DELETE FROM `comments`".dbWhere( array(
			"comid" => $comData[ "comid" ]));

		sql_query( $sql );

		eraseComments( "com", $comData[ "comid" ]);
	}

	mysql_free_result( $comResult );
}

?>