<?

$result = sql_query( "SELECT `useid`,`useUsername` FROM `users`".dbWhere( array(
	"useUsername" => $_cmd[ 1 ] ))."LIMIT 1");

if( !$useData = mysql_fetch_assoc( $result ))
{
	include( INCLUDES."p_notfound.php" );
	return;
}

$useUsername = strtolower( $useData[ "useUsername" ]);

if( $useData[ "useid" ] != $_auth[ "useid" ] && !atLeastModerator() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

$_pollUser = $useData[ "useid" ];

$_documentTitle = $useData[ "useUsername" ].": "._SUBMISSIONS;

?>
<div class="header">
	<div class="f_right mar_left a_center normaltext">
		<?= getUserAvatar( "", $useData[ "useid" ], true ) ?>
	</div>
	<div class="f_left header_title">
		<?= $useData[ "useUsername" ] ?>
		<div class="subheader"><?= _SUBMISSIONS ?></div>
	</div>
	<?

	$active = 2;

	include( INCLUDES."mod_usermenu.php" );

	?>
</div>
<div class="container">
	<?

	$active = 4;

	include( INCLUDES."mod_usersubmenu.php" );

	if( $useData[ "useid" ] == $_auth[ "useid" ])
	{
		?>
		<div class="f_left">
			<button onclick="document.location='<?= url( "purgetrash" ) ?>';">
			<?=getIMG(url()."images/emoticons/cancel.png")?> Purge old trash
			</button>
		</div>
		<?
	}

	include_once( INCLUDES."gallery.php" );

	showThumbnails( array(
		"select" => "SELECT * FROM `objects`, `objExtData`",
		"where" => "`objEid` = `objid` ".
			"AND `objPending` = '0' AND `objDeleted` = '1' ".
			"AND `objCreator` = '".$useData[ "useid" ]."' ".
			( atLeastModerator() ? "" : "AND `objDeletedBy` = '".$useData[ "useid" ]."'" ),
		"showDeleted" => true ));

	?>
</div>
