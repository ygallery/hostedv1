<?=iefixStart()?>

<div class="clear">&nbsp;</div>

<div class="normaltext mar_bottom">
<?

if( $active == 1 || $active == 5 )
{
	if( isLoggedIn() )
	{
		$showFolders = isset( $_GET[ "folders" ]) ? intval( $_GET[ "folders" ]) :
			( isset( $_COOKIE[ "yGalFolders" ]) ? intval( $_COOKIE[ "yGalFolders" ]) : 1 );

		setcookie( "yGalFolders", $showFolders, strtotime( "+9 years" ), "/", ".".$_config[ "galRoot" ]);

		?>
		<div class="f_right" style="padding-top: 4px">
			<form action="<?= url( "." ) ?>" method="get">
				<?= getIMG( urlf()."images/emoticons/folder.png" ) ?>
				<select class="smalltext" name="folders" onchange="this.form.submit()">
					<option <?= $showFolders == 0 ? 'selected="selected"' : "" ?>
						value="0"><?= _SET_FOLDERS_OFF ?></option>
					<option <?= $showFolders == 1 ? 'selected="selected"' : "" ?>
						value="1"><?= _SET_FOLDERS_ON ?></option>
				</select>
			</form>
		</div>
		<?
	}
	else
	{
		$showFolders = 1;
	}
}

?>
<div class="tab<?=$active == 1 ? " tab_active" : "" ?>" onclick="document.location='<?=url("gallery/".$useUsername)?>'"><?=getIMG(url()."images/emoticons/submission.png")?> <?=_SUBMISSIONS ?></div>
<?

$proResult = sql_query( "SELECT COUNT(*) FROM `objects` ".
	"LEFT JOIN `clubs` ON(`objForClub` = `cluid`) ".
	"WHERE `objCreator` = '".$useData[ "useid" ]."' ".
	"AND `objForClub` <> '0' AND `cluIsProject` = '1' LIMIT 1" );

if( mysql_result( $proResult, 0 ) > 0 )
{
	?>
	<div class="tab<?=$active == 3 ? " tab_active" : "" ?>"
		onclick="document.location='<?=url("galleryclubs/".$useUsername)?>'">
		<?=getIMG(url()."images/emoticons/club.png")?> <?=_PROJECTS ?></div>
	<?
}

// Collabs

$where = "`objid` = `objEid` ".
	"AND ((`objCollab` = '".$useData[ "useid" ]."' AND `objCollabConfirmed` = '1') ".
	"OR (`objCollab` > '0' AND `objCreator` = '".$useData[ "useid" ]."')) ".
	"AND `objDeleted` = '0' AND `objPending` = '0'";

applyObjFilters( $where );

$colResult = sql_query( "SELECT COUNT(*) FROM `objects` USE INDEX (`objCollab`), `objExtData` WHERE $where" );

if( mysql_result( $colResult, 0 ) > 0 )
{
	?>
	<div class="tab<?=$active == 6 ? " tab_active" : "" ?>"
		onclick="document.location='<?=url("collabs/".$useUsername)?>'">
		<?=getIMG(url()."images/emoticons/club.png")?> <?=_COLLABS ?></div>
	<?
}

// For others

$where = "`objid` = `objEid` ".
	"AND `objCreator` = '".$useData[ "useid" ]."' ".
	"AND `objForUser` <> '0' ".
	"AND `objDeleted` = '0' AND `objPending` = '0'";

applyObjFilters( $where );

$colResult = sql_query( "SELECT COUNT(*) FROM `objects`, `objExtData` WHERE $where" );

if( mysql_result( $colResult, 0 ) > 0 )
{
	?>
	<div class="tab<?=$active == 8 ? " tab_active" : "" ?>"
		onclick="document.location='<?=url("forothers/".$useUsername)?>'">
		<?=getIMG(url()."images/emoticons/gift.png")?> <?=_GIFTS_OUT ?></div>
	<?
}

// From others

$where = "`objid` = `objEid` ".
	"AND `objForUser` = '".$useData[ "useid" ]."' ".
	"AND `objDeleted` = '0' AND `objPending` = '0'";

applyObjFilters( $where );

$colResult = sql_query( "SELECT COUNT(*) FROM `objects`, `objExtData` WHERE $where" );

if( mysql_result( $colResult, 0 ) > 0 )
{
	?>
	<div class="tab<?=$active == 7 ? " tab_active" : "" ?>"
		onclick="document.location='<?=url("fromothers/".$useUsername)?>'">
		<?=getIMG(url()."images/emoticons/gift.png")?> <?=_GIFTS ?></div>
	<?
}

// Extras

if( isExtras() )
{
	$where = "`objCreator` = '".intval( $useData[ "useid" ])."' AND `objDeleted` = '0' AND `objPending` = '0'";

	applyObjFilters( $where );

	$extResult = sql_query( "SELECT COUNT(*) FROM `extras` WHERE $where" );

	if( mysql_result( $extResult, 0 ) > 0 )
	{
		?>
		<div class="tab<?=$active == 5 ? " tab_active" : "" ?>"
			onclick="document.location='<?=url("extras/".$useUsername)?>'">
			<?=getIMG(url()."images/emoticons/star4.png")?> <?=_SUBMIT_TYPE_EXTRA ?></div>
		<?
	}
}

?>
<div class="tab<?=$active == 2 ? " tab_active" : "" ?>" onclick="document.location='<?=url("favourites/".$useUsername)?>'"><?=getIMG(url()."images/emoticons/fav1.png")?> <?=_FAVOURITES ?></div>
<?

if( $useData[ "useid" ] == $_auth[ "useid" ] || atLeastModerator() )
{
		$delResult = sql_query( "SELECT COUNT(*) FROM `objects`, `objExtData` ".
		"WHERE `objEid` = `objid` AND `objCreator` = '".$useData[ "useid" ]."' ".
		( atLeastModerator() ? "" : "AND `objDeletedBy` = `objCreator` " ).
		"AND `objDeleted` = '1' AND `objPending` = '0' LIMIT 1" );

	if( mysql_result( $delResult, 0 ) > 0 )
	{
		?>
		<div class="tab<?=$active == 4 ? " tab_active" : "" ?>"
			onclick="document.location='<?=url("gallerydeleted/".$useUsername)?>'">
			<?=getIMG(url()."images/emoticons/cancel.png")?> <?= _DELETED ?></div>
		<?
	}
	
	if( atLeastModerator() ) 
	{
		?>
		<div class="tab<?=$active == 5 ? " tab_active" : "" ?>"
				onclick="document.location='<?=url("managegallery/".$useUsername)?>'">
				<?=getIMG(url()."images/ui2/settings.png", 'width="14px"')?> <?= _GALLERY_MANAGEMENT ?></div>
	<?
	}
}

?>
<div class="clear">&nbsp;</div>

</div>

<div class="hline">&nbsp;</div>

<?=iefixEnd()?>
