<?

// This script should be ran once an hour as a cronjob.

// -----------------------------------------------------------------------------
// Update the search cache where needed.

$seaResult = sql_query( "SELECT `seaObject` FROM `searchcache` ".
	"WHERE `seaNeedsUpdate` = '1'" );

while( $seaData = mysql_fetch_row( $seaResult ))
{
	updateSearchCache( $seaData[ 0 ], true );
}

sql_free( $seaResult );

// -----------------------------------------------------------------------------
// Clean up old searches.

sql_query( "TRUNCATE `search`" );
sql_query( "TRUNCATE `searchItems`" );

/*
sql_where( array( "srcSubmitDate<!" => "DATE_SUB( NOW(), INTERVAL 1 HOUR )" ));

$srcResult = sql_rowset( "search", "srcid" );

while( $srcData = sql_next( $srcResult ))
{
	sql_where( array( "sriSearch" => $srcData[ "srcid" ]));
	sql_delete( "searchItems" );
}

sql_free( $srcResult );

sql_where( array( "srcSubmitDate<!" => "DATE_SUB( NOW(), INTERVAL 1 HOUR )" ));
sql_delete( "search" );
*/

// -----------------------------------------------------------------------------

ob_end_clean();
exit();

?>