<?

function isAuthorized($page_id = null)
{
	global $_auth, $_config, $dbh;

	$sth = $dbh->prepare("SELECT *
		FROM `accessExt`");
	$sth->execute();
	if($sth->rowCount() == 0){
		return false;
	}

	$sth = $dbh->prepare("SELECT *
		FROM `accessExt`
		WHERE `uid` = ?");
	$sth->bindParam(1, $_auth[ "useid" ], PDO::PARAM_INT);
	$sth->execute();

	return (( $sth->fetch(PDO::FETCH_ASSOC)[$page_id] == 0 ) ? false : true);
}

// -----------------------------------------------------------------------------
// Returns true if the current user is the gallery administrator.

function isAdmin()
{
	global $_auth, $_config;

	return( $_auth[ "useid" ] > 0 && $_auth[ "useid" ] == $_config[ "adminUser" ]); 
		//$_auth[ "useid" ] == '10581';
		//Blame sianne, she made me
}

// -----------------------------------------------------------------------------
// Returns true if the current user is at least a supermoderator (this includes
// supermoderator and administrator).

function atLeastSModerator()
{
	global $_auth, $_config;

	return( $_auth[ "useid" ] > 0 &&
		( $_auth[ "useid" ] == $_config[ "adminUser" ] ||
		$_auth[ "useIsDeveloper" ] ||
		$_auth[ "useIsSModerator" ]));
}

// -----------------------------------------------------------------------------
// Returns true if the current user is a supermoderator.

function isSModerator()
{
	global $_auth, $_config;

	return( $_auth[ "useid" ] > 0 && $_auth[ "useIsSModerator" ]);
}

// -----------------------------------------------------------------------------
// Returns true if the current user is at least a moderator (this includes
// moderator, supermoderator and administrator).

function atLeastModerator()
{
	global $_auth, $_config;

	return( $_auth[ "useid" ] > 0 &&
		( $_auth[ "useid" ] == $_config[ "adminUser" ] ||
		$_auth[ "useIsDeveloper" ] ||
		$_auth[ "useIsSModerator" ] || $_auth[ "useIsModerator" ]));
}

// -----------------------------------------------------------------------------
// Returns true if the current user is a moderator.

function isModerator()
{
	global $_auth, $_config;

	return( $_auth[ "useid" ] > 0 && $_auth[ "useIsModerator" ]);
}

// -----------------------------------------------------------------------------
// Returns true if the current user is at least a Helpdesk staff (this includes 
// HDStaff, moderator, supermoderator and administrator).

function atLeastHelpdesk() {
	global $_auth, $_config;

	return( $_auth[ "useid" ] > 0 &&
		( $_auth[ "useid" ] == $_config[ "adminUser" ] ||
		$_auth[ "useIsDeveloper" ] ||
		$_auth[ "useIsSModerator" ] || $_auth[ "useIsModerator" ] ||
		$_auth[ "useIsHelpdesk" ]));
}

// -----------------------------------------------------------------------------
// Returns true if the current user is a HDStaff.

function isHelpdesk() {
	global $_auth, $_config;
	
	return( $_auth[ "useid" ] > 0 && $_auth[ "useIsHelpdesk" ]);
	
}

// -----------------------------------------------------------------------------
// Returns true if the current user is at least Retired staff (this includes 
// Retired staff, HDStaff, moderator, supermoderator and administrator).

function atLeastRetired() {
	global $_auth, $_config;

	return( $_auth[ "useid" ] > 0 &&
		( $_auth[ "useid" ] == $_config[ "adminUser" ] ||
		$_auth[ "useIsDeveloper" ] ||
		$_auth[ "useIsSModerator" ] || $_auth[ "useIsModerator" ] ||
		$_auth[ "useIsHelpdesk" ])) ||
		$_auth[ "useIsRetired" ];
}

// -----------------------------------------------------------------------------
// Returns true if the current user is at least logged in.

function isLoggedIn()
{
	global $_auth;

	return $_auth[ "useid" ] > 0;
}

// -----------------------------------------------------------------------------
// Returns true if the user is allowed to view mature submissions.

function isUserMature()
{
	global $_auth;

	if( !isLoggedIn() )
		return false;

	$sec2year = 60 * 60 * 24 * 365;

	if( floor(( time() - strtotime( $_auth[ "useBirthday" ])) / $sec2year) < 18 )
		return false;

	return true;
}

// -----------------------------------------------------------------------------
// Returns true if the user is member of the specified club.

function isClubMember( $useid, $cluid )
{
	$result = sql_query( "SELECT * FROM `useClubs` WHERE `useCclub` = '".
		intval( $cluid )."' AND `useCmember` = '".intval( $useid )."' AND ".
		"`useCpending` = '0' LIMIT 1" );

	return( mysql_num_rows( $result ) > 0 );
}

// -----------------------------------------------------------------------------
// Returns true if the user has made at least one submission.

function isArtist( $useid )
{
	$result = sql_query( "SELECT `useObjCount` FROM `useExtData`".dbWhere( array(
		"useEid" => $useid )));

	$count = mysql_result( $result, 0 );

	mysql_free_result( $result );

	return( $count > 0 );
}

// -----------------------------------------------------------------------------
// Returns true if Extras should be shown.

function isExtras()
{
	global $_auth;

	return( !$_auth[ "useHideExtras" ]);
}

// -----------------------------------------------------------------------------
// Returns user id by given username.

function getUserId( $username )
{
	$result = sql_query( "SELECT `useid` FROM `users`".dbWhere( array(
		"useUsername" => $username ))."LIMIT 1" );

	return( mysql_num_rows( $result ) == 0 ? 0 : mysql_result( $result, 0 ));
}

?>
