<?

$_documentTitle = _PAGENOTFOUND_TITLE;

header( "Status: 404 Not Found" );

?>
<div class="header">
	<div class="header_title">
		<?=_PAGENOTFOUND_TITLE ?>
		<div class="subheader"><?=_PAGENOTFOUND_SUBTITLE ?></div>
	</div>
</div>

<div class="container">
	<? printf(_PAGENOTFOUND_MESSAGE, url("/")) ?>
</div>
