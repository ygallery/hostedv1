<?

// This script should be ran once a day as a cronjob.

// -----------------------------------------------------------------------------
// Remove updates (except moderatorials) that are more than 20 days old.

sql_query( "DELETE FROM `updates` ".
	"WHERE `updType` IN('1','2','5','6','7','15','16','17') ".
	"AND `updDate` < DATE_SUB(CURDATE(), INTERVAL 20 DAY)" );

// Remove comment updates more than 100 days old.

sql_query( "DELETE FROM `updates` ".
	"WHERE `updType` = '8' AND `updDate` < DATE_SUB(CURDATE(), INTERVAL 100 DAY)" );

// Remove art updates more than 20 days old.

sql_query( "DELETE FROM `updatesArt` ".
	"WHERE `updDate` < DATE_SUB(CURDATE(), INTERVAL 20 DAY)" );

// -----------------------------------------------------------------------------
// Remove sessions more than 20 days old.

sql_query( "DELETE FROM `sessions` ".
	"WHERE `sesLastUpdate` < DATE_SUB(CURDATE(), INTERVAL 20 DAY)" );

// -----------------------------------------------------------------------------
// Optimize tables.

/*
sql_query( "OPTIMIZE TABLE `sessions`" );
sql_query( "OPTIMIZE TABLE `updates`" );
sql_query( "OPTIMIZE TABLE `updatesArt`" );
*/

// -----------------------------------------------------------------------------

ob_end_clean();
exit();

?>