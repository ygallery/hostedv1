<div style="clear: both" class="normaltext">

<div class="tab<?= $active == 1 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "settings/site" ) ?>'"><?= _SET_SITE ?></div>

<div class="tab<?= $active == 2 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "settings/profile" ) ?>'"><?= _PROFILE ?></div>

<div class="tab<?= $active == 3 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "settings/interaction" ) ?>'"><?= _SET_INTERACTION ?></div>

<div class="tab<?= $active == 5 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "settings/folders" ) ?>'"><?= _SET_FOLDERS ?></div>

<div class="tab<?= $active == 6 ? " tab_active" : "" ?>" 
	onclick="document.location='<?= url( "themedesigner" ) ?>'"><?= _SET_THEME_DESIGNER ?></div>

<div class="tab<?= $active == 4 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "settings/sidebar" ) ?>'"><?= _SIDEBAR ?></div>

<div class="clear"><br /></div>

</div>
