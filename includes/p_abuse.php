<?

// This page is only available to a moderator or higher.

if( !atLeastHelpdesk() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

$_documentTitle = _ABUSE_SUBMISSION;

include_once( INCLUDES."comments.php" );

// Abuse id must be passed as the first parameter.

$abuid = intval( $_cmd[ 1 ]);

$result = sql_query( "SELECT `abuIsExtras` FROM `abuses`".dbWhere( array(
	"abuid" => $abuid )));

if( mysql_num_rows( $result ) == 0 )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

if( mysql_result( $result, 0 ) == "1" )
{
	$isExtras = true;
	$_objects = "`extras`";
	$_objExtData = "`extExtData`";
}
else
{
	$isExtras = false;
	$_objects = "`objects`";
	$_objExtData = "`objExtData`";
}

$result = sql_query( "SELECT * FROM $_objects, $_objExtData, `abuses` ".
	"WHERE `abuid` = '".$abuid."' AND `objEid` = `objid` ".
	"AND `abuObj` = `objid` LIMIT 1" );

if( !$objData = mysql_fetch_assoc( $result ))
{
	// There's no such abuse report.

	include(INCLUDES."p_notfound.php");
	return;
}

$objid = $objData[ "objid" ];

// Fetch artist's data.

$result = sql_query( "SELECT `useid`, `useUsername` FROM `users` ".
	"WHERE `useid` = '".$objData[ "objCreator" ]."' LIMIT 1" );

$useData = mysql_fetch_assoc( $result );

// This does not work until abuCreator is added. The addition of abuCreator will probably also result in other code modification.
 $useData[ "abuseCases" ] = intval( mysql_result( sql_query( "SELECT COUNT(*) FROM `abuses` WHERE `abuCreator` = '".$objData[ "objCreator" ]."'" ),0 ) );
 $useData[ "abusePercent" ] = round( ( intval( mysql_result( sql_query( "SELECT COUNT(*) FROM `abuses` WHERE `abuCreator` = '".$objData[ "objCreator" ]."' AND ((`abuMod` = '-' AND `abusMod` = '-') OR (`aburMod` = '-'))" ),0 ) ) / $useData[ "abuseCases" ] ) * 100 );


// Fetch reporter's data.

$result = sql_query( "SELECT `useid`, `useUsername` FROM `users` ".
	"WHERE `useid` = '".$objData[ "abuSubmitter" ]."' LIMIT 1" );

$repData = mysql_fetch_assoc( $result );

$repData[ "abuseCases" ] = intval( mysql_result( sql_query( "SELECT COUNT(*) FROM `abuses` WHERE `abuSubmitter` = '".$objData[ "abuSubmitter" ]."'" ),0 ) );
$repData[ "abusePercent" ] = round( ( intval( mysql_result( sql_query( "SELECT COUNT(*) FROM `abuses` WHERE `abuSubmitter` = '".$objData[ "abuSubmitter" ]."' AND ((`abuMod` = '-' AND `abusMod` = '-') OR (`aburMod` = '-'))" ),0 ) ) / $repData[ "abuseCases" ]) * 100 );

// Determine the current user's access level.

$accessLevel = 0; // No access.

if( $objData[ "abuMod" ] == "?" && isModerator() )
{
	$accessLevel = 1; // Moderator.
}
elseif( $objData[ "abuMod" ] != "?" && $objData[ "abusMod" ] == "?" && isSModerator() )
{
	$accessLevel = 2; // Supermoderator.
}
elseif( $objData[ "abuMod" ] != "?" && $objData[ "abusMod" ] != "?" &&
	$objData[ "abuMod" ] != $objData[ "abusMod" ] && isAdmin() )
{
	$accessLevel = 3; // Administrator.
}

if( $objData[ "abuResolved" ])
	$accessLevel = 0; // This abuse case is resolved.

if( $accessLevel > 0 && isset( $_POST[ "submit" ]))
{
	// Check if we got a correct decision: "+" or "-"

	$decision = $_POST[ "decision" ] == "+" ? "+" :
		( $_POST[ "decision" ] == "-" ? "-" : "" );

	if($decision)
	{
		switch( $accessLevel )
		{
			case 1:
				$decisionField = "abuMod";
				break;

			case 2:
				$decisionField = "abusMod";
				break;

			case 3:
				$decisionField = "aburMod";
				break;
		}

		if(( $accessLevel == 2 && $objData[ "abuMod" ] == "-" ) ||
			$accessLevel == 3 )
		{
			// Supermoderator/administrator.
			
			$rule = addslashes( $_POST [ "violatedRule" ]);
			$reason = $_POST[ "reason" ];
			$action = (isset($_POST[ "action" ]) ? $_POST[ "action" ] : "-");
			$reasonEntry = "`abuReason` = '".addslashes($reason)."', `abuRule`='$rule', ";
			$pendingClose = "0";
			
			
			
			
		}
		else
		{
			$reasonEntry = "";
			$rule = "";
		}

		// Update the abuse case.

		sql_query( "UPDATE `abuses` SET $reasonEntry".
			"`$decisionField` = '$decision', ".
			"`{$decisionField}Comment` = '".addslashes( $_POST[ "comment" ])."', ".
			"`{$decisionField}Name` = '".$_auth["useid"]."' ".
			"WHERE `abuid` = '".$objData[ "abuid" ]."'" );

		// If we got enough decisions, perform an action upon the submission.

		switch( $accessLevel )
		{
			case 2: // Got supermoderator's decision.

				// Notify moderator.

				

				if( $objData[ "abuMod"] == "-" && $decision == "-" )
				{
					// Mark submission as violation and take action.
					if($action=="-") {
						notifyModerator( $objData, $objData[ "abuModName" ]);
						abuseDelete( $objData[ "objid" ], $useData[ "useid" ],
							$abuid );
					}
					elseif($action=="*") {
						notifyModerator( $objData, $objData[ "abuModName" ]);
						abuseGrace( $objData[ "objid" ], $useData[ "useid" ],
							$abuid);
					}
					elseif($action=="+") {
						notifyModerator( $objData, $objData[ "abuModName" ]);
						abuseViolation( $objData[ "objid" ], $useData[ "useid" ],
							$abuid );
					}
					notifyAbuser( $objData, $useData[ "useid" ]);
						
				}
				elseif( $objData[ "abuMod" ] == "+" && $decision == "+" )
				{
					// Restore submission.
					notifyModerator( $objData, $objData[ "abuModName" ]);
					abuseRestore( $objData["objid"], $useData["useid"],
						$abuid );
				}
				break;

			case 3: // Got administrator's decision.

				// Notify moderator and supermoderator.

				notifyModerator( $objData, $objData[ "abuModName" ]);
				notifyModerator( $objData, $objData[ "abusModName" ]);

				$points = ( $objData[ "abuMod" ] == "-" ? -1 : 1 );
				$points += ( $objData["abusMod"] == "-" ? -1 : 1 );
				$points += ( $decision == "-" ? -1 : 1 );

				if( $points < 0 )
				{
					// Mark submission as violation and take action.
					if($action=="-") {
						abuseDelete( $objData[ "objid" ], $useData[ "useid" ],
							$abuid );
					}
					elseif($action=="*") {
						abuseGrace( $objData[ "objid" ], $useData[ "useid" ],
							$abuid);
					}
					elseif($action=="+") {
						abuseViolation( $objData[ "objid" ], $useData[ "useid" ],
							$abuid );
					}
					notifyAbuser( $objData, $useData[ "useid" ]);
				}
				else
				{
					// Restore submission.

					abuseRestore( $objData[ "objid" ], $useData[ "useid" ],
						$abuid );
				}

				break;
		}

		redirect( url( "." )); // Redirect to the same page.
	}
}

//Deals with the case when the final decision is made after the grace period

if( $objData[ "objPendingUser" ] == "1" && isset( $_POST[ "pendingAction" ]))
{
	$rule = $objData[ "abuRule" ];
	$reason = "You did not fix the issues within the grace period. See the first PM for more information.";
	$action = $_POST[ "pendingAction" ];
	$pendingClose = "1";
	
	if(atLeastModerator() && $action == "+") 
	{
		abuseRestore( $objData[ "objid" ], $useData[ "useid" ],
						$abuid );
	}
	elseif(atLeastModerator() && $action == "-" && (( time() - $_config["abuseGrace"] ) > strtotime($objData[ "abuCloseDate" ])))
	{
		abuseDelete( $objData[ "objid" ], $useData[ "useid" ],
							$abuid );
		notifyAbuser( $objData, $useData[ "useid" ]);
	}
	redirect( url( "." )); // Redirect to the same page.
}

// -----------------------------------------------------------------------------
// PMs the abuse case to all involved moderators.

function notifyModerator( $objData, $useid )
{
	global $_config;

	$title = ":abuse: Resolved: ".$objData[ "objTitle" ];

	$comment = "The abuse case has been resolved, please click on the following ".
		"link to review it:\n\n".
		"[ [url=".url( "abuse/".$objData[ "abuid" ], array(), "&" )."]".
		$objData[ "objTitle" ]."[/url] ]";

	$userIp = getHexIp( $_SERVER[ "REMOTE_ADDR" ]);

	sql_query( "INSERT INTO `pms`".dbValues( array(
		"pmsObj" => 0,
		"pmsCreator" => $_config[ "adminUser" ],
		"pmsPmUser" => $useid,
		"pmsSubmitDate!" => "NOW()",
		"pmsTitle" => $title,
		"pmsComment" => $comment,
		"pmsSubmitIp" => $userIp,
		"pmsEditIp" => $userIp,
		"pmsNoEmoticons" => 1,
		"pmsNoSig" => 1,
		"pmsNoBBCode" => 0 )));

	$pmsid = mysql_insert_id();

	addUpdate( updTypePM, $useid, $pmsid, $_config[ "adminUser" ]);
}

// -----------------------------------------------------------------------------
// PMs information about the case to the abuser. 

function notifyAbuser( $objData, $useid )
{
	global $_config, $reason, $action;

	$title = ":cancel: Notice of Violation: ".$objData[ "objTitle" ];
	if($action=="*") 
	{
		$comment = "[b]This is an automated message to inform you that your submission, [url=/view/".$objData[ "objid" ]."]".$objData[ "objTitle" ]."[/url] was found to be a violation of the [url=/tos/]Terms of Service[/url].[/b]\n\n".
			"You have a 48 hour grace period, starting at the time this message is sent, to fix any issues with this submission. It will remain hidden to everyone but yourself during this time. ".
			"After the 48 hour period, it will be restored if you have fixed the issues. If you do not fix the issues, it will be deleted. Comment from the staff, including information about the issues, follows:\n\n ".
			"[i]".$reason."[/i]\n\n".
			"[url=/viewedit/".$objData[ "objid" ]."]Click Here[/url] to edit the submission. Also see [faq=50]\n\n".
			"[b]This is an automated message. Replies will be lost.[/b]\n\n".
			"If you have any questions, please read the [url=/helpdesk/faq]FAQ[/url] or contact the staff on [c=1].\n\n Regards,\n y!Gallery Administration";
	}
	else
	{
		$actionText=($action=="+" ? "fixing the issue and restoring the submission" : "deleting the submission");
		$comment = "[b]This is an automated message to inform you that your submission, ".$objData[ "objTitle" ].", was found to be a violation of the [url=/tos/]Terms of Service[/url].[/b]\n\n".
				"Action was taken, [b]$actionText.[/b] Additional comment from the staff follows:\n\n".
				"[i]".$reason."[/i]\n\n".
				"[b]This is an automated message. Replies will be lost.[/b] \n\n ".
				"If you have any questions please read the [url=/helpdesk/faq]FAQ[/url] or contact the staff on [c=1].\n\n Regards,\n y!Gallery Administration";
	}
		
	$userIp = getHexIp( $_SERVER[ "REMOTE_ADDR" ]);

	sql_query( "INSERT INTO `pms`".dbValues( array(
		"pmsObj" => 0,
		"pmsCreator" => $_config[ "adminUser" ],
		"pmsPmUser" => $useid,
		"pmsSubmitDate!" => "NOW()",
		"pmsTitle" => $title,
		"pmsComment" => $comment,
		"pmsSubmitIp" => $userIp,
		"pmsEditIp" => $userIp,
		"pmsNoEmoticons" => 1,
		"pmsNoSig" => 1,
		"pmsNoBBCode" => 0 )));

	$pmsid = mysql_insert_id();

	addUpdate( updTypePM, $useid, $pmsid, $_config[ "adminUser" ]);
}

// -----------------------------------------------------------------------------
// Marks the submission as a violation and then deletes the submission.

function abuseDelete( $objid, $useid, $abuid )
{
	global $_objects, $_objExtData, $isExtras, $rule;

	sql_query( "UPDATE $_objects SET `objDeleted` = '1', `objPending` = '0', `objPendingUser`='0' ".
		"WHERE `objid` = '$objid' LIMIT 1" );

	sql_query( "UPDATE $_objExtData SET `objNoAbuse` = '1', `objDeletedBy` = '0', `objDeleteDate` = NOW() ".
		"WHERE `objEid` = '$objid' LIMIT 1" );

	// Send the message to the artist that their work has been found as a violation.

	addUpdate( updTypeMessageDeleted, $useid, $objid, $isExtras );

	/*
	// Remove this submission from users updates, if any.

	if( $isExtras )
	{
		$result = sql_query( "SELECT DISTINCT `useid` FROM `updates`,`users` ".
			"WHERE `useid` = `updCreator` AND `updObj` = '$objid' ".
			"AND `updType` = '".updTypeArtExtra."'");

		while( $rowData = mysql_fetch_assoc( $result ))
			recountUpdates( updTypeArtExtra, $rowData[ "useid" ]);
	}
	else
	{
		$result = sql_query( "SELECT DISTINCT `useid` FROM `updatesArt`,`users` ".
			"WHERE `useid` = `updCreator` AND `updObj` = '$objid'");

		while( $rowData = mysql_fetch_assoc( $result ))
			recountUpdates( updTypeArt, $rowData[ "useid" ]);
	}
	*/

	// Update object count.

	include_once( INCLUDES."submission.php" );
	updateObjCount( $useid );

	// Set the abuse case to the "resolved" state.

	sql_query( "UPDATE `abuses` SET `abuResolved` = '1', `abuFinal`='-', `abuCloseDate`=NOW() ".
		"WHERE `abuid` = '$abuid' LIMIT 1" );

	// Add message to moderator's log at the submitter's page.

	addModeratorLog( $useid, "Deleted after moderation: ".
		"[url=".url( "abuse/".$abuid, array(), "&" )."]".
		"Abuse Case #".$abuid."[/url]. $rule" );
}

// -----------------------------------------------------------------------------
// Marks the submission as a violation and sets for grace period.

function abuseGrace( $objid, $useid, $abuid )
{
	global $_objects, $_objExtData, $isExtras, $rule;

	sql_query( "UPDATE $_objects SET  `objPending` = '1', `objPendingUser`='1' ".
		"WHERE `objid` = '$objid' LIMIT 1" );

	// Send the message to the artist that their work has been found as a violation.

	addUpdate( updTypeMessageDeleted, $useid, $objid, $isExtras );


	// Set the abuse case's final decision and close date for use after the grace period.

	sql_query( "UPDATE `abuses` SET `abuFinal`='-', `abuCloseDate`=NOW() ".
		"WHERE `abuid` = '$abuid' LIMIT 1" );

	// Add message to moderator's log at the submitter's page.

	addModeratorLog( $useid, "Grace period after moderation: ".
		"[url=".url( "abuse/".$abuid, array(), "&" )."]".
		"Abuse Case #".$abuid."[/url]. $rule" );
}

// -----------------------------------------------------------------------------
// Marks as violation, restores the submission, and makes it non-reportable

function abuseViolation($objid, $useid, $abuid)
{
	global $_objects, $_objExtData, $isExtras, $rule;

	sql_query( "UPDATE $_objects SET `objDeleted` = '0', `objPending` = '0' ".
		"WHERE `objid` = '$objid' LIMIT 1" );

	sql_query( "UPDATE $_objExtData SET `objNoAbuse` = '1' ".
		"WHERE `objEid` = '$objid' LIMIT 1" );

	// Send the violation notice to the user's updates.

	addUpdate( updTypeMessageDeleted, $useid, $objid, $isExtras );

	// Put this submission back to users updates.

	if( $isExtras )
	{
		$result = sql_query( "SELECT DISTINCT `useid` FROM `updates`,`users` ".
			"WHERE `useid` = `updCreator` AND `updObj` = '$objid' ".
			"AND `updType` = '".updTypeArtExtra."'" );

		while( $rowData = mysql_fetch_assoc( $result ))
		{
		//	recountUpdates( updTypeArtExtra, $rowData[ "useid" ]);
			sql_query( "UPDATE `useExtData` SET `useUpdExt` = `useUpdExt` + 1 ".
			           "WHERE `useEid` = '".intval( $rowData[ "useid" ])."' LIMIT 1" );
		}
	}
	else
	{
		$result = sql_query( "SELECT DISTINCT `useid` FROM `updatesArt`,`users` ".
			"WHERE `useid` = `updCreator` AND `updObj` = '$objid'" );

		while( $rowData = mysql_fetch_assoc( $result ))
                {
		//	recountUpdates( updTypeArt, $rowData[ "useid" ]);
			sql_query( "UPDATE `useExtData` SET `useUpdObj` = `useUpdObj` + 1 ".
			           "WHERE `useEid` = '".intval( $rowData[ "useid" ])."' LIMIT 1" );
                }
	}

	// Update object count.

	include_once( INCLUDES."submission.php" );
	updateObjCount( $useid );

	// Set the abuse case to the "resolved" state.

	sql_query( "UPDATE `abuses` SET `abuResolved` = '1', `abuFinal`='-', `abuCloseDate`=NOW()".
		"WHERE `abuid` = '$abuid' LIMIT 1" );
	
	// Add message to moderator's log at the submitter's page.

	addModeratorLog( $useid, "Fixed and restored after moderation: ".
		"[url=".url( "abuse/".$abuid, array(), "&" )."]".
		"Abuse Case #".$abuid."[/url]. $rule" );	
}

// -----------------------------------------------------------------------------
// Leaves the submission and marks it as non-reportable.

function abuseRestore($objid, $useid, $abuid)
{
	global $_objects, $_objExtData, $isExtras;

	sql_query( "UPDATE $_objects SET `objDeleted` = '0', `objPending` = '0', `objPendingUser`='0'".
		"WHERE `objid` = '$objid' LIMIT 1" );

	sql_query( "UPDATE $_objExtData SET `objNoAbuse` = '1' ".
		"WHERE `objEid` = '$objid' LIMIT 1" );

	// Send the message to the artist that their work has been restored.

	addUpdate( updTypeMessageRestored, $useid, $objid, $isExtras );

	// Put this submission back to users updates.

	if( $isExtras )
	{
		$result = sql_query( "SELECT DISTINCT `useid` FROM `updates`,`users` ".
			"WHERE `useid` = `updCreator` AND `updObj` = '$objid' ".
			"AND `updType` = '".updTypeArtExtra."'" );

		while( $rowData = mysql_fetch_assoc( $result ))
		{
		//	recountUpdates( updTypeArtExtra, $rowData[ "useid" ]);
			sql_query( "UPDATE `useExtData` SET `useUpdExt` = `useUpdExt` + 1 ".
			           "WHERE `useEid` = '".intval( $rowData[ "useid" ])."' LIMIT 1" );
		}
	}
	else
	{
		$result = sql_query( "SELECT DISTINCT `useid` FROM `updatesArt`,`users` ".
			"WHERE `useid` = `updCreator` AND `updObj` = '$objid'" );

		while( $rowData = mysql_fetch_assoc( $result ))
                {
		//	recountUpdates( updTypeArt, $rowData[ "useid" ]);
			sql_query( "UPDATE `useExtData` SET `useUpdObj` = `useUpdObj` + 1 ".
			           "WHERE `useEid` = '".intval( $rowData[ "useid" ])."' LIMIT 1" );
                }
	}

	// Update object count.

	include_once( INCLUDES."submission.php" );
	updateObjCount( $useid );

	// Set the abuse case to the "resolved" state.
	
		sql_query( "UPDATE `abuses` SET `abuResolved` = '1', `abuFinal`='+', `abuCloseDate`=NOW() ".
			"WHERE `abuid` = '$abuid' LIMIT 1" );
	
}

// -----------------------------------------------------------------------------

if( $objData[ "abuMod" ] == "?")
{
	$overallDecision = strtolower( _ABUSE_WAIT_MODERATOR );
}
elseif( $objData[ "abuMod" ] != "?" && $objData[ "abusMod" ] == "?" )
{
	$overallDecision = strtolower(_ABUSE_WAIT_SUPERMODERATOR);
}
elseif( $objData[ "abuMod" ] != "?" && $objData[ "abusMod" ] != "?" &&
	$objData[ "abuMod" ] != $objData[ "abusMod" ] &&
	$objData[ "aburMod" ] == "?" )
{
	$overallDecision = strtolower(_ABUSE_WAIT_ADMINISTRATOR);
}
else
{
	$overallDecision = strtolower( $objData[ "objDeleted" ] ?
		_DELETE : _ABUSE_RESTORE);
}

?>
<div class="header">
	<div class="header_title">
		<?= _ABUSE_SUBMISSION ?>
		<div class="subheader">
			<?= formatText( $objData[ "objTitle" ]) ?>
		</div>
	</div>
	<div class="clear">&nbsp;</div>
	<?
	$active = 3;
	include(INCLUDES."mod_adminmenu.php");
	?>
</div>

<div class="container">

	<div class="gallery_col">
		<div class="padded a_center">
			<?= getObjectThumb( $objData[ "objid" ], 100, $isExtras ) ?>
			<div><?= $objData[ "objTitle" ] ?></div>
			<div class="sep">(<a href="<?= url( "moderate/" . $useData["useUsername"] ) ?>"><?= _USER ." ". _MODERATION ?></a>)</div>
		</div>
	</div>
	<?

	$decisions = array(
		"?" => "(?) "._ABUSE_DECISION_NONE,
		"-" => "(&ndash;) "._ABUSE_DECISION_VIOLATION,
		"+" => "(+) "._ABUSE_DECISION_COMPLIANT );

	?>
	<div class="f_right">
		<div class="caption"><?= _STATS ?>:</div>
		<div class="container2 mar_bottom narrow">
			<? printf( _ABUSE_STATS,$repData[ "useUsername" ],$repData[ "abuseCases" ],$repData[ "abusePercent" ],$useData[ "useUsername"],$useData[ "abuseCases" ],$useData[ "abusePercent" ] ); ?>
		</div>
	</div>
	<div class="f_left notsowide">
		<div class="caption">
			<?= _ABUSE_STATUS ?>:
		</div>
		<div class="container2 mar_bottom">
			<div>
				<b><?= _ABUSE_REPORT ?></b>:
			</div>
			<div class="padded">
				<?= getIMG( url()."images/emoticons/comment.png" ) ?>
				<?= getUserLink( $repData[ "useid" ]) ?>:
				<?= formatText( $objData[ "abuComment" ]) ?>
			</div>

			<div>
				<b><?= _MODERATOR ?></b>:
				<span class="error"><?= $decisions[ $objData[ "abuMod" ]] ?></span>
			</div>
			<div class="padded">
				<?= getIMG( url()."images/emoticons/comment.png" ) ?>
				<?= getUserLink( $objData[ "abuModName" ]) ?>:
				<?= formatText( $objData[ "abuModComment" ]) ?>
			</div>
			<?

			if( $accessLevel == 2 || $objData[ "abusMod" ] != "?" )
			{
				?>
				<div><b><?=_SUPERMODERATOR ?></b>:
					<span class="error">
						<b><?= $decisions[ $objData[ "abusMod" ]] ?></b></span>
				</div>
				<div class="padded">
					<?= getIMG( url()."images/emoticons/comment.png" ) ?>
					<?= getUserLink( $objData[ "abusModName" ]) ?>:
					<?= formatText( $objData[ "abusModComment" ]) ?></div>
				<?
			}

			if($accessLevel == 3 || $objData[ "aburMod" ] != "?" )
			{
				?>
				<div>
					<b><?=_ADMINISTRATOR ?></b>:
					<span class="largetext error">
						<b><?= $decisions[ $objData[ "aburMod" ]] ?></b></span>
				</div>
				<div class="padded">
					<?=getIMG(url()."images/emoticons/comment.png")?>
					<?=getUserLink($objData["aburModName"])?>:
					<?=formatText($objData["aburModComment"]) ?>
				</div>
				<?
			}
			if($objData["abuResolved"] == "1") 
			{
				?>
				<div>
					<b><?= _ABUSE_OVERALL_DECISION ?></b>:
					<span class="largetext error"><b><?= $overallDecision ?></b></span>
				</div>
				<?
			}

			if( $objData["abuReason"] != "" )
			{
				?>
				<div class="sep">
					<b><?= _ABUSE_RULE ?></b>: <?=formatText( $objData[ "abuRule" ] )?>
				</div>
				<div class="sep">
					<b><?= _ABUSE_REASON ?></b>:
					<?= formatText( $objData[ "abuReason" ]) ?>
				</div>
				<?
			}
			if( $objData[ "objPendingUser" ]) 
			{
				?>
					<div class="mar_top">
						<b>Object is Pending User Input</b>:<br />
						Note, you must wait 48 hours before you can delete the image. You may restore the image at any time.<br />
						<? if( ( time() - $_config["abuseGrace"] ) < strtotime($objData[ "abuCloseDate" ]))
						{
							echo "This submission may be deleted on ".gmdate( $_auth[ "useDateFormat" ],
								applyTimezone( strtotime( $objData[ "abuCloseDate" ] ) + $_config[ "abuseGrace" ] ));
						}						
							?>
						<form action="<?= url( "." ) ?>" method="post">
						<div class="sep">
							<select id="<?= $pendingActionId = generateId() ?>" name="pendingAction" size="2">
								<? if(( time() - $_config["abuseGrace"] ) > strtotime($objData[ "abuCloseDate" ])) {?>
								<option value="-">&ndash; <?= _ABUSE_DECISION_DELETE ?></option>
								<? }?>
								<option value="+">+ <?= _ABUSE_DECISION_RESTORE ?></option>
							
							</select>
						</div>
						<div class="sep">
							<button class="submit" name="submit" type="submit">
								<?= getIMG( url()."images/emoticons/checked.png" ) ?>
								<?= _ABUSE_SEND_BUTTON ?>
							</button>
						</div>
						</form>
					</div>
				<?
			}

			?>
		</div>
	</div>
	<div class="clear">&nbsp;</div>
	<?

	if( $accessLevel )
	{
		switch( $accessLevel )
		{
			case 1:
				$title = _MODERATOR;
				break;

			case 2:
				$title = _SUPERMODERATOR;
				break;

			case 3:
				$title = _ADMINISTRATOR;
				break;
		}

		?>
		<form action="<?= url( "." ) ?>" method="post">
			<div class="container2">
				<div class="header"><?= $title ?>:</div>

				<div class="caption">
					<?= _ABUSE_DECISION ?>:
				</div>
				<select id="<?= $decisionId = generateId() ?>" name="decision" size="2">
					<option value="-">&ndash; <?= _ABUSE_DECISION_VIOLATION ?></option>
					<option value="+">+ <?= _ABUSE_DECISION_COMPLIANT ?></option>
				</select>
				<? if(( $accessLevel == 2 && $objData[ "abuMod" ] == "-") ||
					$accessLevel == 3 ) //Show Action box for supermod/admin
				{?>
				<div class="caption">
					Action:
				</div>
				<select id="<?= $actionId = generateId() ?>" name="action" size="3">
					<option value="-">&ndash; <?= _ABUSE_DECISION_DELETE ?></option>
					<option value="*">* Flag for Grace Period</option>
					<option value="+">+ <?= _ABUSE_DECISION_RESTORE ?></option>
				</select>
					
				
				<?
				}
				
				
				iefixStart();

				?>
				<div class="sep caption">
					<?= _COMMENT ?>:
				</div>
				<?

				$commentNoOptions = true;

				include(INCLUDES."mod_comment.php"); // returns $commentId

				?>
				<div class="clear">&nbsp;</div>
				<?

				iefixEnd();

				if(( $accessLevel == 2 && $objData[ "abuMod" ] == "-") ||
					$accessLevel == 3 )
				{
					// Supermoderator/administrator.
					
					
					iefixStart();

					?>
					<div class="sep error">The following is <b>NOT YET</b> shown to the user. You must still edit the reason manually. The new system will eventually do this for you, but not just yet.</div>
					<b><?= _ABUSE_RULE?></b>: <input type="text" name="violatedRule" id="violatedRule" size="5" maxlength="3" />
					<div class="sep error">
						<?= _ABUSE_REASON_EXPLAIN ?>
						<br />
					</div>
					<div><?= _MAX_LENGTH_255 ?></div>
					<?

					$commentNoOptions = true;
					$commentRows = 3;
					$commentName = "reason";
					$commentDefault = $objData[ "abuReason" ];

					if( $commentDefault == "" )
						$commentDefault = _ABUSE_DEFAULT_REASON;

					include( INCLUDES."mod_comment.php" ); // Returns $commentId

					?>
					<div class="clear">&nbsp;</div>
					<?

					iefixEnd();
				}

				?>
			</div>
			<div class="sep">
				<button class="submit" name="submit" type="submit"
					onclick="var el = get_by_id( '<?= $decisionId ?>' ); if( !el.value ){ alert( '<?= _ABUSE_DECISION_BLANK ?>' ); return false } el = get_by_id( '<?= $commentId ?>'); if( !el.value ){ alert( '<?= _BLANK_COMMENT ?>' ); return false } el=get_by_id('violatedRule'); if(!el.value){ alert('<?= _ABUSE_RULE_BLANK ?>'); return false } return true">
					<?= getIMG( url()."images/emoticons/checked.png" ) ?>
					<?= _ABUSE_SEND_BUTTON ?>
				</button>
			</div>
		</form>
		<?
	}

	?>
</div>
