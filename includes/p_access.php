<?

if( !atLeastSModerator() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

if( isset( $_GET[ "reset" ]))
{
	sql_query( "TRUNCATE TABLE `access`" );
	redirect( url( "." ));
}

?>
<div class="container2">

<div>
	<a href="<?= url( "." ) ?>">Refresh</a>
	&middot;
	<a onclick="return confirm('<?= _ARE_YOU_SURE ?>')"
		href="<?= url( ".", array( "reset" => "yes" )) ?>">Reset statistics</a>
	&middot;
	<a href="<?= url( "profiler" ) ?>">Profiler</a>
</div>
<div><br /></div>

<table cellspacing="0" cellpadding="4" border="1">
	<tr>
		<td><b>IP</b></td>
		<td><b>Requests</b></td>
		<td><b>User(s)</b></td>
	</tr>
<?

$accResult = sql_query( "SELECT SUM(`accCount`) FROM `access`" );

$totalCount = sql_result( $accResult );

$accResult = sql_query( "SELECT * FROM `access` ORDER BY `accCount` DESC LIMIT 30" );

while( $accData = sql_next( $accResult ))
{
	?>
	<tr>
		<td><?= getDotDecIp( $accData[ "accIp" ]) ?></td>
		<td align="center"><?= $accData[ "accCount" ] ?></td>
		<td><?

			sql_where( array(
				"useid*" => "useEid",
				"useLastIp" => $accData[ "accIp" ]));

			$useResult = sql_rowset( "users, useExtData", "useUsername" );

			while( $useData = sql_next( $useResult ))
			{
				echo $useData[ "useUsername" ]." ";
			}

			sql_free( $useResult );

			?>
		</td>
	</tr>
	<?
}

sql_free( $accResult );

?>
</table>

</div>
