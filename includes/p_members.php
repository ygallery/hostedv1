<?
	$cluid = intval($_cmd[1]);

	$extraRights = atLeastSModerator();

	$result = sql_query("SELECT * FROM `clubs`,`cluExtData` WHERE `cluid` = '$cluid' AND `cluEid` = `cluid` LIMIT 1");
	if(!$cluData = mysql_fetch_assoc($result)) {
		include(INCLUDES."p_notfound.php");
		return;
	}

	$_documentTitle = ($cluData["cluIsProject"] ? _PROJECT : _CLUB1).": ".$cluData["cluName"].
		": "._MEMBERS;
?>

<div class="header">
	<div class="f_right mar_left a_center normaltext">
		<?=getClubIcon($cluData["cluid"])?>
	</div>
	<div class="f_left mar_right header_title">
		<?=$cluData["cluName"]?>
		<div class="subheader"><?=_MEMBERS ?></div>
	</div>
	<?
		$active = 3;
		include(INCLUDES."mod_clubmenu.php");
	?>
</div>

<div class="container">
	<? 	$whereGuest = isLoggedIn() ? "" : ( " AND `useGuestAccess` = '1' " ); 
		?>
	<div class="header">
		<?=_CLUB_MODERATORS ?></div>
	<div class="container2">
		<?
		$friendQuery = "SELECT `useid` FROM `users`,`useClubs`,`useExtData` WHERE `useid` = `useEid` AND `useid` = `useCmember` AND `useCclub` = '$cluid' AND `useCpending` = '0' AND `useCModerator`='1' $whereGuest ORDER BY `useCid` DESC";
		include(INCLUDES."mod_friends.php");
		?>
	</div>
	<div class="sep header">
		<?=_MEMBERS ?></div>
	<div class="container2">
		<?
		$friendQuery = "SELECT `useid` FROM `users`,`useClubs`,`useExtData` WHERE `useid` = `useEid` AND `useid` = `useCmember` AND `useCclub` = '$cluid' AND `useCpending` = '0' $whereGuest ORDER BY `useCid` DESC";
		include(INCLUDES."mod_friends.php");
		?>
	</div>
<?
	if($cluid != 1) {
?>
	<div class="sep header">
		<?=getIMG(url()."images/emoticons/watch.png") ?>
		<?=_WATCHED_BY ?></div>
	<div class="container2">
		<?
		$friendQuery = "SELECT `useid` FROM `watches`,`users`,`useExtData` WHERE `useid` = `useEid` AND `useid` = `watUser` AND `watCreator` = '$cluid' AND `watType` = 'clu' $whereGuest ORDER BY `watSubmitDate` DESC";
		include(INCLUDES."mod_friends.php");
		?>
	</div>
<?
	}
?>
</div>
