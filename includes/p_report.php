<?

if( substr( $_cmd[ 1 ], 0, 1 ) == "e" )
{
	$isExtras = true;
	$objid = intval( substr( $_cmd[ 1 ], 1 ));
	$_objects = "`extras`";
	$_objExtData = "`extExtData`";
}
else
{
	$objid = intval( $_cmd[ 1 ]);
	$isExtras = false;
	$_objects = "`objects`";
	$_objExtData = "`objExtData`";
}

if( !isLoggedIn() )
{
	header("Status: 403 Forbidden");
	echo '<div class="header">' . _ABUSE_REPORT . '</div>'.
		'<div class="container">';
	notice(_REQUIRE_LOGIN);
	echo '</div>';
	return;
}

if($_config["maxOpenAbuse"] <= 0)
{
	include(INCLUDES."p_notfound.php");
	return;
}

$result = sql_query("SELECT * FROM $_objects,$_objExtData WHERE `objid` = '".$objid."' AND `objEid` = `objid` AND `objPending` = '0' AND `objDeleted` = '0' AND `objNoAbuse` = '0' LIMIT 1");

if(!mysql_num_rows($result)) { // if it is not possible to report this object, return the Not Found page
	include(INCLUDES."p_notfound.php");
	return;
}
else
	$objData = mysql_fetch_assoc($result);

if(mysql_result(sql_query("SELECT COUNT(*) FROM `abuses` WHERE `abuSubmitter` = '".$_auth["useid"]."' AND `abuResolved` = '0'"),0) > intval($_config["maxOpenAbuse"]) && !atLeastModerator())
{
	notice(_ABUSE_TOO_MANY);
	return;
}

if(mysql_result(sql_query("SELECT COUNT(*) FROM `abuses` WHERE `abuSubmitter` = '".$_auth["useid"]."' AND `abuCreator`='".$objData[ 'objCreator' ]."' AND `abuResolved` = '0'"),0) > intval($_config["maxOpenAbuseGallery"]) && !atLeastModerator())
{
	notice(_ABUSE_TOO_MANY);
	return;
}



$useResult = sql_query("SELECT `useid`,`useUsername` FROM `users` WHERE `useid` = '".$objData["objCreator"]."' LIMIT 1");
$useData = mysql_fetch_assoc($useResult);

$_documentTitle = _ABUSE_REPORT;

?>
<div class="header">
	<div class="header_title">
		<?=_ABUSE_REPORT?>
		<div class="subheader"><?=formatText($objData["objTitle"]) ?></div>
	</div>
	<div class="clear">&nbsp;</div>
</div>

<div class="container">
<?

if(isset($_POST["submit"]) && $_POST["comment"]) {
	// create an abuse entry
	sql_query("INSERT INTO `abuses`(`abuObj`,`abuIsExtras`,`abuSubmitter`,`abuSubmitDate`,`abuComment`,`abuCreator`) ".
		"VALUES('$objid','".($isExtras ? 1 : 0)."','".$_auth["useid"]."',NOW(),'".addslashes($_POST["comment"])."','".$objData['objCreator']."')");
	// put the object into pending mode
	sql_query("UPDATE $_objects SET `objPending` = '1' WHERE `objid` = '$objid' LIMIT 1");
	// send the message to the artist that their work has been reported
	addUpdate(updTypeMessageAbuse, $useData["useid"], $objid, $isExtras);

	/*
	// remove this submission from users updates, if any

	if( $isExtras )
	{
		$result = sql_query("SELECT DISTINCT `useid` FROM `updates`,`users` WHERE `useid` = `updCreator` AND `updObj` = '$objid' AND `updType` = '".updTypeArtExtra."'");
		while($rowData = mysql_fetch_assoc($result))
			recountUpdates(updTypeArtExtra, $rowData["useid"]);
	}
	else
	{
		$result = sql_query("SELECT DISTINCT `useid` FROM `updatesArt`,`users` WHERE `useid` = `updCreator` AND `updObj` = '$objid'");
		while($rowData = mysql_fetch_assoc($result))
			recountUpdates(updTypeArt, $rowData["useid"]);
	}
	*/

	// show the message that the report is complete
	echo _ABUSE_REPORTED;
	echo '</div>';
	return;
}
?>
	<div class="container2 notsowide mar_bottom">
		<div class="largetext"><?= _WARNING ?></div>
		<div class="sep">
			<?= _ABUSE_FORM_NOTICE ?>
		</div>
<?
if( $isExtras )
{
?>
		<div class="sep"><?= sprintf( _EXTRA_LEGAL, $_config[ "extrasLiftedRules" ]) ?></div>
<?
}
?>
	</div>
<form action="<?=url(".") ?>" method="post">
<div class="caption">Abuse comment:</div>
<?
	$commentNoOptions = true;
	include(INCLUDES."mod_comment.php");
	if(isset($_POST["submit"]) && !$_POST["comment"]) notice(_SUBMIT_NO_COMMENT);
?>
<div class="sep">
	<button class="submit" name="submit" type="submit">
		<?=getIMG(url()."images/emoticons/abuse.png") ?>
		<?=_ABUSE_REPORT ?>
	</button>
</div>
</form>

</div>
