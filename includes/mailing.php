<?

// -----------------------------------------------------------------------------
// Sends a $message to the user $useid via email.

function sendEmail( $useid, $subject, $message, $ignoreErrors = false )
{
	global $_config;

	$result = sql_query( "SELECT `useUsername`,`useRealname`,`useEmail` ".
		"FROM `users`,`useExtData` ".
		"WHERE `useid` = `useEid` AND `useid` = '$useid' LIMIT 1" );

	if( !$useData = mysql_fetch_assoc( $result ))
	{
		// Unable to send an email: there's no such user.

		if( $ignoreErrors )
			return false;
		else
			fatal_error( sprintf( _EMAIL_SENDING_ERROR, $useid ));
	}

	$toName = $useData[ "useRealname" ] ? $useData[ "useRealname" ] :
		$useData["useUsername"];

	// Delete potentially harmful characters from the recipient's name.

	$toName = preg_replace( '/[^\w\s\.\-]/', "", $toName );

	$toEmail = $useData[ "useEmail" ];

	// TODO: get returning email address from config.adminEmail (or something)

	$headers = "MIME-Version: 1.0\r\n".
		"Content-type: text/plain; charset=UTF-8\r\n".
		"From: ".$_config["galName"]." <".$_config["returnEmail"].">\r\n".
		( isset( $_SERVER["REMOTE_HOST"]) ?
			"X-Request-Host: ".$_SERVER["REMOTE_HOST"]." [".$_SERVER["REMOTE_ADDR"]."]\r\n" : "X-Request-IP: ".$_SERVER["REMOTE_ADDR"]."\r\n" );

	$message = str_replace( '<br />', "\n", $message );

	return mail( "$toName <$toEmail>", $subject, strip_tags( $message ),
		$headers, "-f".$_config[ "returnPathEmail" ]);
}

// -----------------------------------------------------------------------------
// Returns true if $email uses correct email address syntax.

function checkEmail( $email )
{
	$regexp = '/([^0-9a-zA-Z\@\.\-\_])|(\.$)|(^\.)|(\@\.)|(\.\@)/';

	if( $email != preg_replace( $regexp, "", $email ))
		return false;

	if( !strstr( $email, "@" ) || !strstr( $email, "." ))
		return false;

	if( !preg_match( '/.+\@.+\..+/', $email ))
		return false;

	if( preg_match('/(klassmaster\.com|fakeinformation\.com|sogetthis\.com|mytrashmail\.com|pookmail\.com|dodgeit\.com|sofort-mail\.de|hoaxmail\.co\.uk|tempinbox\.com)$/i', $email) )
		return false;

	return true;
}

?>
