<?

	// Add this for full XML compliance, but of course IE will fail and
	// drop out of standards mode if it exists, so only send it to browsers
	// that accept application/xhtml+xml

if( isset( $_SERVER[ "HTTP_ACCEPT" ] ) && stristr( $_SERVER[ "HTTP_ACCEPT" ], "application/xhtml+xml" ) )
{
	echo( "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" );
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= $_auth["useid"] ? $_auth["useLanguage"] : "en" ?>" lang="<?= $_auth["useid"] ? $_auth["useLanguage"] : "en" ?>">
<head>
	<?

	// This is the default title of the page. It will be then overridden
	// via Javascript with the value $_documentTitle has at the end of
	// execution.

	?>
	<title><?= $_config[ "galName" ] ?> :: <?= $_config[ "galSubname" ] ?></title>
	<?

	// We've decided UTF-8 should be enough for everyone, instead of
	// making support for a bunch of different charsets.

	/* This is handled in main.php now, sending the actual Content-Type header :)
	?>
	<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8" />
	<?
	*/

	// The following option will hide the annoying toolbar appearing above
	// large images in IE.

	?>
	<meta http-equiv="ImageToolbar" content="no" />
	<?

	// First, include the main stylesheet that defines the basic shapes of
	// the layout.

	?>
	<link href="<?= urlf()."themes/style12.css" ?>"
		rel="stylesheet" type="text/css" media="screen, projection" />
	<?

	// Include the list of alternate stylesheets, so from the browsers such
	// as Firefox it will be possible to change the stylesheet on the fly.
	// The default theme is defined in the `config` table.

	$result = mysql_query( "SELECT * FROM `themes`" )
		or trigger_error( _ERR_MYSQL );

	while( $theData = mysql_fetch_assoc( $result ))
	{
		?>
		<link href="<?=urlf().findNewestFile("themes/".$theData["theName"]."/style*.css","")?>"
			rel="<?= $theData[ "theName" ] != $_config[ "defaultTheme" ] ? "alternate " : "" ?>stylesheet"
			title="<?= $theData[ "theLongName" ]?>" type="text/css" media="screen, projection" />
		<?
	}

	$disableCustom = isset( $_auth[ "useDisableCustom" ]) ? $_auth[ "useDisableCustom" ] : 0;

	if( $disableCustom != 1 ) // 1 = Disable custom theme
	{
		$customTheme = "";

		if( isLoggedIn() && $disableCustom != 2 ) // 2 = Use my custom theme on my own pages only
		{
			// Apply current user's custom theme by default. If they are browsing
			// another user's page, $customTheme will be overridden below.

			$fn = findNewestFile( applyIdToPath( "files/themes/",
				$_auth[ "useid" ])."/style-*.css" );

			if( $fn != "" )
			{
				$customTheme = $fn;
			}
		}

		if( $disableCustom != 3 ) // 3 = Always use my custom theme on ALL pages
		{
			$userPages = array( "clubs", "extras", "favourites", "friends", "gallery",
				"galleryclubs", "gallerydeleted", "journal", "poll", "stats",
				"themedesigner", "user", "watches", "watchedby" );

			$clubPages = array( "announcement", "club", "clubgallery", "clubthemedesigner",
				"manageclub", "members" );

			if( in_array( $_cmd[ 0 ], $userPages ))
			{
				// We're viewing a page that belongs to a certain user.

				$fn = findNewestFile( applyIdToPath( "files/themes/",
					getUserId( $_cmd[ 1 ]))."/style-*.css" );

				if( $fn != "" )
				{
					$customTheme = $fn;
				}
			}

			if( $_cmd[ 0 ] == "view" )
			{
				// We're viewing a submission that belongs to a certain user.

				if( substr( $_cmd[ 1 ], 0, 1 ) == "e" )
				{
					$objid = substr( $_cmd[ 1 ], 1 );
					$_objects = "`extras`";
				}
				else
				{
					$_objects = "`objects`";
					$objid = $_cmd[ 1 ];
				}

				$result = mysql_query( "SELECT `objCreator`,`objForClub` FROM $_objects".dbWhere( array(
					"objid" => $objid ))."LIMIT 1" ) or trigger_error( _ERR_MYSQL );

				if( $rowData = mysql_fetch_assoc( $result ))
				{
					$fn = findNewestFile( applyIdToPath( "files/themes/",
						$rowData[ "objCreator" ])."/style-*.css" );

					if( $fn != "" )
					{
						$customTheme = $fn;
					}

					if( $rowData[ "objForClub" ] > 0 )
					{
						$fn = findNewestFile( applyIdToPath( "files/clubthemes/",
							$rowData[ "objForClub" ])."/style-*.css" );

						if( $fn != "" )
						{
							$customTheme = $fn;
						}
					}
				}
			}

			if( in_array( $_cmd[ 0 ], $clubPages ))
			{
				// We're viewing a page that belongs to a certain club.

				$fn = findNewestFile( applyIdToPath( "files/clubthemes/",
					intval( $_cmd[ 1 ]))."/style-*.css" );

				if( $fn != "" )
				{
					$customTheme = $fn;
				}
			}
		}

		if( $customTheme != "" )
		{
			?>
			<link href="<?= urlf().$customTheme ?>" rel="alternate stylesheet"
				title="(custom)" type="text/css" media="screen, projection" />
			<?
		}
	}

	// Set the font.

	if( isset( $_POST[ "font" ]))
	{
		$font = preg_replace( '/[^\w]/', "", $_POST[ "font" ]);
	}
	elseif( isset( $_COOKIE[ "yGalFont" ]))
	{
		$font = preg_replace( '/[^\w]/', "", $_COOKIE[ "yGalFont" ]);
	}
	elseif( isset( $_GET[ "yGalFont" ]))
	{
		$font = preg_replace( '/[^\w]/', "", $_GET[ "yGalFont" ]);
	}
	else
	{
		$font = $_config[ "defaultFont" ];
	}	

	if(!file_exists("themes/font-".$font.".css"))
		$font = $_config[ "defaultFont" ];

	if( $font != "original" ) // Note: don't do $_config[ "defaultFont" ] here.
	{
		?>
		<link href="<?= urlf()."themes/font-".$font.".css" ?>"
			rel="stylesheet" type="text/css" media="screen, projection" />
		<?
	}

	// Include our Javascript.
	?>

	<script type="text/javascript">
	//<![CDATA[
		var isIE = <?= $isIE ? "true" : "false" ?>;
		var isReadOnly = <?= $_config[ "readOnly" ] ? "true" : "false" ?>;
		var isLoggedIn = <?= $_auth[ "useid" ] != 0 ? "true" : "false" ?>;
		var isFuzzyNumbers = <?= $_auth["useid"] && $_auth["useFuzzyNumbers"] ? "true" : "false" ?>;
		var baseURL = '<?= url() ?>';
		var baseURLF = '<?= urlf() ?>';
		var filesPort = '<?= $_config[ "filesPort" ] ?>';
		var _BB_ALIGN_CENTER = '<?= str_replace( "\n", "", addslashes( _BB_ALIGN_CENTER )) ?>';
		var _BB_ALIGN_JUSTIFY = '<?= str_replace( "\n", "", addslashes( _BB_ALIGN_JUSTIFY )) ?>';
		var _BB_ALIGN_LEFT = '<?= str_replace( "\n", "", addslashes( _BB_ALIGN_LEFT )) ?>';
		var _BB_ALIGN_RIGHT = '<?= str_replace( "\n", "", addslashes( _BB_ALIGN_RIGHT )) ?>';
		var _BB_BOLD = '<?= str_replace( "\n", "", addslashes( _BB_BOLD )) ?>';
		var _BB_CLUBICON = '<?= str_replace( "\n", "", addslashes( _BB_CLUBICON )) ?>';
		var _BB_EMOTICON = '<?= str_replace( "\n", "", addslashes( _BB_EMOTICON )) ?>';
		var _BB_ITALIC = '<?= str_replace( "\n", "", addslashes( _BB_ITALIC )) ?>';
		var _BB_UNDERLINE = '<?= str_replace( "\n", "", addslashes( _BB_UNDERLINE )) ?>';
		var _BB_SUBSCRIPT = '<?= str_replace( "\n", "", addslashes( _BB_SUBSCRIPT )) ?>';
		var _BB_SUPERSCRIPT = '<?= str_replace( "\n", "", addslashes( _BB_SUPERSCRIPT )) ?>';
		var _BB_THUMB = '<?= str_replace( "\n", "", addslashes( _BB_THUMB )) ?>';
		var _BB_URL = '<?= str_replace( "\n", "", addslashes( _BB_URL )) ?>';
		var _BB_USERICON = '<?= str_replace( "\n", "", addslashes( _BB_USERICON )) ?>';
		var _BLANK_COMMENT = '<?= str_replace( "\n", "", addslashes( _BLANK_COMMENT )) ?>';
		var _EMOTICONS = '<?= str_replace( "\n", "", addslashes( _EMOTICONS )) ?>';
		var _EMOTICON_EXPLAIN = '<?= str_replace( "\n", "", addslashes( _EMOTICON_EXPLAIN )) ?>';
		var _ENTER_CLUBNAME = '<?= str_replace( "\n", "", addslashes( _ENTER_CLUBNAME )) ?>';
		var _ENTER_LINK_TEXT = '<?= str_replace( "\n", "", addslashes( _ENTER_LINK_TEXT )) ?>';
		var _ENTER_LINK_URL = '<?= str_replace( "\n", "", addslashes( _ENTER_LINK_URL )) ?>';
		var _ENTER_THUMBID = '<?= str_replace( "\n", "", addslashes( _ENTER_THUMBID )) ?>';
		var _ENTER_USERNAME = '<?= str_replace( "\n", "", addslashes( _ENTER_USERNAME )) ?>';
		var _FUZZY_0 = '<?= str_replace( "\n", "", addslashes( _FUZZY_0 )) ?>';
		var _FUZZY_1 = '<?= str_replace( "\n", "", addslashes( _FUZZY_1 )) ?>';
		var _FUZZY_2 = '<?= str_replace( "\n", "", addslashes( _FUZZY_2 )) ?>';
		var _FUZZY_3 = '<?= str_replace( "\n", "", addslashes( _FUZZY_3 )) ?>';
		var _FUZZY_7 = '<?= str_replace( "\n", "", addslashes( _FUZZY_7 )) ?>';
		var _FUZZY_11 = '<?= str_replace( "\n", "", addslashes( _FUZZY_11 )) ?>';
		var _FUZZY_24 = '<?= str_replace( "\n", "", addslashes( _FUZZY_24 )) ?>';
		var _FUZZY_48 = '<?= str_replace( "\n", "", addslashes( _FUZZY_48 )) ?>';
		var _FUZZY_99 = '<?= str_replace( "\n", "", addslashes( _FUZZY_99 )) ?>';
		var _FUZZY_234 = '<?= str_replace( "\n", "", addslashes( _FUZZY_234 )) ?>';
		var _FUZZY_456 = '<?= str_replace( "\n", "", addslashes( _FUZZY_456 )) ?>';
		var _FUZZY_987 = '<?= str_replace( "\n", "", addslashes( _FUZZY_987 )) ?>';
		var _HIDE = '<?= str_replace( "\n", "", addslashes( _HIDE )) ?>';
		var _INVALID_FILE_TYPE = '<?= str_replace( "\n", "", addslashes( _JS_INVALID_FILE_TYPE )) ?>';
		var _MORE = '<?= str_replace( "\n", "", addslashes( _MORE )) ?>';
		var _NO_BBCODE = '<?= str_replace( "\n", "", addslashes( _NO_BBCODE )) ?>';
		var _NO_EMOTICONS = '<?= str_replace( "\n", "", addslashes( _NO_EMOTICONS )) ?>';
		var _NO_SIG = '<?= str_replace( "\n", "", addslashes( _NO_SIG )) ?>';
		var _OEKAKI_NO_EDITOR = '<?= str_replace( "\n", "", addslashes( _JS_OEKAKI_NO_EDITOR )) ?>';
		var _PLEASE_WAIT = '<?= str_replace( "\n", "", addslashes( _JS_PLEASE_WAIT )) ?>';
		var _PREVIEW = '<?= str_replace( "\n", "", addslashes( _PREVIEW )) ?>';
		var _PREV_FLASH_MOVIE = '<?= str_replace( "\n", "", addslashes( _JS_PREV_FLASH_MOVIE )) ?>';
		var _PREV_TEXT_FILE = '<?= str_replace( "\n", "", addslashes( _JS_PREV_TEXT_FILE )) ?>';
		var _READONLY = '<?= str_replace( "\n", "", addslashes( _READONLY )) ?>';
		var _REQUIRE_LOGIN = '<?= str_replace( "\n", "", addslashes( _REQUIRE_LOGIN )) ?>';
		var _SEND_COMMENT = '<?= str_replace( "\n", "", addslashes( _SEND_COMMENT )) ?>';

		var Emoticons = new Array(
			<?

			$emots = array();

			foreach( $_emoticons as $emo1 )
			{
				if( !$emo1[ "emoExtra" ])
				{
					$emots[ $emo1[ "emoExpr" ]] = $emo1;
				}
			}

			ksort( $emots );

			$first = true;

			foreach( $emots as $emo1 )
			{
				if( $first )
				{
					$first = false;
				}
				else
				{
					echo ",";
				}

				?>{ expr: '<?= $emo1[ "emoExpr" ] ?>', icon: '<?= $emo1[ "emoFilename" ] ?>' }
				<?
			}

			unset( $emots );

			?>

		);

	//]]>
	</script>

	<script src="<?= urlf() ?>scripts/script18.js" type="text/javascript"></script>
	<script src="<?= urlf() ?>scripts/styleswitcher6.js" type="text/javascript"></script>
	<script src="<?= urlf() ?>scripts/instantreply4.js" type="text/javascript"></script>

	<script type="text/javascript">
	//<![CDATA[
		var domain = ".<?= $_config[ "galRoot" ] ?>";
		var comment_count = <?= $_auth[ "useid" ] != 0 ? $_auth[ "useUpdCom" ] : 0 ?>;
	//]]>
	</script>

</head>
<body>
<?

// -----------------------------------------------------------------------------
// Convince IE users to change their browser.

if( !$_config[ "disableIEWarning" ] && $isIE && $_cmd[ 0 ] != "noie" )
{
	?>
	<div class="header a_center" onclick="this.style.display='none'"
		style="position: absolute; top: 0; left: 0; color: #FFF; background: #000">
		<?= _NO_IE ?>
		&nbsp; &nbsp;
		<a href="<?= url( "noie" ) ?>"
			onclick="popup('<?= url( "noie", array( "popup" => "yes" ))?>'); return false">
			<?= _READ_MORE ?> &gt;</a></div>
	<?
}

// -----------------------------------------------------------------------------
// If the "popup" GET variable is set, we assume that it has to appear as a
// popup window; popup windows have reductive layout with only the page content
// shown.

if (isset( $_GET[ "popup" ]))
{
	?>
	<div class="outer">
		<?

		// Include the page content depending on the first pseudo subfolder
		// in the URL, e.g. a request to /edit/ would include p_edit.php here.

		iefixStart();

		$page = INCLUDES."p_".preg_replace( '/[^a-z0-9]/', "",
			$_cmd[ 0 ]).".php";

		include( file_exists( $page ) ? $page : INCLUDES."p_notfound.php" );

		iefixEnd();

		?>
	</div>
	<?=$_addToFooter?>
	<script type="text/javascript" src="http://www.google-analytics.com/ga.js"></script>
	<script type="text/javascript">//<![CDATA[
		try {
			var pageTracker = _gat._getTracker("UA-8845300-1");
			pageTracker._trackPageview();
		} catch(err) {}
	//]]></script>
	</body>
	</html>
	<?

	return;
}

// -----------------------------------------------------------------------------
// Tables are bad, but there is no other way to make the "rightside"
// bottom-aligned :(

?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<?/*
<tr style="height: 0">
	<td class="layoutleft" style="height: 0; font-size: 0px; line-height: 0"><br /></td>
	<td class="layoutright" style="height: 0; font-size: 0px; line-height: 0"><br /></td>
</tr>
*/?>
<tr>
<td class="layoutleft">

	<table border="0" cellpadding="0" cellspacing="0" class="layout_top">
		<tr>
			<td class="leftside" onclick="document.location='<?= url( "/" ) ?>';" style="cursor: pointer;">
				<div class="siteheader">
					<span onclick="document.location='<?= url( "/" ) ?>';"
						style="cursor: pointer;">
						<?= $_config[ "galName" ]?>
					</span>
				</div>
				<div class="siteheader header" style="padding-right: 20px">
					<span onclick="document.location='<?= url( "/" ) ?>';"
						style="cursor: pointer;">
						<?= $_config[ "galSubname" ]?>
					</span>
				</div>
			</td>
		</tr>
	</table>

	<div class="outer">
		<?

		// Include the page content depending on the first pseudo subfolder
		// in the URL, e.g. a request to /edit/ would include p_edit.php here.

		iefixStart();

		$page = INCLUDES."p_".preg_replace( '/[^a-z0-9]/', "",
			$_cmd[ 0 ]).".php";

		include( file_exists( $page ) ? $page : INCLUDES."p_notfound.php" );

		iefixEnd();

		?>
	</div>
</td>

<td class="layoutright">
	<div class="padded">
		<div class="container2 a_center">
			<div>
				<?

				if( isLoggedIn() )
					printf( _WELCOME_USER, getUserLink( $_auth[ "useid" ]));
				else
					printf( _WELCOME_USER, "guest" );

				?>
			</div>
			<?

			if( isLoggedIn() )
			{
				?>
				<div class="sep">
					<?

					// This is just a macro so to not write the same code 5 times
					// in a row (see below).

					function __updIcon( $count, $hint, $icon, $insertCmtId = false )
					{
						global $_auth, $_commentCountId;

						if( $count > 0 )
						{
							return getIMG( url()."images/emoticons/".$icon,
								'alt="'.substr( $hint, 0, 1 ).'" title="'.$hint.'"' ).
								( $_auth[ "useFuzzyNumbers" ] ? " " : "" ).
								'<span'.( $insertCmtId ? ' id="'.$_commentCountId.'"' : '' ).
								' class="v_middle">'.fuzzy_number( $count ).'</span>'.
								( $_auth[ "useFuzzyNumbers" ] ? "<br />" : " " );
						}
						else
							return "";
					}

					$updates =
						__updIcon( $_auth[ "useUpdWat" ], _MESSAGES, "watch.png" ).
						__updIcon( $_auth[ "useUpdFav" ], _FAVOURITES, "fav1.png" ).
						__updIcon( $_auth[ "useUpdCom" ], _COMMENTS, "comment.png", true ).
						__updIcon( $_auth[ "useUpdJou" ], _JOURNALS, "journal.png" ).
						__updIcon( $_auth[ "useUpdObj" ], _SUBMISSIONS, "submission.png" ).
						__updIcon( $_auth[ "useUpdExt" ], _SUBMIT_TYPE_EXTRA, "star4.png" );

					if( $updates != "" )
					{
						?>
						<a href="<?= url( "updates" ) ?>"><b><?= _UPDATES ?></b><br />
							<?= $updates ?></a>
						<?
					}

					?>
				</div>

				<div class="sep" style="margin-left: 8px; margin-right: 8px; white-space: nowrap">
					<a href="<?= url( "journal" )?>"><?= _JOURNAL ?></a>
					&middot;
					<a href="<?= url( "pm" )?>"><?= $_auth["useUpdPms"]
						? getIMG( url()."images/emoticons/pmnew.png" )." "._PRIVATE
						: getIMG( url()."images/emoticons/pmnonew.png" )." "._PRIVATE ?></a>
					&middot;
					<a href="<?= url( "settings/site" )?>"><?= _SETTINGS ?></a>
				</div>
				<?
			}
			else
			{
				?>
				<div class="sep"><?=_NOT_LOGGED ?></div>
				<?

				// Verify that the user has filled in the login form.

				if( isset( $_POST[ "username" ]) && isset( $_POST[ "password" ]))
				{
					if( $_POST[ "username" ] == "" )
					{
						trigger_error( _BLANK_USERNAME );
					}
					elseif( $_POST[ "password" ] == "" )
					{
						trigger_error( _BLANK_PASSWORD );
					}
					else
					{
						include_once( INCLUDES."authenticate.php" ); // Defines verifyLogin()

						$persistentLogin = isset( $_POST[ "persistent" ]) ? 1 : 0;

						if( verifyLogin( $_POST[ "username" ],
							$_POST[ "password" ], $persistentLogin ))
						{
							switch( $_cmd[ 0 ])
							{
								// Below is the list of pages that should redirect the
								// user to the front page after login:

								case "activate":
								case "emailchange":
								case "emailresend":
								case "join":
								case "logout":
								case "lostpassword":
								case "password":
								case "passwordchanged":
									redirect( url( "/" )); // redirect to the main page

								default:
									redirect( url( "." )); // redirect to the same page after login

								// Note: redirection is made to get rid of the POST data.
							}
						}
					}
				}

				?>
				<form action="<?= url( "." ) ?>" method="post">
					<div class="sep caption"><?= _USERNAME ?>:</div>
					<div><input class="narrow" name="username" type="text"
						<?=isset( $_POST[ "username" ])
							? 'value="'.htmlspecialchars( $_POST[ "username" ]).'"' : "" ?> /></div>

					<div class="sep caption"><?= _PASSWORD ?>:</div>
					<div><input class="narrow" name="password" type="password" /></div>

					<div class="sep"><input checked="checked" class="checkbox"
						id="loginPersistent" name="persistent" type="checkbox" />
						<label for="loginPersistent"><?= _USE_REMEMBER ?></label></div>

					<div class="sep">
						<button class="submit" type="submit">
							<?= getIMG( url()."images/emoticons/checked.png" ) ?>
							<?= _LOGIN ?>
						</button>
					</div>
					<div class="sep"><a href="<?= url( "join" )?>">
						<?= getIMG( url()."images/emoticons/star.png" ) ?>
						<?= _CREATE_ACCOUNT ?></a></div>
					<div class="sep"><a href="<?=url("lostpassword" ) ?>">
						<?= getIMG( url()."images/emoticons/sad.png" ) ?>
						<?= _USE_LOSTPASS ?></a></div>
				</form>
				<?

				if( isset( $_POST[ "language" ]))
				{
					$language = addslashes($_POST["language"]);
					$_auth["useLanguage"] = $language;
					$expiry = strtotime("+9 years");
					setcookie("yGalLanguage", $language, $expiry, "/", ".".$_config["galRoot"]); // give the user a fresh language cookie
					$GLOBALS["yGalLanguage"] = $language;
					redirect( url( "." ));
				}

				?>
				<form action="<?= url( "." ) ?>" method="post">
					<?

					$language = $_lang;

					$result = mysql_query( "SELECT * FROM `languages` ORDER BY `lanEngName`" )
						or trigger_error( _ERR_MYSQL );

					?>
					<div class="sep caption"><?= _SET_LANGUAGE ?>:</div>
					<select name="language" onchange="this.form.submit()">
						<?

						while( $rowData = mysql_fetch_assoc( $result ))
						{
							?>
							<option <?=( $language == $rowData[ "lanid" ] ?
								'selected="selected" ' : "" ) ?>
								value="<?= $rowData[ "lanid" ] ?>"><?=
								htmlspecialchars( $rowData[ "lanName" ]) ?>
								(<?= htmlspecialchars( $rowData["lanEngName" ]) ?>)</option>
							<?
						}

						?>
					</select>
				</form>
				<?
			}

			?>
		</div>
		<?

		if( isModerator() )
		{
			$found = false;

			ob_start();

			for( $isExtras = 0; $isExtras < 2; $isExtras++ )
			{
				$_objects = $isExtras ? "`extras`" : "`objects`";

				// Bring attention to the abuse cases pending a moderator's decision.

				$result = mysql_query( "SELECT `objid`,`objTitle`,`abuid` FROM `abuses`,$_objects".dbWhere( array(
					"abuObj*" => "objid",
					"abuIsExtras" => $isExtras,
					"abuMod" => "?" ))."ORDER BY `abuSubmitDate` DESC LIMIT 5" )
					or trigger_error( _ERR_MYSQL );

				while( $rowData = mysql_fetch_assoc( $result ))
				{
					?>
					<div>
						<?= getIMG( url()."images/emoticons/".( $isExtras ? "star4.png" : "keydelete.gif" )) ?>
						<a href="<?= url( "abuse/".$rowData[ "abuid" ]) ?>">
							<?= htmlspecialchars( $rowData[ "objTitle" ]) ?></a>
					</div>
					<?

					$found = true;
				}
			}

			$ht = ob_get_contents();
			ob_end_clean();

			if( $found )
			{
				?>
				<div class="caption error"><?= _MODERATOR ?><br /><?= _ABUSE_LIST ?>:</div>
				<div class="container2 mar_bottom">
				<?

				echo $ht;

				?>
				</div>
				<?
			}
		}

		if( isSModerator() )
		{
			$found = false;

			ob_start();

			$mod_count = 0;
			$adm_count = 0;

			for( $isExtras = 0; $isExtras < 2; $isExtras++ )
			{
				$_objects = $isExtras ? "`extras`" : "`objects`";

				// Bring attention to the abuse cases pending a supermoderator's decision.

				$result = mysql_query( "SELECT `objid`,`objTitle`,`abuid` FROM `abuses`,$_objects ".
					"WHERE `abuObj` = `objid` AND `abuIsExtras` = '$isExtras' AND `abuMod` <> '?' AND `abusMod` = '?' ".
					"ORDER BY `abuSubmitDate` DESC LIMIT 5" )
					or trigger_error( _ERR_MYSQL );

				$result_mod = mysql_query( "SELECT COUNT(*) FROM `abuses`,$_objects ".
					"WHERE `abuObj` = `objid` AND `abuIsExtras` = '$isExtras' AND `abuMod` = '?'" )
					or trigger_error( _ERR_MYSQL );

				$mod_count += mysql_result( $result_mod, 0 );

				$result_adm = mysql_query( "SELECT COUNT(*) FROM `abuses`,$_objects ".
					"WHERE `abuObj` = `objid` AND `abuIsExtras` = '$isExtras' AND `abuMod` <> '?' AND `abusMod` <> '?' ".
					"AND `abuMod` <> `abusMod` AND `aburMod` = '?'" )
					or trigger_error( _ERR_MYSQL );

				$adm_count += mysql_result( $result_adm, 0 );

				while( $rowData = mysql_fetch_assoc( $result ))
				{
					?>
					<div>
						<?= getIMG( url()."images/emoticons/".( $isExtras ? "star4.png" : "keydelete.gif" )) ?>
						<a href="<?= url( "abuse/".$rowData[ "abuid" ]) ?>">
							<?= htmlspecialchars( $rowData[ "objTitle" ]) ?></a>
					</div>
					<?

					$found = true;
				}
			}

			if( $mod_count > 0 )
			{
				?>
				<div<?= $found ? ' class="sep"' : "" ?>><?= _ABUSE_WAIT_MODERATOR.": ".$mod_count ?></div>
				<?

				$found = true;
			}

			if( $adm_count > 0 )
			{
				?>
				<div<?= $found ? ' class="sep"' : "" ?>><?= _ABUSE_WAIT_ADMINISTRATOR.": ".$adm_count ?></div>
				<?

				$found = true;
			}

			$ht = ob_get_contents();
			ob_end_clean();

			if( $found )
			{
				?>
				<div class="caption error"><?= _SUPERMODERATOR ?><br /><?= _ABUSE_LIST ?>:</div>
				<div class="container2 mar_bottom">
				<?

				echo $ht;

				?>
				</div>
				<?
			}
		}

		if( isAdmin() )
		{
			$found = false;

			ob_start();

			for( $isExtras = 0; $isExtras < 2; $isExtras++ )
			{
				$_objects = $isExtras ? "`extras`" : "`objects`";

				// Bring attention to the abuse cases pending admin's decision.

				$result = mysql_query( "SELECT `objid`,`objTitle`,`abuid` FROM `abuses`,$_objects ".
					"WHERE `abuObj` = `objid` AND `abuMod` <> '?' AND `abusMod` <> '?' ".
					"AND `abuMod` <> `abusMod` AND `aburMod` = '?' ".
					"ORDER BY `abuSubmitDate` DESC LIMIT 5" )
					or trigger_error( _ERR_MYSQL );

				while( $rowData = mysql_fetch_assoc( $result ))
				{
					?>
					<div>
						<?= getIMG( url()."images/emoticons/".( $isExtras ? "star4.png" : "keydelete.gif" )) ?>
						<a href="<?= url( "abuse/".$rowData[ "abuid" ]) ?>">
							<?= htmlspecialchars( $rowData[ "objTitle" ]) ?></a>
					</div>
					<?

					$found = true;
				}
			}
			
			$ht = ob_get_contents();
			ob_end_clean();

			if( $found )
			{
				?>
				<div class="caption error"><?=_ADMINISTRATOR ?><br /><?=_ABUSE_LIST ?>:</div>
				<div class="container2 mar_bottom">
				<?

				echo $ht;
		
				?>
				</div>
				<?
			}
		}

		if( !isset($_auth["useSidebarThumbs"]) || $_auth["useSidebarThumbs"])
		{
			?>
			<div class="sep caption"><?= _MOST_RECENT ?>:</div>
			<div class="container2 a_center">
				<?

				$select = "SELECT * FROM `objects`";
				$where = "`objPending` = '0' AND `objDeleted` = '0' AND ".
					"`objSubmitDate` > DATE_SUB(NOW(), INTERVAL ".
					$_config[ "mostRecentDays" ]." DAY)";
				$limit = 5;
				unset( $order );

				include( INCLUDES."mod_minigallery.php" );

				?>
			</div>
			<?
		}

		if( isLoggedIn() && trim( $_auth[ "useSidebar" ]) != "" )
		{
			?>
			<div class="sep caption"><?= _PERSONAL_NOTES ?>:</div>
			<div class="container2">
				<?= formatText( $_auth[ "useSidebar" ])?>
			</div>
			<?
		}

		if( file_exists( INCLUDES."extras.php" ))
			include( INCLUDES."extras.php" );

		if( !isset($_auth["useSidebarThumbs"]) || $_auth["useSidebarThumbs"])
		{
			?>
			<div class="sep caption"><?= _RANDOM ?>:</div>
			<div class="container2 a_center">
				<?

				iefixStart();

				$needRefresh = false;
				$objid = 0;

				$rndResult = mysql_query( "SELECT * FROM `randomObjects`".dbWhere( array(
					"rndFilterPtn" => $_auth[ "useObjFilters" ])).
					"LIMIT 1" ) or trigger_error( _ERR_MYSQL );

				if( $rndData = mysql_fetch_assoc( $rndResult ))
				{
					if( time() > $rndData[ "rndTimeout" ])
					{
						$needRefresh = true; // Random object has timed-out
					}
					else
					{
						$objid = $rndData[ "rndObject" ];
					}
				}
				else
				{
					$needRefresh = true;
				}

				mysql_free_result( $rndResult );

				if( $needRefresh )
				{
					$count = mysql_result( mysql_query(
						"SELECT MAX(`objid`) FROM `objects`" ), 0 );

					$where = "`objPending` = '0' AND `objDeleted` = '0'";

					applyObjFilters( $where );

					// We would give it 5 chances to pick a random submission :)
					// Actually there's a 99.999% possibility it would pick the good
					// one at first try.

					$objid = 0;

					for( $i = 1; $i <= 5; $i++ )
					{
						$id = mt_rand( 1, $count );

						$result = mysql_query( "SELECT `objid` FROM `objects` ".
							"WHERE ($where) AND `objid` = '$id' LIMIT 1" )
							or trigger_error( _ERR_MYSQL );

						if( mysql_num_rows( $result ) > 0 )
						{
							$objid = $id;
							break;
						}
					}

					$timeout = time() + 60; // Will expire in 1 minute

					mysql_query( "LOCK TABLES `randomObjects` WRITE" ) or trigger_error( _ERR_MYSQL );
					//mysql_query( "BEGIN" ) or trigger_error( _ERR_MYSQL );

					mysql_query( "DELETE FROM `randomObjects`".dbWhere( array(
						"rndFilterPtn" => $_auth[ "useObjFilters" ]))) or trigger_error( _ERR_MYSQL );

					mysql_query( "INSERT INTO `randomObjects`".dbValues( array(
						"rndFilterPtn" => $_auth[ "useObjFilters" ],
						"rndObject" => $objid,
						"rndTimeout" => $timeout ))) or trigger_error( _ERR_MYSQL );

					mysql_query( "UNLOCK TABLES" ) or trigger_error( _ERR_MYSQL );
					//mysql_query( "COMMIT" ) or trigger_error( _ERR_MYSQL );
				}

				// Show the thumbnail of that `objid`.

				$select = "SELECT * FROM `objects`";
				$where = "`objid` = '$objid'";
				$limit = 1;

				include( INCLUDES."mod_minigallery.php" );

				iefixEnd();

				?>
			</div>
			<?
		}

		// Include the poll. Global variable $_pollUser defines whose
		// poll that will be.

		if( isset( $_pollUser ) && $_pollUser != 0 )
		{
			$result1 = mysql_query( "SELECT * FROM `polls` ".
				"WHERE `polCreator` = '".intval($_pollUser)."' ".
				"ORDER BY `polSubmitDate` DESC LIMIT 1" )
				or trigger_error( _ERR_MYSQL );

			if( $polData = mysql_fetch_assoc( $result1 ))
			{
				$options = array();

				$result2 = mysql_query( "SELECT * FROM `pollOptions` ".
					"WHERE `polOPoll` = '".$polData[ "polid" ]."' ".
					"ORDER BY `polOVotes` DESC,`polOid` LIMIT 20" )
					or trigger_error(_ERR_MYSQL);

				while( $optData = mysql_fetch_assoc( $result2 ))
					$options[ $optData[ "polOid" ]] = $optData;

				if( count( $options ) > 0 )
				{
					?>
					<a name="poll"></a>
					<div class="sep caption"><?= _POLL ?>:</div>
					<div class="container2">
						<?

						include( INCLUDES."mod_poll.php" );

						?>
					</div>
					<?
				}
			}
		}

		?>
	</div>
</td>
</tr></table>

<div class="padded a_center smalltext" style="clear : both;">
	<a href="javascript:popup('<?= url( "tos", array( "popup" => "yes" )) ?>','tos')"><?= _TOS ?></a>
	<?

	// Show "Page generated in N seconds" if the user is at least
	// a moderator.

	if( atLeastModerator() )
	{
		include_once("serverload.php");
		$time_start = $_stats[ "startTime" ];
		$time_end = gettimeofday();
		$secdiff = $time_end[ "sec" ] - $time_start[ "sec" ];
		$usecdiff = $time_end[ "usec" ] - $time_start[ "usec" ];

		$generationTime = round(( $secdiff * 1000000 + $usecdiff ) / 1000000, 3 );

		$mysqlStat = mysql_stat();

		$queriesPerSecond = round( preg_replace( '/.*'.preg_quote(
			"Queries per second avg: " ).'([0-9\.]+).*/', "\\1", $mysqlStat ), 2 );

		if( isset( $_stats[ "startQueries" ]))
		{
			$queryCount = preg_replace( '/.*'.preg_quote( "Questions: " ).
				'([0-9]+).*/', "\\1", $mysqlStat ) - $_stats[ "startQueries" ];
		}
		else
		{
			$queryCount = 0;
		}

		$mysqlUptime = round( preg_replace( '/.*'.preg_quote(
			"Uptime: " ).'([0-9\.]+).*/', "\\1", $mysqlStat ), 2 );

		$upDays = floor( $mysqlUptime / 60 / 60 / 24 );
		$upHours = floor( $mysqlUptime / 60 / 60 ) % 24;
		$upMinutes = $mysqlUptime / 60 % 60;

		echo " &middot; ".sprintf( _PAGE_GENERATED, $generationTime ).
			" &middot; MySQL load: ".$queriesPerSecond." queries/sec, ".
			$queryCount." queries processed, uptime ".$upDays."d ".$upHours."h ".
			$upMinutes."m. &middot; 5-minute server load: $serverload.";
	}

	?>
</div>

<?= $_addToFooter ?>

<script type="text/javascript">
//<![CDATA[
	document.title = '<?= addslashes( $_documentTitle." :: ".$_config[ "galName" ]." :: ".$_config[ "galSubname" ]) ?>';
//]]>
</script>
<script type="text/javascript" src="http://www.google-analytics.com/ga.js"></script>
<script type="text/javascript">//<![CDATA[
	try {
		var pageTracker = _gat._getTracker("UA-8845300-1");
		pageTracker._trackPageview();
	} catch(err) {}
//]]></script>
</body>
</html>
