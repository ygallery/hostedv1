<?

$_documentTitle = _NEWS;

$newid = intval( $_cmd[ 1 ]);

if($_cmd[1] == 'edit' && $_cmd[2] != '')
{
	$_GET['edit'] = $_cmd[2];
}

if( isset($_POST['editNews']) && $_POST['editNews'] != "")
{
	if(!atLeastSModerator())
	{
		include( INCLUDES."p_notfound.php" );
		return;
	}

	$newid = (int)$_POST['editNews'];

	$sth = $dbh->prepare("UPDATE `news`
		SET `newComment` = ?
		WHERE `newid` = ?
		LIMIT 1");
	$sth->bindParam(1, $_POST['comment'], PDO::PARAM_STR);
	$sth->bindParam(2, $newid, PDO::PARAM_INT);
	$sth->execute();

	redirect( url( "news/".$newid ));
}

if(!isset($_GET['edit']))
{$result = sql_query( "SELECT * FROM `news` WHERE `newid` = '$newid' LIMIT 1" );

if( !$newData = mysql_fetch_assoc( $result ))
{
	include( INCLUDES."p_notfound.php" );
	return;
}}

include_once( INCLUDES."comments.php" );

?>
<?
	if( isset($_GET['edit']))
	{

		if( !atLeastSModerator())
		{
			include( INCLUDES."p_notfound.php" );
			return;
		}
		$sth = $dbh->prepare("SELECT *
			FROM `news`
			WHERE `newid` = ?
			LIMIT 1");
		$sth->bindParam(1, $_GET['edit'], PDO::PARAM_INT);
		$sth->execute();
		$newData = $sth->fetch(PDO::FETCH_ASSOC);
		?>
<div class="header">
	<div class="header_title">
		<?= _NEWS ?>
		<div class="subheader">Edit News: <?=$newData['newSubject'];?> (ID: <?=$newData['newid'];?>)</div>
	</div>
</div>
<div class="container">
<?= iefixStart() ?>
	<div class="header f_left"><?= _MESSAGE ?></div>
	<div class="header f_right a_right">
	</div>
	<div class="clear">&nbsp;</div>
	<?= iefixEnd() ?>

<div class="leftside">
	<div style="margin-top: -8px">
		<?
		$nid = (int)$_GET['edit'];
		$result = sql_query( "SELECT * FROM `news` WHERE `newid` = '$nid' LIMIT 1" );

if( !$newData = mysql_fetch_assoc( $result ))
{
	include( INCLUDES."p_notfound.php" );
	return;
}
		$comData = array();

		$comData[ "comid" ] = 0;
		$comData[ "comCreator" ] = $newData[ "newCreator" ];
		$comData[ "comComment" ] = $newData[ "newComment" ];
		$comData[ "comSubject" ] = $newData[ "newSubject" ];
		$comData[ "comSubmitDate" ] = $newData[ "newSubmitDate" ];
		$comData[ "comNoEmoticons" ] = $newData[ "newNoEmoticons" ];
		$comData[ "comNoSig" ] = $newData[ "newNoSig" ];
		$comData[ "comNoBBCode" ] = $newData[ "newNoBBCode" ];

		showComment( $comData, 0 );

		$commentDefault = $comData[ "comComment" ];

		?><br />
		<div style="float:right;">
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<div class="rightside" style="margin-top: -1px">
	<script type="text/javascript">
	//<![CDATA[
		document.write( showReplyFormHeader( '<?= $_currentPageURL ?>', '<?= $_currentPageURL ?>' ));
	//]]>
	</script>
	<noscript>
		<div class="container2 error">
			<?= _NEED_JAVASCRIPT ?>
		</div>
	</noscript>
	<?
	$commentWide = $_cmd[ 0 ] == "view" ? true : false;

	include( INCLUDES."mod_comment.php" );

	?>
	<input type="hidden" name="editNews" value="<?=$newData["newid"];?>"/>
	<script type="text/javascript">
	//<![CDATA[
		document.write( showReplyFormFooter() );
	//]]>
	</script>
</div>

	<div class="clear">&nbsp;</div>

</div>
		<?
	}
	else 
	{
	?>
<div class="header">
	<div class="header_title">
		<?= _NEWS ?>
		<div class="subheader"><?= $newData[ "newSubject" ] ?></div>
	</div>
</div>

<div class="container">

	<?= iefixStart() ?>
	<div class="header f_left"><?= _MESSAGE ?></div>
	<div class="header f_right a_right">
		<?= isset( $_GET[ "replied" ]) ? _COMMENTS : _LEAVE_COMMENT ?>
	</div>
	<div class="clear">&nbsp;</div>
	<?= iefixEnd() ?>

<div class="leftside">
	<div style="margin-top: -8px">
		<?

		$comData = array();

		$comData[ "comid" ] = 0;
		$comData[ "comCreator" ] = $newData[ "newCreator" ];
		$comData[ "comComment" ] = $newData[ "newComment" ];
		$comData[ "comSubject" ] = $newData[ "newSubject" ];
		$comData[ "comSubmitDate" ] = $newData[ "newSubmitDate" ];
		$comData[ "comNoEmoticons" ] = $newData[ "newNoEmoticons" ];
		$comData[ "comNoSig" ] = $newData[ "newNoSig" ];
		$comData[ "comNoBBCode" ] = $newData[ "newNoBBCode" ];

		showComment( $comData, 0 );

		?><br />
		<div style="float:right;">
		<?
			if(atLeastSModerator())
			{
		?>
			<form action="<?= url('news/edit/'.$newid)?>" method="POST">
				<button class="submit">Edit</button>
			</form>
		<? }
		?>
		</div>
		<div style="clear:both;"></div>
	</div>
</div>

<div class="rightside" style="margin-top: -1px">
	<?

	showAllComments( $newid, "new", false, true, true, false );

	?>
</div>
<div class="clear">&nbsp;</div>

</div>
<?
}
?>
