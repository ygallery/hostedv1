<?php

require_once('config.php');
$dsn = "mysql:host=$_sqlHostname;port=3306;dbname=$_sqlDatabase";
$pdo_conn = new PDO($dsn, $_sqlUsername, $_sqlPassword);

$cacheData = array();

$loadTime = microtime(true);

//$userNumber = mysql_result(sql_query("SELECT COUNT(*) FROM `useExtData` WHERE `useIsActive`='1'"),0);
$dbQuery = $pdo_conn->query("select count(*) as count from useExtData where useIsActive='1'");
$dbResult = $dbQuery->fetch();
$cacheData['user_total'] = $dbResult['count']; 

//$newUsersTime= ;

//Active users in the last 30 days
$dbQuery = $pdo_conn->prepare("select count(*) as count from users where useLastAction>:activeUsersTime");
$dbQuery->bindValue(':activeUsersTime', date("Y\-m\-d",time() - (60*60*24*30)));
$dbQuery->execute();
$dbResult = $dbQuery->fetch();
$cacheData['user_active'] = $dbResult['count'];


//$newUsers = mysql_result(sql_query("SELECT COUNT(*) FROM `useExtData` WHERE `useIsActive`='1' AND `useSignupDate`>'$newUsersTime'"),0);
$dbQuery = $pdo_conn->prepare("select count(*) as count from useExtData where useIsActive='1' and useSignupDate>:newUsersTime");
$dbQuery->bindValue(':newUsersTime', date("Y\-m\-d",time() - (60*60*24)));
$dbQuery->execute();
$dbResult = $dbQuery->fetch();
$cacheData['user_new'] = $dbResult['count'];

//$bannedUsers = mysql_result(sql_query("SELECT COUNT(*) FROM `users` WHERE `useIsBanned`='1' OR `useIsSuspended`='1'"),0);
$dbQuery = $pdo_conn->query("select count(*) as count from users where useIsBanned='1' or useIsSuspended='1'");
$dbResult = $dbQuery->fetch();
$cacheData['user_banned'] = $dbResult['count']; 

//$pendingAbuse = mysql_result(sql_query("SELECT COUNT(*) FROM `abuses` WHERE `abuFinal`='?' AND `abuResolved`='0'"),0);
$dbQuery = $pdo_conn->query("select count(*) as count from abuses where abuFinal='?' and abuResolved='0'");
$dbResult = $dbQuery->fetch();
$cacheData['user_unmodded_abuse'] = $dbResult['count']; 

//$openAbuse = mysql_result(sql_query("SELECT COUNT(*) FROM `abuses` WHERE `abuResolved`='0'"),0);
$dbQuery = $pdo_conn->query("select count(*) as count from abuses where abuResolved='0'");
$dbResult = $dbQuery->fetch();
$cacheData['user_open_abuses'] = $dbResult['count']; 

//$pendingUser = mysql_result(sql_query("SELECT COUNT(*) FROM `objects` WHERE `objPendingUser`='1'"),0);
$dbQuery = $pdo_conn->query("select count(*) as count from objects where objPendingUser='1'");
$dbResult = $dbQuery->fetch();
$cacheData['user_pending_abuses'] = $dbResult['count']; 


$cacheData['load_time'] = microtime(true) - $loadTime;
$cacheData['cache_time'] = time();

//print_r($cacheData);

$fw = fopen('cached_userstats.php', "w");
fwrite($fw, serialize($cacheData));
fclose($fw);


?>
