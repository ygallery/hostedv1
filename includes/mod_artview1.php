<?

ob_start();

?>
<div style="padding-top: 8px; margin-left: 10px">
<?

// Make a list of the users that made updates.

$where = "`updObj` = `objid` AND `useid` = `objCreator` ".
	"AND `updCreator` = '$creator' ".( $isExtras ? "AND `updType` = '".updTypeArtExtra."' " : "" ).
	"AND `objDeleted` = '0' AND `objPending` = '0'";

applyObjFilters( $where );

//$result = sql_query( "SELECT `useid`,`useUsername`,MAX(`objSubmitDate`) AS `MaxSubmitDate` ".
$result = sql_query( "SELECT `useid`,`useUsername`,MAX(`objLastEdit`) AS `MaxSubmitDate` ".
	"FROM `".( $isExtras ? "updates" : "updatesArt" )."`,$_objects,`users` WHERE $where ".
	"GROUP BY `useid` ORDER BY `MaxSubmitDate` DESC LIMIT 11" );

$artist = isset( $_GET[ "artist" ]) ? addslashes( $_GET[ "artist" ]) : "";

while( $rowData = mysql_fetch_assoc( $result ))
{
	if( $artist == "" )
		$artist = $rowData[ "useUsername" ];

	echo '<div class="mar_bottom">'.getUserAvatar( "", $rowData[ "useid" ],
		true, true, 100,
		url( "updates/".( $isExtras ? "extras" : "art" ),
			array( "artist" => $rowData[ "useUsername" ])))."</div>";
}

?>
</div>
<?

$artistsList = ob_get_contents();

ob_end_clean();

$where = "`updCreator` = '$creator' AND `useUsername` = '$artist' ".
	"AND `useid` = `objCreator` ".( $isExtras ? "AND `updType` = '".updTypeArtExtra."' " : "" ).
	"AND `updObj` = `objid` AND `objDeleted` = '0' AND `objPending` = '0'";

applyObjFilters( $where );

$result = sql_query( "SELECT `".( $isExtras ? "updates" : "updatesArt" )."`.*, $_objects.* ".
	"FROM `".( $isExtras ? "updates" : "updatesArt" )."`,$_objects,`users` ".
	"WHERE $where ORDER BY `objLastEdit` DESC LIMIT 12" );

$upd_art = array();

while( $row = mysql_fetch_assoc( $result ))
{
	if( $isExtras )
	{
		$upd_art[ "upd_".$row[ "updType" ]."_".$row[ "updObj" ]."_".$row[ "updUser" ]] = $row;
	}
	else
	{
		$upd_art[ "artupd_".$row[ "updObj" ]] = $row;
	}
}

$maxcols = 3;

?>
<form action="<?= url( "updates/".( $isExtras ? "extras" : "art" ), array( "artist" => $artist ))?>" method="post">
<div class="container2" style="padding: 2px">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td style="vertical-align: top">
<div class="container" style="height: 100%">
<div class="header"><?= $artist ?></div>
