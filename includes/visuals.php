<?

function displayTabStart()
{
	global $_newUI;

	if( isset( $_GET[ "newTabs" ]) /*isset( $_newUI )*/ )
	{
		?>
		<ul class="tabs">
		<?
	}
}

function displayTab( $isSelected = false, $title, $url, $icon = "" )
{
	global $_newUI;

	if( isset( $_GET[ "newTabs" ]) /*isset( $_newUI )*/ )
	{
		$iconImg = $icon != "" ?
			( getIMG( url()."images/emoticons/".$icon )." " ) : "";

		$class = $isSelected ? ' class="active"' : "";

		?>
		<li<?= $class ?>><a href="<?= $url ?>"><?= $iconImg ?><?= $title ?></a></li>
		<?
	}
	else
	{
		?>
		<div class="tab<?= $isSelected ? " tab_active" : "" ?>"
			onclick="document.location='<?= $url ?>'">
			<?= $icon != "" ? getIMG( url()."images/emoticons/".$icon ) : "" ?>
			<?= $title ?></div>
		<?
	}
}

function displayTabEnd()
{
	global $_newUI;

	if( isset( $_GET[ "newTabs" ]) /*isset( $_newUI )*/ )
	{
		?>
		</ul>
		<?
	}
}

?>