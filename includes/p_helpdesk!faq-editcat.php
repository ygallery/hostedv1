<?

if( !$isEditMode )
{
	return;
}

if( !isset( $catKey ))
{
	$catKey = "";
}

if( isset( $_POST[ $editAction."Category".$catKey ]))
{
	sql_values( array(
		"hfcName" => $_POST[ $editAction."Category".$catKey ],
		"hfcIdent" => $_POST[ "ident" ],
		"hfcIsGeneral" => isset( $_POST[ "isGeneral" ])));

	if( $editAction == "Add" )
	{
		sql_insert( "helpdeskFAQCats" );
	}

	if( $editAction == "Edit" && $catKey != "" )
	{
		sql_where( array( "hfcid" => $catKey ));
		sql_update( "helpdeskFAQCats" );
	}

	redirect( url( "." ));
}

?>
<div><br /></div>
<div class="container2 notsowide">
	<?

	$editCatTitle = isset( $catInfo[ "name" ]) ? $catInfo[ "name" ] : "";
	$editCatIdent = isset( $catInfo[ "ident" ]) ? $catInfo[ "ident" ] : "";
	$editCatGeneral = isset( $catInfo[ "general" ]) ? $catInfo[ "general" ] : 1;

	?>
	<form action="<?= url( ".", array( "enableEditMode" => "yes" )) ?>" method="post">
		<label for="idCatTitle<?= $catKey ?>"><?= $editAction ?> Category:</label>
		<input name="<?= $editAction ?>Category<?= $catKey ?>" type="text" size="30" id="idCatTitle<?= $catKey ?>"
			value="<?= htmlspecialchars( $editCatTitle ) ?>" />
		&nbsp;
		<label for="idIdent<?= $catKey ?>">Short:</label>
		<input name="ident" type="text" id="idIdent<?= $catKey ?>" size="10" value="<?= htmlspecialchars( $editCatIdent ) ?>" />
		<div class="mar_top">
			<input name="isGeneral" type="checkbox"<?= $editCatGeneral ? ' checked="checked"' : '' ?>
				id="idIsGeneral<?= $catKey ?>" />
			<label for="idIsGeneral<?= $catKey ?>">List this category on the main FAQ page (otherwise it's a 'hidden' category)</label>
		</div>
		<div class="mar_top">
			<button class="smalltext"><?= getIMG( url()."images/emoticons/checked.png" )?>
				<?= $editAction ?></button>
		</div>
	</form>
</div>
