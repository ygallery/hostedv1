<?
	$_documentTitle = _OEKAKI;

	if($_config["readOnly"]) {
		echo '<div class="header">'._SUBMIT_TITLE.'</div>'.
			'<div class="container">';
		notice(_READONLY);
		echo '</div>';
		return;
	}

	// oekaki is unable to send authentification information
	if(!isset($_GET["getFromEditor"])) {
		if(!$_auth["useid"]) {
			header("Status: 403 Forbidden");
			echo '<div class="header">'._SUBMIT_TITLE.'</div>'.
				'<div class="container">';
			notice(_REQUIRE_LOGIN);
			echo '</div>';
			return;
		}
	}

	$containsAnimation = false;
	$usedEditor = 0;

	if($_cmd[1] != "") {
		$query = "SELECT `objImageWidth`,`objImageHeight`,`objNumEdits`,`objid`,`objSubmitDate`,`objLastEdit`,`objFilename`,`objAniType` FROM `objects`,`objExtData` WHERE `objFilename` LIKE '/oekaki/%' AND `objCreator` = '".$_auth["useid"]."' AND `objEid` = `objid` AND `objEid` = '".intval($_cmd[1])."'".
		( atLeastModerator() ? "" : " AND `objPending` = '0' AND `objDeleted` = '0'" ).
		" LIMIT 1";

		$result = sql_query($query);
		if(!mysql_num_rows($result)) {
			include(INCLUDES."p_notfound.php");
			return;
		}
		$objData = mysql_fetch_assoc($result);
		if($objData["objNumEdits"] >= $_config["maxObjectEdits"]) {
			echo '<div class="header"><div class="header_title">'._OEKAKI;
			echo '<div class="subheader">'._OEKAKI_SUBTITLE_EDIT.'</div>';
			echo '</div></div><div class="container">';
			notice(_EDIT_EXCEEDED);
			echo '</div>';
			return;
		}
		if($objData["objAniType"] != "no")
			$containsAnimation = true;

		switch($objData["objFilename"]) {
			case "/oekaki/".OEK_SHI: $usedEditor = OEK_SHI; break;
			case "/oekaki/".OEK_SHIPRO: $usedEditor = OEK_SHIPRO; break;
			case "/oekaki/".OEK_PAINTBBS: $usedEditor = OEK_PAINTBBS; break;
			case "/oekaki/".OEK_OEKAKIBBS: $usedEditor = OEK_OEKAKIBBS; break;
		}
	}

	if(isset($_GET["getFromEditor"]) && isset($_GET["saveAnimation"])) {
		$editor = intval($_GET["getFromEditor"]);
		$saveAnimation = intval($_GET["saveAnimation"]) > 0 ? 1 : 0;
		// process data returned from an oekaki applet
		ob_end_clean();
		oekakiProcess($editor, $saveAnimation);
		exit();
	}

	if(isset($_GET["viewAnimation"])) {
		echo '<div class="header"><div class="header_title">'._OEKAKI;
		echo '<div class="subheader">'._ANIMATION.'</div></div></div>';
		echo '<div class="container">';
		oekakiViewAnimation(intval($_GET["viewAnimation"]));
		echo '</div>';
		return;
	}

	if(isset($_GET["editor"]) && isset($_GET["width"]) && isset($_GET["height"]) &&
		isset($_GET["saveAnimation"]) && isset($_GET["editObject"])) {
		$editor = intval($_GET["editor"]);
		$editObject = intval($_GET["editObject"]);
		if($editObject > 0) {
			$query = "SELECT `objImageWidth`,`objImageHeight` FROM `objects`,`objExtData` WHERE `objFilename` LIKE '/oekaki/%' AND `objCreator` = '".$_auth["useid"]."' AND `objEid` = `objid` AND `objEid` = '$editObject'".
			( atLeastModerator() ? "" : " AND `objPending` = '0' AND `objDeleted` = '0'" ).
			" LIMIT 1";
			$result = sql_query($query);
			if(!mysql_num_rows($result)) {
				include(INCLUDES."p_notfound.php");
				return;
			}
			$rowData = mysql_fetch_assoc($result);
			$width = $rowData["objImageWidth"];
			$height = $rowData["objImageHeight"];
		}
		else {
			$width = intval($_GET["width"]);
			$height = intval($_GET["height"]);
			if($width < 100) $width = 100;
			if($height < 100) $height = 100;
		}
		$saveAnimation = intval($_GET["saveAnimation"]) > 0 ? 1 : 0;
		// start an oekaki applet
		echo '<div class="header"><div class="header_title">';
		echo '<div class="subheader f_right"><a href="'.url("/").'">'.$_config["galName"].'</a></div>';
		echo _OEKAKI.'</div></div>';
		echo '<div class="container">';
		oekakiStart($editor, $width, $height, $saveAnimation, $editObject);
		echo '</div>';
		return;
	}
?>

<div class="header">
	<div class="header_title">
		<?=_OEKAKI ?>
		<div class="subheader"><?=$_cmd[1] != "" ? _OEKAKI_SUBTITLE_EDIT : _OEKAKI_SUBTITLE ?></div>
	</div>
</div>

<div class="container">
	<form action="<?=url(".")?>" method="get" id="oekakiForm">
	<?
	if($_cmd[1] == "") {
		?>
		<div class="caption"><?=_OEKAKI_RESOLUTION ?>:</div>
		<div class="container2 notsowide mar_bottom">
			<input type="text" name="width" value="500" size="4" />
			&times;
			<input type="text" name="height" value="500" size="4" />
			<?=_OEKAKI_PIXELS ?>
		</div>
		<?
	}
	else {
		?>
		<input type="hidden" name="width" value="0" />
		<input type="hidden" name="height" value="0" />
		<?
	}
	?>
	<div class="caption"><?=_OEKAKI_APPLETS ?>:</div>
	<div class="container2 notsowide">
		<?/*
		<input disabled="disabled" class="radio" type="radio" name="editor" value="<?=OEK_OEKAKIBBS ?>" id="oekakiTypeOekakiBBS"
			<?=$usedEditor == OEK_OEKAKIBBS ? 'checked="checked"' : "" ?> />
		<label for="oekakiTypeOekakiBBS">OekakiBBS -- not working yet T_T</label>
		<br />
		*/?>
		<input class="radio" type="radio" name="editor" value="<?=OEK_PAINTBBS ?>" id="oekakiTypePaintBBS"
			<?=$usedEditor == OEK_PAINTBBS ? 'checked="checked"' : "" ?> />
		<label for="oekakiTypePaintBBS">PaintBBS</label>
		<br />
		<input class="radio" type="radio" name="editor" value="<?=OEK_SHI ?>" id="oekakiTypeShiPainter"
			<?=$usedEditor == OEK_SHI ? 'checked="checked"' : "" ?> />
		<label for="oekakiTypeShiPainter">ShiPainter</label>
		<br />
		<input class="radio" type="radio" name="editor" value="<?=OEK_SHIPRO ?>" id="oekakiTypeShiPainterPro"
			<?=$usedEditor == OEK_SHIPRO ? 'checked="checked"' : "" ?> />
		<label for="oekakiTypeShiPainterPro">ShiPainter Pro</label>
	</div>
	<?
	if($_cmd[1] == "" || $containsAnimation) {
		?>
		<div class="sep caption"><?=_OEKAKI_OPTIONS ?>:</div>
		<div class="container2 notsowide">
			<input type="checkbox" class="checkbox" name="saveAnimation" id="oekakiSaveAnimation"
				<?=$containsAnimation ? 'checked="checked"' : "" ?> />
			<label for="oekakiSaveAnimation"><?=$containsAnimation ? _OEKAKI_CONTINUE_ANIMATION : _OEKAKI_SAVE_ANIMATION ?></label>
			<?=$containsAnimation ? '<div class="sep">'._OEKAKI_ANIM_WARNING.'</div>' : "" ?>
		</div>
		<?
	}
	else {
		?>
		<input type="hidden" name="saveAnimation" value="0" />
		<?
	}
	?>
	<div class="sep">
		<button type="button" class="submit" onclick="start_oekaki('oekakiForm','<?=url("oekaki", array("popup" => "yes", "editObject" => intval($_cmd[1])))?>')">
			<?=getIMG(url()."images/emoticons/checked.png")?>
			<?=_OEKAKI_RUN ?>
		</button>
	</div>
	</form>
</div>

<?

// Puts an Oekaki Java object in the current window.
function oekakiStart($editor, $canvasWidth, $canvasHeight, $saveAnimation = false, $editObject = 0) {
	global $_auth;
	$appletWidth = 740;
	$appletHeight = 550;
	$imageURL = "";

	if($editObject > 0) {
		$query = "SELECT `objid`,`objSubmitDate`,`objLastEdit`,`objExtension` FROM `objects`,`objExtData` WHERE `objEid` = `objid` AND `objid` = '".intval($editObject)."' AND `objCreator` = '".$_auth["useid"]."'".
		( atLeastModerator() ? "" : " AND `objPending` = '0' AND `objDeleted` = '0'" ).
		" LIMIT 1";
		$result = sql_query($query);
		if(!$objData = mysql_fetch_assoc($result)) {
			redirect(url("notfound"));
			return; // cannot edit this submission
		}

		$imageOrigURL = applyIdToPath("files/data/", $objData["objid"])."-".preg_replace('/[^0-9]/', "", $objData["objLastEdit"]).".".$objData["objExtension"];
		$imageURL = "files/oekakitemp/".$_auth[ "useid" ]."-".strval( time() ).".".$objData["objExtension"];

		copy( $imageOrigURL, $imageURL );
	}

	$saveURL = url("oekaki", array(
		"oekakiSession" => $_auth["useid"],
		"getFromEditor" => $editor,
		"saveAnimation" => $saveAnimation), "&");

	if($editObject > 0)
		$exitURL = url("edit/".$editObject, array("edit" => "file", "oekaki" => $editor), "&");
	else
		$exitURL = url("submit/oekaki/".$editor);

	switch($editor) {
		case OEK_SHI:
		case OEK_SHIPRO:
			$help = _OEKAKI_HELP_SHI;
			$appletCode = "c.ShiPainter";
			$appletArchive = url()."scripts/oekaki/spainter_all.jar";
			$appletParams = array(
				"image_width" => $canvasWidth,
				"image_height" => $canvasHeight,
				"dir_resource" => url()."scripts/oekaki/shipainter/",
				//"tt.zip" => "tt_def.zip", -- these files are inside the jar
				"tt.zip" => "tt_def.zip",
				//"res.zip" => "res.zip", -- these files are inside the jar
				"res.zip" => "res.zip",
				"tools" => ($editor == OEK_SHIPRO ? "pro" : "normal"),
				"layer_count" => 5,
				"MAYSCRIPT" => "true",
				"scriptable" => "true",
				"image_jpeg" => "false",
				"image_size" => 65,
				"compress_level" => 10,
				"undo" => 90,
				"undo_in_mg" => 15,
				"url_target" => "_self",
				"url_save" => $saveURL,
				"url_exit" => $exitURL,
				"color_text" => "#000000",
				"color_bk" => "#eeeeee",
				"color_bk2" => "#dddddd",
				"color_icon" => "#cccccc",
				"poo" => "false",
				"send_header" => "",
				"send_header_image_type" => "false",
				"send_advance" => "true",
				"send_language" => "utf8");

			if($editor == OEK_SHIPRO)
				$appletParams["cursor_1"] = "cursor.gif";

			if($editObject > 0) {
				$appletParams["image_canvas"] = urlf().$imageURL;

				if($saveAnimation)
					$appletParams["pch_file"] = preg_replace('/png$/', 'pch', urlf().$imageURL);
			}

			if($saveAnimation)
				$appletParams["thumbnail_type"] = "animation";

			break;

		case OEK_PAINTBBS:
			$help = _OEKAKI_HELP_PAINTBBS;
			$appletCode = "pbbs.PaintBBS";
			$appletArchive = url()."scripts/oekaki/PaintBBS.jar";
			$appletParams = array(
				"image_width" => $canvasWidth,
				"image_height" => $canvasHeight,
				"image_jpeg" => "false",
				"image_size" => 65,
				"compress_level" => 4,
				"undo" => 60,
				"undo_in_mg" => 12,
				"url_target" => "_self",
				"url_save" => $saveURL,
				"url_exit" => $exitURL,
				"image_bkcolor" => "#ffffff",
				"color_text" => "#000000",
				"color_bk" => "#eeeeee",
				"color_bk2" => "#dddddd",
				"color_icon" => "#cccccc",
				"poo" => "false",
				"send_header" => "",
				"send_header_image_type" => "false",
				"tool_advance" => "true",
				"send_advance" => "true",
				"send_language" => "utf8");

			if($editObject > 0) {
				if($saveAnimation)
					$appletParams["image_canvas"] = preg_replace('/png$/', 'pch', urlf().$imageURL);
				else
					$appletParams["image_canvas"] = urlf().$imageURL;
			}

			if($saveAnimation) {
				$appletParams["thumbnail_type"] = "animation";
			}
			break;

		case OEK_OEKAKIBBS:
			$help = _OEKAKI_HELP_OEKAKIBBS;
			$appletCode = "a.p";
			$appletArchive = url()."scripts/oekaki/oekakibbs.jar";
			$appletParams = array(
				"cgi" => $saveURL,
				"url" => $exitURL,
				"popup" => 1,
				"tooltype" => "full",
				"anime" => $saveAnimation ? 1 : 0,
				"animesimple" => 1,
				"tooljpgpng" => 0,
				"tooljpg" => 0,
				"passwd" => "hardidhihihiahrih",
				"passwd2" => "I like to eat, ice cream.",
				"picw" => $canvasWidth,
				"pich" => $canvasHeight,
				"baseC" => "dddddd",
				"brightC" => "ffffff",
				"darkC" => "666666",
				"backC" => "cccccc",
				"mask" => 5,
				"toolpaintmode" => 1,
				"toolmask" => 1,
				"toollayer" => 1,
				"toolalpha" => 1,
				"toolwidth" => 200,
				"target" => "_self",
				"catalog" => 0,
				"catalogwidth" => 100,
				"catalogheight" => 100);

			if($editObject > 0) {
				$appletParams["readfilepath"] = urlf().dirname($imageURL)."/";
				$appletParams["readpicpath"] = urlf().dirname($imageURL)."/";
				$appletParams["readpic"] = basename($imageURL);

				if($saveAnimation) {
					$appletParams["readanmpath"] = urlf().dirname($imageURL)."/";
					$appletParams["readanm"] = preg_replace('/png$/', 'oeb', basename($imageURL));
				}
			}
			break;

		default:
			return;
	}
	$params = "";
	foreach($appletParams as $name => $value)
		$params .= '<param name="'.$name.'" value="'.htmlspecialchars(strval($value)).'" />'."\n\t\t";

	// Changed from embedded objects (which caused a "you need this plugin" message)
	// to browser detection
	?>
	<div align="center">
	<?
		if(ereg("MSIE",$_SERVER["HTTP_USER_AGENT"]) && !ereg("Opera",$_SERVER["HTTP_USER_AGENT"]))
		{
	?>
	<object
		classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93"
		codebase="http://java.sun.com/update/1.5.0/jinstall-1_5-windows-i586.cab#Version=1,5,0,4"
		width="<?=$appletWidth ?>" height="<?=$appletHeight ?>">
		<param name="code" value="<?=$appletCode ?>" />
		<param name="archive" value="<?=$appletArchive ?>" />
		<?=$params ?>
	</object>
	<?
		}
		else
		{
	?>
	<object
		classid="java:<?=$appletCode ?>.class"
		archive="<?=$appletArchive ?>"
		width="<?=$appletWidth ?>" height="<?=$appletHeight ?>">
		<?=$params ?>
		<?=sprintf(_OEKAKI_NO_JAVA,'http://javashoplm.sun.com/ECom/docs/Welcome.jsp?StoreId=22&amp;PartDetailId=jre-1.5.0_04-oth-JPR&amp;SiteId=JSC&amp;TransactionId=noreg') ?>
	</object>
	<?
		}
	?>
	<br />
	<br />
	<?=$help?>
	</div>
	<?
}

// Processes the data returned from an Oekaki applet.
function oekakiViewAnimation($object) {
	$appletWidth = 740;
	$appletHeight = 550;
	$imageURL = "";

	$query = "SELECT `objid`,`objSubmitDate`,`objLastEdit`,`objExtension`,`objImageWidth`,`objImageHeight`,`objFilename` FROM `objects`,`objExtData` WHERE `objEid` = `objid` AND `objid` = '".intval($object)."' AND `objAniType` <> 'no'".
		( atLeastModerator() ? "" : " AND `objPending` = '0' AND `objDeleted` = '0'" ).
		" LIMIT 1";

	$result = sql_query($query);
	if(!$objData = mysql_fetch_assoc($result)) {
		redirect(url("notfound"));
		return; // cannot edit this submission
	}
	$imageURL = applyIdToPath("files/data/", $objData["objid"])."-".preg_replace('/[^0-9]/', "", $objData["objLastEdit"]).".".$objData["objExtension"];

	$editor = 0;
	switch($objData["objFilename"]) {
		case "/oekaki/".OEK_SHI: $editor = OEK_SHI; break;
		case "/oekaki/".OEK_SHIPRO: $editor = OEK_SHIPRO; break;
		case "/oekaki/".OEK_PAINTBBS: $editor = OEK_PAINTBBS; break;
		case "/oekaki/".OEK_OEKAKIBBS: $editor = OEK_OEKAKIBBS; break;
	}

	switch($editor) {
		case OEK_SHI:
		case OEK_SHIPRO:
			$appletArchive = url()."scripts/oekaki/shipainter/PCHViewer.jar";
			$appletCode = "pch2.PCHViewer";
			$appletParams = array(
				"speed" => 2,
				"image_width" => $objData["objImageWidth"],
				"image_height" => $objData["objImageHeight"],
				"pch_file" => preg_replace('/png$/', 'pch', urlf().$imageURL),
				// something here is causing a nullpointerexception
				//"res.zip" => "res.zip",
				"res.zip" => "../scripts/oekaki/shipainter/res.zip",
				//"tt.zip" => "tt.zip",
				"tt.zip" => "../scripts/oekaki/shipainter/tt.zip",
				"run" => "true",
				"buffer_progress" => "true",
				"buffer_canvas" => "true",
				"color_icon" => "#eeeeee",
				"color_bar" => "#dddddd",
				"color_bar_select" => "#ffffff",
				"color_back" => "#eeeeee",
				"layer_count" => 5,
				"layer_max" => 8,
				"layer_last" => 2,
				"quality" => 1);
			break;

		case OEK_PAINTBBS:
			$appletArchive = url()."scripts/oekaki/shipainter/PCHViewer.jar";
			$appletCode = "pch.PCHViewer";
			$appletParams = array(
				"speed" => 2,
				"image_width" => $objData["objImageWidth"],
				"image_height" => $objData["objImageHeight"],
				"pch_file" => preg_replace('/png$/', 'pch', urlf().$imageURL),
				"run" => "true",
				"buffer_progress" => "true",
				"buffer_canvas" => "true",
				"color_icon" => "#eeeeee",
				"color_bar" => "#dddddd",
				"color_bar_select" => "#ffffff",
				"color_back" => "#eeeeee");
			break;

		case OEK_OEKAKIBBS:
			$appletArchive = url()."scripts/oekaki/shipainter/oekakibbs.jar";
			$appletCode = "a.p";
			$appletParams = array(
				"popup" => 0,
				"anime" => 2,
				"readanm" => preg_replace('/png$/', 'oeb', basename($imageURL)),
				"readanmpath" => urlf().dirname($imageURL)."/",
				"picw" => $objData["objImageWidth"],
				"pich" => $objData["objImageHeight"],
				"baseC" => "888888",
				"brightC" => "aaaaaa",
				"darkC" => "666666",
				"backC" => "000000",
				"buffer_canvas" => "true");
			break;

		default:
			notice(_OEKAKI_UNKNOWN_EDITOR);
			return;
	}
	$params = "";
	foreach($appletParams as $name => $value)
		$params .= '<param name="'.$name.'" value="'.htmlspecialchars(strval($value)).'" />'."\n\t\t";

	// Changed from embedded objects (which caused a "you need this plugin" message)
	// to browser detection
	?>
	<div align="center">
	<?
		if(ereg("MSIE",$_SERVER["HTTP_USER_AGENT"]) && !ereg("Opera",$_SERVER["HTTP_USER_AGENT"]))
		{
	?>
	<object
		classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93"
		codebase="http://java.sun.com/update/1.5.0/jinstall-1_5-windows-i586.cab#Version=1,5,0,4"
		width="<?=$appletWidth ?>" height="<?=$appletHeight ?>" >
		<param name="code" value="<?=$appletCode ?>" />
		<param name="archive" value="<?=$appletArchive ?>" />
		<?=$params ?>
	</object>
	<?
		}
		else
		{
	?>
	<object
		type="application/x-java-applet"
		classid="java:<?=$appletCode ?>.class"
		archive="<?=$appletArchive ?>"
		width="<?=$appletWidth ?>" height="<?=$appletHeight ?>">
		<?=$params ?>
		<?=sprintf(_OEKAKI_NO_JAVA,'http://javashoplm.sun.com/ECom/docs/Welcome.jsp?StoreId=22&amp;PartDetailId=jre-1.5.0_04-oth-JPR&amp;SiteId=JSC&amp;TransactionId=noreg') ?>
	</object>
	<?
		}
	?>
	<form action="<?=url("view/".$object)?>" method="get">
		<div class="sep">
			<button type="submit" class="submit">
				<?=getIMG(url()."images/emoticons/nav-prev.png")?>
				<?=_RETURN ?>
			</button>
		</div>
	</form>
	</div>
	<?
}

// Processes the data returned from an Oekaki applet.
function oekakiProcess($editor, $saveAnimation = false) {
	// user's global session id is used as oekaki session id
	$oekakiSession = $_GET["oekakiSession"];
	$baseFilename = "files/oekakitemp/".$oekakiSession;
	include_once(INCLUDES."files.php"); // for forceFolders()
	forceFolders(dirname($baseFilename));
	// delete old files if any
	foreach(glob($baseFilename."-*.*") as $filename)
		@unlink($filename);

	$baseFilename .= "-".strval(time());

	$data = $GLOBALS["HTTP_RAW_POST_DATA"];
	switch($editor) {
		case OEK_SHI:
		case OEK_SHIPRO:
		case OEK_PAINTBBS:
			$signature = $editor == OEK_PAINTBBS ? "P" : "S";
			if(substr($data, 0, 1) != $signature) return;
			$headerLength = intval(substr($data, 1, 8));
			$header = substr($data, 9, $headerLength);
			$imageLength = intval(substr($data, $headerLength + 9, 8));
			$image = substr($data, $headerLength + 19, $imageLength);
			$animLength = intval(substr($data, $headerLength + $imageLength + 19, 8));
			$anim = substr($data, $headerLength + $imageLength + 27, $animLength);
			$animExt = ".pch";
			break;

		case OEK_OEKAKIBBS:
			$start = strpos($data, "Content-type:");
			$middle = 0;
			$end = 0;
			while($start) {
				$end = strpos($data, "Content-type:", $start + 1);
				$middle = strpos($data, "\r", $start);
				$type = substr($data, $start + 13, $middle - $start - 13);
				$middle = strpos($data, "\r", $middle + 1);
				if ($end === false) {
					$end = null;
					$block = substr($data, $middle + 2);
				} else {
					$block = substr($data, $middle + 2, $end-$middle - 2);
				}
				$start = $end;
				if($type == "image/0") // PNG
					$image = $block;
				if($type == "animation/") {
					$anim = $block;
					$animLength = strlen($anim);
					$animExt = ".oeb";
				}
				unset($block);
			}

			break;
	}

	// save the image
	$fp = fopen($baseFilename.".png", "wb");
	fwrite($fp, $image);
	fclose($fp);

	// save the animation
	if($saveAnimation && $animLength > 0) {
		$fp = fopen($baseFilename.$animExt, "wb");
		fwrite($fp, $anim);
		fclose($fp);
	}
}

?>
