<?php

// Add this for full XML compliance, but of course IE will fail and
// drop out of standards mode if it exists, so only send it to browsers
// that accept application/xhtml+xml

if( isset( $_SERVER[ "HTTP_ACCEPT" ]) && stristr( $_SERVER[ "HTTP_ACCEPT" ], "application/xhtml+xml" ))
{
	echo( "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" );
}

if( $_auth[ "useEnableUI2" ] > 0 )
{
	$_newUI = $_auth[ "useEnableUI2" ];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= $_auth["useid"] ? $_auth["useLanguage"] : "en" ?>" lang="<?= $_auth["useid"] ? $_auth["useLanguage"] : "en" ?>">
<head>
	<?php

	// This is the default title of the page. It will be then overridden
	// via Javascript with the value $_documentTitle has at the end of
	// execution.

	?><title><?= $_config[ "galName" ] ?> :: <?= $_config[ "galSubname" ] ?></title>
	<?

	// We've decided UTF-8 should be enough for everyone, instead of
	// making support for a bunch of different charsets.

	/* This is handled in main.php now, sending the actual Content-Type header :)
	?>
	<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8" />
	<?
	*/

	// The following option will hide the annoying toolbar appearing above
	// large images in IE.

	?><meta http-equiv="ImageToolbar" content="no" />
	<link rel="Shortcut Icon" type="image/ico" href="<?= url() ?>favicon.ico" />
	<link href="<?= urlf() ?>themes/style.css?4" rel="stylesheet" type="text/css" media="screen, projection" />
	<?

	if( $_cmd[ 0 ] != "updatestrings" )
	{
		if( isset( $_newUI ))
		{
			?>
			<link href="<?= urlf() ?>themes/ui.v2.0.css?8" rel="stylesheet" type="text/css"
				media="screen, projection" />
			<?

			if( $_newUI == 2 )
			{
				?>
				<link href="<?= urlf() ?>themes/ui.v2.0.notop.css?1" rel="stylesheet" type="text/css"
					media="screen, projection" />
				<?
			}
		}

		include( INCLUDES."themes.inc" );

		$_disableCustom = isset( $_auth[ "useDisableCustom" ]) ? $_auth[ "useDisableCustom" ] : 0;

		if( $_disableCustom != 1 ) // 1 = Disable custom theme
		{
			$_customTheme = "";

			if( isLoggedIn() && $_disableCustom != 2 ) // 2 = Use my custom theme on my own pages only
			{
				// Apply current user's custom theme by default. If they are browsing
				// another user's page, $_customTheme will be overridden below.

				$_tmpfn = findNewestFile( applyIdToPath( "files/themes/",
					$_auth[ "useid" ])."/style-*.css" );

				if( $_tmpfn != "" )
				{
					$_customTheme = $_tmpfn;
				}
			}

			if( $_disableCustom != 3 ) // 3 = Always use my custom theme on ALL pages
			{
				$_userPages = array( "clubs", "collabs", "fromothers", "forothers", "extras",
					"favourites", "friends", "gallery", "galleryclubs", "gallerydeleted",
					"journal", "poll", "stats", "themedesigner", "user", "watches", "watchedby" );

				$_clubPages = array( "announcement", "club", "clubgallery", "clubthemedesigner",
					"manageclub", "members" );

				if( in_array( $_cmd[ 0 ], $_userPages ))
				{
					// We're viewing a page that belongs to a certain user.

					$_tmpfn = findNewestFile( applyIdToPath( "files/themes/",
						getUserId( $_cmd[ 1 ]))."/style-*.css" );

					if( $_tmpfn != "" )
					{
						$_customTheme = $_tmpfn;
					}
				}

				$_viewPages = array( "view", "discuss", "suggest" );

				if( in_array( $_cmd[ 0 ], $_viewPages ))
				{
					// We're viewing a submission that belongs to a certain user.

					if( substr( $_cmd[ 1 ], 0, 1 ) == "e" )
					{
						$objid = substr( $_cmd[ 1 ], 1 );
						$_objects = "`extras`";
					}
					else
					{
						$_objects = "`objects`";
						$objid = $_cmd[ 1 ];
					}

					$_tmpResult = sql_query( "SELECT `objCreator`,`objForClub` FROM $_objects".dbWhere( array(
						"objid" => $objid ))."LIMIT 1" );

					if( $rowData = mysql_fetch_assoc( $_tmpResult ))
					{
						$_tmpfn = findNewestFile( applyIdToPath( "files/themes/",
							$rowData[ "objCreator" ])."/style-*.css" );

						if( $_tmpfn != "" )
						{
							$_customTheme = $_tmpfn;
						}

						if( $rowData[ "objForClub" ] > 0 )
						{
							$_tmpfn = findNewestFile( applyIdToPath( "files/clubthemes/",
								$rowData[ "objForClub" ])."/style-*.css" );

							if( $_tmpfn != "" )
							{
								$_customTheme = $_tmpfn;
							}
						}
					}
				}

				if( in_array( $_cmd[ 0 ], $_clubPages ))
				{
					// We're viewing a page that belongs to a certain club.

					$_tmpfn = findNewestFile( applyIdToPath( "files/clubthemes/",
						intval( $_cmd[ 1 ]))."/style-*.css" );

					if( $_tmpfn != "" )
					{
						$_customTheme = $_tmpfn;
					}
				}
			}

			if( $_customTheme != "" )
			{
				echo "\t";
				?><link href="<?= urlf().$_customTheme ?>" rel="alternate stylesheet" title="(custom)" type="text/css" media="screen, projection" /><?
				echo "\n";
			}
		}

		// Set the font.

		if( isset( $_POST[ "font" ]))
		{
			$_font = preg_replace( '/[^\w]/', "", $_POST[ "font" ]);
		}
		elseif( isset( $_COOKIE[ "yGalFont" ]))
		{
			$_font = preg_replace( '/[^\w]/', "", $_COOKIE[ "yGalFont" ]);
		}
		elseif( isset( $_GET[ "yGalFont" ]))
		{
			$_font = preg_replace( '/[^\w]/', "", $_GET[ "yGalFont" ]);
		}
		else
		{
			$_font = $_config[ "defaultFont" ];
		}

		if(!file_exists("themes/font-".$_font.".css"))
			$_font = $_config[ "defaultFont" ];

		if( $_font != "original" ) // Note: don't do $_config[ "defaultFont" ] here.
		{
			echo "\t";
			?><link href="<?= urlf()."themes/font-".$_font.".css" ?>" <?
				?>rel="stylesheet" type="text/css" media="screen, projection" /><?
			echo "\n";
		}
	}

	// Fix for <label> in Opera.

	?>
	<style type="text/css">
	/* <![CDATA[ */

		<?

		if( $_isOpera )
			echo "label { margin-left: -4px; }\n";
		else
			echo "label { vertical-align: middle; }\n";

		?>

	/* ]]> */
	</style>
	<?

	// Include our Javascript.

	?>

	<script type="text/javascript">
	//<![CDATA[

		var isIE = <?= $_isIE ? "true" : "false" ?>;
		var isReadOnly = <?= $_config[ "readOnly" ] ? "true" : "false" ?>;
		var isLoggedIn = <?= $_auth[ "useid" ] != 0 ? "true" : "false" ?>;
		var isFuzzyNumbers = <?= $_auth["useid"] && $_auth["useFuzzyNumbers"] ? "true" : "false" ?>;
		var isExtras = <?= isExtras() ? "true" : "false" ?>;
		var isCookiesDisabled = <?= isset( $_GET[ "yGalSession" ]) ? "true" : "false" ?>;
		var yGalSession = <?= isset( $_GET[ "yGalSession" ]) ? "'".$_GET[ "yGalSession" ]."'" : "''" ?>;
		var baseURL = '<?= url() ?>';
		var baseURLF = '<?= urlf() ?>';
		var thisURL = '<?= url( "." ) ?>';
		var allowIEPNG = <?= $_config[ "allowIEPNG" ] ? "true" : "false" ?>;
		var domain = '.<?= $_config[ "galRoot" ] ?>';
		var comment_count = <?= $_auth[ "useid" ] != 0 ? $_auth[ "useUpdCom" ] : 0 ?>;
		var journal_count = <?= $_auth[ "useid" ] != 0 ? $_auth[ "useUpdJou" ] : 0 ?>;

	//]]>
	</script>

	<script src="<?= urlf() ?>scripts/strings/<?= $_lang ?>.js?1" type="text/javascript"></script>
	<script src="<?= urlf() ?>scripts/prototype.js?1" type="text/javascript"></script>
	<script src="<?= urlf() ?>scripts/script.js?2" type="text/javascript"></script>
	<script src="<?= urlf() ?>scripts/styleswitcher.js?1" type="text/javascript"></script>
	<script src="<?= urlf() ?>scripts/instantreply.js?1" type="text/javascript"></script>
	<script src="<?= urlf() ?>scripts/visuals.js?2" type="text/javascript"></script>
	<script src="<?= urlf() ?>scripts/assets/ygAsset_comments.js?7" type="text/javascript"></script>
	<script src="<?= urlf() ?>scripts/assets/ygAsset_updates.js?9" type="text/javascript"></script>
	<script src="<?= urlf() ?>scripts/assets/ygAssetRequest.js?10" type="text/javascript"></script>
	<script src="<?= urlf() ?>scripts/assets/yg.js?7" type="text/javascript"></script>
	<script src="<?= urlf() ?>scripts/assets/ygAssetRequest.js?10" type="text/javascript"></script>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-8845300-2']);
	  _gaq.push(['_setDomainName', '.y-gallery.net']);
	  _gaq.push(['_trackPageview']);

		(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
					  })();
	  </script>

</head>
<body onload="activateYG(); refreshVisuals();">
<div class="main-document">
<?

// -----------------------------------------------------------------------------
// Convince IE users to change their browser.

if( !$_config[ "disableIEWarning" ] && $_isIE && $_cmd[ 0 ] != "noie" )
{
	?>
	<div class="header a_center" onclick="this.style.display='none'"
		style="position: absolute; top: 0; left: 0; color: #FFF; background: #000; z-index : 999">
		<?= _NO_IE ?>
		&nbsp; &nbsp;
		<a href="<?= url( "noie" ) ?>"
			onclick="popup('<?= url( "noie", array( "popup" => "yes" ))?>'); return false">
			<?= _READ_MORE ?> &gt;</a></div>
	<?
}

// -----------------------------------------------------------------------------
// If the "popup" GET variable is set, we assume that it has to appear as a
// popup window; popup windows have reductive layout with only the page content
// shown.

if( isset( $_GET[ "popup" ]))
{
	?>
	<div class="outer">
		<?

		// Include the page content depending on the first pseudo subfolder
		// in the URL, e.g. a request to /edit/ would include p_edit.php here.

		iefixStart();

		$_page = INCLUDES."p_".preg_replace( '/[^a-z0-9]/', "",
			$_cmd[ 0 ]).".php";

		include( file_exists( $_page ) ? $_page : INCLUDES."p_notfound.php" );

		iefixEnd();

		?>
	</div>
	<?=$_addToFooter?>
	</div><!-- main-document -->
	<script type="text/javascript" src="http://www.google-analytics.com/ga.js"></script>
	<script type="text/javascript">//<![CDATA[
		try {
			var pageTracker = _gat._getTracker("UA-8845300-1");
			pageTracker._trackPageview();
		} catch(err) {}
	//]]></script>
	</body>
	</html>
	<?

	return;
}

?>
<div class="container2" id="idColorPicker" style="display: none">
	<a id="idColorPicker2" href="/">.</a>
</div>
<script src="<?= urlf() ?>scripts/adcolors.js?1" type="text/javascript"></script>
<?

// -----------------------------------------------------------------------------

if( isset( $_newUI ))
{
	?>
	<div class="ui2-layout-bg ui2-top"></div>
	<div class="ui2-layout-bg ui2-top-sidebar"></div>
	<?
	
	if( $_newUI == 1 )
	{
		?>
		<ul class="ui2-top-menu">
			
		<?

		if( isLoggedIn() )
		{
			?>
			<li><a href="<?= url( "/" ) ?>">
				<?= getIMG( url()."images/ui2/news.png" ) ?>
				News</a></li>
			<li><a href="<?= url( "club/11592/" ) ?>">
				<?= getIMG( url()."images/ui2/chat.png" ) ?>
				Site Changelog</a></li>
			<li><a href="<?= url( "browse" ) ?>">
				<?= getIMG( url()."images/ui2/browse.png" ) ?>
				<?= _BROWSE ?></a></li>
			<li><a href="<?= url( "search" ) ?>">
				<?= getIMG( url()."images/ui2/search.png" ) ?>
				<?= _SEARCH ?></a></li>

			<?php
			// facsimilnym: 2011-09-13
			// Removing the chat link per CuriouslyHigh
			/*<li><a href="<?= url( "chat" ) ?>">
				<?= getIMG( url()."images/ui2/chat.png" ) ?>
				<?= _CHAT ?></a></li>*/
			?>
			<li><a href="<?= url( "helpdesk/faq" ) ?>">
				<?= getIMG( url()."images/ui2/help.png" ) ?>
				FAQ.</a></li>
			<li><a href="<?= url( "submit" ) ?>">
				<?= getIMG( url()."images/ui2/submit.png" ) ?>
				<?= _SUBMIT ?></a></li>
			<li><a href="<?= url( "club/1" ) ?>">
				<?= getIMG( url()."images/ui2/queue.png" ) ?>
				Help Desk</a></li>
			<li><a href="http://www.y-gallery.net/pm/for/SpiralSea">
				<?= getIMG( url()."images/ui2/donate.png" ) ?>
				Ask How to Donate</a></li>
			<li><a href="http://www.y-gallery.net/helpdesk/faq/general/advert">
				<?= getIMG( url()."images/emoticons/star4.png" ) ?>
				Advertise</a></li>
			<?
				if($_auth[ "usePMBanner" ] > 0){
				if($_auth["useUpdPms"] > 0){ ?>
					<li class="pmBanner">
						<a href="<?= url( "pm" )?>" style="color: inherit">You have <b></b><? echo $_auth["useUpdPms"]; ?></b> new Private Messages</a>
					</li>
				<? }}
		}
		else
		{
			?>
			<li><a href="<?= url( "/" ) ?>">
				<?= getIMG( url()."images/ui2/news.png" ) ?>
				News</a></li>
			<li><a href="<?= url( "club/11592/" ) ?>">
				<?= getIMG( url()."images/ui2/chat.png" ) ?>
				Site Changelog</a></li>
			<li><a href="<?= url( "browse" ) ?>">
				<?= getIMG( url()."images/ui2/browse.png" ) ?>
				<?= _BROWSE ?></a></li>
			<li><a href="<?= url( "helpdesk/faq" ) ?>">
				<?= getIMG( url()."images/ui2/help.png" ) ?>
				Faq</a></li>
			<li><a href="<?= url( "helpdesk/faq/staff/info" ) ?>">
				<?= getIMG( url()."images/ui2/queue.png" ) ?>
				<?= _STAFF ?></a></li>
			<?
		}
	    
		?>
		</ul>
		<?

	}

	?>
	<div class="ui2-layout-outer">
	<div class="ui2-layout">
	<div class="ui2-page">
	<?
}

?>
		
<a href="/">
	<div class="layout_top">
		<?

		if( !isset( $_newUI ))
		{
			?>
			<div class="ads">
			<?
			include( INCLUDES."ads.php" );
			?>
			</div>
			<?
		}

		?>
	</div>
</a>
<div class="layout">
	<?

	if( !isset( $_newUI ))
	{
		?>
		<div class="layout-sidebar">
			<?

			include( INCLUDES."layout!sidebar.php" );
			
			?>
		</div>
		<?
	}

	?>
	<div class="layout-page">
		<div class="outer">
			<?

			// Include the page content depending on the first pseudo subfolder
			// in the URL, e.g. a request to /edit/ would include p_edit.php here.

			iefixStart();

			$_page = INCLUDES."p_".preg_replace( '/[^a-z0-9]/', "",
				$_cmd[ 0 ]).".php";

			include_once( "serverload.php" );
			if(!isLoggedIn() && $serverload > 14 &&
			   $_cmd[0] != 'user' && $_cmd[0] != 'join' && $_cmd[0] != 'lostpassword' && $_cmd[0] != 'tos' &&
			   $_cmd[0] != 'activate' && $_cmd[0] != 'password' && $_cmd[0] != 'passwordchanged' &&
			   $_cmd[0] != 'emailchange' && $_cmd[0] != 'emailresend' && $_cmd[0] != 'dailyjob' &&
			   $_cmd[0] != 'noie')
			{
				$_page = INCLUDES."p_overload.php";
			}
			elseif( !file_exists( $_page ))
			{
				$_page = INCLUDES."p_notfound.php";
			}

			include( $_page );

			iefixEnd();

			?>
		</div>
	</div>
</div>

<div class="padded a_center smalltext" style="clear : both;">
			 <a href="<?= url( "club/1" )?>"><?=_HELP?></a> 
	&middot; <b><a href="javascript:popup('<?= url( "tos", array( "popup" => "yes" )) ?>','tos',900,700)"><?= _TOS ?></a></b> 
	&middot; <a href="<?= url( "help" )?>">FAQ</a><br />
	<?php

	if( 0 )
	{
		// Store profiler information.

		$time_start = $_stats[ "startTime" ];
		$time_end = gettimeofday();
		$secdiff = $time_end[ "sec" ] - $time_start[ "sec" ];
		$usecdiff = $time_end[ "usec" ] - $time_start[ "usec" ];
		$generationTime = round(( $secdiff * 1000000 + $usecdiff ) / 1000000, 3 );

		$page = $_cmd[ 0 ];

		if( $_isSearchBot )
		{
			$page .= " (bot)";
		}

		sql_where( array( "prfPage" => $page ));

		if( sql_count( "profiler" ) > 0 )
		{
			sql_query( "UPDATE `profiler` SET `prfCount` = `prfCount` + 1, ".
				"`prfTime` = `prfTime` + '".$generationTime."' ".
				"WHERE `prfPage` = '".addslashes( $page )."'" );
		}
		else
		{
			sql_values( array(
				"prfPage" => $page,
				"prfCount" => 1,
				"prfTime" => $generationTime ));

			sql_insert( "profiler" );
		}
	}

	// Show "Page generated in N seconds" if the user is at least
	// a moderator.

	if( atLeastSModerator() || $_auth[ "useid" ] == 34814/*Mayhem*/ )
	{
		include_once( "serverload.php" );

		$time_start = $_stats[ "startTime" ];
		$time_end = gettimeofday();
		$secdiff = $time_end[ "sec" ] - $time_start[ "sec" ];
		$usecdiff = $time_end[ "usec" ] - $time_start[ "usec" ];

		$generationTime = round(( $secdiff * 1000000 + $usecdiff ) / 1000000, 3 );

		$mysqlStat = mysql_stat();

		$queriesPerSecond = round( preg_replace( '/.*'.preg_quote(
			"Queries per second avg: " ).'([0-9\.]+).*/', "\\1", $mysqlStat ), 2 );

		//if( isset( $_stats[ "startQueries" ]))
		//{
		//	$queryCount = preg_replace( '/.*'.preg_quote( "Questions: " ).
		//		'([0-9]+).*/', "\\1", $mysqlStat ) - $_stats[ "startQueries" ];
		//}
		//else
		//{
		//	$queryCount = 0;
		//}

		$mysqlUptime = round( preg_replace( '/.*'.preg_quote(
			"Uptime: " ).'([0-9\.]+).*/', "\\1", $mysqlStat ), 2 );

		$upDays = floor( $mysqlUptime / 60 / 60 / 24 );
		$upHours = floor( $mysqlUptime / 60 / 60 ) % 24;
		$upMinutes = $mysqlUptime / 60 % 60;

		echo " <br /> ".sprintf( _PAGE_GENERATED, $generationTime ).
			", MySQL: ".
			//sprintf( "%1.3f", $_SQLTime )." sec, ".$_SQLCount." queries, ".
			$queriesPerSecond." queries/sec, ".
			"uptime ".$upDays."d ".$upHours."h ".
			$upMinutes."m. &middot; Server load: $loadavg";
	}


	include_once(INCLUDES . 'layout!sidebar-func.php');
	putBottomBanner();

	?>

</div>

<?

if( isset( $_newUI ))
{
	?>
	</div><!-- ui2-page -->
	<div class="ui2-layout-bg ui2-sidebar">
		<?
	
		include( INCLUDES."ads.php" );
		include( INCLUDES."layout!sidebar2.php" );
	
		?>
	</div>
	</div><!-- ui2-layout -->
	</div><!-- ui2-layout-outer -->
	<?
}

?>

<?= $_addToFooter ?>

<script type="text/javascript">
//<![CDATA[
	document.title = '<?= addslashes( $_documentTitle." :: ".$_config[ "galName" ]." :: ".$_config[ "galSubname" ]) ?>';
//]]>
</script>

</div><!-- main-document -->
<script type="text/javascript" src="http://www.google-analytics.com/ga.js"></script>
<script type="text/javascript">//<![CDATA[
	try {
		var pageTracker = _gat._getTracker("UA-8845300-1");
		pageTracker._trackPageview();
	} catch(err) {}
//]]></script>
</body>
</html>
