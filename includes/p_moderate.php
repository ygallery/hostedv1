<?

if( !atLeastModerator() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

$result = sql_query( "SELECT * FROM `users`, `useExtData` ".
	"WHERE `useid` = `useEid` AND `useUsername` = '".addslashes( $_cmd[ 1 ])."' LIMIT 1" );

if( !$useData = mysql_fetch_assoc( $result ))
{
	include( INCLUDES."p_notfound.php" );
	return;
}

$useUsername = strtolower( $useData[ "useUsername" ]);
$useid = intval( $useData[ "useid" ]);

$_documentTitle = $useData[ "useUsername" ].": "._MODERATION;

?>
<div class="header">
	<div class="f_right mar_left a_center normaltext">
		<?= getUserAvatar( "",$useData[ "useid" ], true )?>
	</div>
	<div class="f_left header_title">
		<?= $useData[ "useUsername" ] ?>
		<div class="subheader"><?= _MODERATION ?></div>
	</div>
	<?

	$active = 9;
	include( INCLUDES."mod_usermenu.php" );

	?>
</div>

<div class="container">

	<div class="mar_bottom container2 notsowide">
		<?=iefixStart()?>
		<div class="a_center largetext">
			<?=_INFORMATION ?>
		</div>
		<div class="mar_bottom">
			<b><?= _REAL_NAME ?></b>:
			<?= htmlspecialchars($useData[ "useRealName" ]) ?>
		</div>
		<div class="mar_bottom">
			<b><?= _USER_ID ?></b>:
			<?= htmlspecialchars($useData[ "useid" ]) ?>
		</div>
		<div class="mar_bottom">
			<b><?= _EMAIL_ADDRESS ?></b>:
			<a href="mailto:<?= htmlspecialchars($useData[ "useEmail" ]) ?>"><?= htmlspecialchars($useData[ "useEmail" ]) ?></a>
		</div>
		<div class="mar_bottom">
			<b>Last IP</b>:
			<a href="http://www.ip-adress.com/ip_tracer/<?= getDotDecIp( $useData[ "useLastIp" ]); ?>"><?= getDotDecIp( $useData[ "useLastIp" ]); ?></a>
		</div>
		<div class="mar_bottom">
			<b>Last action</b>:
			<?= gmdate( $_auth[ "useDateFormat" ], applyTimezone( strtotime( $useData[ "useLastAction" ]))); ?>
		</div>
		<div class="mar_bottom">
			<b><?= _BIRTHDAY ?></b>:
			<?=$useData["useBirthday"] ?>
		</div>
			<div class="mar_bottom">
			<b>Registered</b>:
			<?= gmdate( $_auth[ "useDateFormat" ], applyTimezone( strtotime( $useData[ "useSignupDate" ]))); ?>
		</div>
		<?=iefixEnd()?>
	</div>
	<?

	if( atLeastModerator() )
	{
		if( isset( $_POST[ "submitSuspend" ]))
		{
			$suspendUntil = strtotime( $_POST[ "suspendUntil" ]);
			$suspendReason = $_POST[ "suspendReason" ];

			if( $suspendUntil >= 0 )
			{
				terminateSession( $useData[ "useid" ]);

				sql_query( "UPDATE `useExtData` SET `useSuspendedUntil` = '".
					gmdate( "Y-m-d H:i:s", $suspendUntil )."', ".
					"`useSuspendedReason` = '".
					addslashes( $suspendReason ) ."' ".
					"WHERE `useEid` = '".$useData[ "useid" ]."' LIMIT 1" );

				sql_query( "UPDATE `users` SET `useIsSuspended` = '1' ".
					"WHERE `useid` = '".$useData[ "useid" ]."' LIMIT 1" );
			}

			$susLeft = $suspendUntil - time();

			$susLeft = round(( $susLeft < 0 ? 0 : $susLeft ) / ( 60 * 60 * 24 ));

			addModeratorLog( $useData[ "useid" ],
				"[b]Suspended until[/b]: ".date( "Y-m-d H:i:s", $suspendUntil )." (".$susLeft." days)" );

			redirect( url( "." ));
		}

		// only smod can ban
		if( atLeastSModerator() ) {
			if( isset( $_POST[ "submitBan" ]))
			{
				$suspendReason = $_POST[ "suspendReason" ];
				terminateSession( $useData[ "useid" ]);

				sql_query( "UPDATE `users`,`useExtData` SET `useIsBanned` = '1', ".
					"`useSuspendedReason` = '".
					addslashes( $suspendReason ) ."' ".
					"WHERE `useid` = `useEid` AND `useid` = '".$useData[ "useid" ]."'" );

				addModeratorLog( $useData[ "useid" ], "[b]BANNED![/b]" );

				redirect( url( "." ));
			}

			if( isset( $_POST[ "submitUnban" ]))
			{
				terminateSession( $useData[ "useid" ]);

				sql_query( "UPDATE `users` SET `useIsBanned` = '0' ".
					"WHERE `useid` = '".$useData[ "useid" ]."' LIMIT 1" );

				addModeratorLog( $useData[ "useid" ], "[b]Unbanned![/b]" );

				redirect(url("."));
			}
		}

		if( isset( $_POST[ "submitLog" ]))
		{
			addModeratorLog( $useData[ "useid" ], $_POST[ "modMessage" ]);

			redirect( url( "." )."#modlog" );
		}

		if( isset( $_POST[ "submitUserData" ]))
		{
			$changes = array();
			$changesExt = array();

			if( $useData[ "useCustomTitle" ] != $_POST[ "useCustomTitle" ])
			{
				$changesExt[ "useCustomTitle" ] = $_POST[ "useCustomTitle" ];

				addModeratorLog( $useData[ "useid" ], "[b]Changed user custom title[/b]" );
			}

			if( $useData[ "useProfile" ] != $_POST[ "useProfile" ])
			{
				$changesExt[ "useProfile" ] = $_POST[ "useProfile" ];

				addModeratorLog( $useData[ "useid" ], "[b]Changed user profile[/b]" );
			}

			if( $useData[ "useSignature" ] != $_POST[ "useSignature" ])
			{
				$changesExt[ "useSignature" ] = $_POST[ "useSignature" ];

				addModeratorLog( $useData[ "useid" ], "[b]Changed user signature[/b]" );
			}

			if( isset( $_POST[ "deleteAvatar" ]))
			{
				$filename = applyIdToPath( "files/avatars/", $useData[ "useid" ])."-".
					$useData[ "useAvatarDate" ].".".$useData[ "useAvatarExt" ];

				if( file_exists( $filename ))
				{
					unlink( $filename );
				}

				$changes[ "useAvatarWidth" ] = 0;
				$changes[ "useAvatarHeight" ] = 0;
				$changes[ "useAvatarExt" ] = "---";
				$changes[ "useAvatarDate" ] = 0;

				addModeratorLog( $useData[ "useid" ], "[b]Deleted user avatar[/b]" );
			}

			if( isset( $_POST[ "deleteID" ]))
			{
				$filename = findNewestFile( applyIdToPath( "files/ids/", $useData[ "useid" ])."-*", "" );

				if( $filename != "" )
				{
					unlink( $filename );
				}

				addModeratorLog( $useData[ "useid" ], "[b]Deleted user ID[/b]" );
			}

			if( isset( $_POST[ "deleteTheme" ]))
			{
				include_once( INCLUDES."customthemes.php" );

				removeCustomTheme( $useData[ "useid" ], false );

				addModeratorLog( $useData[ "useid" ], "[b]Removed custom theme[/b]" );
			}
			
			//Only admin can change user class
				if( isAdmin() ) 
				{
					$changesExt[ "useIsRetired" ] = isset( $_POST[ "useIsRetired" ]) ? 1 : 0;
					$changesExt[ "useIsHelpdesk" ] = isset( $_POST[ "useIsHelpdesk" ]) ? 1 : 0;
					$changesExt[ "useIsModerator" ] = isset( $_POST[ "useIsModerator" ]) ? 1 : 0;
					$changesExt[ "useIsSModerator" ] = isset( $_POST[ "useIsSModerator" ]) ? 1 : 0;
					$changesExt[ "useIsDeveloper" ] = isset( $_POST[ "useIsDeveloper" ]) ? 1 : 0;
				}
					
			

			if( count( $changes ) > 0 )
			{
				sql_query( "UPDATE `users`".dbSet( $changes ).dbWhere( array(
					"useid" => $useData[ "useid" ])));
			}

			if( count( $changesExt ) > 0 )
			{
				sql_query( "UPDATE `useExtData`".dbSet( $changesExt ).dbWhere( array(
					"useEid" => $useData[ "useid" ])));
			}

			redirect( url( "." ));
		}

		$suspendedUntil = strtotime( $useData[ "useSuspendedUntil" ]);

		?>
		<div class="container2 mar_bottom notsowide">
			<?= iefixStart() ?>
			<form action="<?= url( "." )?>" method="post">
				<a name="modlog"></a>
				<div class="sep a_center largetext">
					Past Moderation Events
				</div>
				<?

				$modResult = sql_query( "SELECT * FROM `modlogs`".dbWhere( array(
					"modUser" => $useData[ "useid" ]))."ORDER BY `modSubmitDate`" );

				while( $modData = mysql_fetch_assoc( $modResult ))
				{
					?>
					<div class="sep mar_left mar_right">
						<?= substr( $modData[ "modSubmitDate" ], 0, 10 ) ?>
						<?= getUserLink( $modData[ "modModerator" ]) ?>:
						<?= formatText( $modData[ "modMessage" ], true ) ?>
					</div>
					<?
				}

				mysql_free_result( $modResult );

				?>
				<div class="sep a_center">
					<textarea name="modMessage" cols="60" rows="3"></textarea>
				</div>
				<div class="sep a_center">
					<input class="submit" type="submit" name="submitLog" value="Add Message" />
				</div>
			</form>
			<?= iefixEnd() ?>
		</div>

		<div class="container2 mar_bottom notsowide">
			<?= iefixStart() ?>
			<div class="a_center largetext">
				Suspend / Ban
			</div>
			<form action="<?=url(".")?>" method="post">
			<div class="sep mar_left mar_right">
				<div class="caption">
					<?= sprintf( _MOD_SUSPENDED_UNTIL, getUserLink( $useData[ "useid" ])) ?>:
				</div>
				<div>
					<input type="text" name="suspendUntil" size="40"
						value="<?= gmdate( "j F Y H:i", applyTimezone( $suspendedUntil )) ?>" />
				</div>

				<div class="mar_left smalltext">
					<?

					$susLeft = $suspendedUntil - time();

					$susLeft = round(( $susLeft < 0 ? 0 : $susLeft ) / ( 60 * 60 * 24 ));

					?>
					(<?= sprintf( _MOD_SUSPENDED_LEFT, $susLeft ) ?>)
				</div>

				<div class="mar_top caption">
					<?= _FOR_REASON ?>:
				</div>
				<div>
					<input type="text" name="suspendReason" size="40"
						value="<?= $useData[ "useSuspendedReason" ] ?>" />
					<input class="submit" type="submit" name="submitSuspend" value="<?= _CHANGE ?>" />
				</div>

				<div class="sep">
					<?= _ABUSE_STATUS ?>:
					<b><?= $useData[ "useIsBanned" ] ? strtoupper( _MOD_BANNED ) : _OK ?></b>
					&nbsp;
<? if( atLeastSModerator() ) { ?>
					<input class="submit" type="submit"
						name="<?= $useData[ "useIsBanned" ] ? "submitUnban" : "submitBan" ?>"
						value="<?= $useData[ "useIsBanned" ] ? _MOD_UNBAN : _MOD_BAN ?>" />
<? } ?>
				</div>
			</div>
			</form>
			<?= iefixEnd() ?>
		</div>

		<div class="container2 mar_bottom notsowide">
			<?= iefixStart() ?>
			<form action="<?= url( "." )?>" method="post">
				<a name="modlog"></a>
				<div class="sep a_center largetext mar_bottom">
					Edit User Data
				</div>

				<table cellspacing="0" cellpadding="4" border="0" width="100%">
					<tr>
						<td align="right" class="nowrap"><?= _SET_CUSTOM_TITLE ?>:</td>
						<td>
							<input type="text" name="useCustomTitle" size="50"
								value="<?= htmlspecialchars( $useData[ "useCustomTitle" ]) ?>" />
						</td>
					</tr>
					<tr>
						<td valign="top" align="right" class="nowrap">
							<div><?= _PROFILE ?>:</div>
							<?

							$script = "make_invisible('profile_current'); ".
								"make_invisible('profile_edit'); ".
								"make_visible('profile_change'); ".
								"return false;";

							$profileEdit = '<div id="profile_edit">'.
								'(<a href="" onclick="'.$script.'">'._EDIT.'</a>)</div>';

							if( trim( $useData[ "useProfile" ]) != "" )
							{
								echo $profileEdit;
							}

							?>
						</td>
						<td>
							<div id="profile_current">
								<?

								if( trim( $useData[ "useProfile" ]) != "" )
								{
									?>
									<div class="container2"><?= formatText( $useData[ "useProfile" ]) ?></div>
									<?
								}
								else
								{
									echo $profileEdit;
								}

								?>
							</div>
							<div id="profile_change" style="display: none">
								<div>
									<?

									iefixStart();

									$commentName = "useProfile";
									$commentDefault = $useData[ "useProfile" ];
									$commentNoOptions = true;
									$commentRows = 12;

									include( INCLUDES."mod_comment.php" );

									iefixEnd();

									?>
									<div class="clear">&nbsp;</div>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top" align="right" class="nowrap">
							<div><?= _SET_SIGNATURE ?>:</div>
							<?

							$script = "make_invisible('signature_current'); ".
								"make_invisible('signature_edit'); ".
								"make_visible('signature_change'); ".
								"return false;";

							$signatureEdit = '<div id="signature_edit">'.
								'(<a href="" onclick="'.$script.'">'._EDIT.'</a>)</div>';

							if( trim( $useData[ "useSignature" ]) != "" )
							{
								echo $signatureEdit;
							}

							?>
						</td>
						<td>
							<div id="signature_current">
								<?

								if( trim( $useData[ "useSignature" ]) != "" )
								{
									?>
									<div class="container2"><?= formatText( $useData[ "useSignature" ]) ?></div>
									<?
								}
								else
								{
									echo $signatureEdit;
								}

								?>
							</div>
							<div id="signature_change" style="display: none">
								<div>
									<?

									iefixStart();

									$commentName = "useSignature";
									$commentDefault = $useData[ "useSignature" ];
									$commentNoOptions = true;
									$commentRows = 12;

									include( INCLUDES."mod_comment.php" );

									iefixEnd();

									?>
									<div class="clear">&nbsp;</div>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<input type="checkbox" class="checkbox" name="deleteAvatar" id="idDeleteAvatar" />
							<label for="idDeleteAvatar">Delete Avatar</label>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<input type="checkbox" class="checkbox" name="deleteID" id="idDeleteID" />
							<label for="idDeleteID">Delete ID</label>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<input type="checkbox" class="checkbox" name="deleteTheme" id="idDeleteTheme" />
							<label for="idDeleteTheme">Remove Custom Theme</label>
						</td>
					</tr>
					<?php 
					if( isAdmin() )
						{						
						?>
							<tr>
								<td></td>
								<td>
									<input type="checkbox" class="checkbox" name="useIsRetired" id="idIsRetired" <?php  if($useData[ "useIsRetired" ] == '1'){ echo "checked='checked'"; } ?> />
									<label for="idIsRetired" title="If checked user will be marked as Retired Staff">Is Retired</label>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<input type="checkbox" class="checkbox" name="useIsHelpdesk" id="idIsHelpdesk" <?php  if($useData[ "useIsHelpdesk" ] == '1'){ echo "checked='checked'"; } ?> />
									<label for="idIsHelpdesk" title="If checked user will have Helpdesk Staff rights">Is HDStaff</label>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<input type="checkbox" class="checkbox" name="useIsModerator" id="idisModerator" <?php  if($useData[ "useIsModerator" ] == '1'){ echo "checked='checked'"; } ?> />
									<label for="idisModerator" title="If checked user will have moderator rights">Is Moderator</label>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<input type="checkbox" class="checkbox" name="useIsSModerator" id="idisSModerator" <?php  if($useData[ "useIsSModerator" ] == '1'){ echo "checked='checked'"; } ?> />
									<label for="idisSModerator" title="If checked user will have supermoderator rights">Is Supermoderator</label>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<input type="checkbox" class="checkbox" name="useIsDeveloper" id="idisDeveloper" <?php  if($useData[ "useIsDeveloper" ] == '1'){ echo "checked='checked'"; } ?> />
									<label for="idisDeveloper" title="If checked user will have developer rights which include some mod and smod rights">Is Developer</label>
								</td>
							</tr>
							
								
							<?php 
						}		
					?>
				</table>

				<div class="sep a_center">
					<input class="submit" type="submit" name="submitUserData" value="Apply" />
				</div>
			</form>
			<?= iefixEnd() ?>
		</div>
		<?
	}

	?>
</div>
