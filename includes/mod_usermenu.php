<?

include( INCLUDES."mod_watch.php" );

?>
<div style="clear: both" class="normaltext">
<?

displayTabStart();
displayTab( $active == 1, _HOME, url( "user/".$useUsername ));
displayTab( $active == 2, _SUBMISSIONS, url( "gallery/".$useUsername ), "submission.png" );

$showJournalsTab = true;
$showPollsTab = true;
$showClubsTab = true;
$showStatsTab = isLoggedIn();

if( $_auth[ "useid" ] != $useData[ "useid" ])
{
	$result = sql_query( "SELECT COUNT(*) FROM `journals` ".
		"WHERE `jouCreator` = '".intval( $useData[ "useid" ])."' AND `jouCreatorType` = 'use'" );

	$showJournalsTab = mysql_result( $result, 0 ) > 0;

	$result = sql_query( "SELECT COUNT(*) FROM `polls` ".
		"WHERE `polCreator` = '".intval( $useData[ "useid" ])."'" );

	$showPollsTab = mysql_result( $result, 0 ) > 0;

	$result = sql_query( "SELECT COUNT(*) FROM `useClubs` ".
		"WHERE `useCmember` = '".intval( $useData[ "useid" ])."' AND `useCpending` = '0'" );

	$showClubsTab = mysql_result( $result, 0 ) > 0;

	if( isLoggedIn() )
	{
		$result = sql_query( "SELECT COUNT(*) FROM `useExtData` ".
			"WHERE `useEid` = '".intval( $useData[ "useid" ])."' AND `useStatsPublic` = '1'" );

		$showStatsTab = mysql_result( $result, 0 ) > 0;
	}
}

if( $_auth[ "useStatsHide" ])
{
	$showStatsTab = false;
}

if( $showJournalsTab )
{
	displayTab( $active == 7, _JOURNAL, url( "journal/".$useUsername ), "journal.png" );
}

if( $showPollsTab )
{
	displayTab( $active == 8, _POLL, url( "poll/".$useUsername ), "poll.png" );
}

displayTab( $active == 3, _FRIENDS, url( "friends/".$useUsername ), "watch.png" );

if( $showClubsTab )
{
	displayTab( $active == 5, _CLUBS, url( "clubs/".$useUsername ), "club2.png" );
}

if( $showStatsTab )
{
	displayTab( $active == 6, _STATS, url( "stats/".$useUsername ));
}

if( atLeastModerator() )
{
	displayTab( $active == 9, _MODERATION, url( "moderate/".$useUsername ));
}

displayTabEnd();

?>
<div class="clear"><br /></div>

</div>
