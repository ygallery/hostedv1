<? // Module: gallery.

if(!isset($_GET["order"]))
	if(isset($_COOKIE["yGalOrder"]))
	{
		if($_cmd[0] != "")
			$_GET["order"] = $_COOKIE["yGalOrder"];
		else
			$_GET["order"] = 0;
	}
	else
		$_GET["order"] = 0;

switch(intval($_GET["order"])) {
	case 1: $order = isset($overrideOldest) ? $overrideOldest : "`objSubmitDate`"; break;
	case 2: $order = "`objFavs` DESC"; break;
	case 3: $order = "`objViewed` DESC"; break;
	default: $order = isset($overrideNewest) ? $overrideNewest : "`objSubmitDate` DESC";
}

if(!isset($_GET["limit"]))
	if(isset($_COOKIE["yGalLimit"]))
	{
		if($_cmd[0] != "")
			$_GET["limit"] = $_COOKIE["yGalLimit"];
		else
			$_GET["limit"] = 0;
	}
	else
		$_GET["limit"] = 0;

switch(intval($_GET["limit"])) {
	case 1: $limit = 8; break;
	case 2: $limit = 24; break;
	case 3: $limit = 48; break;
	default: $limit = 12;
}

if( $_cmd[ 0 ] != "" )
{
	setcookie("yGalOrder", $_GET["order"], strtotime("+1 month"), "/",
		"." . $_config["galRoot"]); // give the user a fresh cookie

	setcookie("yGalLimit", $_GET["limit"], strtotime("+1 month"), "/",
		"." . $_config["galRoot"]); // give the user a fresh cookie
}

$offset = isset($_GET["offset"]) ? intval($_GET["offset"]) : 0;
if($offset < 0) $offset = 0;

if(!isUserMature()) $where = "($where) AND `objMature` = '0'";

/*
// allow moderators to view deleted submissions
if($_auth["useid"] && ($_auth["useid"] == $_config["adminUser"] || $_auth["useIsSModerator"] || $_auth["useIsModerator"])) {
//	$where = str_replace("`objDeleted` = '0'", "`objExtension` <> ''", $where);
	$where = str_replace("`objDeleted` = '0'", "1", $where);
	$where = str_replace("`objPending` = '0'", "1", $where);
}
*/

$selectCount = preg_replace( '/SELECT(.*)FROM/', "SELECT COUNT(*) FROM", $select );

$result = mysql_query( "$selectCount WHERE $where" ) or trigger_error( _ERR_MYSQL );

$totalCount = mysql_result( $result, 0 );

$result = mysql_query( "$select WHERE $where ORDER BY $order LIMIT $offset,".($limit + 1))
	or trigger_error(_ERR_MYSQL);

$gallery = array();

while($objData = mysql_fetch_assoc($result)) {
	$gallery[$objData["objid"]] = $objData;
}

$getVars = array();
if(isset($_GET["offset"])) $getVars["offset"] = $_GET["offset"];
if(isset($_GET["limit"])) $getVars["limit"] = $_GET["limit"];
if(isset($_GET["order"])) $getVars["order"] = $_GET["order"];
if(isset($_GET["keywordList"])) $getVars["keywordList"] = $_GET["keywordList"];
if(isset($_GET["searchText"])) $getVars["searchText"] = $_GET["searchText"];

if(!isset($disableNav)) $disableNav = false;
if(!isset($enableCategories)) $enableCategories = false;

if(!$disableNav) {
	?>
	<?iefixStart()?>
		<?ob_start()?>
		<?
		if( $_cmd[ 0 ] != "" )
			navControls( $offset, $limit, $totalCount );
		?>
		<div class="a_center">
			<?
			if($enableCategories) {
				?>
				<form action="<?=url("search") ?>" method="get">
				<?
				foreach($_GET as $key => $value)
					if($key != "keywordList" && $key != "offset")
						echo '<input name="'.htmlspecialchars($key).'" type="hidden" value="'.htmlspecialchars($value).'" />';
				?>
				<select name="keywordList" onchange="this.form.submit();">
				<option value=""></option>
				<option value=""><?=_SEARCH ?></option>
				<?
				// by default, select the first root keyword group
				$result = mysql_query("SELECT `keyid` FROM `keywords` WHERE `keySubcat` = '0' ORDER BY `keyWord` LIMIT 1") or trigger_error(_ERR_MYSQL);
				if(mysql_num_rows($result))
					$mainSubcat = mysql_result($result, 0);
				else
					$mainSubcat = 0;

				if(isset($_GET["keywordList"])) {
					// if it's already a search request then see what keyword groups are
					// involved in the search and allow selection inside those groups
					$list = preg_split('/\s/', $_GET["keywordList"], 0, PREG_SPLIT_NO_EMPTY);
				}
				else $list = array($mainSubcat);

				$first1 = true;
				$where1 = "`keyid` IN(";

				foreach($list as $keyid) {
					$keyid = intval($keyid);
					if($keyid == 0) continue;
					$result = mysql_query("SELECT `keySubcat` FROM `keywords` WHERE `keyid` = '$keyid' LIMIT 1") or trigger_error(_ERR_MYSQL);
					if(mysql_num_rows($result) > 0) {
						$keySubcat = mysql_result($result, 0);
						$where1 .= ($first1 ? "" : ",")."'$keySubcat'";
						$first1 = false;
					}
				}
				if($mainSubcat > 0)
					$where1 .= ($first1 ? "" : ",")."'$mainSubcat'";

				$where1 .= ")";
				$limit1 = 5;
				$result = mysql_query("SELECT `keyid`,`keyWord` FROM `keywords` WHERE $where1 ORDER BY `keyWord` LIMIT $limit1") or trigger_error(_ERR_MYSQL);

				while($keyData = mysql_fetch_assoc($result)) {
					$keyData["keyWord"] = trim(preg_replace('/^.*\|/', "", $keyData["keyWord"]));
					if(preg_match('/\@$/', $keyData["keyWord"])) continue;
					?>
					<optgroup label="<?=$keyData["keyWord"]?>">
					<?
					$result2 = mysql_query("SELECT * FROM `keywords` WHERE `keySubcat` = '".$keyData["keyid"]."' ORDER BY `keyWord`") or trigger_error(_ERR_MYSQL);
					while($rowData = mysql_fetch_assoc($result2)) {
						$rowData["keyWord"] = trim(preg_replace('/^.*\|/', "", $rowData["keyWord"]));
						if(preg_match('/\@$/', $rowData["keyWord"])) continue;
						?>
						<option <?=(isset($_GET["keywordList"]) && $_GET["keywordList"] == $rowData["keyid"] ? 'selected="selected"' : "")?>
							value="<?=$rowData["keyid"]?>"><?=$rowData["keyWord"]?></option>
						<?
					}
					?>
					</optgroup>
					<?
				}
				?>
				</select>
				</form>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				<?
			}
			?>
			<form action="<?=url(".")?>" method="get">
			<?
			foreach($_GET as $key => $value)
				if($key != "order" && $key != "limit")
					echo '<input name="'.htmlspecialchars($key).'" type="hidden" value="'.htmlspecialchars($value).'" />';
			?>
			<select name="order">
				<option <?=isset($_GET["order"]) && $_GET["order"] == 0 ? 'selected="selected"' : "" ?> value="0"><?=_NEWEST_FIRST ?></option>
				<option <?=isset($_GET["order"]) && $_GET["order"] == 1 ? 'selected="selected"' : "" ?> value="1"><?=_OLDEST_FIRST ?></option>
				<option <?=isset($_GET["order"]) && $_GET["order"] == 2 ? 'selected="selected"' : "" ?> value="2"><?=_MOST_FAVED ?></option>
				<option <?=isset($_GET["order"]) && $_GET["order"] == 3 ? 'selected="selected"' : "" ?> value="3"><?=_MOST_VIEWED ?></option>
			</select>
			<select name="limit">
				<option <?=isset($_GET["limit"]) && $_GET["limit"] == 1 ? 'selected="selected"' : "" ?> value="1"><?=fuzzy_number(8)?></option>
				<option <?=isset($_GET["limit"]) && $_GET["limit"] == 0 ? 'selected="selected"' : "" ?> value="0"><?=fuzzy_number(12)?></option>
				<option <?=isset($_GET["limit"]) && $_GET["limit"] == 2 ? 'selected="selected"' : "" ?> value="2"><?=fuzzy_number(24)?></option>
				<option <?=isset($_GET["limit"]) && $_GET["limit"] == 3 ? 'selected="selected"' : "" ?> value="3"><?=fuzzy_number(48)?></option>
			</select>
			<input class="submit" type="submit" value="<?=_UPDATE ?>" style="vertical-align: middle" />
			</form>
		</div>
		<?$galleryNavs = ob_get_contents(); ob_end_flush()?>
		<div class="hline">&nbsp;</div>
	<?iefixEnd()?>
	<?
}
else $galleryNavs = "";

$cols = 0;
if(!isset($maxcols)) $maxcols = 4;
$imagesToGo = $limit;

foreach($gallery as $objData)
{
	$anchor = url("view/".$objData["objid"]);
	
	if( $objData[ "objThumbDefault" ])
	{
		$src = url()."images/litthumb.png";
	}
	else {
		$src = url().applyIdToPath("files/thumbs/", $objData["objid"])."-".preg_replace('/[^0-9]/', "", $objData["objRevisionDate"]).".jpg";
	}
	$useResult = mysql_query("SELECT `useUsername` FROM `users` WHERE `useid` = '".$objData["objCreator"]."' LIMIT 1") or trigger_error(_ERR_MYSQL);
	$useData = mysql_fetch_assoc($useResult);

	$objTitle = htmlspecialchars($objData["objTitle"]);

	if($_cmd[0] != "gallery" && $_cmd[0] != "galleryclubs")
		$objTitle .= '<br /> '.sprintf(_BY,getUserLink($objData["objCreator"]));

	if($_cmd[0] == "galleryclubs" || ($_cmd[0] == "gallery" && $objData["objForClub"] > 0))
	{
		$result = mysql_query("SELECT `cluName` FROM `clubs` WHERE `cluid` = '".$objData["objForClub"]."'");
		if(mysql_num_rows($result))
		{
			$club = '<a href="'.url("club/".$objData["objForClub"]).'">'.mysql_result($result, 0).'</a>';
			$objTitle .= '<br /> in '.$club; // TODO: i18n "in %s"
		}
	}

	echo '<div class="gallery_col"><div class="a_center padded mar_bottom'.($cols < 3 ? " mar_right" : "").
		'"><a href="'.$anchor.'">'.
		getIMG($src, 'alt="'.strip_tags($objTitle).'" class="thumb'.
			($objData["objMature"] ? " mature" : "").($objData["objPending"] ? " pending" : "").($objData["objDeleted"] ? " deleted" : "").
			'" width="'.$objData["objThumbWidth"].
			'" height="'.$objData["objThumbHeight"].'" title="'.strip_tags($objTitle).'"').
		'</a>'."\n".
		'<div class="">'.$objTitle.'</div></div></div>';

	$cols++;
	if($cols >= $maxcols) {
		$cols = 0;
		echo '<div class="clear">&nbsp;</div>';
	}
	$imagesToGo--;
	if(!$imagesToGo) break;
}

if(!count($gallery))
	echo '<div>'._NO_SUBMISSIONS.'</div>';

echo '<div class="hline">&nbsp;</div>';

if(isset($frontPage)) {
	?>
	<div class="a_right mar_bottom mar_right">
		<a class="disable_wrapping smalltext" href="<?=url("browse")?>">
		<?=_MORE ?>
		<?=getIMG(url()."images/emoticons/nav-next.png") ?>
		</a>
	</div>
	<?
}

echo $galleryNavs;

unset($select);
unset($where);
unset($offset);
unset($limit);
unset($order);
unset($maxcols);
unset($overrideNewest);
unset($overrideOldest);
unset($disableNav);
unset($enableCategories);
unset($frontPage);

?>
<div class="clear">&nbsp;</div>
