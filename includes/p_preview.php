<?

$text = isset( $_GET[ "text" ]) ? substr( $_GET[ "text" ], 0, 64000 ) :
	( isset( $_POST[ "text" ]) ? substr( $_POST[ "text" ], 0, 64000 ) : "" );

$no_emoticons = isset( $_GET[ "noem" ]) ? ( $_GET[ "noem" ] > 0 ) :
	( isset( $_POST[ "noem" ]) ? ( $_POST[ "noem" ] > 0 ) : false );

$no_bbcode = isset( $_GET[ "nobb" ]) ? ( $_GET[ "nobb" ] > 0 ) :
	( isset( $_POST[ "nobb" ]) ? ( $_POST[ "nobb" ] > 0 ) : false );

$text = formatText( $text, $no_emoticons, $no_bbcode );

if( !isset( $_GET[ "text" ]))
{
	echo rawurlencode($text); // Respond to a XMLHttp request.
	echo $_addToFooter;
	exit;
}

$_documentTitle = _PREVIEW;

?>
<div class="outer">
	<div class="header">
		<div class="header_title"><?= _PREVIEW ?></div>
	</div>
	<div class="container">
		<div class="container2">
			<?= $text ?>
		</div>
	</div>
</div>
