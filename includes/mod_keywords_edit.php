<?php

// Output variables:
//
// $_POST["keywordList"] - after submission of the form, contains the list of chosen keywords,
//   e.g. "7 12 17"

include_once( INCLUDES."keywording.php" );

$keywords = fetchKeywords( 0 );

// ---------------------------------------------------------------------------------------
// Returns true if the keyword contains even more sub-keywords

function showKeywordSubcatEdit( $keywords, $listId, $parentId = 0, $parentKeyName = "",
	$parentLastKeyName = "" )
{
	global $requiredTabs, $firstKeywordId;

	$noSubcats = true;

	foreach( $keywords as $keyData )
	{
		if( isset( $keyData[ "subcats" ]))
		{
			$noSubcats = false;
			break;
		}
	}

	if( $parentId == 0 )
	{
		?>
		<div class="clear">&nbsp;</div>
		<div class="sep notsowide mar_left mar_bottom">
			Add more keyword groups to the Root separated by&nbsp;;
			<br />
			<input class="notsowide" name="addKeywordsUnder<?= $parentId ?>" type="text" />
		</div>
		<?
	}

	foreach( $keywords as $keyData )
	{
		$tabid = generateId( "kwcache_tab" );
		
		$keyName = "&bull; ".$parentKeyName.$keyData[ "keyWord" ];
		
		$nonSelectable = preg_match( '/\@$/', $keyData[ "keyWord" ]);

		$keyData[ "keyWord" ] = preg_replace( '/\@$/', "", $keyData[ "keyWord" ]);

		?>
		<div class="f_left">
			<div class="tab normaltext" id="<?= $tabid ?>"
				onclick="open_tab(this,'keywords_<?= $parentId ?>','keywordTab_<?= $keyData[ "keyid" ] ?>')">

				<a href="<?= url( "keywords", array( "delete" => $keyData[ "keyid" ])) ?>">
					<?= getIMG( url()."images/emoticons/keydelete.gif",
						'alt="del" title="'._KEYWORDS_DELETE.'" '.
						'onmouseout="this.style.background=\'none\'" '.
						'onmouseover="this.style.background=\'#fff\'"' ) ?>
				</a>
				<a href="<?= url( "keywords", array( "edit" => $keyData[ "keyid" ])) ?>">
					<?= getIMG( url()."images/emoticons/keyedit.gif",
						'alt="edit" title="'._KEYWORDS_EDIT.'" '.
						'onmouseout="this.style.background=\'none\'" '.
						'onmouseover="this.style.background=\'#fff\'"' ) ?>
				</a>
				<?
				
				echo htmlspecialchars( $keyData[ "keyWord" ]);

				if( $nonSelectable )
				{
					echo "@";
				}
				
				?>
			</div>
		</div>
		<?
	}

	?>
	<div class="clear">&nbsp;</div>
	<?
	
	foreach( $keywords as $keyData )
	{
		$keyData[ "keyWord" ] = preg_replace( '/\@$/', "", $keyData[ "keyWord" ]);

		unset( $requiredTabs[ $keyData[ "keyid" ]]);

		?>
		<div style="display: none" id="keywordTab_<?= $keyData[ "keyid" ] ?>">
			<? iefixStart() ?>
			<div class="hline">&nbsp;</div>
			<div class="hline">&nbsp;</div>
			<div class="sep notsowide_fix mar_left mar_bottom">
				Add more keywords to the group
				<b><?= $parentKeyName.htmlspecialchars( $keyData[ "keyWord" ]) ?></b>
				separated by&nbsp;;
				<br />
				<input class="notsowide_fix" name="addKeywordsUnder<?= $keyData[ "keyid" ] ?>" type="text" />
			</div>
			<div class="clear">&nbsp;</div>
			<? iefixEnd() ?>
			<?

			if( isset( $keyData[ "subcats" ]))
			{
				iefixStart();

				showKeywordSubcatEdit( $keyData["subcats"], $listId, $keyData[ "keyid" ],
					$parentKeyName.htmlspecialchars( $keyData[ "keyWord" ])." &ndash; ",
					$parentId != 0 ? htmlspecialchars( $keyData[ "keyWord" ]) : "" );
				
				iefixEnd();
			}
			?>
			<div class="clear">&nbsp;</div>
		</div>
		<?
	}
}

showKeywordSubcatEdit( $keywords, "" );

?>
<div class="hline">&nbsp;</div>
<div class="hline">&nbsp;</div>
