<?

// This script controls the "Site" tab of the settings.

if( !isLoggedIn() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

?>
<div class="header">
	<div class="header_title">
		<?= _SETTINGS ?>
		<div class="subheader"><?= _SET_SITE_SETTINGS ?></div>
	</div>
	<?

	$_documentTitle = _SET_SITE_SETTINGS;

	$active = 1;

	include( INCLUDES."mod_setmenu.php" );

	?>
</div>
<div class="container">
	<form action="<?= url( "." ) ?>" method="post">
	<?

	$dateFormatList = array(
		"0" => "Y-m-d \@ g:i A",
		"1" => "Y-m-d \@ G:i",
		"2" => "M j, Y, g:ia",
		"3" => "M j, Y, G:i",
		"4" => "D, M j, Y, g:ia",
		"5" => "D, M j, Y, G:i",
		"6" => "j-M-Y g:ia",
		"7" => "j-M-Y G:i",
		"8" => "m/j/Y g:ia",
		"9" => "m/j/Y G:i",
		"10" => "j/m/Y g:ia",
		"11" => "j/m/Y G:i" );

	if( isset( $_GET[ "saved" ]))
	{
		notice( _SET_SAVED );
	}

	if( isset( $_POST[ "submit" ]))
	{
		// Language, Font

		$_auth[ "useLanguage" ] = addslashes( $_POST[ "language" ]);

		setcookie( "yGalLanguage", $_auth[ "useLanguage" ], strtotime( "+9 years" ),
			"/", ".".$_config[ "galRoot" ]); // Give the user a fresh language cookie.

		$GLOBALS[ "_yGalLanguage" ] = $_auth[ "useLanguage" ];

		setcookie( "yGalFont", addslashes( $_POST[ "font" ]), strtotime( "+9 years" ),
			"/", ".".$_config[ "galRoot" ]); // Give the user a fresh font cookie.

		$GLOBALS[ "_yGalFont" ] = addslashes( $_POST[ "font" ]);

		// Filters

		$first = true;
		$oldObjFilters = $_auth[ "useObjFilters" ];
		$_auth[ "useObjFilters" ] = "";

		foreach( getEnabledFilters() as $filter )
		{
			if( isset( $_POST[ "filter".$filter ]))
			{
				$_auth[ "useObjFilters" ] .= ( $first ? "" : "," ).$filter;

				if( $first )
				{
					$first = false;
				}
			}
		}

		if( !isUserMature() )
		{
			$_auth[ "useObjFilters" ] .= ",".$_config[ "matureFilters" ];

			$_auth[ "useObjFilters" ] = implode( ",", array_unique(
				split( ",", $_auth[ "useObjFilters" ]))); // Get rid of duplicates
		}

		$oldHideExtras = $_auth[ "useHideExtras" ];

		$_auth[ "useHideExtras" ] = isset( $_POST[ "filter-1" ]) ? 1 : 0;

		if( $_auth[ "useObjFilters" ] != $oldObjFilters ||
			$_auth[ "useHideExtras" ] != $oldHideExtras )
		{
			recountUpdates( updTypeArt, $_auth[ "useid" ]); // Recalculate art updates
			recountUpdates( updTypeArtExtra, $_auth[ "useid" ]);
		}

		// Other options

		$_auth[ "useTimezone" ] = isset( $_POST[ "timezone" ]) ? intval( $_POST[ "timezone" ]) : 0;

		$_auth[ "useDateFormat" ] = isset( $dateFormatList[ $_POST[ "dateFormat" ]]) ?
			$dateFormatList[ $_POST[ "dateFormat" ]] : $dateFormatList[ 0 ];

		$_auth[ "useObjPreview" ] = isset( $_POST[ "useObjPreview" ]) ? 1 : 0;

		$_auth[ "useFuzzyNumbers" ] = isset( $_POST[ "useFuzzyNumbers" ]) ? 1 : 0;

		$_auth[ "useStatsHide" ] = isset( $_POST[ "useStatsHide" ]) ? 1 : 0;

		$_auth[ "useTheme" ] = isset( $_COOKIE[ "style" ]) ? $_COOKIE[ "style" ] : "original";

		$_auth[ "useDisableCustom" ] = isset( $_POST[ "useDisableCustom" ] ) ?
			intval( $_POST[ "useDisableCustom" ]) : 0;

		$_auth[ "useEnableUI2" ] =
			isset( $_POST[ "useEnableUI2" ]) ? ( isset( $_POST[ "useEnableUI2a" ]) ? 1 : 2 ) : 0;
		
		$_auth[ "usePMBanner" ] = isset( $_POST[ "usePMBanner" ]) ? 1 : 0;

		// Update the database

		sql_query( "UPDATE `users`".dbSet( array(
			"useDateFormat" => $_auth[ "useDateFormat" ])).dbWhere( array(
			"useid" => $_auth[ "useid" ])));

		sql_query( "UPDATE `useExtData`".dbSet( array(
			"useDisableCustom" => $_auth[ "useDisableCustom" ],
			"useEnableUI2" => $_auth[ "useEnableUI2" ],
			"usePMBanner" => $_auth[ "usePMBanner" ],
			"useFuzzyNumbers" => $_auth[ "useFuzzyNumbers" ],
			"useLanguage" => $_auth[ "useLanguage" ],
			"useObjFilters" => $_auth[ "useObjFilters" ],
			"useObjPreview" => $_auth[ "useObjPreview" ],
			"useStatsHide" => $_auth[ "useStatsHide" ],
			"useHideExtras" => $_auth[ "useHideExtras" ],
			"useTheme" => $_auth[ "useTheme" ],
			"useTimezone" => $_auth[ "useTimezone" ])).dbWhere( array(
			"useEid" => $_auth[ "useid" ])));

		redirect( url( ".", array( "saved" => "yes" )));
	}

	// ======================================================================================================
	// GENERAL SETTINGS
	// ======================================================================================================

	?>
	<? iefixStart() ?>
	<div class="sep largetext"><?= _SET_GENERAL_SETTINGS ?></div>
	<div class="container2 notsowide">
		<table cellspacing="0" cellpadding="4" border="0">
		<tr>
			<td align="right" class="nowrap"><?= _SET_LANGUAGE ?>:</td>
			<td><?

				$result = sql_query( "SELECT * FROM `languages` ORDER BY `lanEngName`" );

				?>
				<select name="language">
					<?

					while( $rowData = mysql_fetch_assoc( $result ))
					{
						?>
						<option <?= ( $rowData[ "lanid" ] == $_auth["useLanguage"] ? 'selected="selected" ' : "" ) ?>
							value="<?= $rowData[ "lanid" ] ?>"><?= htmlspecialchars( $rowData[ "lanName" ]) ?>
							(<?= htmlspecialchars( $rowData[ "lanEngName" ]) ?>)</option>
						<?
					}

					?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" class="nowrap"><?= _FONT ?>:</td>
			<td>
				<select name="font">
					<?

					if( isset( $_COOKIE[ "yGalFont" ]))
					{
						$font = addslashes( $_COOKIE[ "yGalFont" ]);
					}
					elseif( isset( $_GET[ "yGalFont" ]))
					{
						$font = addslashes( $_GET[ "yGalFont" ]);
					}
					else
					{
						$font = "original";
					}

					$fonts = array(
						"original" => "Verdana ("._FONT_NORMAL.")",
						"originalbig" => "Verdana ("._FONT_BIGGER.")",
						"originalhuge" => "Verdana ("._FONT_HUGE.")",
						"comic" => "Comic ("._FONT_NORMAL.")",
						"comicbig" => "Comic ("._FONT_BIGGER.")",
						"comichuge" => "Comic ("._FONT_HUGE.")",
						"times" => "Times ("._FONT_NORMAL.")",
						"timesbig" => "Times ("._FONT_BIGGER.")",
						"timeshuge" => "Times ("._FONT_HUGE.")",
						"trebuchet" => "Trebuchet ("._FONT_NORMAL.")",
						"trebuchetbig" => "Trebuchet ("._FONT_BIGGER.")",
						"trebuchethuge" => "Trebuchet ("._FONT_HUGE.")" );

					foreach( $fonts as $fonid => $fonName )
					{
						?>
						<option <?= ( $font == $fonid ? 'selected="selected" ' : "" ) ?>
							value="<?= $fonid ?>"><?= htmlspecialchars( $fonName ) ?></option>
						<?
					}

					?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" class="nowrap"><?= _SET_TIMEZONE ?>:</td>
			<td><?

				$result = sql_query( "SELECT `timid`, `timOffset`, `timName` ".
					"FROM `timezones` ORDER BY `timOffset`" );

				?>
				<select name="timezone">
					<?

					while( $rowData = mysql_fetch_assoc( $result ))
					{
						$timName = htmlspecialchars( str_replace( "_", " ", $rowData[ "timName" ]));

						$offsetGMT = getTimezoneOffset( $rowData[ "timOffset" ]);

						?>
						<option <?= ( $rowData[ "timid" ] == $_auth[ "useTimezone" ] ? 'selected="selected" ' : "" ) ?>
							value="<?= $rowData[ "timid" ] ?>"><?= "(GMT$offsetGMT) $timName" ?></option>
						<?
					}

					?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" class="nowrap"><?= _SET_DATE_FORMAT ?>:</td>
			<td>
				<select name="dateFormat">
					<?

					foreach( $dateFormatList as $key => $value )
					{
						?>
						<option <?= ( $value == $_auth["useDateFormat"] ? 'selected="selected" ' : "" ) ?>
							value="<?= $key ?>"><?=
							htmlspecialchars( date( $value, mktime( 15, 30, 45, 10, 20, 1999 ))) ?></option>
						<?
					}

					?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right">
				<input <?= $_auth[ "useObjPreview" ] ? 'checked="checked"' : "" ?> class="checkbox"
					id="settingsObjPreview" name="useObjPreview" type="checkbox" />
			</td>
			<td>
				<label for="settingsObjPreview"><?= _SET_OBJ_PREVIEW ?></label>
			</td>
		</tr>
		<tr>
			<td align="right">
				<input <?= $_auth[ "useFuzzyNumbers" ] ? 'checked="checked"' : ""?> class="checkbox"
					id="settingsUseFuzzy" name="useFuzzyNumbers" type="checkbox" />
			</td>
			<td>
				<label for="settingsUseFuzzy"><?= _SET_FUZZY ?></label>
			</td>
		</tr>
		<tr>
			<td align="right">
				<input <?= $_auth[ "useStatsHide" ] ? 'checked="checked"' : "" ?> class="checkbox"
					id="settingsStatsHide" name="useStatsHide" type="checkbox" />
			</td>
			<td>
				<label for="settingsStatsHide"><?=_SET_STATS_HIDE ?></label>
			</td>
		</tr>
		</table>
	</div>
	<? iefixEnd() ?>
	<?

	// ======================================================================================================
	// FILTERS
	// ======================================================================================================

	if( isUserMature() )
	{
		$hideFilters = array();
	}
	else
	{
		// Hide mature filters from the underage user.

		$hideFilters = preg_split( '/\,/', $_config[ "matureFilters" ], 64, PREG_SPLIT_NO_EMPTY );
	}

	$found = false;

	ob_start();

	?>
	<div class="sep largetext"><?= _FILTERS ?></div>
	<div class="container2 notsowide">
		<div>
			<?= _SET_FILTER_INTRO ?>
		</div>
		<div class="sep">
			<table cellspacing="0" cellpadding="4" border="0">
			<tr>
			<?

			$cols = 0;
			$objFilters = preg_split( '/[\s\,\;]/', $_auth[ "useObjFilters" ], 64, PREG_SPLIT_NO_EMPTY );

			$F = getEnabledFilters();

			if( $_config[ "extrasRatio" ] != "0:1" )
			{
				$F[] = -1;

				if( $_auth[ "useHideExtras" ])
				{
					$objFilters[] = -1;
				}
			}

			foreach( $F as $filter )
			{
				if( in_array( $filter, $hideFilters ))
				{
					continue; // Skip hidden filters.
				}

				if( $cols > 1 )
				{
					?>
					</tr><tr>
					<?

					$cols = 0;
				}

				$filterName = $filter == -1 ? _SUBMIT_TYPE_EXTRA : getFilterName( $filter );

				?>
				<td align="right" width="40px">
					<input <?= in_array( $filter, $objFilters ) ? 'checked="checked"' : "" ?>
					class="checkbox" id="idFilter<?= $filter ?>" name="filter<?= $filter ?>"
					type="checkbox" /></td>
				<td>
					<label for="idFilter<?= $filter ?>">
					<? printf( _FILTER, $filterName ) ?></label></td>
				<?

				$found = true;
				$cols++;
			}

			?>
			</tr>
			</table>
		</div>
		<div class="sep">
			<a style="font-weight: normal"
				href="javascript:popup('<?= url( "help", array( "popup" => "yes" )) ?>#nudity')">
				<?= _SET_FILTER_EXPLAIN ?></a>
		</div>
	</div>
	<?

	if( $found )
		ob_end_flush();
	else
		ob_end_clean();

	// ======================================================================================================
	// USER INTERFACE THEME/STYLE
	// ======================================================================================================

	?>
	<? iefixStart() ?>
	<div class="sep largetext"><?= _SET_THEME ?></div>
	<div class="container2 notsowide">
		<div class="mar_bottom">
			<input <?= $_auth[ "useEnableUI2" ] > 0 ? 'checked="checked"' : "" ?>
				class="checkbox" id="idEnableUI2" name="useEnableUI2" type="checkbox" />
			<label for="idEnableUI2">New style layout</label>
			&nbsp;
			<input <?= $_auth[ "useEnableUI2" ] == 1 ? 'checked="checked"' : "" ?>
				class="checkbox" id="idEnableUI2a" name="useEnableUI2a" type="checkbox" />
			<label for="idEnableUI2a">Main menu on the top</label>
			<input <?= $_auth[ "usePMBanner" ] == 1 ? 'checked="checked"' : "" ?>
				class="checkbox" id="idPMBanner" name="usePMBanner" type="checkbox" />
			<label for="idPMBanner">PM Banner on the top</label>
		</div>
		<div>
			<?= _SET_THEME_INTRO ?>
		</div>
		<div class="sep" style="padding: 0 25px">
			<?

			$result = sql_query( "SELECT * FROM `themes`" );

			while( $rowData = mysql_fetch_assoc( $result ))
			{
				?>
				<a href="#" onclick="setActiveStyleSheet('<?= $rowData[ "theLongName" ] ?>'); return false;">
					<?= getIMG( url()."themes/".$rowData[ "theName" ]."/icon.png",
						'alt="'.$rowData[ "theLongName" ].'" title="'.$rowData[ "theLongName" ].'"' ) ?></a>
				<?
			}

			?>
		</div>
		<div class="sep notsowide">
			<?= _SET_THEME_CUSTOM_INTRO ?>
		</div>
		<div class="sep" style="padding: 0 25px">
			<?

			if( isArtist( $_auth[ "useid" ]))
			{
				?>
				<input type="radio" class="radio" id="idDisableCustom1"
					<?= $_auth[ "useDisableCustom"] == 1 ? 'checked="checked"' : "" ?>
					name="useDisableCustom" value="1" />
				<label for="idDisableCustom1"><?= _SET_THEME_OPTION1 ?></label>
				<br />
				<input type="radio" class="radio" id="idDisableCustom2"
					<?= $_auth[ "useDisableCustom"] == 2 ? 'checked="checked"' : "" ?>
					name="useDisableCustom" value="2" />
				<label for="idDisableCustom2"><?= _SET_THEME_OPTION2 ?></label>
				<br />
				<input type="radio" class="radio" id="idDisableCustom0"
					<?= $_auth[ "useDisableCustom"] == 0 ? 'checked="checked"' : "" ?>
					name="useDisableCustom" value="0" />
				<label for="idDisableCustom0"><?= _SET_THEME_OPTION3 ?></label>
				<br />
				<input type="radio" class="radio" id="idDisableCustom3"
					<?= $_auth[ "useDisableCustom"] == 3 ? 'checked="checked"' : "" ?>
					name="useDisableCustom" value="3" />
				<label for="idDisableCustom3"><?= _SET_THEME_OPTION4 ?></label>
				<?
			}
			else
			{
				?>
				<input type="radio" class="radio" id="idDisableCustom1"
					<?= $_auth[ "useDisableCustom"] == 1 ? 'checked="checked"' : "" ?>
					name="useDisableCustom" value="1" />
				<label for="idDisableCustom1"><?= _SET_THEME_OPTION1 ?></label>
				<br />
				<input type="radio" class="radio" id="idDisableCustom0"
					<?= $_auth[ "useDisableCustom"] == 0 ? 'checked="checked"' : "" ?>
					name="useDisableCustom" value="0" />
				<label for="idDisableCustom0"><?= _SET_THEME_OPTION2 ?></label>
				<?
			}

			?>
		</div>
		<?

		if( isArtist( $_auth[ "useid" ]))
		{
			?>
			<div class="sep">
				<a href="<?= url( "themedesigner/".strtolower( $_auth[ "useUsername" ])) ?>">
					<?= _SET_DESIGN_CUSTOM_THEME ?></a>
			</div>
			<?
		}

		?>
	</div>
	<? iefixEnd() ?>
	<?

	// ======================================================================================================

	?>
	<div class="sep">
		<button class="submit" name="submit" type="submit">
			<?= getIMG( url()."images/emoticons/checked.png" ) ?>
			<?= _SAVE_CHANGES ?>
		</button>
	</div>
	</form>
</div>
