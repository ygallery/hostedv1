<?

$viewOK = false;

// Submission id must be passed as the first parameter.

if( substr( $_cmd[ 1 ], 0, 1 ) == "e" )
{
	if( !isExtras() )
	{
		include(INCLUDES."p_notfound.php");
		return;
	}

	$isExtras = true;
	$objid = intval( substr( $_cmd[ 1 ], 1 ));
	$_objects = "`extras`";
	$_objExtData = "`extExtData`";
}
else
{
	$objid = intval( $_cmd[ 1 ]);
	$isExtras = false;
	$_objects = "`objects`";
	$_objExtData = "`objExtData`";
}

$where = "(`objid` = '".$objid."' AND `objEid` = `objid`)".
	( atLeastHelpdesk() ? "" :
	"AND (`objPending` = '0' AND (`objDeleted` = '0' OR `objDeletedBy` = '".$_auth[ "useid" ]."')".
	"OR (`objPendingUser`='1' AND `objCreator`=".$_auth[ "useid" ]."))" );

applyObjFilters( $where );

$result = sql_query( "SELECT * FROM $_objects, $_objExtData ".
	"WHERE $where LIMIT 1" );

if( !$objData = mysql_fetch_assoc( $result ))
{
	// The submission no longer exists.

	if( isLoggedIn() )
	{
		include(INCLUDES."p_notfound.php");
		return;
	}
	else
	{
		include(INCLUDES."p_notfound.php");
		return;
	}
}

$whereGuest = isLoggedIn() ? "" : ( " AND `useGuestAccess` = '1' " );

$result = sql_query( "SELECT * FROM `users`, `useExtData`".dbWhere( array(
	"useid*" => "useEid",
	"useid" => $objData[ "objCreator" ]))."$whereGuest LIMIT 1" );

if( !$useData = mysql_fetch_assoc( $result ))
{
	// Due to some reason the submission's creator no longer
	// exists in the database.

	if( isLoggedIn() )
	{
		include(INCLUDES."p_notfound.php");
		return;
	}
	else
	{
		ob_end_clean();
		header( "Status: 404 Not Found" );
		echo "<html><body><h1>Page not found</h1>".
			"<p>Please check your target location</p>".
			"</body></html>";
		exit();
	}
}

// Increase views count if the artwork is viewed, but not by the artist.
// Also don't increase views count if the page doesn't have the correct server
// in the HTTP_REFERER header (e.g. search-bots, "popularity" tricks, etc..)

if( $_cmd[ 0 ] == "view" && isLoggedIn() && $useData[ "useid" ] != $_auth[ "useid" ] &&
	isset( $_SERVER[ "HTTP_REFERER" ]) &&
	strpos( $_SERVER[ "HTTP_REFERER" ], 'http://'.$_config[ "galRoot" ]) === 0 )
{
	$objData[ "objViewed" ]++;

	if( $isExtras )
	{
		sql_query( "UPDATE $_objects SET `objViewed` = '".$objData[ "objViewed" ]."' ".
			"WHERE `objid` = '".$objid."' LIMIT 1" );
	}
	else
	{
		$objData[ "objPopularity" ] = round(
			$objData[ "objFavs" ] * $objData[ "objFavs" ] * 1000 / $objData[ "objViewed" ]);

		sql_query( "UPDATE $_objExtData SET `objViewed` = '".$objData[ "objViewed" ]."' ".
			"WHERE `objEid` = '".$objid."' LIMIT 1" );

		sql_query( "UPDATE $_objects SET `objPopularity` = '".$objData[ "objPopularity" ]."' ".
			"WHERE `objid` = '".$objid."' LIMIT 1" );
	}

	// Also gather Fan-to-Artist statistics...

	$where = array(
		"fanUser" => $_auth[ "useid" ],
		"fanArtist" => $objData[ "objCreator" ]);

	sql_where( $where );

	if( sql_count( "fans" ) == 0 )
	{
		$where[ "fanNumViews" ] = 1;
		sql_values( $where );
		sql_insert( "fans" );
	}
	else
	{
		sql_where( $where );
		sql_values( array( "fanNumViews!" => "`fanNumViews` + 1" ));
		sql_update( "fans" );
	}

	if( $objData[ "objCollab" ] > 0 )
	{
		$where = array(
			"fanUser" => $_auth[ "useid" ],
			"fanArtist" => $objData[ "objCollab" ]);
	    
		sql_where( $where );
	    
		if( sql_count( "fans" ) == 0 )
		{
			$where[ "fanNumViews" ] = 1;
			sql_values( $where );
			sql_insert( "fans" );
		}
		else
		{
			sql_where( $where );
			sql_values( array( "fanNumViews!" => "`fanNumViews` + 1" ));
			sql_update( "fans" );
		}
	}
}

$_pollUser = $useData[ "useid" ];

$_documentTitle = _SUBMISSION.": ".$objData[ "objTitle" ];

?>
<div class="header">
	<?

	if( $objData[ "objCollab" ] > 0 )
	{
		?>
		<div class="f_right a_center normaltext" style="margin-left: 20px">
			<?= getUserAvatar( "", $objData[ "objCollab" ], true ) ?>
		</div>
		<?
	}

	?>
	<div class="f_right mar_left a_center normaltext">
		<?= getUserAvatar( "", $useData[ "useid" ], true ) ?>
	</div>
	<div class="header_title">
		<?= formatText( $objData[ "objTitle" ]) ?>
		<div class="subheader">
			<?

			if( $objData[ "objCollab" ] > 0 )
			{
				printf( _BY_AND,
					getUserLink( $objData[ "objCreator" ]),
					getUserLink( $objData[ "objCollab" ])." ".($objData[ "objCollabConfirmed" ] ? "" : "("._UNCONFIRMED.")"));
			}
			else
			{
				printf( _BY, getUserLink( $objData[ "objCreator" ]));
			}

			if( $objData[ "objForUser" ] > 0 )
			{
				echo " ";
				printf( _FOR, getUserLink( $objData[ "objForUser" ]));
			}

			?>
		</div>
	</div>
	<div style="clear: both" class="normaltext">

		<div class="tab<?= $active == 1 ? " tab_active" : "" ?>"
			onclick="document.location='<?= url( "view/".( $isExtras ? "e" : "" ).$objid ) ?>'">
			<?= getIMG( url()."images/emoticons/submission.png" )?>
			<?= _SUBMISSION ?></div>
		<?/*
		<div class="tab<?= $active == 2 ? " tab_active" : "" ?>"
			onclick="document.location='<?= url( "discuss/".( $isExtras ? "e" : "" ).$objid ) ?>'">
			<?= getIMG( url()."images/emoticons/comment.png" )?>
			<?= _COMMENTS ?></div>
		<?*/

		/*
		if( isLoggedIn() && !$isExtras && $objData[ "objFavs" ] >= 3 )
		{
			?>
			<div class="tab<?= $active == 3 ? " tab_active" : "" ?>"
				onclick="document.location='<?= url( "suggest/".( $isExtras ? "e" : "" ).$objid ) ?>'">
				<?= getIMG( url()."images/emoticons/nav-next.png" )?>
				<?= _SUGGESTIONS ?></div>
			<?
		}
		*/

		if( atLeastHelpdesk() || $useData[ "useid" ] == $_auth[ "useid" ])
		{
			?>
			<div class="tab<?= $active == 4 ? " tab_active" : "" ?>"
				onclick="document.location='<?= url( "viewedit/".( $isExtras ? "e" : "" ).$objid ) ?>'">
				<?= _EDIT_SUBMISSION ?></div>
			<?
		}

		?>
		<div class="clear"><br /></div>

	</div>
	<div class="clear">&nbsp;</div>
</div>
<?

$viewOK = true;

?>