<?php

// -----------------------------------------------------------------------------
// Common functions.

include_once( INCLUDES."strings/".$_lang.".php" );
include_once( INCLUDES.'strings/global.php');
include_once( INCLUDES."errorhandler.php" );
include_once( INCLUDES."database.php" );

// Remove magic quotes if there's any. This is needed to make sure GET, POST
// and COOKIE never contain magic quotes on any system, so addslashes() won't
// escape those symbols once again.

if( get_magic_quotes_gpc() )
{
	noMagicQuotesGPC( $_GET );
	noMagicQuotesGPC( $_POST );
	noMagicQuotesGPC( $_COOKIE );

	// Prevent possible double-removal of magic quotes at the later time.

	ini_set( "magic_quotes_gpc", 0 );
}

// Parse the current URI command.

getCommand();

// Log some information in case server load is greater than 8 (needed for
// debug reasons, not needed anymore).

/*
$uptime = `uptime`;
$load = intval( trim( substr( strrchr( $uptime, ':' ), 1 )));

if( $load > 8 )
{
	_log( "REQUEST_URI: ".$_SERVER[ "REQUEST_URI" ]);
	_log( "GET: ".serialize( $_GET ));
	_log( "POST: ".serialize( $_POST ));
	_log( "COOKIE: ".serialize( $_COOKIE ));
}
*/

// -----------------------------------------------------------------------------
// Gets rid of extra \'s if magic quotes are on.

function noMagicQuotesGPC( & $var )
{
	if( is_string( $var ))
	{
		$var = stripslashes( $var );
	}
	elseif( is_array( $var ))
	{
		foreach( $var as $key => $value )
			noMagicQuotesGPC( $var[ $key ]);
	}
	elseif( is_object( $var ))
	{
		foreach( get_object_vars( $var ) as $key => $value )
			noMagicQuotesGPC( $var -> $key );
	}
}

// -----------------------------------------------------------------------------
// IP address translation functions.

function getHexIp( $ip )
{
	// Oops, hi IPv6!
	if(strpos($ip, '::ffff:') !== false)
	{
		$ip = substr($ip, 7);
	}

	return dechex( ip2long( $ip ));
}

function getDotDecIp( $ip )
{
	return long2ip( hexdec( $ip ));
}

// -----------------------------------------------------------------------------
// Returns an array of "command line" elements (that's what you put after the
// base URL, e.g. "view/123" and what you pass in the url() function),
// unified in both GET-based and mod_rewrite modes. First command element
// with the index [0] is usually the current page name.

function getCommand()
{
	global $_cmd, $_config, $_mod_rewrite;

	if( $_mod_rewrite )
	{
		// Imitate GET variables in 'mod_rewrite' mode.

		$_cmd = preg_split( '/(\/)/', $_SERVER[ "REQUEST_URI" ],
			10, PREG_SPLIT_NO_EMPTY );

		// Get rid of GET variables.

		$_cmd = preg_replace( '/\?.+$/', "", $_cmd );

		for( $i = count( $_cmd ); $i < 10; $i++ )
			$_cmd[ $i ] = "";
	}
	else
	{
		$_cmd = array( isset( $_GET[ "page" ]) ? $_GET[ "page" ] : "" );

		for( $i = 1; $i < 10; $i++ )
			$_cmd[ $i ] = isset( $_GET[ "par".$i ]) ? $_GET[ "par".$i ] : "";
	}

	for( $i = 0; $i < count( $_cmd ); $i++ )
	{
		$_cmd[ $i ] = preg_replace( '/([^a-zA-Z0-9\.\-\_\~])/', "",
			$_cmd[ $i ]);
	}

	return $_cmd;
}

// -----------------------------------------------------------------------------

function _log( $str )
{
	$fp = fopen( "files/_debug.txt", "a" );
	flock( $fp, LOCK_EX );
	fwrite( $fp, $str."\n" );
	fclose( $fp );
}

// -----------------------------------------------------------------------------
// Redirects the user's browser to a new location.

function redirect( $url )
{
	global $_isOpera;

	sql_write_log();

	// Redirect via an HTML form for PITA webservers.

	ob_end_clean();

	if( /*$_isOpera ||*/ @preg_match( '/Microsoft|WebSTAR|Xitami/',
		$_SERVER[ "SERVER_SOFTWARE" ]))
	{
		header( "Status: 200 OK", true );
		header( "Refresh: 0; URL=".$url );

		?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1 Extended//EN"
			"http://www.zetafleet.com/includes/xhtml11ext.dtd">
		<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<meta http-equiv="refresh" content="0; url=<?= $url ?>" />
			<title>Redirect</title>
		</head>
		<body>
			<br /><br />
			<div align="center">
				<? printf( _REDIRECT_EXPLAIN, $url ) ?>
			</div>
		</body>
		</html>
		<?

		exit();
	}

	if( $_isOpera )
	{
		// Opera (at least 8.5) behaves wronly when redirecting to the different
		// section (#) of the same page, so... :(

		$pos = strpos( $url, "#" );

		if( $pos !== false )
		{
			$url = substr( $url, 0, $pos );
		}
	}

	header( "Status: 302 Found", true);
	header( "Location: ".str_replace( '&amp;', '&', $url ));
	exit();
}

// -----------------------------------------------------------------------------
// Can be used for the INSERT query. Returns the string like
// "(...) VALUES(...)" that has to be inserted inside the MySQL query right
// after "INSERT INTO `mytable`". This also makes the values safe for insertion
// by applying addslashes(). This means addslashes() must NOT be previously
// applied to the values passed to this function.
//
// If array [key] ends with a "!", the brackets around it would be eliminated,
// i.e. "comLastEdit!" => "NOW()" would become "`comLastEdit` = NOW()".

function dbValues( $valuesArray )
{
	$fields = "(";
	$values = " VALUES(";
	$first = true;

	foreach( $valuesArray as $key => $value )
	{
		$isExpr = preg_match( '/\!$/', $key );
		$key = preg_replace( '/\!$/', "", $key );

		if( $value === false )
		{
			$value = 0;
		}

		$value = strval( $value );

		$fields .= ($first ? "" : ",")."`".addslashes( $key )."`";
		$values .= ($first ? "" : ",").($isExpr ? $value : "'".
			addslashes( $value )."'");

		$first = false;
	}

	return( $fields.")".$values.")" );
}

// -----------------------------------------------------------------------------
// Safe generation of the WHERE expression. Each condition is passed as an
// array item like [field name] => [value]; all conditions will be joined
// using the AND operator. If the "field name" ends with "<", ">", "<=", ">="
// or "<>" then the specified operator substitutes the "=" that is used by
// default. If more than one condition to a field is required, you may add
// "|0", "|1" etc at the end of the field name to make the key unique within
// the array. If "field name" is an empty string (even after elimination of
// "|0", "|1" etc) then the "value" is included as is.
//
// If [field name] ends with a "*", the brackets around it would be reverted,
// i.e. "objid*" => "comObj" would become "`objid` = `comObj`".
//
// If [field name] ends with a "!", the brackets around it would be eliminated,
// i.e. "comLastEdit>!" => "NOW()" would become "`comLastEdit` > NOW()".

function dbWhere( $whereArray, $initialString = "WHERE", $delimiter = "AND" )
{
	$where = " ".$initialString." ";
	$first = true;

	// Do not change the order of these:

	$signs = array( "#=", "~=", "<>", "<=", ">=", "<", ">", "=" );

	foreach( $whereArray as $key => $value )
	{
		$key = preg_replace( '/\|[0-9]+$/', "", $key );

		if( $value === false )
		{
			$value = 0;
		}

		$value = strval( $value );
		$where .= $first ? "" : $delimiter." ";
		$first = false;

		if( $key == "" ) // "value" contains the whole expression within itself?
		{
			$where .= "($value) ";
			continue;
		}

		$sign = "="; // default sign is "="
		$brackets = "'";

		if( preg_match( '/\*$/', $key ))
		{
			$key = substr( $key, 0, strlen( $key ) - 1 );
			$brackets = "`";
		}

		if( preg_match( '/\!$/', $key ))
		{
			$key = substr( $key, 0, strlen( $key ) - 1 );
			$brackets = "";
		}

		reset( $signs );

		foreach( $signs as $s1 )
		{
			if( substr( $key, strlen( $key ) - strlen( $s1 ), strlen( $s1 )) == $s1 )
			{
				$sign = $s1;
				$key = substr( $key, 0, strlen( $key ) - strlen( $s1 ));
				break;
			}
		}

		$binary = "";

		if( $sign == "~=" )
		{
			$sign = " LIKE ";
		}

		if( $sign == "#=" )
		{
			$binary = trim( $value ) == "" ? "" : "BINARY ";
			$sign = " = ";
		}

		$where .= "$binary`".addslashes( $key )."`".$sign.
			"$binary$brackets".addslashes( $value )."$brackets ";
	}

	if( $where == " ".$initialString." " )
		$where .= "1 ";

	return( $where );
}

function dbSet( $setArray )
{
	return( dbWhere( $setArray, "SET", "," ));
}

// -----------------------------------------------------------------------------
// Returns an array of the config.enableFilters values.

function getEnabledFilters()
{
	global $_config;

	return( preg_split( '/[\s\,\;]/', $_config[ "enableFilters" ], 64,
		PREG_SPLIT_NO_EMPTY ));
}

// -----------------------------------------------------------------------------
// Returns the title of the given filter value.

function getFilterName( $filter )
{
	if( defined( "_FILTER_".$filter ))
	{
		return( constant( "_FILTER_".$filter ));
	}
	else
	{
		return( "" );
	}
}

// -----------------------------------------------------------------------------
// Returns the filter mask for the currently logged in user (0) or any
// specified user from the database.

function getFilterMask( $useid = 0 )
{
	global $_auth, $_config;

	if( $useid == 0 )
	{
		$useObjFilters = $_auth[ "useObjFilters" ];
	}
	else
	{
		$result = sql_query( "SELECT `useObjFilters` FROM `useExtData` ".
			"WHERE `useEid` = '$useid' LIMIT 1" );

		if( mysql_num_rows( $result ) > 0 )
		{
			$useObjFilters = mysql_result( $result, 0 );
		}
		else
		{
			// If user has not been found, enable all filters.

			$useObjFilters = $_config[ "enableFilters" ];
		}
	}

	$useFilters = preg_split( '/[\s\,\;]/', $useObjFilters, 64,
		PREG_SPLIT_NO_EMPTY );

	$mask = 0;
	$processed = array();

	foreach( $useFilters as $filter )
	{
		if( !isset( $processed[ $filter ]))
		{
			$mask += pow( 2, $filter );
			$processed[ $filter ] = true;
		}
	}

	return( $mask );
}

// -----------------------------------------------------------------------------
// Returns the title of the given filter value.

function applyObjFilters( & $where, $useid = 0 )
{
	$filterMask = getFilterMask( $useid );

	if( $filterMask != 0 )
	{
		$where = "($where) AND (`objMature` & $filterMask) = 0";
	}
}

// -----------------------------------------------------------------------------
// Adds a new message to moderator's log.

function addModeratorLog( $useid, $message )
{
	global $_auth;

	sql_query( "INSERT INTO `modlogs`".dbValues( array(
		"modSubmitDate!" => "NOW()",
		"modModerator" => $_auth[ "useid" ],
		"modUser" => $useid,
		"modMessage" => $message )));
}

// -----------------------------------------------------------------------------
// Returns array of filters according to the given submission's attached
// keywords.

function getFiltersByObject( $objid )
{
	$keyids = array();

	$objKresult = sql_query( "SELECT `objKkeyword` FROM `objKeywords`".dbWhere( array(
		"objKobject" => $objid )));

	while( $objKdata = mysql_fetch_row( $objKresult ))
	{
		$keyid = $objKdata[ 0 ];

		$keyids[] = $keyid;
	}

	mysql_free_result( $objKresult );

	return( getFiltersByKeywords( $keyids ));
}

// -----------------------------------------------------------------------------
// Returns array of filters according to the given array of keyword ids.

function getFiltersByKeywords( $keyids )
{
	$filters = array();

	foreach( $keyids as $keyid )
	{
		$keyResult = sql_query( "SELECT `keyFilter` FROM `keywords`".dbWhere( array(
			"keyid" => $keyid )));

		if( $keyData = mysql_fetch_row( $keyResult ))
		{
			$keyFilter = $keyData[ 0 ];

			$F = preg_split( '/[\s\,\;]/', $keyFilter, 64, PREG_SPLIT_NO_EMPTY );

			$filters = array_merge( $filters, $F );
		}

		mysql_free_result( $keyResult );
	}

	$filters = array_unique( $filters ); // Remove duplicates

	return( $filters );
}

?>
