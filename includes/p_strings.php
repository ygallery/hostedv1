<?

// -----------------------------------------------------------------------------
// This page is only accessible by a supermoderator or admin.

if( !atLeastSModerator() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

// -----------------------------------------------------------------------------
// All the following strings will be passed to formatText(), so they
// should use [url] instead of <a>, [b] instead of <b> and so on.
// All quotes must be " instead of &quot;.
// New lines must be started with \n and not <br />.

$strings = array(

" native language name" => array( // Do not delete the space

	"__LANG" => "English",
	),

"abuse" => array(

	"_ABUSE_DECISION" => "Decision",
	"_ABUSE_DECISION_BLANK" => "Please make your decision first.",
	"_ABUSE_DECISION_DELETE" => "Delete submission",
	"_ABUSE_DECISION_LEAVE" => "Leave submission",
	"_ABUSE_DECISION_VIOLATION" => "Mark as Violation",
	"_ABUSE_DECISION_INVALID" => "Mark as Invalid",
	"_ABUSE_DECISION_COMPLIANT" => "Mark as compliant",
	"_ABUSE_DECISION_RESTORE" => "Restore submission",
	"_ABUSE_DECISION_NONE" => "No decision yet",
	"_ABUSE_DEFAULT_REASON" => "This submission is a violation of the site's Terms of Service. ".
		"Please read the Terms of Service prior to making further submissions.",
	"_ABUSE_FORM_NOTICE" => "This form is used for reporting [url=/tos]Terms of Service[/url] violations ONLY.\nIt is NOT used to leave comments on works.\n[b]If you are reporting a Rule #6 violation, please PROVIDE A LINK to the original work.[/b]",
	"_ABUSE_LIST" => "Abuses list",
	"_ABUSE_OVERALL_DECISION" => "Overall decision",
	"_ABUSE_REASON" => "Reason",
	"_ABUSE_REASON_EXPLAIN" => "ENTER THE REASON WHY THE SUBMISSION IS DELETED (if it is).\n\n".
		"[b]BE CAREFUL. This text will be shown to the artist.[/b]",
	"_ABUSE_REPORT" => "Report",
	"_ABUSE_REPORTED" => "The submission has been temporarily hidden pending abuse moderation.",
	"_ABUSE_REPORT_EXPLAIN" => "Report this submission as abusive",
	"_ABUSE_RESTORE" => "Restore",
	"_ABUSE_RULE" => "Rule",
	"_ABUSE_RULE_BLANK" => "You must specify which rule has been violated",
	"_ABUSE_SEND_BUTTON" => "Submit Decision",
	"_ABUSE_STATUS" => "Current status",
	"_ABUSE_SUSPENDED_NOTICE" => "This account has been suspended until %s for the following reason: %s",
	"_ABUSE_BANNED_NOTICE" => "This account has been permanently banned for the following reason: %s",
	"_ABUSE_SUBMISSION" => "Reported submission",
	"_ABUSE_TOO_MANY" => "You currently have too many abuse cases pending. Please wait until ".
		"some of your cases have cleared and try again.",
	"_ABUSE_WAIT_ADMINISTRATOR" => "Waiting for administrator's decision",
	"_ABUSE_WAIT_MODERATOR" => "Waiting for moderator's decision",
	"_ABUSE_WAIT_SUPERMODERATOR" => "Waiting for supermoderator's decision",
	"_ABUSE_STATS" => "The reporter, %s, has reported %u works. We've agreed with %u%% of their reports.\n\n".
		"The submitter, %s, has had %u works in the abuse system. %u%% of them violated the Terms of Service.",
	),

"ads" => array(
	"_ADS_CAPTION" => "Support %s",
	),

"activate" => array(

	"_ACTIVATE_SUCCESS" => "Your user account has been activated.",
	"_ACTIVATE_TITLE" => "Verify your email address",
	"_ACTIVATE_WRONG_KEY_OR_ACTIVE" => "Wrong activation key specified, or the user account ".
		"is already activated.",
	),

"bbcode explanations" => array(

	"_BB_ALIGN_CENTER" => "Text alignment: Center",
	"_BB_ALIGN_JUSTIFY" => "Text alignment: Justify",
	"_BB_ALIGN_LEFT" => "Text alignment: Left",
	"_BB_ALIGN_RIGHT" => "Text alignment: Right",
	"_BB_BOLD" => "Bold",
	"_BB_CLUBICON" => "Insert a club avatar",
	"_BB_EMOTICON" => "Insert an emotion",
	"_BB_ITALIC" => "Italic",
	"_BB_STRIKED" => "Striked out",
	"_BB_SUBSCRIPT" => "Subscript",
	"_BB_SUPERSCRIPT" => "Superscript",
	"_BB_THUMB" => "Insert a thumbnail linked to a submission",
	"_BB_UNDERLINE" => "Underline",
	"_BB_URL" => "Insert a Link",
	"_BB_USERICON" => "Insert a user avatar",
	),

"clubs" => array(

	"_CLUB_ACCEPT" => "Accept",
	"_CLUB_CREATE" => "Create club/project",
	"_CLUB_DECLINE" => "Decline",
	"_CLUB_DESCR_CURRENT" => "Current short description",
	"_CLUB_DESCR_NEW" => "Edit short description",
	"_CLUB_FEATURE_DESCR" => "Why this featured work was chosen",
	"_CLUB_FEATURE_EXPLAIN" => "Submission number",
	"_CLUB_FEATURE_EXPLAIN2" => "(you can find it at the end of the submission's page location)",
	"_CLUB_FOUND" => "Found a new club/project",
	"_CLUB_HIDE_CLUB" => "Do not show this club/project on the main list of clubs/projects.",
	"_CLUB_ICON_CURRENT" => "Current club/project icon",
	"_CLUB_ICON_NEW" => "New club/project icon",
	"_CLUB_ICON_UPLOADED" => "Club/project icon uploaded.",
	"_CLUB_ID" => "Club/Project ID",
	"_CLUB_JOIN" => "Join club/project (this allows you to submit your works to this club)",
	"_CLUB_LEAVE" => "Leave club/project",
	"_CLUB_MANAGE" => "Manage club/project",
	"_CLUB_MEMBERSHIP" => "Membership",
	"_CLUB_MEMBERS_PENDING" => "Pending members",
	"_CLUB_MEMBERS_PENDING_EXPLAIN" => "(only visible to the club/project's owner)",
	"_CLUB_MEMBER_EXPLAIN" => "Note: If you accept the member, they will be able to post ".
		"their submissions to the club.",
	"_CLUB_MODERATORS" => "Club Moderators",
	"_CLUB_MODERATORS_ADD" => "Add Club Moderator",
	"_CLUB_NAME" => "Club/project name",
	"_CLUB_NAME_BLANK" => "Club/project name not specified.",
	"_CLUB_NAME_EXISTS" => 'Club/project "%s" already exists.',
	"_CLUB_NAME_NOT_CHANGED" => "Club/project name not changed.",
	"_CLUB_NAME_TOO_SHORT" => "Club/project name is too short. Minimum %u characters allowed.",
	"_CLUB_NOT_CREATED" => "Club/project not created.",
	"_CLUB_NOT_A_MEMBER" => 'The user "%s" is not a member of this club/project.',
	"_CLUB_NOT_A_SUBMISSION" => "That submission is not in this club/project.",
	"_CLUB_OPTIONS" => "Club/project options",
	"_CLUB_OWNER" => "Club Owner",
	"_CLUB_PENDING" => "Please wait for the owner's decision...",
	"_CLUB_REMOVE_USER" => "Remove User",
	"_CLUB_REMOVE_SUBMISSION" => "Remove Submission",
	"_CLUB_MODERATORS" => "Club Moderators",
	"_CLUB_REQUIRE_ACCEPT" => "PRIVATE STATUS: Require all new members to be accepted by ".
		"the club/project's owner; note that all members will be able to post submissions ".
		"to the club/project.",
	"_CLUB_SETTINGS_SAVED" => "Club/project settings saved.",
	"_CLUB_STATUS" => "Status",
	"_CLUB_USER_CONTENT" => "User and Content Management",
	"_CLUB_POPULAR" => "Popular",
	"_CLUB_ALLOW_MEMBERSHIP" => "ALLOW MEMBERSHIP: Other users can become members and ".
		"will be able to post their submissions and announcements to the club.",
	),

"database connection" => array(

	"_DB_ERR_CONNECT" => "Unable to connect to SQL database server.",
	"_DB_ERR_DATABASE" => "Unable to select correct database.",
	),

"donations" => array(

	"_DONATION_AMOUNT" => "Amount (USD)",
	"_DONATION_AMOUNT_INVALID" => "Invalid amount entered. Please verify.",
	"_DONATION_ANONYMOUS" => "Anonymous donation",
	"_DONATION_ANONYMOUS_LIST" => "Anonymous donations",
	"_DONATION_DONATE" => "Donate",
	"_DONATION_LIST" => "Collected donations (total / last month)",
	"_DONATION_ONETIME" => "Make one-time donation",
	"_DONATION_SUBSCRIBE" => "Subscribe to monthly donations",
	"_DONATION_SUBTITLE" => "Support %s",
	),

"edit" => array(

	"_EDIT_CANCEL_DELETE" => "Otherwise, %sreturn to the submission's page%s.",
	"_EDIT_CONFIRM_DELETE" => "Click here to confirm that you wish to delete this submission.",
	"_EDIT_DELETED" => "The submission has been deleted.",
	"_EDIT_ERASE" => "ERASE",
	"_EDIT_EXCEEDED" => "You need to speak to a supermoderator if you wish to edit this further.",
	"_EDIT_RESET_EDITS" => "Reset edits (currently %u)",
	"_EDIT_UNDELETE" => "Undelete",
	),

"emails" => array(

	"_EMAIL_SENDING_ERROR" => "Unable to send email. User #%u does not exist.",
	),

"favourites" => array(

	"_FAV" => "Fav",
	"_FAV_ADD" => "Add to Favourites",
	"_FAV_REMOVE" => "Remove from Favourites",
	),

"file uploading" => array(

	"_UPL_ERROR" => "An error %s has occurred during file upload (%s).",
	"_UPL_EXCEED_INI" => "The uploaded file exceeds the upload_max_filesize directive in php.ini (%s).",
	"_UPL_EXCEED_MAXSIZE" => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form (%s).",
	"_UPL_NO_FILE" => "No file was uploaded.",
	"_UPL_PARTIAL" => "The uploaded file was only partially uploaded (%s).",
	"_UPL_WRONG_TYPE" => "Unrecognized uploaded file type: %s (%s).",
	),

"filters" => array(

	"_FILTER" => "Do not show %s", // %s is one of the following
	"_FILTER_OPTIONS" => "This submission contains",
	"_FILTERS" => "Filters",
	"_SET_FILTERS" => "Set Filters",
	),

"fuzzy numbers" => array(

	"_FUZZY_0" => "none",
	"_FUZZY_1" => "one",
	"_FUZZY_2" => "a couple",
	"_FUZZY_3" => "a few",
	"_FUZZY_7" => "several",
	"_FUZZY_11" => "some",
	"_FUZZY_24" => "many",
	"_FUZZY_48" => "a lot",
	"_FUZZY_99" => "plenty",
	"_FUZZY_234" => "heaps",
	"_FUZZY_456" => "a ton",
	"_FUZZY_987" => "too many",
	),

"generic buttons" => array(

	"_BACK" => "Back",
	"_BROWSE" => "Browse",
	"_BROWSE_CLUB" => "Browse Club",
	"_BROWSE_GALLERY" => "Browse Gallery",
	"_CANCEL" => "Cancel",
	"_CHANGE" => "Change",
	"_CLOSE" => "Close",
	"_CONTINUE" => "Continue",
	"_DELETE" => "Delete",
	"_DONATIONS" => "Donations",
	"_EDIT" => "Edit",
	"_GO" => "Go",
	"_HELP" => "Help",
	"_HIDE" => "Hide",
	"_HINT" => "Hint",
	"_JOIN" => "Join",
	"_LOGIN" => "Log in",
	"_LOGOUT" => "Log out",
	"_MARK_AS_READ" => "Mark as read",
	"_MARK_ALL_AS_READ" => "Mark all as read",
	"_MARK_AS_VIEWED" => "Mark selected as viewed",
	"_MARK_ALL_AS_VIEWED" => "Mark ALL as viewed",
	"_OK" => "OK",
	"_PAGE_FIRST" => "First",
	"_PAGE_NEXT" => "Next",
	"_PAGE_PREVIOUS" => "Prev",
	"_PAGE_LAST" => "Last",
	"_PARENT" => "Parent",
	"_REPLY" => "Reply",
	"_RETURN" => "Return",
	"_ROOT" => "Root",
	"_SAVE_CHANGES" => "Save changes",
	"_SEARCH" => "Search",
	"_SELECT_ALL" => "Select all",
	"_SEND_COMMENT" => "Send comment",
	"_STAFF" => "Staff",
	"_SUBMIT" => "Submit",
	"_UPDATE" => "Update",
	"_VOTE" => "Vote",
	"_WHATSNEW" => "What's new",
	"_WHOSONLINE" => "Online users",
	),

"generic captions" => array(

	"_ACCESS_DENIED" => "Access denied",
	"_ALT_IMAGE" => "[Image]",
	"_ALT_THUMB" => "[..]",
	"_ARE_YOU_SURE" => "Are you sure?",
	"_ARTIST_COMMENT" => "Artist's comment",
	"_AVATAR" => "Avatar",
	"_BIRTHDAY" => "Birthday",
	"_BY" => "by %s",
	"_BY_AND" => "by %s and %s",
	"_BY_COLLAB_WITH" => "collab with %s",
	"_BY_NAME" => "By Name",
	"_FOR" => "for %s",
	"_CHAT" => "Chat",
	"_CLICK_TO_CONFIRM" => "Click to confirm",
	"_CONFIRM" => "Confirm",
	"_CONFIRM_MARK_AS_VIEWED" => "Are you sure? This will mark all %s updates as viewed",
	"_CONFIRMATION_NEEDED" => "Confirmation needed",
	"_DEFAULTS" => "Defaults",
	"_DELETED" => "Trashcan",
	"_DELETED_BY" => "Deleted by",
	"_EDIT_COMMENT" => "Edit your comment",
	"_EDIT_PM" => "Edit your private message",
	"_EDIT_SUBMISSION" => "Edit submission",
	"_EMAIL_ADDRESS" => "Email address",
	"_ENTER_CLUBNAME" => "Enter the club's ID (you can find this number at the end ".
		"of the club's page location)",
	"_ENTER_LINK_TEXT" => "Enter text to display for the link",
	"_ENTER_LINK_URL" => "Enter a web page location (URL)",
	"_ENTER_THUMBID" => "Enter the submission's ID (you can find this number at ".
		"the end of the submission's page location)",
	"_ENTER_USERNAME" => "Enter a username",
	"_FILE" => "File",
	"_FOR_REASON" => "For reason",
	"_ICON" => "Icon",
	"_IN" => "in %s",
	"_INFORMATION" => "Information",
	"_INVITATIONS" => "Invitations",
	"_INVITATION_CODE" => "Invitation Code",
	"_LEAVE_COMMENT" => "Leave a comment",
	"_LEAVE_COMMENT_REPLY" => "Reply to the comment",
	"_LINK" => "Link",
	"_MATURE" => "Mature content",
	"_MATURE1" => "Mature",
	"_MATURE_RATING" => "Mature rating",
	"_MESSAGE" => "Message",
	"_MODERATION" => "Moderation",
	"_MOD_BAN" => "Ban",
	"_MOD_BANNED" => "Banned",
	"_MOD_SUSPENDED_LEFT" => "%u days left",
	"_MOD_SUSPENDED_UNTIL" => "%s is suspended until",
	"_MOD_UNBAN" => "Unban",
	"_MORE" => "More",
	"_MOST_FAVED" => "Most faved",
	"_MOST_RECENT" => "Most recent",
	"_MOST_RECENT_FAVS" => "Today's Tops",
	"_MOST_VIEWED" => "Most viewed",
	"_NEWEST_FIRST" => "Newest first",
	"_NOMATURE" => "Do not show mature submissions",
	"_NONE" => "None",
	"_NO_BBCODE" => "No BBCode",
	"_NO_EMOTICONS" => "No Emoticons",
	"_NO_SIG" => "No Signature",
	"_OFFLINE" => "Offline",
	"_OLDEST_FIRST" => "Oldest first",
	"_ONLINE" => "Online",
	"_OTHER_COMMENTS" => "Posted comments",
	"_PASSWORD" => "Password",
	"_PERSONAL_NOTES" => "Personal notes",
	"_PREFERRED_LANGUAGE" => "Preferred language",
	"_PREVIEW" => "Preview",
	"_PROFILE" => "Profile",
	"_RANDOM" => "Random",
	"_READ_MORE_REPLIES" => "Read more replies",
	"_READ_EXPLANATION" => "Read explanation",
	"_REAL_NAME" => "Real name",
	"_RECEIVED" => "Received",
	"_REFUSE" => "Refuse",
	"_REMOVE" => "Remove",
	"_REPORTED_BY" => "Reported by",
	"_SEND_EMAIL" => "Send email",
	"_SETTINGS" => "Settings",
	"_SHOW" => "Show",
	"_SPECIAL" => "Special",
	"_SPECIAL_ACTIONS" => "Special actions",
	"_SPECIAL_SETTINGS" => "Special settings",
	"_TEXT" => "Text",
	"_THUMB_PREVIEW" => "Thumbnail preview",
	"_TITLE" => "Title",
	"_TOTAL_FAVS" => "Total favs",
	"_TOTAL_VIEWS" => "Total views",
	"_UNCONFIRMED" => "Unconfirmed",
	"_UNKNOWN" => "Unknown",
	"_UPDATES" => "Updates",
	"_USERNAME" => "Username",
	"_USER_ID"  => "User ID",
	"_VIEWS" => "Views",
	"_WARNING" => "Warning",
	"_WATCHED_BY" => "Watched by",
	"_WATCH_LIST" => "Watches",
	"_WELCOME_USER" => "Welcome, %s!",
	"_WHATS_THIS" => "What's this?",
	"_WROTE" => "%s wrote",
	"_FONT_NORMAL" => "normal",
	"_FONT_BIGGER" => "bigger",
	"_FONT_HUGE" => "huge",
	),

"generic explanations and messages" => array(

	"_ALREADY_LOGGED" => "You are already logged in.",
	"_ANIMATION_EXPLAIN" => "View Animation",
	"_BLANK_COMMENT" => "Please enter the comment first.",
	"_BLANK_PASSWORD" => "Password not specified.",
	"_BLANK_USERNAME" => "Username not specified.",
	"_COMMENT_EDITED" => "Edited %u time(s), last edit: %s",
	"_COMMENT_POSTED_ON" => "Posted on %s",
	"_COLLAB_CONFIRM_EXPLAIN" => "Please confirm that you are one of the creators ".
		"of this collaborative work.\n\n".
		"If you confirm, you will be also receiving comments on this submission to ".
		"your Updates and it will appear under the Collabs tab in your gallery.",
	"_COLLAB_CONFIRM_WAIT" => "This collaboration is awaiting confirmation from the collaborator.",
	"_CREATE_ACCOUNT" => "Create a new account",
	"_EMAIL_SENT" => "An email has been sent to %s. It should arrive within 5 minutes.",
	"_EMAIL_SENT_HIDDEN_ADDRESS" => "your email address",
	"_EMOTICON_EXPLAIN" => "Add emoticons to your text by clicking on the icons below.",
	"_FAVOURITED" => "You have added \"%s\" to your favourites.",
	"_FULLVIEW_REG_ONLY" => "You have to log in before you can view the submission. ".
		"You can do so from the right-side Account panel.",
	"_GALLERY_MANAGEMENT_EXPLAIN" => "Manage Your Submissions",
	"_JOIN_BANNED_IP" => "You are not allowed to create a new account because an account ".
		"associated with your address is currently suspended/banned.",
	"_LOAD_TOO_HIGH" => "Server load is too high. Please try again later.",
	"_LOGOUT_EXPLAIN" => "Your session has been terminated.",
	"_MAX_LENGTH_255" => "(enter no more than 255 characters)",
	"_NEED_JAVASCRIPT" => "This section requires Javascript to be enabled in your browser.",
	"_NOT_LOGGED" => "You are not logged in.",
	"_NO_COMMENTS" => "No comments posted yet.",
	"_NO_IE" => "SITE NOT WORKING AS EXPECTED? CLICK HERE TO FIND OUT WHY.",
	"_NO_MEMBERS" => "No members found.",
	"_NO_SUBMISSIONS" => "No submissions found.",
	"_ONLINE_USERS" => "Users currently online",
	"_PAGE_GENERATED" => "Page generated in %s sec.",
	"_READONLY" => "The site is currently in read-only mode.",
	"_READ_MORE" => "Read more",
	"_REDIRECT_EXPLAIN" => "If your browser does not support automatic redirection, ".
		"please [url=%s]click here[/url] to be redirected.",
	"_REQUIRE_LOGIN" => "This section requires the user to be logged in.",
	"_SUBMISSION_ERASED" => "The submission has been permanently erased.",
	"_SUGGESTIONS_EXPLAIN" => "People who liked this work also +faved:",
	"_UNKNOWN_CLUB" => "Unknown club #%u",
	"_UNKNOWN_SUBMISSION" => "Unknown submission #%u",
	"_UNKNOWN_USER" => "Unknown user: %s",
	"_WRONG_USERNAME" => "Wrong username specified.",
	),

"generic objects" => array(

	"_ACCOUNT" => "Account",
	"_ADMINISTRATOR" => "Administrator",
	"_ALL_SUBMISSIONS" => "All Submissions",
	"_ANIMATION" => "Animation",
	"_ANNOUNCEMENT" => "Announcement",
	"_ANNOUNCEMENTS" => "Announcements",
	"_ARTIST" => "Artist",
	"_ARTISTS" => "Artists",
	"_CLUB" => "Club/Project",
	"_CLUB1" => "Club",
	"_CLUBS" => "Clubs/Projects",
	"_CLUBS1" => "Clubs",
	"_COLLAB" => "Collab",
	"_COLLABS" => "Collabs",
	"_GIFTS" => "From Others",
	"_GIFTS_OUT" => "For Others",
	"_COMMENT" => "Comment",
	"_COMMENTS" => "Comments",
	"_DELETED_SUBMISSIONS" => "Deleted Submissions",
	"_DESCRIPTION" => "Description",
	"_DEVELOPER" => "Developer",
	"_EMOTICONS" => "Emoticons",
	"_FAVES" => "Faves",
	"_FAVOURITES" => "Favourites",
	"_FEATURE" => "Featured work",
	"_FEATURE1" => "Feature",
	"_FOLDER" => "Folder",
	"_FONT" => "Font",
	"_FRIENDS" => "Friends",
	"_GALLERY" => "Gallery",
	"_GALLERY_MANAGEMENT" => "Gallery Management",
	"_HOME" => "Home",
	"_ID" => "ID",
	"_JOURNAL" => "Journal",
	"_JOURNALS" => "Journals",
	"_KEYWORDS" => "Content details",
	"_MEMBERS" => "Members",
	"_MESSAGES" => "Messages",
	"_MODERATOR" => "Moderator",
	"_NEWS" => "News",
	"_PM" => "Private message",
	"_PMS" => "Private messages",
	"_POLL" => "Poll",
	"_PRIVATE" => "PM",
	"_PROJECT" => "Project",
	"_PROJECTS" => "Projects",
	"_RETIRED" => "Retired",
	"_STATS" => "Stats",
	"_SUBMISSION" => "Submission",
	"_SUBMISSIONS" => "Submissions",
	"_SUGGESTIONS" => "Suggestions",
	"_SUPERMODERATOR" => "Supermoderator",
	"_THUMBNAIL" => "Thumbnail",
	"_TOS" => "Terms of Service",
	"_USER" => "User",
	),
	
"helpdesk" => array(
	"_HELP_DESK" => "Helpdesk",
	"_HELP_DESK_DESC" => "Centralized user-to-staff interaction",
	"_HELP_MY_REQUESTS" => "My Requests",
	"_HELP_UNRESOLVED_REQUESTS" => "Unresolved Requests",
	"_HELP_RESOLVED_REQUESTS" => "Resolved Requests",
	"_HELP_ADD_REQUEST" => "Add a Request",
	"_HELP_SUMMARY" => "Summary",
	"_HELP_SUMMARY_DESC" => "A brief summary of the request.",
	"_HELP_CATEGORY" => "Category",
	"_HELP_REFERENCE_TO" => "In reference to",
	"_HELP_REFERENCE_ID" => "Object ID",
	"_HELP_STATUS" => "Current Status",
	"_HELP_STATUS_HOLD" => "On Hold",
	"_HELP_STATUS_PENDING" => "Pending", 
	"_HELP_STATUS_COMPLETED" => "Resolved",
	"_HELP_STATUS_WAIT_USER" => "Waiting on User",
	"_HELP_STATUS_WAIT_STAFF" => "Waiting on Staff",
	"_HELP_URGENCY" => "Urgency",
	"_HELP_DETAILS" => "Details",
	"_HELP_DETAILS_ADD" => "Add New Details",
	"_HELP_SUBMITTER" => "Request Submitter",
	"_HELP_RESOLUTION" => "Resolution Details"	
	),

"javascript" => array(
	"_JS_INVALID_FILE_TYPE" => "Invalid file type",
	"_JS_PLEASE_WAIT" => "Please wait...",
	"_JS_OEKAKI_NO_EDITOR" => "Please choose an editor first.",
	"_JS_PREV_FLASH_MOVIE" => "[b]Flash Movie[/b] (SWF): click [URL]here[/URL] for ".
		"preview.\n\nIt is impossible to preview the animation right now at its ".
		"original size, however, the final submission will be the size you have ".
		"set for the movie inside Macromedia Flash and will be cropped correctly.",
	"_JS_PREV_TEXT_FILE" => "[b]Text file[/b]: click [URL]here[/URL] for preview.",
	),

"join" => array(

	"_JOIN_ACCOUNT_NOT_CREATED" => "Account not created.",
	"_JOIN_BIRTHDATE_INVALID" => "The date you entered as your birthday is invalid.",
	"_JOIN_BIRTHDATE_TOO_YOUNG" => "You must be at least 18 years old to register.",
	"_JOIN_CREATE_ACCOUNT" => "Create an account",
	"_JOIN_EMAIL_EXISTS" => "The email address %s is already used.",
	"_JOIN_EMAIL_INVALID" => "The email address you have entered is invalid.",
	"_JOIN_EMAIL_DISALLOWED" => "The email address you have entered is from a blacklisted domain.".
									" Please use a real email address, not a disposable one.",
	"_JOIN_LOGIN" => "Please use your username and password to log in.",
	"_JOIN_PASSWORD_CONFIRM_EXPLAIN" => "type in the password once again",
	"_JOIN_PASSWORD_EXPLAIN" => "%d characters or more; the password is case-sensitive",
	"_JOIN_PASSWORD_MISMATCH" => "The passwords you entered do not match.",
	"_JOIN_PASSWORD_TOO_SHORT" => "Password is too short (minimum %d characters).",
	"_JOIN_SUCCESS" => "Your account has been created",
	"_JOIN_UNDERSTOOD_TOS" => "I have read and agree to abide by the Terms of Service",
	"_JOIN_USERNAME_ALPHANUMERIC" => "Username should only consist of letters and numbers.",
	"_JOIN_USERNAME_EXISTS" => "User \"%s\" already exists.",
	"_JOIN_USERNAME_EXPLAIN" => "only letters and digits can be used; you will not be able ".
		"to modify it later",
	"_JOIN_USERNAME_TOO_LONG" => "Username is too long. Maximum %u characters allowed.",
	"_JOIN_USERNAME_TOO_SHORT" => "Username is too short. Minimum %u characters allowed.",
	"_JOIN_INVITATION_EXPLAIN" => "ask a friend who already has an account to send you the ".
		"code, it can be found under Settings > Interaction",
	"_JOIN_INVITATION_INVALID" => "Invalid invitation code.",
	),

"journals" => array(

	"_JOURNAL_DEFAULT_TITLE" => "Journal entry",
	"_JOURNAL_DELETE_ENTRY" => "Delete entry",
	"_JOURNAL_EDIT_ENTRY" => "Edit entry",
	"_JOURNAL_LEAVE_COMMENT" => "Comment on this entry",
	"_JOURNAL_NEW_ENTRY" => "New entry",
	"_JOURNAL_NONE_FOUND" => "No journal entries found.",
	"_JOURNAL_PAST_ENTRIES" => "Past journal entries",
	"_JOURNAL_POST_ENTRY" => "Post entry",
	"_ANNOUNCEMENT_PAST_ENTRIES" => "Past announcements",
	"_ANNOUNCEMENT_NONE_FOUND" => "No announcements found.",
	"_ANNOUNCEMENT_DEFAULT_TITLE" => "Announcement",
	),

"keywords table edition" => array(

	"_KEYWORDS_CHOSEN" => "Chosen content details",
	"_KEYWORDS_DELETE" => "Delete keyword",
	"_KEYWORDS_EDIT" => "Edit keyword",
	"_KEYWORDS_HINT" => "All characters entered before the \"|\" symbol in the keyword ".
		"name will not be displayed. For example, keyword \"123|Some Keyword\" will display ".
		"just as \"Some Keyword\". Please use it if you need a proper sorting of the keywords.\n\n".
		"Symbol @ at the end of the keyword name makes this keyword non-selectable.",
	"_KEYWORDS_SUBTITLE" => "Edit the Keywords table",
	),

"lost password retrieval" => array(

	"_LOSTPASS_TITLE" => "Lost password retrieval",
	"_LOSTPASS_USERNAME" => "Enter your username",
	),

"main page" => array(

	"_MAIN_SUBTITLE" => "The most recent submissions",
	),

"months" => array(

	"_MONTH1" => "January",
	"_MONTH10" => "October",
	"_MONTH11" => "November",
	"_MONTH12" => "December",
	"_MONTH2" => "February",
	"_MONTH3" => "March",
	"_MONTH4" => "April",
	"_MONTH5" => "May",
	"_MONTH6" => "June",
	"_MONTH7" => "July",
	"_MONTH8" => "August",
	"_MONTH9" => "September",
	),

"news" => array(

	"_NEWS_POST" => "Post news",
	"_NEWS_SUBJECT" => "Subject",
	),

"oekaki" => array(

	"_OEKAKI" => "Oekaki",
	"_OEKAKI_ANIM_WARNING" => "IMPORTANT: If you want to preserve the animation, you ".
		"have to edit using [b]the same editor[/b] in which the picture was drawn first. ".
		"If you run the editor and can't see your picture, please close it and choose ".
		"the correct editor, or disable the Record Animation option (if you want to use ".
		"another editor; you will lose the animation).",
	"_OEKAKI_APPLETS" => "Choose your favourite editor",
	"_OEKAKI_CONTINUE_ANIMATION" => "Record Animation (continue)",
	"_OEKAKI_EXPLAIN" => "When you are done with the drawing, click the Upload/Send/Save button.",
	"_OEKAKI_HELP_OEKAKIBBS" => "To enter FULLSCREEN MODE, maximize the editor's pop-up window.\nWhen you are finished with the drawing, click the \"SAVE PNG\" button under the OekakiBBS banner.",
	"_OEKAKI_HELP_PAINTBBS" => "To enter FULLSCREEN MODE, click the \"F\" button in the top-left corner.\nWhen you are finished with the drawing, click the \"Send\" button on the bottom-left.",
	"_OEKAKI_HELP_SHI" => "To enter FULLSCREEN MODE, use the \"Float\" button in the top-left corner.\nWhen you are done with the drawing, click the \"Upload\" button on the top-left.",
	"_OEKAKI_NO_JAVA" => "Your browser does not support Java. [url=%s]Get the latest ".
		"Java plugin here.[/url]",
	"_OEKAKI_OPTIONS" => "Options",
	"_OEKAKI_PIXELS" => "pixels",
	"_OEKAKI_RESOLUTION" => "Canvas size",
	"_OEKAKI_RUN" => "Run Editor",
	"_OEKAKI_SAVE_ANIMATION" => "Record Animation",
	"_OEKAKI_SUBTITLE" => "Start a new Oekaki drawing from this page",
	"_OEKAKI_SUBTITLE_EDIT" => "Edit your Oekaki drawing",
	"_OEKAKI_UNKNOWN_EDITOR" => "Unable to recognize the editor that was used.",
	"_OEKAKI_LOGOUT" => "PLEASE DO NOT LEAVE THIS PAGE! You have been logged out during ".
		"your Oekaki session. Please use the following form to log in and continue submission ".
		"of your Oekaki drawing.",
	),

"page not found" => array(

	"_PAGENOTFOUND_MESSAGE" => "Please check your target location and try again, or ".
		"visit the [url=%s]main page[/url].",
	"_PAGENOTFOUND_SUBTITLE" => "The page you are searching for has not been found on ".
		"the server or you have no access rights to view it.",
	"_PAGENOTFOUND_TITLE" => "Page not found/Access denied",
	),

"polls" => array(

	"_POLL_DELETE_ENTRY" => "Delete poll",
	"_POLL_EDIT_ENTRY" => "Edit poll",
	"_POLL_EXPIRE" => "Expires in",
	"_POLL_EXPIRE_UNITS" => "days",
	"_POLL_LEAVE_COMMENT" => "Comment on this poll",
	"_POLL_NEW_ENTRY" => "New poll",
	"_POLL_NONE_FOUND" => "No polls found.",
	"_POLL_PAST_ENTRIES" => "Past polls",
	"_POLL_POST_ENTRY" => "Post New Poll",
	),

"private messages" => array(

	"_PM_BLANK_MESSAGE" => "Please enter the message first.",
	"_PM_COMPOSE" => "Compose message for %s",
	"_PM_FOR" => "for",
	"_PM_FROM" => "from",
	"_PM_NONE_FOUND" => "No messages found.",
	"_PM_NOT_FOUND" => "Message not found.",
	"_PM_NO_ACCESS" => "You cannot view this PM.",
	"_PM_RECEIVED_MESSAGE" => "Message you have received",
	"_PM_RECEIVED_MESSAGES" => "Received messages",
	"_PM_SEND" => "Send PM",
	"_PM_SEND_TO" => "Send PM to",
	"_PM_SENT_MESSAGE" => "Message you have sent",
	"_PM_SENT_MESSAGES" => "Sent messages",
	"_PM_SUBTITLE" => "Read and write private messages",
	"_PM_UNTITLED" => "Message",
	),

"search engine" => array(

	"_SEARCH_HINT" => "Hint: If you enter \"word*\" (without brackets) it will ".
		"search for all words beginning with \"word\".",
	"_SEARCH_HINT2" => "You may search for content details, perform a text search, ".
		"or combine both kinds of search.",
	"_SEARCH_KEYWORDS_EXPLAIN" => "Click on the following tabs and choose the content ".
		"details you want to search for:",
	"_SEARCH_SUBTITLE" => "Submission search engine",
	"_SEARCH_TEXT" => "Text to search for",
	"_SEARCH_RELEVANT" => "Relevant",
	),

"settings" => array(

	"_SET_AVATAR_CURRENT" => "Current avatar icon",
	"_SET_AVATAR_EXPLAIN" => "(leave blank if case you do not wish to change the avatar)",
	"_SET_AVATAR_NEW" => "New avatar icon",
	"_SET_AVATAR_SIZE_EXCEEDED" => "Avatar icon must be less than %u bytes.",
	"_SET_AVATAR_TOO_LARGE" => "Avatar icon must be between %s and %s pixels in size and ".
		"must be a GIF, JPG, or PNG image file.",
	"_SET_AVATAR_UPLOADED" => "Avatar icon uploaded.",
	"_SET_ID_FILE_EXPLAIN" => "ID must be a GIF, JPG, or PNG image file. It can also be a ".
		"Flash animation with the size limit of %u bytes. ID will be placed at the beginning ".
		"of your profile at your home page.",
	"_SET_COMMENTS_NOTIFY_FAV" => "Send email when art is +fav'd by default",
	"_SET_COMMENTS_NOTIFY_OTHER" => "Send email on all other actions by default",
	"_SET_COMMENTS_NO_BBCODE" => "No BBCode in all posts by default",
	"_SET_COMMENTS_NO_EMOTICONS" => "No Emoticons in all posts by default",
	"_SET_COMMENTS_NO_SIG" => "No Signature in all posts by default",
	"_SET_COMMENTS_SAVED" => "Commenting settings saved.",
	"_SET_CONTACTS" => "Contacts",
	"_SET_CONTACTS_AIM" => "AIM",
	"_SET_CONTACTS_ICQ" => "ICQ",
	"_SET_CONTACTS_MSN" => "MSN",
	"_SET_CONTACTS_YIM" => "YIM",
	"_SET_CONTACTS_JABBER" => "Jabber",
	"_SET_CUSTOM_TITLE" => "Custom title",
	"_SET_DATE_FORMAT" => "Date format",
	"_SET_FEATURE_CHANGED" => "Feature work changed to \"%s\"",
	"_SET_FEATURE_CLEARED" => "Feature work removed.",
	"_SET_FEATURE_EXPLAIN" => "Note: Feature work containing mature content will not be ".
		"visible to underage/unregistered users.",
	"_SET_FUZZY" => "Use fuzzy numbers (\"a few\", \"many\", etc. instead of numbers)",
	"_SET_GENERAL" => "General",
	"_SET_GENERAL_SAVED" => "General settings saved.",
	"_SET_HIDDEN" => "Hide online status",
	"_SET_ID_CHANGED" => "ID changed to \"%s\".",
	"_SET_ID_CLEARED" => "ID removed.",
	"_SET_ID_EXPLAIN" => "Note: IDs containing mature content will not be visible to ".
		"underage/unregistered users.",
	"_SET_LANGUAGE" => "Language",
	"_SET_PERSONAL_DATA" => "Personal data",
	"_SET_PROFILE_CURRENT" => "Current profile",
	"_SET_PROFILE_NEW" => "Edit profile",
	"_SET_PROFILE_SAVED" => "Profile saved.",
	"_SET_SIG_CURRENT" => "Current signature",
	"_SET_SIG_NEW" => "Edit signature",
	"_SET_SIDEBAR_CURRENT" => "Current sidebar",
	"_SET_SIDEBAR_NEW" => "New sidebar",
	"_SET_SIDEBAR_EXPLAIN" => "This is some text for your own personal use, displayed ".
		"on the right side of the layout. It is only viewable by yourself. For example, ".
		"here you may write down various reminders (such as your to-do list), put quick ".
		"links to locations that you visit often, or put a list of your best friends ".
		"using [u=[i]username[/i]] to be aware of their online status.",
	"_SET_SIDEBAR_SAVED" => "Sidebar settings saved.",
	"_SET_SIDEBAR_THUMBS" => "Show the Most Recent and Random panels",
	"_SET_SIGNATURE_LIMIT" => "Signatures must be less than 1000 characters in length and contain no more than 5 linebreaks.",
	"_SET_SPECIAL_SAVED" => "Special settings saved.",
	"_SET_SUBTITLE" => "Manage your settings",
	"_SET_THEME" => "Theme",
	"_SET_TIMEZONE" => "Time zone",
	"_SET_OBJ_PREVIEW" => "Prefer viewing submissions in normal quality (smaller files)",
	"_SET_EXPORT_IMPORT" => "Export/Import",
	"_SET_EXPORT" => "Export your settings",
	"_SET_EXPORT_EXPLAIN" => "Export settings into a file on your computer. This would ".
		"help you to transfer your settings to compatible galleries.",
	"_SET_EXPORT_BUTTON" => "Save all settings into a file",
	"_SET_IMPORT" => "Import your settings",
	"_SET_IMPORT_EXPLAIN" => "Import settings from a file that was previously stored ".
		"on your computer.",
	"_SET_IMPORT_BUTTON" => "Import all settings from the selected file",
	"_SET_STATS_PUBLIC" => "Make my account statistics public",
	"_SET_STATS_HIDE" => "Do not show statistics (number of favs, pageviews, etc.)",
	"_SET_GUEST_ACCESS" => "Let guests (unregistered users) access my account",
	"_SET_NOTIFY_WATCH" => "Notify me about other users adding me to their watchlist",
	"_SET_NOTIFY_FAVS" => "Notify me when other users add my works to their favourites",
	"_SET_SITE" => "Site",
	"_SET_SITE_SETTINGS" => "Site settings",
	"_SET_PROFILE_SETTINGS" => "Profile settings",
	"_SET_INTERACTION" => "Interaction",
	"_SET_INTERACTION_SETTINGS" => "Interaction settings",
	"_SET_SIDEBAR_SETTINGS" => "Sidebar settings",
	"_SET_FOLDERS" => "Folders",
	"_SET_FOLDER_SETTINGS" => "Folder settings",
	"_SET_DESIGN_CUSTOM_THEME" => "Design your own custom theme",
	"_SET_THEME_OPTION1" => "Disable custom themes",
	"_SET_THEME_OPTION2" => "Use custom themes on pages where they are present",
	"_SET_THEME_OPTION3" => "Use custom themes but use my own if some page does not contain a custom theme",
	"_SET_THEME_OPTION4" => "Always use my custom theme on ALL pages",
	"_SET_GENERAL_SETTINGS" => "General settings",
	"_SET_FILTER_INTRO" => "What content would you rather not see on this site?",
	"_SET_FILTER_EXPLAIN" => "Click here for the explanations",
	"_SET_THEME_INTRO" => "Pick one of the standard themes by clicking on the icons below.",
	"_SET_THEME_CUSTOM_INTRO" => "Users with one or more submissions may also have their own ".
		"custom themes on their pages. Would you prefer custom themes above the standard ones?",
	"_SET_SAVED" => "Settings saved.",
	"_SET_SHOW_EMAIL" => "Show email address publicly",
	"_SET_SHOW_REAL_NAME" => "Show real name publicly",
	"_SET_MESSENGERS" => "Messengers",
	"_SET_TWIT_LIST" => "Ignore List",
	"_SET_TWIT_EXPLAIN" => "Comments and PMs made by malicious/annoying users listed above will be ".
		"removed from your view.",
	"_SET_TWIT_EXAMPLE" => "A list of usernames, for example: \"badUser, nettroll123\".",
	"_SET_IGNORE" => "Ignore",
	"_SET_SIGNATURE" => "Signature",
	"_SET_FOLDER_CREATE" => "Create New Folder",
	"_SET_ICON_SIZE_EXCEEDED" => "Folder icon must be less than %u bytes.",
	"_SET_ICON_TOO_LARGE" => "Folder icon must be exactly %s pixels in size and must be a ".
		"GIF, JPG, or PNG image file.",
	"_SET_FOLDER_EXISTS" => "The new folder title conflicts with one of the previously ".
		"created folders. Please change your new folder title and try again.",
	"_SET_FOLDER_CREATED" => "Folder \"%s\" has been created",
	"_SET_FOLDER_EDIT" => "Edit Folder",
	"_SET_FOLDER_EDITED" => "Folder \"%s\" has been edited",
	"_SET_FOLDER_REMOVE_CONFIRM" => "You are going to remove folder \"%s\".",
	"_SET_FOLDER_REMOVE_EXPLAIN" => "[b]Note[/b]: Removing a folder would not delete ".
		"any submissions from your gallery, they will be just moved out of the deleted folder.",
	"_SET_FOLDER_BACK" => "Upper level",
	"_SET_FOLDERS_OFF" => "Folders OFF",
	"_SET_FOLDERS_ON" => "Folders ON",
	"_SET_THEME_DESIGNER" => "Theme Designer",
	),

"sidebar" => array(
	"_SIDEBAR" => "Sidebar",
	),

"submission" => array(

	"_SUBMIT_TO_FOLDER" => "Move submission to this folder inside my gallery",
	"_SUBMIT_FOR_CLUB" => "Move submission to one or more clubs/projects",
	"_SUBMIT_COLLAB" => "This submission is a collaborative work with",
	"_SUBMIT_GIFT" => "This submission was made for",
	"_SUBMIT_KEYWORDS_EXPLAIN" => "Describe your artwork by marking the keywords for ".
		"better categorisation of your artwork. [b]If there is a keyword that would ".
		"help better categorise your art that is missing from our system, please post ".
		"[url=%s]here[/url].[/b]",
	"_SUBMIT_MATURE_NO" => "Submission does not contain mature content",
	"_SUBMIT_MATURE_YES" => "Submission contains mature content",
	"_SUBMIT_NO_COMMENT" => "Comment is required.",
	"_SUBMIT_NO_MATURE" => "Mature rating not specified.",
	"_SUBMIT_NO_TITLE" => "Title is required.",
	"_SUBMIT_OEKAKI" => "Oekaki drawing you are submitting",
	"_SUBMIT_OEKAKI_ANIM" => "Note: Animation is also included.",
	"_SUBMIT_OEKAKI_EXPLAIN" => "Thank you for your drawing. Once the following information has been entered, your drawing will be submitted.",
	"_SUBMIT_REQUIRE_KEYWORDS" => "You need to choose at least one keyword in the following ".
		"groups (click on the tabs above):",
	"_SUBMIT_SELECT_FILE" => "Select your file please.",
	"_SUBMIT_SUBTITLE0" => "Select submission type",
	"_SUBMIT_SUBTITLE1" => "I. Describe your artwork",
	"_SUBMIT_SUBTITLE2" => "II. Categorise your artwork",
	"_SUBMIT_SUBTITLE3" => "III. Preview your submission",
	"_SUBMIT_SUBTITLE4" => "IV. Upload submission files",
	"_SUBMIT_THUMBNAIL_ERROR" => "Thumbnail has been uploaded incorrectly. Please re-upload.",
	"_SUBMIT_THUMBANIL_DEFAULT" => "Use Default Thumbnail",
	"_SUBMIT_TITLE" => "Submit your artwork",
	"_SUBMIT_WAIT" => "Please wait %u seconds before proceeding further with the submission.",
	"_SUBMIT_THUMB_REQUIRED" => "Thumbnail is required for this type of submission. ".
		"Please re-upload.",
	"_SUBMIT_THUMB_NOT_REQUIRED" => "Thumbnail is not required for JPG (JPEG) or PNG files.",
	"_SUBMIT_THUMB_ERROR" => "Thumbnail must be a JPG or PNG image. Please re-upload.",
	"_SUBMIT_TYPE_IMAGE" => "Image or Animation",
	"_SUBMIT_TYPE_IMAGE_EXPLAIN" => "This will allow you to submit an image in the JPG ".
		"(JPEG), GIF or PNG image format, or an animation in the Flash (SWF) format.",
	"_SUBMIT_TYPE_LIT" => "Literary work",
	"_SUBMIT_TYPE_LIT_EXPLAIN" => "This option will allow you to easily format and edit ".
		"your textual submission using the same interface that you use when writing ".
		"comments.",
	"_SUBMIT_TYPE_EXTRA" => "Extras",
	"_SUBMIT_TYPE_EXTRA_EXPLAIN" => "This will allow you to make a submission of an image ".
		"which is stored on your own server. The following [url=/tos]Terms of Service[/url] ".
		"rules do not apply to works submitted as Extras: %s. You can submit an image in the ".
		"JPG (JPEG), GIF or PNG image format, or an animation in the Flash (SWF) format.",
	"_HIGH_QUALITY" => "View in High Quality",
	"_FULL_WINDOW" => "Full Window",
	"_SUBMIT_DISABLED_FILTERS" => "You cannot make submissions containing this, as you have ".
		"chosen to disable this content in your Settings.",
	"_SUBMIT_CANNOT_CONTINUE" => "Cannot continue, read above",
	),

"updates" => array(

	"_UPD_COMMENTS_PAGE" => "Comments page",
	"_UPD_COMMENT_ON" => "On %s",
	"_UPD_EXPLAIN" => "Manage your updates and messages",
	"_UPD_JOURNAL" => "%s's journal: %s",
	"_UPD_ANNOUNCEMENT" => "%s announcement: %s",
	"_UPD_MESSAGE_ABUSE" => "%s has been reported, and is hidden pending abuse moderation. Please see [faq=31] ".
		"More information will be provided to you upon conclusion of the investigation.",
	"_UPD_MESSAGE_CLUB" => "You are now a member of club %s.",
	"_UPD_MESSAGE_CLUB_DECLINED" => "You have been declined as a member of club %s.",
	"_UPD_MESSAGE_CLUB_MEMBER" => "%s has joined the club %s.",
	"_UPD_MESSAGE_CLUB_MEMBER_PENDING" => "%s wanted to join the club %s.",
	"_UPD_MESSAGE_CLUB_REFUSED" => "Submission %s has been refused by the owner of club %s ".
		"and moved back to your main gallery.",
	"_UPD_MESSAGE_DELETED" => "%s has been marked as a violation after abuse moderation. ".
		"Please check your PMs for more details about the decision and what action was taken.",
	"_UPD_MESSAGE_FAV" => "%s has added %s to their favourites.",
	"_UPD_MESSAGE_HELPDESK_RESOLVED" => "Helpdesk case %s has been resolved.",
	"_UPD_MESSAGE_HELPDESK_UPDATED" => "Helpdesk case %s has been updated.",
	"_UPD_MESSAGE_RESTORED" => "%s has been restored after abuse moderation.",
	"_UPD_MESSAGE_WATCH" => "%s has added you to their friends list.",
	"_UPD_MESSAGE_WATCH_CLUB" => "%s has added club %s to their watch list.",
	"_UPD_NO_COMMENTS" => "No new comments at the moment.",
	"_UPD_NO_JOURNALS" => "No new journals/polls at the moment.",
	"_UPD_NO_MESSAGES" => "No new messages at the moment.",
	"_UPD_NO_SUBMISSIONS" => "No new submissions at the moment.",
	"_UPD_POLL" => "%s's poll: %s",
	"_UPD_BY_DATE" => "By Date",
	"_UPD_BY_ARTISTS" => "By Artists",
	"_UPD_ART" => "Art Updates",
	"_UPD_ART_EXPLAIN" => "Latest updates from artists and clubs on your watch list",
	),

"user authentication" => array(

	"_USE_ACTIVATE_BODY" => "Please click on the following link or copy and paste it into your Web browser to activate your account:\n\n%s",
	"_USE_ACTIVATE_CHANGE" => "Click here if you have entered an incorrect email address",
	"_USE_ACTIVATE_NOT_FOUND" => "Wrong username specified or the user account is already ".
		"activated.",
	"_USE_ACTIVATE_RESEND" => "Click here if you have not received the email",
	"_USE_ACTIVATE_RESEND_TITLE" => "Sending the email",
	"_USE_ACTIVATE_SUBJ" => "Welcome to %s",
	"_USE_BANNED" => "This user account has been banned.",
	"_USE_CHANGE_EMAIL_NO_PERMISSION" => "You do not have permission to change this account's ".
		"email address.",
	"_USE_CHANGE_PASSWORD_NO_PERMISSION" => "You do not have permission to change this ".
		"account's password.",
	"_USE_EMAIL_CHANGE" => "Change your email address",
	"_USE_EMAIL_NEW_ADDRESS" => "New email address",
	"_USE_ENTER_NEW_PASSWORD" => "Enter new password",
	"_USE_ENTER_NEW_PASSWORD_CONFIRM" => "Re-type password to confirm",
	"_USE_INACTIVE" => "You need to click on the link sent via email to %s in order to ".
		"activate your account.",
	"_USE_LOGIN_ERROR" => "This username/password combination is not valid.",
	"_USE_LOSTPASS" => "Lost password",
	"_USE_LOSTPASS_BODY" => "Someone has requested that your password be reset! Do NOT ".
		"click the following link if it was not you who requested the password be changed. ".
		"Otherwise, please click on the link below to proceed with lost password resetting:\n\n%s",
	"_USE_LOSTPASS_SUBJ" => "Lost password for %s",
	"_USE_PASSWORD_CHANGE" => "Change your password",
	"_USE_PASSWORD_CHANGED" => "Your password has been changed successfully. Please log in ".
		"using your new password.",
	"_USE_REMEMBER" => "Remember me",
	"_USE_SUSPENDED_UNTIL" => "This account has been suspended until %s.",
	),

"view" => array(

	"_VIEW_NOT_FOUND" => "Image file has not been found on the server.",
	"_VIEW_TITLE" => "Artwork",
	),

"watches" => array(

	"_WATCH" => "Watch",
	"_WATCH_CLUB_START" => "Watch this club for updates",
	"_WATCH_CLUB_STOP" => "Stop watching this club",
	"_WATCH_LIST_EMPTY" => "Your watch list is currently empty.",
	"_WATCH_REMOVE" => "Remove this artist (club/project) from your watch list:",
	"_WATCH_SEND_EMAIL" => "Send email when new art is released by this artist (club/project):",
	"_WATCH_START" => "Watch this user for updates",
	"_WATCH_STOP" => "Stop watching",
	"_WATCH_SUBTITLE" => "Manage your watch list",
	"_WATCH_UPDATED" => "Watch list updated.",
	),

"extras" => array(

	"_EXTRA_SUBMIT" => "Submit Extras",
	"_EXTRA_SUBMIT_EXPLAIN" => "Submit an image located at your own server",
	"_EXTRA_NO_THUMB" => "Thumbnail URL is required",
	"_EXTRA_NO_PREVIEW" => "Normal version URL is required",
	"_EXTRA_NO_FULL" => "Full version URL is required",
	"_EXTRA_BROWSE" => "Browse Extras",
	"_EXTRA_LEGAL" => "The following [url=/tos]Terms of Service[/url] ".
		"rules do not apply to works submitted as Extras: %s.",
	"_EXTRA_RATIO_EXPLAIN" => "You need %d more submissions before you can submit another extra.",
	),
);

$result = sql_query( "SELECT * FROM `filters`" );

while( $fltData = mysql_fetch_assoc( $result ))
{
	$strings[ "filters" ][ "_FILTER_".$fltData[ "fltid" ]] = $fltData[ "fltName" ];
}

// -----------------------------------------------------------------------------
// Update the strings in the database.

foreach( $strings as $category => $strArray )
{
	foreach( $strArray as $key => $value )
	{
		$result = sql_query( "SELECT `strid` FROM `strings` ".
			"WHERE `strLanguage` = 'en' AND `strName` = '$key' LIMIT 1" );

		if( $strData = mysql_fetch_row( $result ))
		{
			// The string is already in the database.

			$strid = $strData[ 0 ];

			$result = sql_query( "SELECT COUNT(*) FROM `strings` WHERE `strid` = '$strid' ".
				"AND `strText` = '".addslashes( $value )."' LIMIT 1" );

			if( mysql_result( $result, 0 ) > 0 )
			{
				// The string has not changed, so we don't do anything.

				continue;
			}
			else
			{
				// The string has changed, so we need to update the string in
				// the database and touch the last modification time.

				sql_query( "UPDATE `strings` SET `strText` = '".addslashes( $value )."', ".
					"`strLastModified` = NOW(), `strCategory` = '".addslashes( $category )."' ".
					"WHERE `strid` = '$strid' LIMIT 1" );
			}
		}
		else
		{
			// The string is new, we need to insert it.

			sql_query( "INSERT INTO `strings`(`strLanguage`,`strName`,`strText`,".
				"`strLastModified`,`strCategory`) ".
				"VALUES('en','".addslashes( $key )."','".addslashes( $value )."',".
				"NOW(),'".addslashes( $category )."')" );
		}
	}
}

redirect( url( "updatestrings" ));

?>
