<?

$viewOK = false;
$active = 4;
include( INCLUDES."mod_artmenu.php" );

if( !$viewOK )
{
	return;
}

if( !atLeastHelpdesk() && $_auth[ "useid" ] != $objData[ "objCreator" ])
{
	include( INCLUDES."p_notfound.php" );
	return;
}

?>
<div class="container">
	<?

	$clear = false;

	if( atLeastSModerator() )
	{
		// Supermoderator or admin can Reset edits.

		$urlResetEdits = url( "editsm/".( $isExtras ? "e" : "" ).$objid, array( "edit" => "resetedits" ));

		?>
		<div class="mar_right mar_bottom button" style="float : left"
			onclick="document.location='<?= $urlResetEdits ?>'">
			<?= sprintf( _EDIT_RESET_EDITS, $objData[ "objNumEdits" ]) ?>
		</div>
		<?

		$clear = true;
	}

	if( $objData[ "objDeleted" ] && ( isAdmin() ||
		$objData[ "objDeletedBy" ] == $_auth[ "useid" ]))
	{
		// Admin or Artist can Undelete a submission in case it was previously
		// deleted. Artist can only undelete submission if it was not deleted
		// by moderation.

		$urlUndelete = url( "editsm/".( $isExtras ? "e" : "" ).$objid, array( "edit" => "undelete" ));

		?>
		<div class="mar_right mar_bottom button" style="float : left"
			onclick="document.location='<?= $urlUndelete ?>'">
			<?= _EDIT_UNDELETE ?>
		</div>
		<?

		$clear = true;
	}

	if( isAdmin() )
	{
		// Admin can Erase any submission; this will delete all submission's
		// files and remove it completely from the database.

		$urlErase = url( "editsm/".( $isExtras ? "e" : "" ).$objid, array( "edit" => "erase" ));

		?>
		<div class="mar_right mar_bottom button" style="float : right"
			onclick="if(confirm('<?= _ARE_YOU_SURE ?>')) document.location='<?= $urlErase ?>'">
			<?= _EDIT_ERASE ?>
		</div>
		<?

		$clear = true;
	}

	if( $clear )
	{
		?>
		<div class="clear" style="margin-bottom: 5px">&nbsp;</div>
		<?
	}

	$found = false;

	ob_start();

	?>
	<div class="sep caption"><?= _INFORMATION ?>:</div>
	<div class="container2 notsowide">
		<?

		// Abuse/deletion display.

		if( $objData[ "objDeleted" ] && ( atLeastHelpdesk() ||
			$objData[ "objDeletedBy" ] == $_auth[ "useid" ]))
		{
			$found = true;

			if( $objData[ "objDeletedBy" ] != 0 )
			{
				// If the submission is deleted and deletedBy is set to
				// non-zero, the submission has been deleted by a person.

				?>
				<div>
					<?= _DELETED_BY ?>:
					<?= getUserLink( $objData[ "objDeletedBy" ]) ?>
				</div>
				<?
			}
			else
			{
				// If the submission is deleted but deletedBy is 0, this
				// means it's been deleted by moderation (i.e. a group of
				// moderators).

				$result = sql_query( "SELECT * FROM `abuses`".dbWhere( array(
					"abuObj" => $objData[ "objid" ],
					"abuIsExtras" => $isExtras ))."ORDER BY `abuSubmitDate` DESC LIMIT 1" );

				if(( !$abuData = mysql_fetch_assoc( $result )) ||
					!$abuData[ "abuResolved" ])
				{
					?>
					<div>
						<?= _DELETED_BY ?>:
						<?= _ABUSE_DECISION_NONE ?>
					</div>
					<?
				}
				else
				{
					?>
					<div>
						<?= _DELETED_BY ?>:
						<?= _MODERATION ?>
						(<a href="<?= url( "abuse/".$abuData[ "abuid" ]) ?>"><?= $abuData[ "abuid" ] ?></a>)
					</div>
					<div>
						<?= _REPORTED_BY ?>:
						<?= getUserLink( $abuData[ "abuSubmitter" ]) ?>
					</div>
					<div>
						<?= _MODERATOR ?>:
						<?= getUserLink( $abuData[ "abuModName" ]) ?>
						(<?= $abuData[ "abuMod" ] ?>)
					</div>
					<div>
						<?= _SUPERMODERATOR ?>:
						<?= getUserLink( $abuData[ "abusModName" ]) ?>
						(<?= $abuData[ "abusMod" ] ?>)
					</div>
					<?

					if( $abuData[ "aburMod" ] != "?" )
					{
						?>
						<div>
							<?= _ADMINISTRATOR ?>:
							<?= getUserLink( $abuData[ "aburModName" ]) ?>
							(<?= $abuData[ "aburMod" ] ?>)
						</div>
						<?
					}

					?>
					<div>
						<?= _FOR_REASON ?>:
						<?= formatText( $abuData[ "abuReason" ]) ?>
					</div>
					<div><br /></div>
					<?
				}
			}
		}

		$statsPublic = $useData[ "useid" ] != $_auth[ "useid" ] ?
			$useData[ "useStatsPublic" ] : true;

		if( isLoggedIn() &&
			$objData[ "objCollab" ] == $_auth[ "useid" ] &&
			$objData[ "objCollabConfirmed" ])
		{
			$statsPublic = true;
		}

		// Show filename and collab status to HDStaff+ and IPs to Moderators+

		if( atLeastHelpdesk() )
		{
			?>
			<div>
				
				<?
				if( $objData[ "objCollab" ] > 0) {
					?>	
					<div>
						Collaboration With: <?= getUserLink( $objData[ "objCollab" ] )?>
						<br />
						Collaboration Status: <?= ($objData[ "objCollabConfirmed"] ? "Confirmed" : "Unconfirmed") ?>
					</div>
					<?
				}
				if( !$isExtras )
				{
					?>
					<div>
						<?= _FILE ?>:
						<?= htmlspecialchars( stripslashes( $objData[ "objFilename" ]) ) ?>
					</div>
					<?
				}
				if(atLeastModerator() ) {
				?>
				<div>
					Submitted from IP: <?= getDotDecIp( $objData[ "objSubmitIp" ]) ?>
					<br />
					Last edited from IP: <?= getDotDecIp( $objData[ "objEditIp" ]) ?>
				</div>
				<? } ?>
			</div>
			<?

			$found = true;
		}

		?>
	</div>
	<?

	if( $found )
		ob_end_flush();
	else
		ob_end_clean();

	if(( atLeastHelpdesk() || $useData[ "useid" ] == $_auth[ "useid" ]))
	{
		// The creator of this submission and also HDStaff (and higher)
		// are allowed to edit the submission's details: title, comment,
		// mature rating, club/project belonging, keywords, files, and also
		// delete the submission.
		//
		// Note: Only the creator or a moderator/supermoderator can edit submission's
		// files. HDStaff can only edit keywords. Only the creator/supermod can delete/undelete.
		// Admin can purge, which involves permanently deleting the file. 

		?>
		<div class="sep caption">
			<?= _EDIT_SUBMISSION ?>:
		</div>
		<div class="container2 notsowide">
			<?

			if( $objData[ "objDeleted" ] && ( isAdmin() ||
				$objData[ "objDeletedBy" ] == $_auth[ "useid" ]))
			{
				$urlUndelete = url( "editsm/".( $isExtras ? "e" : "" ).$objid,
					array( "edit" => "undelete" ));

				?>
				<a href="<?= $urlUndelete ?>"><?= _EDIT_UNDELETE ?></a>
				<?
			}
			else
			{
				if( $isExtras )
				{
					?>
					<a href="<?= url( "extrasubmit/".$objid ) ?>">
						<?= _TITLE."/"._COMMENT.", "._FILTERS.", "._FOLDER.", "._FILE."/"._THUMBNAIL ?></a>
					<?

					if( atLeastSModerator() || $useData[ "useid" ] == $_auth[ "useid" ])
					{
						if( !$objData[ "objDeleted" ])
						{
							?>
							&middot;
							<a onclick="return confirm('<?= _ARE_YOU_SURE ?>')"
								href="<?= url( "edit/".( $isExtras ? "e" : "" ).$objid, array( "edit" => "delete" )) ?>">
								<?= _DELETE ?></a>
							<?
						}
					}
				}
				else
				{
					if( atLeastModerator() || $useData[ "useid" ] == $_auth[ "useid" ]) {
					?>
					
						<div>
							<a href="<?= url( "edit/".( $isExtras ? "e" : "" ).$objid, array( "edit" => "title" )) ?>">
								<?= _TITLE."/"._COMMENT.", "._FOLDER.", "._CLUB ?></a>
						</div>
						<div class="mar_top">			
					<? } else { ?>
						<div>
					<? }?>
					
						<a href="<?= url( "edit/".( $isExtras ? "e" : "" ).$objid, array( "edit" => "keywords" ))?>">
							<?= _KEYWORDS ?></a>
					</div>
					<?

					if( atLeastModerator() || $useData[ "useid" ] == $_auth[ "useid" ])
					{
						$oekakiSign = "/oekaki/";

						if( !$isExtras &&
							substr( $objData[ "objFilename" ], 0, strlen( $oekakiSign )) == $oekakiSign )
						{
							?>
							<div class="mar_top">
								<a href="<?= url( "oekaki/".$objid ) ?>"><?= _OEKAKI ?></a>
							</div>
							<?
						}
						else
						{
							?>
							<div class="mar_top">
								<a href="<?= url( "edit/".( $isExtras ? "e" : "" ).$objid, array( "edit" => "file" )) ?>">
									<?= ( $objData[ "objExtension" ] == "txt" ? _TEXT : _FILE ).
										"/"._THUMBNAIL ?></a>
							</div>
							<?
						}
						if(atLeastSModerator() || $useData[ "useid" ] == $_auth[ "useid" ] && $objData[ "objPendingUser" ] != "1")
						{
							if( !$objData[ "objDeleted" ])
							{
								?>
								<div class="mar_top">
									<a onclick="return confirm('<?= _ARE_YOU_SURE ?>')"
										href="<?= url( "edit/".( $isExtras ? "e" : "" ).$objid, array( "edit" => "delete" )) ?>">
										<?= _DELETE ?></a>
								</div>
								<?
							}
						}
					}
				}
			}

			?>
		</div>
		<?
	}

	?>
</div>
