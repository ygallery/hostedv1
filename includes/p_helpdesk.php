<div class="header">
	<div class="header_title">
		Helpdesk
		<div class="subheader">
			Centralized user-to-staff interaction
		</div>
	</div>
	<?

	$_documentTitle = "Helpdesk";

	$active = 0;

	switch( $_cmd[ 1 ])
	{
		case "request":
		{
			$active = 1;
			break;
		}

		case "faq":
		{
			$active = 2;
			break;
		}
	}

	?>
	<div class="normaltext">
		<?

		if( atLeastSModerator() )
		{
			?>
			<div class="tab<?= $active == 0 ? " tab_active" : "" ?>"
				onclick="document.location='<?= url( "helpdesk" ) ?>';">
				<?= getIMG( url()."images/emoticons/a-left.png" )?>
				My Requests
			</div>
			<div class="tab<?= $active == 1 ? " tab_active" : "" ?>"
				onclick="document.location='<?= url( "helpdesk/request" ) ?>';">
				<?= getIMG( url()."images/emoticons/star.png" )?>
				Add a Request
			</div>
			<?
		}

		?>
		<div class="tab<?= $active == 2 ? " tab_active" : "" ?>"
			onclick="document.location='<?= url( "helpdesk/faq" ) ?>';">
			<?= getIMG( url()."images/emoticons/help.png" )?>
			Frequently Asked Questions
		</div>
	</div>
	<div style="clear : both;"></div>
</div>
<div class="container">
	<?

	switch( $_cmd[ 1 ])
	{
		case "":
		{
			include( INCLUDES."p_helpdesk!requests.php" );
			break;
		}

		case "details":
		{
			include( INCLUDES."p_helpdesk!details.php" );
			break;
		}

		case "faq":
		{
			include( INCLUDES."p_helpdesk!faq.php" );
			break;
		}

		case "request":
		{
			include( INCLUDES."p_helpdesk!request.php" );
			break;
		}
	}

	?>
</div>
<?

function addRequestDetail( $helpdeskItem, $detailPostVar, $detailFileVar,
	$detailPrivacy )
{
	global $_auth;

	$detailText = isset( $_POST[ $detailPostVar ]) ?
		trim( $_POST[ $detailPostVar ]) : "";

	$fileName = "";
	$fileNameOrig = "";

	include_once( INCLUDES."files.php" );

	$uploadError = checkUploadedFile( $detailFileVar );

	if( $uploadError != _UPL_NO_FILE && $uploadError != "" )
	{
		return( $uploadError );
	}

	if( $uploadError == "" )
	{
		$fileNameOrig = $_FILES[ $detailFileVar ][ "name" ];

		$fileName = applyIdToPath( "files/helpdesk/", $helpdeskItem );
		$fileName .= "-".substr( sha1( mt_rand() * ( microtime() * 0.001 )),
			1, 16 );

		uploadFile( $detailFileVar, $fileName, $extension );

		$fileName .= ".".$extension;

		if( $detailText == "" )
		{
			$detailText = "There is no text message in this detail.";
		}
	}
	
	if( $detailText != "" )
	{
		sql_values( array(
			"hddItem" => $helpdeskItem,
			"hddSubmitDate!" => "NOW()",
			"hddPrivacy" => $detailPrivacy,
			"hddCreator" => $_auth[ "useid" ],
			"hddMessage" => $detailText,
			"hddAttachment" => $fileName,
			"hddAttachOrigName" => $fileNameOrig ));

		sql_insert( "helpdeskDetails" );
	}

	return( "" );
}

function getRequestCategoryText( $categoryCode )
{
	sql_where( array( "hdcid" => $categoryCode ));

	return( sql_value( "helpdeskCats", "hdcName", "?" ));
}

function getRequestPrivacyText( $privacyCode, $hlpData )
{
	global $_auth;

	$hasOwner = ( $hlpData[ "hlpOwner" ] > 0 );
	$isSubmitter = isLoggedIn() &&
		( $_auth[ "useid" ] == $hlpData[ "hlpSubmitter" ]);
	$isOwner = isLoggedIn() &&
		( $_auth[ "useid" ] == $hlpData[ "hlpOwner" ]);
	$submitterText = $isSubmitter ? "You (Submitter)" : ( getUserLink( $hlpData[ "hlpSubmitter" ])." (Submitter)" );
	$ownerText = $isOwner ? "You (Owner)" : ( getUserLink( $hlpData[ "hlpOwner" ])." (Owner)" );
	
	$privacy = "?";
	$icon = "ghost";

	switch( $privacyCode )
	{
		case "all":
		{
			$privacy = $hasOwner ?
				( "Staff, ".$submitterText." and ".$ownerText ) :
				( "Staff and ".$submitterText );
			$icon = "star";
			break;
		}
	
		case "submitter":
		{
			$privacy = "Staff and ".$submitterText;
			$icon = $hasOwner ? "ghost" : "star";
			break;
		}
	
		case "owner":
		{
			$privacy = $hasOwner ? ( "Staff and ".$ownerText ) : "Staff only";
			break;
		}
	
		case "private":
		{
			$privacy = "Staff only";
			break;
		}
	}

	return( "<b>To</b>: ".getIMG( url()."images/emoticons/".$icon.".png" )." ".$privacy );
}

function getRequestRefOwner( $refType, $refID )
{
	$result = 0;

	switch( $refType )
	{
		case "submission":
		{
			sql_where( array( "objid" => $refID ));
			$result = sql_value( "objects", "objCreator", "0" );
			break;
		}

		case "extra":
		{
			sql_where( array( "objid" => $refID ));
			$result = sql_value( "extras", "objCreator", "0" );
			break;
		}

		case "pm":
		{
			break;
		}

		case "comment":
		{
			break;
		}

		case "journal":
		{
			break;
		}

		case "poll":
		{
			break;
		}

		case "faq":
		{
			sql_where( array( "hfqid" => $refID ));
			$result = sql_value( "helpdeskFAQ", "hfqCreatedBy", "0" );
			break;
		}
	}

	return( $result );
}

function getRequestRefText( $refType, $refID )
{
	$result = "?";

	switch( $refType )
	{
		case "none":
		{
			$result = "--";
			break;
		}

		case "submission":
		{
			sql_where( array( "objid" => $refID ));

			$result = 'Submission &ndash; <a href="'.url( "view/".$refID ).'">'.
				htmlspecialchars( sql_value( "objects", "objTitle", "?" ))."</a>";

			break;
		}

		case "extra":
		{
			sql_where( array( "objid" => $refID ));

			$result = 'Extras &ndash; <a href="'.url( "view/e".$refID ).'">'.
				htmlspecialchars( sql_value( "extras", "objTitle", "?" ))."</a>";

			break;
		}

		case "pm":
		{
			break;
		}

		case "comment":
		{
			break;
		}

		case "journal":
		{
			break;
		}

		case "poll":
		{
			break;
		}

		case "faq":
		{
			$result = "FAQ";

			sql_where( array( "hfqid" => $refID ));

			if( $data = sql_row( "helpdeskFAQ, helpdeskFAQCats" ))
			{
				$result .= " &ndash; ".
					htmlspecialchars( preg_replace( '/^.*\|/', "", $data[ "hfcName" ])).
					' &ndash; <a href="'.url( "helpdesk/faq/".$data[ "hfcIdent" ]."/".$data[ "hfqIdent" ]).'">'.
					htmlspecialchars( preg_replace( '/^.*\|/', "", $data[ "hfqTitle" ])).
					"</a>";
			}

			break;
		}
	}

	return( $result );
}

function getRequestStatusText( $statusCode )
{
	$status = "?";

	switch( $statusCode )
	{
		case "wait-assign":
		{
			$status = "<i>Waiting for assignment...</i>";
			break;
		}
		case "in-progress":
		{
			$status = "Assigned, in progress";
			break;
		}
		case "wait-submitter":
		{
			$status = "<i>Waiting for response from submitter...</i>";
			break;
		}
		case "wait-owner":
		{
			$status = "<i>Waiting for response from owner...</i>";
			break;
		}
		case "completed":
		{
			$status = "<b>Completed</b>";
			break;
		}
	}

	return( $status );
}

function getRequestUrgencyText( $urgencyCode )
{
	$result = "?";
	$icon = "bullet2";

	switch( $urgencyCode )
	{
		case 1:
		{
			$result = "very low";
			$icon = "dead";
			break;
		}
		
		case 2:
		{
			$result = "lower";
			$icon = "bullet3";
			break;
		}
		
		case 3:
		{
			$result = "normal";
			$icon = "bullet";
			break;
		}
		
		case 4:
		{
			$result = "higher";
			$icon = "bullet2";
			break;
		}

		case 5:
		{
			$result = "VERY HIGH";
			$icon = "evil";
			break;
		}
	}

	$result .= " ";

	for( $i = 0; $i < $urgencyCode; $i++ )
	{
		$result .= getIMG( url()."images/emoticons/".$icon.".png" );
	}

	return( $result );
}

function putRequestData( $hlpData, $showDetails = false )
{
	global $_auth;

	?>
	<div class="container2 mar_bottom">
		<div style="margin-left : 20px;">
			<div style="margin-left : -20px; margin-top : 0.4em; float : left;">
				<?= getIMG( url()."images/emoticons/star.png" )?>
			</div>
			<?

			if( $hlpData[ "hlpReferenceType" ] == "submission" )
			{
				?>
				<div style="float : right; margin-left : 1em; margin-bottom : 0.5em;">
					<?= getObjectThumb( $hlpData[ "hlpReferenceId" ], 100, false ) ?>
				</div>
				<?
			}

			if( $hlpData[ "hlpReferenceType" ] == "extras" )
			{
				?>
				<div style="float : right; margin-left : 1em; margin-bottom : 0.5em;">
					<?= getObjectThumb( $hlpData[ "hlpReferenceId" ], 100, true ) ?>
				</div>
				<?
			}

			?>
			<div class="header" style="padding : 0; padding-bottom : 0.3em;">
				<a href="<?= url( "helpdesk/details/".$hlpData[ "hlpid" ]) ?>">
					<?= getRequestCategoryText( $hlpData[ "hlpCategory" ]) ?></a>
			</div>
			<?

			$ref = getRequestRefText(
				$hlpData[ "hlpReferenceType" ],
				$hlpData[ "hlpReferenceId" ]);

			if( $ref != "--" )
			{
				?>
				<div>
					<b>In reference to</b>: <?= $ref ?>
				</div>
				<?
			}

			?>
			<div>
				<b>Status</b>:
				<?= getRequestStatusText( $hlpData[ "hlpStatus" ]) ?>
			</div>
			<div>
				<b>Urgency</b>:
				<?= getRequestUrgencyText( $hlpData[ "hlpUrgency" ]) ?>
			</div>
			<?

			$hasOwner = ( $hlpData[ "hlpOwner" ] > 0 );
			$isStaff = atLeastModerator();
			$isSubmitter = isLoggedIn() &&
				( $_auth[ "useid" ] == $hlpData[ "hlpSubmitter" ]);
			$isOwner = isLoggedIn() &&
				( $_auth[ "useid" ] == $hlpData[ "hlpOwner" ]);

			if( $isOwner && $hlpData[ "hlpStatus" ] == "wait-assign" )
			{
				?>
				<div class="mar_top">
					<span class="error">Summary will be available after the request is assigned.</span>
				</div>
				<?
			}
			else
			{
				?>
				<div class="mar_top">
					<?= formatText( $hlpData[ "hlpSummary" ], true, true ) ?>
				</div>
				<?
			}

			$allowedPrivacy = "'all'";

			if( $isStaff )
			{
				$allowedPrivacy .= ",'private','submitter','owner'";
			}
			else
			{
				if( $isSubmitter )
				{
					$allowedPrivacy .= ",'submitter'";
				}

				if( $isOwner )
				{
					$allowedPrivacy .= ",'owner'";
				}
			}

			if( $showDetails )
			{
				?>
				<div style="clear : both;"></div>
				<?

				sql_order( "hddSubmitDate" );
				sql_where( array(
					"hddItem" => $hlpData[ "hlpid" ],
					"|1" => "`hddPrivacy` IN(".$allowedPrivacy.")" ));

				$hddResult = sql_rowset( "helpdeskDetails" );

				while( $hddData = sql_next( $hddResult ))
				{
					putRequestDetail( $hlpData, $hddData );
				}

				sql_free( $hddResult );

				$doSubmitDetail = false;
				$detailPrivacy = "?";

				if( isset( $_POST[ "submitPrivate" ]))
				{
					$doSubmitDetail = true;
					$detailPrivacy = "private";
				}

				if( isset( $_POST[ "submitSubmitter" ]))
				{
					$doSubmitDetail = true;
					$detailPrivacy = "submitter";
				}

				if( isset( $_POST[ "submitOwner" ]))
				{
					$doSubmitDetail = true;
					$detailPrivacy = "owner";
				}

				if( isset( $_POST[ "submitAll" ]))
				{
					$doSubmitDetail = true;
					$detailPrivacy = "all";
				}

				if( $doSubmitDetail )
				{
					addRequestDetail( $hlpData[ "hlpid" ], "detailText",
						"detailFile", $detailPrivacy );

					redirect( url( "." ));
				}

				if( $hlpData[ "hlpStatus" ] == "completed" )
				{
					?>
					<span class="error"><b>COMPLETED</b></span>.
					This request's status is 'completed', no further details are accepted.
					<?
				}
				else
				{
					?>
					<div class="container2 mar_bottom">
						<div class="header" style="padding-left : 0; padding-top : 0;">
							Submit new detail
						</div>
						<form action="<?= url( "." ) ?>" enctype="multipart/form-data" method="post">
							<div class="mar_bottom">
								<textarea name="detailText" rows="9" style="width : 95%;"></textarea>
							</div>
							<div class="mar_bottom" style="padding-right : 10%">
								(optionally) <b>Upload a file</b>:
							</div>
							<div class="mar_bottom">
								<input name="detailFile" type="file" size="30" />
							</div>
							<?
				    
							if( $isSubmitter && !$hasOwner )
							{
								// A simple submit button.
				    
								?>
								<div class="mar_top">
									<input type="submit" name="submitSubmitter" value="<?= _SUBMIT ?>" />
								</div>
								<?
							}
							else
							{
								?>
								<div class="mar_top">
									Who should be able to read this detail:
								</div>
								<?
						    
								if( $hasOwner )
								{
									?>
									<div class="mar_top">
										<button name="submitAll">
											<?= getRequestPrivacyText( "all", $hlpData ) ?></button>
									</div>
									<?

									if( !$isSubmitter )
									{
										?>
										<div class="mar_top">
											<button name="submitOwner">
												<?= getRequestPrivacyText( "owner", $hlpData ) ?></button>
										</div>
										<?
									}
								}
	                        
								?>
								<div class="mar_top">
									<button name="submitSubmitter">
										<?= getRequestPrivacyText( "submitter", $hlpData ) ?></button>
								</div>
								<?
	                        
								if( $isStaff )
								{
									// Only staff can submit staff-only details :3
						    
									?>
									<div class="mar_top">
										<button name="submitPrivate">
											<?= getRequestPrivacyText( "private", $hlpData ) ?></button>
									</div>
									<?
								}
						    
								?>
								<div class="mar_top">
									* <b>Submitter</b> &mdash; The user that made the request.
									<?
						    
									if( $hasOwner )
									{
										?>
										<br />
										* <b>Owner</b> &mdash; The user that owns the object questioned by the request.
										<?
									}
						    
									?>
								</div>
								<?
							}
				    
							?>
						</form>
					</div>
					<?
				}
			}
			else
			{
				sql_where( array(
					"hddItem" => $hlpData[ "hlpid" ],
					"|1" => "`hddPrivacy` IN(".$allowedPrivacy.")" ));

				$detailCount = sql_count( "helpdeskDetails" );

				?>
				<div style="text-align : left; margin-top : 0.6em;">
					<a href="<?= url( "helpdesk/details/".$hlpData[ "hlpid" ]) ?>">
						View Request Details</a>
						(<?= $detailCount ?>)
				</div>
				<div style="clear : both;"></div>
				<?
			}

			?>
		</div>
	</div>
	<?
}

function putRequestDetail( $hlpData, $hddData )
{
	global $_auth;

	?>
	<div class="container2 mar_bottom">
		<?

		if( $hddData[ "hddAttachment" ] != "" )
		{
			?>
			<div class="f_right a_right" style="margin-left : 2em; margin-bottom : 1em;">
				<b>Attachment</b>:<br />
				<a target="new-tab"
					href="<?= url("/").$hddData[ "hddAttachment" ]."/".$hddData[ "hddAttachOrigName" ] ?>">
					<?= htmlspecialchars( $hddData[ "hddAttachOrigName" ]) ?></a>
				<?= getIMG( url()."images/emoticons/zip.png" ) ?>
			</div>
			<?
		}

		?>
		<div class="mar_bottom">
			<?= getRequestPrivacyText( $hddData[ "hddPrivacy" ], $hlpData ) ?>
		</div>
		<div>
			<?= getUserLink( $hddData[ "hddCreator" ]) ?>:
			<?= formatText( $hddData[ "hddMessage" ], true, true ) ?>
		</div>
	</div>
	<?
}

?>