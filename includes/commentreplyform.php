<?

/**
 * Shows the reply form.
 */

function showReplyForm()
{
	global $_cmd, $_currentPageURL;

	if( isset( $GLOBALS[ "yg_is_asset" ]))
	{
		return;
	}

	?>
	<script type="text/javascript">
	//<![CDATA[
		document.write( showReplyFormHeader( '<?= $_currentPageURL ?>', '<?= $_currentPageURL ?>' ));
	//]]>
	</script>
	<noscript>
		<div class="container2 error">
			<?= _NEED_JAVASCRIPT ?>
		</div>
	</noscript>
	<?

	$commentWide = $_cmd[ 0 ] == "view" ? true : false;

	include( INCLUDES."mod_comment.php" );

	?>
	<script type="text/javascript">
	//<![CDATA[
		document.write( showReplyFormFooter() );
	//]]>
	</script>

	<a name="replies"></a>
	<div class="header a_right"><?=_OTHER_COMMENTS ?></div>
	<?
}

?>