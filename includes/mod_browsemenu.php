<div class="clear">&nbsp;</div>
<div class="normaltext">

	<div class="tab<?= $active == 1 ? " tab_active" : "" ?>"
		onclick="document.location='<?= url( "browse" )?>'">
		<?= getIMG( url()."images/emoticons/submission.png" ) ?> <?= _ALL_SUBMISSIONS ?></div>

	<div class="tab<?= $active == 7 ? " tab_active" : "" ?>"
		onclick="document.location='<?= url( "browsetops" )?>'">
		<?= getIMG( url()."images/emoticons/fav1.png" ) ?> <?= _MOST_RECENT_FAVS ?></div>

	<div class="tab<?= $active == 2 ? " tab_active" : "" ?>"
		onclick="document.location='<?= url( "browseartists" )?>'">
		<?= getIMG( url()."images/emoticons/avatar_v2.png" ) ?> <?= _ARTISTS ?></div>

	<div class="tab<?= $active == 3 ? " tab_active" : "" ?>"
		onclick="document.location='<?= url( "browseclubs" ) ?>'">
		<?= getIMG( url()."images/emoticons/club2.png" ) ?> <?= _CLUBS1 ?></div>

	<div class="tab<?= $active == 5 ? " tab_active" : "" ?>"
		onclick="document.location='<?= url( "browseprojects" ) ?>'">
		<?= getIMG( url()."images/emoticons/club.png" ) ?> <?= _PROJECTS ?></div>
	<?

	if( isLoggedIn() )
	{
		?>
		<div class="tab<?= $active == 4 ? " tab_active" : "" ?>"
			onclick="document.location='<?= url( "online" ) ?>'">
			<?= getIMG( url()."images/emoticons/online.png" ) ?> <?= _WHOSONLINE ?></div>
		<?
	}

	if( atLeastModerator() )
	{
		?>
		<div class="tab<?= $active == 6 ? " tab_active" : "" ?>"
			onclick="document.location='<?= url( "browsedeleted" ) ?>'">
			<?= getIMG( url()."images/emoticons/cancel.png" ) ?> <?= _DELETED ?></div>
		<?
	}

	?>
	<div class="clear">&nbsp;</div>
</div>
