<div style="clear: both" class="normaltext">

<div class="tab<?= $active == 1 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "admin" ) ?>'"><?= _HOME ?></div>
	
<?php if( atLeastHelpdesk() )
{ ?>
	<div class="tab<?= $active == 2 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "adminchat" ) ?>'">Admin Chat</div>
<?php } ?>

<?php if(isAuthorized('isKeywordsAdmin')) {?>
<div class="tab<?= $active == 3 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "keywords" ) ?>'"><?= _KEYWORDS ?></div>
<?php }?>
    
<?php if(isAuthorized('isPermAdmin')) {?>
<div class="tab<?= $active == 4 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "permissions" ) ?>'">Permissions</div>
<?php }?>

<div class="tab<?= $active == 5 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "abusecases" ) ?>'"><?= _ABUSE_LIST ?></div>

<?php if(atLeastModerator()) {?>
<div class="tab<?= $active == 6 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "bannedips" ) ?>'">Banned IPs</div>

<div class="tab<?= $active == 7 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "bannedipssus" ) ?>'">Suspicious IPs</div>
	
<div class="tab<?= $active == 8 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "browseemail" ) ?>'">Email Search</div>
<div class="tab<?= $active == 9 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "browseip" ) ?>'">IP Search</div>
	
<div class="tab<?= $active == 10 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "removethumb" ) ?>'">Remove Thumbnail</div>
<!--<div class="tab<?= $active == 11 ? " tab_active" : "" ?>"
	onclick="document.location='<?= url( "scfullupdate" ) ?>'">Update Search Cache</div>-->
<?php }?>
<div class="clear"><br /></div>

</div>
