<?

if( !atLeastSModerator() )
{
	return;
}

if( !isset( $_GET[ "nolayout" ]))
{
	redirect( url( ".", array( "nolayout" => 1 )));
}

$procOffset = isset( $_GET[ "offset" ]) ? intval( $_GET[ "offset" ]) : 0;
$procLimit = 50;

include( INCLUDES."customthemes.php" );

?><pre>Updating custom themes...<br /><br /><?

$count = 0;
$result = sql_query( "SELECT * FROM `customThemes` ORDER BY `cusUser` LIMIT $procOffset,$procLimit" );

while( $data = mysql_fetch_assoc( $result ))
{
	$count++;

	$useid = $data[ "cusUser" ];
	$isClub = $data[ "cusIsClub" ];
	$themeData = loadCustomThemeData( $useid, $isClub );
	$errors = array();

	if( updateCustomTheme( $useid, $isClub, $themeData, $errors ))
	{
		echo "$useid ($isClub)<br />";
	}

	foreach( $errors as $key => $error )
	{
		if( $error != _UPL_NO_FILE )
		{
			echo "$useid: [$key] $error<br >";
		}
	}
}

?></pre><?

if( $count > 0 )
{
	?>
	<script type="text/javascript">
	//<![CDATA[

		window.setTimeout(
			"document.location='<?= url( ".", array( "offset" => $procOffset + 50, "nolayout" => 1 ), '&') ?>';", 200);

	//]]>
	</script>
	<?
}

?>
