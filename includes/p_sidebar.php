<?

// This script controls the "Sidebar" tab of the settings.

if( !isLoggedIn() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

?>
<div class="header">
	<div class="header_title">
		<?= _SETTINGS ?>
		<div class="subheader"><?= _SET_SIDEBAR_SETTINGS ?></div>
	</div>
	<?

	$_documentTitle = _SET_SIDEBAR_SETTINGS;

	$active = 4;

	include( INCLUDES."mod_setmenu.php" );

	?>
</div>
<div class="container">
	<form action="<?= url( "." ) ?>" method="post">
	<?

	if( isset( $_POST[ "submit" ]))
	{
		// Options

		$_auth[ "useSidebarThumbs" ] = isset( $_POST[ "useSidebarThumbs" ]) ? 1 : 0;

		// Sidebar

		$_auth[ "useSidebar" ] = $_POST[ "useSidebar" ];

		// Update the database

		sql_query( "UPDATE `useExtData`".dbSet( array(
			"useSidebar" => $_auth[ "useSidebar" ],
			"useSidebarThumbs" => $_auth[ "useSidebarThumbs" ])).dbWhere( array(
			"useEid" => $_auth[ "useid" ])));

		notice( _SET_SAVED );
	}

	// ======================================================================================================
	// OPTIONS
	// ======================================================================================================

	?>
	<? iefixStart() ?>
	<div class="sep largetext"><?= _OEKAKI_OPTIONS ?></div>
	<div class="container2 notsowide">
		<table cellspacing="0" cellpadding="4" border="0">
		<tr>
			<td align="right">
				<input <?= $_auth[ "useSidebarThumbs" ] ? 'checked="checked"' : ""?> class="checkbox"
					id="idSidebarThumbs" name="useSidebarThumbs" type="checkbox" />
			</td>
			<td>
				<label for="idSidebarThumbs"><?= _SET_SIDEBAR_THUMBS ?></label>
			</td>
		</tr>
		</table>
	</div>
	<? iefixEnd() ?>
	<?

	// ======================================================================================================
	// SIDEBAR
	// ======================================================================================================

	?>
	<? iefixStart() ?>
	<div class="sep largetext"><?= _PERSONAL_NOTES ?></div>
	<div class="container2 notsowide">
		<table cellspacing="0" cellpadding="4" border="0" width="100%">
		<tr>
			<td valign="top" align="right" class="nowrap">
				<div><?= _TEXT ?>:</div>
				<?

				$script = "make_invisible('sidebar_current'); ".
					"make_invisible('sidebar_edit'); ".
					"make_invisible('sidebar_explain'); ".
					"make_visible('sidebar_change'); ".
					"return false;";

				$sidebarEdit = '<div id="sidebar_edit">'.
					'(<a href="" onclick="'.$script.'">'._EDIT.'</a>)</div>';

				if( trim( $_auth[ "useSidebar" ]) != "" )
				{
					echo $sidebarEdit;
				}

				?>
			</td>
			<td width="100%">
				<div id="sidebar_current">
					<?

					if( trim( $_auth[ "useSidebar" ]) != "" )
					{
						?>
						<div class="container2"><?= formatText( $_auth[ "useSidebar" ]) ?></div>
						<?
					}
					else
					{
						echo $sidebarEdit;
					}

					?>
				</div>
				<div id="sidebar_change" style="display: none">
					<div>
						<?

						iefixStart();

						$commentName = "useSidebar";
						$commentDefault = $_auth[ "useSidebar" ];
						$commentNoOptions = true;
						$commentRows = 12;

						include( INCLUDES."mod_comment.php" );

						iefixEnd();

						?>
						<div class="clear">&nbsp;</div>
					</div>
				</div>
				<div id="sidebar_explain" class="sep notsowide"><?= _SET_SIDEBAR_EXPLAIN ?></div>
			</td>
		</tr>
		</table>
	</div>
	<? iefixEnd() ?>
	<?

	// ======================================================================================================

	?>
	<div class="sep">
		<button class="submit" name="submit" type="submit">
			<?= getIMG( url()."images/emoticons/checked.png" ) ?>
			<?= _SAVE_CHANGES ?>
		</button>
	</div>
	</form>
</div>
