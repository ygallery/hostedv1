<?

// Functions to perform such operations as fav/unfav, watch/unwatch,
// clear art update & mark as read.

// -----------------------------------------------------------------------------

function performDelayedOperations()
{
	global $_config;

	if( !isLoggedIn() || $_config[ "readOnly" ] || !isset( $_COOKIE[ "yGalOps" ]))
	{
		return false;
	}

	$return = false;
	if(count($ops = preg_split( '/\s/', $_COOKIE[ "yGalOps" ], -1, PREG_SPLIT_NO_EMPTY )))
	{
		$return = true;
	}

	foreach( $ops as $op )
	{
		performOperation( $op );
	}

	setcookie( "yGalOps", "", time(), "/", ".".$_config[ "galRoot" ]);
	return $return;
}

// -----------------------------------------------------------------------------

function performOperation( $op )
{
	$op = trim( $op );
	$param = substr( $op, 1 );

	switch( $op[ 0 ])
	{
		case "f": // fav/unfav
		{
			if( $param[ 0 ] == "u" )
				operationUnfav( substr( $param, 1 ));
			else
				operationFav( $param );

			break;
		}

		case "w": // watch/unwatch
		{
			if( $param[ 0 ] == "u" )
				operationUnwatch( substr( $param, 1 ));
			else
				operationWatch( $param );

			break;
		}

		/*
		case "c": // clear art update
		{
			operationClearArt( $param );
			break;
		}
		*/

		case "m": // mark as read
		{
			switch( $param[ 0 ])
			{
				case "a":
				{
					operationMarkAsRead( substr( $param, 1 ), updTypeAnnouncement );
					break;
				}

				case "j":
				{
					operationMarkAsRead( substr( $param, 1 ), updTypeJournal );
					break;
				}

				case "p":
				{
					operationMarkAsRead( substr( $param, 1 ), updTypeJournalPoll );
					break;
				}

				default:
				{
					operationMarkAsRead( $param, updTypeComment );
					break;
				}
			}

			break;
		}
	}
}

// -----------------------------------------------------------------------------

function operationFav( $objid )
{
	global $_auth;

	$objid = intval( $objid );

	if( $objid == 0 )
		return;

	// Adds submission to user's favourites.

	$sql = "SELECT * FROM `objects`, `objExtData`".dbWhere( array(
		"objid" => $objid,
		"objEid*" => "objid",
		"objCreator<>" => $_auth[ "useid" ],
		"objDeleted" => 0,
		"objPending" => 0 ));

	$objResult = sql_query( $sql );

	if( !$objData = mysql_fetch_assoc( $objResult ))
	{
		include( INCLUDES."p_notfound.php" );
		return;
	}

	// Check if the submission is already +faved.

	$sql = "SELECT COUNT(*) FROM `favourites`".dbWhere( array(
		"favCreator" => $_auth[ "useid" ],
		"favObj" => $objData[ "objid" ]));

	$cntResult = sql_query( $sql );

	if( mysql_result( $cntResult, 0 ) == 0 )
	{
		$sql = "INSERT INTO `favourites`".dbValues( array(
			"favCreator" => $_auth[ "useid" ],
			"favObj" => $objData[ "objid" ],
			"favSubmitDate!" => "NOW()" ));

		sql_query( $sql );

		// Notify the artist about +fav.

		addUpdate( updTypeMessageFav, $objData[ "objCreator" ],
			$objData[ "objid" ], $_auth[ "useid" ]);

		markAsRead( updTypeArt, $objData[ "objid" ]);

		$collab = $objData[ "objCollabConfirmed" ] ? $objData[ "objCollab" ] : 0;

		if( $collab > 0 && $collab != $_auth[ "useid" ])
		{
			addUpdate( updTypeMessageFav, $collab, $objData[ "objid" ],
				$_auth[ "useid" ]);
		}

		updateSubmissionFavs( $objid );
	}
}

// -----------------------------------------------------------------------------

function operationUnfav( $objid )
{
	global $_auth;

	$objid = intval( $objid );

	if( $objid == 0 )
		return;

	// Remove submission from user's favourites.

	$sql = "SELECT * FROM `objects`, `objExtData`".dbWhere( array(
		"objid" => $objid,
		"objEid*" => "objid",
		"objCreator<>" => $_auth[ "useid" ],
		"objDeleted" => 0,
		"objPending" => 0 ));

	$objResult = sql_query( $sql );

	if( !$objData = mysql_fetch_assoc( $objResult ))
	{
		include( INCLUDES."p_notfound.php" );
		return;
	}

	$sql = "DELETE FROM `favourites`".dbWhere( array(
		"favCreator" => $_auth[ "useid" ],
		"favObj" => $objid ));

	sql_query( $sql );

	// Remove +fav notification from artist(s)' updates.

	markAsRead( updTypeMessageFav, $objData[ "objid" ], $objData[ "objCreator" ]);

	$collab = $objData[ "objCollabConfirmed" ] ? $objData[ "objCollab" ] : 0;

	if( $collab > 0 && $collab != $_auth[ "useid" ])
	{
		markAsRead( updTypeMessageFav, $objData[ "objid" ], $collab );
	}

	updateSubmissionFavs( $objid );
}

// -----------------------------------------------------------------------------
// Update submission's favs counter.

function updateSubmissionFavs( $objid )
{
	$sql = "SELECT COUNT(*) FROM `favourites`".dbWhere( array(
		"favObj" => $objid ));

	$result = sql_query( $sql );

	$favCount = mysql_result( $result, 0 );

	$sql = "SELECT `objViewed` FROM `objExtData`".dbWhere( array(
		"objEid" => $objid ));

	$result = sql_query( $sql );

	$viewCount = mysql_result( $result, 0 );

	if( $viewCount == 0 )
		$popularity = 0;
	else
		$popularity = round( $favCount * $favCount * 1000 / $viewCount );

	sql_query( "UPDATE `objects`".dbSet( array(
		"objPopularity" => $popularity )).dbWhere( array(
		"objid" => $objid )));

	sql_query( "UPDATE `objExtData`".dbSet( array(
		"objFavs" => $favCount )).dbWhere( array(
		"objEid" => $objid )));
}

// -----------------------------------------------------------------------------
// Add user to the current user's watch list

function operationWatch( $useid )
{
	global $_auth;

	$useid = intval( $useid );

	if( $useid == 0 || $useid == $_auth[ "useid" ])
		return;

	$values = array(
		"watUser" => $_auth[ "useid" ],
		"watCreator" => $useid,
		"watType" => "use" );

	$sql = "SELECT COUNT(*) FROM `watches`".dbWhere( $values );

	$result = sql_query( $sql );

	if( mysql_result( $result, 0 ) == 0 )
	{
		$values[ "watSubmitDate!" ] = "NOW()";

		$sql = "INSERT IGNORE INTO `watches`".dbValues( $values );

		sql_query( $sql );

		// Notify the artist about the +watch.

		addUpdate( updTypeMessageWatch, $useid, 0, $_auth[ "useid" ]);
	}
}

// -----------------------------------------------------------------------------
// Removes user from the current user's watch list.

function operationUnwatch( $useid )
{
	global $_auth;

	$useid = intval( $useid );

	if( $useid == 0 )
		return;

	$sql = "DELETE FROM `watches`".dbWhere( array(
		"watUser" => $_auth[ "useid" ],
		"watCreator" => $useid,
		"watType" => "use" ));

	sql_query( $sql );

	// Remove +watch notification from artist's updates.

	$sql = "DELETE FROM `updates`".dbWhere( array(
		"updType" => updTypeMessageWatch,
		"updCreator" => $useid,
		"updUser" => $_auth[ "useid" ]));

	sql_query( $sql );

	recountUpdates( updTypeMessageWatch, $useid );
}

// -----------------------------------------------------------------------------

function operationClearArt( $objidList )
{
	$idList = preg_split( '/\-/', $objidList, -1, PREG_SPLIT_NO_EMPTY );

	foreach( $idList as $id )
	{
		if( substr( $id, 0, 1 ) == "e" )
		{
			$isExtras = true;
			$objid = intval( substr( $id, 1 ));
		}
		else
		{
			$objid = intval( $id );
			$isExtras = false;
		}
	}
}

// -----------------------------------------------------------------------------

function operationMarkAsRead( $updObj, $type )
{
	global $_auth;

	$updObj = intval( $updObj );

	if( $updObj == 0 )
		return;

	$sql = "DELETE FROM `updates`".dbWhere( array(
		"updType" => $type,
		"updCreator" => $_auth[ "useid" ],
		"updObj" => $updObj ));

	sql_query( $sql );

	recountUpdates( $type, $_auth[ "useid" ]);
}

// -----------------------------------------------------------------------------

?>
