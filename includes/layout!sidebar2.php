<?

include_once( INCLUDES."layout!sidebar-func.php" );

if( isLoggedIn() )
{
	?>
	<div style="margin-bottom : 8px;">
		<?
	
		$w = str_replace( " ", "<br />", sprintf( _WELCOME_USER, "[user]" ));
		$w = str_replace( "[user]", "<b>".getUserLink( $_auth[ "useid" ])."</b>", $w );

		echo $w;
	
		?>
	</div>
	<?

	putSidebarUpdates();

	if( $_newUI == 2 && isLoggedIn() )
	{
		?>
		<div class="ui2-layout-bg ui2-separator"></div>
		<table cellspacing="0" cellpadding="0" border="0" style="text-align : left; padding-right : 8px; margin : 0 auto;"><tr><td>
			<a href="<?= url( "/" ) ?>">
                <?= getIMG( url()."images/ui2/news.png" ) ?>
                News</a><br>
        <a href="<?= url( "club/11592/" ) ?>">
            <?= getIMG( url()."images/ui2/chat.png" ) ?>
            Site Changelog</a><br>
        <a href="<?= url( "browse" ) ?>">
            <?= getIMG( url()."images/ui2/browse.png" ) ?>
            <?= _BROWSE ?></a><br>
        <a href="<?= url( "search" ) ?>">
            <?= getIMG( url()."images/ui2/search.png" ) ?>
            <?= _SEARCH ?></a><br>
        <a href="<?= url( "helpdesk/faq" ) ?>">
            <?= getIMG( url()."images/ui2/help.png" ) ?>
            FAQ.</a><br>
        <a href="<?= url( "submit" ) ?>">
            <?= getIMG( url()."images/ui2/submit.png" ) ?>
            <?= _SUBMIT ?></a><br>
        <a href="<?= url( "club/1" ) ?>">
            <?= getIMG( url()."images/ui2/queue.png" ) ?>
            Help Desk</a><br>
        <a href="http://www.y-gallery.net/pm/for/SpiralSea">
            <?= getIMG( url()."images/ui2/donate.png" ) ?>
            Ask How to Donate</a><br>
        <a href="http://www.y-gallery.net/helpdesk/faq/general/advert">
            <?= getIMG( url()."images/emoticons/star4.png" ) ?>
            Advertise</a><br>
			<a href="<?= url( "settings" ) ?>">
				<?= getIMG( url()."images/ui2/settings.png", 'alt="" style="height : 18px;"' ) ?>
				<?= _SETTINGS ?></a><br />
			<a href="<?= url( "logout" ) ?>">
				<?= getIMG( url()."images/ui2/logout.png", 'alt="" style="height : 18px;"' ) ?>
				<?= _LOGOUT ?></a><br />
		</td></tr></table>
		<?
	}

	?>
	<div class="ui2-layout-bg ui2-separator"></div>
	<div class="ui2-section-body" style="text-align : center;">
		<a style="font-weight : normal;" href="<?= url( "journal" ) ?>"><?= _JOURNAL ?></a>
		&middot;
		<a href="<?= url( "pm" )?>"><?= $_auth["useUpdPms"] > 0
			? ( getIMG( url()."images/emoticons/pmnew.png" )." "._PRIVATE." (".$_auth["useUpdPms"].")" )
			: ( getIMG( url()."images/emoticons/pmnonew.png" )." "._PRIVATE ) ?></a>
	</div>
	<div class="ui2-section-body" style="text-align : center;">
		<div>
			<a href="javascript:popup('<?= url( "tos", array( "popup" => "yes" )) ?>','tos',900,700)"><?= _TOS ?></a>
		</div>
		<?

		if( atLeastHelpdesk() )
		{
			?>
			<div><a href="<?= url( "admin" ) ?>"><?= _ADMINISTRATION ?></a></div>
			<?
		}

		if( $_newUI == 1 )
		{
			?>
			<div>
				<a style="font-weight : normal;" href="<?= url( "settings/site" ) ?>"><?= _SETTINGS ?></a>
			</div>
			<div>
				<a style="font-weight : normal;" href="<?= url( "logout" ) ?>"><?= _LOGOUT ?></a>
			</div>
			<?
		}

		?>
	</div>
	<div class="ui2-layout-bg ui2-separator" style="margin-bottom : 8px;"></div>
	<?
}
else
{
	?>
	<div style="margin-bottom : 8px;">
		<div style="margin-bottom : 5px;">
			<?= sprintf( _WELCOME_USER, "guest" ) ?>
		</div>
		<? putSidebarLogin(); ?>
	</div>
	<?
}

putSidebarModStuff();

if( isLoggedIn() && $_cmd[ 0 ] == "updates" )
{
	$sql = "SELECT * FROM `objects`, `objExtData`".dbWhere( array(
		"objid*" => "objEid",
		"objCollab" => $_auth[ "useid" ],
		"objCollabConfirmed" => 0,
		"objDeleted" => 0,
		"objPending" => 0 ));

	$objResult = sql_query( $sql );

	if( $objData = mysql_fetch_assoc( $objResult ))
	{
		$objid = $objData[ "objid" ];

		?>
		<div class="ui2-layout-bg ui2-section-closed"><?= _CONFIRMATION_NEEDED ?>:</div>
		<div class="ui2-section-body">
			<div class="a_left padded">
				<?= _COLLAB_CONFIRM_EXPLAIN ?>
			</div>
			<table align="center" cellspacing="0" cellpadding="8" border="0">
			<tr>
				<td><?= getObjectThumb( $objid, 40 ) ?></td>
				<td>
					<a href="<?= url( "confirm/".$objid, array( "accept" => "1" )) ?>"
						onclick="return confirm('<?= _CONFIRM ?>? <?= _ARE_YOU_SURE ?>')">
						<?= getIMG( url()."images/emoticons/checked.png" ) ?>
						<?= _CONFIRM ?>
					</a>
					<br /><br />
					<a href="<?= url( "confirm/".$objid, array( "accept" => "0" )) ?>"
						onclick="return confirm('<?= _REFUSE ?>? <?= _ARE_YOU_SURE ?>')">
						<?= getIMG( url()."images/emoticons/cancel.png" ) ?>
						<?= _REFUSE ?>
					</a>
				</td>
			</tr>
			</table>
		</div>
		<?
	}
}

putSidebarBanner();

if( !isset($_auth["useSidebarThumbs"]) || $_auth["useSidebarThumbs"])
{
	?>
	<div class="ui2-layout-bg ui2-section-closed"><?= _MOST_RECENT ?></div>
	<?

	putSidebarMostRecent();
}

if( isLoggedIn() && trim( $_auth[ "useSidebar" ]) != "" )
{
	?>
	<div class="ui2-layout-bg ui2-section-closed"><?= _PERSONAL_NOTES ?></div>
	<div class="ui2-section-body">
		<?= formatText( $_auth[ "useSidebar" ]) ?>
	</div>
	<?
}

if( !isset($_auth["useSidebarThumbs"]) || $_auth["useSidebarThumbs"])
{
	?>
	<div class="ui2-layout-bg ui2-section-closed" style="margin-bottom : 8px;"><?= _RANDOM ?></div>
	<?

	putSidebarRandom();
}

putSidebarPoll();

?>
<div class="ui2-layout-bg ui2-section-closed"><? printf( _ADS_CAPTION, $_config[ "galName" ]); ?></div>
<div class="ui2-section-body" style="text-align : center;">
	<a href="http://www.y-gallery.net/pm/for/Lyiint"><b>Ask How to Donate</b></a><br /><br />
	<a href="https://www.reddit.com/r/yGallery/"><b>Visit us on Reddit</b></a>
</div>
<?php
/*
<div class="ui2-section-body" style="text-align : center;">
	<a href="http://www.y-gallery.net/donate.html"><b>Donate to y!Gallery</b></a>
	<div style="font-size : 9px;">via Amazon Payments</div>
</div>
 */
?>
