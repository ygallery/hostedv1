<?php

include_once( INCLUDES."files.php" );

// ---------------------------------------------------------------------------

function getCustomThemePath( $useid, $isClub )
{
	return( applyIdToPath( "files/".( $isClub ? "club" : "" )."themes/",
		$useid )."/" );
}

// ---------------------------------------------------------------------------

function getLastCustomThemeFile( $useid, $isClub )
{
	return( findNewestFile( getCustomThemePath( $useid, $isClub ).
		"style-*.css" ));
}

// ---------------------------------------------------------------------------

function removeCustomTheme( $useid, $isClub )
{
	$lastThemeFile = getLastCustomThemeFile( $useid, $isClub );

	if( $lastThemeFile != "" )
	{
		unlink( $lastThemeFile );
	}
}

// ---------------------------------------------------------------------------

function loadCustomThemeData( $useid, $isClub )
{
	$cusWhere = dbWhere( array(
		"cusUser" => $useid,
		"cusIsClub" => $isClub ));

	$result = sql_query( "SELECT `cusData` FROM `customThemes`".
		$cusWhere."LIMIT 1" );

	if( mysql_num_rows( $result ) == 0 )
	{
		$data = array( "Tile" => 0 );
	}
	else
	{
		$data = unserialize( mysql_result( $result, 0 ));
	}

	return( $data );
}

// ---------------------------------------------------------------------------

function updateCustomTheme( $useid, $isClub, $data, &$errors )
{
	$themePath = getCustomThemePath( $useid, $isClub );

	forceFolders( $themePath );

	$lastThemeFile = getLastCustomThemeFile( $useid, $isClub );

	$err = !uploadSingleImage( "logo", 0, 0, 200000, $fnLogo, $errors, $themePath );
	$err |= !uploadSingleImage( "outer", 0, 0, 200000, $fnOuter, $errors, $themePath );
	$err |= !uploadSingleImage( "container", 0, 0, 200000, $fnContainer, $errors, $themePath );
	$err |= !uploadSingleImage( "container2", 0, 0, 200000, $fnContainer2, $errors, $themePath );

	if( $err )
	{
		return( false );
	}

	$fileLogo = urlf().findNewestFile( $themePath."logo-*.jpg" );
	$fileOuter = urlf().findNewestFile( $themePath."outer-*.jpg" );
	$fileContainer = urlf().findNewestFile( $themePath."container-*.jpg" );
	$fileContainer2 = urlf().findNewestFile( $themePath."container2-*.jpg" );

	$pixLogo = PIX( $fnLogo );
	$pixOuter = PIX( $fnOuter );
	$pixContainer = PIX( $fnContainer );
	$pixContainer2 = PIX( $fnContainer2 );
	$pix00Container = PIX( $fnContainer, 0, 0 );

	$avgLogo = AVG( $fnLogo );
	$avgOuter = AVG( $fnOuter );
	$avgContainer = AVG( $fnContainer );
	$avgContainer2 = AVG( $fnContainer2 );

	$colors = array(
		// Background colors:
		'{bBody}' => $pixLogo,
		'{bOuter}' => $pixOuter,
		'{bContainer}' => $pixContainer,
		'{bContainer2}' => $pixContainer2,
		'{bInput}' => BRIGHT( $pixContainer2, 1.2 ),
		'{bInputHover}' => BRIGHT( $pixContainer2, 1.5 ),
		'{bInputFocus}' => $pix00Container,
		'{bButton}' => BRIGHT( $pixContainer2, 1.2 ),
		'{bButtonHover}' => BRIGHT( $pixContainer2, 1.5 ),
		'{bTabActive}' => $pix00Container,
		'{bTabInactive}' => $pixOuter,
		'{bUI2Layout}' => $pixContainer2,

		// Font/border colors:
		'{cBody}' => BRIGHT( CONTRAST( $avgLogo ), 1.3 ),
		'{cOuter}' => BRIGHT( CONTRAST( $avgOuter ), 1.2 ),
		'{cContainer}' => BRIGHT( CONTRAST( $avgContainer ), 1.3 ),
		'{cContainer2}' => BRIGHT( CONTRAST( $avgContainer2 ), 1.3 ),
		'{cOutline}' => BRIGHT( CONTRAST( $avgContainer2 ), 1.3 ),
		'{cTabActive}' => CONTRAST( $avgContainer ),
		'{cTabInactive}' => CONTRAST( $avgOuter ),
		'{cInput}' => CONTRAST( $avgContainer2 ),
		'{cLink}' => RICH( BRIGHT( $pixContainer2, -2.5 ), 1.1 ),
		'{cLinkHover}' => RICH( BRIGHT( $pixContainer2, -3 ), 1.1 ),
		'{cHeaderLink}' => RICH( BRIGHT( $pixOuter, -2.5 ), 1.1 ),
		'{cHeaderLinkHover}' => RICH( BRIGHT( $pixOuter, -3 ), 1.1 ),
		'{cMarkedText}' => BRIGHT( HARMONY( $pixContainer2 ), 1.5 ),
		'{cTabOutline}' => BRIGHT( CONTRAST( $avgContainer2 ), 1.3 ),
		'{cThumbBorder}' => BRIGHT( CONTRAST( $avgContainer2 ), 1.3 ),
		'{cThumbBorderMature}' => RICH( BRIGHT( $pixContainer2, -3 ), 1.1 ));

	if( tooClose( $colors[ '{cLink}' ], $colors[ '{cContainer2}' ], 32 ))
	{
		$colors[ '{cLink}' ] = RICH( PIX( $fnOuter ), 2 );
		$colors[ '{cLinkHover}' ] = BRIGHT( $colors[ '{cLink}' ], 2.5 );
	}

	if( tooClose( $colors[ '{cContainer}' ], $colors[ '{cMarkedText}' ], 140 ))
	{
		$colors[ '{cMarkedText}' ] = BRIGHT( HARMONY( PIX( $fnContainer2 )), -1.3 );
	}

	// Override auto-generated colors with POSTed colors.

	if( !isset( $data[ "c" ]))
	{
		$data[ "c" ] = array();
	}

	$acceptedColors = array( "cBody", "cOuter", "cContainer", "cContainer2",
		"cOutline", "cTabActive", "cTabInactive", "cInput", "cLink",
		"cLinkHover", "cHeaderLink", "cHeaderLinkHover", "cMarkedText",
		"cTabOutline", "cThumbBorder", "cThumbBorderMature",
		"sOutline", "sTitleMargin", "bButton", "bButtonHover", "bInput", "bInputFocus",
		"bUI2Layout" );

	// Override some data from POST (if available)

	foreach( $acceptedColors as $c1 )
	{
		if( isset( $_POST[ $c1 ]) && $_POST[ $c1 ] != "" )
		{
			$data[ "c" ][ $c1 ] = $_POST[ $c1 ];
		}
	}

	foreach( $data[ "c" ] as $key => $value )
	{
		if( preg_match( '/^[0-9A-Fa-f]{3}$/', $value ))
		{
			$r = hexdec( substr( $value, 0, 1 )) * 17;
			$g = hexdec( substr( $value, 1, 1 )) * 17;
			$b = hexdec( substr( $value, 2, 1 )) * 17;

			$colors[ "{".$key."}" ] = getColor( $r, $g, $b );
		}

		if( preg_match( '/^[0-9A-Fa-f]{6}$/', $value ))
		{
			$r = hexdec( substr( $value, 0, 2 ));
			$g = hexdec( substr( $value, 2, 2 ));
			$b = hexdec( substr( $value, 4, 2 ));

			$colors[ "{".$key."}" ] = getColor( $r, $g, $b );
		}
	}

	$sOutline = 'border-width: 1px;';

	if( isset( $data[ "c" ][ "sOutline" ]))
	{
		switch( $data[ "c" ][ "sOutline" ])
		{
			case 2:
				$sOutline = 'border-width: 2px;';
				break;

			case 3:
				$sOutline = 'border-width: 2px; border-left: none; border-right: none;';
				break;

			case 4:
				$sOutline = 'border: none;';
				break;
		}
	}

	$sTitleMargin = 'padding-left: 0px;';

	if( isset( $data[ "c" ][ "sTitleMargin" ]))
	{
		$m = intval( $data[ "c" ][ "sTitleMargin" ]);

		$m = $m > 400 ? 400 : $m;
		$m = $m < 0 ? 0 : $m;

		$data[ "c" ][ "sTitleMargin" ] = $m;

		$sTitleMargin = 'padding-left: '.( $m ).'px;';
	}

	$css = file_get_contents( INCLUDES."p_themedesigner.css" );

	$css = str_replace( '{sOutline}', $sOutline, $css );
	$css = str_replace( '{sTitleMargin}', $sTitleMargin, $css );

	$css = str_replace( '{fileLogo}', $fileLogo, $css );
	$css = str_replace( '{fileOuter}', $fileOuter, $css );
	$css = str_replace( '{fileContainer}', $fileContainer, $css );
	$css = str_replace( '{fileContainer2}', $fileContainer2, $css );

	foreach( $colors as $colorVar => $colorValue )
	{
		$css = str_replace( $colorVar, getWebColor( $colorValue ), $css );
	}

	if( !isset( $data[ "Tile" ]) || isset( $_POST[ "submitTheme" ]))
	{
		$data[ "Tile" ] = 0;
	}

	// Override some data from POST (if available)

	if( isset( $_POST[ "BGLayoutTiledH" ]))     $data[ "Tile" ] |= 1;
	if( isset( $_POST[ "BGOuterTiledH" ]))      $data[ "Tile" ] |= 2;
	if( isset( $_POST[ "BGContainerTiledH" ]))  $data[ "Tile" ] |= 4;
	if( isset( $_POST[ "BGContainer2TiledH" ])) $data[ "Tile" ] |= 8;
	if( isset( $_POST[ "BGLayoutTiledV" ]))     $data[ "Tile" ] |= 16;
	if( isset( $_POST[ "BGOuterTiledV" ]))      $data[ "Tile" ] |= 32;
	if( isset( $_POST[ "BGContainerTiledV" ]))  $data[ "Tile" ] |= 64;
	if( isset( $_POST[ "BGContainer2TiledV" ])) $data[ "Tile" ] |= 128;

	$css = str_replace( '{layoutTiling}',
		getTiling( $data[ "Tile" ] & 1, $data[ "Tile" ] & 16 ), $css );

	$css = str_replace( '{outerTiling}',
		getTiling( $data[ "Tile" ] & 2, $data[ "Tile" ] & 32 ), $css );

	$css = str_replace( '{containerTiling}',
		getTiling( $data[ "Tile" ] & 4, $data[ "Tile" ] & 64 ), $css );

	$css = str_replace( '{container2Tiling}',
		getTiling( $data[ "Tile" ] & 8, $data[ "Tile" ] & 128 ), $css );

	// Remove comments from the final CSS file.

	$css = preg_replace( '/\/\*(.+?)\*\//', "", $css );

	if( $lastThemeFile != "" )
	{
		$css_orig = file_get_contents( $lastThemeFile );
	}
	else
	{
		$css_orig = "";
	}

	if( $css != $css_orig )
	{
		// Create new CSS file and delete the previous CSS file.

		$fp = fopen( $themePath."style-".time().".css", "w" );
		fwrite( $fp, $css );
		fclose( $fp );

		if( $lastThemeFile != "" )
		{
			unlink( $lastThemeFile );
		}

		// Store updated data to the database.

		$cusWhere = dbWhere( array( "cusUser" => $useid, "cusIsClub" => $isClub ));

		$result = sql_query( "SELECT `cusid` FROM `customThemes`".$cusWhere."LIMIT 1" );

		if( mysql_num_rows( $result ) == 0 )
		{
			sql_query( "INSERT INTO `customThemes`".dbValues( array(
				"cusUser" => $useid,
				"cusIsClub" => $isClub,
				"cusData" => serialize( $data ))));
		}
		else
		{
			sql_query( "UPDATE `customThemes`".dbSet( array(
				"cusData" => serialize( $data ))).$cusWhere."LIMIT 1" );
		}

		return( true );
	}
	else
	{
		return( false );
	}
}

// ---------------------------------------------------------------------------

function uploadSingleImage( $fileVar, $width = 0, $height = 0,
	$maxFilesize = 0, & $filename, & $errors, $themePath )
{
	$error = checkUploadedFile( $fileVar );

	$lastfile = findNewestFile( $themePath.$fileVar."-*.jpg" );

	$filenameWithoutExt = $themePath.$fileVar."-".time();
	$filename = $filenameWithoutExt.".jpg";

	$allowNoFile = false;

	if( $lastfile != "" )
	{
		$allowNoFile = true;
	}

	if( $error != "" && $error != _UPL_NO_FILE )
	{
		$errors[ $fileVar ] = $error;
		$filename = $lastfile;
		return( false );
	}

	if( $error == _UPL_NO_FILE )
	{
		$filename = $lastfile;

		if( $allowNoFile )
		{
			return( true );
		}
		else
		{
			$errors[ $fileVar ] = $error;
			return( false );
		}
	}

	uploadFile( $fileVar, $filenameWithoutExt, $ext );

	if( $ext != "jpg" )
	{
		$errors[ $fileVar ] = "Image type must be JPEG";
	}
	else
	{
		if( $maxFilesize != 0 && filesize( $filename ) > $maxFilesize )
		{
			$errors[ $fileVar ] = "The image file size must not exceed $maxFilesize bytes.";
		}
		else
		{
			$fileinfo = getimagesize( $filename );

			if( $width != 0 && $fileinfo[ 0 ] > $width )
			{
				$errors[ $fileVar ] = "The image width must be exactly $width pixels.";
			}
			else
			{
				if( $height != 0 && $fileinfo[ 1 ] > $height )
				{
					$errors[ $fileVar ] = "The image height must be exactly $height pixels.";
				}
			}
		}
	}

	if( count( $errors ) > 0 )
	{
		if( file_exists( $filename ))
		{
			unlink( $filename );
		}

		$filename = $lastfile;
		return( false );
	}

	if( $lastfile != "" )
	{
		unlink( $lastfile );
	}

	return( true );
}

// ---------------------------------------------------------------------------

function AVG( $filename )
{
	$rgb1 = PIX( $filename, 0, 0 );
	$rgb2 = PIX( $filename, 0, 1 );
	$rgb3 = PIX( $filename, 1, 0 );
	$rgb4 = PIX( $filename, 1, 1 );

	getRGB( $rgb1, $r1, $g1, $b1 );
	getRGB( $rgb2, $r2, $g2, $b2 );
	getRGB( $rgb3, $r3, $g3, $b3 );
	getRGB( $rgb4, $r4, $g4, $b4 );

	$r = round(( $r1 + $r2 + $r3 + $r4 ) / 4 );
	$g = round(( $g1 + $g2 + $g3 + $g4 ) / 4 );
	$b = round(( $b1 + $b2 + $b3 + $b4 ) / 4 );

	return( getColor( $r, $g, $b ));
}

// ---------------------------------------------------------------------------

function PIX( $filename, $hor = 1, $vert = 1 )
{
	if( $filename == "" || !file_exists( $filename ))
	{
		return( "#ffffff" );
	}

	$info = getimagesize( $filename );
	$image = @imagecreatefromjpeg( $filename );

	if( !$image )
	{
		return( "#ffffff" );
	}

	$x = ( $info[ 0 ] - 1 ) * $hor;
	$y = ( $info[ 1 ] - 1 ) * $hor;
	$count = 0;
	$r = 0;
	$g = 0;
	$b = 0;

	for( $i = $x - 10; $i <= $x + 10; $i++ )
	{
		for( $j = $y - 10; $j <= $y + 10; $j++ )
		{
			if( $i >= 0 && $j >= 0 && $i < $info[ 0 ] && $j < $info[ 1 ])
			{
				$rgb = imagecolorat( $image, $i, $j );

				getRGB( $rgb, $tr, $tg, $tb );

				$r += $tr;
				$g += $tg;
				$b += $tb;

				$count++;
			}
		}
	}

	$r = round( $r / $count );
	$g = round( $g / $count );
	$b = round( $b / $count );

	imagedestroy( $image );

	return( getColor( $r, $g, $b ));
}

// ---------------------------------------------------------------------------

function getRGB( $rgb, & $r, & $g, & $b )
{
	$r = ( $rgb >> 16 ) & 0xFF;
	$g = ( $rgb >> 8 ) & 0xFF;
	$b = $rgb & 0xFF;
}

// ---------------------------------------------------------------------------

function getColor( $r, $g, $b )
{
	return( ( $r << 16 ) + ( $g << 8 ) + $b );
}

// ---------------------------------------------------------------------------

function CONTRAST( $rgb )
{
	getRGB( $rgb, $r, $g, $b );

	$y = round( 0.299 * $r + 0.587 * $g + 0.114 * $b );

	if( $y < 160 )
	{
		$r = round( 200 + 55 * $r / 255 );
		$g = round( 200 + 55 * $g / 255 );
		$b = round( 200 + 55 * $b / 255 );
	}
	else
	{
		$r = round( $r / 3 );
		$g = round( $g / 3 );
		$b = round( $b / 3 );
	}

	return( getColor( $r, $g, $b ));
}

// ---------------------------------------------------------------------------

function HARMONY( $rgb )
{
	getRGB( $rgb, $r, $g, $b );

	if( $r > $g && $r > $b )
	{
		$r = round( $r / 3 );
		$g = round( 200 + 55 * $g / 255 );
		$b = round( 200 + 55 * $b / 255 );
	}
	else
	{
		if( $g > $r && $g > $b )
		{
			$r = round( 200 + 55 * $r / 255 );
			$g = round( $g / 3 );
			$b = round( 200 + 55 * $b / 255 );
		}
		else
		{
			$r = round( 200 + 55 * $r / 255 );
			$g = round( 200 + 55 * $g / 255 );
			$b = round( $b / 3 );
		}
	}

	return( getColor( $r, $g, $b ));
}

// ---------------------------------------------------------------------------

function getSwappedColor( $rgb )
{
	getRGB( $rgb, $r, $g, $b );

	$t = $r;
	$r = $b;
	$b = $t;

	return( getColor( $r, $g, $b ));
}

// ---------------------------------------------------------------------------

function RICH( $rgb, $mult = 1.5 )
{
	getRGB( $rgb, $r, $g, $b );

	$r = getRicherComponent( $r, $mult );
	$g = getRicherComponent( $g, $mult );
	$b = getRicherComponent( $b, $mult );

	return( getColor( $r, $g, $b ));
}

// ---------------------------------------------------------------------------

function getRicherComponent( $c, $mult = 1.5 )
{
	$c = round(((( $c / 255 - 0.5 ) * $mult ) + 0.5 ) * 255 );

	return( $c > 255 ? 255 : ( $c < 0 ? 0 : $c ));
}

// ---------------------------------------------------------------------------

function BRIGHT( $rgb, $mult = 1.5 )
{
	getRGB( $rgb, $r, $g, $b );

	$darker = false;

	if( $mult < 0 )
	{
		$darker = true;
		$mult = -$mult;
	}

	$a = round(( $r - 128 ) * $mult - ( $r - 128 ));
	$ag = round(( $g - 128 ) * $mult - ( $g - 128 ));
	$ab = round(( $b - 128 ) * $mult - ( $b - 128 ));

	if( abs( $ag ) > abs( $a )) $a = $ag;
	if( abs( $ab ) > abs( $a )) $a = $ab;

	if( $darker )
	{
		$r = $r - $a;
		$g = $g - $a;
		$b = $b - $a;
	}
	else
	{
		$r = $r + $a;
		$g = $g + $a;
		$b = $b + $a;
	}

	$r = $r > 255 ? 255 : ( $r < 0 ? 0 : $r );
	$g = $g > 255 ? 255 : ( $g < 0 ? 0 : $g );
	$b = $b > 255 ? 255 : ( $b < 0 ? 0 : $b );

	return( getColor( $r, $g, $b ));
}

// ---------------------------------------------------------------------------

function tooClose( $color1, $color2, $thres = 32 )
{
	getRGB( $color1, $r1, $g1, $b1 );
	getRGB( $color2, $r2, $g2, $b2 );

	$y1 = 0.299 * $r1 + 0.587 * $g1 + 0.114 * $b1;
	$y2 = 0.299 * $r2 + 0.587 * $g2 + 0.114 * $b2;

	return( abs( $y1 - $y2 ) < $thres );
}

// ---------------------------------------------------------------------------

function getWebColor( $rgb )
{
	getRGB( $rgb, $r, $g, $b );

	return( "#".sprintf( "%02x%02x%02x", $r, $g, $b ));
}

// ---------------------------------------------------------------------------

function getTiling( $isHorizontal, $isVertical )
{
	if( $isHorizontal && $isVertical )
		return( "repeat" );
	else
		if( $isHorizontal )
			return( "repeat-x" );
		else
			if( $isVertical )
				return( "repeat-y" );
			else
				return( "no-repeat" );
}

// ---------------------------------------------------------------------------

?>
