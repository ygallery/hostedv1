<?

$_pollUser = $_auth[ "useid" ];

$_documentTitle = _UPDATES;

if( !$_auth[ "useid" ])
{
	header("Status: 403 Forbidden");
	?>
	<div class="header">
		<div class="header_title"><?= $_documentTitle ?></div>
	</div>
	<div class="container">
		<? notice( _REQUIRE_LOGIN ) ?>
	</div>
	<?

	return;
}

$_commentsPerPage = 10;

if( isset( $_POST[ "markAsRead" ]))
{
	// Mark selected updates as read.

	if( markUpdatesAsRead() )
	{
		redirect( url( "." ));
	}
}

	

if( isset( $_POST[ "markAllObjects" ])) {
	if ( markAllObjectsAsRead() ) {
		redirect( url( "." ));
	}
}

// Fetch updates for simple messages.

$creator = $_auth[ "useid" ];

$messageUpdTypes = array( updTypeMessageFav, updTypeMessageWatch,
	updTypeMessageAbuse, updTypeMessageDeleted, updTypeMessageRestored,
	updTypeMessageClub, updTypeMessageClubMember, updTypeMessageClubDeclined,
	updTypeMessageClubRefused, updTypeMessageWatchClub, updTypeMessageHelpUpdate, updTypeMessageHelpResolved );

$result = sql_query( "SELECT * FROM `updates` ".
	"WHERE `updCreator` = '$creator' AND ".
	"`updType` IN ('".implode( "','", $messageUpdTypes )."') ".
	"ORDER BY `updDate` DESC LIMIT 20" );

$updMessages = array();

while( $updData = mysql_fetch_assoc( $result ))
	$updMessages[ "upd_".$updData[ "updType" ]."_".$updData[ "updObj" ]."_".$updData[ "updUser" ]] = $updData;

// Fetch updates for comments.

$pageCurrent = isset( $_GET[ "page" ]) ? intval( $_GET[ "page" ]) : 1;

if( $pageCurrent < 1 )
	$pageCurrent = 1;

$cmtOffset = ( $pageCurrent - 1 ) * $_commentsPerPage;

$cmtSort = isset( $_GET[ "sort" ]) ? intval( $_GET[ "sort" ]) :
	( isset( $_COOKIE[ "yGalCmtSort" ]) ? intval( $_COOKIE[ "yGalCmtSort" ]) : 0 );

setcookie( "yGalCmtSort", $cmtSort, strtotime( "+9 years" ), "/", ".".$_config[ "galRoot" ]);

$result = sql_query( "SELECT * FROM `updates`, `comments` ".
	"WHERE `updCreator` = '$creator' ".
	"AND `updObj` = `comid` ".
	"AND `updType` = '".updTypeComment."' ".
	"ORDER BY `comid` ".( $cmtSort == 0 ? "DESC " : "" ).
	"LIMIT $cmtOffset, $_commentsPerPage" );

$updComments = array();

while( $updData = mysql_fetch_assoc( $result ))
	$updComments[ "upd_".$updData[ "updType" ]."_".$updData[ "updObj" ]."_".$updData[ "updUser" ]] = $updData;

// Fetch updates for journals/polls.

$result = sql_query("SELECT * FROM `updates` ".
	"WHERE `updCreator` = '$creator' ".
	"AND `updType` IN ('".updTypeJournal."','".updTypeJournalPoll."','".updTypeAnnouncement."') ".
	"ORDER BY `updDate` DESC LIMIT 10");

$updJournals = array();

while( $updData = mysql_fetch_assoc( $result ))
	$updJournals[ "upd_".$updData[ "updType" ]."_".$updData[ "updObj" ]."_".$updData[ "updUser" ]] = $updData;

?>
<div class="header">
	<div class="header_title">
		<?= _UPDATES ?>
		<div class="subheader"><?= _UPD_EXPLAIN ?></div>
	</div>
	<div style="clear: both" class="normaltext">
		<?

		function __showUpdatesTab( $id, $icon, $title, $count, $insertCmtId = false,
			$insertJouId = false )
		{
			?>
			<div id="bar_<?= $id ?>" class="tab"
				onclick="open_tab( this, 'tabs', 'tab_<?= $id ?>' )">
				<?

				echo getIMG( url()."images/emoticons/".$icon )." ".$title;

				if( $count > 0 )
				{
					echo " (".
						( $insertCmtId ? '<span id="_updCmtCnt">' : '' ).
						( $insertJouId ? '<span id="_updJouCnt">' : '' ).
						fuzzy_number( $count ).
						( $insertCmtId | $insertJouId ? '</span>' : '' ).
						")";
				}

				?>
			</div>
			<?
		}

		__showUpdatesTab( "messages", "watch.png", _MESSAGES,
			$_auth[ "useUpdFav" ] + $_auth[ "useUpdWat" ]);

		__showUpdatesTab( "comments", "comment.png", _COMMENTS,
			$_auth[ "useUpdCom" ], true );

		__showUpdatesTab( "journals", "journal.png", _JOURNALS,
			$_auth[ "useUpdJou" ], false, true );

		if( isset( $_GET[ "newartview" ]))
		{
			__showUpdatesTab( "art", "submission.png", _SUBMISSIONS, 0 );
			__showUpdatesTab( "collabs", "club.png", _COLLABS, 0 );
			__showUpdatesTab( "clubs", "club3.png", _CLUBS, 0 );

			if( isExtras() )
			{
				__showUpdatesTab( "extras", "star4.png", _SUBMIT_TYPE_EXTRA, 0 );
			}
		}
		else
		{
			__showUpdatesTab( "art", "submission.png", _SUBMISSIONS,
				$_auth[ "useUpdObj" ]);

			if( isExtras() )
			{
				__showUpdatesTab( "extras", "star4.png", _SUBMIT_TYPE_EXTRA,
					$_auth[ "useUpdExt" ]);
			}
		}

		?>
		<div class="clear">&nbsp;</div>
	</div>
</div>

<div class="container">
	<?

	// -------------------------------------------------------------------------
	// Messages
	// -------------------------------------------------------------------------

	?>
	<div id="tab_messages">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td valign="top" width="100%">
		<div class="header">
			<?= _MESSAGES ?>
		</div>
		<form action="<?= url( "updates/messages" ) ?>" method="post">
			<div class="container2">
				<?

				$allIds = array();

				if( count( $updMessages ) == 0 )
				{
					?>
					<div><?= _UPD_NO_MESSAGES ?></div>
					<?
				}

				reset( $updMessages );

				foreach($updMessages as $key => $data)
				{
					$message = ""; // This is the message shown to the user

					switch( $data[ "updType" ])
					{
						case updTypeMessageFav:

							$user = getUserLink( $data[ "updUser" ]);

							$result = sql_query( "SELECT `objTitle` FROM `objects` ".
								"WHERE `objid` = '".$data[ "updObj" ]."'" );

							if( mysql_num_rows( $result ) == 0 )
								continue;

							$objTitle = mysql_result( $result, 0 );

							$object = '<a href="'.url( "view/".$data[ "updObj" ]).'">'.
								strip_tags( formatText( $objTitle )).'</a>';

							$message = getIMG( url()."images/emoticons/fav1.png" )." ".
								sprintf( _UPD_MESSAGE_FAV, $user, $object );

							break;
							
						case updTypeMessageWatch:

							$user = getUserLink( $data[ "updUser" ]);

							$message = getIMG( url()."images/emoticons/watch.png" )." ".
								sprintf( _UPD_MESSAGE_WATCH, $user );

							break;

						case updTypeMessageHelpUpdate:

							$hdCase = '<a href="'.url("helpdesk/details/".$data[ "updObj" ]).'">#'.$data[ "updObj" ].'</a>';

							$message = getIMG( url()."images/emoticons/help.png" )." ".
								sprintf( _UPD_MESSAGE_HELPDESK_UPDATED, $hdCase );

							break;
						
						case updTypeMessageHelpResolved:

							$hdCase = '<a href="'.url("helpdesk/details/".$data[ "updObj" ]).'">#'.$data[ "updObj" ].'</a>';
														
							$message = getIMG( url()."images/emoticons/help.png" )." ".
								sprintf( _UPD_MESSAGE_HELPDESK_RESOLVED, $hdCase );

							break;

						case updTypeMessageAbuse:

							if( $data[ "updUser" ] == 0 )
							{
								$isExtras = false;
								$_objects = "`objects`";
							}
							else
							{
								$isExtras = true;
								$_objects = "`extras`";
							}

							$result = sql_query( "SELECT `objTitle` FROM $_objects ".
								"WHERE `objid` = '".$data[ "updObj" ]."'" );

							if( mysql_num_rows( $result ) == 0 )
								continue;

							$objTitle = mysql_result( $result, 0 );

							$object = '<a href="'.url( "view/".( $isExtras ? "e" : "" ).$data[ "updObj" ]).'">'.
								strip_tags( formatText( $objTitle )).'</a>';

							$message = getIMG( url()."images/emoticons/abuse.png" )." ".
								sprintf( _UPD_MESSAGE_ABUSE, $object );

							break;

						case updTypeMessageDeleted:

							if( $data[ "updUser" ] == 0 )
							{
								$isExtras = false;
								$_objects = "`objects`";
							}
							else
							{
								$isExtras = true;
								$_objects = "`extras`";
							}

							$result = sql_query( "SELECT `objTitle` FROM $_objects ".
								"WHERE `objid` = '".$data[ "updObj" ]."'" );

							if( mysql_num_rows( $result ) == 0 )
								continue;

							$objTitle = mysql_result( $result, 0 );

							$object = '<a href="'.url( "view/".( $isExtras ? "e" : "" ).$data[ "updObj" ]).'">'.
								strip_tags( formatText( $objTitle )).'</a>';

							$message = getIMG( url()."images/emoticons/cancel.png" )." ".
								sprintf( _UPD_MESSAGE_DELETED, $object );

							// Also show the explanation why it has been deleted.
							// Removed as the rest of the notice is sent to the user via PM now.
							/*
							$result = sql_query( "SELECT `abuReason` FROM `abuses` ".
								"WHERE `abuObj` = '".$data[ "updObj" ]."' ".
								"ORDER BY `abuSubmitDate` DESC LIMIT 1" );

							if( $abuData = mysql_fetch_assoc( $result ))
							{
								$explanation = $abuData[ "abuReason" ];

								if( trim( $explanation ) != "" )
								{
									$message .= '<div class="notsowide mar_left mar_bottom error">'.
										'<b>'._ABUSE_REASON.'</b>: '.
										formatText( $explanation ).'</div>';
								}
							}
							*/

							break;

						case updTypeMessageRestored:

							if( $data[ "updUser" ] == 0 )
							{
								$isExtras = false;
								$_objects = "`objects`";
							}
							else
							{
								$isExtras = true;
								$_objects = "`extras`";
							}

							$result = sql_query( "SELECT `objTitle` FROM $_objects ".
								"WHERE `objid` = '".$data[ "updObj" ]."'" );

							if( mysql_num_rows( $result ) == 0 )
								continue;

							$objTitle = mysql_result( $result, 0 );

							$object = '<a href="'.url( "view/".( $isExtras ? "e" : "" ).$data[ "updObj" ]).'">'.
								strip_tags( formatText( $objTitle )).'</a>';

							$message = getIMG( url()."images/emoticons/checked.png" )." ".
								sprintf( _UPD_MESSAGE_RESTORED, $object );

							break;

						case updTypeMessageClub:

							$result = sql_query( "SELECT `cluName` FROM `clubs` ".
								"WHERE `cluid` = '".$data[ "updObj" ]."'" );

							if( mysql_num_rows( $result ) == 0 )
								continue;

							$objTitle = mysql_result( $result, 0 );

							$object = '<a href="'.url( "club/".$data[ "updObj" ]).'">'.
								strip_tags( formatText( $objTitle )).'</a>';

							$message = getIMG( url()."images/emoticons/club2.png" )." ".
								sprintf( _UPD_MESSAGE_CLUB, $object );

							break;

						case updTypeMessageClubDeclined:

							$result = sql_query( "SELECT `cluName` FROM `clubs` ".
								"WHERE `cluid` = '".$data[ "updObj" ]."'" );

							if( mysql_num_rows( $result ) == 0 )
								continue;

							$cluName = mysql_result( $result, 0 );

							$club = '<a href="'.url( "club/".$data[ "updObj" ]).'">'.
								$cluName.'</a>';

							$message = getIMG( url()."images/emoticons/club2.png" )." ".
								sprintf( _UPD_MESSAGE_CLUB_DECLINED, $club );

							break;

						case updTypeMessageClubRefused:

							$result = sql_query( "SELECT `objTitle` FROM `objects` ".
								"WHERE `objid` = '".$data[ "updObj" ]."'" );

							if( mysql_num_rows( $result ) == 0 )
								continue;

							$objTitle = mysql_result( $result, 0 );

							$object = '<a href="'.url( "view/".$data[ "updObj" ]).'">'.
								strip_tags( formatText( $objTitle )).'</a>';

							$result = sql_query( "SELECT `cluName` FROM `clubs` ".
								"WHERE `cluid` = '".$data[ "updUser" ]."'" );

							if( mysql_num_rows( $result ) == 0 )
								continue;

							$cluName = mysql_result( $result, 0 );

							$club = '<a href="'.url( "club/".$data[ "updUser" ]).'">'.
								$cluName.'</a>';

							$message = getIMG( url()."images/emoticons/club2.png" )." ".
								sprintf( _UPD_MESSAGE_CLUB_REFUSED, $object, $club );

							break;

						case updTypeMessageClubMember:

							$result = sql_query( "SELECT `cluName`, `cluRequireAccept` ".
								"FROM `clubs`, `cluExtData` ".
								"WHERE `cluEid` = `cluid` ".
								"AND `cluid` = '".$data[ "updObj" ]."' LIMIT 1" );

							if( $cluData = mysql_fetch_assoc( $result ))
							{
								$user = getUserLink( $data[ "updUser" ]);

								$object = '<a href="'.url( "club/".$data[ "updObj" ]).'">'.
									$cluData[ "cluName" ].'</a>';

								$message = getIMG( url()."images/emoticons/club2.png" )." ".
									sprintf( $cluData[ "cluRequireAccept" ] ?
									_UPD_MESSAGE_CLUB_MEMBER_PENDING :
									_UPD_MESSAGE_CLUB_MEMBER, $user, $object );
							}

							break;

						case updTypeMessageWatchClub:

							$user = getUserLink( $data[ "updUser" ]);

							$result = sql_query( "SELECT `cluName` FROM `clubs` ".
								"WHERE `cluid` = '".$data[ "updObj" ]."'" );

							if( mysql_num_rows( $result ) == 0 )
								continue;

							$cluName = mysql_result( $result, 0 );

							$club = '<a href="'.url( "club/".$data[ "updObj" ]).'">'.
								$cluName.'</a>';

							$message = getIMG( url()."images/emoticons/watch.png" )." ".
								sprintf( _UPD_MESSAGE_WATCH_CLUB, $user, $club );

							break;
					}

					$id = "id_".$key;

					$allIds[] = $id;

					?>
					<div>
						<input class="checkbox" id="<?= $id ?>" name="<?= $key ?>"
							type="checkbox" /> <?= $message ?>
					</div>
					<?
				}

				?>
			</div>
			<div class="sep">
				<?

				$scriptSelectAll = "var el = 0; ";

				foreach( $allIds as $id )
				{
					$scriptSelectAll .= "el = get_by_id( '$id' ); el.checked = true; ";
				}

				?>
				<button type="button" class="submit"
					onclick="<?= $scriptSelectAll ?>return false;">
					<?= _SELECT_ALL ?>
				</button>
				<button class="submit" name="markAsRead" type="submit">
					<?= _MARK_AS_READ ?>
				</button>
			</div>
		</form>
	</td>
	<td valign="top" style="padding-left: 20px">
		<div class="header">
			<?= _NEWS ?>
		</div>
		<table cellspacing="4" cellpadding="0" border="0" width="300">
		<?

		$newResult = sql_query( "SELECT * FROM `news` ".
			"ORDER BY `newSubmitDate` DESC LIMIT 5" );

		$first = true;

		while( $newData = mysql_fetch_assoc( $newResult ))
		{
			$time = strtotime( $newData[ "newSubmitDate" ]);
			$bold = ( time() - $time < 60 * 60 * 24 * 3 );

			?>
			<tr>
				<td class="nowrap" align="center" valign="top">
					<?= $bold ? "<b>" : "" ?><?= gmdate( "M d", applyTimezone( $time )) ?>
					<?= $bold ? "</b>" : "" ?></td>
				<td valign="top" align="center">
					<?= $bold ? getIMG( urlf()."images/emoticons/star.png" ) : "&middot;" ?></td>
				<td width="100%" valign="top">
					<?= $bold ? "<b>" : "" ?><a href="<?= url( "news/".$newData[ "newid" ]) ?>">
					<?= $newData[ "newSubject" ] ?></a><?= $bold ? "</b>" : "" ?></td>
			</tr>
			<?
		}

		mysql_free_result( $newResult );

		?>
		</table>
	</td>
	</tr>
	</table>
	</div>
	<?

	// -------------------------------------------------------------------------
	// Comments
	// -------------------------------------------------------------------------

	?>
	<div id="tab_comments">
		<div class="f_right">
			<form action="<?= url( "updates/comments" ) ?>" method="get">
				<select name="sort" onchange="this.form.submit()" class="smalltext">
					<option <?= $cmtSort == 0 ? 'selected="selected"' : "" ?> value="0"><?= _NEWEST_FIRST ?></option>
					<option <?= $cmtSort == 1 ? 'selected="selected"' : "" ?> value="1"><?= _OLDEST_FIRST ?></option>
				</select>
			</form>
		</div>
		<?

		$saveCurrentPageURL = $_currentPageURL;
		$_currentPageURL = url( "updates/comments" );

		iefixStart();

		?>
		<div class="header">
			<?

			if( $_auth[ "useUpdCom" ] <= $_commentsPerPage )
			{
				echo _COMMENTS;
			}
			else
			{
				// Display multiple pages.

				echo _UPD_COMMENTS_PAGE;

				$pageCount = ceil( $_auth[ "useUpdCom" ] / $_commentsPerPage );

				for( $i = 1; $i <= $pageCount; $i++ )
				{
					if( $i == $pageCurrent )
					{
						echo " - <u>$i</u> ";
					}
					else
					{
						echo ' - <a href="'.url( "updates/comments",
							array( "page" => $i )).'">'.$i.'</a>';
					}
				}
			}

			?>
		</div>

		<div>
			<?

			$allIds = array();

			if( count( $updComments ) == 0 )
			{
				?>
				<div class="container2"><?= _UPD_NO_COMMENTS ?></div>
				<?
			}

			include_once( INCLUDES."comments.php" );

			?>
			<div class="notsowide">
				<?

				reset( $updComments );

				foreach( $updComments as $key => $data )
				{
					$id = "id_cmt".$key;
					$allIds[] = $id;

					$result = sql_query( "SELECT * FROM `comments` ".
						"WHERE `comid` = '".$data[ "updObj" ]."' LIMIT 1" );

					if( !$comData = mysql_fetch_assoc( $result ))
						continue; // The comment no longer exists

					getCommentParent( $comData, $parentURL, $parentType, $title, true );

					switch( $comData[ "comRootObjType" ])
					{
						case "obj":
							$icon = "submission.png";
							break;

						case "ext":
							$icon = "star4.png";
							break;

						case "com":
							$icon = "comment.png";
							break;

						case "clu":
							$icon = "club2.png";
							break;

						case "jou":
							$icon = "journal.png";
							break;

						case "pol":
							$icon = "poll.png";
							break;

						case "new":
							$icon = "new.png";
							break;

						case "use":
							$icon = "avatar_v2.png";
							break;

						default:
							$icon = "cancel.png";
					}

					$markAsReadId = "id_mark".$key;

					?>
					<a name="<?= $key ?>"></a>
					<a name="<?= $comData[ "comid" ] ?>"></a>

					<div id="<?= $markAsReadId ?>" class="comment" data-timestamp="<?= $comData[ "comSubmitDate" ] ?>">
						<div class="mar_bottom" id="<?= $markAsReadId ?>_">
							<div style="padding: 2px" class="commentHeader">
								<?= getIMG( url()."images/emoticons/".$icon ) ?>
								<?= sprintf( _UPD_COMMENT_ON, $parentType.
									' <a href="'.$parentURL.'">'.strip_tags( formatText( $title )).'</a>' ) ?>:
							</div>
							<div style="margin-top: -8px">
								<?

								showComment( $comData, 0, true, $markAsReadId );

								?>
							</div>
						</div>
					</div>
					<?
				}
			?>
			</div>
		</div>
		<?

		iefixEnd();

		$_currentPageURL = $saveCurrentPageURL;

		?>
	</div>
	<?

	// -------------------------------------------------------------------------
	// Journals/Polls/Announcements
	// ------------------------------------------------------------------------

	?>
	<div id="tab_journals">
		<div class="header">
			<?= _JOURNALS ?>
		</div>
		<form action="<?= url( "updates/journals" ) ?>" method="post">
			<?

			if( count( $updJournals ) == 0 )
			{
				?>
				<div class="container2">
					<?= _UPD_NO_JOURNALS ?>
				</div>
				<?
			}

			$scriptMarkAll = "";

			reset( $updJournals );

			foreach( $updJournals as $key => $data )
			{
				$avatar = "";
				$title = "["._DELETED."]";
				$url = "";
				$caption = "";
				$body = "";
				$markAsReadOp = "?";

				switch( $data[ "updType" ])
				{
					case updTypeJournal:
					{
						$markAsReadOp = "mj";
						$user = getUserLink( $data[ "updUser" ]);
						$avatar = getUserAvatar( "",$data[ "updUser" ], true );

						$usernameData = getUserData( $data[ "updUser" ]);
						$username = $usernameData[ "useUsername" ];

						$result = sql_query( "SELECT `jouTitle`,LEFT(`jouEntry`,500) AS `jouText` FROM `journals` ".
							"WHERE `jouid` = '".$data[ "updObj" ]."'" );

						if( mysql_num_rows( $result ) > 0 )
						{
							$title = mysql_result( $result, 0, 0 );
							$body = mysql_result( $result, 0, 1 );
							$url = url( "journal/".strtolower( $username ).
								"/".$data[ "updObj" ]);
						}

						$caption = getIMG( url()."images/emoticons/journal.png" )." ".
							sprintf( _UPD_JOURNAL, $user, "" );

						break;
					}

					case updTypeJournalPoll:
					{
						$markAsReadOp = "mp";
						$user = getUserLink( $data[ "updUser" ]);
						$avatar = getUserAvatar( "",$data[ "updUser" ], true );

						$usernameData = getUserData( $data[ "updUser" ]);
						$username = $usernameData[ "useUsername" ];

						$result = sql_query( "SELECT `polSubject`,LEFT(`polComment`,500) AS `polText` FROM `polls` ".
							"WHERE `polid` = '".$data[ "updObj" ]."'" );

						if( mysql_num_rows( $result ))
						{
							$title = mysql_result( $result, 0, 0 );
							$body = mysql_result( $result, 0, 1 );
							$url = url( "poll/".strtolower( $username )."/".
								$data["updObj"]);
						}

						$caption = getIMG( url()."images/emoticons/poll.png" )." ".
							sprintf( _UPD_POLL, $user, "" );

						break;
					}

					case updTypeAnnouncement:
					{
						$markAsReadOp = "ma";
						$clubname = "["._UNKNOWN."]";

						$result = sql_query( "SELECT `cluName` FROM `clubs` ".
							"WHERE `cluid` = '".$data[ "updUser" ]."' LIMIT 1" );

						if( mysql_num_rows( $result ) > 0 )
						{
							$clubname = mysql_result( $result, 0 );
						}

						$result = sql_query( "SELECT `jouTitle`,LEFT(`jouEntry`,500) AS `jouText`,`jouAnnCreator` FROM `journals` ".
							"WHERE `jouid` = '".$data[ "updObj" ]."'" );

						if( mysql_num_rows( $result ) > 0 )
						{
							$title = mysql_result( $result, 0, 0 );
							$body = mysql_result( $result, 0, 1 );
							$author = mysql_result( $result, 0, 2 );
							$url = url( "announcement/".$data[ "updUser" ].
								"/".$data[ "updObj" ]);
							$avatar = getUserAvatar( "",$author, true );
						}

						$club = '<a href="'.url( "club/".$data[ "updUser" ]).'">'.
							$clubname.'</a>';

						$caption = getIMG( url()."images/emoticons/club2.png" )." ".
							sprintf( _UPD_ANNOUNCEMENT, $club, "" );

						break;
					}
				}

				$jouDivId = "id_jou".$key;

				?>
				<div id="<?= $jouDivId ?>">
					<div class="mar_left" style="margin-bottom : 0.2em;">
						<?= $caption ?>
					</div>
					<div class="container2 notsowide" style="margin-bottom : 12px; padding : 0; overflow : hidden;">
						<div class="f_right a_center normaltext" style="margin : 8px;">
							<?= $avatar ?>
						</div>
						<div class="largetext" style="padding : 8px;">
							<b><?= formatText( $title, false, true ) ?></b>
						</div>
						<div style="padding : 0 8px; min-height : 9em; max-height : 11em; position : relative;">
							<div>
								<?= formatText( $body ) ?>
							</div>
							<div class="container" style="padding : 4px 8px; position : absolute; <?
								?>left : 0; bottom : 0; right : 0;">
								<a style="float : right;" href="<?= $url ?>">
									<span class="button smalltext">
										<?= _REPLY ?>
									</span>
								</a>
								<?
			    
								$marId = "id_joumark1".$key;
								$marId2 = "id_joumark2".$key;
			    
								$script =
									"$('".$marId."').hide(); ".
									"$('".$marId2."').show(); ";
			    
								$script2 =
									"add_operation( '".$markAsReadOp.$data[ "updObj" ]."' ); ".
									"journal_count--; ".
									"$('_updJouCnt').innerHTML = fuzzy_number( journal_count ); ".
									"$('_globJouCnt').innerHTML = fuzzy_number( journal_count ); ".
									"$('".$jouDivId."').hide(); ";

								//$scriptMarkAll .= $script2;

								$scriptMarkAll .= "add_operation( '".$markAsReadOp.$data[ "updObj" ]."' );";
			    
								?>
								<div id="<?= $marId ?>" onclick="<?= $script ?>" style="float : right;" class="button smalltext">
									<?= _MARK_AS_READ ?>
								</div>
								<div id="<?= $marId2 ?>" onclick="<?= $script2 ?>"
									style="display : none; float : right;" class="button smalltext">
									<?=_MARK_AS_READ?>: <span class="error"><?=_CLICK_TO_CONFIRM ?></span>
								</div>
								<a href="<?= $url ?>">
									<?= _READ_MORE ?>
									<?= getIMG( url()."images/emoticons/nav-next.png" ) ?>
								</a>
							</div>
						</div>
					</div>
				</div>
				<?
			}

			if( count( $updJournals ) > 0 )
			{
				$marId = "id_joumarkall1";
				$marId2 = "id_joumarkall2";
			
				$script =
					"$('".$marId."').hide(); ".
					"$('".$marId2."').show(); ";

				//$scriptMarkAll .= "$('".$marId2."').hide();";

				$scriptMarkAll .= "document.location='".url( "updates/journals" )."';";
			
				?>
				<div id="<?= $marId ?>" onclick="<?= $script ?>" class="button">
					<?= _MARK_ALL_AS_READ ?>
				</div>
				<div id="<?= $marId2 ?>" onclick="<?= $scriptMarkAll ?>"
					style="display : none;" class="button">
					<?= _MARK_ALL_AS_READ ?>:
					<span class="error"><?=_CLICK_TO_CONFIRM ?></span>
				</div>
				<div style="clear : both;"></div>
				<?
			}

			?>
		</form>
	</div>
	<?

	// -------------------------------------------------------------------------
	// Submissions
	// ------------------------------------------------------------------------

	?>
	<div id="tab_art">
		<?

		$isCollabs = false;
		$isClubs = false;
		$isExtras = false;
		include( INCLUDES."mod_artview.php" );

		?>
	</div>
	<?

	if( isset( $_GET[ "newartview" ]))
	{
		// -------------------------------------------------------------------------
		// Collabs
		// ------------------------------------------------------------------------

		?>
		<div id="tab_collabs">
			<?

			$isCollabs = true;
			$isClubs = false;
			$isExtras = false;
			include( INCLUDES."mod_artview.php" );

			?>
		</div>
		<?

		// -------------------------------------------------------------------------
		// Clubs/projects
		// ------------------------------------------------------------------------

		?>
		<div id="tab_clubs">
			<?

			$isCollabs = false;
			$isClubs = true;
			$isExtras = false;
			include( INCLUDES."mod_artview.php" );

			?>
		</div>
		<?
	}

	// -------------------------------------------------------------------------
	// Extras
	// ------------------------------------------------------------------------

	if( isExtras() )
	{
		?>
		<div id="tab_extras">
			<?

			$isCollabs = false;
			$isClubs = false;
			$isExtras = true;
			include( INCLUDES."mod_artview.php" );

			?>
		</div>
		<?
	}

	?>
</div>

<script type="text/javascript">
//<![CDATA[
	toggle_visibility( "tab_messages" );
	toggle_visibility( "tab_comments" );
	toggle_visibility( "tab_journals" );
	toggle_visibility( "tab_art" );
	<?

	if( isset( $_GET[ "newartview" ]))
	{
		?>
		toggle_visibility( "tab_collabs" );
		toggle_visibility( "tab_clubs" );
		<?
	}

	if( isExtras() )
	{
		?>
		toggle_visibility( "tab_extras" );
		<?
	}

	$default_page = "messages";

	// Auto-click on the tab that has some updates.

	if( !isset( $_GET[ "newartview" ]))
	{
		if( isExtras() )
		{
			if( $_auth[ "useUpdExt" ] > 0 )
				$default_page = "extras";
		}

		if( $_auth[ "useUpdObj" ] > 0 )
			$default_page = "art";
	}

	if( $_auth[ "useUpdJou" ] > 0 )
		$default_page = "journals";

	if( $_auth[ "useUpdCom" ] > 0 )
		$default_page = "comments";

	if( $_auth[ "useUpdFav" ] + $_auth[ "useUpdWat" ] > 0 )
		$default_page = "messages";

	$current_page = preg_replace( '/[^0-9a-z]/', "",
		$_cmd[ 1 ] == "" ? $default_page : $_cmd[ 1 ]);

	?>
	var bar = get_by_id( "bar_<?= $current_page ?>" );
	open_tab( bar, "tabs", "tab_<?= $current_page ?>" );
//]]>
</script>
