<?

$logReaders = preg_split( '/[^0-9]/', $_config[ "logReaders" ], -1, PREG_SPLIT_NO_EMPTY );

if( !in_array( $_auth[ "useid" ], $logReaders ) )
{
	return;
}

?>
<div class="header">
	<div class="header_title">
		Log file
	</div>
</div>
<div class="container">
<?

$logFile = "_log.html";

if( isset( $_GET[ "clear" ]))
{
	fclose( fopen( $logFile, "w" ));
	redirect( url( "." ));
}

if( isset( $_GET[ "disable" ]))
{
	sql_query( "UPDATE `config` SET `conValue` = '1' WHERE `conName` = 'logDisabled'" );
	redirect( url( "." ));
}

if( isset( $_GET[ "enable" ]))
{
	sql_query( "UPDATE `config` SET `conValue` = '0' WHERE `conName` = 'logDisabled'" );
	redirect( url( "." ));
}

if( file_exists( $logFile ) && filesize( $logFile ) > 0 )
{
	?>
	<div class="mar_bottom">
		<?= file_get_contents( $logFile ) ?>
	</div>
	<div class="hline"><br /></div>
	<div class="largetext">
		<a onclick="return confirm('<?= _ARE_YOU_SURE ?>');"
			href="<?= url( ".", array( "clear" => "yes" )) ?>">Clear log</a>
	</div>
	<?
}
else
{
	notice( "Log file is empty" );
}

$action = $_config[ "logDisabled" ] ? "enable" : "disable";
$title = $_config[ "logDisabled" ] ? "Enable log" : "Disable log";

?>
<div class="hline"><br /></div>
<div class="largetext">
	<a onclick="return confirm('<?= _ARE_YOU_SURE ?>');"
		href="<?= url( ".", array( $action => "yes" )) ?>"><?= $title ?></a>
</div>

</div>
