<?

function notFound()
{
	include(INCLUDES."p_notfound.php");
	return;
}

$objid = intval( $_cmd[ 1 ]);

if( substr( $_cmd[ 1 ], 0, 1 ) == "e" )
{
	notFound();
}

$where = "`objid` = '".$objid."' ".
	"AND `objEid` = `objid` ".
	"AND `objDeleted` = '0' ".
	"AND `objPending` = '0' ".
	"AND `objGuestAccess` = '1'";

applyObjFilters( $where );

$objResult = sql_query( "SELECT `objCreator`,`objCollab`,`objTitle`,`objComment`,`objExtension`,`objLastEdit` ".
	"FROM `objects`,`objExtData` WHERE $where LIMIT 1" );

if( mysql_num_rows( $objResult ) == 0 )
{
	notFound();
}

$objData = mysql_fetch_assoc( $objResult );

$useResult = sql_query( "SELECT `useUsername` FROM `users` ".
	"WHERE `useid` = '".intval( $objData[ "objCreator" ])."' LIMIT 1" );

if( mysql_num_rows( $useResult ) == 0 )
{
	notFound();
}

$username = mysql_result( $useResult, 0, 0 );

if( $objData[ "objCollab" ] != 0 )
{
	$useResult = sql_query( "SELECT `useUsername` FROM `users` ".
		"WHERE `useid` = '".intval( $objData[ "objCollab" ])."' LIMIT 1" );

	if( mysql_num_rows( $useResult ) == 0 )
	{
		notFound();
	}

	$username .= " and ".mysql_result( $useResult, 0, 0 );
}

echo '<?xml version="1.0" encoding="UTF-8" ?>';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title><?= $objData[ "objTitle" ] ?> by <?= $username ?> :: <?= $_config[ "galName" ] ?> :: <?= $_config[ "galSubname" ] ?></title>
</head>
<body>
	<?

	$urlBase = "http://".$_config[ "galRoot" ];

	?>
	<h1><?= $_config[ "galName" ] ?> - <?= $_config[ "galSubname" ] ?></h1>
	<h2><?= $objData[ "objTitle" ] ?></h2>
	<p>by <cite><a href="<?= $urlBase ?>/user/<?= strtolower( $username ) ?>/"><?= $username ?></a></cite></p>
	<blockquote><?= str_replace( "\n", "<br />", $objData[ "objComment" ]) ?></blockquote>
	<?

	switch( $objData[ "objExtension" ])
	{
		case "html":
		case "txt":
		{
			?>
			<h3>Text</h3>
			<blockquote>
			<?

			function applyIdToPath($path, $id)
			{
				$subid2 = strval(floor($id / 10000));
				$subid1 = substr(strval($subid2), 0, 1);
	            
				return $path.$subid1."/".$subid2."/".$id;
			}

			$filename = applyIdToPath( "files/data/", $objid )."-".
				preg_replace( '/[^0-9]/', "", $objData[ "objLastEdit" ]).".".
				$objData[ "objExtension" ];

			if( file_exists( $filename ))
			{
				readfile( $filename );
			}

			?>
			</blockquote>
			<?

			break;
		}
	}

	?>
	<h3>Keywords</h3>
	<?

	function traverseKeyword( $keyid )
	{
		$result = sql_query( "SELECT `keyWord`,`keySubcat` FROM `keywords` ".
			"WHERE `keyid` = '".intval( $keyid )."' LIMIT 1" );

		if( mysql_num_rows( $result ) == 0 )
		{
			return( "???" );
		}

		$kwName = mysql_result( $result, 0, 0 );
		$kwSubcat = mysql_result( $result, 0, 1 );

		$kwName = trim(
			preg_replace( '/^.*\|/', "",
			preg_replace( '/\@$/', "", $kwName )));

		if( $kwSubcat != 0 ) // keySubcat?
		{
			$kwName = traverseKeyword( $kwSubcat )." - ".$kwName;
		}

		return( $kwName );
	}

	// Show the list of chosen keywords.

	$result = sql_query( "SELECT `objKkeyword` FROM `objKeywords` ".
		"WHERE `objKobject` = '".$objid."'" );

	$idList = array();

	while( $rowData = mysql_fetch_row( $result ))
	{
		$idList[] = $rowData[ 0 ];
	}

	$stringList = array();

	foreach( $idList as $keyid )
	{
		$stringList[ $keyid ] = "<li>".traverseKeyword( $keyid )."</li>\n";
	}

	sort( $stringList );

	$stringList = implode( "", $stringList );

	if( $stringList == "" )
	{
		echo _NONE;
	}
	else
	{
		?>
		<ul>
			<?= $stringList ?>
		</ul>
		<?
	}

	?>
	<h3>Links</h3>
	<ul>
		<li><a href="<?= $urlBase ?>/">What's new</a></li>
		<li><a href="<?= $urlBase ?>/browse/">Browse</a></li>
		<li><a href="<?= $urlBase ?>/help/">Help</a></li>
		<li><a href="<?= $urlBase ?>/members/1/">Staff</a></li>
		<li><a href="<?= $urlBase ?>/tos/">Terms of Service</a></li>
	</ul>
</body>
</html>
<?

if( 0 )
{
	// Store profiler information.

	$time_start = $_stats[ "startTime" ];
	$time_end = gettimeofday();
	$secdiff = $time_end[ "sec" ] - $time_start[ "sec" ];
	$usecdiff = $time_end[ "usec" ] - $time_start[ "usec" ];
	$generationTime = round(( $secdiff * 1000000 + $usecdiff ) / 1000000, 3 );

	$page = "view (bot)";

	sql_where( array( "prfPage" => $page ));

	if( sql_count( "profiler" ) > 0 )
	{
		sql_query( "UPDATE `profiler` SET `prfCount` = `prfCount` + 1, ".
			"`prfTime` = `prfTime` + '".$generationTime."' ".
			"WHERE `prfPage` = '".addslashes( $page )."'" );
	}
	else
	{
		sql_values( array(
			"prfPage" => $page,
			"prfCount" => 1,
			"prfTime" => $generationTime ));

		sql_insert( "profiler" );
	}
}

?>
