<?

// This script can be ran by a supermod. It will force updating of the
// entire searching cache, updating all the submissions from the first
// to the last. In fact, this script should not ever be ran unless you
// completely lost the `searchcache` table or the caching mechanism
// has been changed.

if( !atLeastSModerator() )
{
	return;
}
if( !isset( $_GET[ "first" ]) || !isset( $_GET[ "last" ]))
{
	redirect( url( ".", array( "first" => 1, "last" => 1000 )));
}

$first = intval( $_GET[ "first" ]);
$last = intval( $_GET[ "last" ]);
$isFinished = false;

$result = sql_query( "SELECT MAX(`objid`) FROM `objects`" );

if( $data = mysql_fetch_row( $result ))
{
	if( $last > $data[ 0 ])
	{
		$last = $data[ 0 ];
		$isFinished = true;
	}
}

for( $objid = $first; $objid <= $last; $objid++ )
{
	updateSearchCache( $objid, true );
}

echo "Updating search cache for submissions from $first to $last...";

$_documentTitle = "$first to $last";

if( !$isFinished )
{
	?>
	<script type="text/javascript">
	//<![CDATA[

		window.setTimeout(
			"document.location='<?= url( ".", array( "first" => $last + 1, "last" => $last + 500 ), '&') ?>';", 1000);

	//]]
	</script>
	<?
}

?>