<?

if( !atLeastSModerator() )
{
	return;
}

$cntResult = sql_query( "SELECT COUNT(*) FROM `users`, `useExtData` ".
	"LEFT JOIN `objects` ON `useid` = `objCreator` ".
	"WHERE `objCreator` IS NULL ".
	"AND `useid` = `useEid` ".
	"AND `useIsActive` = '1'" );

$countNoSubmit = mysql_result( $cntResult, 0 );

$cntResult = sql_query( "SELECT COUNT(*) FROM `users`, `useExtData` ".
	"WHERE `useIsActive` = '1' AND `useid` = `useEid`" );

$countTotal = mysql_result( $cntResult, 0 );

?>
<div class="container">
Artists: <?= $countTotal - $countNoSubmit ?> out of <?= $countTotal ?> users<br />
<?

sql_query( "CREATE TEMPORARY TABLE `imgsizes` (".
	"`sizid` int(11) UNSIGNED NOT NULL auto_increment,".
	"`sizUser` varchar(255) NOT NULL,".
	"`sizTotal` int(11) UNSIGNED NOT NULL,".
	"PRIMARY KEY (`sizid`)) TYPE=MyISAM" );

$sumResult = sql_query( "SELECT `useUsername`, ".
	"SUM(`objImageSize`) AS `totalSize` ".
	"FROM `objects`, `objExtData`, `users` ".
	"WHERE `objid` = `objEid` ".
	"AND `useid` = `objCreator` ".
	"GROUP BY `useid` LIMIT 3000" );

while( $sumData = mysql_fetch_assoc( $sumResult ))
{
	$total = $sumData[ "totalSize" ];

	sql_query( "INSERT INTO `imgsizes`(`sizUser`, `sizTotal`) ".
		"VALUES('".$sumData[ "useUsername" ]."', '".$sumData[ "totalSize" ]."')" );
}

mysql_free_result( $sumResult );

$avgResult = sql_query( "SELECT AVG(`sizTotal`) FROM `imgsizes`" );

?>
Average storage per artist: <?= round( mysql_result( $avgResult, 0 ) / 1024 ) ?> KB<br />
<?

$maxResult = sql_query( "SELECT MAX(`sizTotal`) FROM `imgsizes`" );

?>
Maximal storage per artist: <?= round( mysql_result( $maxResult, 0 ) / 1024 ) ?> KB<br />
<?

$sumResult = sql_query( "SELECT SUM(`sizTotal`) FROM `imgsizes`" );

?>
Total storage on the server: <?= round( mysql_result( $sumResult, 0 ) / 1024 / 1024 / 1024, 1 ) ?> GB<br />
</div>
