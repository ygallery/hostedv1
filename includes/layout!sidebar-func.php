<?php

function putSidebarBanner()
{
	global $_isSearchBot, $dbh;

	if( $_isSearchBot )
	{
		return;
	}
	
	$stmt = $dbh->prepare("SELECT * FROM `banner`
	WHERE end >= NOW()
	AND start <= NOW()
	AND type = 'sidebar'
	ORDER BY RAND()
	LIMIT 1");
	$stmt->execute();
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
	
	if($banid = $result['id'])
	{
		?>
        	<div style="text-align : center; margin : 0.7em;">
				<a href="<?= url( "banner/goto/".$banid ) ?>"><img alt="" src="/images/banners/<?= $banid ?>.gif" /></a>
				<div style="font-size : 9px; margin : 0.2em;"><a href="<?=url("helpdesk/faq/general/advert")?>">Advertise on y!Gallery</a></div>
			</div>
        <?php
	}
	
}

function putBottomBanner(){
	global $_isSearchBot, $dbh;
	
	if($_isSearchBot){
		return;
	}
	
	$stmt = $dbh->prepare("SELECT * FROM `banner`
	WHERE end >= NOW() 
	AND start <= NOW()
	AND type = 'banner'
	ORDER BY RAND()
	LIMIT 1");
	$stmt->execute();
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
	
	if($banid = $result['id'])
	{
		?>
        	<div class="mar_top">
				<a href="<?= url("banner/goto/".$banid) ?>"><img alt="" src="/images/banners/<?= $banid ?>.gif" /></a>
                <div style="font-size : 9px; margin : 0.2em;"><a href="<?=url("helpdesk/faq/general/advert")?>">Advertise on y!Gallery</a></div>
			</div>
        <?php
	}
}

function putSidebarUpdates()
{
	global $_auth;

	$result = false;

	?>
	<div id="updates">
		<?
	
		// This is just a macro so to not write the same code 5 times
		// in a row (see below).
	
		function __updIcon( $count, $hint, $icon )
		{
			global $_auth;
	
			if( $count > 0 )
			{
				return getIMG( url()."images/emoticons/".$icon,
					'alt="'.substr( $hint, 0, 1 ).'" title="'.$hint.'"' ).
					( $_auth[ "useFuzzyNumbers" ] ? " " : "" ).
					( $hint == _COMMENTS ? '<span id="_globCmtCnt">' : "" ).
					( $hint == _JOURNALS ? '<span id="_globJouCnt">' : "" ).
					fuzzy_number( $count ).
					( $hint == _COMMENTS || $hint == _JOURNALS ? '</span>' : "" ).
					( $_auth[ "useFuzzyNumbers" ] ? "<br />" : " " );
			}
			else
				return "";
		}
	
		$updates =
			__updIcon( $_auth[ "useUpdWat" ], _MESSAGES, "watch.png" ).
			__updIcon( $_auth[ "useUpdFav" ], _FAVOURITES, "fav1.png" ).
			__updIcon( $_auth[ "useUpdCom" ], _COMMENTS, "comment.png" ).
			__updIcon( $_auth[ "useUpdJou" ], _JOURNALS, "journal.png" ).
			__updIcon( $_auth[ "useUpdObj" ], _SUBMISSIONS, "submission.png" );
	
		if( isExtras() )
		{
			$updates .= __updIcon( $_auth[ "useUpdExt" ], _SUBMIT_TYPE_EXTRA, "star4.png" );
		}
	
		if( $updates != "" )
		{
			?>
			<a href="<?= url( "updates" ) ?>"><?= $updates ?></a>
			<?

			$result = true;
		}
	
		?>
	</div>
	<?

	return( $result );
}

function putSidebarMostRecent()
{
	global $_config;

	$select = "SELECT * FROM `objects`";
	$where = "`objPending` = '0' AND `objDeleted` = '0' AND ".
		"`objSubmitDate` > DATE_SUB(NOW(), INTERVAL ".
		$_config[ "mostRecentDays" ]." DAY)";
	$limit = 5;
	unset( $order );

	include( INCLUDES."mod_minigallery.php" );
}

function putSidebarRandom()
{
	global $_auth, $_config;

	iefixStart();

	$needRefresh = false;
	$objid = 0;

	$rndResult = sql_query( "SELECT * FROM `randomObjects`".dbWhere( array(
		"rndFilterPtn" => $_auth[ "useObjFilters" ])).
		"LIMIT 1" );

	if( $rndData = mysql_fetch_assoc( $rndResult ))
	{
		if( time() > $rndData[ "rndTimeout" ])
		{
			$needRefresh = true; // Random object has timed-out
		}
		else
		{
			$objid = $rndData[ "rndObject" ];
		}
	}
	else
	{
		$needRefresh = true;
	}

	mysql_free_result( $rndResult );

	if( $needRefresh )
	{
		$count = mysql_result( sql_query(
			"SELECT MAX(`objid`) FROM `objects`" ), 0 );

		$where = "`objPending` = '0' AND `objDeleted` = '0'";

		applyObjFilters( $where );

		// We would give it 5 chances to pick a random submission :)
		// Actually there's a 99.999% possibility it would pick the good
		// one at first try.

		$objid = 0;

		for( $i = 1; $i <= 5; $i++ )
		{
			$id = mt_rand( 1, $count );

			$_tmpResult = sql_query( "SELECT `objid` FROM `objects` ".
				"WHERE ($where) AND `objid` = '$id' LIMIT 1" );

			if( mysql_num_rows( $_tmpResult ) > 0 )
			{
				$objid = $id;
				break;
			}
		}

		$timeout = time() + 60; // Will expire in 1 minute

		sql_query( "LOCK TABLES `randomObjects` WRITE" );
		//sql_query( "BEGIN" );

		sql_query( "DELETE FROM `randomObjects`".dbWhere( array(
			"rndFilterPtn" => $_auth[ "useObjFilters" ])));

		sql_query( "INSERT INTO `randomObjects`".dbValues( array(
			"rndFilterPtn" => $_auth[ "useObjFilters" ],
			"rndObject" => $objid,
			"rndTimeout" => $timeout )));

		sql_query( "UNLOCK TABLES" );
		//sql_query( "COMMIT" );
	}

	// Show the thumbnail of that `objid`.

	$select = "SELECT * FROM `objects`";
	$where = "`objid` = '$objid'";
	$limit = 1;
	unset( $order );

	include( INCLUDES."mod_minigallery.php" );

	iefixEnd();
}

function putSidebarLogin()
{
	global $_lang, $_cmd, $_config;

	?>
	<div class="sep"><?=_NOT_LOGGED ?></div>
	<?

	// Verify that the user has filled in the login form.

	if( isset( $_POST[ "username" ]) && isset( $_POST[ "password" ]))
	{
		if( $_POST[ "username" ] == "" )
		{
			notice( _BLANK_USERNAME );
		}
		elseif( $_POST[ "password" ] == "" )
		{
			notice( _BLANK_PASSWORD );
		}
		else
		{
			include_once( INCLUDES."authenticate.php" ); // Defines verifyLogin()

			$persistentLogin = isset( $_POST[ "persistent" ]) ? 1 : 0;

			if( verifyLogin( addslashes( $_POST[ "username" ]),
				$_POST[ "password" ], $persistentLogin ))
			{
				switch( $_cmd[ 0 ])
				{
					// Below is the list of pages that should redirect the
					// user to the front page after login:

					case "activate":
						redirect( url( "settings/site" )); // redirect to Settings after activation

					case "emailchange":
					case "emailresend":
					case "join":
					case "logout":
					case "lostpassword":
					case "password":
					case "passwordchanged":
						redirect( url( "/" )); // redirect to the main page

					default:
						redirect( url( "." )); // redirect to the same page after login

					// Note: redirection is made to get rid of the POST data.
				}
			}
		}
	}

	?>
	<form action="<?= url( "." ) ?>" method="post">
		<div class="sep caption"><?= _USERNAME ?>:</div>
		<div><input class="narrow" name="username" type="text"
			<?=isset( $_POST[ "username" ])
				? 'value="'.htmlspecialchars( $_POST[ "username" ]).'"' : "" ?> /></div>

		<div class="sep caption"><?= _PASSWORD ?>:</div>
		<div><input class="narrow" name="password" type="password" /></div>

		<div class="sep"><input checked="checked" class="checkbox"
			id="loginPersistent" name="persistent" type="checkbox" />
			<label for="loginPersistent"><?= _USE_REMEMBER ?></label></div>

		<div class="sep">
			<button class="submit" type="submit">
				<?= getIMG( url()."images/emoticons/checked.png" ) ?>
				<?= _LOGIN ?>
			</button>
		</div>
		<div class="sep"><a href="<?= url( "join" )?>">
			<?= getIMG( url()."images/emoticons/star.png" ) ?>
			<?= _CREATE_ACCOUNT ?></a></div>
		<div class="sep"><a href="<?=url("lostpassword" ) ?>">
			<?= getIMG( url()."images/emoticons/sad.png" ) ?>
			<?= _USE_LOSTPASS ?></a></div>
		<div class="sep"><a href="javascript:popup('<?= url( "tos", array( "popup" => "yes" )) ?>','tos',900,700)">
			<?= _TOS ?></a></div>
	</form>
	<?

	if( isset( $_POST[ "language" ]))
	{
		$language = addslashes($_POST["language"]);
		$_auth["useLanguage"] = $language;
		$expiry = strtotime("+9 years");
		setcookie("yGalLanguage", $language, $expiry, "/", ".".$_config["galRoot"]); // give the user a fresh language cookie
		$GLOBALS["_yGalLanguage"] = $language;
		redirect( url( "." ));
	}

	?>
	<form action="<?= url( "." ) ?>" method="post">
		<?

		$language = $_lang;

		$_tmpResult = sql_query( "SELECT * FROM `languages` ORDER BY `lanEngName`" );

		?>
		<div class="sep caption"><?= _SET_LANGUAGE ?>:</div>
		<select name="language" onchange="this.form.submit()" style="width : 150px;">
			<?

			while( $rowData = mysql_fetch_assoc( $_tmpResult ))
			{
				?>
				<option <?=( $language == $rowData[ "lanid" ] ?
					'selected="selected" ' : "" ) ?>
					value="<?= $rowData[ "lanid" ] ?>"><?=
					htmlspecialchars( $rowData[ "lanName" ]) ?>
					(<?= htmlspecialchars( $rowData["lanEngName" ]) ?>)</option>
				<?
			}

			?>
		</select>
	</form>
	<?
}

function putSidebarPoll()
{
	global $_pollUser, $_newUI, $_cmd, $_auth;

	// Include the poll. Global variable $_pollUser defines whose
	// poll that will be.

	if( isset( $_pollUser ) && $_pollUser != 0 )
	{
		$_tmpResult1 = sql_query( "SELECT * FROM `polls` ".
			"WHERE `polCreator` = '".intval($_pollUser)."' ".
			"ORDER BY `polSubmitDate` DESC LIMIT 1" );

		if( $polData = mysql_fetch_assoc( $_tmpResult1 ))
		{
			$options = array();

			$_tmpResult2 = sql_query( "SELECT * FROM `pollOptions` ".
				"WHERE `polOPoll` = '".$polData[ "polid" ]."' ".
				"ORDER BY `polOVotes` DESC,`polOid` LIMIT 20" );

			while( $optData = mysql_fetch_assoc( $_tmpResult2 ))
				$options[ $optData[ "polOid" ]] = $optData;

			if( count( $options ) > 0 )
			{
				if( isset( $_newUI ))
				{
					?>
					<div class="ui2-layout-bg ui2-section-closed"><?= _POLL ?></div>
					<div class="ui2-section-body">
						<?
						
						include( INCLUDES."mod_poll.php" );
						
						?>
					</div>
					<?
				}
				else
				{
					?>
					<a name="poll"></a>
					<div class="sep caption"><?= _POLL ?>:</div>
					<div class="container2">
						<?

						include( INCLUDES."mod_poll.php" );

						?>
					</div>
					<?
				}
			}
		}
	}
}

function putSidebarModStuff()
{
	global $_newUI, $_auth;

	if( isModerator() || $_auth[ "useid" ] == 7 )
	{
		$found = false;

		ob_start();

		for( $isExtras = 0; $isExtras < 2; $isExtras++ )
		{
			$_objects = $isExtras ? "`extras`" : "`objects`";

			// Bring attention to the abuse cases pending a moderator's decision.

			$_tmpResult = sql_query( "SELECT `objid`,`objTitle`,`abuid` FROM `abuses`,$_objects".dbWhere( array(
				"abuObj*" => "objid",
				"abuIsExtras" => $isExtras,
				"abuMod" => "?" ))."ORDER BY `abuSubmitDate` DESC LIMIT 5" );

			while( $rowData = mysql_fetch_assoc( $_tmpResult ))
			{
				?>
				<div>
					<?= getIMG( url()."images/emoticons/".( $isExtras ? "star4.png" : "keydelete.gif" )) ?>
					<a href="<?= url( "abuse/".$rowData[ "abuid" ]) ?>">
						<?= htmlspecialchars( $rowData[ "objTitle" ]) ?></a>
				</div>
				<?

				$found = true;
			}
		}

		$ht = ob_get_contents();
		ob_end_clean();

		if( $found )
		{
			if( isset( $_newUI ))
			{
				?>
				<div class="ui2-layout-bg ui2-section-closed"><?= _MODERATOR ?></div>
				<div class="ui2-section-body">
					<?= $ht ?>
				</div>
				<?
			}
			else
			{
				?>
				<div class="caption error"><?= _MODERATOR ?><br /><?= _ABUSE_LIST ?>:</div>
				<div class="container2 mar_bottom">
					<?= $ht ?>
				</div>
				<?
			}
		}
	}

	if( isSModerator() || $_auth[ "useid" ] == 7 )
	{
		$found = false;

		ob_start();

		$mod_count = 0;
		$adm_count = 0;

		for( $isExtras = 0; $isExtras < 2; $isExtras++ )
		{
			$_objects = $isExtras ? "`extras`" : "`objects`";

			// Bring attention to the abuse cases pending a supermoderator's decision.

			$_tmpResult = sql_query( "SELECT `objid`,`objTitle`,`abuid` FROM `abuses`,$_objects ".
				"WHERE `abuObj` = `objid` AND `abuIsExtras` = '$isExtras' AND `abuMod` <> '?' AND `abusMod` = '?' ".
				"ORDER BY `abuSubmitDate` DESC LIMIT 5" );

			$_tmpResult_mod = sql_query( "SELECT COUNT(*) FROM `abuses`,$_objects ".
				"WHERE `abuObj` = `objid` AND `abuIsExtras` = '$isExtras' AND `abuMod` = '?'" );

			$mod_count += mysql_result( $_tmpResult_mod, 0 );

			$_tmpResult_adm = sql_query( "SELECT COUNT(*) FROM `abuses`,$_objects ".
				"WHERE `abuObj` = `objid` AND `abuIsExtras` = '$isExtras' AND `abuMod` <> '?' AND `abusMod` <> '?' ".
				"AND `abuMod` <> `abusMod` AND `aburMod` = '?'" );

			$adm_count += mysql_result( $_tmpResult_adm, 0 );

			while( $rowData = mysql_fetch_assoc( $_tmpResult ))
			{
				?>
				<div>
					<?= getIMG( url()."images/emoticons/".( $isExtras ? "star4.png" : "keydelete.gif" )) ?>
					<a href="<?= url( "abuse/".$rowData[ "abuid" ]) ?>">
						<?= htmlspecialchars( $rowData[ "objTitle" ]) ?></a>
				</div>
				<?

				$found = true;
			}
		}

		if( $mod_count > 0 )
		{
			?>
			<div<?= $found ? ' class="sep"' : "" ?>><?= _ABUSE_WAIT_MODERATOR.": ".$mod_count ?></div>
			<?

			$found = true;
		}

		if( $adm_count > 0 )
		{
			?>
			<div<?= $found ? ' class="sep"' : "" ?>><?= _ABUSE_WAIT_ADMINISTRATOR.": ".$adm_count ?></div>
			<?

			$found = true;
		}

		$ht = ob_get_contents();
		ob_end_clean();

		if( $found )
		{
			if( isset( $_newUI ))
			{
				?>
				<div class="ui2-layout-bg ui2-section-closed"><?= _SUPERMODERATOR ?></div>
				<div class="ui2-section-body">
					<?= $ht ?>
				</div>
				<?
			}
			else
			{
				?>
				<div class="caption error"><?= _SUPERMODERATOR ?><br /><?= _ABUSE_LIST ?>:</div>
				<div class="container2 mar_bottom">
					<?= $ht ?>
				</div>
				<?
			}
		}
	}

	if( isAdmin() || $_auth[ "useid" ] == 7 )
	{
		$found = false;

		ob_start();

		for( $isExtras = 0; $isExtras < 2; $isExtras++ )
		{
			$_objects = $isExtras ? "`extras`" : "`objects`";

			// Bring attention to the abuse cases pending admin's decision.

			$_tmpResult = sql_query( "SELECT `objid`,`objTitle`,`abuid` FROM `abuses`,$_objects ".
				"WHERE `abuObj` = `objid` AND `abuMod` <> '?' AND `abusMod` <> '?' ".
				"AND `abuMod` <> `abusMod` AND `aburMod` = '?' ".
				"ORDER BY `abuSubmitDate` DESC LIMIT 5" );

			while( $rowData = mysql_fetch_assoc( $_tmpResult ))
			{
				?>
				<div>
					<?= getIMG( url()."images/emoticons/".( $isExtras ? "star4.png" : "keydelete.gif" )) ?>
					<a href="<?= url( "abuse/".$rowData[ "abuid" ]) ?>">
						<?= htmlspecialchars( $rowData[ "objTitle" ]) ?></a>
				</div>
				<?

				$found = true;
			}
		}

		$ht = ob_get_contents();
		ob_end_clean();

		if( $found )
		{
			if( isset( $_newUI ))
			{
				?>
				<div class="ui2-layout-bg ui2-section-closed"><?= _ADMINISTRATOR ?></div>
				<div class="ui2-section-body">
					<?= $ht ?>
				</div>
				<?
			}
			else
			{
				?>
				<div class="caption error"><?= _ADMINISTRATOR ?><br /><?= _ABUSE_LIST ?>:</div>
				<div class="container2 mar_bottom">
					<?= $ht ?>
				</div>
				<?
			}
		}
	}
}

?>
