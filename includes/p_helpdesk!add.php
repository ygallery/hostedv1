<?

$cats = array();

$catsResult = sql_rowset( "helpdeskCats" );

while( $catsData = sql_next( $catsResult ))
{
	$cats[ $catsData[ "hdcid" ]] = $catsData[ "hdcName" ];
}

sql_free( $catsResult );

?>
<div class="header">
	Add a Request
</div>
<div class="container2 mar_bottom">
	<table cellspacing="15" cellpadding="0" border="0">
	<tr>
	<td valign="bottom" width="50%">
		<div class="mar_bottom">
			<?= getIMG( url()."images/emoticons/a-left.png" )?>
			<b>Short summary</b>
		</div>
		<div class="mar_bottom" style="padding-right : 10%">
			The following text will be available to be viewed by the owner
			of the reported object:
		</div>
		<div class="mar_bottom">
			<textarea rows="5" style="width : 95%"></textarea>
		</div>
	</td>
	<td valign="bottom" width="50%">
		<div style="margin-left : 10%">
			<div class="mar_bottom">
				Category:
			</div>
			<div class="mar_bottom">
				<select name="category">
				<?
	        
				foreach( $cats as $id => $name )
				{
					?>
					<option value="<?= $id ?>"><?= $name ?></option>
					<?
				}
	        
				?>
				</select>
			</div>
			<div><br /></div>
			
			<div class="mar_bottom" style="padding-right : 10%">
				Choose the type of the reported object:
			</div>
			<div class="mar_bottom">
				<select name="referenceType">
					<option value="none">None</option>
					<option value="object">Submission</option>
					<option value="extra">Submission in Extras</option>
					<option value="pm">Private message</option>
					<option value="comment">Comment</option>
					<option value="journal">Journal entry</option>
					<option value="poll">Poll</option>
				</select>
			</div
			<table cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<div style="margin-left : 2%" class="nowrap">
							&nbsp; &nbsp;
							Object ID:
							<input type="text" name="referenceID" size="18" />
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="smalltext" style="margin-top : 0.2em; text-align : right;">
							<a href="javascript:;" onclick="$('whats-objid').toggle();">(what's this?)</a>
						</div>
					</td>
				</tr>
			</table>
			<div class="container2 mar_top" style="display : none" id="whats-objid">
				<div class="mar_bottom">
                    Object ID is the <b>first</b> number you can see in the
                    address bar in your browser when viewing a certain
                    object's page. For example:
				</div>
				<div class="mar_bottom">
					<b>Submission</b>:<br />
					<?= url( "view/12345" ) ?><br />
					Object ID: 12345
				</div>
				<div class="mar_bottom">
					<b>Comment</b>:<br />
					<?= url( "comment/98765" ) ?><br />
					Object ID: 98765
				</div>
				<div class="mar_bottom">
					<b>Journal entry</b>:<br />
					<?= url( "journal/12121" ) ?><br />
					Object ID: 12121
				</div>
				<div class="mar_bottom">
                    Always keep in mind it's the very first number in the
                    address bar, it may be followed (or not) by more numbers
                    and other data.
				</div>
				<div class="mar_bottom">
                    <b>Note</b>: Normally, you don't need to type in the Object
                    ID by hand, instead go to the object's page and find and
                    click the &quot;Report&quot; link.
				</div>
				<div class="button" onclick="$('whats-objid').hide();">OK</div>
				<div style="clear : both"></div>
			</div>
		</div>
	</td>
	</tr>
	<tr>
	<td valign="bottom" width="50%">
		<div class="mar_bottom">
			<?= getIMG( url()."images/emoticons/star.png" )?>
			<b>Public details</b>
		</div>
		<div class="mar_bottom" style="padding-right : 10%">
			The following text will be available to be viewed by the owner
			of the reported object:
		</div>
		<div class="mar_bottom">
			<textarea rows="9" style="width : 95%;"></textarea>
		</div>
		<div class="mar_bottom" style="padding-right : 10%">
			(optionally) <b>Upload a file</b>:
		</div>
		<div class="mar_bottom">
			<input type="file" size="30" />
		</div>
	</td>
	<td valign="bottom" width="50%">
		<div class="mar_bottom">
			<?= getIMG( url()."images/emoticons/ghost.png" )?>
			<b>Private details</b>
		</div>
		<div class="mar_bottom" style="padding-right : 10%">
			The following text will be viewed only by the site staff:
		</div>
		<div class="mar_bottom">
			<textarea rows="9" style="width : 95%"></textarea>
		</div>
		<div class="mar_bottom" style="padding-right : 10%">
			(optionally) <b>Upload a file</b> only viewable by the site staff:
		</div>
		<div class="mar_bottom">
			<input type="file" size="30" />
		</div>
	</td>
	</tr>
	</table>
</div>
<div style="margin-left : 25px">
	<button>
		<?= getIMG( url()."images/emoticons/checked.png" )?>
		Submit Request
	</button>
</div>
