<?php 

if( !atLeastModerator() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

elseif( atLeastModerator() ) {
	
	$_documentTitle = "Email Search";
	
?>

<div class="header">
	<div class="header_title">
		<?= _ADMINISTRATION ?>
		<div class="subheader">Email Search</div>
	</div>
	<?php 
		$active = 8;
		include(INCLUDES."mod_adminmenu.php");
	
	?>
</div>

	
<div class="container">

	<div class="mar_bottom container2 notsowide">
			<?=iefixStart()?>
			<div class="mar_bottom">
				<b>Enter Email</b>:
				<form action="<?= url( "." )?>" method="post">
					<input type="text" name="useEmailReq" size="50" /> 
				    <input class="submit" type="submit" name="submitEmailSearch" value="Search" />
				</form>
			</div>
			
			
				<?php 
					if( isset($_POST[ "useEmailReq" ])) {
						$useEmailReq = $_POST[ "useEmailReq" ];
						$result = sql_query( "SELECT `useEid` FROM `useExtData` WHERE `useEmail` = '".addslashes( $useEmailReq )."' LIMIT 1" );
						$resultcount = mysql_num_rows( $result );
						if( $resultcount == 0 )
						{
							echo "No user found with this email.";
						}
						else {
							$useId = mysql_fetch_assoc( $result );
							?>
							<div class="mar_bottom">
							<b>Result</b>: 
							<?php 
							echo 'User '.getUserLink($useId[ "useEid" ]);
							?>
							</div>
							<?php 
						}
					}
				
				?>
			
			</div>
			<?=iefixEnd()?>

</div>
<?php 
}
?>