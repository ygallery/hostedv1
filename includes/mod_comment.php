<?

// Module: comment + text formatting tools + emoticons.
// Include it somewhere where you need a textarea named "comment" to appear
// (the default name is changeable).
//
// Input variables:
//
// $commentName - the name of the POST variable that will store the textarea content
// $commentDefault - default text inside the textarea
// $commentRows - the height of the textarea in rows (default 7)
// $commentNoOptions - if true, no additional options will be shown ("No Emoticons" etc)
// $commentNoEmoticons - if set, the "No Emoticons" checkbox will be checked by default
// $commentNoSig - if set, the "No Signature" checkbox will be checked by default
// $commentNoBBCode - if set, the "No BBCode" checkbox will be checked by default
//
// Output variables:
//
// $commentId - the id of the comment textarea

global $_auth, $_config, $_emoticons;

if( !isset( $commentDefault ))
	$commentDefault = "";

if( !isset( $commentName ))
	$commentName = "comment";

if( !isset( $commentNoBBCode ))
	$commentNoBBCode = isLoggedIn() && $_auth[ "useNoBBCode" ];

if( !isset( $commentNoEmoticons ))
	$commentNoEmoticons = isLoggedIn() && $_auth[ "useNoEmoticons" ];

if( !isset( $commentNoOptions ))
	$commentNoOptions = false;

if( !isset( $commentNoSig ))
	$commentNoSig = isLoggedIn() && $_auth[ "useNoSig" ];

if( !isset( $commentRows ))
	$commentRows = 7;

if( !isset( $commentWide ))
	$commentWide = true;

$commentId = generateId();

?>
<script type="text/javascript">
//<![CDATA[

	_IR = {
		commentId: '<?= $commentId ?>',
		commentName: '<?= $commentName ?>',
		commentDefault: <?= json_encode( $commentDefault ) ?>,
		commentWide: <?= $commentWide ? "true" : "false" ?>,
		commentRows: <?= $commentRows ?>,
		commentNoBBCode: <?= $commentNoBBCode ? "true" : "false" ?>,
		commentNoEmoticons: <?= $commentNoEmoticons ? "true" : "false" ?>,
		commentNoOptions: <?= $commentNoOptions ? "true" : "false" ?>,
		commentNoSig: <?= $commentNoSig ? "true" : "false" ?>,
		emoticonPopupURL: '<?= url( "emoticons",
			array( "popup" => "yes", "comment" => $commentId )) ?>',
		previewURL: '<?= url( "preview", array( "popup" => "yes" )) ?>'
		};

	document.write( showReplyBox() );

//]]>
</script>
<?

unset( $commentDefault );
unset( $commentName );
unset( $commentNoOptions );
unset( $commentNoEmoticons );
unset( $commentNoSig );
unset( $commentNoBBCode );
unset( $commentRows );
unset( $commentWide );

?>
