<?

if( !atLeastSModerator() && $_auth[ "useid" ] != 34814/*Mayhem*/ )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

if( isset( $_GET[ "reset" ]))
{
	sql_query( "UPDATE `profiler` SET `prfCount` = '0', `prfTime` = '0' WHERE 1" );
	redirect( url( "." ));
}

if( isset( $_GET[ "wmark" ]))
{
	sql_query( "DELETE FROM `profiler` WHERE `prfCount` < 100" );
	sql_query( "UPDATE `profiler` SET `prfWMCount` = `prfCount`, `prfWMTime` = `prfTime` WHERE 1" );
	sql_query( "UPDATE `profiler` SET `prfCount` = '0', `prfTime` = '0' WHERE 1" );
	redirect( url( "." ));
}

?>
<div class="container2">

<div>
	<a href="<?= url( "." ) ?>">Refresh</a>
	&middot;
	<a onclick="return confirm('<?= _ARE_YOU_SURE ?>')"
		href="<?= url( ".", array( "wmark" => "yes" )) ?>">Replace watermark</a>
	&middot;
	<a onclick="return confirm('<?= _ARE_YOU_SURE ?>')"
		href="<?= url( ".", array( "reset" => "yes" )) ?>">Reset statistics</a>
	&middot;
	<a href="<?= url( "access" ) ?>">Access</a>
</div>
<div><br /></div>

<table cellspacing="0" cellpadding="4" border="1">
	<tr>
		<td><b>Page</b></td>
		<td><b>Requests</b></td>
		<td><b>Avg time</b></td>
		<td><b>Total time</b></td>
		<td><b>%</b></td>
		<td><b>W/M Requests</b></td>
		<td><b>W/M Avg time</b></td>
		<td><b>W/M Total time</b></td>
		<td><b>W/M %</b></td>
	</tr>
<?

$prfResult = sql_query( "SELECT SUM(`prfTime`) FROM `profiler`" );

$totalTime = sql_result( $prfResult );

if( $totalTime == 0 )
{
	$totalTime = 1;
}

$prfResult = sql_query( "SELECT SUM(`prfWMTime`) FROM `profiler`" );

$totalTimeWM = sql_result( $prfResult );

if( $totalTimeWM == 0 )
{
	$totalTimeWM = 1;
}

$prfResult = sql_query( "SELECT * FROM `profiler` ORDER BY `prfTime` DESC" );

while( $prfData = sql_next( $prfResult ))
{
	if( $prfData[ "prfCount" ] == 0 )
	{
		$prfData[ "prfCount" ] = 1;
	}

	if( $prfData[ "prfWMCount" ] == 0 )
	{
		$prfData[ "prfWMCount" ] = 1;
	}

	if( strpos( $prfData[ "prfPage" ], "(bot)" ) !== false )
	{
		$prfData[ "prfPage" ] = '<span class="error">'.$prfData[ "prfPage" ].'</span>';
	}

	?>
	<tr>
		<td>/<?= $prfData[ "prfPage" ] ?></td>

		<td align="center"><?= $prfData[ "prfCount" ] ?></td>
		<td align="right"><?= sprintf( "%1.3f", $prfData[ "prfTime" ] / $prfData[ "prfCount" ]) ?></td>
		<td align="right"><?= $prfData[ "prfTime" ] ?></td>
		<td align="center"><?= round( $prfData[ "prfTime" ] / $totalTime * 100 ) ?></td>

		<td align="center"><?= $prfData[ "prfWMCount" ] ?></td>
		<td align="right"><?= sprintf( "%1.3f", $prfData[ "prfWMTime" ] / $prfData[ "prfWMCount" ]) ?></td>
		<td align="right"><?= $prfData[ "prfWMTime" ] ?></td>
		<td align="center"><?= round( $prfData[ "prfWMTime" ] / $totalTimeWM * 100 ) ?></td>
	</tr>
	<?
}

sql_free( $prfResult );

?>
</table>

</div>
