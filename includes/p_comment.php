<?

	if($_config["readOnly"]) {
		echo '<div class="header">'._COMMENT.'</div>'.
			'<div class="container">';
		notice(_READONLY);
		echo '</div>';
		return;
	}

	$comid = intval($_cmd[1]);
	$comSubmitDate = preg_replace('/[^0-9]/', "", $_cmd[2]);

	$result = sql_query("SELECT * FROM `comments` WHERE `comid` = '$comid' ".
		($_config["checkSubmitDate"] ? "AND `comSubmitDate` = '$comSubmitDate'" : "").
		" LIMIT 1");
	// if the comment does not exist, return the Not Found page
	if(!mysql_num_rows($result)) {
		include(INCLUDES."p_notfound.php");
		return;
	}
	else
		$comData = mysql_fetch_assoc($result);

	$useResult = sql_query("SELECT `useid`,`useUsername` FROM `users` WHERE `useid` = '".$comData["comCreator"]."' LIMIT 1");
	$useData = mysql_fetch_assoc($useResult);

	$_documentTitle = _COMMENT." (".sprintf(_BY,$useData["useUsername"]).")";
?>

<div class="header">
	<div class="header_title">
		<?=_COMMENT ?>
		<div class="subheader"><? printf(_BY,$useData["useUsername"]); ?></div>
		<div class="clear">&nbsp;</div>
	</div>
</div>

<div class="container">
<div align="center"><table class="a_left"><tr><td class="notsowide">
<?
	include_once(INCLUDES."comments.php");
	showComment($comData, 0, true);

	if(!isset($_GET["replied"]))
		echo '<div class="sep header">'._LEAVE_COMMENT_REPLY.'</div>';
	else
		echo '<div class="sep" style="margin-left: 16px">';

	showAllComments($comid, "com", false);

	if(isset($_GET["replied"]))
		echo '</div>';
?>
</td></tr></table></div>
</div>
