<?=iefixStart()?>

<div class="clear">&nbsp;</div>

<div class="normaltext mar_bottom">

	<div class="tab<?=$active == 1 ? " tab_active" : "" ?>"
		onclick="document.location='<?=url( "friends/".$useUsername ) ?>'">
		<?= getIMG( url()."images/emoticons/heart3.png" ) ?> <?= _FRIENDS ?></div>

	<div class="tab<?=$active == 2 ? " tab_active" : "" ?>"
		onclick="document.location='<?=url( "watches/".$useUsername ) ?>'">
		<?= getIMG( url()."images/emoticons/heart.png" ) ?> <?= _WATCH_LIST ?></div>

	<div class="tab<?=$active == 3 ? " tab_active" : "" ?>"
		onclick="document.location='<?=url( "watchedby/".$useUsername ) ?>'">
		<?= getIMG( url()."images/emoticons/watch.png" ) ?> <?= _WATCHED_BY ?></div>
	<?

	navControls( $fr_offset, $fr_limit, $fr_totalCount );

	?>
	<div class="clear">&nbsp;</div>

</div>

<div class="hline">&nbsp;</div>

<?=iefixEnd()?>
