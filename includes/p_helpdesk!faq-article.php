<?

sql_where( array(
	"hfcIdent" => $_cmd[ 2 ],
	"hfqCategory*" => "hfcid",
	"hfqIdent" => $_cmd[ 3 ]));

if( !$faqData = sql_row( "helpdeskFAQ, helpdeskFAQCats" ))
{
	redirect( url( "helpdesk/faq" ));
}

$_documentTitle = "FAQ: ".preg_replace( '/^.*\|/', "", $faqData[ "hfqTitle" ]);

if( !$isEditMode )
{
	?>
	<div class="header">
		<?= htmlspecialchars( preg_replace( '/^.*\|/', "", $faqData[ "hfqTitle" ])) ?>
		<div class="subheader">
			Category: <a href="<?= url( "helpdesk/faq/".$faqData[ "hfcIdent" ]) ?>">
				<?= htmlspecialchars( preg_replace( '/^.*\|/', "", $faqData[ "hfcName" ])) ?></a><br />
			ID: <?= $faqData[ "hfqid" ] ?>
		</div>
	</div>
	
	<table class="notsowide" cellspacing="0" cellpadding="0" border="0"><tr><td>
		<div class="container2">
			<?= formatText( $faqData[ "hfqText" ]) ?>
		</div>
		<div><br /></div>
		<div style="float : right;">
			<form action="<?= url( "helpdesk/faq" ) ?>" method="get">
			<button class="smalltext">
				<?= getIMG( url()."images/emoticons/nav-prev.png" )?>
				Return to FAQ
			</button>
			</form>
		</div>
		<div style="margin-left : 0.7em;">
			Tags:
			<?
	    
			sql_order( "hfmid" );
			sql_where( array(
				"hfmArticle" => $faqData[ "hfqid" ],
				"hfmTag*" => "hftid" ));
	    
			$tags = array();
			$tagResult = sql_rowset( "helpdeskFAQTagMap, helpdeskFAQTags" );
	    
			while( $tagData = sql_next( $tagResult ))
			{
				if( count( $tags ) > 0 )
				{
					echo ", ";
				}
	    
				$url = url( "helpdesk/faq/tag", array( "tag" => $tagData[ "hftName" ]));
	    
				?><a href="<?= $url ?>"><?= $tagData[ "hftName" ] ?></a><?
	    
				$tags[] = $tagData[ "hftid" ];
			}
	    
			sql_free( $tagResult );
	    
			if( count( $tags ) == 0 )
			{
				echo "--";
			}
	    
			?>
		</div>
		<?
	    
		if( count( $tags ) > 0 )
		{
			ob_start();
	    
			$processedIds = array( $faqData[ "hfqid" ]);
	    
			foreach( $tags as $tagid )
			{
				sql_where( array(
					"hfmTag" => $tagid,
					"hfmArticle*" => "hfqid",
					"hfqCategory*" => "hfcid" ));
	    
				$result = sql_rowset( "helpdeskFAQ, helpdeskFAQCats, helpdeskFAQTagMap" );
	    
				while( $data = sql_next( $result ))
				{
					if( in_array( $data[ "hfqid" ], $processedIds ))
					{
						continue;
					}
	    
					$url = url( "helpdesk/faq/".$data[ "hfcIdent" ]."/".$data[ "hfqIdent" ]);
					$title = preg_replace( '/^.*\|/', "", $data[ "hfqTitle" ]);
	    
					?><li><a href="<?= $url ?>"><?= htmlspecialchars( $title ) ?></a></li><?
	    
					$processedIds[] = $data[ "hfqid" ];
				}
	    
				sql_free( $result );
			}
	    
			$html = ob_get_contents();
			ob_end_clean();
	    
			if( $html != "" )
			{
				?>
				<div style="margin-top : 0.6em; margin-left : 0.7em;">
					See also:
					<ul>
						<?= $html ?>
					</ul>
				</div>
				<?
			}
		}
	    
		?>
	</td></tr></table>
	<div class="notsowide" style="margin-left : 0.7em; padding-top : 0.7em;">
		<?

		$url = url( "helpdesk/request/improve/faq/".$faqData[ "hfqid" ]);

		?>
		<div class="mar_top">
			This article was created by <?= getUserLink( $faqData[ "hfqCreatedBy" ]) ?> on <?
			echo gmdate( $_auth[ "useDateFormat" ], applyTimezone( strtotime( $faqData[ "hfqCreateDate" ])));

			if( $faqData[ "hfqEditDate" ] != $faqData[ "hfqCreateDate" ])
			{
				?>
				and was last modified by <?= getUserLink( $faqData[ "hfqEditedBy" ]) ?> on <?
				echo gmdate( $_auth[ "useDateFormat" ], applyTimezone( strtotime( $faqData[ "hfqEditDate" ])));
			}

			?>.
		</div>
		<div class="mar_top">
			If you feel that this article is outdated, needs improvement
			or lacks some important tags that would help to link it better
			with the other articles, please <a href="<?= $url ?>">report to helpdesk</a>.
		</div>
	</div>
	<?
}

if( $isEditMode )
{
	$editAction = "Edit";
	include( INCLUDES."p_helpdesk!faq-edit.php" );
}
