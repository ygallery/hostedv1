<div class="header">
	<div class="header_title"><?=_USE_EMAIL_CHANGE ?></div>
</div>
<div class="container">
<?
	$_documentTitle = _USE_EMAIL_CHANGE;

	$user = $_cmd[1];
	if(isset($_GET["key"])) {
		$key = $_GET["key"] ? $_GET["key"] : "--";
		$result = sql_query("SELECT `useid`,`useActivationKey`,`useEmail`,`useUsername` FROM `users`,`useExtData` WHERE `useid` = `useEid` AND `useUsername` = '".addslashes($user)."' AND `useActivationKey` = '".addslashes($key)."' LIMIT 1");
		$onError = _USE_CHANGE_EMAIL_NO_PERMISSION;
	}
	else {
		$result = sql_query("SELECT `useid`,`useActivationKey`,`useEmail`,`useUsername` FROM `users`,`useExtData` WHERE `useid` = `useEid` AND `useUsername` = '".addslashes($user)."' AND `useIsActive` = '0' LIMIT 1");
		$onError = _USE_ACTIVATE_NOT_FOUND;
	}
	if(!$useData = mysql_fetch_assoc($result)) {
		notice($onError);
		echo '</div>';
		return;
	}
	// change the email address and resend the activation key
	include_once(INCLUDES."mailing.php");
	if(isset($_POST["email"])) {
		if(checkEmail($_POST["email"])) {
			sql_query("UPDATE `useExtData` SET `useIsActive` = '0', `useEmail` = '".addslashes($_POST["email"])."' WHERE `useEid` = '".$useData["useid"]."' LIMIT 1");
			terminateSession($_auth["useid"]); // force the user to verify their new email address first
			redirect(url("emailresend/".strtolower($useData["useUsername"])));
		}
		else
			notice(_JOIN_EMAIL_INVALID);
	}
?>

<form action="<?=url(".", isset($_GET["key"]) ? array("key" => $_GET["key"]) : array())?>" method="post">
<div class="caption"><?=_USE_EMAIL_NEW_ADDRESS ?>:</div>
<div><input class="notsowide" name="email" type="text" value="<?=htmlspecialchars(isset($_POST["email"]) ? $_POST["email"] : $useData["useEmail"])?>" /></div>
<div class="sep"><input class="submit" type="submit" value="<?=_CHANGE ?>" /></div>
</form>

</div>
