<?

include_once( INCLUDES."layout!sidebar-func.php" );

?>
<div class="padded" style="padding-top: 0">
	<?

	putSidebarModStuff();

	?>
	<?/*<div class="caption"><?= _ACCOUNT ?>:</div>*/?>
	<div class="container2 a_center">
		<div>
			<?

			if( isLoggedIn() )
				printf( _WELCOME_USER, getUserLink( $_auth[ "useid" ]));
			else
				printf( _WELCOME_USER, "guest" );

			?>
		</div>
		<?

		if( isLoggedIn() )
		{
			?>
			<div class="sep">
				<a href="<?= url( "updates" ) ?>"><b><?= _UPDATES ?></b></a><br />
				<?

				putSidebarUpdates();

				?>
			</div>
			<?
		}

		?>
		<div class="hline"><br /></div>
		<div class="sep">
			<?

			if( isLoggedIn() )
            {
            ?>
            <div class="f_left" style="margin-left: 12px">
                <a href="<?= url( "/" ) ?>">
                    News
                </a>
            </div>
            <div class="f_right" style="margin-right: 12px">
                <a href="<?= url( "club/11592/" ) ?>">
                    Site Changelog
                </a>
            </div>
            <div class="clear">
                <br />
            </div>
            <div class="f_left" style="margin-left: 12px">
                <a href="<?= url( "browse" ) ?>">
                    <?= _BROWSE ?>
                </a>
            </div>
            <div class="f_right" style="margin-right: 12px">
                <a href="<?= url( "search" ) ?>">
                    <?= _SEARCH ?>
                </a>
            </div>
            <div class="clear">
                <br />
            </div>
            <div class="f_left" style="margin-left: 12px">
                <a href="<?= url( "helpdesk/faq" ) ?>">
                    FAQ.
                </a>
            </div>
            <div class="f_right" style="margin-right: 12px">
                <a href="<?= url( "submit" ) ?>">
                    <?= _SUBMIT ?>
                </a>
            </div>
            <div class="clear">
                <br />
            </div>
            <div class="f_left" style="margin-left: 12px">
                <a href="<?= url( "club/1" ) ?>">
                    Help Desk
                </a>
            </div>
            <div class="f_right" style="margin-right: 12px">
                <a href="http://www.y-gallery.net/pm/for/SpiralSea">
                    Ask How to Donate
                </a>
            </div>
            <div class="clear">
                <br />
            </div>
            <div class="f_left" style="margin-left: 12px">
                <a href="http://www.y-gallery.net/helpdesk/faq/general/advert">
                    Advertise
                </a>
            </div>
            <div class="f_right" style="margin-right: 12px">
                <a href="<?= url( "logout" ) ?>" onclick="return confirm('<?= _ARE_YOU_SURE ?>')">
                    <?= _LOGOUT ?>
                </a>
            </div>
            <div class="clear">
                <br />
            </div>
        <?
            }
            else
            {
        ?>
            <div class="f_left" style="margin-left: 12px">
                <a href="<?= url( "/" ) ?>">
                    News
                </a>
            </div>
            <div class="f_right" style="margin-right: 12px">
                <a href="<?= url( "club/11592/" ) ?>">
                    Site Changelog
                </a>
            </div>
            <div class="clear">
                <br />
            </div>
            <div class="f_left" style="margin-left: 12px">
                <a href="<?= url( "browse" ) ?>">
                    <?= _BROWSE ?>
                </a>
            </div>
            <div class="f_right" style="margin-right: 12px">
                <a href="<?= url( "helpdesk/faq" ) ?>">
                    Faq
                </a>
            </div>
            <div class="clear">
                <br />
            </div>
            <div class="f_left" style="margin-left: 12px">
                <a href="<?= url( "helpdesk/faq/staff/info" ) ?>">
                    <?= _STAFF ?>
                </a>
            </div>
        <?
            }
            

			?>
		</div>
		<div class="hline"><br /></div>
		<?

		if( isLoggedIn() )
		{
			?>
			<div class="sep" style="margin-left: 8px; margin-right: 8px; white-space: nowrap">
				<a href="<?= url( "journal" )?>"><?= _JOURNAL ?></a>
				&middot;
				<a href="<?= url( "pm" )?>"><?= $_auth["useUpdPms"]
					? getIMG( url()."images/emoticons/pmnew.png" )." "._PRIVATE
					: getIMG( url()."images/emoticons/pmnonew.png" )." "._PRIVATE ?></a>
				&middot;
				<a href="<?= url( "settings/site" )?>"><?= _SETTINGS ?></a>
			</div>
			<div class="sep">
				<a href="javascript:popup('<?= url( "tos", array( "popup" => "yes" )) ?>','tos',900,700)"><?= _TOS ?></a>
				<?
				if( atLeastHelpdesk() )
				{
				?>
				<br /><a href="<?= url( "admin" ) ?>"><?= _ADMINISTRATION ?></a>
				<?
				}
				?>
			</div>
			<?
		}
		else
		{
			putSidebarLogin();
		}

		putSidebarBanner();

		?>
	</div>
	<?

	if( isLoggedIn() && $_cmd[ 0 ] == "updates" )
	{
		$sql = "SELECT * FROM `objects`, `objExtData`".dbWhere( array(
			"objid*" => "objEid",
			"objCollab" => $_auth[ "useid" ],
			"objCollabConfirmed" => 0,
			"objDeleted" => 0,
			"objPending" => 0 ));

		$objResult = sql_query( $sql );

		if( $objData = mysql_fetch_assoc( $objResult ))
		{
			$objid = $objData[ "objid" ];

			?>
			<div class="sep caption"><?= _CONFIRMATION_NEEDED ?>:</div>
			<div class="container2 a_center">
				<div class="a_left padded">
					<?= _COLLAB_CONFIRM_EXPLAIN ?>
				</div>
				<table align="center" cellspacing="0" cellpadding="8" border="0">
				<tr>
					<td><?= getObjectThumb( $objid, 40 ) ?></td>
					<td>
						<a href="<?= url( "confirm/".$objid, array( "accept" => "1" )) ?>"
							onclick="return confirm('<?= _CONFIRM ?>? <?= _ARE_YOU_SURE ?>')">
							<?= getIMG( url()."images/emoticons/checked.png" ) ?>
							<?= _CONFIRM ?>
						</a>
						<br /><br />
						<a href="<?= url( "confirm/".$objid, array( "accept" => "0" )) ?>"
							onclick="return confirm('<?= _REFUSE ?>? <?= _ARE_YOU_SURE ?>')">
							<?= getIMG( url()."images/emoticons/cancel.png" ) ?>
							<?= _REFUSE ?>
						</a>
					</td>
				</tr>
				</table>
			</div>
			<?
		}
	}

	if( !isset($_auth["useSidebarThumbs"]) || $_auth["useSidebarThumbs"])
	{
		?>
		<div class="sep caption"><?= _MOST_RECENT ?>:</div>
		<div class="container2 a_center">
			<?

			putSidebarMostRecent();

			?>
		</div>
		<?
	}

	if( isLoggedIn() && trim( $_auth[ "useSidebar" ]) != "" )
	{
		?>
		<div class="sep caption"><?= _PERSONAL_NOTES ?>:</div>
		<div class="container2">
			<?= formatText( $_auth[ "useSidebar" ])?>
		</div>
		<?
	}

	if( file_exists( INCLUDES."extras.php" ))
		include( INCLUDES."extras.php" );

	if( !isset($_auth["useSidebarThumbs"]) || $_auth["useSidebarThumbs"])
	{
		?>
		<div class="sep caption"><?= _RANDOM ?>:</div>
		<div class="container2 a_center">
			<?

			putSidebarRandom();

			?>
		</div>
		<?
	}

	putSidebarPoll();

	?>
	<div class="sep caption">
		<? printf( _ADS_CAPTION, $_config[ "galName" ]); ?>:
	</div>
	<div class="container2 a_center">
		<div style="padding-top: 20px; padding-bottom: 20px">
			<?= putAds( 160, 600 ) ?>
		</div>
	</div>
</div>
