<?

// This script controls the "Interaction" tab of the settings.

if( !isLoggedIn() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

?>
<div class="header">
	<div class="header_title">
		<?= _SETTINGS ?>
		<div class="subheader"><?= _SET_INTERACTION_SETTINGS ?></div>
	</div>
	<?

	$_documentTitle = _SET_INTERACTION_SETTINGS;

	$active = 3;

	include( INCLUDES."mod_setmenu.php" );

	?>
</div>
<div class="container">
	<?

	if( isset( $_POST[ "submit" ]))
	{
		// Comments
		
		$signatureLines=count( explode( "\n", $_POST[ "useSignature"]));
		
		
		if(strlen( $_POST[ "useSignature" ] ) < 1000 && $signatureLines < 6) {
			 	$_auth[ "useSignature" ] = $_POST[ "useSignature" ];
		}
		else {
			$_auth[ "useSignature" ] = $_auth[ "useSignature" ];
			notice( _SET_SIGNATURE_LIMIT );
		}

		$_auth[ "useNoSig" ] = isset( $_POST[ "useNoSig" ]) ? 1 : 0;

		$_auth[ "useNoEmoticons" ] = isset( $_POST[ "useNoEmoticons" ]) ? 1 : 0;

		$_auth[ "useNoBBCode" ] = isset( $_POST[ "useNoBBCode" ]) ? 1 : 0;

		// Options

		$_auth[ "useNotifyWatch" ] = isset( $_POST[ "useNotifyWatch" ]) ? 1 : 0;

		$_auth[ "useNotifyFavs" ] = isset( $_POST[ "useNotifyFavs" ]) ? 1 : 0;

		// Twit List

		$ignoreList = preg_split( '/[^a-zA-Z0-9]/', $_POST[ "ignoreList" ], -1, PREG_SPLIT_NO_EMPTY );
		$twit = array();

		foreach( $ignoreList as $username )
		{
			$useResult = sql_query( "SELECT `useid` FROM `users`".dbWhere( array(
				"useUsername" => $username )));

			if( $useData = mysql_fetch_assoc( $useResult ))
			{
				if( $useData[ "useid" ] != $_auth[ "useid" ])
				{
					$twit[] = $useData[ "useid" ];
				}
			}

			mysql_free_result( $useResult );
		}

		sql_query( "DELETE FROM `twitList`".dbWhere( array(
			"twtCreator" => $_auth[ "useid" ])));

		$twit = array_unique( $twit );

		foreach( $twit as $useid )
		{
			sql_query( "INSERT INTO `twitList`".dbValues( array(
				"twtCreator" => $_auth[ "useid" ],
				"twtBadUser" => $useid )));
		}

		recountAllUpdates( $_auth[ "useid" ]);

		// Update the database

		sql_query( "UPDATE `useExtData`".dbSet( array(
			"useSignature" => $_auth[ "useSignature" ],
			"useNoSig" => $_auth[ "useNoSig" ],
			"useNoEmoticons" => $_auth[ "useNoEmoticons" ],
			"useNoBBCode" => $_auth[ "useNoBBCode" ],
			"useNotifyWatch" => $_auth[ "useNotifyWatch" ],
			"useNotifyFavs" => $_auth[ "useNotifyFavs" ])).dbWhere( array(
			"useEid" => $_auth[ "useid" ])));

		notice( _SET_SAVED );
	}

	// ======================================================================================================
	// FOUND NEW CLUB
	// ======================================================================================================

	?>
	<? iefixStart() ?>
	<div class="sep largetext"><?= _CLUB_FOUND ?></div>
	<div class="container2 notsowide">
		<form action="<?= url("newclub") ?>" method="post">
			<table cellspacing="0" cellpadding="4" border="0" width="100%">
			<tr>
				<td><?= _TITLE ?>:</td>
				<td width="100%"><input class="largetext" size="40" name="clubname" type="text" value="" /></td>
			</tr>
			<tr>
				<td></td>
				<td width="100%">
					<button class="submit" name="submit" type="submit">
						<?=getIMG(url()."images/emoticons/club2.png") ?>
						<?=_CLUB_CREATE ?>
					</button>
				</td>
			</tr>
			</table>
		</form>
	</div>
	<? iefixEnd() ?>
	<?

	// ======================================================================================================
	// COMMENTS
	// ======================================================================================================

	?>
	<form action="<?= url( "." ) ?>" method="post">
	<? iefixStart() ?>
	<div class="sep largetext"><?= _COMMENTS ?></div>
	<div class="container2 notsowide">
		<table cellspacing="0" cellpadding="4" border="0" width="100%">
		<tr>
			<td valign="top" align="right" class="nowrap">
				<div><?= _SET_SIGNATURE ?>:</div>
				<?

				$script = "make_invisible('sign_current'); ".
					"make_invisible('sign_edit'); ".
					"make_visible('sign_change'); ".
					"return false;";

				$signEdit = '<div id="sign_edit">'.
					'(<a href="" onclick="'.$script.'">'._EDIT.'</a>)</div>';

				if( trim( $_auth[ "useSignature" ]) != "" )
				{
					echo $signEdit;
				}

				?>
			</td>
			<td width="100%">
				<div id="sign_current">
					<?

					if( trim( $_auth[ "useSignature" ]) != "" )
					{
						?>
						<div class="container2"><?= formatText( $_auth[ "useSignature" ]) ?></div>
						<?
					}
					else
					{
						echo $signEdit;
					}

					?>
				</div>
				<div id="sign_change" style="display: none">
					<div>
						<?

						iefixStart();

						$commentName = "useSignature";
						$commentDefault = $_auth[ "useSignature" ];
						$commentNoOptions = true;
						$commentRows = 6;

						include( INCLUDES."mod_comment.php" );

						iefixEnd();

						?>
						<div class="clear">&nbsp;</div>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td align="right">
				<input <?= $_auth[ "useNoSig" ] ? 'checked="checked"' : ""?> class="checkbox"
					id="idNoSig" name="useNoSig" type="checkbox" />
			</td>
			<td>
				<label for="idNoSig"><?= _SET_COMMENTS_NO_SIG ?></label>
			</td>
		</tr>
		<tr>
			<td align="right">
				<input <?= $_auth[ "useNoEmoticons" ] ? 'checked="checked"' : ""?> class="checkbox"
					id="idNoEmoticons" name="useNoEmoticons" type="checkbox" />
			</td>
			<td>
				<label for="idNoEmoticons"><?= _SET_COMMENTS_NO_EMOTICONS ?></label>
			</td>
		</tr>
		<tr>
			<td align="right">
				<input <?= $_auth[ "useNoBBCode" ] ? 'checked="checked"' : ""?> class="checkbox"
					id="idNoBBCode" name="useNoBBCode" type="checkbox" />
			</td>
			<td>
				<label for="idNoBBCode"><?= _SET_COMMENTS_NO_BBCODE ?></label>
			</td>
		</tr>
		</table>
	</div>
	<? iefixEnd() ?>
	<?

	// ======================================================================================================
	// OPTIONS
	// ======================================================================================================

	?>
	<? iefixStart() ?>
	<div class="sep largetext"><?= _OEKAKI_OPTIONS ?></div>
	<div class="container2 notsowide">
		<table cellspacing="0" cellpadding="4" border="0">
		<tr>
			<td align="right">
				<input <?= $_auth[ "useNotifyWatch" ] ? 'checked="checked"' : ""?> class="checkbox"
					id="idNotifyWatch" name="useNotifyWatch" type="checkbox" />
			</td>
			<td>
				<label for="idNotifyWatch"><?= _SET_NOTIFY_WATCH ?></label>
			</td>
		</tr>
		<tr>
			<td align="right">
				<input <?= $_auth[ "useNotifyFavs" ] ? 'checked="checked"' : ""?> class="checkbox"
					id="idNotifyFavs" name="useNotifyFavs" type="checkbox" />
			</td>
			<td>
				<label for="idNotifyFavs"><?= _SET_NOTIFY_FAVS ?></label>
			</td>
		</tr>
		</table>
	</div>
	<? iefixEnd() ?>
	<?

	// ======================================================================================================
	// IGNORE LIST
	// ======================================================================================================

	?>
	<? iefixStart() ?>
	<div class="sep largetext"><?= _SET_TWIT_LIST ?></div>
	<div class="container2 notsowide">
		<table cellspacing="0" cellpadding="4" border="0" width="100%">
		<tr>
			<td valign="top" align="right" class="nowrap">
				<div><?= _SET_IGNORE ?>:</div>
				<?

				$script = "make_invisible('ignore_current'); ".
					"make_invisible('ignore_edit'); ".
					"make_visible('ignore_change'); ".
					"return false;";

				$ignoreEdit = '<div id="ignore_edit">'.
					'(<a href="" onclick="'.$script.'">'._EDIT.'</a>)</div>';

				$ignoreListRaw = "";
				$ignoreList = "";

				$twtResult = sql_query( "SELECT * FROM `twitList`, `users`".dbWhere( array(
					"useid*" => "twtBadUser",
					"twtCreator" => $_auth[ "useid" ]))."ORDER BY `useUsername`" );

				$first = true;

				while( $twtData = mysql_fetch_assoc( $twtResult ))
				{
					$ignoreListRaw .= ( $first ? "" : ", " ).$twtData[ "useUsername" ];
					$ignoreList .= "[u=".$twtData[ "useUsername" ]."] ";

					$first = false;
				}

				mysql_free_result( $twtResult );

				if( $ignoreList != "" )
				{
					echo $ignoreEdit;
				}

				?>
			</td>
			<td width="100%">
				<div id="ignore_current">
					<?

					if( $ignoreList != "" )
					{
						?>
						<div class="container2"><?= formatText( $ignoreList ) ?></div>
						<?
					}
					else
					{
						echo $ignoreEdit;
					}

					?>
					<div class="sep">
						<?= _SET_TWIT_EXPLAIN ?>
					</div>
				</div>
				<div id="ignore_change" style="display: none">
					<div>
						<textarea cols="60" rows="3" name="ignoreList"><?= $ignoreListRaw ?></textarea>
					</div>
					<div class="sep">
						<?= _SET_TWIT_EXAMPLE ?>
					</div>
				</div>
			</td>
		</tr>
		</table>
	</div>
	<? iefixEnd() ?>
	<?

	// ======================================================================================================
	// INVITATIONS
	// ======================================================================================================

	if( $_config[ "invitations" ])
	{
		if( strlen( $_auth[ "useInvitation" ] < 10 ))
		{
			while( true )
			{
				// Generate a new random code.

				$code = "";
				$l = 10;

				while( $l > 0 )
				{
					$code .= mt_rand( 0, 9 );
					$l--;
				}

				// Make sure the code is unique.

				$result = sql_query( "SELECT COUNT(*) FROM `useExtData`".dbWhere( array(
					"useInvitation" => $code )));

				if( mysql_result( $result, 0 ) == 0 )
				{
					break;
				}
			}

			sql_query( "UPDATE `useExtData`".dbSet( array(
				"useInvitation" => $code )).dbWhere( array(
				"useEid" => $_auth[ "useid" ])));

			$_auth[ "useInvitation" ] = $code;
		}

		?>
		<? iefixStart() ?>
		<div class="sep largetext"><?= _INVITATIONS ?></div>
		<div class="container2 notsowide">
			<table cellspacing="0" cellpadding="4" border="0" width="100%">
			<tr>
				<td valign="center" class="nowrap">
					<?= _INVITATION_CODE ?>:
				</td>
				<td valign="center" width="100%" class="largetext">
					<?= $_auth[ "useInvitation" ] ?>
				</td>
			</tr>
			<tr>
				<td valign="center" width="100%" colspan="2">
					You may give this invitation code to your friend who wants to join
					<?= $_config[ "galName" ] ?>.
					You can give it to only one person at a time.
					This code will change after it was used.
				</td>
			</tr>
			</table>
		</div>
		<? iefixEnd() ?>
		<?
	}

	// ======================================================================================================

	?>
	<div class="sep">
		<button class="submit" name="submit" type="submit">
			<?= getIMG( url()."images/emoticons/checked.png" ) ?>
			<?= _SAVE_CHANGES ?>
		</button>
	</div>
	</form>
</div>
