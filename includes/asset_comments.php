<?

include_once( INCLUDES."formatting.php" );
include_once( INCLUDES."timezones.php" );
include_once( INCLUDES."comments.php" );

$GLOBALS[ "yg_is_asset" ] = true;

$_GET[ "offset" ] = $_asset_in[ "offset" ];
$_commentsPerPage = $_asset_in[ "limit" ];

$saveCmd = $_cmd;

$_cmd[ 0 ] = $_asset_in[ "cmd_0" ];
$_cmd[ 1 ] = $_asset_in[ "cmd_1" ];
$_cmd[ 2 ] = $_asset_in[ "cmd_2" ];

$_currentPageURL = url( $_cmd[ 0 ]."/".$_cmd[ 1 ]."/".$_cmd[ 2 ],
	array( "replied" => "yes" ));

ob_start();

showAllComments(
	$_asset_in[ "objid" ],
	$_asset_in[ "objType" ],
	/*'reserved' is always false*/false,
	$_asset_in[ "allowReply" ],
	$_asset_in[ "showChilds" ],
	$_asset_in[ "oldestFirst" ],
	$_asset_in[ "useCache" ]);

$_asset_out[ "html" ] = ob_get_contents();
ob_end_clean();

$_cmd = $saveCmd;

?>