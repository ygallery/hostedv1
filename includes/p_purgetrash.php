<div class="header">
	<div class="header_title">
		Purge trash
	</div>
</div>
<div class="container">
	<?

	sql_order( "objTitle" );
	sql_where( array(
		"objEid*" => "objid",
		"objPending" => 0,
		"objCreator" => $_auth[ "useid" ],
		"objDeleted" => 1,
		"objDeletedBy" => $_auth[ "useid" ],
		"objDeleteDate<!" => "DATE_SUB( NOW(), INTERVAL 30 DAY )" ));

	$result = sql_rowset( "objects, objExtData" );

	$showForm = sql_num_rows( $result ) > 0 && !isset( $_POST[ "submit" ]);

	if( $showForm )
	{
		?>
		<form action="<?= url( "." ) ?>" method="post">
		<div>
			Select deleted submissions you want to purge forever:
		</div>
		<?
	}

	include_once( INCLUDES."submission.php" );

	while( $objData = sql_next( $result ))
	{
		if( isset( $_POST[ "delete".$objData[ "objid" ]]))
		{
   			eraseSubmission( $objData[ "objid" ], false, false );

   			notice( "Purged: ".$objData[ "objTitle" ]);

			continue;
		}

		if( $showForm )
		{
			?>
			<div class="sep">
				<input type="checkbox" class="checkbox"
					name="delete<?= $objData[ "objid" ] ?>" />
				<a target="_blank" href="<?= url( "view/".$objData[ "objid" ] ) ?>">
					<?= $objData[ "objTitle" ] ?></a>
			</div>
			<?
		}
	}

	sql_free( $result );

	?>
	<div class="sep">
		<u>Note</u>: you can only purge trash that has been in the trashcan for <b>at least 30 days</b>.
	</div>
	<div class="sep">
		Purging removes all related files from the server and all the comments attached to submissions.
	</div>
	<?

	if( $showForm )
	{
		?>
		<div class="sep">
			<b>This cannot be undone!</b>
		</div>
		<div class="sep">
			<input type="submit" class="submit" name="submit" value="Purge" />
		</div>
		</form>
		<?
	}

	?>
</div>
