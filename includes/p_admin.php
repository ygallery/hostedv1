<?

$_documentTitle = _ADMINISTRATION;

if( !atLeastHelpdesk() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

if( isset( $_POST[ "submitLog" ]))
{
	sql_query( "INSERT INTO `adminNotes`".dbValues( array(
		"admNote" => str_replace("\r\n", "\n", $_POST['modMessage']),
		"admCreator" => $_auth[ "useid" ],
		"admSubmitDate!" => "NOW()")));
		redirect( url( "." ));
}

$modOffset = isset($_GET[ "offset" ]) ? $_GET[ "offset" ] : "0";

?>
<div class="header">
	<div class="header_title">
		<?= _ADMINISTRATION ?>
		<div class="subheader"><?= _HOME ?></div>
	</div>	
	<?

	$active = 1;
	include(INCLUDES."mod_adminmenu.php");

	?>
</div>

<div class="container">
	<h1>Currently online staff</h1>
<?php
#	$minTime = date( "Y-m-d H:i:s", time() - 600 );
#	$minTime2 = date( "Y-m-d H:i:s", time() - 800 );
#	$result = sql_query("select count(*) from users where useLastAction > date_sub(NOW(), interval 5 minute)");
#	$result = sql_query("SELECT COUNT(*) FROM `users` WHERE `useLastAction` > '$minTime'");
#	$onlineUsers = mysql_result($result, 0);
#	print("$minTime2 <br />");
#	print_r($onlineUsers);
?>
	<div class="container2 mar_bottom">
<?
		$friendQuery = "SELECT `useid` FROM `users`,`useExtData` WHERE `useid` = `useEid` AND (`useIsModerator` = '1' OR `useIsSModerator` = '1' OR `useIsDeveloper` = '1' OR `useIsHelpdesk` = '1') AND `useLastAction` > date_sub(NOW(), interval 5 minute) ORDER BY `useIsSModerator` DESC, `useIsModerator` DESC, `useLastAction` DESC";
		include(INCLUDES."mod_friends.php");
?>
	</div>

	<h1>Admin notes</h1>
	<div class="container2 mar_bottom">
		<?= iefixStart() ?>
		<div class="mar_bottom">
			<a href="<?=url(".").isset($_GET['all']) ? '?all' : '' ?>">Refresh</a>
			 &bull; <a href="<?=url(".")?>">View all previous notes</a>
			 <?= $modOffset >= "30" ? '&bull; <a href="'.url( ".", array( "offset" => $modOffset-30 )).'">Previous Page</a>' : ""?>
			 &bull; <a href="<?= url( ".", array( "offset" => $modOffset+30))?>">Next Page</a>
		</div>
		<form action="<?= url( "." )?>" method="post">
			<a name="modlog"></a>
			<?

			if(isset($_GET['all']))
			{
				$modLimit = "";
			}
			else
			{
				$modLimit = "LIMIT $modOffset,30";
			}

			$modResult = sql_query("SELECT * FROM `adminNotes` ORDER BY `admSubmitDate` DESC $modLimit");

			while( $modData = mysql_fetch_assoc( $modResult ))
			{
				?>
				<div class="sep mar_left mar_right">
					#<?= $modData["admid"] ?> - <?= gmdate($_auth["useDateFormat"], applyTimezone(strtotime($modData[ "admSubmitDate" ]))) ?>
					- <?= getUserLink( $modData[ "admCreator" ]) ?>:
					<?= formatText( $modData[ "admNote" ] ) ?>
				</div>
				<?
			}

			mysql_free_result( $modResult );

			?>
			<div class="sep a_center">
				<?
				iefixStart();

						$commentName = "modMessage";
						$commentNoOptions = true;
						$commentRows = 8;

						include( INCLUDES."mod_comment.php" );

					iefixEnd();
							
				?>
			</div>
			<div class="sep a_center">
				<input class="submit" type="submit" name="submitLog" value="Add Message" />
			</div>
		</form>
		<?= iefixEnd() ?>
	</div>
</div>
