<div class="header">
	<div class="header_title">
		<?=_CLUB ?>
		<div class="subheader"><?=_CLUB_FOUND ?></div>
	</div>
</div>

<div class="container">
	<?
	$_documentTitle = _CLUB;

	if(!$_auth["useid"]) {
		header("Status: 403 Forbidden");
		notice(_REQUIRE_LOGIN);
		echo "</div>";
		return;
	}
	if(isset($_POST["submit"])) {
		$validated = true;
		$clubname = trim($_POST["clubname"]);

		if(!$clubname) {
			$validated = false; notice(_CLUB_NAME_BLANK);
		}
		elseif(strlen($clubname) < $_config["minUsernameLength"]) {
			$validated = false;
			notice(sprintf(_CLUB_NAME_TOO_SHORT, $_config["minUsernameLength"]));
		}
		elseif(mysql_num_rows(sql_query(
			"SELECT `cluid` FROM `clubs` WHERE `cluName` = '".addslashes($clubname)."'"))) {
			$validated = false; notice(sprintf(_CLUB_NAME_EXISTS,$clubname));
		}

		if(!$validated)
			notice(_CLUB_NOT_CREATED);
		else {
			// add club to the database
			sql_query("INSERT INTO `clubs`(`cluName`) VALUES('".addslashes($clubname)."')");
			$cluid = mysql_insert_id(); // get newly created club id
			// add extra club data to the database
			sql_query("INSERT INTO `cluExtData`(`cluEid`,`cluCreator`,`cluCreationDate`) VALUES('$cluid','".$_auth["useid"]."',NOW())");
			// add the user themselves to the club's members
			sql_query("INSERT INTO `useClubs`(`useCclub`,`useCmember`,`useCpending`, `useCModerator`) VALUES('$cluid','".$_auth["useid"]."','0','1')");
			// add the user to the club's watch list
			sql_query("INSERT INTO `watches`(`watCreator`,`watUser`,`watSubmitDate`,`watType`) ".
				"VALUES('$cluid','".$_auth["useid"]."',NOW(),'clu')");
			// redirect the user to their newly founded club
			redirect(url("manageclub/".$cluid));
		}
	}
	?>
	<form action="<?=url(".")?>" method="post">
	<div class="caption"><?=_CLUB_NAME ?>:</div>
	<div>
		<input class="largetext notsowide" name="clubname" type="text"
			<?= isset ($_POST ['clubname']) ? 'value="'.htmlspecialchars($_POST["clubname"]).'"' : "" ?> />
	</div>
	<div class="sep">
		<button class="submit" name="submit" type="submit">
			<?=getIMG(url()."images/emoticons/checked.png") ?>
			<?=_CLUB_CREATE ?>
		</button>
	</div>
	</form>
</div>
