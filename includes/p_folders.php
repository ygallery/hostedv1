<?

// This script controls the "Folders" tab of the settings.

if( !isLoggedIn() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

?>
<div class="header">
	<div class="header_title">
		<?= _SETTINGS ?>
		<div class="subheader"><?= _SET_FOLDER_SETTINGS ?></div>
	</div>
	<?

	$_documentTitle = _SET_FOLDER_SETTINGS;

	$active = 5;

	include( INCLUDES."mod_setmenu.php" );

	?>
</div>
<div class="container">
	<?

	$folid = 0;
	$folIdent = "";
	$defaultTitle = "";

	if( isset( $_GET[ "created" ]))
	{
		notice( sprintf( _SET_FOLDER_CREATED, formatText( $_GET[ "created" ], false, true )));
	}

	if( isset( $_GET[ "edited" ]))
	{
		notice( sprintf( _SET_FOLDER_EDITED, formatText( $_GET[ "edited" ], false, true )));
	}

	if( $_cmd[ 1 ] == "edit" || $_cmd[ 1 ] == "remove" || $_cmd[ 1 ] == "remove2" )
	{
		$folResult = sql_query( "SELECT * FROM `folders`".dbWhere( array(
			"folCreator" => $_auth[ "useid" ],
			"folIdent" => $_cmd[ 2 ])));

		if( $folData = mysql_fetch_assoc( $folResult ))
		{
			$folid = $folData[ "folid" ];
			$folIdent = $folData[ "folIdent" ];
			$defaultTitle = $folData[ "folName" ];
		}
		else
		{
			redirect( url( "folders" ));
		}

		mysql_free_result( $folResult );
	}

	if( $_cmd[ 1 ] == "remove2" && $folid > 0 )
	{
		$iconFilename = applyIdToPath( "files/foldericons/", $folid );

		$oldFiles = glob( $iconFilename."-*", GLOB_NOESCAPE );

		if( is_array( $oldFiles ) && count( $oldFiles ) > 0 )
		{
			foreach( $oldFiles as $oldFile )
			{
				unlink( $oldFile ); // Delete old files
			}
		}

		sql_query( "DELETE FROM `folders`".dbWhere( array(
			"folid" => $folid )));

		sql_query( "UPDATE `objects`".dbSet( array(
			"objFolder" => 0 )).dbWhere( array(
			"objFolder" => $folid )));

		redirect( url( "folders" ));
	}

	if( isset( $_POST[ "submit" ]))
	{
		while( true ) // Easy to break on errors
		{
			$folName = $_POST[ "folName" ];
			$folIdent = substr( preg_replace( '/[^a-z0-9]/', "",
				strtolower( strip_tags( formatText( $folName )))), 0, 15 );

			if( $folIdent == "" )
			{
				break;
			}

			// Folder identifier must not match any other folder identifiers
			// for the current user.

			$folResult = sql_query( "SELECT COUNT(*) FROM `folders`".dbWhere( array(
				"folid<>" => $folid,
				"folCreator" => $_auth[ "useid" ],
				"folIdent" => $folIdent )));

			$conflicting = mysql_result( $folResult, 0 ) > 0;

			mysql_free_result( $folResult );

			if( $conflicting )
			{
				$defaultTitle = $folName; // Put it back in the form for edition

				notice( _SET_FOLDER_EXISTS );
				break;
			}

			$values = array(
				"folCreator" => $_auth[ "useid" ],
				"folName" => $folName,
				"folIdent" => $folIdent );

			if( $folid == 0 )
			{
				sql_query( "INSERT INTO `folders`".dbValues( $values ));

				$folid = mysql_insert_id();
			}
			else
			{
				sql_query( "UPDATE `folders`".dbSet( $values ).dbWhere( array(
					"folid" => $folid )));
			}

			// Upload icon.

			include_once( INCLUDES."files.php" );

			$iconError = checkUploadedFile( "folIcon" );

			if( !$iconError )
			{
				if( filesize( $_FILES[ "folIcon" ][ "tmp_name" ]) > $_config[ "maxIconSize" ])
				{
					$iconError = sprintf( _SET_ICON_SIZE_EXCEEDED, $_config[ "maxIconSize" ]);
				}
				else
				{
					// Check avatar image size/type.

					$iconInfo = getimagesize( $_FILES[ "folIcon" ][ "tmp_name" ]);

					list( $iconWidth, $iconHeight ) = preg_split( '/x/', $_config[ "iconResolution" ]);

					if( $iconInfo[ 0 ] != $iconWidth || $iconInfo[ 1 ] != $iconHeight ||
						( $iconInfo[ 2 ] != 1 && $iconInfo[ 2 ] != 2 && $iconInfo[ 2 ] != 3 ))
					{
						$iconError = sprintf( _SET_ICON_TOO_LARGE, $_config[ "iconResolution" ]);
					}
					else
					{
						// Upload folder icon to /files/foldericons/#/#####/

						$iconFilename = applyIdToPath( "files/foldericons/", $folid );

						$oldFiles = glob( $iconFilename."-*", GLOB_NOESCAPE );

						if( is_array( $oldFiles ) && count( $oldFiles ) > 0 )
						{
							foreach( $oldFiles as $oldFile )
							{
								unlink( $oldFile ); // Delete old files
							}
						}

						uploadFile( "folIcon", $iconFilename.'-'.time(), $extension );
					}
				}
			}

			if( $iconError != "" && $iconError != _UPL_NO_FILE )
			{
				if( $_cmd[ 1 ] == "new" )
				{
					sql_query( "DELETE FROM `folders`".dbWhere( array(
						"folid" => $folid )));
				}

				$defaultTitle = $folName; // Put it back in the form for edition

				notice( $iconError );
				break;
			}

			if( $_cmd[ 1 ] == "new" )
			{
				redirect( url( "folders", array( "created" => $folName )));
			}
			else
			{
				redirect( url( "folders", array( "edited" => $folName )));
			}
		}
	}

	if( $_cmd[ 1 ] == "remove" && $folid > 0 )
	{
		?>
		<div class="sep largetext"><?= _ARE_YOU_SURE ?></div>
		<div class="container2 notsowide">
			<div>
				<?= sprintf( _SET_FOLDER_REMOVE_CONFIRM, $defaultTitle ) ?>
			</div>
			<div class="sep">
				<?= _SET_FOLDER_REMOVE_EXPLAIN ?>
			</div>
			<div class="sep">
				<form action="<?= url( "folders/remove2/".$folIdent ) ?>" method="get">
					<button class="submit" name="submit" type="submit">
						<?= getIMG( urlf()."images/emoticons/abuse.png" ) ?>
						<?= _DELETE ?>
					</button>
				</form>
				&nbsp;
				<a href="<?= url( "folders" ) ?>"><?= _CANCEL ?></a>
			</div>
		</div>
		<?
	}

	if( $_cmd[ 1 ] == "new" || $_cmd[ 1 ] == "edit" )
	{
		?>
		<? iefixStart() ?>
		<div class="sep largetext"><?= $_cmd[ 1 ] == "new" ? _SET_FOLDER_CREATE : _SET_FOLDER_EDIT ?></div>
		<div class="container2 notsowide">
			<form action="<?= url( "." ) ?>" enctype="multipart/form-data" method="post">
			<table cellspacing="0" cellpadding="4" border="0">
			<tr>
				<td align="right"><?= _TITLE ?>:</td>
				<td>
					<input type="text" name="folName" size="40"
						value="<?= htmlspecialchars( $defaultTitle ) ?>" />
				</td>
			</tr>
			<tr>
				<td align="right" class="nowrap"><?= _ICON ?>:</td>
				<td width="100%">
					<div id="icon_current">
						<?= getFolderIcon( $folid ) ?>
						<?

						$script = "make_invisible('icon_current'); ".
							"make_visible('icon_hint'); ".
							"make_visible('icon_choose'); ".
							"return false;";

						?>
						&nbsp;<a href="" onclick="<?= $script ?>"><?= _CHANGE ?></a>
					</div>
					<div id="icon_choose" style="display: none">
						<?

						$script = "make_visible('preview_container'); ".
							"make_invisible('preview_hint'); ".
							"show_preview_image('preview', 'preview_message', this.value);";

						?>
						<input accept="image/gif, image/jpeg, image/png" onchange="<?= $script ?>"
							name="folIcon" size="50" type="file" />
					</div>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<div style="display: none" id="icon_hint">
						<li><?= sprintf( _SET_ICON_TOO_LARGE, $_config[ "iconResolution" ]) ?></li>
						<li><?= sprintf( _SET_ICON_SIZE_EXCEEDED, $_config[ "maxIconSize" ]) ?></li>
					</div>
					<div style="display: none" id="preview_container">
						<div class="sep caption"><?= _PREVIEW ?>:</div>
						<div class="a_center">
							<img alt="preview" style="display: none" id="preview" src="" />
							<div id="preview_message"><?= _SUBMIT_SELECT_FILE ?></div>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<button class="submit" name="submit" type="submit">
						<?= getIMG( urlf()."images/emoticons/checked.png" ) ?>
						<?= _OK ?>
					</button>
					&nbsp; &nbsp;
					<a href="<?= url( "folders" ) ?>">
						<?= getIMG( urlf()."images/emoticons/cancel.png" )." "._CANCEL ?></a>
				</td>
			</tr>
			</table>
			</form>
		</div>
		<? iefixEnd() ?>
		<?
	}

	if( $_cmd[ 1 ] != "new" && $_cmd[ 1 ] != "edit" && $_cmd[ 1 ] != "remove" )
	{
		?>
		<? iefixStart() ?>
		<div class="sep largetext"><?= _SET_FOLDERS ?></div>
		<div class="container2 notsowide">
			<table cellspacing="0" cellpadding="4" border="0" width="100%">
				<?

				$folResult = sql_query( "SELECT * FROM `folders`".dbWhere( array(
					"folCreator" => $_auth[ "useid" ]))."ORDER BY `folName`" );

				while( $folData = mysql_fetch_assoc( $folResult ))
				{
					?>
					<tr>
						<td>
							<?= getFolderIcon( $folData[ "folid" ]) ?>
						</td>
						<td width="100%">
							&nbsp;
							<span class="largetext">
								<?= formatText( $folData[ "folName" ], false, true ) ?>
							</span>
							&nbsp; &nbsp;
							<a href="<?= url( "folders/edit/".$folData[ "folIdent" ]) ?>">
								<?= getIMG( urlf()."images/emoticons/edit.png", "" )." "._EDIT ?></a>
							&nbsp;
							<a href="<?= url( "folders/remove/".$folData[ "folIdent" ]) ?>"
								onclick="return confirm('<?= _ARE_YOU_SURE ?>')">
								<?= getIMG( urlf()."images/emoticons/cancel.png", "" )." "._REMOVE ?></a>
						</td>
					</tr>
					<?
				}

				?>
				<tr>
					<td colspan="2">
						<div>
							<a href="<?= url( "folders/new" ) ?>">
								<?= getIMG( urlf()."images/emoticons/new.png", "" )." "._SET_FOLDER_CREATE ?></a>
							<br />
						</div>
					</td>
				</tr>
			</table>
		</div>
		<? iefixEnd() ?>
		<?

	}

	?>
</div>
