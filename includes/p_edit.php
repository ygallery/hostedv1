<?

$_documentTitle = _EDIT_SUBMISSION;

if( !$_auth[ "useid" ])
{
	include( INCLUDES."p_notfound.php" );
	return;
}

if( substr( $_cmd[ 1 ], 0, 1 ) == "e" )
{
	$isExtras = true;
	$objid = intval( substr( $_cmd[ 1 ], 1 ));
	$_objects = "`extras`";
	$_objExtData = "`extExtData`";
}
else
{
	$objid = intval( $_cmd[ 1 ]);
	$isExtras = false;
	$_objects = "`objects`";
	$_objExtData = "`objExtData`";
}
$extraRights = atLeastModerator();

if( $extraRights )
{
	$conditions = "";
}
else
{
	if( isHelpdesk() )
	{
		$conditions = "AND `objDeleted` = '0'";
	}
	else
	{
		$conditions = "AND (`objPending` = '0' OR `objPendingUser` = '1') AND `objDeleted` = '0' ".
			"AND `objCreator` = '".$_auth[ "useid" ]."'";
	}
}

$result = sql_query( "SELECT * FROM $_objects,$_objExtData ".
	"WHERE `objid` = '".$objid."' AND `objEid` = '".$objid."' $conditions LIMIT 1" );

if( !mysql_num_rows( $result ))
{
	 // If the artwork does not exist, return the Not Found page.

	include(INCLUDES."p_notfound.php");
	return;
}
else
{
	$objData = mysql_fetch_assoc( $result );
}

$part = isset( $_GET[ "edit" ]) ? $_GET[ "edit" ] : "";

if( $part != "keywords" )
{
	?>
	<div class="header">
		<div class="header_title">
			<?= _EDIT_SUBMISSION ?>
			<div class="subheader"><?= formatText( $objData[ "objTitle" ]) ?></div>
		</div>
		<div class="clear">&nbsp;</div>
	</div>
	<div class="container">
	<?
}

switch( $part )
{
	case "title": // edit title/comment
	{
		if( $isExtras )
		{
			return;
		}

		if( isset( $_POST[ "cancel" ]))
		{
			redirect( url( "view/".$objid ));
		}

		$title = isset( $_POST[ "title" ]) ? $_POST[ "title" ] : $objData[ "objTitle" ];
		$comment = isset( $_POST[ "comment" ]) ? $_POST[ "comment" ] : $objData[ "objComment" ];
		$forClub = isset( $_POST[ "forClub" ]) ? intval( $_POST[ "forClub" ]) : $objData[ "objForClub" ];
		$forClub2 = isset( $_POST[ "forClub2" ]) ? intval( $_POST[ "forClub2" ]) : $objData[ "objForClub2" ];
		$forClub3 = isset( $_POST[ "forClub3" ]) ? intval( $_POST[ "forClub3" ]) : $objData[ "objForClub3" ];
		$folder = isset( $_POST[ "folder" ]) ? intval( $_POST[ "folder" ]) : $objData[ "objFolder" ];
		$collab = isset( $_POST[ "collab" ]) ? intval( $_POST[ "collab" ]) : $objData[ "objCollab" ];
		$gift = isset( $_POST[ "gift" ]) ? intval( $_POST[ "gift" ]) : $objData[ "objForUser" ];

		// Double-check if the user is the owner of specified folder.

		$result = sql_query( "SELECT COUNT(*) FROM `folders`".dbWhere( array(
			"folCreator" => $_auth[ "useid" ],
			"folid" => $folder )));

		if( mysql_result( $result, 0 ) == 0 )
			$folder = 0;

		// Double-check if the user is a member of this club.

		function checkClubMember( $creator, $forClub )
		{
			$result = sql_query( "SELECT `useCid` FROM `useClubs` ".
				"WHERE `useCclub` = '$forClub' AND `useCmember` = '".$creator."' ".
				"AND `useCpending` = '0' LIMIT 1" );

			if( mysql_num_rows( $result ) == 0 )
			{
				return( 0 ); // User is no longer a member of that club.
			}
			else
			{
				return( $forClub );
			}
		}

		$forClub = checkClubMember( $objData[ "objCreator" ], $forClub );
		$forClub2 = checkClubMember( $objData[ "objCreator" ], $forClub2 );
		$forClub3 = checkClubMember( $objData[ "objCreator" ], $forClub3 );

		if( isset( $_POST[ "submit" ]) && $title != "" && $comment != "" )
		{
			include_once( INCLUDES."submission.php" );

			// Modify title in the database.

			submitModifiedTitle( $objid, $title, $comment, $objData[ "objMature" ],
				$forClub, $folder, $collab, $gift, $forClub2, $forClub3 );

			// Redirect the user to view the submission.

			redirect( url( "view/".$objid ));
		}

		?>
		<form action="<?=url(".", array("edit" => "title"))?>" method="post">
			<?
			$_POST["title"] = $title;
			$_POST["comment"] = $comment;
			$_POST["forClub"] = $forClub;
			$_POST["forClub2"] = $forClub2;
			$_POST["forClub3"] = $forClub3;
			$_POST["folder"] = $folder;
			$_POST["collab"] = $collab;
			$_POST["gift"] = $gift;
			if(!$title) $requireTitle = true;
			if(!$comment) $requireComment = true;
			$clubMember = $objData["objCreator"];
			include(INCLUDES."mod_submit_page1.php");
			?>
			<input class="submit" name="submit" type="submit" value="<?=_SAVE_CHANGES ?>" />
			<input class="submit" name="cancel" type="submit" value="<?=_CANCEL ?>" />
		</form>
		<?

		break;
	}

	case "keywords": // edit keywords
	{
		if( $isExtras )
		{
			return;
		}

		if( isset( $_POST[ "cancel" ]))
		{
			redirect( url( "view/".$objid ));
		}

		if( isset( $_POST[ "submit" ]))
		{
			include_once( INCLUDES."keywording.php" );

			$requireKGroups = requireRootKeywords( $_POST[ "keywordList" ]);

			if( $requireKGroups == "" )
			{
				include_once( INCLUDES."submission.php" );
				submitKeywords( $objid, $_POST[ "keywordList" ]);

				// Redirect the user to view the submission.

				redirect( url( "view/".$objid ));
			}
		}

		?>
		<form action="<?= url( ".", array( "edit" => "keywords" ))?>" method="post">
		<div class="header">
			<div class="header_title">
				<?= _EDIT_SUBMISSION ?>
				<div class="subheader">
					<?= formatText( $objData[ "objTitle" ]) ?>
				</div>
			</div>
			<?

			if( isset( $_POST[ "keywordList" ]))
			{
				$defaultKeywords = $_POST[ "keywordList" ];
			}
			else
			{
				$sql = "SELECT * FROM `objKeywords`".dbWhere( array(
					"objKobject" => $objid ));

				$result = sql_query( $sql );

				$idList = array();

				while( $rowData = mysql_fetch_assoc( $result ))
				{
					$idList[] = $rowData[ "objKkeyword" ];
				}

				$defaultKeywords = implode( " ", $idList );
			}

			include( INCLUDES."mod_keywords.php" );

			if( isset( $requireKGroups ) && $requireKGroups )
				notice( $requireKGroups );

			?>
			<div class="padded notsowide">
				<?= sprintf( _SUBMIT_KEYWORDS_EXPLAIN, $_config[ "keywordExplainURI" ]) ?>
			</div>
			<div>
				<input class="submit" name="cancel" type="submit" value="<?= _CANCEL ?>" />
				<input class="submit" name="submit" type="submit" value="<?= _SAVE_CHANGES ?>" />
			</div>
		</div>
		</form>
		<?

		break;
	}

	case "file": // edit file/thumb
	{
		if( $isExtras )
		{
			return;
		}

		if( !$extraRights && $_auth[ "useid" ] != $objData[ "objCreator" ])
		{
			notice( _ACCESS_DENIED );
			echo '</div>';
			return;
		}

		/* Facsimilnym: Removed max numedits 16 Apr 12
		if( $objData[ "objNumEdits" ] >= $_config[ "maxObjectEdits" ])
		{
			notice( _EDIT_EXCEEDED );
			echo '</div>';
			return;
		}
		 */

		if( isset( $_POST[ "cancel" ]))
		{
			redirect( url( "view/".$objid ));
		}

		$oekakiExtras = "";
		$oekakiSign = "/oekaki/";

		if( substr( $objData[ "objFilename" ], 0, strlen( $oekakiSign )) == $oekakiSign )
		{
			$oekakiSession = $_auth[ "useid" ];

			$baseFilename = preg_replace( '/\..+$/', "",
				findNewestFile( "files/oekakitemp/".$oekakiSession."-*", "" ));

			if( file_exists( $baseFilename.".png" ))
			{
				$_FILES[ "submission" ][ "error" ] = UPLOAD_ERR_OK;
				$_FILES[ "submission" ][ "tmp_name" ] = $baseFilename.".png";
				$_FILES[ "submission" ][ "name" ] = $oekakiSign.intval( $_GET[ "oekaki" ]);
				$_FILES[ "submission" ][ "type" ] = "image/png";
				$_FILES[ "submission" ][ "simple_move" ] = true;

				if( file_exists( $baseFilename.".pch" ))
				{
					$_FILES[ "submission" ][ "also_move" ] = $baseFilename.".pch";
					$_FILES[ "submission" ][ "also_move_ext" ] = ".pch";

					$oekakiExtras = ", `objAniType` = 'pch'";
				}
				elseif( file_exists( $baseFilename.".oeb" ))
				{
					$_FILES[ "submission" ][ "also_move" ] = $baseFilename.".oeb";
					$_FILES[ "submission" ][ "also_move_ext" ] = ".oeb";

					$oekakiExtras = ", `objAniType` = 'oeb'";
				}
				else
				{
					$oekakiExtras = ", `objAniType` = 'no'";
				}

				$_POST[ "submit" ] = true;
			}
			else
			{
				redirect( url( "oekaki/".$objid ));
			}
		}

		include_once( INCLUDES."submission.php" );
		include_once( INCLUDES."files.php" ); // file uploading functions

		if( isset( $_POST[ "textfile" ]) && $_POST[ "textfile" ] != "" )
		{
			$tempfilename = "files/texttemp/".$_auth[ "useid" ].".txt";

			forceFolders( dirname( $tempfilename ));

			$fp = fopen( $tempfilename, "wb" );
			fputs( $fp, $_POST[ "textfile" ]);
			fclose( $fp );

			$_FILES[ "submission" ][ "error" ] = UPLOAD_ERR_OK;
			$_FILES[ "submission" ][ "tmp_name" ] = $tempfilename;
			$_FILES[ "submission" ][ "name" ] = "textfile.txt";
			$_FILES[ "submission" ][ "type" ] = "text/plain";
			$_FILES[ "submission" ][ "simple_move" ] = true;

			if( checkUploadedFile("thumb") == _UPL_NO_FILE )
			{
				$thumbfilename = applyIdToPath( "files/thumbs/", $objData[ "objid" ])."-".
					preg_replace( '/[^0-9]/', "", $objData[ "objLastEdit" ]).".jpg";

				$_FILES[ "thumb" ][ "error" ] = UPLOAD_ERR_OK;
				$_FILES[ "thumb" ][ "tmp_name" ] = $thumbfilename;
				$_FILES[ "thumb" ][ "name" ] = "thumbnail.jpg";
				$_FILES[ "thumb" ][ "type" ] = "image/jpeg";
				$_FILES[ "thumb" ][ "simple_move" ] = true;
			}
		}

		if( isset( $_POST[ "submit" ]))
		{
			$uploadError = checkUploadedFile( "submission" );
			$uploadErrorThumb = checkUploadedFile( "thumb" );

			if(( $uploadError == "" || $uploadError == _UPL_NO_FILE ) &&
				( $uploadErrorThumb == "" || $uploadErrorThumb == _UPL_NO_FILE ))
			{
				if( submitImage( $objid,
					$uploadError == _UPL_NO_FILE ? "" : "submission",
					$uploadErrorThumb == _UPL_NO_FILE ? "" : "thumb",
					$uploadErrorThumb, $imageChanged ))
				{
					if( $imageChanged )
					{
						if( isset($_POST[ "sendNotification" ]) && $_POST[ "sendNotification" ])
						{
							function notifyClubWatchers( $objCreator, $objid, $clubField )
							{
								global $_objects;

								// If this submission is going into some club, notify the club's watchers.

								$result = sql_query( "SELECT `objForClub` FROM $_objects WHERE `objid` = '$objid' LIMIT 1" );
		
								$forClub = mysql_result( $result, 0 );

								// Notify the watchers.

								addArtUpdateToWatchers( $objCreator, $objid, $forClub );
							}

							notifyClubWatchers( $objData[ "objCreator" ], $objid, "objForClub" );
							notifyClubWatchers( $objData[ "objCreator" ], $objid, "objForClub2" );
							notifyClubWatchers( $objData[ "objCreator" ], $objid, "objForClub3" );

							removeDupeArtUpdates( $objid );
						}

						// Increase object's number of edits and also set abuse report
						// to "enabled" because the submission content has changed.

						$result = sql_query( "SELECT `objNumEdits` FROM $_objExtData ".
							"WHERE `objEid` = '$objid' LIMIT 1" );

						$numEdits = mysql_result( $result, 0 ) + 1;

						sql_query( "UPDATE $_objExtData SET `objNoAbuse` = '0', ".
							"`objNumEdits` = '$numEdits'$oekakiExtras ".
							"WHERE `objEid` = '$objid' LIMIT 1" );
					}

					// Redirect the user to view the submission.

					redirect( url( "view/".$objid ));
				}
			}

			if( !$uploadError && $uploadErrorThumb )
			{
				$uploadError = _SUBMIT_THUMBNAIL_ERROR;
			}
		}

		?>
		<form action="<?= url( ".", array( "edit" => "file" )) ?>" enctype="multipart/form-data" method="post">
			<?

			if( $objData[ "objExtension" ] != "txt" )
			{
				?>
				<div class="sep caption"><?= _FILE ?>:</div>
				<div><input accept="text/plain, image/gif, image/jpeg, image/png, application/x-shockwave-flash"
					onchange="make_visible('preview_container'); show_preview_image('previewx', 'preview_message', this.value)"
					name="submission" size="79" type="file" /></div>
				<?

				if( isset( $uploadError ))
				{
					notice( $uploadError );
				}

				?>
				<table><tr><td>
				<div style="display: none" id="preview_container">
					<div class="sep caption"><?= _PREVIEW ?>:</div>
					<div class="container2 a_center">
						<img alt="preview" style="display: none" id="previewx" src="" />
						<div id="preview_message"><?= _SUBMIT_SELECT_FILE ?></div>
					</div>
				</div>
				</td></tr></table>
				<?
			}

			?>
			<div class="sep caption"><?= _THUMBNAIL ?>:</div>
			<div><input accept="image/gif, image/jpeg, image/png"
				onchange="make_visible('preview_container_thumb'); show_preview_image('previewx_thumb', 'preview_message_thumb', this.value)"
				name="thumb" size="79" type="file" /></div>
			<?

			if( isset( $uploadErrorThumb ))
			{
				notice( $uploadErrorThumb );
			}

			?>
			<table><tr><td>
			<div style="display: none" id="preview_container_thumb">
				<div class="sep caption"><?= _THUMB_PREVIEW ?>:</div>
				<div class="container2 a_center">
					<img alt="preview" style="display: none" id="previewx_thumb" src="" />
					<div id="preview_message_thumb"><?= _SUBMIT_SELECT_FILE ?></div>
				</div>
			</div>
			</td></tr></table>
			<?

			if( $objData[ "objExtension" ] == "txt" )
			{
			?>
					<div class="caption"><?= _TEXT ?>:</div>
				<?
				

				iefixStart();

				if( isset( $_POST[ "textfile" ]))
				{
					$commentDefault = $_POST[ "textfile" ];
				}
				else
				{
					$filename = applyIdToPath( "files/data/", $objData[ "objid" ])."-".
						preg_replace( '/[^0-9]/', "", $objData[ "objLastEdit" ]).".".
						$objData[ "objExtension" ];

					if( file_exists( $filename ))
					{
						$commentDefault = trim( implode( "", file( $filename )));
					}
					else
					{
						$commentDefault = "";
					}
				}

				$commentName = "textfile";
				$commentRows = 16;
				$commentNoOptions = true;

				include( INCLUDES."mod_comment.php" );

				?>
				<div class="clear">&nbsp;</div>
				<?

				iefixEnd();
			}
			else
			{
				?>
				<div class="sep">
					<?= _SUBMIT_THUMB_NOT_REQUIRED ?>
				</div>
				<?
			}

			?>
			<div class="sep">
				<input class="submit" name="cancel" type="submit" value="<?=_CANCEL?>" />
				<input class="submit" name="submit" type="submit" value="<?=_SAVE_CHANGES?>" />
			</div>
		</form>
		<?

		break;
	}

	case "delete": // delete - request a confirmation
	{
		if( !$extraRights && $_auth[ "useid" ] != $objData[ "objCreator" ])
		{
			notice( _ACCESS_DENIED );
			echo '</div>';
			return;
		}

		?>
		<div class="largetext">
			<b><?= _WARNING ?>!</b>
		</div>
		<div class="sep"><b><a href="<?=url(".", array("edit" => "delete2"))?>">
			<?=_EDIT_CONFIRM_DELETE ?></a></b>
		</div>
		<div class="sep">
			<? printf( _EDIT_CANCEL_DELETE, "<a href=\"".url( "view/".$objid )."\">", "</a>" ); ?>
		</div>
		<?

		break;
	}

	case "delete2": // Delete - perform deletion.
	{
		if( !$extraRights && $_auth[ "useid" ] != $objData[ "objCreator" ])
		{
			notice( _ACCESS_DENIED );
			echo '</div>';
			return;
		}

		sql_query( "UPDATE $_objects SET `objDeleted` = '1' WHERE `objid` = '$objid'" );

		sql_query( "UPDATE $_objExtData SET `objDeletedBy` = '".$_auth[ "useid" ]."', `objDeleteDate` = NOW() WHERE `objEid` = '$objid'" );

		include_once( INCLUDES."submission.php" );
		updateObjCount( $objData[ "objCreator" ]);
		echo "<div>"._EDIT_DELETED."</div>";
		break;
	}

	default: // Incorrect action specified, redirect back to the submission.
	{
		redirect( url( "view/".$objid ));
	}
}

if( $part != "keywords" )
{
	?>
	</div>
	<?
}

?>
