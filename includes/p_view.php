<?php


function traverseKeyword( $keyid, $isSearchable )
{
	$result = sql_query( "SELECT `keyWord`,`keySubcat` FROM `keywords` ".
		"WHERE `keyid` = '".intval( $keyid )."' LIMIT 1" );

	if( mysql_num_rows( $result ) == 0 )
	{
		return( "???" );
	}

	$kwName = mysql_result( $result, 0, 0 );
	$kwSubcat = mysql_result( $result, 0, 1 );

	$kwName = trim(
		preg_replace( '/^.*\|/', "",
		preg_replace( '/\@$/', "", $kwName )));

	if( $isSearchable )
	{
		$kwName = '<a href="'.
			url( "search", array( "keywordList" => $keyid )).'">'.
			$kwName.'</a>';
	}

	if( $kwSubcat != 0 ) // keySubcat?
	{
		$kwName = traverseKeyword( $kwSubcat, false )." &ndash; ".$kwName;
	}

	return( $kwName );
}
	
function showKeywordList( $objid )
{
	$result = sql_query( "SELECT `objKkeyword` FROM `objKeywords` ".
		"WHERE `objKobject` = '".$objid."'" );
	
	$idList = array();
	
	while( $rowData = mysql_fetch_row( $result ))
	{
		$idList[] = $rowData[ 0 ];
	}
	
	$stringList = array();
	
	foreach( $idList as $keyid )
	{
		$stringList[ $keyid ] = "&bull; ".traverseKeyword( $keyid, true )."<br />";
	}
	
	sort( $stringList );
	
	echo implode( "", $stringList );
	
	if( $stringList == "" )
	{
		echo _NONE;
	}
}


$viewOK = TRUE;
$active = 1;
include( INCLUDES."mod_artmenu.php" );
$fileText = false;

if( !$viewOK )
{
	return;
}

?>
<div class="container">
	<?php

	// Do we need to show a preview (normal quality) image or a high
	// quality image?

	$isPreview = false;

	if( isset( $_auth[ "useObjPreview" ]) && $_auth[ "useObjPreview" ])
		$isPreview = true;

	if( isset( $_GET[ "quality" ]))
	{
		if( $_GET[ "quality" ] == "normal" )
			$isPreview = true;

		if( $_GET[ "quality" ] == "high" )
			$isPreview = false;
	}

	if( $isExtras && $objData[ "objPreviewURL" ] == $objData[ "objImageURL" ])
	{
		$isPreview = false;
	}

	if( $objData[ "objPreviewWidth" ] == 0 ||
		$objData[ "objPreviewHeight" ] == 0 )
	{
		$isPreview = false;
	}

	// The submission's file is stored in /files/data/. Its filename consists
	// of the submission's id, revision date and the extension which defines
	// the way the submission should be displayed to the viewer. Also for file
	// system optimisation there are two numeric subfolders after /files/data/
	// to make sure every folder does not contain more than 10,000 files.

	$filename = $isExtras ? "" :
		applyIdToPath( "files/data/", $objData[ "objid" ])."-".
		preg_replace( '/[^0-9]/', "", $objData[ "objLastEdit" ]).".".
		$objData[ "objExtension" ];

	// If mod_rewrite is enabled, we can give the file a nice name
	// containing the artist's name and the submission's title
	// (see /files/data/.htaccess), instead of just a strange number.

	$fn2main = $useData[ "useUsername" ];

	if( $objData[ "objCollab" ] > 0 && $objData[ "objCollabConfirmed" ] > 0)
	{
		$usernameData = getUserData( $objData[ "objCollab" ]);

		$fn2main .= " & ".$usernameData[ "useUsername" ];
	}

	$fn2main .= " - ".trim( substr( $objData[ "objTitle" ], 0, 40 ));

	$filename2 = $isExtras ? "" :
		( $_mod_rewrite ? htmlspecialchars( "/".
		preg_replace( '/[^\w\s\'\-\!\(\)\[\]\&]/', "", $fn2main ).
		".".$objData[ "objExtension" ]) : "" );

	// Gather submission's parameters.

	$src = $isExtras ? $objData[ "objImageURL" ] : urlf().$filename.$filename2;
	$src_orig = $src; // will be used when the user clicks on the image
	$title = formatText( $objData[ "objTitle" ]);
	$width = $objData[ "objImageWidth" ];
	$height = $objData[ "objImageHeight" ];
	$containsAnimation = $isExtras ? false : $objData[ "objAniType" ] != "no";

	if( $isPreview )
	{
		if( $isExtras )
		{
			$src = $objData[ "objPreviewURL" ];
		}
		else
		{
			$filename = applyIdToPath( "files/preview/", $objData[ "objid" ])."-".
				preg_replace( '/[^0-9]/', "", $objData[ "objLastEdit" ]).".jpg";

			$src = urlf().$filename.$filename2;
		}

		$width = $objData[ "objPreviewWidth" ];
		$height = $objData[ "objPreviewHeight" ];
		$objData[ "objExtension" ] = "jpg";
	}

	if( $isExtras )
	{
		$objData[ "objExtension" ] = substr( $src, strlen( $src ) - 3 );
	}

	if( $isPreview && isLoggedIn() ) // High quality images only for logged in users.
	{
		// Supermoderator or admin can Reset edits.

		$urlHighQuality = url( "view/".( $isExtras ? "e" : "" ).$objid, array( "quality" => "high" ));

		$script =
			"var el = get_by_id( 'idPreviewImage' ); ".
			"if( !el ) return false; ".
			"el.style.width = '".$objData[ "objImageWidth" ]."px'; ".
			"el.style.height = '".$objData[ "objImageHeight" ]."px'; ".
			"el.src = '".str_replace( "'", "\'", $src_orig )."'; ".
			"make_invisible( 'idViewInHQButton' ); ".
			"return false;";

		?>
		<div class="mar_right mar_bottom button smalltext" id="idViewInHQButton"
			onclick="<?= $script ?>">
			<?= getIMG( url()."images/emoticons/submission.png" ) ?>
			<?= _HIGH_QUALITY ?>:
			<?= $objData[ "objImageWidth" ] ?>&times;<?= $objData[ "objImageHeight" ] ?>,
			<?= round( $objData[ "objImageSize" ] / 1024 ) ?> KB
		</div>
		<div class="clear" style="margin-bottom: 5px">&nbsp;</div>
		<?
	}

	// We use the <table> tag here again to force the center alignment for the
	// div of a dynamic width.

	?>
	<div align="center">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td>
			<div class="a_center container2">
			<?

			// If config.fullviewRegOnly is turned on, only registered users
			// are allowed to view submissions.

			if( !$_config[ "fullviewRegOnly" ] || isLoggedIn() )
			{

				// Display the submission depending on its file's extension.

				switch( $objData[ "objExtension" ])
				{
					case "gif":
					case "jpg":
					case "mng":
					case "png":

						// Display the image. Use getIMG () to enable PNG transparency
						// support.

						echo getIMG( $src, 'alt="'.strip_tags( $title ).
							'" id="idPreviewImage" title="'.strip_tags( $title ).'" '.
							( isLoggedIn() ? // High quality images only for logged in users.
								'onclick="popup(\''.addslashes( $src_orig ).'\',\'image\','.
								( $objData[ "objImageWidth" ] + 32 ).','.( $objData[ "objImageHeight" ] + 32 ).')" '
								: ""
							).
							'style="'.( isLoggedIn() ? 'cursor: pointer; ' : "" ).
							'width: '.$width.'px; height: '.$height.'px;"' );

						break;

					case "swf":

						// Display the Flash animation. This is not XHTML compliant :(

						?>
						<object type="application/x-shockwave-flash" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
							codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"
							width="<?= $width ?>" height="<?= $height ?>">
							<param name="movie" value="<?= $src ?>" />
							<param name="quality" value="high" />
							<param name="wmode" value="transparent" />
							<embed src="<?= $src ?>" quality="high" wmode="transparent"
								width="<?= $width ?>" height="<?= $height ?>"
								name="submission" type="application/x-shockwave-flash"
								pluginspage="http://www.macromedia.com/go/getflashplayer">
							</embed>
						</object>
						<?

						break;
					case "mp4":
						// Display the Flash animation. This is not XHTML compliant :(
						?>
						<video width="700px" controls poster="/images/clickPlay.png" allowfullscreen>
							<source src="<?= $src ?>" type="video/mp4">
							Your browser doesn't support the Video tag. Please update to a modern browser.
						</video>
						<?
						break;
					case "wmv":
						// Display the Flash animation. This is not XHTML compliant :(
						?>
						<video width="700px" controls poster="/images/clickPlay.png" allowfullscreen>
							<source src="<?= $src ?>" type="video/wmv">
							Your browser doesn't support the Video tag. Please update to a modern browser.
						</video>
						<?
						break;

					case "html":
					case "txt":

						// Display a text submission.

						if( file_exists( $filename ))
							$text = trim( implode( "", file( $filename )));
						else
							$text = "";

						if( $objData[ "objExtension" ] == "html" )
						{
							$text = str_replace( "<b>", "["._BBCODE_B."]", $text );
							$text = str_replace( "</b>", "[/"._BBCODE_B."]", $text );
							$text = str_replace( "<i>", "["._BBCODE_I."]", $text );
							$text = str_replace( "</i>", "[/"._BBCODE_I."]", $text );
							$text = str_replace( "<u>", "["._BBCODE_U."]", $text );
							$text = str_replace( "</u>", "[/"._BBCODE_U."]", $text );
							$text = str_replace( "<sub>", "["._BBCODE_SUB."]", $text );
							$text = str_replace( "</sub>", "[/"._BBCODE_SUB."]", $text );
							$text = str_replace( "<sup>", "["._BBCODE_SUP."]", $text );
							$text = str_replace( "</sup>", "[/"._BBCODE_SUP."]", $text );

							$text = preg_replace( '/\<img.*?src\=\"(.*?)\".*?\>/',
								'['._BBCODE_URL.'=\\1]'._ALT_IMAGE.'[/'._BBCODE_URL.']', $text );
						}
						$fileText = true;

						?>
						<script src="<?= urlf() ?>scripts/jquery-2-1-4.min.js" type="text/javascript"></script>
						<div class="a_left notsowide_fix" style="overflow: auto; height: 500px" id="submission-text">
							<?= formatText( $text ) ?></div>
						<div id="biggerView" style="display: none;">
							<div>
								<h3><a href="javascript:void(0)" onclick="document.getElementById('biggerView').style.display='none';document.getElementById('fadeCover').style.display='none'" id="closeButton">Close View</a>
								&nbsp; &nbsp;
								<a href="javascript:void(0)" id="incfont">A+</a>
								&nbsp; &nbsp;
								<a href="javascript:void(0)" id="decfont">A-</a>
								&nbsp; &nbsp;
								<a href="javascript:void(0)" id="blackTWhiteB">T</a>
								&nbsp; &nbsp;
								<a href="javascript:void(0)" id="whiteTBlackB">T</a>
								</h3>
							</div>
							<hr>
							<div class="a_left" id="expandedTextView">
							</div>
							<hr>
						</div>
						<div id="fadeCover" class="fadeCover" style="display:none;" onclick="document.getElementById('biggerView').style.display='none';document.getElementById('fadeCover').style.display='none'"></div>
						<script type="text/javascript">
							$(document).ready(function() {
								$('#incfont').click(function(){    
									curSize= parseInt($('#expandedTextView').css('font-size')) + 2;
									if(curSize<=46)
										$('#expandedTextView').css('font-size', curSize);
								}); 
								$('#decfont').click(function(){    
									curSize= parseInt($('#expandedTextView').css('font-size')) - 2;
									if(curSize>=12)
										$('#expandedTextView').css('font-size', curSize);
								});
								$("#blackTWhiteB").click(function(){
									$('#biggerView').css("background-color","white");
									$('#expandedTextView').css("color","#333");
									$('#closeButton').css("color","#333");
									$('#incfont').css("color","#333");
									$('#decfont').css("color","#333");
								});
								$("#whiteTBlackB").click(function(){
									$('#biggerView').css("background-color","#333");
									$('#expandedTextView').css("color","whitesmoke");
									$('#closeButton').css("color","whitesmoke");
									$('#incfont').css("color","whitesmoke");
									$('#decfont').css("color","whitesmoke");
								});					var divs = document.getElementById('submission-text').innerHTML;
								document.getElementById('expandedTextView').innerHTML= divs;
							});
						</script>
						<?

						break;
				}
			}
			else
			{
				?>
				<div class="a_left notsowide_fix"><?= _FULLVIEW_REG_ONLY ?></div>
				<?
			}

			?>
			</div>
		</td>
		</tr>
		</table>
		<?

		function showPrevNext( $cl, $cluVar ) // 0 - By Artist, 1 - By Club
		{
			global $_objects, $isExtras, $objData, $useData, $fileText;

			if( $cl == 1 && $objData[ $cluVar ] == 0 )
			{
				return;
			}

			if( $cl == 1 && $isExtras )
			{
				return;
			}

			$prevId = 0;
			$nextId = 0;

			if( $cl == 1 )
			{
				// Navigate inside this club/project.

				$select = "SELECT `objid`,`objTitle` FROM $_objects,`clubObjects`";

				$where = "`cloObject` = `objid` AND `cloClub` = '".$objData[ $cluVar ]."'";
				
				$browseURL = url("clubgallery/".$objData[ $cluVar ]);
			}
			else
			{
				// Navigate inside the artist's gallery (this also includes
				// submissions posted to clubs).

				$select = "SELECT `objid`,`objTitle` FROM $_objects";
				$where = "`objCreator` = '".$objData[ "objCreator" ]."'";

				$browseURL = url(( $isExtras ? "extras/" : "gallery/" ).
					strtolower( $useData[ "useUsername" ]));
			}

			// Make sure deleted and pending submissions don't display.

			$where = "($where) AND `objDeleted` = '0' AND `objPending` = '0'";

			// Make sure mature submissions don't display if the user has chosen
			// not to view them.

			applyObjFilters( $where );

			// To find the next submission, sort all submissions that were
			// posted after the current submission by submission dates
			// in the ascending order and then get the very first one.

			$result = sql_query( "$select WHERE $where ".
				"AND `objSubmitDate` > '".$objData[ "objSubmitDate" ]."' ".
				"ORDER BY `objSubmitDate` LIMIT 1" );

			if( $rowData = mysql_fetch_row( $result ))
			{
				$nextId = $rowData[ 0 ];
			}

			// To find the previous submission, sort all submissions that were
			// posted before the current submission by submission dates
			// in the descending order and then get the very first one.

			$result = sql_query( "$select WHERE $where ".
				"AND `objSubmitDate` < '".$objData[ "objSubmitDate" ]."' ".
				"ORDER BY `objSubmitDate` DESC LIMIT 1");

			if( $rowData = mysql_fetch_row( $result ))
			{
				$prevId = $rowData[ 0 ];
			}

			// Display navigation buttons (Prev, Browse, Next).

			?>
			<div class="mar_top mar_bottom smalltext">
				<?

				if( $prevId > 0 )
				{
					echo getObjectThumb( $prevId, 30, $isExtras ).
						getIMG( url()."images/emoticons/nav-prev.png" ).
						" "._PAGE_PREVIOUS;
				}

				?>
				&nbsp; &nbsp;
				<a class="disable_wrapping" href="<?= $browseURL ?>">
					<?= $cl == 0 ? ( $isExtras ? _EXTRA_BROWSE : _BROWSE_GALLERY ) : _BROWSE_CLUB ?></a>
				
				
				
				<?
					if($fileText)
					{
				?>
						&nbsp; &nbsp;
						<!-- Lighbox effect for text boxes -->
						<a href="javascript:void(0)" onclick="document.getElementById('biggerView').style.display='block';document.getElementById('fadeCover').style.display='block'">Display Bigger</a>
				<?	} ?>
				&nbsp; &nbsp;
				<?

				if( $nextId > 0 )
				{
					echo _PAGE_NEXT." ".
						getIMG( url()."images/emoticons/nav-next.png" ).
						getObjectThumb( $nextId, 30, $isExtras );
				}

				?>
			</div>
			<?
		}

		showPrevNext( 0, "objForClub" ); // Prev/Next by Artist

		?>
	</div> <?/* align="center" */?>

	<table align="center">
	<tr>
	<td class="notsowide">
		<?

		$favAddedId = generateId();

		if( $useData[ "useid" ] != $_auth[ "useid" ] || $containsAnimation ||
			$objData[ "objExtension" ] == "swf" )
		{
			?>
			<div class="container2 a_center"
				style="padding: 4px; margin: <?= $_isIE ? "8px 100px 16px 100px" : "8px 20% 16px 20%" ?>">
				<?

				$first = true;

				if( $containsAnimation )
				{
					?>
					<acronym title="<?= _ANIMATION_EXPLAIN ?>">
						<a href="<?= url( "oekaki", array( "viewAnimation" => $objid )) ?>">
							<?= _ANIMATION ?></a>
					</acronym>
					<?

					$first = false;
				}

				if( isLoggedIn() && !$isExtras && $useData[ "useid" ] != $_auth[ "useid" ])
				{
					if( !$first )
						echo " &middot; ";

					$result = sql_query( "SELECT `favObj` FROM `favourites` ".
						"WHERE `favObj` = '$objid' AND `favCreator` = '".$_auth["useid"]."' LIMIT 1" );

					$faved = mysql_num_rows( $result ) > 0;

					$favId = generateId();
					$unfavId = generateId();

					$favScript =
						"add_operation( 'f$objid' ); ".
						"var el = get_by_id( '$favId' ); ".
						"if( el ) el.style.display = 'none'; ".
						"el = get_by_id( '$unfavId' ); ".
						"if( el ) el.style.display = 'inline'; ".
						"el = get_by_id( '$favAddedId' ); ".
						"if( el ) el.style.display = 'block'; ".
						"return false;";

					$unfavScript =
						"add_operation( 'fu$objid' ); ".
						"var el = get_by_id( '$unfavId' ); ".
						"if( el ) el.style.display = 'none'; ".
						"el = get_by_id( '$favId' ); ".
						"if( el ) el.style.display = 'inline'; ".
						"el = get_by_id( '$favAddedId' ); ".
						"if( el ) el.style.display = 'none'; ".
						"return false;";

					?>
					<span id="<?= $unfavId ?>" <?= $faved ? "" : 'style="display: none"' ?>>
						<acronym title="<?= _FAV_REMOVE ?>">
							<a onclick="<?= $unfavScript ?>" href="<?= url( "op/fu$objid" ) ?>">
							<?= getIMG( url()."images/emoticons/fav0.png" ) ?>
							<?= _FAV ?></a>
						</acronym>
					</span>
					<span id="<?= $favId ?>" <?= $faved ? 'style="display: none"' : "" ?>>
						<acronym title="<?= _FAV_ADD ?>"
							<a onclick="<?= $favScript ?>" href="<?= url( "op/f$objid" ) ?>">
							<?= getIMG( url()."images/emoticons/fav1.png" ) ?>
							<?= _FAV ?></a>
						</acronym>
					</span>
					<?

					$first = false;
				}

				if( $objData[ "objExtension" ] == "swf" )
				{
					if( !$first )
						echo " &middot; ";

					?>
					<a target="_blank" href="<?= $src ?>">
						<?= getIMG( url()."images/emoticons/submission.png" ) ?>
						<?= _FULL_WINDOW ?></a>
					<?

					$first = false;
				}

				if( isLoggedIn() && $useData[ "useid" ] != $_auth[ "useid" ])
				{
					if(	!$objData[ "objNoAbuse" ] && !$objData[ "objPending" ] &&
						$_config["maxOpenAbuse"] > 0 && !$objData[ "objDeleted" ])
					{
						if( !$first )
							echo " &middot; ";

						?>
						<acronym title="<?=_ABUSE_REPORT_EXPLAIN ?>">
							<a href="<?=url("report/".( $isExtras ? "e" : "" ).$objid)?>"
								onclick="return confirm('<?=_ARE_YOU_SURE ?>')">
							<?= getIMG( url()."images/emoticons/abuse.png" ) ?>
							<?=_ABUSE_REPORT ?></a>
						</acronym>
						<?
					}

					$first = false;
				}

				?>
				<div class="sep" id="<?= $favAddedId ?>" style="display: none">
					<? printf( _FAVOURITED, formatText( $objData[ "objTitle" ])); ?>
				</div>
			</div>
			<?
		}

		
		if($objData[ "objCollab" ] > 0 && !$objData[ "objCollabConfirmed"])
		{
			?>
			<div class="sep caption"><?= _COLLAB.": "._UNCONFIRMED?></div>
			<div class="container2"><?=_COLLAB_CONFIRM_WAIT?></div>
			<?
		}
		?>
		<div class="sep caption"><?= _ARTIST_COMMENT ?>:</div>
		<div style="margin-top: -8px" id="artist-comment">
			<?

			include_once( INCLUDES."comments.php" );

			showComment( array(
				"comid" => 0,
				"comCreator" => $objData[ "objCreator" ],
				"comComment" => $objData[ "objComment" ]), 0 );

			?>
		</div>
		<?

		function putClubData( $objData, $num )
		{
			$numStr = ( $num == 1 ? "" : $num );

			// Show which club it is submitted to (if any).

			$result = sql_query( "SELECT `cluIsProject` FROM `clubs` ".
				"WHERE `cluid` = '".intval( $objData[ "objForClub".$numStr ])."' LIMIT 1" );

			if( mysql_num_rows( $result ) > 0 )
			{
				$isProject = mysql_result( $result, 0 );
	    
				?>
				<div class="sep caption">
					<?= $isProject ? _PROJECT : _CLUB1 ?>:
				</div>
				<?
	    
				$clubQuery = "SELECT `cluid`,`cluName`,`cluDesc`,`cluIsProject` ".
					"FROM `clubs`,`useClubs` ".
					"WHERE `cluid` = `useCclub` ".
					"AND `cluid` = '".intval( $objData[ "objForClub".$numStr ])."' LIMIT 1";
	    
				include( INCLUDES."mod_clubs.php" );

				?>
				<div class="a_center">
					<?

					showPrevNext( 1, "objForClub".$numStr ); // Prev/Next by Club

					?>
				</div>
				<?
			}
		}

		putClubData( $objData, 1 );
		putClubData( $objData, 2 );
		putClubData( $objData, 3 );

		?>
		<div class="sep caption"><?= _KEYWORDS ?>:</div>
		<div class="container2" id="keywords">
			<?

			iefixStart();

			if( isLoggedIn() )
			{
				// If the submission is mature, display this information on the
				// content details panel.

				$objFilters = preg_split( '/[\s\,\;]/', $objData[ "objMature" ],
					64, PREG_SPLIT_NO_EMPTY );

				?>
				<div class="f_right mar_left mar_right mar_bottom">
					<?

					foreach( getEnabledFilters() as $filter )
					{
						if( in_array( $filter, $objFilters ))
						{
							echo "&bull; ".getFilterName( $filter )."<br />";
						}
					}

					?>
					<div class="sep a_right smalltext">
						<a href="<?= url( "site" ) ?>"><?= _SET_FILTERS ?>
							<?=getIMG(url()."images/emoticons/nav-next.png") ?></a>
					</div>
				</div>
				<?
			}

			if( !$isExtras )
			{
				showKeywordList( $objid );
			}
			else
			{
				echo _SUBMIT_TYPE_EXTRA;
			}

			?>
			<div class="clear">&nbsp;</div>
			<?

			iefixEnd();

			?>
		</div>
		<?

		$statsPublic = $useData[ "useid" ] != $_auth[ "useid" ] ?
			$useData[ "useStatsPublic" ] : true;

		if( isLoggedIn() &&
			$objData[ "objCollab" ] == $_auth[ "useid" ] &&
			$objData[ "objCollabConfirmed" ])
		{
			$statsPublic = true;
		}

		?>
		<div class="sep caption"><?= _INFORMATION ?>:</div>
		<div class="container2">
			<?

			if( ( !$_auth[ "useStatsHide" ] && $statsPublic ) || atLeastModerator())
			{
				?>
				<div>
					<?= _VIEWS ?>:
					<?= fuzzy_number( $objData[ "objViewed" ]) ?>
				</div>
				<?

				if( !$isExtras )
				{
					?>
					<div>
						<?= _FAVOURITES ?>:
						<?= fuzzy_number( $objData[ "objFavs" ]) ?> 
						(<a href="javascript:popup('<?= url( "favs/".$objData[ 'objid' ]."", array( "popup" => "yes" )) ?>','tos',900,600)">Who?</a>)
					</div>
					
					<?
				}
			}

			// Show submission date

			?>
			<div class="a_right smalltext">
				<?

				printf( _COMMENT_POSTED_ON, gmdate( $_auth[ "useDateFormat" ],
					applyTimezone( strtotime( $objData[ "objSubmitDate" ]))));

				?>
			</div>
		</div>
	</td>
	</tr>
	</table>

	<table align="center">
	<tr>
	<td class="notsowide">
		<a name="allComments"></a>
		<div id="comments_scrollpos" />
		<div class="header">
			<?= isset( $_GET[ "replied" ]) ? _COMMENTS : _LEAVE_COMMENT ?>
		</div>
		<?

		$offset = isset( $_GET[ "offset" ]) ? intval( $_GET[ "offset" ]) : 0;

		if( !isset( $_GET[ "showComments" ]) && $offset == 0 &&
			!isset( $_GET[ "replied" ]) && !isset( $_POST[ "refererURL" ]))
		{
			//Comments now load immediately by default. This isn't 2005 anymore.
			//
			include_once( INCLUDES."comments.php" );
			showAllComments( $objid, $isExtras ? "ext" : "obj", false, true, true, false, true );
		}
		else
		{
			include_once( INCLUDES."comments.php" );
			showAllComments( $objid, $isExtras ? "ext" : "obj", false, true, true, false, true );
		}
		
		?>
	</td>
	</tr>
	</table>
</div>
