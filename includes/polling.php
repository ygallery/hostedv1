<?

function showPollOptions($options, $alreadyVoted, $totalVoted, $polid)
{
	if($alreadyVoted)
	{
		$maxVoted = $totalVoted;

		foreach($options as $optData)
		{
			$maxVoted = $optData["polOVotes"];
			break;
		}

		foreach($options as $optData)
		{
			?>
			<table cellspacing="0" cellpadding="0" border="0"><tr>
				<td class="wide">
					<?=formatText($optData["polOOption"]) ?>
				</td>
				<td class="smalltext disable_wrapping" style="vertical-align: bottom">
					(<?=fuzzy_number($optData["polOVotes"]) ?>)
				</td>
			</tr></table>
			<?
			showPollBar($optData["polOVotes"], $maxVoted, $totalVoted);
		}
	}
	else
	{
		$options = array();

		$result2 = sql_query( "SELECT * FROM `pollOptions` ".
			"WHERE `polOPoll` = '".$polid."' ORDER BY `polOid` LIMIT 20" );

		while( $optData = mysql_fetch_assoc( $result2 ))
			$options[ $optData[ "polOid" ]] = $optData;

		foreach($options as $optData)
		{
			?>
			<div>
				<input class="checkbox" type="checkbox" id="<?=$chkid=generateId()?>" name="option<?=$optData["polOid"]?>" />
				<label for="<?=$chkid?>">
					<?=formatText($optData["polOOption"]) ?>
				</label>
			</div>
			<?
		}
	}
}

function showPollBar($votes, $maxVoted, $totalVoted)
{
	$barSize = ($maxVoted == 0) ? 0 : ($votes / $maxVoted) * 100;
	$percent = ($totalVoted == 0) ? 0 : ($votes / $totalVoted) * 100;
	?>
	<div class="pollbar" title="<?=round($percent)?> %">
		<div class="pollbarvoted" style="width: <?=$barSize?>%">&nbsp;</div>
	</div>
	<?
}

?>