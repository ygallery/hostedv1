<?

$_documentTitle = _BROWSE.": "._ALL_SUBMISSIONS;

?>
<div class="header">
	<div class="header_title">
		<?= _BROWSE ?>
		<div class="subheader"><?= _ALL_SUBMISSIONS ?></div>
	</div>	
	<?

	$active = 1;

	include( INCLUDES."mod_browsemenu.php" );

	?>
</div>

<div class="container">
	<?

	include_once( INCLUDES."gallery.php" );

	showThumbnails( array(
		"noMostFaved" => true,
		"quickSearch" => true,
		"countDisabled" => true ));

	?>
</div>
