<?

if( !isset( $isClub ))
{
	$isClub = false;
}

if( $isClub )
{
	$useid = intval( $_cmd[ 1 ]);

	// Only club owner (or supermoderator) can design club's custom theme.

	$where = array( "cluEid" => $useid );

	if( !atLeastSModerator() )
	{
		$where[ "cluCreator" ] = $_auth[ "useid" ];
	}

	$sql = "SELECT `cluEid` FROM `cluExtData`".dbWhere( $where );

	$result = sql_query( $sql );

	if( mysql_num_rows( $result ) == 0 )
	{
		include( INCLUDES."p_notfound.php" );
		return;
	}
}
else
{
	if( $_cmd[ 1 ] == "" && $_auth[ "useid" ] > 0 )
	{
		redirect( url( "themedesigner/".strtolower( $_auth[ "useUsername" ])));
	}

	$useName = $_cmd[ 1 ];

	// Current user can only design their own custom theme.

	if( strtolower( $useName ) != strtolower( $_auth[ "useUsername" ]))
	{
		include( INCLUDES."p_notfound.php" );
		return;
	}

	$useid = getUserId( $useName );

	if( $useid == 0 )
	{
		include( INCLUDES."p_notfound.php" );
		return;
	}
	/*
	Obsolete
	
	
	// The user must be an artist (have at least 1 submission)
	// to be able to design custom themes.

	if( !isArtist( $useid ))
	{
		include( INCLUDES."p_notfound.php" );
		return;
	}*/
}

$_documentTitle = "Theme Designer";

include_once( INCLUDES."customthemes.php" );

$data = loadCustomThemeData( $useid, $isClub );

$lastThemeFile = getLastCustomThemeFile( $useid, $isClub );

?>
<div class="header">
	<div class="header_title" style="height: 100px">
		Theme Designer
		<div class="subheader">A tool for designing your custom theme</div>
	</div>
	<?

	if( !$isClub )
	{
		$active = 6;

		include( INCLUDES."mod_setmenu.php" );
	}

	?>
</div>

<div class="container">
	<?

	$errors = array();

	if( isset( $_GET[ "remove" ]))
	{
		removeCustomTheme( $useid, $isClub );

		redirect( url( "." ));
	}

	if( isset( $_POST[ "submitTheme" ]))
	{
		while( true ) // Would break on errors and redirect on success.
		{
			updateCustomTheme( $useid, $isClub, $data, $errors );

			redirect( url( "." ));
		}
	}

	?>
	<div class="notsowide sep" style="background: #dddddd; color: #222222; border: 1px solid #000000; padding: 8px">
		<? iefixStart() ?>
		<div class="f_right mar_left mar_bottom">
			<a href="<?= url( ".", array( "remove" => "yes" )) ?>"
				onclick="return confirm('<?= _ARE_YOU_SURE ?>');">
			<?= getIMG( url()."themes/".$_config[ "defaultTheme" ]."/icon.png" ) ?>
			</a>
		</div>
		<b>Important!</b> If you cannot see the website normally after applying some changes,
		click on the image on the right to temporarily switch to our default theme.
		<br /><br />
		<b>Colours.</b> All fields marked with # are colours. The RRGGBB format should be used
		to define colours, e.g. FF0000 (or F00) is red, 0000FF (or 00F) is blue. If you leave
		the colour fields blank the system would try to generate them for you automatically.
		<br /><br />
		<b>Images.</b> Only JPEG images are supported for all backgrounds. Please optimise
		your images according to the file size limitations shown below (this is to ensure your
		pages would not load too slow). When editing your theme, you don't have to upload
		backgrounds that didn't change, you may just leave the fields blank.
		<br /><br />
		<div class="a_center">
			<?= getIMG( url()."images/figure1.jpg" ) ?>
			<br /><br />
			<b>Figure 1</b>. (1) Background (logo), (2) Title bar, (3) Page, (4) Panel.
			<br />
			A Panel can also appear inside the Page (e.g. comments, journals, image selectors,
			etc. are all displayed on Panels).
		</div>
		<div class="clear"><br /></div>
		<? iefixEnd() ?>
	</div>
	<br />

	<form action="<?php echo url( "." ) ?>" enctype="multipart/form-data" method="post">
		<div class="container2 mar_bottom">
			<div class="header">Background:</div>
			<div style="padding: 3px"><b>Logo:</b> file less than 200 KB</div>
			<?
				showFileInputWithPreview( "logo", "BGLayoutTiledH", "BGLayoutTiledV",
					$data[ "Tile" ] & 1, $data[ "Tile" ] & 16 );
			?>
			<div style="padding: 6px">
				<label for="id_cBody"><b>Text</b>: #</label>
				<input type="text" class="text" name="cBody" size="6" id="id_cBody"
					value="<?= isset( $data[ "c" ][ "cBody" ]) ? $data[ "c" ][ "cBody" ] : "" ?>" />
			</div>
		</div>

		<div class="container2 mar_bottom">
			<div class="header">Title bar:</div>
			<div style="padding: 3px"><b>Background:</b> file less than 200 KB</div>
			<?
				showFileInputWithPreview( "outer", "BGOuterTiledH", "BGOuterTiledV",
					$data[ "Tile" ] & 2, $data[ "Tile" ] & 32 );
			?>
			<div style="padding: 6px">
				<label for="id_cOuter"><b>Text</b>: #</label>
				<input type="text" class="text" name="cOuter" size="6" id="id_cOuter"
					value="<?= isset( $data[ "c" ][ "cOuter" ]) ? $data[ "c" ][ "cOuter" ] : "" ?>" />
				&nbsp; &nbsp;
				<label for="id_cHeaderLink"><b>Link</b>: #</label>
				<input type="text" class="text" name="cHeaderLink" size="6" id="id_cHeaderLink"
					value="<?= isset( $data[ "c" ][ "cHeaderLink" ]) ? $data[ "c" ][ "cHeaderLink" ] : "" ?>" />
				&nbsp; &nbsp;
				<label for="id_cHeaderLinkHover"><b>Hovered</b>: #</label>
				<input type="text" class="text" name="cHeaderLinkHover" size="6" id="id_cHeaderLinkHover"
					value="<?= isset( $data[ "c" ][ "cHeaderLinkHover" ]) ? $data[ "c" ][ "cHeaderLinkHover" ] : "" ?>" />
			</div>
			<div style="padding: 6px">
				<label for="id_sTitleMargin"><b>Title left margin</b>:</label>
				<input type="text" class="text" name="sTitleMargin" size="6" id="id_sTitleMargin"
					value="<?= isset( $data[ "c" ][ "sTitleMargin" ]) ? $data[ "c" ][ "sTitleMargin" ] : "" ?>" />
				<label>(in pixels)</label>
			</div>
		</div>

		<div class="container2 mar_bottom">
			<div class="header">Page:</div>
			<div style="padding: 3px"><b>Background:</b> file less than 200 KB</div>
			<?
				showFileInputWithPreview( "container", "BGContainerTiledH", "BGContainerTiledV",
					$data[ "Tile" ] & 4, $data[ "Tile" ] & 64 );
			?>
			<div style="padding: 6px">
				<label for="id_cContainer"><b>Text</b>: #</label>
				<input type="text" class="text" name="cContainer" size="6" id="id_cContainer"
					value="<?= isset( $data[ "c" ][ "cContainer" ]) ? $data[ "c" ][ "cContainer" ] : "" ?>" />
				&nbsp; &nbsp;
				<label for="id_cLink"><b>Link</b>: #</label>
				<input type="text" class="text" name="cLink" size="6" id="id_cLink"
					value="<?= isset( $data[ "c" ][ "cLink" ]) ? $data[ "c" ][ "cLink" ] : "" ?>" />
				&nbsp; &nbsp;
				<label for="id_cLinkHover"><b>Hovered</b>: #</label>
				<input type="text" class="text" name="cLinkHover" size="6" id="id_cLinkHover"
					value="<?= isset( $data[ "c" ][ "cLinkHover" ]) ? $data[ "c" ][ "cLinkHover" ] : "" ?>" />
			</div>
		</div>

		<div class="container2 mar_bottom">
			<div class="header">Panel:</div>
			<div style="padding: 3px"><b>Background:</b> file less than 200 KB</div>
			<?
				showFileInputWithPreview( "container2", "BGContainer2TiledH", "BGContainer2TiledV",
					$data[ "Tile" ] & 8, $data[ "Tile" ] & 128 );
			?>
			<div style="padding: 6px">
				<label for="id_cContainer2"><b>Text</b>: #</label>
				<input type="text" class="text" name="cContainer2" size="6" id="id_cContainer2"
					value="<?= isset( $data[ "c" ][ "cContainer2" ]) ? $data[ "c" ][ "cContainer2" ] : "" ?>" />
			</div>
			<div style="padding: 6px">
				<label for="id_cOutline"><b>Outline</b>: #</label>
				<input type="text" class="text" name="cOutline" size="6" id="id_cOutline"
					value="<?= isset( $data[ "c" ][ "cOutline" ]) ? $data[ "c" ][ "cOutline" ] : "" ?>" />
				&nbsp; &nbsp;
				<label for="id_sOutline1"><b>Style</b>:</label>
				<input type="radio" class="radio" name="sOutline" id="id_sOutline4" value="4"
					<?= isset( $data[ "c" ][ "sOutline" ]) && $data[ "c" ][ "sOutline" ] == 4 ? 'checked="checked"' : "" ?>
					/><label for="id_sOutline4">None</label>
				<input type="radio" class="radio" name="sOutline" id="id_sOutline1" value="1"
					<?= isset( $data[ "c" ][ "sOutline" ]) && $data[ "c" ][ "sOutline" ] == 1 ? 'checked="checked"' : "" ?>
					/><label for="id_sOutline1">Single</label>
				<input type="radio" class="radio" name="sOutline" id="id_sOutline2" value="2"
					<?= isset( $data[ "c" ][ "sOutline" ]) && $data[ "c" ][ "sOutline" ] == 2 ? 'checked="checked"' : "" ?>
					/><label for="id_sOutline2">Double</label>
				<input type="radio" class="radio" name="sOutline" id="id_sOutline3" value="3"
					<?= isset( $data[ "c" ][ "sOutline" ]) && $data[ "c" ][ "sOutline" ] == 3 ? 'checked="checked"' : "" ?>
					/><label for="id_sOutline3">Top/Bottom</label>
			</div>
		</div>

		<div class="container2 mar_bottom">
			<div class="header">Other colours:</div>
			<div style="padding: 6px">
				<label for="id_cTabActive"><b>Tab text, active</b>: #</label>
				<input type="text" class="text" name="cTabActive" size="6" id="id_cTabActive"
					value="<?= isset( $data[ "c" ][ "cTabActive" ]) ? $data[ "c" ][ "cTabActive" ] : "" ?>" />
				&nbsp; &nbsp;
				<label for="id_cTabInactive"><b>Inactive</b>: #</label>
				<input type="text" class="text" name="cTabInactive" size="6" id="id_cTabInactive"
					value="<?= isset( $data[ "c" ][ "cTabInactive" ]) ? $data[ "c" ][ "cTabInactive" ] : "" ?>" />
			</div>
			<div style="padding: 6px">
				<label for="id_cTabOutline"><b>Button/Tab outline</b>: #</label>
				<input type="text" class="text" name="cTabOutline" size="6" id="id_cTabOutline"
					value="<?= isset( $data[ "c" ][ "cTabOutline" ]) ? $data[ "c" ][ "cTabOutline" ] : "" ?>" />
			</div>
			<div style="padding: 6px">
				<label for="id_bButton"><b>Button background</b>: #</label>
				<input type="text" class="text" name="bButton" size="6" id="id_bButton"
					value="<?= isset( $data[ "c" ][ "bButton" ]) ? $data[ "c" ][ "bButton" ] : "" ?>" />
				&nbsp; &nbsp;
				<label for="id_bButtonHover"><b>Hovered</b>: #</label>
				<input type="text" class="text" name="bButtonHover" size="6" id="id_bButtonHover"
					value="<?= isset( $data[ "c" ][ "bButtonHover" ]) ? $data[ "c" ][ "bButtonHover" ] : "" ?>" />
			</div>
			<div style="padding: 6px">
				<label for="id_cInput"><b>User input</b>: #</label>
				<input type="text" class="text" name="cInput" size="6" id="id_cInput"
					value="<?= isset( $data[ "c" ][ "cInput" ]) ? $data[ "c" ][ "cInput" ] : "" ?>" />
				&nbsp;&nbsp;
				<label for="id_bInput"><b>Background</b>: #</label>
				<input type="text" class="text" name="bInput" size="6" id="id_bInput"
					value="<?= isset( $data[ "c" ][ "bInput" ]) ? $data[ "c" ][ "bInput" ] : "" ?>" />
				&nbsp;&nbsp;
				<label for="id_bInputFocus"><b>Focused BG</b>: #</label>
				<input type="text" class="text" name="bInputFocus" size="6" id="id_bInputFocus"
					value="<?= isset( $data[ "c" ][ "bInputFocus" ]) ? $data[ "c" ][ "bInputFocus" ] : "" ?>" />
			</div>
			<div style="padding: 6px">
				<label for="id_cMarkedText"><b>Marked keyword</b>: #</label>
				<input type="text" class="text" name="cMarkedText" size="6" id="id_cMarkedText"
					value="<?= isset( $data[ "c" ][ "cMarkedText" ]) ? $data[ "c" ][ "cMarkedText" ] : "" ?>" />
			</div>
			<div style="padding: 6px">
				<label for="id_cThumbBorder"><b>Thumbnail border</b>: #</label>
				<input type="text" class="text" name="cThumbBorder" size="6" id="id_cThumbBorder"
					value="<?= isset( $data[ "c" ][ "cThumbBorder" ]) ? $data[ "c" ][ "cThumbBorder" ] : "" ?>" />
				&nbsp; &nbsp;
				<label for="id_cThumbBorderMature"><b>Filtered</b>: #</label>
				<input type="text" class="text" name="cThumbBorderMature" size="6" id="id_cThumbBorderMature"
					value="<?= isset( $data[ "c" ][ "cThumbBorderMature" ]) ? $data[ "c" ][ "cThumbBorderMature" ] : "" ?>" />
			</div>
			<div style="padding: 6px">
				<label for="id_bUI2Layout"><b>New style layout</b>: #</label>
				<input type="text" class="text" name="bUI2Layout" size="6" id="id_bUI2Layout"
					value="<?= isset( $data[ "c" ][ "bUI2Layout" ]) ? $data[ "c" ][ "bUI2Layout" ] : "" ?>" />
			</div>
		</div>

		<div class="clear"><br /></div>
		<div class="sep">
			<button class="submit" name="submitTheme" type="submit">
				<?= getIMG( url()."images/emoticons/checked.png" ) ?>
				<?= _SAVE_CHANGES ?>
			</button>
			<?

			if( $lastThemeFile != "" )
			{
				?>
				&nbsp; &nbsp;
				<a href="<?= url( ".", array( "remove" => "yes" )) ?>"
					onclick="return confirm('<?= _ARE_YOU_SURE ?>');">
					Remove custom theme</a> (use Save changes to restore your theme later)
				<?
			}

			?>
		</div>

	</form>
</div>
<?

// ---------------------------------------------------------------------------

function showFileInputWithPreview( $name, $tiledHName, $tiledVName,
	$tiledHChecked, $tiledVChecked )
{
	global $errors;

	$previewContainerId = generateId();
	$previewImageId = generateId();
	$previewMessageId = generateId();
	$tileHId = generateId();
	$tileVId = generateId();

	?>
	<div>
		<input accept="image/jpeg"
		onchange="make_visible('<?= $previewContainerId ?>'); show_preview_image('<?= $previewImageId ?>', '<?= $previewMessageId ?>', this.value)"
		name="<?= $name ?>" size="60" type="file" />
		&nbsp;
		<input type="checkbox" class="checkbox" id="<?= $tileHId ?>"
			<?= $tiledHChecked ? 'checked="checked"' : "" ?> name="<?= $tiledHName ?>" />
		<label title="Horizontal tiling" for="<?= $tileHId ?>">H.Tiling</label>
		&nbsp;
		<input type="checkbox" class="checkbox" id="<?= $tileVId ?>"
			<?= $tiledVChecked ? 'checked="checked"' : "" ?> name="<?= $tiledVName ?>" />
		<label title="Vertical tiling" for="<?= $tileVId ?>">V.Tiling</label>
	</div>
	<?

	if( isset( $errors[ $name ]))
	{
		notice( $errors[ $name ]);
	}

	/*
	?>
	<table><tr><td>
	<div style="display: none" id="<?= $previewContainerId ?>">
		<div class="sep caption"><?= _PREVIEW ?>:</div>
		<div class="container2 a_center">
			<img alt="preview" style="display: none" id="<?= $previewImageId ?>" src="" />
			<div id="<?= $previewMessageId ?>"><?= _SUBMIT_SELECT_FILE ?></div>
		</div>
	</div>
	</td></tr></table>
	<?
	*/
}

// ---------------------------------------------------------------------------

?>
