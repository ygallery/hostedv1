<?

redirect( url( "helpdesk/faq" ));

?>
<div class="header">
	<div class="header_title"><?=_HELP ?></div>
</div>

<div class="container">
	<div class="notsowide">
	<?
		$_documentTitle = _HELP;

		$filename = INCLUDES."strings/".$_lang."_help.php";

		if( !file_exists( $filename ))
			$filename = INCLUDES."strings/en_help.php";

		include($filename);

		if(isset($_GET["popup"])) {
			?>
			<div style="height: 600px"><br /></div>
			<?
		}
	?>
	</div>
</div>
