<?

if( !isExtras() && !atLeastSModerator() || !isArtist( $_auth[ "useid" ]) && !atLeastSModerator() )
{
	include(INCLUDES."p_notfound.php");
	return;
}
$_documentTitle = _EXTRA_SUBMIT;

$objid = intval( $_cmd[ 1 ]);

if( $objid > 0 )
{
	$where = array(
		"objid" => $objid,
		"objid*" => "objEid" );

	if( !atLeastSModerator() )
	{
		$where[ "objCreator" ] = $_auth[ "useid" ];
	}

	$result = sql_query( "SELECT * FROM `extras`, `extExtData`".dbWhere( $where ));

	if( $objData = mysql_fetch_assoc( $result ))
	{
		if( !isset( $_POST[ "submit" ]))
		{
			$_POST[ "title" ] = $objData[ "objTitle" ];
			$_POST[ "comment" ] = $objData[ "objComment" ];

			$objFilters = preg_split( '/[\s\,\;]/', $objData[ "objMature" ],
				64, PREG_SPLIT_NO_EMPTY );

			foreach( $objFilters as $filter )
			{
				$_POST[ "filter".$filter ] = 1;
			}

			$_POST[ "thumbURL" ] = $objData[ "objThumbURL" ];
			$_POST[ "previewURL" ] = $objData[ "objPreviewURL" ];
			$_POST[ "fullURL" ] = $objData[ "objImageURL" ];
		}
	}
	else
	{
		include(INCLUDES."p_notfound.php");
		return;
	}
}

if( $objid == 0 )
{
	$objCreator = $_auth[ "useid" ];
}
else
{
	$objCreator = $objData[ "objCreator" ];
}

?>
<div class="header">
	<div class="header_title">
		<?=_EXTRA_SUBMIT ?>
		<div class="subheader"><?= _EXTRA_SUBMIT_EXPLAIN ?></div>
	</div>
</div>

<div class="container">
<form action="<?= url( "." ) ?>" method="post">
<?

if( $objid == 0 )
{
	list( $rat1, $rat2 ) = preg_split( '/\:/', $_config[ "extrasRatio" ]);

	$extrasCount = mysql_result( sql_query( "SELECT COUNT(*) FROM `extras`".dbWhere( array(
		"objPending" => 0,
		"objDeleted" => 0,
		"objCreator" => $_auth[ "useid" ]))), 0 );

	$neededObjCount = ceil(( $extrasCount + 1 ) * $rat2 / $rat1 - $_auth[ "useObjCount" ]); 

	if( $neededObjCount > 0 )
	{
		?>
		<div class="container2 notsowide">
			<div class="largetext mar_bottom"><?= _WARNING ?></div>
			<?= sprintf( _EXTRA_RATIO_EXPLAIN, $neededObjCount ) ?>
		</div>
		</div>
		<?
		return;
	}
}

if( $objid > 0 && $objData[ "objNumEdits" ] >= $_config[ "maxObjectEdits" ])
{
	notice( _EDIT_EXCEEDED );
	echo '</div>';
	return;
}

if( isset( $_POST[ "submit" ]))
{
	while( true )
	{
		if( !isset( $_POST[ "title" ]) || trim( $_POST[ "title" ]) == "" )
		{
			notice( _SUBMIT_NO_TITLE );
			break;
		}

		if( !isset( $_POST[ "comment" ]) || trim( $_POST[ "comment" ]) == "" )
		{
			notice( _SUBMIT_NO_COMMENT );
			break;
		}

		if( !isset( $_POST[ "thumbURL" ]) || trim( $_POST[ "thumbURL" ]) == "" )
		{
			notice( _EXTRA_NO_THUMB );
			break;
		}

		if( !isset( $_POST[ "previewURL" ]) || trim( $_POST[ "previewURL" ]) == "" )
		{
			notice( _EXTRA_NO_PREVIEW );
			break;
		}

		if( !isset( $_POST[ "fullURL" ]) || trim( $_POST[ "fullURL" ]) == "" )
		{
			notice( _EXTRA_NO_FULL );
			break;
		}

		// Validate Thumbnail

		$thumbSize = intval( remote_filesize( $_POST[ "thumbURL" ]));

		// Validate

		if( $thumbSize > 1048576 || !filter_var($_POST['thumbURL'], FILTER_VALIDATE_URL))
		{
			notice( "Unable to access the thumbnail image file at your server. ".
				"Please ensure that the given URL is correct and try again. ".
				"Also make sure that your server supports direct linking to the images." );

			break;
		}

		$thumbInfo = getimagesize( $_POST[ "thumbURL" ]);
		list( $thumbMaxWidth, $thumbMaxHeight ) = preg_split( '/x/', $_config[ "thumbResolution" ]);
		$thumbMaxSize = $_config[ "extrasThumbSize" ];

		if( $thumbInfo[ 2 ] != 2 )
		{
			notice( "Your thumbnail is not a valid JPG file." );
			break;
		}

		if( $thumbInfo[ 0 ] > $thumbMaxWidth || $thumbInfo[ 1 ] > $thumbMaxHeight )
		{
			notice( sprintf( "Your thumbnail is %dx%d pixels. Maximum is %dx%d pixels.",
				$thumbInfo[ 0 ], $thumbInfo[ 1 ], $thumbMaxWidth, $thumbMaxHeight ));

			break;
		}

		if( $thumbSize > $thumbMaxSize )
		{
			notice( sprintf( "Your thumbnail is %d bytes. Maximum is %d bytes.", $thumbSize, $thumbMaxSize ));
			break;
		}

		// Validate Normal quality image

		$previewSize = intval( remote_filesize( $_POST[ "previewURL" ]));

		if( $previewSize > 2097152 || !filter_var($_POST['previewURL'], FILTER_VALIDATE_URL))
		{
			notice( "Unable to access the normal quality image file at your server. ".
				"Please ensure that the given URL is correct and try again. ".
				"Also make sure that your server supports direct linking to the images." );

			break;
		}

		$previewInfo = getimagesize( $_POST[ "previewURL" ]);
		list( $previewMaxWidth, $previewMaxHeight ) = preg_split( '/x/', $_config[ "extrasPreviewRes" ]);
		$previewMaxSize = $_config[ "extrasPreviewSize" ];

		if(( $previewInfo[ 2 ] < 1 || $previewInfo[ 2 ] > 4 ) && $previewInfo[ 2 ] != 13 )
			// 1 = GIF, 2 = JPG, 3 = PNG, 4 = SWF, 13 = SWC
		{
			notice( "Your normal quality image is not a valid JPG, GIF, PNG or SWF image file." );
			break;
		}

		if( $previewInfo[ 0 ] > $previewMaxWidth || $previewInfo[ 1 ] > $previewMaxHeight )
		{
			notice( sprintf( "Your normal quality image is %dx%d pixels. Maximum is %dx%d pixels.",
				$previewInfo[ 0 ], $previewInfo[ 1 ], $previewMaxWidth, $previewMaxHeight ));

			break;
		}

		if( $previewSize > $previewMaxSize )
		{
			notice( sprintf( "Your normal quality image is %d bytes. Maximum is %d bytes.",
				$previewSize, $previewMaxSize ));

			break;
		}

		// Validate Full quality image

		$fullSize = intval( remote_filesize( $_POST[ "fullURL" ]));

		if( $fullSize > 10485760 || !filter_var($_POST['fullURL'], FILTER_VALIDATE_URL))
		{
			notice( "Unable to access the full quality image file at your server. ".
				"Please ensure that the given URL is correct and try again. ".
				"Also make sure that your server supports direct linking to the images." );

			break;
		}

		$fullInfo = getimagesize( $_POST[ "fullURL" ]);

		if(( $fullInfo[ 2 ] < 1 || $fullInfo[ 2 ] > 4 ) && $fullInfo[ 2 ] != 13 )
			// 1 = GIF, 2 = JPG, 3 = PNG, 4 = SWF, 13 = SWC
		{
			notice( "Your full quality image is not a valid JPG, GIF, PNG or SWF image file." );
			break;
		}

		// Everything's ok!

		$filters = "";
		$first = true;

		foreach( getEnabledFilters() as $filter )
		{
			if( isset( $_POST[ "filter".$filter ]))
			{
				$filters .= ( $first ? "" : "," ).$filter;

				if( $first )
					$first = false;
			}
		}

		// Double-check if the user is the owner of specified folder

		$folder = isset( $_POST[ "folder" ]) ? intval( $_POST[ "folder" ]) : 0;

		$result = sql_query( "SELECT COUNT(*) FROM `folders`".dbWhere( array(
			"folCreator" => $objCreator,
			"folid" => $folder )));

		if( mysql_result( $result, 0 ) == 0 )
			$folder = 0;

		// Check if the submission was updated or not, i.e. do we need to send it to
		// the watchers?

		if( $objid == 0 )
		{
			$isUpdated = true;
		}
		else
		{
			$isUpdated = ( $_POST[ "fullURL" ] != $objData[ "objImageURL" ]);
		}

		$values1 = array(
			"objTitle" => $_POST[ "title" ],
			"objFolder" => $folder,
			"objThumbWidth" => $thumbInfo[ 0 ],
			"objThumbHeight" => $thumbInfo[ 1 ],
			"objThumbURL" => $_POST[ "thumbURL" ],
			"objLastEdit!" => "NOW()",
			"objMature" => $filters );

		$values2 = array(
			"objImageWidth" => $fullInfo[ 0 ],
			"objImageHeight" => $fullInfo[ 1 ],
			"objImageSize" => $fullSize,
			"objImageURL" => $_POST[ "fullURL" ],
			"objPreviewWidth" => $previewInfo[ 0 ],
			"objPreviewHeight" => $previewInfo[ 1 ],
			"objPreviewURL" => $_POST[ "previewURL" ],
			"objComment" => $_POST[ "comment" ],
			"objEditIp" => getHexIp( $_SERVER[ "REMOTE_ADDR" ]));

		if( $objid == 0 )
		{
			$values1[ "objCreator" ] = $objCreator;
			$values1[ "objSubmitDate!" ] = "NOW()";

			sql_query( "INSERT INTO `extras`".dbValues( $values1 ));

			$objid = mysql_insert_id();

			$values2[ "objEid" ] = $objid;
			$values2[ "objSubmitIp" ] = getHexIp( $_SERVER[ "REMOTE_ADDR" ]);

			sql_query( "INSERT INTO `extExtData`".dbValues( $values2 ));
		}
		else
		{
			$values2[ "objNumEdits" ] = $objData[ "objNumEdits" ] + 1;
			$values2[ "objNoAbuse" ] = 0;

			sql_query( "UPDATE `extras`".dbSet( $values1 ).dbWhere( array(
				"objid" => $objid )));

			sql_query( "UPDATE `extExtData`".dbSet( $values2 ).dbWhere( array(
				"objEid" => $objid )));
		}

		if( $isUpdated && $objCreator == $_auth[ "useid" ])
		{
			// Notify the watchers.

			addUpdateToWatchers( updTypeArtExtra, $objCreator, $objid );
		}

		redirect( url( "view/e".$objid ));
	}
}

?>
<div class="caption"><?= _TITLE ?>:</div>
<div>
	<input class="notsowide largetext" id="submitTitle" name="title" type="text"
		<?= isset( $_POST[ "title" ]) ? 'value="'.htmlspecialchars( $_POST[ "title" ]).'"' : "" ?> />
</div>
<div class="sep caption"><?= _COMMENT ?>:</div>
<table cellspacing="0" cellpadding="0" border="0" class="notsowide"><tr><td>
<?

$commentDefault = isset( $_POST[ "comment" ]) ? $_POST[ "comment" ] : "";
$commentNoOptions = true;

include( INCLUDES."mod_comment.php" );

?>
</td></tr></table>
<?

$filters = getEnabledFilters();
$userFilters = preg_split( '/\,/', $_auth[ "useObjFilters" ], 64, PREG_SPLIT_NO_EMPTY );
$found = false;

if( atLeastModerator() )
{
	$userFilters = array();
}

ob_start();

?>
<div class="sep caption">
	<?= _FILTER_OPTIONS ?>:
	&nbsp; &nbsp;
	<a style="font-weight: normal" href="<?=url("helpdesk/faq/filters/nudity" )?>" target="new-tab">
		<?= getIMG( url()."images/emoticons/help.png" ) ?>
		<?=_WHATS_THIS ?></a>
</div>
<div class="container2 notsowide">
<?

foreach( $filters as $filter )
{
	if( in_array( $filter, $userFilters ))
		continue; // User has decided to not show images filtered with this filter.

	?>
	<div>
		<input class="checkbox" type="checkbox" name="filter<?= $filter ?>"
			id="idFilter<?= $filter ?>"
			<?= isset( $_POST[ "filter".$filter ]) ? 'checked="checked"' : "" ?> />
		<label for="idFilter<?= $filter ?>"><?= getFilterName( $filter ) ?></label>
	</div>
	<?

	$found = true;
}

?>
</div>
<?

if( $found )
	ob_end_flush();
else
	ob_end_clean();

?>
<div class="clear">&nbsp;</div>
<?

$found = false;

ob_start();

?>
<div class="sep caption"><?=_SUBMIT_TO_FOLDER ?>:</div>
<div class="f_left mar_right mar_top">
	<?=getIMG(url()."images/emoticons/folder.png")?>
</div>
<select name="folder" class="largetext">
	<option value="0">( <?=_NONE ?> )</option>
	<?

	$folResult = sql_query( "SELECT `folid`, `folName` FROM `folders`".dbWhere( array(
		"folCreator" => $objCreator ))."ORDER BY `folName`" );

	$folder = isset( $_POST[ "folder" ]) ? intval( $_POST[ "folder" ]) :
		( $objid > 0 ? $objData[ "objFolder" ] : 0 );

	while( $folData = mysql_fetch_assoc( $folResult ))
	{
		?>
		<option <?= $folder == $folData[ "folid" ] ? 'selected="selected"' : "" ?>
			value="<?= $folData[ "folid" ]?>"><?= htmlspecialchars( $folData[ "folName" ]) ?></option>
		<?

		$found = true;
	}

	?>
</select>
&nbsp;
<a target="new-tab" href="<?=url("helpdesk/faq/general/folders")?>">
	<?= getIMG( url()."images/emoticons/help.png" ) ?>
	<?=_WHATS_THIS ?></a>
<div class="clear">&nbsp;</div>
<?

if( $found )
	ob_end_flush();
else
{
	ob_end_clean();

	?>
	<input type="hidden" name="folder" value="0" />
	<?
}

?>
<div class="sep caption">
	URLs of your files:
	&nbsp; &nbsp;
	<a style="font-weight: normal" href="<?=url("helpdesk/faq/general/extras")?>" target="new-tab">
		<?= getIMG( url()."images/emoticons/help.png" ) ?>
		<?=_WHATS_THIS ?></a>
</div>
<div class="container2 notsowide">
	<table cellspacing="0" cellpadding="4" border="0">
	<tr>
		<td class="nowrap" align="right"><b>Thumbnail</b>:</td>
		<td width="100%"><input class="notsowide" name="thumbURL" type="text"
			<?= isset( $_POST[ "thumbURL" ]) ? 'value="'.htmlspecialchars( $_POST[ "thumbURL" ]).'"' : "" ?> /></td>
	</tr>
	<tr>
		<td></td>
		<td>Maximum <?= $_config[ "thumbResolution" ] ?> pixels and
			<?= $_config[ "extrasThumbSize" ] ?> bytes (<?= round( $_config[ "extrasThumbSize" ] / 1024 ) ?> KB).<br />
			Must be a JPG file.</td>
	</tr>
	<tr>
		<td class="nowrap" align="right"><b>Normal quality</b>:</td>
		<td width="100%"><input class="notsowide" name="previewURL" type="text"
			<?= isset( $_POST[ "previewURL" ]) ? 'value="'.htmlspecialchars( $_POST[ "previewURL" ]).'"' : "" ?> /></td>
	</tr>
	<tr>
		<td></td>
		<td>Maximum <?= $_config[ "extrasPreviewRes" ] ?> pixels and
			<?= $_config[ "extrasPreviewSize" ] ?> bytes (<?= round( $_config[ "extrasPreviewSize" ] / 1024 ) ?> KB).</td>
	</tr>
	<tr>
		<td class="nowrap" align="right"><b>Full quality</b>:</td>
		<td width="100%"><input class="notsowide" name="fullURL" type="text"
			<?= isset( $_POST[ "fullURL" ]) ? 'value="'.htmlspecialchars( $_POST[ "fullURL" ]).'"' : "" ?> /></td>
	</tr>
	<tr>
		<td></td>
		<td>Not limited.</td>
	</tr>
	</table>
	<div><br /></div>
	<div>
		<?php // ADD LANGUAGE SUPPORT FOR THIS SECTION?>
		Make three versions of your submission (however, Normal and Full quality can be the
		same image file if it is small enough), upload them to your server and specify the
		URL's of your files, for example:
	</div>
	<div class="sep">
		<table align="center" cellspacing="0" cellpadding="4" border="0">
		<tr>
			<td class="nowrap" align="right"><b>Thumbnail</b>:</td>
			<td>http://www.mysite.com/pics/image_thumbnail.jpg</td>
		</tr>
		<tr>
			<td class="nowrap" align="right"><b>Normal quality</b>:</td>
			<td>http://www.mysite.com/pics/image_preview.jpg</td>
		</tr>
		<tr>
			<td class="nowrap" align="right"><b>Full quality</b>:</td>
			<td>http://www.mysite.com/pics/image.jpg</td>
		</tr>
		</table>
	</div>
	<div class="sep">
		<b>Note</b>: your server must support direct linking to the images.
	</div>
</div>

<div><br /></div>

<button class="submit" name="submit" type="submit">
	<?=getIMG(url()."images/emoticons/checked.png") ?>
	<?=_SUBMIT ?>
</button>

</form>
</div>
<?

/**
 * Returns mixed values
 *
 * @author    Laurentiu Tanase <expertphp@yahoo.com>
 * @version   1.2 Beta 2
 * @param     string  $url     URL address
 * @param     string  $method  HTTP request method (HEAD, GET, POST)
 * @param     string  $query   POST query
 * @param     integer $out     Time out connection
 */

function remote_filesize($url, $method = "", $query = "", $out = 30){

    $parse = parse_url($url);
    if(!isset($parse['scheme'], $parse['host'])) return 'unknown';
    // set default method HEAD
    if(!($method == "GET" || $method == "POST")) $method = "HEAD";
    // if is https
    if($parse['scheme'] == "https"){
        $proto = "ssl://";
        $port = 443;
    }else{
        $proto = "";
        $port = 80;
    }
    // set return default value
    $size = 'unknown';
    // open a socket connection
    if($fp = fsockopen($proto.$parse['host'], $port, $errno, $errstr, $out)){
        // set request
        $get = $method." ".(isset($parse['path']) ? $parse['path'] : "/").
            (isset($parse['query']) ? "?".$parse['query'] : "")." HTTP/1.1\r\n".
            "Host: ".$parse['host']."\r\n";
        // if is set user & pass
        if(isset($parse['user'], $parse['pass']) && $parse['user'] != "" && $parse['pass'] != ""){
            $get .= "Authorization: Basic ".base64_encode($parse['user'].":".$parse['pass'])."\r\n";
        }
        $get .= "Connection: close\r\n\r\n". // post query
            (($method == "POST" && $query != "") ? $query : "");
        fputs($fp, $get);
        $header = "";
        while(!feof($fp)){
            // get ONLY header informations
            if($method != "HEAD" && substr($header, -4) == "\r\n\r\n") break;
            else $header .= fgets($fp, 128);
        }
        fclose($fp);
        // for debugging
        // echo $get."---------------------\n\n".$header;
        // if response is ok
        if(strstr($header, '200 OK')){
            // match file size
            preg_match('/Content-Length:\s([0-9].+?)\s/', $header, $matches);
            if(isset($matches[1])) $size = intval($matches[1]);
        }
    }

    return $size;

} // End remote_filesize --------------------------------

?>