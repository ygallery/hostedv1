<?

if( !isLoggedIn() )
{
	echo _REQUIRE_LOGIN;
	return;
}

if( !atLeastSModerator() )
{
	echo formatText( "Sorry, the Helpdesk is currently in development. If you have a request, please send it to [c=1]." );
	return;
}

if( isset( $_POST[ "summary" ]))
{
	sql_values( array(
		"hlpSummary" => $_POST[ "summary" ],
		"hlpCategory" => $_POST[ "category" ],
		"hlpSubmitDate!" => "NOW()",
		"hlpSubmitter" => $_auth[ "useid" ],
		"hlpReferenceType" => $_POST[ "referenceType" ],
		"hlpReferenceId" => $_POST[ "referenceID" ],
		"hlpOwner" => getRequestRefOwner( $_POST[ "referenceType" ],
			$_POST[ "referenceID" ])));

	$helpdeskItem = sql_insert( "helpdesk" );

	addRequestDetail( $helpdeskItem, "publicDetail", "publicFile", "all" );
	addRequestDetail( $helpdeskItem, "privateDetail", "privateFile",
		"submitter" );

	redirect( url( "helpdesk" ));
}

$requestCat = strtolower( $_cmd[ 2 ]);
$requestRef = strtolower( $_cmd[ 3 ]);
$requestRefId = intval( $_cmd[ 4 ]);

$cats = array();

$catsResult = sql_rowset( "helpdeskCats" );

while( $catsData = sql_next( $catsResult ))
{
	$cats[ $catsData[ "hdcid" ]] = array(
		"name" => $catsData[ "hdcName" ],
		"type" => $catsData[ "hdcType" ]);
}

sql_free( $catsResult );

?>
<div class="header">
	Add a Request
</div>
<form action="<?= url( "." ) ?>" enctype="multipart/form-data" method="post">
<div class="container2 mar_bottom">
	<table cellspacing="15" cellpadding="0" border="0">
	<tr>
	<td valign="bottom" width="50%">
		<div class="mar_bottom">
			<?= getIMG( url()."images/emoticons/a-left.png" )?>
			<b>Short summary</b>
		</div>
		<div class="mar_bottom" style="padding-right : 10%">
			A brief public summary of the request. The following text will be
			available to be viewed by the owner of the reported object (if any):
		</div>
		<div class="mar_bottom">
			<?

			$summary = "";

			/*
			if( isset( $_GET[ "quickSummary" ]))
			{
				$summary = htmlspecialchars( $_GET[ "quickSummary" ]);
			}
			*/

			?>
			<textarea name="summary" rows="5" style="width : 95%"><?= $summary ?></textarea>
		</div>
	</td>
	<td valign="bottom" width="50%">
		<div style="margin-left : 10%">
			<div class="mar_bottom">
				Category:
			</div>
			<div class="mar_bottom">
				<select name="category">
				<?
	        
				foreach( $cats as $id => $info )
				{
					$isSelected = ( $info[ "type" ] == $requestCat );

					?>
					<option<?= $isSelected ? ' selected="selected"' : '' ?>
						value="<?= $id ?>"><?= $info[ "name" ] ?></option>
					<?
				}
	        
				?>
				</select>
			</div>
			<div><br /></div>
			
			<div class="mar_bottom" style="padding-right : 10%">
				In reference to:
			</div>
			<div class="mar_bottom">
				<?

				$ref = "none";

				switch( $requestRef )
				{
					case "submission":
					case "extra":
					case "pm":
					case "comment":
					case "journal":
					case "poll":
					case "faq":
					{
						$ref = $requestRef;
						break;
					}
				}

				$refid = $requestRefId == 0 ? "" : $requestRefId;

				?>
				<select name="referenceType">
					<option value="none">None</option>
					<option<?= $ref == "submission" ? ' selected="selected"' : '' ?> value="submission">Submission</option>
					<option<?= $ref == "extra" ? ' selected="selected"' : '' ?> value="extra">Submission in Extras</option>
					<option<?= $ref == "pm" ? ' selected="selected"' : '' ?> value="pm">Private message</option>
					<option<?= $ref == "comment" ? ' selected="selected"' : '' ?> value="comment">Comment</option>
					<option<?= $ref == "journal" ? ' selected="selected"' : '' ?> value="journal">Journal entry</option>
					<option<?= $ref == "poll" ? ' selected="selected"' : '' ?> value="poll">Poll</option>
					<option<?= $ref == "faq" ? ' selected="selected"' : '' ?> value="faq">FAQ article</option>
				</select>
			</div
			<table cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<div style="margin-left : 2%" class="nowrap">
							&nbsp; &nbsp;
							Object ID:
							<input type="text" name="referenceID" size="18" value="<?= $refid ?>" />
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="smalltext" style="margin-top : 0.2em; text-align : right;">
							<a href="javascript:;" onclick="$('whats-objid').toggle();">(what's this?)</a>
						</div>
					</td>
				</tr>
			</table>
			<div class="container2 mar_top" style="display : none" id="whats-objid">
				<div class="mar_bottom">
                    Object ID is the <b>first</b> number you can see in the
                    address bar in your browser when viewing a certain
                    object's page. For example:
				</div>
				<div class="mar_bottom">
					<b>Submission</b>:<br />
					<?= url( "view/12345" ) ?><br />
					Object ID: 12345
				</div>
				<div class="mar_bottom">
					<b>Comment</b>:<br />
					<?= url( "comment/98765" ) ?><br />
					Object ID: 98765
				</div>
				<div class="mar_bottom">
					<b>Journal entry</b>:<br />
					<?= url( "journal/12121" ) ?><br />
					Object ID: 12121
				</div>
				<div class="mar_bottom">
                    Always keep in mind it's the very first number in the
                    address bar, it may be followed (or not) by more numbers
                    and other data.
				</div>
				<div class="mar_bottom">
                    <b>Note</b>: Normally, you don't need to type in the Object
                    ID by hand, instead go to the object's page and find and
                    click the &quot;Report&quot; link.
				</div>
				<div class="button" onclick="$('whats-objid').hide();">OK</div>
				<div style="clear : both"></div>
			</div>
		</div>
	</td>
	</tr>
	<tr>
	<td valign="bottom" width="50%">
		<div class="mar_bottom">
			<?= getIMG( url()."images/emoticons/star.png" )?>
			<b>Public details</b>
		</div>
		<div class="mar_bottom" style="padding-right : 10%">
			Details available to the site staff and the owner of the reported object (if any):
		</div>
		<div class="mar_bottom">
			<textarea name="publicDetail" rows="9" style="width : 95%;"></textarea>
		</div>
		<div class="mar_bottom" style="padding-right : 10%">
			(optionally) <b>Upload a file</b> (public):
		</div>
		<div class="mar_bottom">
			<input name="publicFile" type="file" size="30" />
		</div>
	</td>
	<td valign="bottom" width="50%">
		<div class="mar_bottom">
			<?= getIMG( url()."images/emoticons/ghost.png" )?>
			<b>Private details</b>
		</div>
		<div class="mar_bottom" style="padding-right : 10%">
			Details available to the site staff only:
		</div>
		<div class="mar_bottom">
			<textarea name="privateDetail" rows="9" style="width : 95%"></textarea>
		</div>
		<div class="mar_bottom" style="padding-right : 10%">
			(optionally) <b>Upload a file</b> (private):
		</div>
		<div class="mar_bottom">
			<input name="privateFile" type="file" size="30" />
		</div>
	</td>
	</tr>
	</table>
</div>
<div style="margin-left : 25px">
	<button>
		<?= getIMG( url()."images/emoticons/checked.png" )?>
		Submit Request
	</button>
</div>
</form>
