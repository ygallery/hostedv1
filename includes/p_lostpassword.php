<div class="header">
	<div class="header_title"><?=_LOSTPASS_TITLE ?></div>
</div>
<div class="container">
<?php
	$_documentTitle = _LOSTPASS_TITLE;

	$user = isset($_POST["username"]) ? $_POST["username"] : "";
	$result = sql_query("SELECT `useid`,`useActivationKey`,`useEmail` FROM `users`,`useExtData` WHERE `useid` = `useEid` AND `useUsername` = '".addslashes($user)."' LIMIT 1");
	if(!$useData = mysql_fetch_assoc($result)) {
		if($user)
			notice(_WRONG_USERNAME);
	}
	else {
		// send the lost password retrieval confimation request via email
		include_once(INCLUDES."mailing.php");
		#Hack for the current lack of mod_rewrite
#		$activationLink = url("index.php?page=password&par1=".$useData["useActivationKey"]);
		$activationLink = url("password/".$useData["useActivationKey"]);

		// TODO: how to avoid a possible hack that would be sending like 1000 emails a second ???

		sendEmail($useData["useid"], sprintf(_USE_LOSTPASS_SUBJ, $_config["galName"]),
			sprintf(_USE_LOSTPASS_BODY, $activationLink));

		notice(sprintf(_EMAIL_SENT, _EMAIL_SENT_HIDDEN_ADDRESS));
		echo "</div>";
		return;
	}
?>

<form action="<?=url(".")?>" method="post">
<div class="caption"><?=_LOSTPASS_USERNAME ?>:</div>
<div><input class="narrow" name="username" type="text" value="<?=htmlspecialchars($user)?>" /></div>
<div class="sep">
	<button class="submit" type="submit">
		<?=getIMG(url()."images/emoticons/checked.png") ?>
		<?=_OK ?>
	</button>
</div>
</form>

</div>
