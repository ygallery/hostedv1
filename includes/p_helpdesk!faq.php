<?

$_documentTitle = "Frequently Asked Questions";

$allowEditMode = atLeastSModerator();
$isEditMode = ( $allowEditMode && isset( $_GET[ "enableEditMode" ]));

if( $allowEditMode && !$isEditMode )
{
	?>
	<div style="float : right;">
		<div class="button"><a href="<?= url( ".", array( "enableEditMode" => "yes" )) ?>" class="smalltext">Edit Mode</a></div>
	</div>
	<?
}

if( $_cmd[ 2 ] == "tag" )
{
	include( INCLUDES."p_helpdesk!faq-tag.php" );
	return;
}

if( $_cmd[ 3 ] != "" )
{
	include( INCLUDES."p_helpdesk!faq-article.php" );
	return;
}

$cats = array();

$where = array();

if( !$isEditMode )
{
	$where[ "hfcIsGeneral" ] = 1;
}

if( $_cmd[ 2 ] != "" )
{
	$where[ "hfcIdent" ] = $_cmd[ 2 ];
}

sql_order( "hfcName" );
sql_where( $where );

$catResult = sql_rowset( "helpdeskFAQCats" );

while( $catData = sql_next( $catResult ))
{
	$cats[ $catData[ "hfcid" ]] = array(
		"general" => $catData[ "hfcIsGeneral" ],
		"ident" => $catData[ "hfcIdent" ],
		"name" => $catData[ "hfcName" ]);
}

sql_free( $catResult );

foreach( $cats as $catKey => $catInfo )
{
	if( $isEditMode )
	{
		$editAction = "Edit";
		include( INCLUDES."p_helpdesk!faq-editcat.php" );
	}
	else
	{
		?>
		<div class="header">
			<?= htmlspecialchars( preg_replace( '/^.*\|/', "", $catInfo[ "name" ])) ?>
		</div>
		<?
	}

	?>
	<ul>
	<?

	sql_order( "hfqTitle" );
	sql_where( array( "hfqCategory" => $catKey ));

	$faqResult = sql_rowset( "helpdeskFAQ", "hfqid, hfqIdent, hfqTitle" );

	while( $faqData = sql_next( $faqResult ))
	{
		$url = url( "helpdesk/faq/".$catInfo[ "ident" ]."/".$faqData[ "hfqIdent" ]);
		$title = preg_replace( '/^.*\|/', "", $faqData[ "hfqTitle" ]);

		?>
		<li><a href="<?= $url ?>"><?= htmlspecialchars( $title ) ?></a></li>
		<?
	}

	sql_free( $faqResult );

	?>
	</ul>
	<?
}

unset( $catKey );
unset( $catInfo );

if( $_cmd[ 2 ] != "" )
{
	?>
	<div style="margin-left : 0.5em; padding-top : 0.7em;">
		<form action="<?= url( "helpdesk/faq" ) ?>" method="get">
		<button class="smalltext">
			<?= getIMG( url()."images/emoticons/nav-prev.png" )?>
			Return to FAQ
		</button>
		</form>
	</div>
	<?
}
else
{
	function putTagList( $title, $order )
	{
		?>
		<div style="margin-left : 2em;">
			<?= $title ?>:
		</div>
		<ul style="margin : 0.3em 1.2em; padding : 0; padding-left : 2em;">
		<?

		sql_order( $order );
		sql_where( array( "hftCount>" => 0 ));
	
		$tagResult = sql_rowset( "helpdeskFAQTags" );
	
		while( $tagData = sql_next( $tagResult ))
		{
			$url = url( "helpdesk/faq/tag", array( "tag" => $tagData[ "hftName" ]));
	
			?><li><a href="<?= $url ?>"><?= $tagData[ "hftName" ] ?></a> (<?= $tagData[ "hftCount" ] ?>)</li><?
		}
	
		sql_free( $tagResult );

		?>
		</ul>
		<?
	}

	?>
	<div class="header">
		Tags
	</div>
	<table cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td><? putTagList( "by number of articles", "hftCount DESC" ); ?></td>
		<td><? putTagList( "by name", "hftName" ); ?></td>
	</tr>
	</table>
	<?
	
	if( $isEditMode )
	{
		$editAction = "Add";
		include( INCLUDES."p_helpdesk!faq-editcat.php" );
	
		$editAction = "Add";
		include( INCLUDES."p_helpdesk!faq-edit.php" );
	}
}

?>