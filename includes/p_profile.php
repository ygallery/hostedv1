<?

// This script controls the "Profile" tab of the settings.

if( !isLoggedIn() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

?>
<div class="header">
	<div class="header_title">
		<?= _SETTINGS ?>
		<div class="subheader"><?= _SET_PROFILE_SETTINGS ?></div>
	</div>
	<?

	$_documentTitle = _SET_PROFILE_SETTINGS;

	$active = 2;

	include( INCLUDES."mod_setmenu.php" );

	?>
</div>
<div class="container">
	<form action="<?= url( "." ) ?>" enctype="multipart/form-data" method="post">
	<?

	if( isset( $_POST[ "submit" ]))
	{
		// Account options

		$_auth[ "useShowEmail" ] = isset( $_POST[ "useShowEmail" ]) ? 1 : 0;

		$_auth[ "useIsHidden" ] = isset( $_POST[ "useIsHidden" ]) ? 1 : 0;

		$_auth[ "useStatsPublic" ] = isset( $_POST[ "useStatsPublic" ]) ? 1 : 0;

		$oldGuestAccess = $_auth[ "useGuestAccess" ];

		$_auth[ "useGuestAccess" ] = isset( $_POST[ "useGuestAccess" ]) ? 1 : 0;

		include_once( INCLUDES."files.php" );

		// Avatar

		$avatarError = checkUploadedFile( "avatar" );

		if( !$avatarError )
		{
			if( filesize( $_FILES[ "avatar" ][ "tmp_name" ]) > $_config[ "maxAvatarSize" ])
			{
				$avatarError = sprintf( _SET_AVATAR_SIZE_EXCEEDED, $_config[ "maxAvatarSize" ]);
			}
			else
			{
				// Check avatar image size/type.

				$avatarInfo = getimagesize( $_FILES[ "avatar" ][ "tmp_name" ]);

				list( $minAvatarWidth, $minAvatarHeight ) = preg_split( '/x/', $_config[ "minAvatarResolution" ]);
				list( $maxAvatarWidth, $maxAvatarHeight ) = preg_split( '/x/', $_config[ "maxAvatarResolution" ]);

				if( $avatarInfo[ 0 ] > $maxAvatarWidth || $avatarInfo[ 1 ] > $maxAvatarHeight ||
					$avatarInfo[ 0 ] < $minAvatarWidth || $avatarInfo[ 1 ] < $minAvatarHeight ||
					( $avatarInfo[ 2 ] != 1 && $avatarInfo[ 2 ] != 2 && $avatarInfo[ 2 ] != 3 ))
				{
					$avatarError = sprintf( _SET_AVATAR_TOO_LARGE, $_config[ "minAvatarResolution" ],
						$_config[ "maxAvatarResolution" ]);
				}
				else
				{
					// Upload avatar icon to /files/avatars/#/#####/

					$avatarFilename = applyIdToPath("files/avatars/", $_auth["useid"]);

					$oldFiles = glob( $avatarFilename."-*", GLOB_NOESCAPE );

					if( is_array( $oldFiles ) && count( $oldFiles ) > 0 )
					{
						foreach( $oldFiles as $oldFile )
						{
							unlink( $oldFile ); // Delete old files
						}
					}

					uploadFile( "avatar", $avatarFilename.'-'.time(), $extension );

					updateAvatar( $_auth[ "useid" ]);
				}
			}
		}

		if( $avatarError != "" && $avatarError != _UPL_NO_FILE )
		{
			notice( $avatarError );
		}

		// ID

		$idError = checkUploadedFile( "id" );

		if( !$idError )
		{
			$idImageName = $_FILES[ "id" ][ "tmp_name" ];
		}
		else
		{
			$idImageName = "";
		}

		if( $idError != "" && $idError != _UPL_NO_FILE )
		{
			notice( $idError );
		}

		// Featured work

		$objid = $_POST[ "useFeaturedObj" ];

		$objResult = sql_query( "SELECT `objid`, `objTitle`, `objExtension`, `objLastEdit` ".
			"FROM `objects`, `objExtData`".dbWhere( array(
			"objid" => $objid,
			"objCreator" => $_auth[ "useid" ],
			"objEid*" => "objid",
			"objDeleted" => 0,
			"objPending" => 0 ))."LIMIT 1" );

		$featChanged = false;

		if( $objData = mysql_fetch_assoc( $objResult ))
		{
			$featImageName = applyIdToPath( "files/data/", $objid)."-".
				preg_replace( '/[^0-9]/', "", $objData[ "objLastEdit" ]).".".$objData[ "objExtension" ];

			$featImageName2 = applyIdToPath( "files/thumbs/", $objid )."-".
				preg_replace( '/[^0-9]/', "", $objData[ "objLastEdit" ]).".jpg";

			if( $_auth[ "useFeaturedObj" ] != $objData[ "objid" ])
			{
				$_auth[ "useFeaturedObj" ] = $objData[ "objid" ];
				$featChanged = true;
			}
		}
		else
		{
			$featImageName = "";
			$featImageName2 = "";
			$featChanged = true;
			$_auth[ "useFeaturedObj" ] = 0;
		}

		mysql_free_result( $objResult );

		// Make thumbnails for ID & Featured work.

		foreach( array(
			array(
				"name" => "",
				"path" => "files/ids/",
				"res" => $_config[ "idResolution" ],
				"allowSWF" => true,
				"imgname" => $idImageName,
				"imgname2" => "" ),
			array(
				"name" => "useFeaturedObj",
				"path" => "files/features/",
				"res" => $_config[ "featureResolution" ],
				"allowSWF" => false,
				"imgname" => $featImageName,
				"imgname2" => $featImageName2 )) as $item )
		{
			if( $item[ "name" ] == "useFeaturedObj" && !$featChanged )
			{
				continue; // Item did not change.
			}
			
			if( $item[ "name" ] == "" && $idError == _UPL_NO_FILE && !isset( $_POST[ "removeId" ]))
			  {
			    continue; // Item did not change.
			  }
			
			$fileName = applyIdToPath( $item[ "path" ], $_auth[ "useid" ]);
			
			$oldFiles = glob( $fileName."-*", GLOB_NOESCAPE );

			if( is_array( $oldFiles ) && count( $oldFiles ) > 0 )
			{
				foreach( $oldFiles as $oldFile )
			    		{
						unlink( $oldFile ); // Delete old files
					}
			}
			
			if( ( $item[ "imgname" ] == "" || !file_exists( $item[ "imgname" ] ) ) )
			  {
			    continue;
			  }

			if( $item[ "name" ] == "" && isset( $_POST[ "removeId" ] ) )
			{
				// Just remove - do not upload a new image
				continue;
			}

			$imageInfo = getimagesize( $item[ "imgname" ]);

			if( $item[ "allowSWF" ] && ( $imageInfo[ 2 ] == 4 || $imageInfo[ 2 ] == 13 )) // SWF/SWC
			{
				$useSWF = true;
				$newName = $fileName."-".time().".swf";
			}
			else
			{
				$useSWF = false;
				$newName = $fileName."-".time().".jpg";
			}

			if( $item[ "imgname" ] != "" )
			{
				$imageName = $item[ "imgname" ];

				list( $thumbMaxWidth, $thumbMaxHeight ) = preg_split( '/x/', $item[ "res" ]);

				if( $useSWF )
				{
					$filesize = filesize( $imageName );

					if( $filesize > $_config[ "idSWFMaxSize" ])
					{
						notice( sprintf( "Your Flash animation is %d bytes. Maximum is %d bytes.",
							$filesize, $_config[ "idSWFMaxSize" ]));
					}
					else
					{
						move_uploaded_file( $imageName, $newName );
						chmod( $newName, 0644 );
					}
				}
				else
				{
					if( !thumbifyImage( $imageName, $newName, $thumbMaxWidth, $thumbMaxHeight, 86 ))
					{
						// If it's impossible to read the original image (e.g. it's a text file
						// or a Flash animation), then make the featured image from the thumbnail.

						$imageName = $item[ "imgname2" ];

						thumbifyImage( $imageName, $newName, $thumbMaxWidth, $thumbMaxHeight, 86 );
					}
				}
			}
		}

		// Profile options

		$_auth[ "useCustomTitle" ] = $_POST[ "useCustomTitle" ];

		$_auth[ "useRealName" ] = $_POST[ "useRealName" ];

		$_auth[ "useShowRealName" ] = isset( $_POST[ "useShowRealName" ]) ? 1 : 0;

		$_auth[ "useProfile" ] = $_POST[ "useProfile" ];

		$_auth[ "useAIM" ] = $_POST[ "useAIM" ];
		$_auth[ "useICQ" ] = $_POST[ "useICQ" ];
		$_auth[ "useMSN" ] = $_POST[ "useMSN" ];
		$_auth[ "useYIM" ] = $_POST[ "useYIM" ];
		$_auth[ "useJabber" ] = $_POST[ "useJabber" ];

		// Update the database

		sql_query( "UPDATE `users`".dbSet( array(
			"useRealName" => $_auth[ "useRealName" ],
			"useShowRealName" => $_auth[ "useShowRealName" ],
			"useIsHidden" => $_auth[ "useIsHidden" ])).dbWhere( array(
			"useid" => $_auth[ "useid" ])));

		sql_query( "UPDATE `useExtData`".dbSet( array(
			"useAIM" => $_auth[ "useAIM" ],
			"useICQ" => $_auth[ "useICQ" ],
			"useMSN" => $_auth[ "useMSN" ],
			"useYIM" => $_auth[ "useYIM" ],
			"useJabber" => $_auth[ "useJabber" ],
			"useCustomTitle" => $_auth[ "useCustomTitle" ],
			"useFeaturedObj" => $_auth[ "useFeaturedObj" ],
			"useProfile" => $_auth[ "useProfile" ],
			"useShowEmail" => $_auth[ "useShowEmail" ],
			"useStatsPublic" => $_auth[ "useStatsPublic" ],
			"useGuestAccess" => $_auth[ "useGuestAccess" ])).dbWhere( array(
			"useEid" => $_auth[ "useid" ])));

		if( $oldGuestAccess != $_auth[ "useGuestAccess" ])
		{
			sql_query( "UPDATE `objects`".dbSet( array(
				"objGuestAccess" => $_auth[ "useGuestAccess" ])).dbWhere( array(
				"objCreator" => $_auth[ "useid" ])));
		}

		notice( _SET_SAVED );
	}

	// ======================================================================================================
	// ACCOUNT
	// ======================================================================================================

	?>
	<? iefixStart() ?>
	<div class="sep largetext"><?= _ACCOUNT ?></div>
	<div class="container2 notsowide">
		<table cellspacing="0" cellpadding="4" border="0">
		<tr>
			<td align="right" class="nowrap"><?= _PASSWORD ?>:</td>
			<td><?

				$url = url( "password/".$_auth[ "useActivationKey" ]);

				?>
				# # # # #
				&nbsp;<a href="<?= $url ?>"><?= _CHANGE ?></a>
			</td>
		</tr>
		<tr>
			<td align="right" class="nowrap"><?= _EMAIL_ADDRESS ?>:</td>
			<td>
				<?

				$url = url( "emailchange/".strtolower( $_auth[ "useUsername" ]),
					array( "key" => $_auth[ "useActivationKey" ]));

				?>
				<b><?= $_auth[ "useEmail" ]?></b>
				&nbsp;<a href="<?= $url ?>"><?= _CHANGE ?></a>
			</td>
		</tr>
		<tr>
			<td align="right">
				<input <?= $_auth[ "useShowEmail" ] ? 'checked="checked"' : "" ?> class="checkbox"
					id="idShowEmail" name="useShowEmail" type="checkbox" />
			</td>
			<td>
				<label for="idShowEmail"><?= _SET_SHOW_EMAIL ?></label>
			</td>
		</tr>
		<tr>
			<td align="right">
				<input <?= $_auth[ "useIsHidden" ] ? 'checked="checked"' : "" ?> class="checkbox"
					id="idIsHidden" name="useIsHidden" type="checkbox" />
			</td>
			<td>
				<label for="idIsHidden"><?= _SET_HIDDEN ?></label>
			</td>
		</tr>
		<tr>
			<td align="right">
				<input <?= $_auth[ "useStatsPublic" ] ? 'checked="checked"' : "" ?> class="checkbox"
					id="idStatsPublic" name="useStatsPublic" type="checkbox" />
			</td>
			<td>
				<label for="idStatsPublic"><?=_SET_STATS_PUBLIC ?></label>
			</td>
		</tr>
		<tr>
			<td align="right">
				<input <?= $_auth[ "useGuestAccess" ] ? 'checked="checked"' : "" ?> class="checkbox"
					id="idGuestAccess" name="useGuestAccess" type="checkbox" />
			</td>
			<td>
				<label for="idGuestAccess"><?=_SET_GUEST_ACCESS ?></label>
			</td>
		</tr>
		</table>
	</div>
	<? iefixEnd() ?>
	<?

	// ======================================================================================================
	// PROFILE
	// ======================================================================================================

	?>
	<? iefixStart() ?>
	<div class="sep largetext"><?= _PROFILE ?></div>
	<div class="container2 notsowide">
		<table cellspacing="0" cellpadding="4" border="0" width="100%">
		<tr>
			<td align="right" class="nowrap"><?= _AVATAR ?>:</td>
			<td width="100%">
				<div id="avatar_current">
					<?= getUserAvatar( "", $_auth[ "useid" ]) ?>
					<?

					$script = "make_invisible('avatar_current'); ".
						"make_visible('avatar_hint'); ".
						"make_visible('avatar_choose'); ".
						"return false;";

					?>
					&nbsp;<a href="" onclick="<?= $script ?>">
						<?= getIMG( urlf()."images/emoticons/edit.png", "" )." "._CHANGE ?></a>
				</div>
				<div id="avatar_choose" style="display: none">
					<?

					$script = "make_visible('preview_container'); ".
						"make_invisible('preview_hint'); ".
						"show_preview_image('preview', 'preview_message', this.value);";

					?>
					<input accept="image/gif, image/jpeg, image/png" onchange="<?= $script ?>"
						name="avatar" size="50" type="file" />
				</div>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<div style="display: none" id="avatar_hint">
					<li><?= sprintf( _SET_AVATAR_TOO_LARGE,
						$_config[ "minAvatarResolution" ],
						$_config[ "maxAvatarResolution" ]) ?></li>
					<li><?= sprintf( _SET_AVATAR_SIZE_EXCEEDED,
						$_config[ "maxAvatarSize" ]) ?></li>
				</div>
				<div style="display: none" id="preview_container">
					<div class="sep caption"><?= _PREVIEW ?>:</div>
					<div class="a_center">
						<img alt="preview" style="display: none" id="preview" src="" />
						<div id="preview_message"><?= _SUBMIT_SELECT_FILE ?></div>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td align="right" class="nowrap"><?= _FEATURE ?>:</td>
			<td>
				<select name="useFeaturedObj">
					<option value="0">( <?= _NONE ?> )</option>
					<?

					$objResult = sql_query( "SELECT * FROM `objects`".dbWhere( array(
						"objCreator" => $_auth[ "useid" ],
						"objPending" => 0,
						"objDeleted" => 0 ))."ORDER BY `objTitle`" );

					while( $objData = mysql_fetch_assoc( $objResult ))
					{
						?>
						<option <?= $objData[ "objid" ] == $_auth[ "useFeaturedObj" ] ? ' selected="selected"' : "" ?>
							value="<?= $objData[ "objid" ] ?>">
							<?= substr( strip_tags( formatText( $objData[ "objTitle" ])), 0, 40 ) ?></option>
						<?
					}

					mysql_free_result( $objResult );

					?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="right" class="nowrap"><?= _SET_CUSTOM_TITLE ?>:</td>
			<td>
				<input type="text" name="useCustomTitle" size="50"
					value="<?= htmlspecialchars( $_auth[ "useCustomTitle" ]) ?>" />
			</td>
		</tr>
		<tr>
			<td align="right" class="nowrap"><?= _REAL_NAME ?>:</td>
			<td>
				<input type="text" name="useRealName" size="50"
					value="<?= htmlspecialchars( $_auth[ "useRealName" ]) ?>" />
			</td>
		</tr>
		<tr>
			<td align="right">
				<input <?= $_auth[ "useShowRealName" ] ? 'checked="checked"' : "" ?> class="checkbox"
					id="idShowRealName" name="useShowRealName" type="checkbox" />
			</td>
			<td>
				<label for="idShowRealName"><?=_SET_SHOW_REAL_NAME ?></label>
			</td>
		</tr>
		<tr>
			<td align="right" class="nowrap"><?= _ID ?>:</td>
			<td width="100%">
				<div id="id_current">
					<?

					$filename = findNewestFileById( "files/ids/", $_auth[ "useid" ],
						"images/nothumb.gif" );

					$imageInfo = getimagesize( $filename );

					if( $imageInfo[ 2 ] == 4 || $imageInfo[ 2 ] == 13 ) // SWF/SWC
					{
						echo getSWF( $filename, 1, 150, 150 );
					}
					else
					{
						$width = round( $imageInfo[ 0 ] * 0.25 );
						$height = round( $imageInfo[ 1 ] * 0.25 );

						makeFloatingThumb( "", url().$filename, $imageInfo[ 0 ], $imageInfo[ 1 ],
							0, false, $onmouseover, $onmouseout );

						echo getIMG( urlf().$filename, 'style="width: '.$width.'px; height: '.$height.'px" '.
							'onmouseover="'.$onmouseover.'" onmouseout="'.$onmouseout.'"', true );
					}

					$script = "make_invisible('id_current'); ".
						"make_visible('id_hint'); ".
						"make_visible('id_choose'); ".
						"return false;";

					?>
					&nbsp;<a href="" onclick="<?= $script ?>">
						<?= getIMG( urlf()."images/emoticons/edit.png", "" )." "._CHANGE ?></a>
					&nbsp;<input type="checkbox" class="checkbox" name="removeId" id="idRemoveID" />
						<label for="idRemoveID"><?= _REMOVE ?></label>
				</div>
				<div id="id_choose" style="display: none">
					<?

					$script = "make_visible('id_preview_container'); ".
						"make_invisible('id_preview_hint'); ".
						"show_preview_image('id_preview', 'id_preview_message', this.value);";

					?>
					<input accept="image/gif, image/jpeg, image/png" onchange="<?= $script ?>"
						name="id" size="50" type="file" />
				</div>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<div style="display: none" id="id_hint">
					<?= sprintf( _SET_ID_FILE_EXPLAIN, $_config[ "idSWFMaxSize" ]) ?>
				</div>
				<div style="display: none" id="id_preview_container">
					<div class="sep caption"><?= _PREVIEW ?>:</div>
					<div class="a_center">
						<img alt="preview" style="display: none" id="id_preview" src="" />
						<div id="id_preview_message"><?= _SUBMIT_SELECT_FILE ?></div>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td valign="top" align="right" class="nowrap">
				<div><?= _PROFILE ?>:</div>
				<?

				$script = "make_invisible('profile_current'); ".
					"make_invisible('profile_edit'); ".
					"make_visible('profile_change'); ".
					"return false;";

				$profileEdit = '<div id="profile_edit">'.
					'(<a href="" onclick="'.$script.'">'._EDIT.'</a>)</div>';

				if( trim( $_auth[ "useProfile" ]) != "" )
				{
					echo $profileEdit;
				}

				?>
			</td>
			<td>
				<div id="profile_current">
					<?

					if( trim( $_auth[ "useProfile" ]) != "" )
					{
						?>
						<div class="container2"><?= formatText( $_auth[ "useProfile" ]) ?></div>
						<?
					}
					else
					{
						echo $profileEdit;
					}

					?>
				</div>
				<div id="profile_change" style="display: none">
					<div>
						<?

						iefixStart();

						$commentName = "useProfile";
						$commentDefault = $_auth[ "useProfile" ];
						$commentNoOptions = true;
						$commentRows = 12;

						include( INCLUDES."mod_comment.php" );

						iefixEnd();

						?>
						<div class="clear">&nbsp;</div>
					</div>
				</div>
			</td>
		</tr>
		</table>
	</div>
	<? iefixEnd() ?>
	<?

	// ======================================================================================================
	// MESSENGERS
	// ======================================================================================================

	?>
	<? iefixStart() ?>
	<div class="sep largetext"><?= _SET_MESSENGERS ?></div>
	<div class="container2 notsowide">
		<table cellspacing="0" cellpadding="4" border="0">
		<tr>
			<td width="20px"><td>
			<td align="right" class="nowrap"><?= _SET_CONTACTS_AIM ?>:</td>
			<td>
				<input type="text" name="useAIM" size="25"
					value="<?= htmlspecialchars( $_auth[ "useAIM" ]) ?>" />
			</td>
			<td width="20px"><td>
			<td align="right" class="nowrap"><?= _SET_CONTACTS_MSN ?>:</td>
			<td>
				<input type="text" name="useMSN" size="25"
					value="<?= htmlspecialchars( $_auth[ "useMSN" ]) ?>" />
			</td>
		</tr>
		<tr>
			<td width="20px"><td>
			<td align="right" class="nowrap"><?= _SET_CONTACTS_ICQ ?>:</td>
			<td>
				<input type="text" name="useICQ" size="25"
					value="<?= htmlspecialchars( $_auth[ "useICQ" ]) ?>" />
			</td>
			<td width="20px"><td>
			<td align="right" class="nowrap"><?= _SET_CONTACTS_YIM ?>:</td>
			<td>
				<input type="text" name="useYIM" size="25"
					value="<?= htmlspecialchars( $_auth[ "useYIM" ]) ?>" />
			</td>
		</tr>
		<tr>
			<td width="20px"><td>
			<td align="right" class="nowrap"><?= _SET_CONTACTS_JABBER ?>:</td>
			<td>
				<input type="text" name="useJabber" size="25"
					value="<?= htmlspecialchars( $_auth[ "useJabber" ]) ?>" />
			</td>
			<td width="20px"><td>
			<td></td>
			<td></td>
		</tr>
		</table>
	</div>
	<? iefixEnd() ?>
	<?

	// ======================================================================================================

	?>
	<div class="sep">
		<button class="submit" name="submit" type="submit">
			<?= getIMG( url()."images/emoticons/checked.png" ) ?>
			<?= _SAVE_CHANGES ?>
		</button>
	</div>
	</form>
</div>
