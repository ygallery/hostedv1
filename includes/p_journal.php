<?

if( $_cmd[ 1 ] == "" )
{
	if( isLoggedIn() )
	{
		redirect( url( "journal/".strtolower( $_auth[ "useUsername" ])));
	}

	$_cmd[ 2 ] = $_cmd[ 1 ];
	$_cmd[ 1 ] = strtolower( $_auth[ "useUsername" ]);
}

$journalAction = isset( $_GET[ "action" ]) ? $_GET[ "action" ] : "";

$whereGuest = isLoggedIn() ? "" : ( " AND `useGuestAccess` = '1' " );

$result = sql_query( "SELECT * FROM `users`, `useExtData`".dbWhere( array(
	"useUsername" => $_cmd[ 1 ],
	"useEid*" => "useid" ))."$whereGuest LIMIT 1" );

if( !$useData = mysql_fetch_assoc( $result ))
{
	include( INCLUDES."p_notfound.php" );
	return;
}

$useUsername = strtolower( $useData[ "useUsername" ]);

$jouid = $_cmd[ 2 ] != "" ? intval( $_cmd[ 2 ]) : 0;

// TODO: Separate to pages if there are too many entries.

$entries = array();

$offset = isset( $_GET[ "journalOffset" ]) ? intval( $_GET[ "journalOffset" ]) : 0;
$limit = 24;

$result = sql_query( "SELECT SQL_CALC_FOUND_ROWS * FROM `journals`".dbWhere( array(
	"jouCreatorType" => "use",
	"jouCreator" => $useData[ "useid" ])).
	"ORDER BY `jouSubmitDate` DESC LIMIT $offset, $limit" );

$cntResult = sql_query( "SELECT FOUND_ROWS()" );

$totalCount = mysql_result( $cntResult, 0 );

while( $jouData = mysql_fetch_assoc( $result ))
{
	$entries[] = $jouData;
}

if( $jouid == 0 ) // No journal entry id specified?
{
	// Pick the latest journal entry from this user.

	$jouResult = sql_query( "SELECT `jouid` FROM `journals`".dbWhere( array(
		"jouCreatorType" => "use",
		"jouCreator" => $useData[ "useid" ])).
		"ORDER BY `jouSubmitDate` DESC LIMIT 1" );

	if( $jouData = mysql_fetch_assoc( $jouResult ))
	{
		$jouid = $jouData[ "jouid" ];
	}

	mysql_free_result( $jouResult );
}

if( $jouid > 0 )
{
	// Make sure the specified journal entry exists and belongs to the correct user.

	$jouResult = sql_query( "SELECT * FROM `journals`".dbWhere( array(
		"jouCreatorType" => "use",
		"jouCreator" => $useData[ "useid" ],
		"jouid" => $jouid ))."LIMIT 1" );

	if( !mysql_num_rows( $jouResult ))
	{
		include( INCLUDES."p_notfound.php" );
		return;
	}

	$currentEntry = mysql_fetch_assoc( $jouResult );

	mysql_free_result( $jouResult );
}

// Only the journal's owner can do special actions (post/edit/delete).

if( $_auth["useid"] != $useData[ "useid" ] && $journalAction != "" && !atLeastSModerator() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

$_pollUser = $useData[ "useid" ];

$_documentTitle = $useData[ "useUsername" ].": "._JOURNAL;

?>
<div class="header">
	<div class="f_right mar_left a_center normaltext">
		<?= getUserAvatar( "",$useData[ "useid" ], true ) ?>
	</div>
	<div class="f_left header_title">
		<?= $useData[ "useUsername" ]?>
		<div class="subheader"><?= _JOURNAL ?></div>
	</div>
	<?

	$active = 7;
	include( INCLUDES."mod_usermenu.php" );

	?>
</div>

<div class="container">
	<? iefixStart() ?>

	<div class="f_left leftside">
		<div class="header">
			<?

			switch( $journalAction )
			{
				case "post":
				{
					echo _JOURNAL_NEW_ENTRY;
					break;
				}

				case "edit":
				{
					echo _JOURNAL_EDIT_ENTRY;
					break;
				}

				case "delete":
				{
					echo _JOURNAL_DELETE_ENTRY;
					break;
				}

				default:
				{
					echo _JOURNAL;

					// Also the New Entry button:

					if( $_auth[ "useid" ] == $useData[ "useid" ])
					{
						$url = url( ".", array( "action" => "post" ));

						?>
						&nbsp; &nbsp;
						<div class="button normaltext"
							style="float: none; display: inline; margin-right: -8px"
							onclick="document.location='<?= $url ?>'">
							<a href="<?= $url ?>">
							<?= getIMG( url()."images/emoticons/journal.png" ) ?>
							<?= _JOURNAL_NEW_ENTRY ?>
							</a>
						</div>
						<?
					}
				}
			}

			?>
		</div>
	</div>

	<div class="f_right a_right">
		<div class="header">
			<?

			switch( $journalAction )
			{
				case "post":
				case "edit":
				case "delete":
				{
					echo _JOURNAL_PAST_ENTRIES;
					break;
				}

				default:
				{
					if( $jouid > 0 )
					{
						echo isset( $_GET[ "replied" ]) ? _COMMENTS :
							_JOURNAL_LEAVE_COMMENT;
					}
				}
			}

			?>
		</div>
	</div>

	<div class="clear"><br /></div>
	<? iefixEnd() ?>

	<div class="leftside">
		<?

		$ableToPost = ( $_auth[ "useid" ] == $useData[ "useid" ]);

		if( $ableToPost && isset( $_POST[ "postNewEntry" ]))
		{
			sql_query( "INSERT INTO `journals`".dbValues( array(
				"jouCreatorType" => "use",
				"jouCreator" => $_auth[ "useid" ],
				"jouSubmitIp" => getHexIp( $_SERVER[ "REMOTE_ADDR" ]),
				"jouSubmitDate!" => "NOW()",
				"jouLastEdit!" => "NOW()",
				"jouTitle" => $_POST[ "title" ] != "" ? $_POST[ "title" ] :
					_JOURNAL_DEFAULT_TITLE,
				"jouEntry" => substr( $_POST[ "entry" ], 0, 50000 ),
				"jouNoEmoticons" => isset( $_POST[ "entryNoEmoticons" ]) ? 1 : 0,
				"jouNoSig" => isset( $_POST[ "entryNoSig" ]) ? 1 : 0,
				"jouNoBBCode" => isset( $_POST[ "entryNoBBCode" ]) ? 1 : 0 )));

			$jouidNew = mysql_insert_id(); // Get the id of the newly added entry.

			// Notify watchers about the journal update.

			addUpdateToWatchers( updTypeJournal, $_auth[ "useid" ], $jouidNew );

			redirect( url( "journal/".$useUsername ));
		}

		if(( $ableToPost || atLeastSModerator() ) && isset( $_POST[ "editOldEntry" ]))
		{
			sql_query( "UPDATE `journals`".dbSet( array(
				"jouLastEdit!" => "NOW()",
				"jouEditIp" => getHexIp( $_SERVER[ "REMOTE_ADDR" ]),
				"jouTitle" => $_POST[ "title" ] != "" ? $_POST[ "title" ] :
					_JOURNAL_DEFAULT_TITLE,
				"jouEntry" => substr( $_POST[ "entry" ], 0, 50000 ),
				"jouNoEmoticons" => isset( $_POST[ "entryNoEmoticons" ]) ? 1 : 0,
				"jouNoSig" => isset( $_POST[ "entryNoSig" ]) ? 1 : 0,
				"jouNoBBCode" => isset( $_POST[ "entryNoBBCode" ]) ? 1 : 0 )).
				dbWhere( array ( "jouid" => $jouid ))."LIMIT 1" );

			// Notify watchers about the journal update.

			if( $_auth[ "useid" ] == $useData[ "useid" ])
			{
				addUpdateToWatchers( updTypeJournal, $_auth[ "useid" ], $jouid );
			}

			redirect( url( "journal/".$useUsername."/".$jouid ));
		}

		if(( $ableToPost || atLeastSModerator() ) && $journalAction == "delete" )
		{
			sql_query( "DELETE FROM `journals`".dbWhere( array(
				"jouid" => $jouid ))."LIMIT 1" );

			sql_query( "DELETE FROM `updates`".dbWhere( array(
				"updType" => updTypeJournal,
				"updObj" => $jouid )));

			redirect( url( "journal/".$useUsername ));
		}

		if(( $ableToPost || atLeastSModerator() ) &&
			( $journalAction == "post" || $journalAction == "edit" ))
		{
			?>
			<div class="mar_bottom">
				<form action="<?= url(".") ?>" method="post">
				<div class="caption"><?= _TITLE ?>:</div>
				<div><input class="notsowide largetext" name="title" type="text"
					<?= $journalAction == "edit" ?
						'value="'.htmlspecialchars( $currentEntry[ "jouTitle" ]).'"' : "" ?> /></div>
				<div class="sep">
					<?

					$commentRows = 18;
					$commentWide = false;
					$commentName = "entry";
					$commentDefault = "";

					if( $journalAction == "edit" )
					{
						$commentDefault = $currentEntry["jouEntry"];

						if( $currentEntry[ "jouNoEmoticons" ])
						{
							$commentNoEmoticons = true;
						}

						if( $currentEntry[ "jouNoSig" ])
						{
							$commentNoSig = true;
						}

						if( $currentEntry[ "jouNoBBCode" ])
						{
							$commentNoBBCode = true;
						}
					}

					include( INCLUDES."mod_comment.php" );

					?>
				</div>
				<div class="sep">
					<button class="submit"
						name="<?= $journalAction == "edit" ? "editOldEntry" : "postNewEntry" ?>"
						<?= !$commentId ? 'disabled="disabled"' : "" ?>
						onclick="el=get_by_id('<?= $commentId ?>'); if(!el.value){ alert('<?= _BLANK_COMMENT ?>'); return false } else return true"
						type="submit">
						<?= getIMG( url()."images/emoticons/checked.png" ) ?>
						<?= $journalAction == "edit" ? _SAVE_CHANGES : _JOURNAL_POST_ENTRY ?>
					</button>
				</div>
				</form>
			</div>
			<?
		}

		if( $journalAction == "" && isset( $currentEntry ))
		{
			?>
			<div class="mar_bottom" style="margin-top: -8px">
			<?

			include_once( INCLUDES."comments.php" );

			$jouData = $currentEntry;
			$comData = array();
			$comData["comid"] = 0;
			$comData["comCreator"] = $jouData["jouCreator"];
			$comData["comComment"] = $jouData["jouEntry"];
			$comData["comSubject"] = $jouData["jouTitle"];
			$comData["comSubmitDate"] = $jouData["jouSubmitDate"];
			$comData["comSubmitIp"] = $jouData["jouSubmitIp"];
			$comData["comLastEdit"] = $jouData["jouLastEdit"];
			$comData["comEditIp"] = $jouData["jouEditIp"];
			$comData["comNoEmoticons"] = $jouData["jouNoEmoticons"];
			$comData["comNoSig"] = $jouData["jouNoSig"];
			$comData["comNoBBCode"] = $jouData["jouNoBBCode"];
			$comData["comAllowImage"] = true;

			showComment( $comData, 0 );

			?>
			</div>
			<?

			if( $_auth[ "useid" ] == $currentEntry[ "jouCreator" ] || atLeastSModerator() )
			{
				$urlEdit = url( ".", array( "action" => "edit" ));
				$urlDelete = url( ".", array( "action" => "delete" ));

				?>
				<div class="mar_bottom" style="margin-top: -8px">
					<? iefixStart() ?>
					<div class="button" style="float: right"
						onclick="if(confirm('<?= _ARE_YOU_SURE ?>')) document.location='<?= $urlDelete ?>';">
						<a href="<?= $urlDelete ?>" onclick="return false;">
						<?= getIMG( url()."images/emoticons/delete.png" ) ?>
						<?= _DELETE ?>
						</a>
					</div>
					<div class="button" style="float: right" onclick="document.location='<?= $urlEdit ?>';">
						<a href="<?= $urlEdit ?>">
						<?= getIMG( url()."images/emoticons/edit.png" ) ?>
						<?= _EDIT ?>
						</a>
					</div>
					<div class="clear">&nbsp;</div>
					<? iefixEnd() ?>
				</div>
				<?
			}
		}

		if( $journalAction != "" )
		{
			// if editing/posting, switch to the right side here
			// and show the past entries there
			?>
			</div>
			<div class="rightside" style="margin-top: -1px">
			<?
		}
		else
		{
			?>
			<div class="header"><?= _JOURNAL_PAST_ENTRIES ?></div>
			<?
		}

		?>
		<div class="container2 mar_bottom">
			<?

			iefixStart();

			$found = false;

			foreach( $entries as $entry )
			{
				iefixStart();

				?>
				<div class="f_left mar_bottom">
					<a href="<?= url( "journal/".$useUsername."/".$entry[ "jouid" ])?>">
					<?= getIMG( url()."images/emoticons/journal.png" ) ?>
					<?= formatText( $entry[ "jouTitle" ]) ?>
					</a>
				</div>
				<div class="f_right mar_bottom">
					<?= gmdate( $_auth["useDateFormat"],
						applyTimezone( strtotime( $entry[ "jouSubmitDate" ]))) ?>
				</div>
				<div class="clear">&nbsp;</div>
				<?

				iefixEnd();

				$found = true;
			}

			if( !$found )
			{
				echo _JOURNAL_NONE_FOUND;
			}

			iefixEnd();

			?>
		</div>
		<div class="clear"><br /></div>
		<?

		if( $found )
		{
			navControls( $offset, $limit, $totalCount, "", "", "", "", "journalOffset" );

			?><div class="clear"><br /></div><?
		}

		?>
	</div>
	<?

	if( $journalAction == "" )
	{
		?>
		<div class="rightside" style="margin-top: -1px">
		<?

		if( isset( $currentEntry ))
		{
			unset( $commentRows );
			unset( $commentWide );
			unset( $commentName );

			include_once( INCLUDES."comments.php" );

			showAllComments( $currentEntry[ "jouid" ], "jou",
				false, true, true, false );
		}

		?>
		</div>
		<?
	}

	?>
	<div class="clear">&nbsp;</div>
</div>
