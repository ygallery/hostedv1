<?

switch( $_cmd[ 1 ])
{
	case "folders":
	{
		include( INCLUDES."p_folders.php" );
		break;
	}

	case "interaction":
	{
		include( INCLUDES."p_interaction.php" );
		break;
	}

	case "profile":
	{
		include( INCLUDES."p_profile.php" );
		break;
	}

	case "sidebar":
	{
		include( INCLUDES."p_sidebar.php" );
		break;
	}

	case "site":
	{
		include( INCLUDES."p_site.php" );
		break;
	}

	default:
	{
		redirect( url( "settings/site" ));
	}
}

?>