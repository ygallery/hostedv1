<?

$_documentTitle = _BROWSE.": "._DELETED_SUBMISSIONS;

if( !atLeastModerator() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

?>
<div class="header">
	<div class="header_title">
		<?= _BROWSE ?>
		<div class="subheader"><?= _DELETED_SUBMISSIONS ?></div>
	</div>	
	<?

	$active = 6;
	include( INCLUDES."mod_browsemenu.php" );

	?>
</div>

<div class="container">
	<?

	include_once( INCLUDES."gallery.php" );

	showThumbnails( array(
		"quickSearch" => true,
		"where" => "`objPending` = '1' OR `objDeleted` = '1'",
		"showDeleted" => true ));

	?>
</div>
