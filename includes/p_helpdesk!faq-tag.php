<?

if( isset( $_cmd[ 3 ]) && !isset( $_GET[ "tag" ]))
{
	$_GET[ "tag" ] = $_cmd[ 3 ];
}

if( !isset( $_GET[ "tag" ]))
{
	redirect( url( "helpdesk/faq" ));
}

$tagName = $_GET[ "tag" ];

?>
<div class="header">
	<?= $tagName ?>
	<div class="subheader">
		All articles tagged with '<?= $tagName ?>'
	</div>
</div>
<ul>
<?

sql_where( array(
	"hftName" => $tagName,
	"hfmTag*" => "hftid",
	"hfmArticle*" => "hfqid",
	"hfqCategory*" => "hfcid" ));

$result = sql_rowset( "helpdeskFAQ, helpdeskFAQCats, helpdeskFAQTagMap, helpdeskFAQTags" );

if( sql_num_rows( $result ) == 0 )
{
	?>--<?
}
else
{
	while( $data = sql_next( $result ))
	{
		$url = url( "helpdesk/faq/".$data[ "hfcIdent" ]."/".$data[ "hfqIdent" ]);
		$title = htmlspecialchars( preg_replace( '/^.*\|/', "", $data[ "hfqTitle" ]));

		?><li><a href="<?= $url ?>"><?= $title ?></a></li><?
	}
}

sql_free( $result );

?>
</ul>
<div style="margin-left : 0.5em; padding-top : 0.7em;">
	<form action="<?= url( "helpdesk/faq" ) ?>" method="get">
	<button class="smalltext">
		<?= getIMG( url()."images/emoticons/nav-prev.png" )?>
		Return to FAQ
	</button>
	</form>
</div>
