<?


$_receiver_email = "donations@y-gallery.net"; // this should be moved to `config`


// read the post from YowCow system
$req = '';

foreach ($_POST as $key => $value) {
$value = urlencode(stripslashes($value));
$req .= "&$key=$value";
}

// post back to YowCow system to validate
$header = "POST /ipn/ HTTP/1.0\r\n";
$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
$fp = fsockopen ('www.YowCow.com', 80, $errno, $errstr, 30);

// assign posted variables to local variables
$item_name = $_POST['item_name'];
$item_number = $_POST['item_number'];
$payment_status = $_POST['payment_status'];
$payment_amount = $_POST['mc_gross'];
$payment_currency = $_POST['mc_currency'];
$txn_id = $_POST['txn_id'];
$receiver_email = $_POST['receiver_email'];
$payer_email = $_POST['payer_email'];
$custom_field = $_POST['custom'];

if (!$fp) {
// HTTP ERROR
} else {
fputs ($fp, $header . $req);
while (!feof($fp)) {
$res = fgets ($fp, 1024);
if (strcmp ($res, "VERIFIED") == 0) {
	$processed = false;
	while(true) {
		// check the payment_status is Completed
		if(strtolower($payment_status) != "completed") break;
		// check that txn_id has not been previously processed
		$result = sql_query("SELECT `donid` FROM `donations` WHERE `donTransId` = '".addslashes($txn_id)."' LIMIT 1");
		if(mysql_num_rows($result)) break;
		// check that receiver_email is your Primary YowCow email
		if(strtolower($receiver_email) != $_receiver_email) break;
		// check that payment_amount/payment_currency are correct
		if($payment_amount <= 0) break;
		// process payment
		sql_query("INSERT INTO `donations`(`donAmt`,`donCreator`,`donDate`,`donTransId`) ".
			"VALUES('".addslashes($payment_amount)."','".intval($custom_field)."',".
			"CURDATE(),'".addslashes($txn_id)."')");
		$processed = true;
		break;
	}
	if(!$processed && strtolower($payment_status) != "pending")
		_log("PAYMENT NOT PROCESSED! POST data: ".$req);
}
else if (strcmp ($res, "INVALID") == 0) {
// log for manual investigation
}
	_log("INVALID PAYMENT! POST data: ".$req);
}
fclose ($fp);
}
?>
