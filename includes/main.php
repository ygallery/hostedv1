<?

// -----------------------------------------------------------------------------
// Set the default timezone (required by PHP5+).

if( function_exists( "date_default_timezone_set" ))
{
	date_default_timezone_set( "Etc/GMT" );
#	date_default_timezone_set( "UTC/0.0/no DST" );
}

// -----------------------------------------------------------------------------
// All global variables have the prefix $_.
//

$_commentsPerPage = 9;

$_stats = array();
$_stats[ "startTime" ] = gettimeofday();

// Global variable $_mod_rewrite defines whether mod_rewrite is used or not.
// We use mod_rewrite to create virtual subfolders such as /edit/, /view/ etc
// and make them all lead to the root /index.php. In short words, with
// $_mod_rewrite turned off, the URLs would look like /?page=view&par1=123
// instead of /view/123/.

//$_mod_rewrite = false;
$_mod_rewrite = true;

// -----------------------------------------------------------------------------
// Global variable $_lang contains the currently chosen language's short
// identifier ("en" by default, for English).
//
// We have to use $_COOKIE[ "yGalLanguage" ] because we have no access to the
// database at this point, yet we have to include strings_??.php.

if( isset( $_COOKIE[ "yGalLanguage" ]))
{
	$_lang = preg_replace( '/[^\w]/', "", $_COOKIE[ "yGalLanguage" ]);
}
elseif( isset( $_GET[ "yGalLanguage" ]))
{
	$_lang = preg_replace( '/[^\w]/', "", $_GET[ "yGalLanguage" ]);
}
else
{
	// Get the language from the first-level domain name, e.g. a new user from
	// "dsl.proxy.net.de" would get "de" language. If the language would not be
	// found, "en" will be used instead anyway.
	$_lang = "en";
// Slooow
//	$_lang = substr( strrchr( gethostbyaddr( $_SERVER[ "REMOTE_ADDR" ]), "." ), 1 );
}

if( !file_exists( INCLUDES."strings/".$_lang.".php" ))
	$_lang = "en";

// -----------------------------------------------------------------------------
// Oekaki editors.

define( "OEK_SHI", "1" );
define( "OEK_SHIPRO", "2" );
define( "OEK_PAINTBBS", "3" );
define( "OEK_OEKAKIBBS", "4" );

// -----------------------------------------------------------------------------
// Common includes.

include_once( INCLUDES."common.php" );
include_once( INCLUDES."errorhandler.php" );
include_once( INCLUDES."database.php" );
include_once( INCLUDES."sessions.php" );
include_once( INCLUDES."access.php" );

sql_init();

$_stats[ "startQueries" ] = preg_replace( '/.*'.preg_quote( "Questions: " ).
	'([0-9]+).*/', "\\1", mysql_stat() );

// -----------------------------------------------------------------------------
// Banner system.

if( $_cmd[ 0 ] == "banner" )
{
	$stmt = $dbh->prepare("SELECT * FROM banner
	WHERE id = ?");
	$stmt->bindParam(1, $_cmd[2], PDO::PARAM_INT);
	$stmt->execute();
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
	$banid = $result['id'];
	print_r($_cmd);
	
	if( !$banid )
	{
		exit;
	}

	switch( $_cmd[ 1 ])
	{
		case "goto":
		{
			$stmt = $dbh->prepare("UPDATE banner SET clicks = clicks + 1 WHERE id = ?");
			$stmt->bindParam(1, $banid, PDO::PARAM_INT);
			$stmt->execute();
			
			$stmt = $dbh->prepare("SELECT url FROM banner WHERE id = ?");
			$stmt->bindParam(1, $banid, PDO::PARAM_INT);
			$stmt->execute();
			$url = $stmt->fetch(PDO::FETCH_ASSOC)['url'];
			redirect($url);
		}

		default: // case "show":
		{			
			$stmt = $dbh->prepare("UPDATE banner SET shows = shows + 1 WHERE id = ?");
			$stmt->bindParam(1, $banid, PDO::PARAM_INT);
			$stmt->execute();
			
			header( "Content-type: image/gif" );
			readfile( "images/banners/".$banid.".gif" );
			exit;
		}
	}
}
// -----------------------------------------------------------------------------
// Enable proper caching of the pages in the user's browser.

if( !empty( $_SERVER[ "SERVER_SOFTWARE" ]) &&
	strstr( $_SERVER[ "SERVER_SOFTWARE" ], "Apache/2" ))
{
	header( "Cache-Control: no-cache, pre-check=0, post-check=0" );
}
else
{
	header( "Cache-Control: private, pre-check=0, post-check=0, max-age=0" );
}

header( "Expires: 0" );
header( "Pragma: no-cache" );

// -----------------------------------------------------------------------------
// Enable proper MIME-type output for browsers that can handle it

//if( isset( $_SERVER[ "HTTP_ACCEPT" ] ) && stristr( $_SERVER[ "HTTP_ACCEPT" ], "application/xhtml+xml" ) )
//{
//	header( "Content-Type: application/xhtml+xml; charset=UTF-8" );
//}
//else
//{
	header( "Content-type: text/html; charset=UTF-8" );
//}

// -----------------------------------------------------------------------------
// User authentification starts here.

$_auth = array(
	"useid" => 0,
	"useUsername" => "guest",
	"useDateFormat" => $_config[ "dateFormat" ],
	"useObjFilters" => $_config[ "nonRegFilters" ],
	"useObjPreview" => $_config[ "previewDefault" ],
	"useIsModerator" => 0,
	"useIsSModerator" => 0,
	"useIsDeveloper" => 0,
	"useIsHelpdesk" => 0,
	"useStatsHide" => 1,
	"useHideExtras" => 1,
	"useEnableUI2" => 1,
	"usePMBanner" => 1 );

if( $_cmd[ 0 ] == "" || $_cmd[ 0 ] == "browse" )
{
	$_auth[ "useObjFilters" ] = $_config[ "nonRegBrowseFilters" ];
}

if( $_sesid = checkSession() )
{
	$_tmpResult = sql_query( "SELECT * FROM `users`,`useExtData` ".
		"WHERE `useid` = `useEid` AND `useid` = '".$_sesid."' LIMIT 1" );

	if( mysql_num_rows( $_tmpResult ) > 0 )
		$_auth = mysql_fetch_assoc( $_tmpResult );

	sql_init();

	sql_query( "UPDATE LOW_PRIORITY `users` SET `useLastAction` = NOW() ".
		"WHERE `useid` = '".$_auth["useid"]."' LIMIT 1" );

	sql_query( "UPDATE LOW_PRIORITY `useExtData` SET `useLastIP` = '".
		getHexIp( $_SERVER[ "REMOTE_ADDR" ])."' ".
		"WHERE `useEid` = '".$_auth["useid"]."' LIMIT 1" );
}

if( atLeastSModerator() )
{
	$_config[ "readOnly" ] = 0;
}

if( !isUserMature() )
{
	$_auth[ "useObjFilters" ] .= ",".$_config[ "matureFilters" ];
}

if( $_config[ "extrasRatio" ] == "0:1" )
{
	$_auth[ "useHideExtras" ] = 1;
}

// Get rid of duplicate filters.

$_auth[ "useObjFilters" ] = implode( ",", array_unique( explode( ",", $_auth[ "useObjFilters" ])));

// -----------------------------------------------------------------------------
// Detect if the user is actually a searching bot (we only test against the most
// popular ones).

$_isSearchBot = false;

if( isset( $_SERVER[ "HTTP_USER_AGENT" ]))
{
	if( isset( $_GET[ "testSEO" ]) ||
		strpos( $_SERVER[ "HTTP_USER_AGENT" ], "Googlebot" ) !== false ||
		strpos( $_SERVER[ "HTTP_USER_AGENT" ], "Yahoo! Slurp" ) !== false ||
		strpos( $_SERVER[ "HTTP_USER_AGENT" ], "psbot" ) !== false ||
		strpos( $_SERVER[ "HTTP_USER_AGENT" ], "msnbot" ) !== false )
	{
		// Good bye!
		die();

		$_isSearchBot = true;
	}
}

if( !isLoggedIn() )
{
	// Store guest access info.

	if( 0 )
	{
		$userIp = getHexIp( $_SERVER[ "REMOTE_ADDR" ]);
		$bot = $_isSearchBot ? 1 : 0;

		sql_query( "INSERT INTO `guests`(`gstIp`,`gstCount`,`gstLastVisit`,`gstIsBot`) ".
			"VALUES('$userIp','1',NOW(),'$bot') ".
			"ON DUPLICATE KEY UPDATE `gstCount` = `gstCount` + 1, `gstLastVisit` = NOW(), `gstIsBot` = '$bot'" );
	}
}

if( $_cmd[ 0 ] == "view" && $_isSearchBot )
{
	include( INCLUDES."p_view!bot.php" );
	return;
}

// -----------------------------------------------------------------------------
// Set the timezone defined by the user and provide time correction functions.

include_once( INCLUDES."timezones.php" );
include_once( INCLUDES."formatting.php" );
include_once( INCLUDES."updates.php" );
include_once( INCLUDES."operations.php" );
include_once( INCLUDES."visuals.php" );

if( isLoggedIn() )
{
	performDelayedOperations();
	recountAllUpdatesIfNeeded( $_auth[ "useid" ]);
}

// -----------------------------------------------------------------------------
// Everything should be ready by now to process assets.

// Check if an asset is being requested.

if( $_cmd[ 0 ] == "asset" && isset( $_POST[ "count" ]))
{
	include( INCLUDES."asset.php" );
	return;
}

// -----------------------------------------------------------------------------
// The /preview request is used to output the preview message without any
// layout at all.

if( $_cmd[ 0 ] == "preview" && isset( $_POST[ "text" ]))
{
	include( INCLUDES."p_preview.php" );
}

// -----------------------------------------------------------------------------
// The /acceptdonation request processes the donation from YowCow and does
// not generate any layout.


//if( $_cmd[ 0 ] == "processPayment" )
//{
//	include( INCLUDES."p_processPayment.php" );
//}


// -----------------------------------------------------------------------------
// Global variable $_pollUser contains the user id of whose poll shall be
// displayed on the right side bar. By default, the admin's poll is displayed,
// but this value can be overridden by the p_*.php include files.

$_pollUser = $_config["adminUser"];

// -----------------------------------------------------------------------------
// Global variable $_documentTitle contains the title of the document that
// shall be sent to the browser via Javascript (document.title='...'), this
// happens at the very bottom of the generated XHTML.

$_documentTitle = "";

$_currentPageURL = url( "." );

// -----------------------------------------------------------------------------
// Enable GZIP compression of the output buffer (note that if this is disabled,
// redirect() will not work, unless output buffering is enabled globally in
// PHP).

if( !isset( $_GET[ "nolayout" ]))
{
	ob_start( "ob_gzhandler" );
}
else
{
	ob_start();
}

if( $_cmd[ 0 ] == "updatestrings" )
	$_GET[ "popup" ] = true;

// -----------------------------------------------------------------------------
// Start the output of the layout.

if( isset( $_GET[ "nolayout" ]))
{
	echo "<html><body>";

	$_page = INCLUDES."p_".preg_replace( '/[^a-z0-9]/', "",
		$_cmd[ 0 ]).".php";

	include( file_exists( $_page ) ? $_page : INCLUDES."p_notfound.php" );

	//echo $_addToFooter;

	echo "</body></html>";
}
else
{
	include( INCLUDES."layout.php" );
}

ob_flush();
flush();
sql_write_log();

?>
