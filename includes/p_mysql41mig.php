<?

if( !atLeastSModerator() )
{
	include( INCLUDES."p_notfound.php" );
	return;
}

?>
<div class="container">
<?

$_documentTitle = "MySQL 4.0 to 4.1 migration";

$result = sql_query( "SHOW TABLES" );

$tables = array();

while( $data = mysql_fetch_row( $result ))
{
	$tables[] = $data[ 0 ];
}

sort( $tables );

$step1 = "";
$step2 = "";

mysql_free_result( $result );

foreach( $tables as $tableName )
{
	$step2 .= "ALTER TABLE $tableName CHARACTER SET utf8 COLLATE utf8_general_ci;\n";
	$step2trail = "";

	$result = sql_query( "SHOW INDEX FROM ".$tableName );

	while( $indexData = mysql_fetch_assoc( $result ))
	{
		if( $indexData[ "Index_type" ] == "FULLTEXT" )
		{
			$step1 .= "DROP INDEX ".$indexData[ "Key_name" ]." ON $tableName;\n";
			$step2trail .= "CREATE FULLTEXT INDEX ".$indexData[ "Key_name" ].
				" ON $tableName (".$indexData[ "Column_name" ].");\n";
		}
	}

	mysql_free_result( $result );

	$result = sql_query( "DESCRIBE ".$tableName );

	while( $fieldData = mysql_fetch_assoc( $result ))
	{
		$field = $fieldData[ "Field" ];
		$type = strtoupper( $fieldData[ "Type" ]);
		$isNull = ( strtoupper( $fieldData[ "Null" ]) == "YES" );
		$nullStr = $isNull ? "" : " NOT NULL";

		if( preg_match( '/^VARCHAR/', $type ))
		{
			$type2 = preg_replace( '/^VARCHAR/', "VARBINARY", $type );
		}
		elseif( preg_match( '/^VARBINARY/', $type ))
		{
			$type2 = $type;
			$type = preg_replace( '/^VARBINARY/', "VARCHAR", $type2 );
		}
		elseif( preg_match( '/^CHAR/', $type ))
		{
			$type2 = preg_replace( '/^CHAR/', "BINARY", $type );
		}
		elseif( preg_match( '/^BINARY/', $type ))
		{
			$type2 = $type;
			$type = preg_replace( '/^BINARY/', "CHAR", $type2 );
		}
		elseif( preg_match( '/^TEXT/', $type ))
		{
			$type2 = preg_replace( '/^TEXT/', "BLOB", $type );
		}
		elseif( preg_match( '/^BLOB/', $type ))
		{
			$type2 = $type;
			$type = preg_replace( '/^BLOB/', "TEXT", $type2 );
		}
		else
		{
			continue;
		}

		$step1 .= "ALTER TABLE $tableName MODIFY $field $type2$nullStr;\n";
		$step2 .= "ALTER TABLE $tableName MODIFY $field $type CHARACTER SET utf8 COLLATE utf8_general_ci$nullStr;\n";
	}

	mysql_free_result( $result );

	$step2 .= $step2trail;
}

?><h1>Step 1. Run under MySQL 4.0</h1><?

echo nl2br( $step1 );

?><h1>Step 2. Run after installing MySQL 4.1</h1><?

echo nl2br( $step2 );

?>
</div>
