<?

$_documentTitle = _KEYWORDS;

// Show "Page not found" for a non-administrator user.

if( !atLeastModerator() )
{
	include(INCLUDES."p_notfound.php");
	return;
}

if(!isAuthorized('isKeywordsAdmin'))
{
	include( INCLUDES."p_notfound.php" );
	return;
}

if( $_cmd[ 1 ] == "build" )
{
	// rebuild keywords cache
	include( INCLUDES."mod_keywords_build.php" );
	redirect( url( "keywords", array( "updated" => 1 )));
}

?>
<div class="header">
	<div class="header_title">
		<?= _ADMINISTRATION ?>
		<div class="subheader"><?= _KEYWORDS_SUBTITLE ?></div>
	</div>
	<?

	$active = 3;
	include(INCLUDES."mod_adminmenu.php");

	?>
</div>

<div class="container">
	<?

	if( isset( $_GET[ "edit" ]))
	{
		
		$keyid = (int)$_GET['edit'];
		
		$sth = $dbh->prepare("SELECT *
							  FROM `keywords`
							  WHERE `keyid` = ?
							  LIMIT 1");
		$sth->bindParam(1, $keyid, PDO::PARAM_INT);
		$sth->execute();
		
		// Check to see if this actually exists....
		if(!$sth->rowCount()){
			redirect( url('keywords'));
		}
		
		$keyData = $sth->fetch(PDO::FETCH_ASSOC);
		
		$value = $keyData['keyWord'];
		$desc = $keyData['keyDesc'];
		
		if( isset( $_POST[ "value" ]) && isset( $_POST[ "desc" ]))
		{
			// Change keyword's parent keyword.

			if( isset( $_POST[ "subcatConfirm" ]) && isset( $_POST[ "subcat" ]))
			{
				/*sql_query( "UPDATE `keywords` ".
					"SET `keySubcat` = '".intval( $_POST[ "subcat" ])."' ".
					"WHERE `keyid` = '$keyid' LIMIT 1" );*/
					
				$sth = $dbh->prepare("UPDATE keywords
				SET keySubcat = ?
				WHERE keyid = ?
				LIMIT 1");
				
				$sth->bindParam(1, $_POST['subcat'], PDO::PARAM_INT);
				$sth->bindParam(2, $keyid, PDO::PARAM_INT);
				
				$sth->execute();
			}

			// Filters

			$first = true;
			$filters = "";

			foreach( getEnabledFilters() as $filter )
			{
				if( isset( $_POST[ "filter".$filter ]))
				{
					$filters .= ( $first ? "" : "," ).$filter;

					if( $first )
					{
						$first = false;
					}
				}
			}

			// Update the keyword's caption & filters.

			/*sql_query( "UPDATE `keywords` ".
				"SET `keyWord` = '".addslashes( $_POST[ "value" ])."', ".
				"`keyDesc` = '".addslashes( $_POST[ "desc" ])."', ".
				"`keyFilter` = '".$filters."' ".
				"WHERE `keyid` = '$keyid' LIMIT 1" );*/
				
			$sth = $dbh->prepare("UPDATE keywords
			SET keyWord = ?,
			keyDesc = ?,
			keyFilter = ?
			WHERE keyid = ?
			LIMIT 1");
			
			$sth->bindParam(1, $_POST['value'], PDO::PARAM_STR);
			$sth->bindParam(2, $_POST['desc'], PDO::PARAM_STR);
			$sth->bindParam(3, $filters);
			$sth->bindParam(4, $keyid);
			
			$sth->execute();

			// Get back to the keywords page.

			redirect( url( "keywords", array( "updated" => 1 )));
		}
	
		?>
		<form action="<?=url( "keywords", array( "edit" => $keyid ))?>" method="post">
			<div class="caption"><?= _KEYWORDS_EDIT ?>:</div>
			<div>
				<div class="f_right">^
					<input class="narrow" name="subcat" type="text"
						value="<?= $keyData[ "keySubcat" ] ?>" />
					<input class="checkbox" id="editSubcatConfirm"
						name="subcatConfirm" type="checkbox" />
					<label for="editSubcatConfirm"><?= _CHANGE." "._PARENT ?></label>
				</div>
				<input name="value" type="text" value="<?= htmlspecialchars( $value ) ?>" />
			</div>
			<div class="sep caption"><?= _COMMENT ?>:</div>
			<div>
				<?

				$commentDefault = $desc;
				$commentName = "desc";
				$commentRows = 5;
				$commentNoOptions = true;

				include( INCLUDES."mod_comment.php" );

				?>
			</div>
			<div class="clear"><br /></div>
			<?

			// FILTERS

			?>
			<div class="sep caption"><?= _FILTERS ?></div>
			<div class="container2 notsowide">
				<div>
					Filters automatically applied by the system when a submission uses this keyword:
				</div>
				<div class="sep">
					<table cellspacing="0" cellpadding="4" border="0">
					<tr>
					<?

					$cols = 0;
					$objFilters = preg_split( '/[\s\,\;]/', $keyData[ "keyFilter" ], 64, PREG_SPLIT_NO_EMPTY );

					foreach( getEnabledFilters() as $filter )
					{
						if( $cols > 2 )
						{
							?>
							</tr><tr>
							<?

							$cols = 0;
						}

						$filterName = $filter == -1 ? _SUBMIT_TYPE_EXTRA : getFilterName( $filter );

						?>
						<td align="right" width="40px">
							<input <?= in_array( $filter, $objFilters ) ? 'checked="checked"' : "" ?>
							class="checkbox" id="idFilter<?= $filter ?>" name="filter<?= $filter ?>"
							type="checkbox" /></td>
						<td>
							<label for="idFilter<?= $filter ?>"><?= $filterName ?></label></td>
						<?

						$found = true;
						$cols++;
					}

					?>
					</tr>
					</table>
				</div>
			</div>

			<div class="sep">
				<input class="submit" type="submit" value="<?=_SAVE_CHANGES ?>" />
			</div>
		</form>
		</div>
		<?

		return;
	}

	if( isset( $_GET[ "delete" ]))
	{
		//$keyid = intval( $_GET[ "delete" ]);
		$keyid = (int)$_GET['delete'];

		$result = sql_query( "SELECT `keyWord` FROM `keywords` ".
			"WHERE `keyid` = '$keyid' LIMIT 1" );

		if( isset( $_POST[ "cancel" ]) || !mysql_num_rows( $result ))
		{
			redirect( url( "keywords" )); // keyword not found
		}

		$value = mysql_result( $result, 0 );

		mysql_free_result( $result );

		if( isset( $_POST[ "confirm" ]))
		{
			// Remove this keyword from the database.

			sql_query( "DELETE FROM `keywords` WHERE `keyid` = '$keyid' LIMIT 1" );

			// Remove this keyword from submissions, if any of them used it.

			sql_query( "DELETE FROM `objKeywords` WHERE `objKkeyword` = '$keyid'" );

			// Get back to the keywords page.

			redirect( url( "keywords", array( "updated" => 1 )));
		}

		?>
		<form action="<?= url( "keywords", array( "delete" => $keyid )) ?>" method="post">
			<div class="error notsowide">
				VERY IMPORTANT. Please do NOT delete keywords in order to move them to another
				group/subcategory. This action, in order to be done correctly, requires
				modification on the database and should not be done by deleting keywords
				and creating them in the other subcategory.
			</div>
			<div>
				<input class="checkbox" id="confirmDeletion" name="confirm" type="checkbox" />
				<label for="confirmDeletion"><?= _KEYWORDS_DELETE ?>: <?= $value ?></label>
			</div>
			<div class="sep">
				<input class="submit" name="cancel" type="submit" value="<?= _CANCEL ?>" />
				<input class="submit" name="submit" type="submit" value="<?= _DELETE ?>" />
			</div>
		</form>
		</div>
		<?
		return;
	}

	?>
	<div class="mar_bottom">
		<div class="minibutton" onclick="toggle_visibility ('keywords_hint')"><?= _HINT ?></div>
	</div>
	<div class="clear">&nbsp;</div>
	<div class="mar_bottom notsowide" style="display: none" id="keywords_hint"><?= _KEYWORDS_HINT ?></div>
	<?php


	if( isset( $_POST[ "submitKeywords" ]))
	{
		reset( $_POST );
		

		foreach( $_POST as $postKey => $postVar )
		{
			if( preg_match( '/^addKeywordsUnder[0-9]+$/', $postKey ) && $postVar )
			{
				$addUnder = intval( preg_replace( '/^addKeywordsUnder/', "", $postKey ));

				$keywords = preg_split( '/\;/', $postVar, -1, PREG_SPLIT_NO_EMPTY );

				foreach( $keywords as $keyWord )
				{
					$keyWord = trim( $keyWord );

					if( !$keyWord )
					{
						continue;
					}


					sql_query( "INSERT INTO `keywords`".dbValues( array(
						"keyWord" => $keyWord,
						"keySubcat" => $addUnder )));
				}
			}
		}
	}

	if( isset( $_GET[ "updated" ]))
	{
		
		?>
		Keywords added/updated.
		<form action="<?= url( "." ) ?>" method="get">
			<div class="sep">
			<button class="submit" name="submitKeywords" type="submit">
				<?= getIMG( url()."images/emoticons/nav-prev.png" ) ?>
				<?= _RETURN ?>
			</button>
			</div>
		</form>
		</div>
		<?

		return;
	}

	?>
	<form action="<?= url( ".", array( "updated" => 1 )) ?>" method="post">
		<?

		include( INCLUDES."mod_keywords_edit.php" );

		?>
		<div class="sep">
			<button class="submit" name="submitKeywords" type="submit" >
				<?= getIMG( url()."images/emoticons/checked.png" ) ?>
				<?= _SAVE_CHANGES ?>
			</button>
			&nbsp; &nbsp;
			<a href="<?= url( "keywords/build" ) ?>">
				<b><?= _UPDATE ?>!</b> (slow; run after mass changes on the keywords table)
			</a>
		</div>
	</form>
</div>
