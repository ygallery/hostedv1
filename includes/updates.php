<?

// Note: updCreator is always the one who receives the update notification.

define( "updTypeMessageFav", "1" ); // updUser has added updCreator's artwork updObj to their favourites.
define( "updTypeMessageWatch", "2" ); // updUser has added updCreator to their friends list.
define( "updTypeMessageClub", "3" ); // updCreator has been accepted as a member of the club updObj.
define( "updTypeMessageAbuse", "4" ); // updCreator's artwork updObj has been reported as abusive.
define( "updTypeArt", "5" ); // updCreator has been notified that updUser has submitted a new artwork updObj.
define( "updTypeJournal", "6" ); // updCreator has been notified that updUser has posted a new journal entry updObj.
define( "updTypeJournalPoll", "7" ); // updCreator has been notified that updUser has posted a new opinion poll updObj.
define( "updTypeComment", "8" ); // updCreator has received a comment updObj posted by user updUser.
define( "updTypeMessageDeleted", "9" ); // updCreator's artwork updObj has been deleted by moderator
define( "updTypeMessageRestored", "10" ); // updCreator's artwork updObj has been restored by moderator
define( "updTypeMessageClubMember", "11" ); // updCreator has been notified that updUser has joined their club updObj.
define( "updTypeMessageClubDeclined", "12" ); // updCreator has NOT been accepted as a member of the club updObj.
define( "updTypeMessageClubRefused", "13" ); // updCreator has been notified that their submission updObj was removed from the club updUser by the club's owner.
define( "updTypePM", "14" ); // updCreator has been notified that they have an unread private message updObj.
define( "updTypeMessageWatchClub", "15" ); // updUser has added club updObj to their watch list and the club's owner updCreator is notified about that.
define( "updTypeAnnouncement", "16" ); // updCreator has been notified that club updUser has posted a new announcement updObj.
define( "updTypeArtExtra", "17" ); // updCreator has been notified that updUser has submitted a new artwork updObj.
define ("updTypeMessageHelpUpdate", "18"); //updCreator has been notified that a helpdesk case of theirs has been updated.
define ("updTypeMessageHelpResolved", "19"); //updCreator has been notified that a helpdesk case of their has been marked as resolved. 

// -----------------------------------------------------------------------------
// Add a single update of $type in $creator's updates list.

function addUpdate( $type, $creator, $object = 0, $user = 0 )
{
	switch( $type )
	{
		case updTypeMessageFav:
		{
			$useResult = sql_query( "SELECT `useNotifyFavs` FROM `useExtData`".dbWhere( array(
				"useEid" => $creator )));

			if( !$useData = mysql_fetch_assoc( $useResult ))
				return; // User not found.

			if( !$useData[ "useNotifyFavs" ])
				return; // This user would like not to be notified about +favs.

			mysql_free_result( $useResult );

			break;
		}

		case updTypeMessageWatchClub:
		case updTypeMessageWatch:
		{
			$useResult = sql_query( "SELECT `useNotifyWatch` FROM `useExtData`".dbWhere( array(
				"useEid" => $creator )));

			if( !$useData = mysql_fetch_assoc( $useResult ))
				return; // User not found.

			if( !$useData[ "useNotifyWatch" ])
				return; // This user would like not to be notified about +watches.

			mysql_free_result( $useResult );

			break;
		}
	}

	// Check if this is Extras that the user doesn't want to receive.

	if( $type == updTypeArtExtra )
	{
		$result = sql_query( "SELECT `useHideExtras` FROM `useExtData`".dbWhere( array(
			"useEid" => $creator )));

		if( mysql_num_rows( $result ) > 0 && mysql_result( $result, 0 ))
		{
			return;
		}
	}

	// Check if such update already exists in order to avoid duplicate
	// updates (for ex. +fav the same submission 2+ times in a row).

	$values = array(
		"updType" => $type,
		"updCreator" => $creator,
		"updObj" => $object,
		"updUser" => $user );

	$result = sql_query( "SELECT COUNT(*) FROM `updates`".
		dbWhere( $values )."LIMIT 1" );

	if( mysql_result( $result, 0 ) == 0 )
	{
		// Not found? Then we should add it.

		$values[ "updDate!" ] = "NOW()";

		sql_query( "INSERT INTO `updates`".dbValues( $values ));
	}

	recountUpdates( $type, $creator );
}

// -----------------------------------------------------------------------------
// Add multiple updates to everyone who watches user $poster.

function addUpdateToWatchers( $type, $poster, $objid )
{
	sql_query( "DELETE FROM `updates`".dbWhere( array(
		"updType" => $type,
		"updUser" => $poster,
		"updObj" => $objid )));

	sql_query( "INSERT INTO `updates`".
		"(`updDate`,`updType`,`updCreator`,`updObj`,`updUser`) ".
		"SELECT NOW(), '".intval( $type )."', `watUser`, ".
		"'".intval( $objid )."', '".intval( $poster )."' ".
		"FROM `watches` ".
		"WHERE `watCreator` = '".intval( $poster )."' AND `watType` = 'use'" );
}

// -----------------------------------------------------------------------------
// Add multiple updates to everyone who watches club $club.

function addUpdateToClubWatchers( $type, $poster, $objid, $club )
{
	sql_query( "DELETE FROM `updates`".dbWhere( array(
		"updType" => $type,
		"updUser" => $club,
		"updObj" => $objid )));

	sql_query( "INSERT INTO `updates`".
		"(`updDate`,`updType`,`updCreator`,`updObj`,`updUser`) ".
		"SELECT NOW(), '".intval( $type )."', `watUser`, ".
		"'".intval( $objid )."', '".intval( $club )."' ".
		"FROM `watches` ".
		"WHERE `watCreator` = '".intval( $club )."' AND `watType` = 'clu' ".
		"AND `watUser` <> '".intval( $poster )."'" );
}

// -----------------------------------------------------------------------------
// Add multiple art updates to everyone who watches user $poster and,
// optionally, club $club.

function addArtUpdateToWatchers( $poster, $objid, $club = 0, $skipUser = 0 )
{
	$distinct = "";
	$where = "`watCreator` = '".intval( $poster )."' AND `watType` = 'use'";

	if( $club > 0 )
	{
		$distinct = "DISTINCT";
		$where = "(".$where.") OR (`watCreator` = '".intval( $club ).
			"' AND `watType` = 'clu')";
	}

	if( $skipUser > 0 )
	{
		$where = "(".$where.") AND `watUser` <> '".intval( $skipUser )."'";
	}

	$where = "(".$where.") AND `watUser` <> '".intval( $poster )."'";

	/*
	sql_query( "INSERT INTO `updatesArt`(`updDate`,`updCreator`,`updObj`) ".
		"SELECT $distinct NOW(), `watUser`, '".intval( $objid )."' ".
		"FROM `watches` WHERE ".$where );
		*/

	sql_query(
		"INSERT IGNORE INTO `updatesArt`(`updDate`,`updCreator`,`updObj`) ".
		"(SELECT NOW(), `watUser`, '".intval( $objid )."' ".
		"FROM `watches` WHERE ".$where.")" );
}

// -----------------------------------------------------------------------------
// Remove duplicate updates (this happens after collab confirmation if some
// users watch both artists).

function removeDupeArtUpdates( $objid )
{
	$sql = "SELECT `updCreator`, COUNT(*) AS `count` ".
		"FROM `updatesArt` WHERE `updObj` = '".intval( $objid )."' ".
		"GROUP BY `updCreator`, `updObj` ".
		"HAVING `count` > '1'";

	$result = sql_query( $sql );

	while( $data = mysql_fetch_row( $result ))
	{
		$creator = $data[ 0 ];
		$count = $data[ 1 ];

		// Delete duplicate rows.

		sql_query( "DELETE FROM `updatesArt`".dbWhere( array(
			"updCreator" => $creator,
			"updObj" => $objid ))."LIMIT ".( $count - 1 ));
	}
}

// -----------------------------------------------------------------------------
// Removes a certain update from the current user's updates.

function markAsRead( $type, $object, $creator = 0, $user = 0 )
{
	global $_auth;

	if( $creator == 0 )
		$creator = $_auth[ "useid" ];

	// If the replied comment is in the current user's updates, remove it
	// from there.

	$where = array(
		"updCreator" => $creator,
		"updObj" => $object );

	if( $user > 0 )
		$where[ "updUser" ] = $user;

	if( $type == updTypeArt )
	{
		sql_query( "DELETE FROM `updatesArt`".dbWhere( $where ));
	}
	else
	{
		$where[ "updType" ] = $type;

		sql_query( "DELETE FROM `updates`".dbWhere( $where ));
	}

	recountUpdates( $type, $creator );
}

// -----------------------------------------------------------------------------
// Removes all updates marked as read from the current user's updates list
// (ids to remove we get from $_POST variables "upd_#####" and "artupd_#####").

function markUpdatesAsRead()
{
	global $_auth, $_config;

	if( $_config[ "readOnly" ])
	{
		// Something selected but we're in the read-only mode?

		notice( _READONLY );
		return false;
	}

	reset( $_POST );

	foreach( $_POST as $key => $value )
	{
		if( preg_match( '/^upd\_/', $key ))
		{
			$str = preg_replace( '/^upd\_/', "", $key );

			list( $type, $obj, $user ) = preg_split( '/\_/', $str, 3,
				PREG_SPLIT_NO_EMPTY );

			markAsRead( $type, $obj, 0, $user );
		}

		if( preg_match( '/^artupd\_/', $key ))
		{
			$objid = preg_replace( '/^artupd\_/', "", $key );
			markAsRead( updTypeArt, $objid );
		}
	}

	// Recount the current user's updates.

	recountAllUpdates( $_auth[ "useid" ]);

	// No need to call it, for example, for updTypeMessageClub because
	// updTypeMessageClub is already processed within updTypeMessageWatch
	// (see the recountUpdates() function).

	return( true );
}

// -----------------------------------------------------------------------------
// Marks all objects (submissions) as read

function markAllObjectsAsRead() {
	
	global $_auth, $_config;

	if( $_config[ "readOnly" ])
	{
		// Something selected but we're in the read-only mode?

		notice( _READONLY );
		return false;
	}
	$creator = $_auth[ "useid" ];
	sql_query( "DELETE FROM `updatesArt` WHERE `updCreator`='$creator'");
	//sql_query( "DELETE FROM `updates` WHERE `updCreator`='$creator' AND `updType=`'17'");
	
	//Recount the user's current updates
	recountAllUpdates( $_auth[ "useid" ]);

	return( true );
}



// -----------------------------------------------------------------------------
// Returns true if updates should be recounted for that user.

function recountAllUpdatesIfNeeded( $user )
{
	global $_auth;

	$now = time();

	if( $now - $_auth[ "useLastUpdate" ] < 240 )
	{
		return;
	}

/* We used to set useLastUpdate to $count, but now we're going to store the time of the update check instead
	$updResult = sql_query( "SELECT COUNT(*) FROM `updates` ".
		"WHERE `updCreator` = '".intval( $user )."'" );

	$count = mysql_result( $updResult, 0 );

	$updResult = sql_query( "SELECT COUNT(*) FROM `updatesArt` ".
		"WHERE `updCreator` = '".intval( $user )."'" );

	$count += mysql_result( $updResult, 0 );

	if( $_auth[ "useLastUpdate" ] == $count )
	{
		return;
	}
*/

	recountAllUpdates( $user );

	// Update useLastUpdate so that we don't have to recount again next time.

	sql_query( "UPDATE `useExtData` SET `useLastUpdate` = '$now' ".
		"WHERE `useEid` = '".intval( $user )."' LIMIT 1" );

	if( $user == $_auth[ "useid" ])
	{
		$_auth[ "useLastUpdate" ] = $now; // Immediate effect.
	}

	return( true );
}

// -----------------------------------------------------------------------------
// Removes updates from ignored users.

function killTwitUpdates( $user )
{
	$twtResult = sql_query( "SELECT `twtBadUser` FROM `twitList`".dbWhere( array(
		"twtCreator" => $user )));

	$badUsers = array();

	while( $twtData = mysql_fetch_assoc( $twtResult ))
	{
		$badUsers[] = $twtData[ "twtBadUser" ];
	}

	if(!count($badUsers))
	{
		return;
	}

	$types = array( updTypeMessageFav, updTypeMessageWatch, updTypeMessageClub,
		updTypeArt, updTypeJournal, updTypeJournalPoll, updTypeComment, updTypePM,
		updTypeMessageWatchClub, updTypeAnnouncement, updTypeArtExtra );

	sql_query( "DELETE FROM `updates` WHERE `updCreator` = '".$user."' ".
		"AND `updType` IN('".implode( "','", $types )."') ".
		"AND `updUser` IN('".implode( "','", $badUsers )."')" );
}

// -----------------------------------------------------------------------------
// Recalculates all types of updates at once.

function recountAllUpdates( $user )
{
	killTwitUpdates( $user );

	recountUpdates( updTypeMessageFav, $user );
	recountUpdates( updTypeMessageWatch, $user );
	recountUpdates( updTypeArt, $user );
	recountUpdates( updTypeArtExtra, $user );
	recountUpdates( updTypeJournal, $user );
	recountUpdates( updTypeComment, $user );
	recountUpdates( updTypePM, $user );
}

// -----------------------------------------------------------------------------
// Recalculate the useUpd* update counters for the user $creator and specified
// update $type.

function recountUpdates( $type, $user )
{
	global $_auth;

	switch( $type )
	{
		case updTypeMessageFav:
		{
			$result = sql_query( "SELECT COUNT(*) FROM `updates` ".
				"WHERE `updType` = '".updTypeMessageFav."' ".
				"AND `updCreator` = '".intval( $user )."'" );

			$updField = "useUpdFav";
			break;
		}

		case updTypeMessageWatch:
		case updTypeMessageClub:
		case updTypeMessageAbuse:
		case updTypeMessageDeleted:
		case updTypeMessageRestored:
		case updTypeMessageClubMember:
		case updTypeMessageClubDeclined:
		case updTypeMessageClubRefused:
		case updTypeMessageWatchClub:
		case updTypeMessageHelpUpdate:
		case updTypeMessageHelpResolved:
		{
			$types = array( updTypeMessageWatch,
				updTypeMessageClub, updTypeMessageAbuse,
				updTypeMessageDeleted, updTypeMessageRestored,
				updTypeMessageClubMember, updTypeMessageClubDeclined,
				updTypeMessageClubRefused, updTypeMessageWatchClub, 
				updTypeMessageHelpUpdate, updTypeMessageHelpResolved );

			$result = sql_query( "SELECT COUNT(*) FROM `updates` ".
				"WHERE `updType` IN('".implode( "','", $types )."') ".
				"AND `updCreator` = '".intval( $user )."'" );

			$updField = "useUpdWat";
			break;
		}

		case updTypeArt:
		{
			$where = "`objid` = `updObj` ".
				"AND `objDeleted` = '0' AND `objPending` = '0' ".
				"AND `updCreator` = '".intval( $user )."'";

			applyObjFilters( $where, $user );

			$result = sql_query( "SELECT COUNT(*) FROM `updatesArt`,`objects` ".
				"WHERE ".$where );

			$updField = "useUpdObj";
			break;
		}

		case updTypeArtExtra:
		{
			$where = "`updType` = '".updTypeArtExtra."' AND `objid` = `updObj` ".
				"AND `objDeleted` = '0' AND `objPending` = '0' ".
				"AND `updCreator` = '".intval( $user )."'";

			applyObjFilters( $where, $user );

			$result = sql_query( "SELECT COUNT(*) FROM `updates`,`extras` ".
				"WHERE ".$where );

			$updField = "useUpdExt";
			break;
		}

		case updTypeJournal:
		case updTypeJournalPoll:
		case updTypeAnnouncement:
		{
			$types = array( updTypeJournal, updTypeJournalPoll,
				updTypeAnnouncement );

			$result = sql_query( "SELECT COUNT(*) FROM `updates` ".
				"WHERE `updType` IN('".implode( "','", $types )."') ".
				"AND `updCreator` = '".intval( $user )."'" );

			$updField = "useUpdJou";
			break;
		}

		case updTypeComment:
		{
			$result = sql_query( "SELECT COUNT(*) FROM `updates` ".
				"WHERE `updType` = '".updTypeComment."' ".
				"AND `updCreator` = '".intval( $user )."'" );

			$updField = "useUpdCom";
			break;
		}

		case updTypePM:
		{
			include_once( INCLUDES."comments.php" );

			$result = sql_query( "SELECT COUNT(*) FROM `updates` ".
				"WHERE `updType` = '".updTypePM."' ".
				"AND `updCreator` = '".intval( $user )."'" );

			$updField = "useUpdPms";
			break;
		}

		default:
		{
			return;
		}
	}

	$updCount = mysql_result( $result, 0 );

	sql_query( "UPDATE `useExtData` SET `$updField` = '$updCount' ".
		"WHERE `useEid` = '".intval( $user )."' LIMIT 1" );

	if( $user == $_auth[ "useid" ])
	{
		$_auth[ $updField ] = $updCount; // Immediate effect.
	}
}

// -----------------------------------------------------------------------------
// Updates all the cached information for the search engine for the specified
// submission.

function updateSearchCache( $objid, $force = false )
{
	if( !$force )
	{
		// If we're not forcing an update, maybe we could just mark it as
		// updated in case the object already exists in the cache. Then it
		// will be updated by a script that runs once a day or so.

		$result = sql_query( "SELECT COUNT(*) FROM `searchcache` ".
			"WHERE `seaObject` = '".intval( $objid )."' LIMIT 1" );

		if( mysql_result( $result, 0 ) > 0 )
		{
			mysql_free_result( $result );

			sql_query( "UPDATE `searchcache` ".
				"SET `seaNeedsUpdate` = '1' ".
				"WHERE `seaObject` = '".intval( $objid )."' LIMIT 1" );

			return;
		}
	}

	$creator = 0;
	$text = "";

	// Add the submission title.

	$result = sql_query( "SELECT `objTitle`, `objCreator` FROM `objects` ".
		"WHERE `objid` = '".intval( $objid )."' LIMIT 1" );

	if( $data = mysql_fetch_row( $result ))
	{
		$text .= " ".$data[ 0 ];
		$creator = $data[ 1 ];
	}

	mysql_free_result( $result );

	// Add the artist's comment.

	$result = sql_query( "SELECT `objComment` FROM `objExtData` ".
		"WHERE `objEid` = '".intval( $objid )."' LIMIT 1" );

	if( $data = mysql_fetch_row( $result ))
	{
		$text .= " ".$data[ 0 ];
	}

	mysql_free_result( $result );

	// Add all the first-level comment made on the submission.

	$result = sql_query( "SELECT `comComment` FROM `comments` ".
		"WHERE `comObj` = '".intval( $objid )."' AND `comObjType` = 'obj'" );

	while( $data = mysql_fetch_row( $result ))
	{
		$text .= " ".$data[ 0 ];
	}

	mysql_free_result( $result );

	$result = sql_query( "SELECT `useUsername`, `useRealName` FROM `users` ".
		"WHERE `useid` = '".intval( $creator )."' LIMIT 1" );

	if( $data = mysql_fetch_row( $result ))
	{
		$text .= " ".$data[ 0 ]." ".$data[ 1 ];
	}

	mysql_free_result( $result );

	// Make all the words unique and lowercased, also remove way too short
	// and way too long words.

	$info = preg_split( "/[^a-z0-9\-\']/", strtolower( $text ), -1,
		PREG_SPLIT_NO_EMPTY );

	$text = "";

	foreach( $info as $word )
	{
		if( strlen( $word ) >= 3 && strlen( $word ) < 20 )
		{
			// Make sure the word isn't in the string yet.

			if( !preg_match( '/'.$word.'\s/i', $text ))
			{
				$text .= "$word "; // Add the new unique word.
			}
		}
	}

	$result = sql_query( "SELECT COUNT(*) FROM `searchcache` ".
		"WHERE `seaObject` = '".intval( $objid )."' LIMIT 1" );

	if( mysql_result( $result, 0 ) > 0 )
	{
		sql_query( "UPDATE `searchcache` ".
			"SET `seaText` = '".addslashes( $text )."', ".
			"`seaNeedsUpdate` = '0' ".
			"WHERE `seaObject` = '".intval( $objid )."' LIMIT 1" );
	}
	else
	{
		sql_query( "INSERT INTO `searchcache`(`seaObject`, `seaText`, `seaNeedsUpdate`) ".
			"VALUES('".intval( $objid )."', '".addslashes( $text )."', '0')" );
	}

	mysql_free_result( $result );
}

// -----------------------------------------------------------------------------

function updateAvatar( $useid )
{
	$filename = findNewestFileById( "files/avatars/", $useid, "" );

	if( $filename == "" )
	{
		$width = 0;
		$height = 0;
		$date = 0;
		$ext = "---";
	}
	else
	{
		list( $width, $height ) = getimagesize( $filename );

		$date = substr( strrchr( $filename, "-" ), 1 );
		$date = substr( $date, 0, strpos( $date, "." ));

		$ext = substr( strrchr( $filename, "." ), 1 );
	}

	sql_query( "UPDATE `users`".dbSet( array(
		"useAvatarWidth" => $width,
		"useAvatarHeight" => $height,
		"useAvatarDate" => $date,
		"useAvatarExt" => $ext )).dbWhere( array(
		"useid" => $useid )));
}

// -----------------------------------------------------------------------------

?>
