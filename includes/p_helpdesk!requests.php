<?

if( !isLoggedIn() )
{
	echo _REQUIRE_LOGIN;
	return;
}

function putRequestList( $listResolved )
{
	global $_auth;

	sql_order( "hlpSubmitDate DESC" );

	$useid = intval( $_auth[ "useid" ]);

	sql_where( array(
		"|1" => "(`hlpOwner` = '$useid' OR `hlpSubmitter` = '$useid')",
		"hlpStatus".( $listResolved ? "" : "<>" ) => "completed" ));

	$hlpResult = sql_rowset( "helpdesk" );

	if( sql_num_rows( $hlpResult ) == 0 )
	{
		?>
		<div class="container2">
			<?= _NONE ?>.
		</div>
		<?
	}

	while( $hlpData = sql_next( $hlpResult ))
	{
		putRequestData( $hlpData );
	}

	sql_free( $hlpResult );
}

?>
<div class="header">
	My Requests
</div>
<div class="notsowide">
	<div class="f_right">	
		<a href="<?= url( "helpdesk/request" ) ?>">
			<b>Add a Request</b>
			<?= getIMG( url()."images/emoticons/star.png" )?>
		</a>
	</div>
	<div style="margin-top : 8px;" class="mar_bottom">
		<?= getIMG( url()."images/emoticons/bullet2.png" )?>
		Unresolved requests
	</div>
	<? putRequestList( false ); ?>
	<div><br /></div>
	<div class="mar_bottom">
		<?= getIMG( url()."images/emoticons/checked.png" )?>
		Resolved requests
	</div>
	<? putRequestList( true ); ?>
</div>
