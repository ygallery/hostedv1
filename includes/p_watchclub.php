<?

// Adds club $_cmd[ 1 ] to the current user's watch list.

// watCreator in the `watches` table is the club that we watch.
// watUser is the user who is watching the club.

if( $_auth[ "useid" ] && !$_config[ "readOnly" ])
{
	// Check if this club exists.

	$result = sql_query( "SELECT `cluid`,`cluCreator` FROM `clubs`,`cluExtData` ".
		"WHERE `cluEid` = `cluid` AND `cluid` = '".intval( $_cmd[ 1 ])."' LIMIT 1" );

	if( $cluData = mysql_fetch_assoc( $result ))
	{
		// Check if the club is already +watch'd.

		$result = sql_query( "SELECT `watid` FROM `watches` ".
			"WHERE `watUser` = '".$_auth[ "useid" ]."' ".
			"AND `watCreator` = '".$cluData[ "cluid" ]."' ".
			"AND `watType` = 'clu' LIMIT 1" );

		if( $watData = mysql_fetch_assoc( $result ))
		{
			// +watch'd already? remove them from the watches.

			if( !isset( $_GET[ "disableUnwatch" ]))
			{
				sql_query( "DELETE FROM `watches` WHERE `watid` = '".$watData[ "watid" ]."' LIMIT 1" );
			}
		}
		else
		{
			// It's not watch'd? add it to the watches then.

			sql_query( "INSERT INTO `watches`(`watCreator`,`watUser`,`watSubmitDate`,`watType`) ".
				"VALUES('".$cluData[ "cluid" ]."','".$_auth[ "useid" ]."',NOW(),'clu')");

			// Notify the club's owner about the +watch.

			addUpdate( updTypeMessageWatchClub, $cluData[ "cluCreator" ], $cluData[ "cluid" ], $_auth[ "useid" ]);
		}

		// Recalculate club watcher-count

		sql_where( array(
			"watType" => "clu",
			"watCreator" => $cluData[ "cluid" ]));

		$watcherCount = sql_count( "watches" );

		sql_where( array( "cluEid" => $cluData[ "cluid" ]));
		sql_values( array( "cluWatcherCount" => $watcherCount ));
		sql_update( "cluExtData" );
	}
}

redirect( url( "club/".intval( $_cmd[ 1 ]))); // Get back to the club page.

?>