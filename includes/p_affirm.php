<?

$cluid = intval($_cmd[1]);
$useUsername = $_cmd[2];

if(!isset($_GET["accept"]))
	redirect(url("club/".intval($cluid)));

$accept = ($_GET["accept"] == 1);

$result = sql_query("SELECT * FROM `clubs`,`cluExtData` WHERE `cluid` = '$cluid' AND `cluEid` = `cluid` AND `cluCreator` = '".$_auth["useid"]."' LIMIT 1");
if(!$cluData = mysql_fetch_assoc($result))
	redirect(url("club/".intval($cluid)));

$result = sql_query("SELECT `useid`,`useCid` FROM `users`,`useClubs` WHERE `useUsername` = '".addslashes($useUsername)."' AND `useCclub` = '$cluid' AND `useCmember` = `useid` AND `useCpending` = '1' LIMIT 1");
if(!$useData = mysql_fetch_assoc($result))
	redirect(url("club/".intval($cluid)));

if($accept) {
	// accept the user as a member of the club
	sql_query("UPDATE `useClubs` SET `useCpending` = '0' WHERE `useCid` = '".$useData["useCid"]."' LIMIT 1");
	// notify the user that they are accepted
	addUpdate(updTypeMessageClub, $useData["useid"], $cluid);
}
else {
	// remove the user from the club's members list
	sql_query("DELETE FROM `useClubs` WHERE `useCid` = '".$useData["useCid"]."' LIMIT 1");
	// notify the user that they are not accepted
	addUpdate(updTypeMessageClubDeclined, $useData["useid"], $cluid);
}

redirect(url("club/".intval($cluid)));

?>