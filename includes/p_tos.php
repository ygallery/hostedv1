<?
	$_documentTitle = _TOS;
?>

<div class="header">
	<div class="header_title"><?=_TOS ?></div>
</div>	

<div class="container">
	<?
	if( isset($_GET['lang']) )
		$tos_lang = preg_replace("/[^[a-z]\-]/","",$_GET['lang']);

	if( isset($tos_lang) && !empty($tos_lang) )
		$filename = INCLUDES."strings/".$tos_lang."_tos.php";
	else
		$filename = INCLUDES."strings/".$_lang."_tos.php";

	if( !file_exists( $filename ))
		$filename = INCLUDES."strings/en_tos.php";

	include($filename);

	?>
</div>
