<?

include_once( "serverload.php" );

if( !isset( $serverload ))
{
	$serverload = 69;
}

$_documentTitle = _SEARCH;

if( $_config[ "disableUnder18" ] && !isLoggedIn() )
{
	header("Status: 403 Forbidden");
	?>
	<div class="header">
		<div class="header_title">
			<?= _SEARCH ?>
			<div class="subheader">
				<?= _SEARCH_SUBTITLE ?>
			</div>
		</div>
	</div>
	<div class="container">
		<? notice( _REQUIRE_LOGIN ); ?>
	</div>
	<?

	return;
}

$searching = false;

if( isset( $_GET[ "keywordList" ]) && $_GET[ "keywordList" ] != "" )
{
	$searching = true;
}

if( isset( $_GET[ "searchText" ]) && $_GET[ "searchText" ] != "" )
{
	$searching = true;
}

if( $searching )
{	
	$select = "";
	$where = "";
	$hasK1 = false;

	performSearch( $select, $where, $hasK1 ); // Find this search criteria in the cache.

	?>
	<div class="header">
		<div class="header_title">
			<?= _SEARCH ?>
			<div class="subheader"><?= _SEARCH_SUBTITLE ?></div>
		</div>
	</div>

	<div class="container">
		<?

		if( $select == "" )
		{
			notice( _LOAD_TOO_HIGH );
			return;
		}
		else
		{
			// Show the gallery.

			include_once( INCLUDES."gallery.php" );

			$searchText = isset( $_GET[ "searchText" ]) ? trim( $_GET[ "searchText" ]) : "";
			$sortById = $searchText != "" ? "`seaObject`" :
				( $hasK1 ? "k1.`objKobject`" : "objid" );

			showThumbnails( array(
				"quickSearch" => true,
				"disableFiltering" => true, // Filters apply during searching
				"countDisabled" => true, // Disable "Last" button
				"onDisplayFiltering" => true,
				"sortById" => $sortById,
				"select" => $select,
				"where" => $where ));
		}

		?>
	</div>
	<?

	return;
}

?>
<form action="<?= url( "search" ) ?>" method="get">
<div class="header">
	<div class="header_title">
		<?= _SEARCH ?>
		<div class="subheader"><?= _SEARCH_KEYWORDS_EXPLAIN ?></div>
		<div class="clear">&nbsp;</div>
	</div>
	<?

	$isInHeader = true;

	include( INCLUDES."mod_keywords.php" );

	?>
	<div class="sep mar_left">
		<b>Search type:</b>
		<input type="radio" class="radio" name="searchType" id="idSearchType0" 
			value="0" /><label for="idSearchType0">At least one of the chosen</label>
		<input type="radio" class="radio" name="searchType" id="idSearchType1" checked="checked"
			value="1" /><label for="idSearchType1">All of the chosen</label>
	</div>
	<br />
	<div class="caption"><?= _SEARCH_TEXT ?>:</div>
	<div><input class="notsowide" name="searchText" type="text" /></div>
	<div class="sep notsowide">
		<?= _SEARCH_HINT ?>
	</div>
	<div class="sep notsowide">
		<?= _SEARCH_HINT2 ?>
	</div>
	<div class="sep">
		<button class="submit" type="submit">
			<?= getIMG( url()."images/emoticons/checked.png" ) ?>
			<?= _SEARCH ?>
		</button>
	</div>
</div>
</form>
<?

function performSearch( &$select, &$where, &$hasK1 )
{
	global $_auth, $serverload;

	$keywordList = isset( $_GET[ "keywordList" ]) ? trim( $_GET[ "keywordList" ]) : "";
	$searchType = isset( $_GET[ "searchType" ]) ? $_GET[ "searchType" ] : 0;
	$searchText = isset( $_GET[ "searchText" ]) ? trim( $_GET[ "searchText" ]) : "";

	// Now copy all objids that we can find using this search criteria
	// to the `searchItems` table.

	if( $keywordList != "" )
	{
		// Some keywords defined, use them.

		$idList = preg_split( '/\s/', $keywordList, 5, // Maximum 5 keywords!
			PREG_SPLIT_NO_EMPTY );

		for( $i = 0; $i < count( $idList ); $i++ )
		{
			// Make sure they are all digits.

			$idList[ $i ] = intval( $idList[ $i ]);
		}

		// In case this keyword has subkeywords, use them as well.

		/*

		$keyids = "'".implode( "','", $idList )."'";

		$result = sql_query( "SELECT `keyid` FROM `keywords` ".
			"WHERE `keySubcat` IN($keyids)" );

		while( $rowData = mysql_fetch_row( $result ))
		{
			$idList = array_merge( $idList, array( $rowData[ 0 ]));
		}
		*/
	}

	// Build the search query.

	$select = "SELECT "./*( $searchType == 0 && count( $idList ) > 1 ? "DISTINCT " : "" ).*/
		"`objects`.* FROM `objects`";

	$where = "1";

	if( isset( $idList ) && count( $idList ) > 0 )
	{
		if( $searchType == 1 )
		{
			// All keywords must match.

			$num = 1;

			foreach( $idList as $id )
			{
				$select .= ",`objKeywords` AS k".$num;

				if( $num == 1 )
				{
					$hasK1 = true;
				}

				$where .= " AND k".$num.".`objKobject` = `objid` ".
					"AND k".$num.".`objKkeyword` = '".$id."'";

				$num++;

				if( $num > 5 )
				{
					break; // 5 keywords maximum
				}
			}
		}
		else
		{
			// At least 1 keyword must match.

			$select .= ",`objKeywords` AS k1";
			$hasK1 = true;

			if( count( $idList ) == 1 )
			{
				$where .= " AND k1.`objKobject` = `objid`".
					" AND k1.`objKkeyword` = '".$idList[ 0 ]."'";
			}
			else
			{
				$where .= " AND k1.`objKobject` = `objid`".
					" AND k1.`objKkeyword` IN('".implode( "','", $idList )."')";
			}
		}
	}

	if( $searchText != "" )
	{
		$select .= ",`searchcache`";

		$where .= " AND `seaObject` = `objid` ".
			"AND MATCH(`seaText`) AGAINST('".addslashes( $searchText )."' IN BOOLEAN MODE)";
	}

	if( $serverload >= 12 && !atLeastRetired())
	{
		$select = "";
		$where = "";
		return;
	}
}

?>
